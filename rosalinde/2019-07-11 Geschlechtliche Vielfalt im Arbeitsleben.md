---
id: '433745397422057'
title: Geschlechtliche Vielfalt im Arbeitsleben
start: '2019-07-11 18:00'
end: '2019-07-11 20:00'
locationName: null
address: 'Volkshaus, Karl-Liebknecht-Straße 32, Erich-Schilling-Saal (5. Etage)'
link: 'https://www.facebook.com/events/433745397422057/'
image: 61306133_2217209328374396_6300560647767719936_n.jpg
teaser: '// Brauchen wir eine 3. Toilette? - Geschlechtliche Vielfalt im Arbeitsleben //  Lesben, Schwule, Bisexuelle, Trans*- und Inter*-Personen und queere M'
recurring: null
isCrawled: true
---
// Brauchen wir eine 3. Toilette? - Geschlechtliche Vielfalt im Arbeitsleben //

Lesben, Schwule, Bisexuelle, Trans*- und Inter*-Personen und queere Menschen (LSBTIQ*) gehören zur Lebensrealität in Sachsen. Von einem offenen Umgang mit den Themen sexuelle und geschlechtliche Vielfalt am Arbeitsplatz kann jedoch gegenwärtig nicht die Rede sein. So erleben laut einer Studie aus dem Jahr 2017 rund 83% der trans*-Personen in mindestens einer Form Diskriminierungen am Arbeitsplatz. 

Welche Chancen haben trans*- und inter*-Personen sowie nicht-binäre Menschen in der Arbeitswelt? Welche Strategien haben Unternehmen, deren Bedürfnisse mitzudenken? Wo gibt es Handlungsbedarf im Umgang mit dem Thema trans* bzw. inter*? Studien zeigen, dass trans* Menschen starker Diskriminierung und großer Unsicherheit ausgesetzt sind. Unsicherheit besteht nicht selten auch auf Seiten der Kolleg*innen und Vorgesetzten*, weil es an Informationen und Erfahrungen fehlt. 

Die Veranstaltung klärt über Lebenslagen von trans*- und inter*-Personen sowie nicht-binären Menschen auf und bietet Austausch und Informationen zum Diskriminierungsabbau am Arbeitsplatz.

Eine Veranstaltung der ver.di Jugend Sachsen, Sachsen-Anhalt, Thüringen im Rahmen des diesjährigen CSD Leipzig // Eintritt frei. 

Referent*innen: 
Silvia Rentzsch, Trans-Inter-Aktiv in Mitteldeutschland e.V. (TIAM e.V.)
Tammo Wende, RosaLinde Leipzig e.V.
Moderation: Vera Ohlendorf, LAG Queeres Netzwerk Sachsen e.V.