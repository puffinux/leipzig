---
id: "459428208064390"
title: Ah! Sexuell - Gruppe für asexuelle Menschen
start: 2020-02-26 19:00
end: 2020-02-26 22:00
locationName: RosaLinde Leipzig e.V.
address: Demmeringstr. 32, 04177 Leipzig
link: https://www.facebook.com/events/459428208064390/
image: 79410457_2662965687103346_7185678090926817280_o.jpg
teaser: Wenn es um das Thema Sexualität geht, erlebst du häufiger solche
  Ah(a)-Momente, weil dir selbst das Ganze eher fremd erscheint?  Du bezeichnest
  dich s
isCrawled: true
---
Wenn es um das Thema Sexualität geht, erlebst du häufiger solche Ah(a)-Momente, weil dir selbst das Ganze eher fremd erscheint?

Du bezeichnest dich selbst als a-, demi-, grey-sexuell oder kannst/willst dich da nicht auf einen Begriff festlegen?

Vielleicht bist du a-, demi-, grey-romantisch, kannst mit dem Romantik-Begriff nichts anfangen?

Du bist dir vielleicht einfach unsicher, wie und ob du dich selbst durch diese Bezeichnungen definieren sollst, aber merkst, dass du in Bezug auf Sexualität nicht wie die anderen tickst?

Du findest dieses Schubladendenken zwar nervig, würdest dich aber trotzdem eher in diesem Spektrum verorten?

Du ärgerst dich, dass du gleich als hetero-sexuell/-romantisch abgestempelt wirst, wenn du weder als homo-, noch bi-sexuell/-romantisch zu erkennen bist und suchst einen Schutzraum, in dem du mit deiner Asexualität sichtbar wirst?

Dann komm vorbei! Wir treffen uns jeden 4. Mittwoch im Monat um 19 Uhr in den Räumen der RosaLinde!