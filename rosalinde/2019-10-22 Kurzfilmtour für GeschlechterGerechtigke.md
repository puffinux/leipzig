---
id: '3003023976391388'
title: Kurzfilmtour für GeschlechterGerechtigkeit in Mittweida
start: '2019-10-22 19:30'
end: '2019-10-22 21:00'
locationName: Filmbühne Mittweida
address: 'Theaterstr. 1, 09648 Mittweida'
link: 'https://www.facebook.com/events/3003023976391388/'
image: 71082507_10158950592153452_7960045485916422144_n.jpg
teaser: 'Kurzfilmtour für GeschlechterGerechtigkeit Die Tour macht Halt in der Filmbühne Mittweida  Klein aber fein, kurz aber geistreich – das Genre des Kurzf'
recurring: null
isCrawled: true
---
Kurzfilmtour für GeschlechterGerechtigkeit
Die Tour macht Halt in der Filmbühne Mittweida

Klein aber fein, kurz aber geistreich – das Genre des Kurzfilms ist abwechslungsreiches Experimentierfeld und kreative Fundgrube zugleich. In seiner künstlerischen Vielfalt bildet der Kurzfilm gesellschaftliche und menschliche Vielfalt ab. Doch nicht nur die Diversität der Geschlechter, der Lebensweisen, der Identitäten und der Biographien ist Thema der Kurzfilmtour, die die LAG Queeres Netzwerk Sachsen, das Genderkompetenzzentrum Sachsen sowie die Landesarbeitsgemeinschaft Jungen- und Männerarbeit Sachsen e.V., in Kooperation mit dem Filmfest Dresden zusammen auf den Weg bringen. Auch die Gewalt, die Menschen aufgrund eines angeblichen „Andersseins“ erleiden, findet Ausdruck. Und manchmal ist es die Solidarität untereinander, die die wahre Größe in unserer Gesellschaft ausmacht.

Mit unserer Kurzfilmtour halten wir (neu)rechter Polemik und gruppenbezogener Menschenfeindlichkeit entgegen:
Menschen sind vielfältig. Und Vielfalt bereichert unsere Gesellschaft.

Mit einem Programm aus fünf Kurzfilmen, die allesamt im Wettbewerb des Filmfest Dresden liefen, touren wir vom 2. Oktober bis 14. November 2019 durch Sachsen. Halt machen wir dabei jeweils bei lokal ansässigen Vereinen und Initiativen, die sich für eine demokratische und offene Gesellschaft einsetzen. Der Eintritt ist jeweils frei, um eine Spende von 2-7 Euro wird aber gebeten. Die Filme werden einzeln von Kooperationspartner*innen vor Ort laudatiert. Sie laufen entweder auf Deutsch oder mit deutschen Untertiteln. Im Anschluss und zwischen den Filmen gibt es Raum für Gespräche und Diskussionen.

Die Tourdaten 2019 sind:
2.10. Chemnitz, Weltecho (Annaberger Str. 24), 18:30 Uhr
10.10. Bautzen, Steinhaus (Steinstr. 37), 18:30 Uhr
17.10. Zwickau, Jugendcafé Citypoint (Hauptstraße 44), 19:00 Uhr
22.10. Mittweida, Filmbühne (Theaterstraße 1), 19:30 Uhr
30.10. Pirna, Begegnungszentrum (Lange Straße 43), 18:30 Uhr
6.11. Meißen, Filmpalast Meißen (Theaterplatz 14), 18:30 Uhr
14.11. Plauen, Malzhaus (Alter Teich 7), 18:30 Uhr

Das Programm 2019 zeigt:
GAME| Jeannie Donohoe | USA 2017 | Spielfilm | 16 min | englische Originalfassung, deutsche Untertitel
AJ ist neu in der Stadt, taucht beim Basketballtraining der Jungs auf und schindet sofort Eindruck. Werden Talent und Tatkraft ausreichen, um ein Team zu bilden?

DER KÄPT'N | Steve Bache | Deutschland 2019 | Spielfilm | 14 min | deutsche Originalfassung
Nach einem lebensverändernden Unfall versucht der Rettungsschwimmer Frank, sich als der starke Vater zu präsentieren, der er einmal war.

O ÓRFAO / THE ORPHAN | Carolina Markowicz | Brasilien 2018 | Spielfilm | 15 min | portugiesische Originalfassung, deutsche Untertitel
Jonathas wurde adoptiert, kehrte aber dann aufgrund seines Andersseins zurück. Inspiriert von wahren Ereignissen.

WREN BOYS | Harry Lighton | Großbritannien 2017 | Spielfilm | 10 min | englische Originalfassung, deutsche Untertitel
In einer Predigt am Stephanstag erinnert sich ein Priester an eine alte Tradition. Jungen an der Schwelle zur Männlichkeit wurden ausgesandt, um einen Zaunkönig zu töten. Später an diesem Tag fährt der Priester mit seinem Neffen ins Gefängnis, um einen Häftling zu besuchen.

JUCK | Olivia Kastebring, Julia Gumpert, Ulrika Bandeira | Schweden 2018 | Fiktion | 18 min | schwedische Originalfassung, deutsche Untertitel - Gewinnerfilm des Preises für GeschlechterGerechtigkeit 2019
JUCK ist eine Mischung aus Dokumentarfilm, Tanz und Fiktion. Der Beschränkung körperlicher Ausdrucksformen und weiblicher* Lebenswelten durch Sexismus, Rassismus, Heteronormativität, Gewalt, Alter oder Aussehen setzt die Gruppe JUCK ein empowerndes Selbstverständnis für alle entgegen: provokant, verbindend, aggressiv, solidarisch, ermutigend, befreiend.

Gesamtlänge: 73 min.

Eine Veranstaltung der LAG Queeres Netzwerk Sachsen, des Genderkompetenzzentrum Sachsen sowie der Landesarbeitsgemeinschaft Jungen- und Männerarbeit Sachsen e.V., in Kooperation mit dem Filmfest Dresden, Müllerhof Mittweida e.V.,  RosaLinde Leipzig e.V. und PRO FAMILIA Landesverband Sachsen und dem erucula e.V. Mittweida.

Unterstützt von der Hannchen Mehrzweck Stiftung. Gefördert im Rahmen der „Lokalen Partnerschaft für Demokratie“ im   Landkreis Mittelsachsen durch das Bundesprogramm „Demokratie Leben!“ und den Landespräventionsrat Sachsen. Diese Maßnahme wird mitfanziert mit Steuermitteln auf Grundlage des von den
Abgeordneten des Sächsischen Landtags beschlossenen Haushalts.