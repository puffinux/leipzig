---
id: "498100964074639"
title: Kostenloser und assistierter HIV-Selbsttest
start: 2019-11-13 18:00
end: 2019-11-13 20:00
locationName: RosaLinde Leipzig e.V.
address: Demmeringstr. 32, 04177 Leipzig
link: https://www.facebook.com/events/498100964074639/
image: 72336723_2527717313961518_6393671600259989504_o.jpg
teaser: "Kostenloser und assistierter HIV-Selbsttest –  ein Angebot der aidshilfe
  leipzig in der RosaLinde   Begleitet von: Peter Thürer, Sexualpädagoge/ Berat"
isCrawled: true
---
Kostenloser und assistierter HIV-Selbsttest –  ein Angebot der aidshilfe leipzig in der RosaLinde 

Begleitet von:
Peter Thürer, Sexualpädagoge/ Berater aidshilfe leipzig
Tammo Wende, psychosoziale Beratung RosaLinde Leipzig e.V.


Die aidshilfe leipzig bietet den HIV-Selbsttest mit begleitender Beratung und Testauswertung anonym und kostenlos an. Das Angebot richtet sich an alle Menschen, die ihren HIV-Status erfahren möchten und findet am Mittwoch, den 13. November, von 18 bis 20 Uhr in den Räumen des RosaLinde Leipzig e.V. statt.

Bitte beachte, dass eine HIV-Risikosituation mindestens 12 Wochen zurückliegen muss.

Übrigens: HIV ist eine gut behandelbare chronische Infektion, mit der man eine normale Lebenserwartung hat. Mit Medikamenten kann man dafür Sorge tragen, dass man das Virus auch beim Sex ohne Kondom nicht mehr weitergeben kann. Deshalb lohnt es sich, seinen HIV-Status zu kennen.

