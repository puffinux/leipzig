---
id: "1062294290768693"
title: Clitoral Embodiment by Nicole Bindler, Philadelphia US
start: 2019-11-22 19:00
end: 2019-11-22 21:00
locationName: RosaLinde Leipzig e.V.
address: Demmeringstr. 32, 04177 Leipzig
link: https://www.facebook.com/events/1062294290768693/
image: 74598177_2595290663870849_6606361979940503552_n.jpg
teaser: Lecture in english, translated to german if needed    The embryology of the
  genitalia represents an unexplored frontier in Body-Mind Centering® (BMC®)
isCrawled: true
---
Lecture in english, translated to german if needed  

The embryology of the genitalia represents an unexplored frontier in Body-Mind Centering® (BMC®). Unlike its embryological corollary, the penis, the clitoris is absent from most illustrated anatomy texts. Clitoral Embodiment remedies the inattention paid to female and non-binary genital development by presenting a framework for embodying multiple potentials for sex and gender expressions through imagery, movement and embryological study, with an emphasis on invagination over penetration. Standing at the intersection of somatics, embryology and gender/sexuality studies, Clitoral Embodiment includes lecture, discussion, and BMC embodied anatomy through guided solo somatic movement to explore an underlying biological explanation for sex and gender fluidity. People of all genders are welcome, and all explorations in this workshop will be practiced fully clothed. 


Nicole Bindler's performance work and teaching have been presented at festivals and intensives throughout the U.S., Canada, Argentina, Europe, and in Tokyo, Beirut, Bethlehem, Mexico City, and Quito. Her dances have been supported by the Pew Center for Arts and Heritage, Leeway Foundation, Puffin Foundation, Foundation for Contemporary Arts, Pennsylvania Council on the Arts, and the Ellen Forman Memorial Award. Bindler holds a BA in Dance and Poetry from Hampshire College, a degree in Muscular Therapy from the Muscular Therapy Institute, and certificates in Embodied Anatomy Yoga, Embodied Developmental Movement and Yoga, and Practitioner of Body-Mind Centering® from the School for Body-Mind Centering®. She has been on faculty at Temple University, University of the Arts, and the University of Pennsylvania, and has guest lectured at Drexel University, University of Wisconsin, Milwaukee, University of Minnesota, Minneapolis, Hameline University, Arizona State University, Tempe, University of Texas, Austin, Goldsmith’s University, St Mary’s College of Maryland, Moore College of Art and Design, and Ursinus College. Her writing on dance and somatics has been published in Critical Correspondence, Contact Quarterly, Emergency Index by Ugly Duckling Presse, Jewish Currents, BMC® Currents, Curate This, Journal of Dance & Somatic Practices, Somatics Toolkit, and thINKingDANCE. She currently serves on the Earthdance Diversity, Equity, and Inclusion Committee.

www.nicolebindler.com