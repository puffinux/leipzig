---
id: "2404353043160112"
title: Austauschabend zur Dritten Option des Geschlechtseintrags
start: 2019-10-22 19:00
end: 2019-10-22 22:00
locationName: RosaLinde Leipzig e.V.
address: Demmeringstr. 32, 04177 Leipzig
link: https://www.facebook.com/events/2404353043160112/
image: 71340470_2510647705668479_2497042854308741120_o.jpg
teaser: In einer offenen Gesprächsrunde wollen wir unsere Kenntnisstände zur "dritten
  Option" mit euch teilen. Desweiteren rufen wir explizit Menschen, die ih
isCrawled: true
---
In einer offenen Gesprächsrunde wollen wir unsere Kenntnisstände zur "dritten Option" mit euch teilen. Desweiteren rufen wir explizit Menschen, die ihren Geschlechtseintrag über die neue Regelung ändern konnten, dazu auf vorbei zu kommen und ihre Erfahrungen, wenn sie mögen, zur Verfügung zu stellen. 