---
id: '2224056747651023'
title: 'Karin Rick liest "Ladies, Lust und Leidenschaft"'
start: '2019-03-21 19:00'
end: '2019-03-21 22:00'
locationName: RosaLinde Leipzig e.V.
address: 'Lange Straße 11, 04103 Leipzig'
link: 'https://www.facebook.com/events/2224056747651023/'
image: 53778710_2152838018116118_4621123878347866112_o.jpg
teaser: 'Romantisch und leidenschaftlich, dann wieder hinreißend und schräg werden queere Lebenswelten durcheinandergewirbelt. Schauplätze ihrer Erzählungen si'
recurring: null
isCrawled: true
---
Romantisch und leidenschaftlich, dann wieder hinreißend und schräg werden queere Lebenswelten durcheinandergewirbelt. Schauplätze ihrer Erzählungen sind Flughäfen und Filmsets, Gärten und Hotels, Bars und der wilde Westen. Aus dem heißen Sexnachmittag beim "Making of" entwickeln sich slapstickartige Szenen, die voyeuristische Gier genauso genüsslich aufs Korn nehmen wie das chaotische und sexuell aufgeladene Treiben bei Filmdreharbeiten. Düster und melancholisch-romantisch entwickelt sich die Liebesgeschichte "Platzflimmern". "Ride, Sally, Ride" entwirft in musikalisch-poetischen Bildern die Sehnsucht nach unberührter Natur und endet in Momenten ungezügelter Lust...
Kunstvoll schilder Rick innige Gefühle und verschränkt sie mit sexuellen Begegnungen, verschiedene Ebenen in den Texten schimmern auf. Ein genauer Blick und scharfer Humor runden die Erzählung ab.

Kurzvita der Autorin:
Die Wienerin Karin Rick schreibt seit vielen Jahren über Liebe, Sex und Macht. Ihre Romane verbinden grenzgängerisch sexuelle Freizügigkeit mit innigen Gefühlen. Von 1973-79 absolvierte sie ein Dolmetsch- und Übersetzungsstudium für Französisch und Englisch an der Universität Wien, außerdem studierte sie Kommunikationswissenschaft und Kunstgeschichte. Rick war Lehrbeauftragte in Frankreich, Belgien und Spanien. Sie organisierte Symposien und übersetzte u.a. Bücher von Hélène Cixous und Alain Robbe-Grillet.