---
id: "718493695198537"
title: Regenbogentresen
start: 2019-12-19 19:00
end: 2019-12-19 22:00
address: Demmeringstr. 32
link: https://www.facebook.com/events/718493695198537/
image: 49688637_2067241066675814_7428521881214386176_o.jpg
teaser: Unsere Gruppe bietet monatlich Gelegenheit zum Kennenlernen und Austausch für
  junggebliebene lsbti* Menschen. Darüber hinaus werden Informationsverans
isCrawled: true
---
Unsere Gruppe bietet monatlich Gelegenheit zum Kennenlernen und Austausch für junggebliebene lsbti* Menschen. Darüber hinaus werden Informationsveranstaltungen, Gesprächsabende sowie Freizeitaktivitäten organisiert.
Wir treffen uns immer am 3. Donnerstag im Monat ab 19 Uhr.