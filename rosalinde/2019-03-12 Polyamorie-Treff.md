---
id: '2213967515591033'
title: Polyamorie-Treff
start: '2019-03-12 19:00'
end: '2019-03-12 22:00'
locationName: RosaLinde Leipzig e.V.
address: 'Lange Straße 11, 04103 Leipzig'
link: 'https://www.facebook.com/events/2213967515591033/'
image: 49393977_2067213306678590_3703103790245740544_o.jpg
teaser: 'Du führst schon lange polyamore oder offene Beziehungen? Du bist neugierig, ob und wie das für dich funktionieren kann? Oder du möchtest einfach nur m'
recurring: null
isCrawled: true
---
Du führst schon lange polyamore oder offene Beziehungen? Du bist neugierig, ob und wie das für dich funktionieren kann? Oder du möchtest einfach nur mal kritisch über Monoamorie nachdenken, ohne schief angeguckt zu werden?

Dann komm zu uns! Die Gruppe bietet Informations- und Austauschmöglichkeiten zu verantwortungsvoller nicht-monoamorer Beziehungsgestaltung für alle Menschen unabhängig von sexueller Identität und Beziehungsstatus.

Wir treffen uns immer am 2. Dienstag und am 4. Donnerstag im Monat ab 19 Uhr in der RosaLinde.
Einlass: 19 Uhr. Pünktlicher Beginn der Vorstellungsrunde: 19.30 Uhr.
