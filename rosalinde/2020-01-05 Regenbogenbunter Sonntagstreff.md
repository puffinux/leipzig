---
id: "497131724409740"
title: Regenbogenbunter Sonntagstreff
start: 2020-01-05 15:00
end: 2020-01-05 20:00
locationName: RosaLinde Leipzig e.V.
address: Demmeringstr. 32, 04177 Leipzig
link: https://www.facebook.com/events/497131724409740/
image: 70826423_2481736518559598_5010272443923693568_o.jpg
teaser: Alle sind willkommen am ersten Sonntag im Monat von 15-20 Uhr zu unserem
  Sonntagnachmittagtreff.
isCrawled: true
---
Alle sind willkommen am ersten Sonntag im Monat von 15-20 Uhr zu unserem Sonntagnachmittagtreff.