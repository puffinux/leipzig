---
id: "468414577109030"
title: RosaLinde Einweihungsfeier
start: 2019-11-19 18:30
end: 2019-11-19 23:00
locationName: RosaLinde Leipzig e.V.
address: Demmeringstr. 32, 04177 Leipzig
link: https://www.facebook.com/events/468414577109030/
image: 74891473_2553876268012289_2314877382726516736_o.jpg
teaser: "Es ist vollbracht: Nach vielen Jahren der Suche haben wir endlich sehr schöne
  neue Räume bezogen. Nicht zuletzt die steigenden Mitgliederzahlen und da"
isCrawled: true
---
Es ist vollbracht: Nach vielen Jahren der Suche haben wir endlich sehr schöne neue Räume bezogen. Nicht zuletzt die steigenden Mitgliederzahlen und damit die bessere finanzielle Basis haben den Ausschlag für diese Entscheidung gegeben. Das wollen wir gebührend mit euch feiern und gemeinsam anstoßen!

Darum laden wir am 19.11.19 ab 18.30 Uhr in die Demmeringstr. 32 in Leipzig-Lindenau.

Für Essen sorgen wir, Getränke könnt ihr gewohnt günstig an unserer Bar erwerben. 

18.30 Uhr Ankommen
19.00 Uhr Rück- und Ausblicke auf die Vereinsarbeit
ab 19.30 Uhr Get-together 

Es gibt Musik von Crossing Borders!

Wir freuen uns auf euch!