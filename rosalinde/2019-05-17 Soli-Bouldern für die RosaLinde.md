---
id: '280303882919570'
title: Soli-Bouldern für die RosaLinde
start: '2019-05-17 10:00'
end: '2019-05-17 21:00'
locationName: Kosmos - Bouldern in Leipzig
address: 'Erich-Zeigner-Allee 64 c, 04229 Leipzig'
link: 'https://www.facebook.com/events/280303882919570/'
image: 57056145_2203294116403841_2914233854065639424_n.jpg
teaser: 'Soli-Bouldern für die RosaLinde zum IDAHIT*  ACHTUNG: neue Zeit für den Beginn ist aus technischen Gründen 12 Uhr!  Zum diesjährigen Internationalen T'
recurring: null
isCrawled: true
---
Soli-Bouldern für die RosaLinde zum IDAHIT*

ACHTUNG: neue Zeit für den Beginn ist aus technischen Gründen 12 Uhr!

Zum diesjährigen Internationalen Tag gegen Homo-, Bi-, Inter- und Transfeindlichkeit (IDAHIT*) freuen wir uns über eine Kooperation mit der KOSMOS Boulderhalle.

Ein tolles Angebot für all diejenigen, die ohnehin bereits bouldern gehen, es schon länger mal ausprobieren wollten oder deren Interesse jetzt geweckt wurde. Kommt vorbei und bringt Freund*innen mit, lernt neue Leute kennen und habt einen schönen Tag in angenehmer Atmosphäre – all das für einen guten Zweck!

+ von jedem Eintritt spendet die Boulderhalle 2 Euro an die RosaLinde
+ von 15 bis 21 Uhr sind wir mit einem Infostand vertreten

Leihschuhe gegen Gebühr und kostenloses Chalk vor Ort. Ansonsten braucht ihr nur Sportklamotten und Klettermotivation.

Hinweis zu Kindern und Jugendlichen: 
+ Kinder unter 6 Jahren können im liebevoll eingerichteten Kinderbereich spielen.
+ Kinder von 6 bis 11 Jahren können mit 1:1 Betreuung im Erwachsenenbereich bouldern, wenn ZUVOR an einer Elternschulung teilgenommen wurde.
+ Kinder und Jugendliche ab 12 Jahren können selbstständig bouldern, wenn eine Einverständniserklärung der Eltern vorliegt.

Nähere Infos dazu unter http://kosmos-bouldern.de/.

Am Tresen bekommt ihr Getränke, Snacks und Pizza, das meiste  davon auch in vegan. Der gemütliche Außenbereich lädt zum Verweilen ein.

--

Der IDAHIT* erinnert an den 17.05.1990, an welchem die Weltgesundheitsorganisation beschloss, Homosexualität aus ihrem Krankheitskatalog zu streichen.

--

Am Vorabend zeigen wir den Film KIKI in der Cinémathèque: https://www.facebook.com/events/881556045569768