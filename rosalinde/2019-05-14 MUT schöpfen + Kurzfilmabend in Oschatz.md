---
id: '851846391825187'
title: MUT schöpfen + Kurzfilmabend in Oschatz
start: '2019-05-14 14:00'
end: '2019-05-14 19:00'
locationName: null
address: 'Altmarkt, 04758 Oschatz'
link: 'https://www.facebook.com/events/851846391825187/'
image: 56675219_2146797542082242_9082817724559654912_n.jpg
teaser: 'Im Rahmen des Internationalen Tag gegen Homo-, Bi-, Trans*- und Inter*feindlichkeit (IDAHIT*) schaffen wir mit einer Aktionswoche Sichtbarkeit von Ges'
recurring: null
isCrawled: true
---
Im Rahmen des Internationalen Tag gegen Homo-, Bi-, Trans*- und Inter*feindlichkeit (IDAHIT*) schaffen wir mit einer Aktionswoche Sichtbarkeit von Geschlechtervielfalt.

14.05. Oschatz

16.00 – 18.00 Uhr // Altmarkt Oschatz
Aktion „Mut schöpfen für Respekt & Akzeptanz von sexueller und
geschlechtlicher Vielfalt“ // Infostand mit Redebeiträgen
In Kooperation mit dem Wegweiser e.V., der Lebenshilfe e.V. RV Oschatz und der Gleichstellungsbeauftragten des Landkreises Nordsachsen schöpfen wir Wasser wie Mut in viele Gefäße. Symbolisch stehen diese für Menschen, die von Homo-, Bi-, Trans*- und Inter*feindlichkeit und Gewalt betroffen sind. Wir machen sichtbar, dass von Diskriminierung Betroffene sehr verschieden sind und ihren ganz eigenen Mut benötigen, um damit umzugehen. Bitte bringt Schüsseln, Tassen, Vasen, Kannen, Töpfe usw. mit, die ihr nicht mehr braucht!

19 Uhr // Soziokulturelles Zentrum "E-Werk" Oschatz, Lichtstraße 1
Kurzfilmabend und Diskussion: 
* Neko No Hi - Cat Days (Jon Frickey, BRD 2018, 11 min)
* Nasser (Melissa Martens, NL 2015, 19 min)
* Dawn (Jake Graf, UK 2016, 14 min)
* Mrs McCutcheon (John Sheedy, Australien 2017, 17 min)
Alle Filme laufen mit deutschen Untertiteln. Der Eintritt ist frei.

+ + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + 

Jährlich bietet der Tag gegen Homo-, Bi-, Trans*- und Inter*feindlichkeit (IDAHIT*) einen Anlass zur Erinnerung daran, dass Homosexualität erst am 17.05.1990 aus dem Krankheitskatalog der Weltgesundheitsorganisation gestrichen wurde.

Wir wollen gemeinsam mit Euch für gleiche Rechte, gesellschaftlichen Zusammenhalt und gegenseitigen Respekt von Lesben, Schwulen, Bisexuellen, trans*-, inter*geschlechtlichen und queeren Menschen sowie Asexuellen/Aromantischen (LSBTIQA*) streiten und jeder Ideologie der Ungleichwertigkeit entgegentreten.

Trans*- und Inter*geschlechtlichkeit gelten auch 2019 noch als Geschlechtsdifferenzierungs- bzw. Geschlechtsidentitätsstörungen. In Bezug auf gleichgeschlechtliche Lebensweisen ist ebenfalls noch keine volle Gleichstellung erreicht. Nicht zuletzt der aktuelle rechtskonservative Rollback und sog. „besorgte Eltern“ erfordern ein aktives Zeichen gegen Homo-, Bi-, Trans*- und Inter*feindlichkeit in Oschatz, Sachsen und überall.
Selbstbestimmt, offen und diskriminierungsfrei – eine demokratische Gesellschaft muss es allen Menschen ermöglichen, jederzeit und an jedem Ort ohne Angst verschieden sein zu können. Gewalt und Vorurteile gegenüber Lesben, Schwulen, Bisexuellen, trans*-, inter*geschlechtlichen und queeren Menschen sowie Asexuellen/Aromantischen haben keinen Platz in Sachsen.

LSBTIQA*-Rechte sind Menschenrechte!

+ + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + 

In Kooperation mit: RosaLinde Leipzig e.V.,  Queer durch Sachsen, der Gleichstellungsbeauftragten des Landkreises Nordsachsen, Caola Koch, Soziokulturelles Zentrum "E-Werk" Oschatz, Wegweiser e.V., Lebenshilfe e.V. RV Oschatz; Filmfest Dresden

Diese Veranstaltung wird gefördert vom BMFSFJ im Rahmen des Modellprojekts "Demokratie Leben!" sowie dem Freistaat Sachsen und der Partnerschaft für Demokratie im Landkreis Nordsachsen.

+ + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + 

Die Aktionswoche:
13.05. Bautzen
14.05. Oschatz
15.05. Döbeln
16.05. Hoyerswerda
17.05. Wurzen
18.05. Görlitz
19.05. Zwickau