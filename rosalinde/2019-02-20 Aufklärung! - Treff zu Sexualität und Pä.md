---
id: '199685674234445'
title: Aufklärung! - Treff zu Sexualität und Pädagogik
start: '2019-02-20 19:00'
end: '2019-02-20 22:00'
locationName: RosaLinde Leipzig e.V.
address: 'Lange Straße 11, 04103 Leipzig'
link: 'https://www.facebook.com/events/199685674234445/'
image: 49831817_2067153133351274_2897329004522504192_o.jpg
teaser: 'Du bist Sozialarbeiter*in, Lehrer*in, Erzieher*in oder arbeitest in einem anderen pädagogischen Beruf?  Du hast Lust, dich mit anderen Menschen zum Th'
recurring: null
isCrawled: true
---
Du bist Sozialarbeiter*in, Lehrer*in, Erzieher*in oder arbeitest in einem anderen pädagogischen Beruf?

Du hast Lust, dich mit anderen Menschen zum Thema Aufklärung, Sexualität, Gender, LGBTIAPQ* u.ä. auszutauschen und willst Wege finden, dies in deinem Berufsfeld zu integrieren?

Dann komm zum Treff zu Sexualität und Pädagogik, jeden 3. Mittwoch im Monat ab 19 Uhr! Wir freuen uns auf dich.