---
id: "562707967847885"
title: Trans* inter* Jugendgruppe
start: 2020-01-18 15:00
end: 2020-01-18 18:00
locationName: RosaLinde Leipzig e.V.
address: Demmeringstr. 32, 04177 Leipzig
link: https://www.facebook.com/events/562707967847885/
image: 78980262_2658044100928838_5386794892276006912_o.jpg
teaser: Du bist trans* und oder inter* und willst Teil einer Community werden? Wir
  sind eine Gruppe für junge trans* und inter* Personen im Alter von 14-25 Ja
isCrawled: true
---
Du bist trans* und oder inter* und willst Teil einer Community werden?
Wir sind eine Gruppe für junge trans* und inter* Personen im Alter von
14-25 Jahren, egal ob Mann, Frau, nicht binär, inter oder agender: ihr
alle seid bei uns willkommen. 

Wir treffen uns monatlich am 1. Montag um 19 Uhr und jeden 3. Samstag um 15 Uhr in den Räumen der RosaLinde (rechte Seite).

Seid dabei und entwickelt mit uns die Dynamik der Gruppe, gestaltet mit und bringt eure Ideen ein (oder kommt einfach und habt einen gemütlichen Nachmittag/Abend). Wir freuen uns auf euch!