---
id: "529728231111217"
title: Elterngruppe - Vernetzungstreffen für Eltern von Transkindern
start: 2019-10-24 19:00
end: 2019-10-24 22:00
locationName: RosaLinde Leipzig e.V.
address: Demmeringstr. 32, 04177 Leipzig
link: https://www.facebook.com/events/529728231111217/
image: 71391064_2510418779024705_165596127704383488_o.jpg
teaser: Ihr Kind hat sich ihnen anvertraut und ihnen mitgeteilt, dass es sich als
  Junge oder Mädchen oder keines von beiden identifiziert? Dann sind sie genau
isCrawled: true
---
Ihr Kind hat sich ihnen anvertraut und ihnen mitgeteilt, dass es sich als Junge oder Mädchen oder keines von beiden identifiziert? Dann sind sie genau richtig. Ziel der Gruppe soll sein, sich mit anderen Eltern über die Herausforderungen, die ein möglicher Transitionsprozess mit sich bringt auszutauschen, Fragen bei alltäglichen und strukturellen Hürden zu klären und sich bei Unsicherheiten zu unterstützen.


Moderation:
Manuela Tillmanns, M.A. 
Tammo Wende, M.A.