---
id: '586449678469936'
title: Bi/Pan Gruppe – Treff für bi- und pansexuelle Menschen
start: '2019-09-12 19:00'
end: '2019-09-12 22:00'
locationName: RosaLinde Leipzig e.V.
address: 'Lange Straße 11, 04103 Leipzig'
link: 'https://www.facebook.com/events/586449678469936/'
image: 49617441_2066277853438802_4920149448639643648_o.jpg
teaser: 'Hast du Interesse, dich zum Thema Bi_Pansexualität auszutauschen und/oder Lust, dich einzubringen? Wir auch! Deshalb wollen wir uns einen Bi_Pan-Raum'
recurring: null
isCrawled: true
---
Hast du Interesse, dich zum Thema Bi_Pansexualität auszutauschen und/oder Lust, dich einzubringen? Wir auch! Deshalb wollen wir uns einen Bi_Pan-Raum schaffen. Lasst uns gemeinsam überlegen, was wir wollen und was so geht. Wir treffen uns dazu immer am zweiten Donnerstag im Monat in der RosaLinde.

Wir freuen uns aufs (Er)Finden!
