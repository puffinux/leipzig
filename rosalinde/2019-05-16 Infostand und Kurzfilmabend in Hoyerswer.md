---
id: '314432649227806'
title: Infostand und Kurzfilmabend in Hoyerswerda
start: '2019-05-16 11:00'
end: '2019-05-16 17:00'
locationName: null
address: Zentralpark am Lausitz-Center
link: 'https://www.facebook.com/events/314432649227806/'
image: 56558660_2146825115412818_558019154871844864_n.jpg
teaser: 'Im Rahmen des Internationalen Tag gegen Homo-, Bi-, Trans*- und Inter*feindlichkeit (IDAHIT*) schaffen wir mit einer Aktionswoche Sichtbarkeit von Ges'
recurring: null
isCrawled: true
---
Im Rahmen des Internationalen Tag gegen Homo-, Bi-, Trans*- und Inter*feindlichkeit (IDAHIT*) schaffen wir mit einer Aktionswoche Sichtbarkeit von Geschlechtervielfalt.

16.05. Hoyerswerda 

13.00 – 15.30 Uhr // Zentralpark am Lausitz-Center (gegenüber Westeingang)
Infostand mit Redebeiträgen: Wir zeigen öffentlich Präsenz und setzen ein Zeichen gegen Homo-, Bi-, Trans*- und Inter*feindlichkeit. Seid dabei!

17.00 Uhr // Jugendclubhaus Ossi, Liselotte-Herrmann-Straße 1
Kurzfilmabend und Diskussion: 
* Neko No Hi - Cat Days (Jon Frickey, BRD 2018, 11 min)
* Nasser (Melissa Martens, NL 2015, 19 min)
* Dawn (Jake Graf, UK 2016, 14 min)
* Mrs McCutcheon (John Sheedy, Australien 2017, 17 min)
Alle Filme laufen mit deutschen Untertiteln. Der Eintritt ist frei.

+ + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + 

Jährlich bietet der Tag gegen Homo-, Bi-, Trans*- und Inter*feindlichkeit (IDAHIT*) einen Anlass zur Erinnerung daran, dass Homosexualität erst am 17.05.1990 aus dem Krankheitskatalog der Weltgesundheitsorganisation gestrichen wurde.

Wir wollen gemeinsam mit Euch für gleiche Rechte, gesellschaftlichen Zusammenhalt und gegenseitigen Respekt von Lesben, Schwulen, Bisexuellen, trans*-, inter*geschlechtlichen und queeren Menschen sowie Asexuellen/Aromantischen (LSBTIQA*) streiten und jeder Ideologie der Ungleichwertigkeit entgegentreten.

Trans*- und Inter*geschlechtlichkeit gelten auch 2019 noch als Geschlechtsdifferenzierungs- bzw. Geschlechtsidentitätsstörungen. In Bezug auf gleichgeschlechtliche Lebensweisen ist ebenfalls noch keine volle Gleichstellung erreicht. Nicht zuletzt der aktuelle rechtskonservative Rollback und sog. „besorgte Eltern“ erfordern ein aktives Zeichen gegen Homo-, Bi-, Trans*- und Inter*feindlichkeit in Hoyerswerda, Sachsen und überall.
Selbstbestimmt, offen und diskriminierungsfrei – eine demokratische Gesellschaft muss es allen Menschen ermöglichen, jederzeit und an jedem Ort ohne Angst verschieden sein zu können. Gewalt und Vorurteile gegenüber Lesben, Schwulen, Bisexuellen, trans*-, inter*geschlechtlichen und queeren Menschen sowie Asexuellen/Aromantischen haben keinen Platz in Sachsen.

LSBTIQA*-Rechte sind Menschenrechte!

+ + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + 

In Kooperation mit: Gerede E V Dresden, Queer durch Sachsen, RosaLinde Leipzig e.V., Jugendclubhaus Ossi, RAA Sachsen Opferberatung Hoyerswerda/ Ostsachsen e.V., Filmfest Dresden

Diese Veranstaltung wird gefördert vom BMFSFJ im Rahmen des Modellprojekts "Demokratie Leben!" sowie dem Freistaat Sachsen und der Partnerschaft für Demokratie im Landkreis Bautzen.

+ + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + 

Die Aktionswoche:
13.05. Bautzen
14.05. Oschatz
15.05. Döbeln
16.05. Hoyerswerda
17.05. Wurzen
18.05. Görlitz
19.05. Zwickau