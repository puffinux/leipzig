---
id: "812524769176568"
title: Aufklärung! - Treff zu Sexualität und Pädagogik
start: 2020-02-19 19:00
end: 2020-02-19 22:00
locationName: RosaLinde Leipzig e.V.
address: Demmeringstr. 32, 04177 Leipzig
link: https://www.facebook.com/events/812524769176568/
image: 78474735_2658022787597636_1382743044247257088_o.jpg
teaser: Vorstellung Bildungsarbeit des RosaLinde Leipzig e.V.  Im März-Treff wird
  Steff von der RosaLinde die Bildungsarbeit des Vereins vorstellen. Das Kerna
isCrawled: true
---
Vorstellung Bildungsarbeit des RosaLinde Leipzig e.V.

Im März-Treff wird Steff von der RosaLinde die Bildungsarbeit des Vereins vorstellen. Das Kernangebot besteht aus einem Antidiskriminierungsprojekt, welches vor allem an Schulen Workshops durchführt, um Schüler*innen zu sexuellen Orientierungen und Geschlechtlichkeiten zu informieren und für Diskriminierung zu sensibilisieren. Die Besonderheit des Projekts liegt darin, dass die Workshops von jungen Ehrenamtlichen durchgeführt werden, die selbst über Coming-out-Erfahrungen verfügen und diese am Ende mit den Schüler*innen teilen. Außerdem bietet der Verein Fortbildungen für pädagogische Fachkräfte an, um ihnen Methoden- und Handlungskompetenz für ihren pädagogischen Alltag zu vermitteln. Neu im letzten Jahr hinzugekommen sind die Schul-AGs, in welchen sich Schüler*innen regelmäßig zu Themen rund um sexuelle Orientierungen und Geschlechtlichkeiten austauschen können.

--

Du bist Sozialarbeiter*in, Lehrer*in, Erzieher*in oder arbeitest in einem anderen pädagogischen Beruf?

Du hast Lust, dich mit anderen Menschen zum Thema Aufklärung, Sexualität, Gender, LGBTIAPQ* u.ä. auszutauschen und willst Wege finden, dies in deinem Berufsfeld zu integrieren?

Dann komm zum Treff zu Sexualität und Pädagogik, jeden 3. Mittwoch im Monat ab 19 Uhr! Wir freuen uns auf dich.