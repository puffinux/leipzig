---
id: "490486818254370"
title: Queer und Glauben
start: 2020-02-12 19:00
end: 2020-02-12 22:00
locationName: RosaLinde Leipzig e.V.
address: Demmeringstr. 32, 04177 Leipzig
link: https://www.facebook.com/events/490486818254370/
image: 78814913_2657833670949881_8752860156284370944_o.jpg
teaser: Am 2. Mittwoch im Monat ab 19 Uhr treffen wir uns in den Räumen des Rosa Linde
  e.V. Lange Straße 11, 04103 Leipzig.  Wir reden miteinander darüber, wa
isCrawled: true
---
Am 2. Mittwoch im Monat ab 19 Uhr treffen wir uns in den Räumen des Rosa Linde e.V. Lange Straße 11, 04103 Leipzig.

Wir reden miteinander darüber, was wir (nicht) glauben, mit oder ohne religiöse Vorkenntnisse, gleich, in welcher Kirche wir sind oder (nie) waren.
Dazu denkt sich jeweils eine/r einen Einstieg aus. Manchmal singen wir etwas, ein Gebet zum Abschluss hat sich inzwischen etabliert. Es kommt vor, dass wir nur wenige sind, aber die Vorbereitung eines Friedensgebetes zum Leipziger CSD schließt uns immer wieder zusammen.

Ob LSBTI oder nicht, Du bist willkommen! Bring Dich ein oder hör erst einmal einfach nur zu. Gemeinsam sind wir auf dem Weg, im Vertrauen darauf, dass Menschen in Vielfalt geschaffen sind und jedes Wesen geliebt ist – ohne Vorbedingung.