---
id: '832815313772061'
title: Empowerment-Workshop für LSBTIAQ*-Lehrkräfte und Studierende
start: '2019-07-11 09:30'
end: '2019-07-11 15:30'
locationName: null
address: 'Dittrichring 5-7, Raum 405'
link: 'https://www.facebook.com/events/832815313772061/'
image: 64280034_2313208428745742_7920876926070161408_o.jpg
teaser: 'Empowerment-Workshop für LSBTIAQ*-Lehrkräfte und Studierende  Do, 11.07.19, 9.30-15.30 Dittrichring 5-7, Raum 405  Schule gilt als einer der queerfein'
recurring: null
isCrawled: true
---
Empowerment-Workshop für LSBTIAQ*-Lehrkräfte und Studierende

Do, 11.07.19, 9.30-15.30
Dittrichring 5-7, Raum 405

Schule gilt als einer der queerfeindlichsten Orte unserer Gesellschaft. In diesem Klima fällt es nicht nur Schüler*innen schwer, ihr Coming-out zu haben, sondern auch (angehende) Lehrkräfte stehen vor der Frage, wie sie mit ihrer sexuellen Orientierung oder Geschlechtlichkeit in der Schule umgehen sollen. Der Workshop wird hinsichtlich dessen Raum für Austauschmöglichkeiten sowie Auseinandersetzungen mit der eigenen Biografie bieten.

Unser Workshop richtet sich an alle, die in der Schule arbeiten oder arbeiten werden und sich im LSBTIAQ*-Spektrum verorten. Anmeldung via anmeldung@rosalinde-leipzig.de.


Stefanie Krüger arbeitet seit 2013 im RosaLinde Leipzig e.V. und ist dort für die Bildungsarbeit zuständig. Tina Grübler absolviert momentan ein Praktikum im RosaLinde Leipzig e.V., ist bei SchLAu NRW aktiv und gibt Fortbildungen bei "Schule der Vielfalt". Marielle Post ist auf Honorarbasis für den RosaLinde Leipzig e.V. im Bereich der Erwachsenenbildung tätig.


Der Workshop wird veranstaltet in Zusammenarbeit mit dem Projekt „Vielfalt Lehren!“ im Rahmen des Modellprojekts „Akzeptanz für Vielfalt - gegen Homo-, Trans*- und Inter*feindlichkeit“ im Bundesprogramm „Demokratie Leben“ des Bundesministerium für Familie, Senioren, Frauen und Jugend." 

--

Sexuelle Orientierung/Geschlechtlichkeit als Thema in der Schule: https://www.facebook.com/events/688038954988586/