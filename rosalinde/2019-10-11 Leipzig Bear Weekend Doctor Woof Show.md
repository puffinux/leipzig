---
id: '489056825189964'
title: 'Leipzig Bear Weekend: Doctor Woof Show'
start: '2019-10-11 19:00'
end: '2019-10-11 22:00'
locationName: null
address: null
link: 'https://www.facebook.com/events/489056825189964/'
image: 65830174_699495280520809_7176519768952799232_n.jpg
teaser: 'Eine Explosion aus Glitzer, Glamour und Travestieeee. In dieser Escandelousment Chic Edition bringt der Doctor einige bezaubernde Broadway Classic Son'
recurring: null
isCrawled: true
---
Eine Explosion aus Glitzer, Glamour und Travestieeee. In dieser Escandelousment Chic Edition bringt der Doctor einige bezaubernde Broadway Classic Songs mit einem ganz anderen Spin auf die Bühne nach Leipzig.

Er ist keck, sexy, frech… und soooo charmant!

Das Garantiert Ihnen ein Lächeln im Gesicht und vielleicht auch einen Klos in Ihrem…….. Hals.
Lassen Sie sich überraschen von Doctor Woof aus London im Blauen Salon Leipzig! 

Eine öffentliche Veranstaltung für Bären, Freunde und Freundinnen! Alle Geschlechter und sexuellen Orientierungen sind herzlich Willkommen! 