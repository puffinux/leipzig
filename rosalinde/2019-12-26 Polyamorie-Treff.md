---
id: "1266966513504664"
title: Polyamorie-Treff
start: 2019-12-26 19:00
end: 2019-12-26 21:00
locationName: RosaLinde Leipzig e.V.
address: Demmeringstr. 32, 04177 Leipzig
link: https://www.facebook.com/events/1266966513504664/
image: 78182256_2646910525375529_1375829237822390272_o.jpg
teaser: Du führst schon lange polyamore oder offene Beziehungen? Du bist neugierig, ob
  und wie das für dich funktionieren kann? Oder du möchtest einfach nur m
isCrawled: true
---
Du führst schon lange polyamore oder offene Beziehungen? Du bist neugierig, ob und wie das für dich funktionieren kann? Oder du möchtest einfach nur mal kritisch über Monoamorie nachdenken, ohne schief angeguckt zu werden?

Dann komm zu uns! Die Gruppe bietet Informations- und Austauschmöglichkeiten zu verantwortungsvoller nicht-monoamorer Beziehungsgestaltung für alle Menschen unabhängig von sexueller Identität und Beziehungsstatus.

Wir treffen uns immer am 2. Dienstag und am 4. Donnerstag im Monat ab 19 Uhr in der RosaLinde.
Einlass: 19 Uhr. Pünktlicher Beginn der Vorstellungsrunde: 19.30 Uhr.