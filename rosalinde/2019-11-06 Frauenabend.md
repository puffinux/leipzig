---
id: "1218282928340563"
title: Frauen*abend
start: 2019-11-06 19:00
end: 2019-11-06 22:00
address: Demmeringstr. 32
link: https://www.facebook.com/events/1218282928340563/
image: 49411185_2067178703348717_8140747717975474176_o.jpg
teaser: Der Frauen*abend bietet Frauen* die Möglichkeit zu regelmäßigen Treffen und
  gemeinsamen Unternehmungen. Kommt vorbei, immer am 1. Mittwoch im Monat ab
isCrawled: true
---
Der Frauen*abend bietet Frauen* die Möglichkeit zu regelmäßigen Treffen und gemeinsamen Unternehmungen. Kommt vorbei, immer am 1. Mittwoch im Monat ab 19 Uhr!