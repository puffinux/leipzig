---
id: "2529291317124431"
title: Regenbogentresen
start: 2020-02-20 19:00
end: 2020-02-20 22:00
locationName: RosaLinde Leipzig e.V.
address: Demmeringstr. 32, 04177 Leipzig
link: https://www.facebook.com/events/2529291317124431/
image: 78976203_2658028020930446_4045110047456362496_o.jpg
teaser: Unsere Gruppe bietet monatlich Gelegenheit zum Kennenlernen und Austausch für
  junggebliebene lsbti* Menschen. Darüber hinaus werden Informationsverans
isCrawled: true
---
Unsere Gruppe bietet monatlich Gelegenheit zum Kennenlernen und Austausch für junggebliebene lsbti* Menschen. Darüber hinaus werden Informationsveranstaltungen, Gesprächsabende sowie Freizeitaktivitäten organisiert.
Wir treffen uns immer am 3. Donnerstag im Monat ab 19 Uhr.