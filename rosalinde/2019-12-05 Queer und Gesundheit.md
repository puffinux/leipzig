---
id: "222971371925081"
title: Queer und Gesundheit
start: 2019-12-05 19:00
end: 2019-12-05 22:00
address: Demmeringstr. 32
link: https://www.facebook.com/events/222971371925081/
image: 49734975_2067221490011105_7532805156040081408_o.jpg
teaser: Du bist Hebamme/Entbindungspfleger, Psycholog*in, Ärzt*in oder
  Krankenschwester/Krankenpfleger, Physiotherapeut*in oder arbeitest in einem
  anderen Ges
isCrawled: true
---
Du bist Hebamme/Entbindungspfleger, Psycholog*in, Ärzt*in oder Krankenschwester/Krankenpfleger, Physiotherapeut*in oder arbeitest in einem anderen Gesundheitsberuf und bist queer* bzw. LGBTI*? Dann komm doch zum Stammtisch queere Menschen in Gesundheitsberufen, immer am 1. Donnerstag im Monat ab 19 Uhr! Das Treffen findet in der rechten Linde-Hälfte statt.