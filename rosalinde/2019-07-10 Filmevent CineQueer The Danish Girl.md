---
id: '443118332932483'
title: 'Filmevent CineQueer*: The Danish Girl'
start: '2019-07-10 19:00'
end: '2019-07-10 22:30'
locationName: Cinestar Leipzig
address: 'Petersstraße 44, 04109 Leipzig'
link: 'https://www.facebook.com/events/443118332932483/'
image: 62542668_2249212118447396_998616004110057472_n.jpg
teaser: 'Im Rahmen des diesjährigen CSD veranstaltet das CineStar Leipzig erstmalig einen queeren Filmabend. Das CineQueer* Event startet um 19:15 Uhr, der Fil'
recurring: null
isCrawled: true
---
Im Rahmen des diesjährigen CSD veranstaltet das CineStar Leipzig erstmalig einen queeren Filmabend. Das CineQueer* Event startet um 19:15 Uhr, der Film "The Danish Girl" beginnt um 20 Uhr. In Kooperation mit der RosaLinde Leipzig e.V. bieten wir in unserem Barbereich die Möglichkeit zum Informationsaustausch und zur Vernetzung. Kurz vor Filmbeginn wird es ein Gewinnspiel im Saal geben, bei dem wir unter anderem Folgendes verlosen: 1 x 2 Freikarten für den Prideball im Täubchenthal am 13.07.19 und 1 x 2 Freikarten für die KissKissBangBang im TwentyOne am 19.07.19.
CineQueer* heißt alle herzlichst Willkommen.
