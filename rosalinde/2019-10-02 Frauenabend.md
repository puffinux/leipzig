---
id: '1218282921673897'
title: Frauen*abend
start: '2019-10-02 19:00'
end: '2019-10-02 22:00'
locationName: null
address: Demmeringstr. 32
link: 'https://www.facebook.com/events/1218282921673897/'
image: 49411185_2067178703348717_8140747717975474176_o.jpg
teaser: 'Der Frauen*abend bietet Frauen* die Möglichkeit zu regelmäßigen Treffen und gemeinsamen Unternehmungen. Kommt vorbei, immer am 1. Mittwoch im Monat ab'
recurring: null
isCrawled: true
---
Der Frauen*abend bietet Frauen* die Möglichkeit zu regelmäßigen Treffen und gemeinsamen Unternehmungen. Kommt vorbei, immer am 1. Mittwoch im Monat ab 19 Uhr!