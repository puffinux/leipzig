---
id: '3023564657669248'
title: JuLe lesbische & bisexuelle Jugend und ComingOutGruppe(14-27 J.)
start: '2019-05-03 18:00'
end: '2019-05-03 21:00'
locationName: RosaLinde Leipzig e.V.
address: 'Lange Straße 11, 04103 Leipzig'
link: 'https://www.facebook.com/events/3023564657669248/'
image: 49544861_2067201726679748_7871250442980687872_o.jpg
teaser: 'Wir sind eine offene Coming-Out Gruppe für Mädchen und junge Frauen zwischen 14 bis 27 Jahren aus Leipzig und Umgebung, die sich jeden 1. und 3. Freit'
recurring: null
isCrawled: true
---
Wir sind eine offene Coming-Out Gruppe für Mädchen und junge Frauen zwischen 14 bis 27 Jahren aus Leipzig und Umgebung, die sich jeden 1. und 3. Freitag ab 18 Uhr in der RosaLinde trifft.

Bei unseren Treffen versuchen wir eine abwechslungsreiche Mischung aus thematischen Inhalten und Freizeitaktivitäten auszubalancieren. Auf dem Programm stehen Aufklärung, Vorträge und Veranstaltungen genauso wie Spieleabende, gemeinsames Kochen, Backen, Basteln und Sport.
