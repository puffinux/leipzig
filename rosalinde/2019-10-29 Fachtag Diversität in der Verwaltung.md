---
id: "2384378178278136"
title: "Fachtag: Diversität in der Verwaltung"
start: 2019-10-29 09:00
end: 2019-10-29 16:30
locationName: Sächsische Landeszentrale für politische Bildung (SLpB)
address: Schützenhofstraße 36, 01129 Dresden
link: https://www.facebook.com/events/2384378178278136/
image: 69007450_2372465182848809_6359363560413331456_n.jpg
teaser: "// Fachtag: Diversität in der Verwaltung leben –  Herausforderungen als
  Chance //  Zeit: Dienstag, 29.10.2019, 09.00 – 16.30 Uhr Ort: Sächsische
  Lande"
isCrawled: true
---
// Fachtag: Diversität in der Verwaltung leben – 
Herausforderungen als Chance //

Zeit: Dienstag, 29.10.2019, 09.00 – 16.30 Uhr
Ort: Sächsische Landeszentrale für politische Bildung
Schützenhofstraße 36, 01129 Dresden

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
Anmeldung: www.LSNQ.de/diversitaet 
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

Verwaltungen sind von Vielfalt geprägt. Dies ist nicht immer auf den ersten Blick sichtbar, rückt aber zunehmend ins Bewusstsein, wie auch Diversität in anderen Bereichen der Gesellschaft bewusster wahrgenommen und gelebt wird. Dies birgt Herausforderungen, ist aber auch das Potential für die Entwicklung einer Unternehmenskultur, die auf Chancengleichheit basiert und sich an den Fähigkeiten der 
Mitarbeitenden orientiert.

Laut Allgemeinem Gleichbehandlungsgesetz hat jeder Arbeitgeber und jede Arbeitgeberin die Pflicht, Benachteiligungen abzubauen und ihnen präventiv entgegen zu wirken. Ein diskriminierungsfreies Miteinander ist im Interesse der sächsischen Behörden. Ziel des Fachtages 
ist es, Möglichkeiten aufzuzeigen, die ein solches Miteinander fördern und etablieren. Der positive Effekt einer diversitätssensiblen Organisationskultur soll dabei im 
Mittelpunkt stehen.

Trotz rechtlicher Gleichstellung und gewachsenem Bewusstsein für die Lebensrealitäten von Lesben, Schwulen, Bisexuellen, trans*- und inter*geschlechtlichen Menschen (LSBT*I*) sind diese am Arbeitsplatz seitens ihrer Kolleg*-innen und Vorgesetzten noch immer mit Unsicherheiten, Vorurteilen und Ausgrenzungen in Bezug auf ihre geschlechtliche oder sexuelle Identität konfrontiert; 
auch in Ministerien, Landesbehörden und Kommunen.

Mit dem Fachtag soll für die Vielfalt von Menschen unterschiedlicher sexueller Orientierung und geschlechtlichen Empfindens im Arbeitskontext sensibilisiert werden. 

Eingeladen sind Führungskräfte und Bedienstete mit Personalverantwortung, Bedienstete in Landes- und Kommunalverwaltungen, Personalvertretungen, Frauen- und Gleichstellungsbeauftragte, Mitarbeitende von Beschwerdestellen zum Allgemeinen Gleichbehandlungsgesetz (AGG), Ausbildende und Auszubildende.

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
Gesamtmoderation: Kevin Rosenberger 
(Akademie Waldschlösschen)

09.00 Uhr Ankommen bei Kaffee und Tee
09.30 Uhr Begrüßung durch die Veranstalter*innen
10.00 Uhr Impulsvorträge 

„LSBT*I* und Coming Out“ - 
Vera Ohlendorf, LAG Queeres Netzwerk Sachsen

„Rechtliche Lage von LSBT*I* am Arbeitsplatz“ - 
Frank-Peter Wieth, Geschäftsbereich Gleichstellung und Integration des Sächsischen Staatsministeriums für Soziales und Verbraucherschutz

„Out im Office?! Sexuelle und Geschlechtsidentität im Kontext von Arbeit“ - Prof. Dr. Dominic Frohn, IDA - Institut für Diversity- & Antidiskriminierungsforschung

11.20 Uhr Pause
11.30 Uhr	 Diskussion 
12.00 Uhr Mittagspause und Markt der Möglichkeiten
13.00 Uhr Parallele Workshops

1.) Wir freuen uns auf Sie! – Diversität im Bewerbungsprozess

Chancengleichheit für alle steht im Arbeitsleben im engen Kontext mit dem Bewerbungsprozess. Die Sächsische Landesregierung prüft derzeit ein Modellprojekt zum anonymisierten Bewerbungsverfahren in Verwaltungen. Dieses wäre jedoch nur ein Baustein im Komplex der erfolgreichen Personal- und Fachkräftegewinnung. Wie lässt sich der Prozess von der Stellenausschreibung bis zur Integration des/der neuen Kolleg/in ins Team so gestalten, dass auch sexuelle und geschlechtliche Vielfalt Berücksichtigung und Wertschätzung findet? Der Workshop wird auch der Frage nachgehen, welche strukturellen Rahmenbedingungen dafür nötig sind.

Leitung: Kathrin Darlatt (Gleichstellungspolitische Referentin, Stadt Leipzig); Georg Teichert (Gleichstellungsbüro der Universität Leipzig)

2.) Alle dabei?! Eine Organisationskultur ganzheitlich und professionell gestalten

Wie kann eine Organisationskultur so entwickelt werden, dass sie offen ist für geschlechtliche und sexuelle Vielfalt? Im Workshop werden die am Vormittag des Fachtags im Zentrum stehenden Aspekte auf die konkreten Arbeitsbereiche der Teilnehmenden bezogen und konkretisiert. Dabei wird es auch um die Auswirkungen verschiedener Umgangsweisen mit geschlechtlicher und sexueller Vielfalt in Organisationen gehen – sowohl auf der personalen Ebene, als auch auf Team –  sowie Organisationsebene. Abschließend werden praktische Maßnahmen und gute Beispiele besprochen.

Leitung: Prof. Dr. Dominic Frohn (IDA - Institut für Diversity- & Antidiskriminierungsforschung), Sophie Ruby (Geschäftsbereich Gleichstellung und Integration des Sächsischen Staatsministeriums für Soziales und Verbraucherschutz)

3.) Diversität und Teamentwicklung – sexuelle und geschlechtliche Vielfalt als Chance

Vielfalt in Teams bedeutet, dass unterschiedliche Erfahrungen, Lebenskontexte, Kompetenzen und Handlungsweisen aufeinandertreffen. Auch sexuelle und geschlechtliche Vielfalt spielen dabei eine Rolle. Um kreativ und leistungsfähig zu arbeiten, benötigt es eine Teamkultur, in der jede/r Akzeptanz findet. Der Workshop bietet die Möglichkeit, sich mit dem Facettenreichtum der eigenen Teams auseinanderzusetzen, Haltungen zu reflektieren, Gemeinsamkeiten zu er-kennen und Unterschiede schätzen zu lernen. Weiter-hin wird Handwerkszeug vermittelt, um mögliche Ausgrenzungsmechanismen zu erkennen, Vielfalt transparent zu machen und gelingendes Miteinander zu gestalten. 

Leitung: Claudia Kühnel-Kalamorz (PariFID - Paritätische Fach- und Informationsstelle für interkulturelle Öffnung und Diversität), Uwe Tüffers (Aids-Hilfe Dresden e.V.)

4.) Frauen- und Gleichstellungsbeauftragte als Vorbild für LSBT*I*-Beauftragte?

Was können wir aus der Arbeit der Frauen- und Gleichstellungsbeauftragten lernen, wenn es um die Stärkung von LSBT*I* am Arbeitsplatz geht? Welche Verfahren, Abläufe und Zuständigkeiten könnten Vorbilder sein? Im Workshop möchten wir diskutieren, inwiefern Erfahrungen und Erkenntnisse aus der Gleichstellungsarbeit für Themen von geschlechtlicher und sexueller Vielfalt nutzbar gemacht werden können und wo bereits konkrete Anknüpfungspunkte bestehen. Dabei interessiert auch die Frage nach künftigen Zuständigkeiten. Sollten Frauen- und Gleichstellungsbeauftragte diese Aufgaben übernehmen, benötigen wir eigenständige LSBT*I*-Beauftragte oder besser gleich Vielfalts- und Diversity-Beauftragte?

Leitung: Dr. Cornelia Winkler, (Frauenbeauftragte und Referentin im Landesamt für Schule und Bildung, Sachsen), Astrid Tautz (Genderkompetenzzentrum Sachsen)

15:00 Uhr Kaffeepause
15:30 Uhr Ergebnispodium mit Entscheidungsträger*innen 		 aus der Landes- und Kommunalverwaltung
16:15 Uhr Abschluss und Ausblick

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

Die Teilnahme ist kostenfrei. Der Fachtag kann in Absprache mit Ihren Vorgesetzten als Fortbildung anerkannt werden. Für Personalräte gilt er nach § 47 Abs. 2 Satz 1 SächsPersVG ebenfalls als Fortbildung.

Die Anmeldung erfolgt über das Beteiligungs-portal der Sächsischen Staatsregierung. Bitte melden Sie sich über folgenden Link an:  www.LSNQ.de/diversitaet 

Anmeldeschluss: 15.10.2019

Die Teilnehmer*innenanzahl sowie die Plätze in den Workshops sind begrenzt. 

Alle Informationen zum Datenschutz finden Sie im Beteiligungsportal.

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 
Dieser Fachtag wird gemeinsam veranstaltet von:
Geschäftsbereich Gleichstellung und Integration des Sächsischen Staatsministeriums für Soziales und Verbraucherschutz / LAG Queeres Netzwerk Sachsen / RosaLinde Leipzig e.V. / Gerede E V Dresden / Genderkompetenzzentrum Sachsen / AG LSBTI GEW Sachsen
