---
id: '536214210230737'
title: JungS schwule & bisexuelle Jugend und ComingOutGruppe (14-25 J.)
start: '2019-04-27 16:00'
end: '2019-04-27 19:00'
locationName: RosaLinde Leipzig e.V.
address: 'Lange Straße 11, 04103 Leipzig'
link: 'https://www.facebook.com/events/536214210230737/'
image: 50051872_2067194346680486_8973649611629527040_o.jpg
teaser: Wir sind eine Jugend- und Coming-Out-Gruppe für alle schwulen und bisexuellen Jungen im Alter von 14 bis 25 Jahren. Wer neue Leute und Freunde kennenl
recurring: null
isCrawled: true
---
Wir sind eine Jugend- und Coming-Out-Gruppe für alle schwulen und bisexuellen Jungen im Alter von 14 bis 25 Jahren. Wer neue Leute und Freunde kennenlernen und Fragen zu Homo- und Bisexualität oder dem schwulen Leben allgemein hat, ist bei uns genau richtig.

Wir treffen uns jeden 2., 4. und 5. Samstag im Monat ab 16 Uhr in den Räumen des RosaLinde Leipzig e.V.