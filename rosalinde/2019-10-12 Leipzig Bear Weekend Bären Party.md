---
id: '669347270198956'
title: 'Leipzig Bear Weekend: Bären Party'
start: '2019-10-12 20:00'
end: '2019-10-13 03:00'
locationName: SKY CLUB - Leipzig
address: 'Riesaer Straße 56, 04328 Leipzig'
link: 'https://www.facebook.com/events/669347270198956/'
image: 65595231_699518370518500_3887452114531647488_n.jpg
teaser: 'Unsere Bären Party – eine Party für Bären, Ledermänner, Jeansträger und Freunde! Die größte Bären-Party in Mitteldeutschland mit DJ James (München) un'
recurring: null
isCrawled: true
---
Unsere Bären Party – eine Party für Bären, Ledermänner, Jeansträger und Freunde! Die größte Bären-Party in Mitteldeutschland mit DJ James (München) und DJ Alex (Leipzig). Ein tolles „Familentreffen“ mit alten und neuen Freunden aus nah und fern! Lasst uns feiern, tanzen, flirten bis in die Morgenstunden! Wie auch im vergangenen Jahr findet unsere Bärenparty in einem der angesagtesten Clubs in Leipzig statt – dem Sky Club. Die im Leipziger Stadtteil Paunsdorf angesiedelte Eventlocation befindet sich in einer urigen ehemaligen Fabrikhalle und verfügt über mehrere Dancefloors. Wir sind sicher Ihr werdet euch hier sehr wohl fühlen. Für das leibliche Wohl in Form von Flüssig- oder Festnahrung ist natürlich gesorgt!
Der Abend wird heiß, verdammt heiß!
Einlass: 19:30 Uhr