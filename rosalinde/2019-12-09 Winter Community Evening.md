---
id: "565063034064073"
title: Winter Community Evening
start: 2019-12-09 19:00
end: 2019-12-09 22:00
locationName: RosaLinde Leipzig e.V.
address: Demmeringstr. 32, 04177 Leipzig
link: https://www.facebook.com/events/565063034064073/
image: 79383196_588936661934143_500459712567836672_n.jpg
teaser: "english below - español abajo - français ci-dessous  Deutsch: Im Namen von
  trans*inter* nicht-binär* action eine herzliche Einladung zum offenen Treff"
isCrawled: true
---
english below - español abajo - français ci-dessous

Deutsch:
Im Namen von trans*inter* nicht-binär* action eine herzliche Einladung zum offenen Treffen am 9.12. 19 Uhr in der RosaLinde.

Wir wollen einen Blick in die Zukunft werfen und vor allem ein bisschen abhängen und Waffeln naschen ;)
Also kommt gerne auch einfach so rum zum inter* trans* nicht-binär* Community Abend!

english:
In the name of trans*inter* nonbinär* action a warm invitation to the open meeting on 9.12. 19 o'clock in the RosaLinde.

We want to take a look into the future and above all hang out and eat waffles ;)
So come around to the inter* trans* non binary* Community Evening!

español :
En nombre de la acción trans*inter* nonbinär* una cálida invitación a la reunión abierta de las 9.12. 19 horas en el RosaLinde.

Queremos mirar hacia el futuro y, sobre todo, pasar el rato y comer gofres;)
Así que ven a la Noche de Comunidad inter* trans* no binaria*!

français:
Au nom de l'action trans*inter* nonbinär* une invitation chaleureuse à la réunion publique du 9.12.19 heures dans la RosaLinde.

Nous voulons regarder vers l'avenir et surtout traîner et manger des gaufres ;)
Venez donc à la soirée communautaire inter* trans* non binaire* !
