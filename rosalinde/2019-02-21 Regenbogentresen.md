---
id: '718493668531873'
title: Regenbogentresen
start: '2019-02-21 19:00'
end: '2019-02-21 22:00'
locationName: RosaLinde Leipzig e.V.
address: 'Lange Straße 11, 04103 Leipzig'
link: 'https://www.facebook.com/events/718493668531873/'
image: 49688637_2067241066675814_7428521881214386176_o.jpg
teaser: Unsere Gruppe bietet monatlich Gelegenheit zum Kennenlernen und Austausch für junggebliebene lsbti* Menschen. Darüber hinaus werden Informationsverans
recurring: null
isCrawled: true
---
Unsere Gruppe bietet monatlich Gelegenheit zum Kennenlernen und Austausch für junggebliebene lsbti* Menschen. Darüber hinaus werden Informationsveranstaltungen, Gesprächsabende sowie Freizeitaktivitäten organisiert.
Wir treffen uns immer am 3. Donnerstag im Monat ab 19 Uhr.