---
id: '1218282915007231'
title: Frauen*abend
start: '2019-06-05 19:00'
end: '2019-06-05 22:00'
locationName: RosaLinde Leipzig e.V.
address: 'Lange Straße 11, 04103 Leipzig'
link: 'https://www.facebook.com/events/1218282915007231/'
image: 49411185_2067178703348717_8140747717975474176_o.jpg
teaser: 'Der Frauen*abend bietet Frauen* die Möglichkeit zu regelmäßigen Treffen und gemeinsamen Unternehmungen. Kommt vorbei, immer am 1. Mittwoch im Monat ab'
recurring: null
isCrawled: true
---
Der Frauen*abend bietet Frauen* die Möglichkeit zu regelmäßigen Treffen und gemeinsamen Unternehmungen. Kommt vorbei, immer am 1. Mittwoch im Monat ab 19 Uhr!