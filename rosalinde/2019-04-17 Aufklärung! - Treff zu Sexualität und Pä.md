---
id: '199685670901112'
title: Aufklärung! - Treff zu Sexualität und Pädagogik
start: '2019-04-17 19:00'
end: '2019-04-17 22:00'
locationName: RosaLinde Leipzig e.V.
address: 'Lange Straße 11, 04103 Leipzig'
link: 'https://www.facebook.com/events/199685670901112/'
image: 49831817_2067153133351274_2897329004522504192_o.jpg
teaser: Vorstellung Bildungsarbeit des RosaLinde Leipzig e.V.  Im März-Treff wird Steff von der RosaLinde die Bildungsarbeit des Vereins vorstellen. Das Kerna
recurring: null
isCrawled: true
---
Vorstellung Bildungsarbeit des RosaLinde Leipzig e.V.

Im März-Treff wird Steff von der RosaLinde die Bildungsarbeit des Vereins vorstellen. Das Kernangebot besteht aus einem Antidiskriminierungsprojekt, welches vor allem an Schulen Workshops durchführt, um Schüler*innen zu sexuellen Orientierungen und Geschlechtlichkeiten zu informieren und für Diskriminierung zu sensibilisieren. Die Besonderheit des Projekts liegt darin, dass die Workshops von jungen Ehrenamtlichen durchgeführt werden, die selbst über Coming-out-Erfahrungen verfügen und diese am Ende mit den Schüler*innen teilen. Außerdem bietet der Verein Fortbildungen für pädagogische Fachkräfte an, um ihnen Methoden- und Handlungskompetenz für ihren pädagogischen Alltag zu vermitteln. Neu im letzten Jahr hinzugekommen sind die Schul-AGs, in welchen sich Schüler*innen regelmäßig zu Themen rund um sexuelle Orientierungen und Geschlechtlichkeiten austauschen können.

--

Du bist Sozialarbeiter*in, Lehrer*in, Erzieher*in oder arbeitest in einem anderen pädagogischen Beruf?

Du hast Lust, dich mit anderen Menschen zum Thema Aufklärung, Sexualität, Gender, LGBTIAPQ* u.ä. auszutauschen und willst Wege finden, dies in deinem Berufsfeld zu integrieren?

Dann komm zum Treff zu Sexualität und Pädagogik, jeden 3. Mittwoch im Monat ab 19 Uhr! Wir freuen uns auf dich.