---
id: '881556045569768'
title: Dokumentarfilm KIKI zum Idahit* 2019
start: '2019-05-16 17:00'
end: '2019-05-16 19:00'
locationName: die naTo
address: 'Karl-Liebknecht-Straße 46, 04275 Leipzig'
link: 'https://www.facebook.com/events/881556045569768/'
image: 57004853_2210244689042117_2534949187691216896_n.jpg
teaser: 'RosaLinde Leipzig e.V. zeigt  KIKI https://vimeo.com/164329652  USA/Schweden 2016, Dok, 94 min, Regie: Sara Jordenö  Sprache: Englisch Language: Engli'
recurring: null
isCrawled: true
---
RosaLinde Leipzig e.V. zeigt

KIKI
https://vimeo.com/164329652

USA/Schweden 2016, Dok, 94 min, Regie: Sara Jordenö 
Sprache: Englisch
Language: English

Der RosaLinde Leipzig e.V. zeigt KIKI anlässlich des Internationalen Tages gegen Homo-, Inter- und Transfeindlichkeit (IDAHIT*) am 17. Mai 2019. Der Tag erinnert an den 17.05.1990, das Datum an dem Homosexualität aus dem Krankheitskatalog der Weltgesundheitsorganisation (WHO) gestrichen wurde.

Eintritt: 6 Euro (zur Deckung unserer Kosten)
weitere Spenden gehen an das Russian LGBT Network https://lgbtnet.org/en

Am Freitag Soli-Bouldern für die RosaLinde: https://www.facebook.com/events/280303882919570/

+++English version below+++

Junge Queers of Colour finden sich in New York City in der KIKI-Szene zusammen. Beim Entwickeln und Proben von vielschichtigen Performances und Moves finden sie Freundschaften und die Möglichkeit,
eigene Identitäten und Einzigartigkeit zu entwickeln. Ähnlich wie die
Ball-Szene, die im Film "Paris is Burning" gezeigt wird, vermitteln die
Events Lust und Freude. Dabei geht es aber auch immer zentral um den Kampf gegen Rassismus, Sexismus und Trans- und Homo-Feindlichkeit. Es geht ums eigene Überleben, um die Gesundheit, ein Zuhause und das Recht auf die eigene geschlechtliche Identität. KIKI wird zum Ort, an dem sich viele Queers of Colour sicher und unterstützt fühlen.


In New York City, LGBTQ youth-of-color gather out on the Christopher Street Pier, practicing a performance-based artform, Ballroom, which was made famous in the early 1990s by Madonna’s music video “Vogue” and the documentary “Paris Is Burning.” Twenty-five years after these cultural touchstones, a new and very different generation of LGBTQ youth have formed an artistic activist subculture, named the Kiki Scene.
KIKI follows seven characters from the Kiki community over the course of four years, using their preparations and spectacular performances at events known as Kiki balls as a framing device while delving into their battles with homelessness, illness and prejudice as well as their gains towards political influence and the conquering of affirming gender-expressions. In KIKI we meet Twiggy Pucci Garçon, the founder and gatekeeper for the Haus of Pucci, Chi Chi, Gia, Chris, Divo, Symba and Zariya. Each of these remarkable young people represents a unique and powerful personal story, illuminating the Kiki scene in particular, as well as queer life in the U.S. for LGBTQ youth-of-color as a whole.



