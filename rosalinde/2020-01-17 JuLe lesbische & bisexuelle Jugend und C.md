---
id: "583372068874372"
title: JuLe lesbische & bisexuelle Jugend und ComingOutGruppe(14-27 J.)
start: 2020-01-17 18:00
end: 2020-01-17 21:00
locationName: RosaLinde Leipzig e.V.
address: Demmeringstr. 32, 04177 Leipzig
link: https://www.facebook.com/events/583372068874372/
image: 79372433_2658032350930013_579049488006512640_o.jpg
teaser: Wir sind eine offene Coming-Out Gruppe für Mädchen und junge Frauen zwischen
  14 bis 27 Jahren aus Leipzig und Umgebung, die sich jeden 1. und 3. Freit
isCrawled: true
---
Wir sind eine offene Coming-Out Gruppe für Mädchen und junge Frauen zwischen 14 bis 27 Jahren aus Leipzig und Umgebung, die sich jeden 1. und 3. Freitag ab 18 Uhr in der RosaLinde trifft.

Bei unseren Treffen versuchen wir eine abwechslungsreiche Mischung aus thematischen Inhalten und Freizeitaktivitäten auszubalancieren. Auf dem Programm stehen Aufklärung, Vorträge und Veranstaltungen genauso wie Spieleabende, gemeinsames Kochen, Backen, Basteln und Sport.