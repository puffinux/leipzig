---
id: "598307973962558"
title: Queer und Glauben
start: 2019-12-11 19:00
end: 2019-12-11 22:00
address: Demmeringstr. 32
link: https://www.facebook.com/events/598307973962558/
image: 49613421_2067230266676894_7891992197262737408_o.jpg
teaser: Am 2. Mittwoch im Monat ab 19 Uhr treffen wir uns in den Räumen des Rosa Linde
  e.V. Lange Straße 11, 04103 Leipzig.  Wir reden miteinander darüber, wa
isCrawled: true
---
Am 2. Mittwoch im Monat ab 19 Uhr treffen wir uns in den Räumen des Rosa Linde e.V. Lange Straße 11, 04103 Leipzig.

Wir reden miteinander darüber, was wir (nicht) glauben, mit oder ohne religiöse Vorkenntnisse, gleich, in welcher Kirche wir sind oder (nie) waren.
Dazu denkt sich jeweils eine/r einen Einstieg aus. Manchmal singen wir etwas, ein Gebet zum Abschluss hat sich inzwischen etabliert. Es kommt vor, dass wir nur wenige sind, aber die Vorbereitung eines Friedensgebetes zum Leipziger CSD schließt uns immer wieder zusammen.

Ob LSBTI oder nicht, Du bist willkommen! Bring Dich ein oder hör erst einmal einfach nur zu. Gemeinsam sind wir auf dem Weg, im Vertrauen darauf, dass Menschen in Vielfalt geschaffen sind und jedes Wesen geliebt ist – ohne Vorbedingung.
