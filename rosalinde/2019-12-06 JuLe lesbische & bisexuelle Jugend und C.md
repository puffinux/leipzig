---
id: "3023564701002577"
title: JuLe lesbische & bisexuelle Jugend und ComingOutGruppe(14-27 J.)
start: 2019-12-06 18:00
end: 2019-12-06 21:00
address: Demmeringstr. 32
link: https://www.facebook.com/events/3023564701002577/
image: 49544861_2067201726679748_7871250442980687872_o.jpg
teaser: Wir sind eine offene Coming-Out Gruppe für Mädchen und junge Frauen zwischen
  14 bis 27 Jahren aus Leipzig und Umgebung, die sich jeden 1. und 3. Freit
isCrawled: true
---
Wir sind eine offene Coming-Out Gruppe für Mädchen und junge Frauen zwischen 14 bis 27 Jahren aus Leipzig und Umgebung, die sich jeden 1. und 3. Freitag ab 18 Uhr in der RosaLinde trifft.

Bei unseren Treffen versuchen wir eine abwechslungsreiche Mischung aus thematischen Inhalten und Freizeitaktivitäten auszubalancieren. Auf dem Programm stehen Aufklärung, Vorträge und Veranstaltungen genauso wie Spieleabende, gemeinsames Kochen, Backen, Basteln und Sport.
