---
id: '2198579003795089'
title: TransGenderTown & Rosenkränzchen
start: '2019-09-13 19:00'
end: '2019-09-13 22:00'
locationName: RosaLinde Leipzig e.V.
address: 'Lange Straße 11, 04103 Leipzig'
link: 'https://www.facebook.com/events/2198579003795089/'
image: 50003337_2067334239999830_8991576023438983168_o.jpg
teaser: Gehört dein angeborenes Geschlecht (nicht) zu dir oder bist du non-binär?  Für alle diejenigen die schon einmal über diese und ähnliche Fragen nachged
recurring: null
isCrawled: true
---
Gehört dein angeborenes Geschlecht (nicht) zu dir oder bist du non-binär?

Für alle diejenigen die schon einmal über diese und ähnliche Fragen nachgedacht haben, sich vielleicht auch unsicher sind, soll unsere Gruppe die Möglichkeit bieten, diese Gedanken gemeinsam weiter zu verfolgen.

Unser nettes Grüppchen besteht aus Transfrauen, Transmännern, non-binären und intergeschlechtlichen Menschen, Crossdressern, Transvestiten noch Unschlüssigen sowie deren Familien und Partnern. Wir freuen uns über viele neue junge und ältere Interessierte.

Für Fragen oder ein Gespräch unter vier Augen, stehen Euch unsere Ansprechpartner*innen Alexander und Michi gern zur Verfügung.

Wir treffen uns immer am 2. Freitag des Monats ab 19 Uhr in den Räumen der RosaLinde.

NEU: Vor der Gruppe „TransGenderTown – Leipzig“ gibt es nun die Möglichkeit in der Zeit von 18 – 19 Uhr ein Informationsgespräch zu Fragen rund um das Thema Trans* durch Alexander (Leiter der Gruppe TransGenderTown und ausgebildeter Peer-Berater von TIAM e.V.) in Anspruch zu nehmen. Gern kann das Angebot auch genutzt werden, wenn vor Besuch der Gruppe erst einmal ein Vier-Augen-Gespräch gesucht wird. Um Voranmeldung vie E-Mail (a.nass@trans-inter-aktiv.de) wird gebeten.
