---
id: '367620470703945'
title: Ah! Sexuell - Gruppe für asexuelle Menschen
start: '2019-05-22 19:00'
end: '2019-05-22 22:00'
locationName: RosaLinde Leipzig e.V.
address: 'Lange Straße 11, 04103 Leipzig'
link: 'https://www.facebook.com/events/367620470703945/'
image: 49947291_2067148006685120_3887219808340541440_o.jpg
teaser: 'Wenn es um das Thema Sexualität geht, erlebst du häufiger solche Ah(a)-Momente, weil dir selbst das Ganze eher fremd erscheint?  Du bezeichnest dich s'
recurring: null
isCrawled: true
---
Wenn es um das Thema Sexualität geht, erlebst du häufiger solche Ah(a)-Momente, weil dir selbst das Ganze eher fremd erscheint?

Du bezeichnest dich selbst als a-, demi-, grey-sexuell oder kannst/willst dich da nicht auf einen Begriff festlegen?

Vielleicht bist du a-, demi-, grey-romantisch, kannst mit dem Romantik-Begriff nichts anfangen?

Du bist dir vielleicht einfach unsicher, wie und ob du dich selbst durch diese Bezeichnungen definieren sollst, aber merkst, dass du in Bezug auf Sexualität nicht wie die anderen tickst?

Du findest dieses Schubladendenken zwar nervig, würdest dich aber trotzdem eher in diesem Spektrum verorten?

Du ärgerst dich, dass du gleich als hetero-sexuell/-romantisch abgestempelt wirst, wenn du weder als homo-, noch bi-sexuell/-romantisch zu erkennen bist und suchst einen Schutzraum, in dem du mit deiner Asexualität sichtbar wirst?

Dann komm vorbei! Wir treffen uns das erste Mal am 29.03. ab 19 Uhr und ab da jeden 4. Mittwoch im Monat um 19 Uhr in der RosaLinde!

Die Gruppe ist ganz neu und kann durch jede*n mitgestaltet werden.
