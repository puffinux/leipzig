---
id: "627113497826540"
title: Enby-Gruppe - Treffen für Menschen, die sich nonbinär verorten
start: 2020-02-28 19:00
end: 2020-02-28 20:00
locationName: RosaLinde Leipzig e.V.
address: Demmeringstr. 32, 04177 Leipzig
link: https://www.facebook.com/events/627113497826540/
image: 78931200_2658056707594244_6637493298766282752_o.jpg
teaser: Du fragst dich oder bist dir sicher nicht binär zu sein – komm gern dazu.
  Fragst du dich nur, was es denn damit nun schon wieder auf sich hat, informi
isCrawled: true
---
Du fragst dich oder bist dir sicher nicht binär zu sein – komm gern dazu. Fragst du dich nur, was es denn damit nun schon wieder auf sich hat, informier dich bitte erstmal woanders.

Wir treffen uns mit unterschiedlichen Vorstellungen und Erfahrungen, Ausgangssituationen und Einstellungen – wir wollen aber respektvoll miteinander umgehen und uns erst mal zuhören. Verallgemeinerungen und Feindlichkeiten bekommen hier keinen Raum.

Jeden 4. Dienstag im Monat, 19-20 Uhr.

Ausführliche Informationen finden sich hier:

Die Enby-Gruppe möchte einmal im Monat einen Schutzraum für Personen, die sich nicht binär/non binary, agender, genderfluid, genderqueer, no gender, neutrois als demigirl oder demiboy verorten, schaffen. Sie versteht sich als Selbsthilfegruppe und Raum für Informationsaustausch.

Nicht-binär/Nichtbinarität wird (gelegentlich auch unter dem Oberbegriff trans*) für ein breites Spektrum geschlechtlicher (nicht) Identifikationen verstanden und die hier aufgezählten Identitäten sind nicht erschöpft und können erweitert werden – grenzen sich aber von (cis) binärer Verortung (Mann und Frau) ab. Verschiedene Formen (paralleler) Weiblichkeit, Männlichkeit schließen sich nicht aus. Nicht binär wird häufig mit ‚zwischen/außerhalb den (binären) Geschlechtern‘ beschrieben.

Was wir nicht bieten (wollen), ist ein Raum in dem Informationen ‚über‘ Nichtbinarität eingeholt werden können, von Personen die sich diesem Spektrum nicht nah fühlen. (Fragen können an die Gruppe gestellt werden, der Raum soll aber vertraulich sein). Personen, die sich nicht sicher sind in ihrer Identität, sich ausprobieren möchten, den Austausch zur Orientierung suchen, sind herzlich willkommen.

Wir verstehen uns als Gruppe die sich gegen Diskriminierungen jeder Art stellt. Es geht uns auch nicht um gezielte Verurteilung von dya-cis heteronormen Personen(gruppen), denn unser kritisches Verständnis wendet sich den Strukturen zu.

Es ist uns wichtig, dass wir sensibel miteinander umgehen und Verständnis füreinander aufbringen, auch wenn es zu unterschiedlichen Aufassungen kommt, wenn unterschiedliches Erfahren wird und wenn nicht alle auf gleichem Kenntnisstand sind und/oder mit sich und ihrer Identität (noch) nicht sicher.

In der gemeinsamen Zeit können nicht binär spezifische Themen (Outing, Passing/Lesbarkeit, Pronomina, sensible Sprache, Sensibilisierung und Erwartungen an die Umgebung, …) besprochen werden, wir können (nicht binäre) Filme und Kunst ansehen und aus unserer Perspektive reflektieren, uns über Literatur, Blogs und Diskurse austauschen, Referent_innen einladen, etwas unternehmen, … .