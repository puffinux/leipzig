---
id: "1367257950119004"
title: Bi/Pan Gruppe – Treff für bi- und pansexuelle Menschen
start: 2020-02-13 19:00
end: 2020-02-13 22:00
locationName: RosaLinde Leipzig e.V.
address: Demmeringstr. 32, 04177 Leipzig
link: https://www.facebook.com/events/1367257950119004/
image: 79209434_2658006957599219_5397607489623556096_o.jpg
teaser: Hast du Interesse, dich zum Thema Bi_Pansexualität auszutauschen und/oder
  Lust, dich einzubringen? Wir auch! Deshalb wollen wir uns einen Bi_Pan-Raum
isCrawled: true
---
Hast du Interesse, dich zum Thema Bi_Pansexualität auszutauschen und/oder Lust, dich einzubringen? Wir auch! Deshalb wollen wir uns einen Bi_Pan-Raum schaffen. Lasst uns gemeinsam überlegen, was wir wollen und was so geht. Wir treffen uns dazu immer am zweiten Donnerstag im Monat in der RosaLinde.

Wir freuen uns aufs (Er)Finden!