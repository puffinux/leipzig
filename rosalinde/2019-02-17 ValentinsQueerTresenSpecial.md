---
id: '346101722785827'
title: ValentinsQueerTresenSpecial
start: '2019-02-17 15:00'
end: '2019-02-17 18:00'
locationName: RosaLinde Leipzig e.V.
address: 'Lange Straße 11, 04103 Leipzig'
link: 'https://www.facebook.com/events/346101722785827/'
image: 50183042_2076235402443047_1415103758806810624_n.jpg
teaser: Liebe Liebende ♥ Willkommen zu unserem Valentinstags-Special! In gemütlich kuscheliger Atmosphäre möchten wir mit euch zusammen feiern. Neben kitschig
recurring: null
isCrawled: true
---
Liebe Liebende ♥
Willkommen zu unserem Valentinstags-Special! In gemütlich kuscheliger Atmosphäre möchten wir mit euch zusammen feiern.
Neben kitschiger Deko, netten Getränken und viel Wohlfühlraum wird es diesmal ein besonderes Highlight geben: Das Speed-Dating! 
Hierbei seid ihr gefragt! Erzählt und redet was das Zeug hält und sollte es euch schon von Anfang an die Sprache verschlagen haben, keine Sorge: Wir haben ein Frage-Antwort-Spiel für euch vorbereitet!
Also zögert nicht, schnappt eure*n liebste*n Menschen und kommt in Scharen!
Wir freuen uns auf euch!

------------------------------
Dears lovers 
Welcome to our valentine's special! In a very cozy and cuddling atmosphere,  we want to celebrate with you. 

Besides cheesy decorations, nice drinks and a lot of save space,  there will be one highlight: Speed-Dating! 

Therefore we need you! Keep talking as much as you can and if your breath was taken away right from the beginning, don't worry: we have a quick question game prepared for you! 

So don't hesitate,  take your beloved ones and come over! 

We're looking forward to seeing you!
