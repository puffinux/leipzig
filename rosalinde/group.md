---
name: RosaLinde Leipzig e.V.
website: https://www.rosalinde-leipzig.de
email: kontakt@rosalinde-leipzig.de
scrape:
  source: facebook
  options:
    page_id: 146296738770266
  filter:
    title: "^((?!Queer-Tresen).)*$"
---
Wir sind ein Verein für queere Begegnung, Bildung und Beratung. Unsere Angebote und Aktivitäten richten sich vor allem an lesbische, schwule, bisexuelle, trans*- und intergeschlechtliche, sowie queere und asexuelle/ aromantische Personen (kurz: LSBTIAQ*). Darüber hinaus beraten wir auch Familien, Angehörige sowie Multiplikator*innen.
