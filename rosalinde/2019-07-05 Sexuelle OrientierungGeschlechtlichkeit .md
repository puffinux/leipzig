---
id: '688038954988586'
title: Sexuelle Orientierung/Geschlechtlichkeit als Thema in der Schule
start: '2019-07-05 09:30'
end: '2019-07-05 16:30'
locationName: null
address: 'Dittrichring 5-7, Raum 405'
link: 'https://www.facebook.com/events/688038954988586/'
image: 64319744_2313200938746491_7827465257147695104_o.jpg
teaser: 'Sexuelle Orientierung und Geschlechtlichkeit als Thema in der Schule  Fr, 05.07.19, 9.30-16.30 Uhr Dittrichring 5-7, Raum 405  „Schwul“ ist eines der'
recurring: null
isCrawled: true
---
Sexuelle Orientierung und Geschlechtlichkeit als Thema in der Schule

Fr, 05.07.19, 9.30-16.30 Uhr
Dittrichring 5-7, Raum 405

„Schwul“ ist eines der beliebtesten Schimpfwörter auf deutschen Schulhöfen, um andere abzuwerten – unabhängig von ihrer tatsächlichen sexuellen Orientierung. Entsprechend warten viele Jugendliche mit ihrem Coming-out bis die Schulzeit vorbei ist. Lehrkräfte sind häufig überfordert im Umgang mit homophoben Beleidigungen.

Im Workshop soll es darum gehen, eine grundlegende Sensibilität für die Lebenssituationen von lesbischen, schwulen, bi- und asexuellen sowie trans- und intergeschlechtlichen Menschen zu erwerben, Begriffe zu klären, Methoden kennenzulernen und Fallbeispiele zu besprechen. 

Zielgruppe: Lehramtsstudierende, Referendar*innen und Lehrkräfte. Anmeldung mit Nennung der aktuellen Tätigkeit via anmeldung@rosalinde-leipzig.de.

Stefanie Krüger arbeitet seit 2013 im RosaLinde Leipzig e.V. und ist dort für die Bildungsarbeit zuständig.


Der Workshop wird veranstaltet in Zusammenarbeit mit dem Projekt „Vielfalt Lehren!“ im Rahmen des Modellprojekts „Akzeptanz für Vielfalt - gegen Homo-, Trans*- und Inter*feindlichkeit“ im Bundesprogramm „Demokratie Leben“ des Bundesministerium für Familie, Senioren, Frauen und Jugend." 

--

Empowerment-Workshop für LSBTIAQ-Lehrkräfte und Studierende: https://www.facebook.com/events/832815313772061/