---
id: '2280370565576073'
title: Idahit* Infostand und Kurzfilmabend in Döbeln
start: '2019-05-15 14:30'
end: '2019-05-15 19:00'
locationName: null
address: 'Niedermarkt, Breite Straße 5 Döbeln'
link: 'https://www.facebook.com/events/2280370565576073/'
image: 56656084_2146811895414140_7538086733158547456_n.jpg
teaser: 'Im Rahmen des Internationalen Tag gegen Homo-, Bi-, Trans*- und Inter*feindlichkeit (IDAHIT*) schaffen wir mit einer Aktionswoche Sichtbarkeit von Ges'
recurring: null
isCrawled: true
---
Im Rahmen des Internationalen Tag gegen Homo-, Bi-, Trans*- und Inter*feindlichkeit (IDAHIT*) schaffen wir mit einer Aktionswoche Sichtbarkeit von Geschlechtervielfalt.

15.05. Döbeln

16.30 – 18.00 Uhr // Niedermarkt, Breite Straße 5
Infostand mit Redebeiträgen: Wir zeigen öffentlich Präsenz und setzen ein Zeichen gegen Homo-, Bi-, Trans*- und Inter*feindlichkeit. Seid dabei!

19.00 Uhr // Café Courage, Bahnhofstraße 56
Kurzfilmabend und Diskussion: 
* Neko No Hi - Cat Days (Jon Frickey, BRD 2018, 11 min)
* Nasser (Melissa Martens, NL 2015, 19 min)
* Dawn (Jake Graf, UK 2016, 14 min)
* Mrs McCutcheon (John Sheedy, Australien 2017, 17 min)
Alle Filme laufen mit deutschen Untertiteln. Der Eintritt ist frei.

+ + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + 

Jährlich bietet der Tag gegen Homo-, Bi-, Trans*- und Inter*feindlichkeit (IDAHIT*) einen Anlass zur Erinnerung daran, dass Homosexualität erst am 17.05.1990 aus dem Krankheitskatalog der Weltgesundheitsorganisation gestrichen wurde.

Wir wollen gemeinsam mit Euch für gleiche Rechte, gesellschaftlichen Zusammenhalt und gegenseitigen Respekt von Lesben, Schwulen, Bisexuellen, trans*-, inter*geschlechtlichen und queeren Menschen sowie Asexuellen/Aromantischen (LSBTIQA*) streiten und jeder Ideologie der Ungleichwertigkeit entgegentreten.

Trans*- und Inter*geschlechtlichkeit gelten auch 2019 noch als Geschlechtsdifferenzierungs- bzw. Geschlechtsidentitätsstörungen. In Bezug auf gleichgeschlechtliche Lebensweisen ist ebenfalls noch keine volle Gleichstellung erreicht. Nicht zuletzt der aktuelle rechtskonservative Rollback und sog. „besorgte Eltern“ erfordern ein aktives Zeichen gegen Homo-, Bi-, Trans*- und Inter*feindlichkeit in Döbeln, Sachsen und überall.
Selbstbestimmt, offen und diskriminierungsfrei – eine demokratische Gesellschaft muss es allen Menschen ermöglichen, jederzeit und an jedem Ort ohne Angst verschieden sein zu können. Gewalt und Vorurteile gegenüber Lesben, Schwulen, Bisexuellen, trans*-, inter*geschlechtlichen und queeren Menschen sowie Asexuellen/Aromantischen haben keinen Platz in Sachsen.

LSBTIQA*-Rechte sind Menschenrechte!

+ + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + 

In Kooperation mit: RosaLinde Leipzig e.V., Queer durch Sachsen, Treibhaus e.V., Filmfest Dresden

Diese Veranstaltung wird gefördert vom BMFSFJ im Rahmen des Modellprojekts "Demokratie Leben!" sowie dem Freistaat Sachsen und dem Aktionsfonds des Landkreises Mittelsachsen.

+ + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + 

Die Aktionswoche:
13.05. Bautzen
14.05. Oschatz
15.05. Döbeln
16.05. Hoyerswerda
17.05. Wurzen
18.05. Görlitz
19.05. Zwickau