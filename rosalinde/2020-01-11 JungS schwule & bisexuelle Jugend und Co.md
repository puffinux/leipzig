---
id: "3161026227245587"
title: JungS schwule & bisexuelle Jugend und ComingOutGruppe (14-25 J.)
start: 2020-01-11 16:00
end: 2020-01-11 19:00
locationName: RosaLinde Leipzig e.V.
address: Demmeringstr. 32, 04177 Leipzig
link: https://www.facebook.com/events/3161026227245587/
image: 78497424_2658017407598174_5837355438575714304_o.jpg
teaser: Wir sind eine Jugend- und Coming-Out-Gruppe für alle schwulen und bisexuellen
  Jungen im Alter von 14 bis 25 Jahren. Wer neue Leute und Freunde kennenl
isCrawled: true
---
Wir sind eine Jugend- und Coming-Out-Gruppe für alle schwulen und bisexuellen Jungen im Alter von 14 bis 25 Jahren. Wer neue Leute und Freunde kennenlernen und Fragen zu Homo- und Bisexualität oder dem schwulen Leben allgemein hat, ist bei uns genau richtig.

Wir treffen uns jeden 2., 4. und 5. Samstag im Monat ab 16 Uhr in den Räumen des RosaLinde Leipzig e.V.