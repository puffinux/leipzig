---
id: '243524086548134'
title: Trans* inter* Jugendgruppe
start: '2019-07-01 19:00'
end: '2019-07-01 22:00'
locationName: RosaLinde Leipzig e.V.
address: 'Lange Straße 11, 04103 Leipzig'
link: 'https://www.facebook.com/events/243524086548134/'
image: 50005865_2067326663333921_4302395390398824448_o.jpg
teaser: Du bist trans* und oder inter* und willst Teil einer Community werden? Wir sind eine Gruppe für junge trans* und inter* Personen im Alter von 14-25 Ja
recurring: null
isCrawled: true
---
Du bist trans* und oder inter* und willst Teil einer Community werden?
Wir sind eine Gruppe für junge trans* und inter* Personen im Alter von
14-25 Jahren, egal ob Mann, Frau, nicht binär, inter oder agender: ihr
alle seid bei uns willkommen. 

Wir treffen uns monatlich am 1. Montag um 19 Uhr und jeden 3. Samstag um 15 Uhr in den Räumen der RosaLinde (rechte Seite).

Seid dabei und entwickelt mit uns die Dynamik der Gruppe, gestaltet mit und bringt eure Ideen ein (oder kommt einfach und habt einen gemütlichen Nachmittag/Abend). Wir freuen uns auf euch!
