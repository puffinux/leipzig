---
id: '572050239941393'
title: AK Politik
start: '2019-03-12 19:00'
end: '2019-03-12 22:00'
locationName: RosaLinde Leipzig e.V.
address: 'Lange Straße 11, 04103 Leipzig'
link: 'https://www.facebook.com/events/572050239941393/'
image: 52407050_2119967431403177_3004581029462671360_o.jpg
teaser: Du hast Bock auf queer-politisches Engagement? Du hast Lust die Situation für LSBTIQ* in Leipzig zu verbessern? Du willst nicht tatenlos auf die sächs
recurring: null
isCrawled: true
---
Du hast Bock auf queer-politisches Engagement? Du hast Lust die Situation für LSBTIQ* in Leipzig zu verbessern? Du willst nicht tatenlos auf die sächsischen Landtagswahlen 2019 warten? Du möchtest queere Themen in die Öffentlichkeit tragen und mit uns sichtbar machen? 

Na dann haben wir was für dich: 
Arbeitskreis Politik
jeden 2. Dienstag im Monat um 19 Uhr 
Ort: RosaLinde Leipzig (linke Seite).

Komm vorbei und lass uns was tun!