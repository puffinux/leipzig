---
id: '222971348591750'
title: Queer und Gesundheit
start: '2019-04-04 19:00'
end: '2019-04-04 22:00'
locationName: RosaLinde Leipzig e.V.
address: 'Lange Straße 11, 04103 Leipzig'
link: 'https://www.facebook.com/events/222971348591750/'
image: 49734975_2067221490011105_7532805156040081408_o.jpg
teaser: 'Du bist Hebamme/Entbindungspfleger, Psycholog*in, Ärzt*in oder Krankenschwester/Krankenpfleger, Physiotherapeut*in oder arbeitest in einem anderen Ges'
recurring: null
isCrawled: true
---
Du bist Hebamme/Entbindungspfleger, Psycholog*in, Ärzt*in oder Krankenschwester/Krankenpfleger, Physiotherapeut*in oder arbeitest in einem anderen Gesundheitsberuf und bist queer* bzw. LGBTI*? Dann komm doch zum Stammtisch queere Menschen in Gesundheitsberufen, immer am 1. Donnerstag im Monat ab 19 Uhr! Das Treffen findet in der rechten Linde-Hälfte statt.