---
name: dezentrale Hackspace Leipzig
website: https://dezentrale.space/
email: vorstand@dezentrale.space
address: Dreilindenstraße 19, 04177 Leipzig
scrape:
    source: dezentrale
---
Die Dezentrale ist ein gemeinschaftlich verwalteter Ort,
an dem technologisch interessierte Menschen zusammen kommen können um voneinander zu lernen und an Projekten zu basteln. 