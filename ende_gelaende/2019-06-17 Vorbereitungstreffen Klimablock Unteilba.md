---
id: '756675614734310'
title: Vorbereitungstreffen Klimablock "Unteilbardemo" 6.7
start: '2019-06-17 15:30'
end: '2019-06-17 18:30'
locationName: null
address: 'Ziegenledersaal, Uni Leipzig'
link: 'https://www.facebook.com/events/756675614734310/'
image: 62199942_449086225904439_3683042865589518336_n.jpg
teaser: 'Wer Lust hat beteiligt sich gemeinsam mit uns und anderen Klimagruppen an der Vorbereitung der Demo- was fest steht, wir kommen am 6.7. alle in schöne'
recurring: null
isCrawled: true
---
Wer Lust hat beteiligt sich gemeinsam mit uns und anderen Klimagruppen an der Vorbereitung der Demo- was fest steht, wir kommen am 6.7. alle in schönem Grün zur Demo!