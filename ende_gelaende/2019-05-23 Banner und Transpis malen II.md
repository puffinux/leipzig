---
id: '1193211084185351'
title: Banner und Transpis malen II
start: '2019-05-23 10:45'
end: '2019-05-23 16:45'
locationName: UNIVERSITÄT LEIPZIG
address: 'Augustusplatz 10, 04109 Leipzig'
link: 'https://www.facebook.com/events/1193211084185351/'
image: 60447178_435640217249040_8328554835259424768_n.jpg
teaser: 'Für alle die Lust haben uns näher kennenzulernen oder mitzumachen:   Ab 10:45 findest du uns im Innenhof des Hauptcampus am Augustusplatz. Wir werden'
recurring: null
isCrawled: true
---
Für alle die Lust haben uns näher kennenzulernen oder mitzumachen:  
Ab 10:45 findest du uns im Innenhof des Hauptcampus am Augustusplatz. Wir werden Banner und Transpis für die kommenden Aktionen malen.
Im Hintergrund gibt es wie immer musikalische Beschallung und Kaffee gegen Spende vom Couchcafé. 
Kommt vorbei, wir freuen uns =)