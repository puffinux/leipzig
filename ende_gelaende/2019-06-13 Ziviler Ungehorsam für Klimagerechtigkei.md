---
id: '365848260681157'
title: Ziviler Ungehorsam für Klimagerechtigkeit. Hintergrund & Praxis
start: '2019-06-13 15:00'
end: '2019-06-13 17:00'
locationName: null
address: Campus Augustusplatz / S 202
link: 'https://www.facebook.com/events/365848260681157/'
image: 55549698_907507409419459_9108965120385482752_o.jpg
teaser: 'Do, 13.06., 15 – 17 Uhr:  Ziviler Ungehorsam für Klimagerechtigkeit. Hintergrund und Praxis  Ende Gelände Leipzig / Campus Augustusplatz / S 202  Ende'
recurring: null
isCrawled: true
---
Do, 13.06., 15 – 17 Uhr:

Ziviler Ungehorsam für Klimagerechtigkeit. Hintergrund und Praxis

Ende Gelände Leipzig / Campus Augustusplatz / S 202

Ende Gelände praktiziert zivilen Ungehorsam erfolgreich seit mehreren Jahren. Welche Schwierigkeiten treten dabei rechtlich und praktisch auf? Wie lässt sich diesen begegnen und wie können Menschen sich gegenseitig solidarisch unterstützen und eine gute Kommunikation untereinander gelingen? Während des ganzen Workshops gibt es Raum für Fragen, Austausch und Experimente.
