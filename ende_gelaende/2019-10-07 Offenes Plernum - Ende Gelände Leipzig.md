---
id: '2402537013356827'
title: Offenes Plernum - Ende Gelände Leipzig
start: '2019-10-07 19:00'
end: '2019-10-07 22:30'
locationName: Interim
address: 'Demmeringstraße 32, 04177 Leipzig'
link: 'https://www.facebook.com/events/2402537013356827/'
image: 70218027_519789602167434_1443644881640620032_n.jpg
teaser: 'Liebe Klimaaktivist*innen,  unser zweites offenes Plenum im Herbst findet am 07.Oktober um 19 Uhr im Interim statt!  Für alle Menschen, die noch nicht'
recurring: null
isCrawled: true
---
Liebe Klimaaktivist*innen,

unser zweites offenes Plenum im Herbst findet am 07.Oktober um 19 Uhr im Interim statt! 
Für alle Menschen, die noch nicht bei einem unserer Plena dabei waren und Lust bekommen haben sich bei Ende Gelände Leipzig zu engagieren: Kommt gern vorbei und lernt uns kennen! Es gibt viele Möglichkeiten sich einzubringen!

Wir freuen uns auf euch! 