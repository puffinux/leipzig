---
id: '619043808617942'
title: Soli-Bar für die Massenaktion
start: '2019-06-13 21:00'
end: '2019-06-14 00:00'
locationName: Kollektivhaus Wurze GmbH
address: '2  Wurzner Straße, 04315 Leipzig'
link: 'https://www.facebook.com/events/619043808617942/'
image: 62241073_449091142570614_5696968114609061888_n.jpg
teaser: Unterstützt doch solidarisch unsere Massenaktion mit feinen Getränken!
recurring: null
isCrawled: true
---
Unterstützt doch solidarisch unsere Massenaktion mit feinen Getränken!