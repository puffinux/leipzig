---
id: "454469708536357"
title: "PCS: Klimagerechtigkeit und globale Ungleichheiten"
start: 2019-11-27 17:15
end: 2019-11-27 19:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/454469708536357/
image: 78260936_442080746727408_8045987128674353152_n.jpg
teaser: "Titel: Klimagerechtigkeit und globale Ungleichheiten Referent*in: Ende
  Gelände Leipzig Ort: NSG S212, Hauptcampus Zeit: 17.15-19.45 Uhr  Der
  Klimawand"
isCrawled: true
---
Titel: Klimagerechtigkeit und globale Ungleichheiten
Referent*in: Ende Gelände Leipzig
Ort: NSG S212, Hauptcampus
Zeit: 17.15-19.45 Uhr

Der Klimawandel ist heute schon weltweit spürbar und könnte bei gegenwärtiger Entwicklung zukünftig katastrophale Ausmaße annehmen. Doch trifft diese Entwicklung nicht alle Menschen und Regionen gleichschnell und gleichstark. Die daraus resultierenden Ungerechtigkeiten, globalen Machtverhältnisse und Lösungsperspektiven unter dem Stichwort Klimagerechtigkeit wollen wir im Workshop erarbeiten.

Diese Veranstaltung ist teil der Public Climate School. 
Das vollständige Programm findet ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 