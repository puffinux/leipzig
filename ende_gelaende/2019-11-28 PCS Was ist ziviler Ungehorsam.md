---
id: "638120959927409"
title: "PCS: Was ist ziviler Ungehorsam?"
start: 2019-11-28 11:15
end: 2019-11-28 12:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/638120959927409/
image: 74794792_443873623214787_3459768964937154560_n.jpg
teaser: "Titel: Was ist ziviler Ungehorsam? Referent*innen: Ende Gelände Leipzig Raum:
  NSG S204, Hauptcampus Uhrzeit: 11:15- 12:45 Uhr  In diesem Workshop woll"
isCrawled: true
---
Titel: Was ist ziviler Ungehorsam?
Referent*innen: Ende Gelände Leipzig
Raum: NSG S204, Hauptcampus
Uhrzeit: 11:15- 12:45 Uhr

In diesem Workshop wollen wir uns gemeinsam dem Konzept des Zivilen Ungehorsams nähern. Dabei schauen wir uns neben der historischen Entstehung, Widerstandsformen in der Klimagerechtigkeitsbewegung an. Ziel des Workshops ist es, einen Einblick in Zivilen Ungehorsam zu bekommen und alle Interessierten zu ermutigen.

Diese Veranstalung ist Teil der Public Climate School. Das vollständige Programm findet ihr bei studentsforfuture.info
Wir freuen uns auf euch!