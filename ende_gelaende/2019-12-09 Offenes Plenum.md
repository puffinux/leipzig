---
id: "532645510646579"
title: Offenes Plenum
start: 2019-12-09 18:30
end: 2019-12-09 21:30
address: Eisenbahnstarße 127
link: https://www.facebook.com/events/532645510646579/
image: 74456394_550979532381774_8057113112605622272_n.jpg
teaser: Liebe Klimaaktivist*innen,  unser nächstes offenes Plenum findet am 9.12. um
  18:30 Uhr im in der Eisnbahnstraße 127 statt!  Für alle Menschen, die noc
isCrawled: true
---
Liebe Klimaaktivist*innen,

unser nächstes offenes Plenum findet am 9.12. um 18:30 Uhr im in der Eisnbahnstraße 127 statt! 
Für alle Menschen, die noch nicht bei einem unserer Plena dabei waren und Lust bekommen haben sich bei Ende Gelände Leipzig zu engagieren: Kommt gern vorbei und lernt uns kennen! Es gibt viele Möglichkeiten sich einzubringen!

Wir freuen uns auf euch! 