---
id: '417942252288625'
title: Offenes Plenum von Ende Gelände
start: '2019-04-15 19:00'
end: '2019-04-15 22:00'
locationName: Interim
address: 'Demmeringstraße 32, 04177 Leipzig'
link: 'https://www.facebook.com/events/417942252288625/'
image: 55611061_908637859306414_3466837450314219520_o.jpg
teaser: Wir sind Teil des bundesweiten Ende Gelände Bündnisses und fordern mit Massenaktionen des zivilen Ungehorsams Klimagerechtigkeit und den sofortigen Ko
recurring: null
isCrawled: true
---
Wir sind Teil des bundesweiten Ende Gelände Bündnisses und fordern mit Massenaktionen des zivilen Ungehorsams Klimagerechtigkeit und den sofortigen Kohleausstieg! Als relativ neue Ortsgruppe wollen wir uns regional stärker vernetzen und vermehrt Aktionen in Leipzig und Umgebung planen - Du willst selbst aktiv werden? - Dich mit Klimagerechtigkeit beschäftigen, Demos organisieren oder Bagger blockieren? Dann komm vorbei!