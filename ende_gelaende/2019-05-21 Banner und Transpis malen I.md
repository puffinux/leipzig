---
id: '422970461593777'
title: Banner und Transpis malen I
start: '2019-05-21 10:45'
end: '2019-05-21 16:45'
locationName: UNIVERSITÄT LEIPZIG
address: 'Augustusplatz 10, 04109 Leipzig'
link: 'https://www.facebook.com/events/422970461593777/'
image: 60609946_435633903916338_3743273635817717760_n.jpg
teaser: 'Für alle die Lust haben uns näher kennenzulernen oder mitzumachen:    Wir werden Banner und Transpis für die kommenden Aktionen malen. Ab 10:45 findes'
recurring: null
isCrawled: true
---
Für alle die Lust haben uns näher kennenzulernen oder mitzumachen:  

Wir werden Banner und Transpis für die kommenden Aktionen malen. Ab 10:45 findest du uns im Innenhof des Hauptcampus am Augustusplatz. 
Im Hintergrund gibt es wie immer musikalische Beschallung und Kaffee gegen Spende vom Couchcafé. 
Kommt vorbei, wir freuen uns =)