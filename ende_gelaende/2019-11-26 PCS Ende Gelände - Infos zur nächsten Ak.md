---
id: "601889650566954"
title: "PCS: Ende Gelände - Infos zur nächsten Aktion"
start: 2019-11-26 15:15
end: 2019-11-26 16:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/601889650566954/
image: 76775180_444034313198718_5172758143427411968_n.jpg
teaser: "Titel: Ende Gelände - Infos zur nächsten Aktion Referent*in: Ende Gelände
  Raum: HS 8 - Pua Lay Peng (Malaysia) Uhrzeit: 15:15 - 16:45 Uhr  Vom 29.11."
isCrawled: true
---
Titel: Ende Gelände - Infos zur nächsten Aktion
Referent*in: Ende Gelände
Raum: HS 8 - Pua Lay Peng (Malaysia)
Uhrzeit: 15:15 - 16:45 Uhr

Vom 29.11. bis 1.12. findet die nächste Massenaktion von Ende Gelände statt. Leipzig wird als Startpunkt eine wichtige Rolle spielen. Für weitere Infos rund um die Aktion kommt vorbei!

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findest du auf studentsforfuture.info/leipzig
 
Wir freuen uns auf euch!
