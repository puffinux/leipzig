---
id: "751027735400482"
title: Aktionstraining „Fließen & Blockieren“ & Bezugsgruppenfindung
start: 2019-11-23 11:00
end: 2019-11-23 18:00
address: Meuterei, Zollschuppenstr. 1
link: https://www.facebook.com/events/751027735400482/
image: 74790625_554164418729952_8875500117038202880_n.jpg
teaser: Du möchtest an der Massenaktion vom 29.11. - 01-12. teilnehmen, hast so etwas
  aber noch nie gemacht? Dann ist unser Aktionstraining „Fließen & Blockie
isCrawled: true
---
Du möchtest an der Massenaktion vom 29.11. - 01-12. teilnehmen, hast so etwas aber noch nie gemacht? Dann ist unser Aktionstraining „Fließen & Blockieren“ genau das Richtige für dich! Hier bekommst du alle Infos, die du unbedingt für die Vorbereitung brauchst! Offen für alle Menschis! 

Wo? Meuterei, Zollschuppenstr. 1
Wann? 11.00 - 18.00 Uhr
Dauer: 7 Stunden (mit Pausen!).
Achtung! Bitte seid pünktlich und nehmt euch für die gesamte Dauer des Trainings Zeit! Ein späteres Dazukommen oder früheres Gehen ist nicht sinnvoll!

Das moderierte Training soll dazu dienen, Erfahrungen auszutauschen, individuelle und kollektive Handlungsfähigkeit durch Übungen zu erhöhen, Befürchtungen abzubauen und Neues kennen zu lernen. Grundlegende Inhalte des Trainings sind:
Überlegungen zu Zivilem Ungehorsam; Motivationen und Ziele; Ängste und Befürchtungen; Bezugsgruppen als Basis von Aktionen; Entscheidungsfindung in Bezugsgruppen und zwischen Bezugsgruppen; Blockadetechniken mit dem Körper; Einführung zu rechtlichen Fragen, Repression und Umgang damit.

Für Menschen, die noch keine Bezugsgruppe haben, wird es im Anschluss die Möglichkeit geben, eine Bezugsgruppe zu finden.

Bitte denkt daran, euch Verpflegung einzupacken.

Wir freuen uns auf euch!