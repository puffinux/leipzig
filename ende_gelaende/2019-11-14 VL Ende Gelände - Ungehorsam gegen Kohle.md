---
id: "455952375035480"
title: "VL: Ende Gelände - Ungehorsam gegen Kohle und Kapitalismus!"
start: 2019-11-14 15:00
end: 2019-11-14 17:00
address: Uni Leipzig HSG, HS11
link: https://www.facebook.com/events/455952375035480/
image: 74714980_551780158968378_2265609997557694464_n.jpg
teaser: Wir, das Bündnis Ende Gelände, fordern den sofortigen Kohleausstieg!  Doch
  warum eigentlich? Und wie machen wir das? Was können für Schwierigkeiten au
isCrawled: true
---
Wir, das Bündnis Ende Gelände, fordern den sofortigen Kohleausstieg!

Doch warum eigentlich?
Und wie machen wir das?
Was können für Schwierigkeiten auftauchen?

Diese und viele weitere spannende Fragen kommen in diesem Vortrag im
Rahmen der Ringvorlesung "Wege aus der Klimakrise - Zukunft gestalten
lernen" des Moduls "Bildung für nachhaltige Entwicklung" auf und werden
in einer anschließenden offenen Runde kommentiert, kritisiert und
diskutiert.

Wir sehen uns!

Eure Ende Geländis ♥


https://www.uni-leipzig.de/veranstaltungsdetail/artikel/buendnis-ende-gelaende-2019-11-14/