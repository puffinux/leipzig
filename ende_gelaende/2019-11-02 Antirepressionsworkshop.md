---
id: "1101711106694388"
title: Antirepressionsworkshop
start: 2019-11-02 14:00
end: 2019-11-02 18:00
address: Eisenbahnstraße 127
link: https://www.facebook.com/events/1101711106694388/
image: 73146160_539224123557315_3363584456538456064_n.jpg
teaser: Vom 29.11. bis zum 1.12 findet die nächste Ende Gelände Massenaktion statt,
  diesmal in der Lausitz. Repressionen gehören zu Aktionen zivilen Ungehorsa
isCrawled: true
---
Vom 29.11. bis zum 1.12 findet die nächste Ende Gelände Massenaktion statt, diesmal in der Lausitz.
Repressionen gehören zu Aktionen zivilen Ungehorsams oftmals dazu, das wird auch diesmal nicht anders sein. Sie können sehr unterschiedlich ausfallen und Menschen unterschiedlich stark belasten. Klimaaktivist*innen sehen sich leider immer härter mit staatlicher, aber auch ziviler Repression konfrontiert. Die neuen Polizeigesetzt tragen zu dieser Verschärfung bei.
Wissen um unsere Rechte und Solidarität sind Grundvoraussetzung für nachhaltigen Aktivismus.
Wir lassen uns nicht unterkriegen!
Doch was meinen wir eigentlich, wenn wir über „Repressionen“ sprechen und welche Support-Strukturen gibt, die uns helfen mit ihnen umzugehen?
Wie bereiten wir uns als Bezugsgruppe auf mögliche Repressionen vor?
In diesem Workshop wollen wir uns zusammen Gedanken machen zu Themen wie „Bezugsgruppe“, „Gesa“, „U-Haft“ oder „Vor- und Nachteile der Personalienverweigerung“.
Lasst uns gemeinsam überlegen, wie wir uns solidarisch besser schützen können und uns gegenseitig unterstützen können. Der Raum ist offen für Fragen, kommt vorbei!
