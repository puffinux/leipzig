---
id: '680906489013845'
title: Offenes Plenum
start: '2019-05-27 18:00'
end: '2019-05-27 20:30'
locationName: null
address: Erytrosin
link: 'https://www.facebook.com/events/680906489013845/'
image: 60262400_435644840581911_576288184812437504_n.jpg
teaser: 'Liebe Klimaaktivistis, Unser nächstes offenes Plenum ist am 25. Mai um 18 Uhr im Erytrosin!  Beim Plenum organisieren wir alle gemeinsam die nächsten'
recurring: null
isCrawled: true
---
Liebe Klimaaktivistis,
Unser nächstes offenes Plenum ist am 25. Mai um 18 Uhr im Erytrosin! 
Beim Plenum organisieren wir alle gemeinsam die nächsten Aktionen, tauschen uns über Ideen aus und vernetzen uns untereinander. Wir sind basisdemokratisch organisiert und entscheiden nach dem Konsensprinzip. 
Für alle Menschen, die noch nicht bei einem unserer Plena dabei waren und Lust bekommen haben sich bei Ende Gelände Leipzig zu engagieren: Kommt gern vorbei! Es gibt viele Möglichkeiten sich einzubringen!
#climatejusticenow!