---
id: '324531211772219'
title: Workshop zum Umgang mit Repressionen – Ende Gelände
start: '2019-06-12 18:00'
end: '2019-06-12 21:00'
locationName: null
address: Verein zur Förderung einer guten Sache" (Eisenbahnstraße 127)
link: 'https://www.facebook.com/events/324531211772219/'
image: 62558741_447236369422758_8145723063925735424_n.jpg
teaser: 'Workshop mit den Schwerpunkten ID Verweigerung, Gesa-Aufenthalt und Soliarbeit     Die Ende-Gelände Aktion im Rheinischen Revier vom 19.-24. JUNI steh'
recurring: null
isCrawled: true
---
Workshop mit den Schwerpunkten ID Verweigerung, Gesa-Aufenthalt und Soliarbeit
 


Die Ende-Gelände Aktion im Rheinischen Revier vom 19.-24. JUNI steht vor der Tür und wieder werden viele Menschen erwartet. So viele, dass es sinnvoll erscheint die Vorbereitung bzgl. Antirepression VOR der Anreise auf einem Aktionscamp zu beginnen und zwar dezentral. Hinzu kommt, dass es ein neues Polizeigesetz in NRW gibt, welches 7 Tage Gewahrsam zur Identitätsfeststellung erlaubt. 
Es wird kein juristischer Vortrag gehalten werden, sondern wir wollen den Raum öffnen um einen Austausch zu den Themen allgemeine Wirkung von Repression und Handlungsoptionen und den daran anknüpfenden Punkten wie Identitätsverweigerung und dem Umgang mit (langen) Aufenthalten in der Gesa stattfinden zu lassen.
Das Ziel ist es, dass euch die Informationen dabei helfen, mit einem sicheren Gefühl auf die Aktion zu fahren.
Vorweg müssen wir allerdings bekunden, dass wir kein*e Jurist*innen sind. Die Inhalte dieses Workshops stammen überwiegend aus dem vom Legal-Team erarbeiteten Workshop, der auf der Wurzel Schlagen Konferenz gehalten wurde. Deshalb können wir nicht alle Fragen beantworten und werden auch im Laufe das Workshops immer wieder auf etablierte Anti-Rep- Strukturen wie z.B. die Rote Hilfe verweisen.
 
Oft haben wir den Eindruck, dass es bei Repression vor allem um juristisches geht, doch unsere Waffe ist nicht, alle Paragraphen auswendig zu können, an die sich die Polizei dann leider doch oft nicht hält. Unsere Stärke liegt vielmehr darin, dass wir gemeinsam, kreativ und unberechenbar handeln können. Lasst uns kreativ bleiben und werden und über den Tellerrand der scheinbar beschränken Möglichkeiten hinausschauen!