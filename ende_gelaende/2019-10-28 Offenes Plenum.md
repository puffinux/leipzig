---
id: "2586467041441753"
title: Offenes Plenum
start: 2019-10-28 18:30
end: 2019-10-28 21:30
locationName: Interim
address: Demmeringstraße 32, 04177 Leipzig
link: https://www.facebook.com/events/2586467041441753/
image: 74676993_539195686893492_3926180779532484608_n.jpg
teaser: Liebe Klimaaktivist*innen,  unser nächstes offenes Plenum findet am 28.10. um
  18:30 Uhr im Interim statt!  Für alle Menschen, die noch nicht bei einem
isCrawled: true
---
Liebe Klimaaktivist*innen,

unser nächstes offenes Plenum findet am 28.10. um 18:30 Uhr im Interim statt! 
Für alle Menschen, die noch nicht bei einem unserer Plena dabei waren und Lust bekommen haben sich bei Ende Gelände Leipzig zu engagieren: Kommt gern vorbei und lernt uns kennen! Es gibt viele Möglichkeiten sich einzubringen!

Wir freuen uns auf euch! 