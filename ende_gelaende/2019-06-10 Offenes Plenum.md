---
id: '365558200985359'
title: Offenes Plenum
start: '2019-06-10 19:00'
end: '2019-06-10 21:30'
locationName: Interim
address: 'Demmeringstraße 32, 04177 Leipzig'
link: 'https://www.facebook.com/events/365558200985359/'
image: 61917274_445870269559368_5918048601233162240_n.jpg
teaser: Wir sind Teil des bundesweiten Ende Gelände Bündnisses und fordern mit Massenaktionen des zivilen Ungehorsams Klimagerechtigkeit und den sofortigen Ko
recurring: null
isCrawled: true
---
Wir sind Teil des bundesweiten Ende Gelände Bündnisses und fordern mit Massenaktionen des zivilen Ungehorsams Klimagerechtigkeit und den sofortigen Kohleausstieg! 
Als rnoch elativ neue Ortsgruppe wollen wir uns regional stärker vernetzen und vermehrt Aktionen in Leipzig und Umgebung planen. 
Du willst selbst aktiv werden? - Dich mit Klimagerechtigkeit beschäftigen, Demos organisieren oder Bagger blockieren? Dann komm vorbei, wir freuen uns =)
