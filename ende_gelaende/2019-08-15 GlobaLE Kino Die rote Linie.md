---
id: '471232343612556'
title: 'GlobaLE Kino: Die rote Linie'
start: '2019-08-15 20:00'
end: '2019-08-15 23:00'
locationName: Kulkwitzer See
address: 'Seestraße 1, 04207 Leipzig'
link: 'https://www.facebook.com/events/471232343612556/'
image: 64678240_3091524437526176_1986779097203736576_o.jpg
teaser: 'Wann: Do 15 August 2019 20:00 - 23:00  Die rote Linie - Widerstand im Hambacher Forst  BRD / 2019 / 115 min / Karin de Miguel Wessendorf / dt. /Im Ans'
recurring: null
isCrawled: true
---
Wann: Do 15 August 2019 20:00 - 23:00

Die rote Linie - Widerstand im Hambacher Forst

BRD / 2019 / 115 min / Karin de Miguel Wessendorf / dt. /Im Anschluss Diskussion u.a. mit Aktivist/innen von Ende Gelände und Extinction Rebellion. Die Veranstaltung ist Teil des Grünauer Kultursommers. Eintritt frei.

Der Film erzählt den Protest gegen die Vernichtung des Hambacher Forstes und den Widerstand gegen den Braunkohleabbau aus Sicht verschiedener Gruppen, die sich erst alleine, dann gemeinsam gegen den Energieriesen RWE stellen. Eine lokale David-gegen-Goliath-Geschichte unabdingbar mit den global relevanten Themen Klimawandel, Energiepolitik und ziviler Protestbewegung verknüpft. Der Hambacher Forst ist zu einem Symbol des Widerstandes gegen die bisherige Energiepolitik geworden. Die Auseinandersetzungen um die Räumung des Waldes im Herbst 2018 haben gezeigt, wie dringend die Diskussion um einen früheren Braunkohleausstieg für viele Menschen ist.
Seit 2015 begleitet die Regisseurin Menschen, die sich gegen den Braunkohletagebau im rheinischen Revier wehren: Clumsy, ein junger Waldbesetzer, der im Baumhaus lebt, um die Rodung des Waldes zu verhindern. Antje Grothus, eine Anwohnerin aus Buir, die sich erst mit einer Bürgerinitiative für den Erhalt der Lebensqualität in ihrem Dorf einsetzt und später von der Bundesregierung in die Kohlekommission einberufen wird als Vertreterin der Betroffenen in der Region. Lars Zimmer, ein Familienvater, der in einem Geisterdorf ausharrt, um Sand im Getriebe der Umsiedlung zu sein. Michael Zobel, Naturpädagoge, der erst kleine Führungen anbietet, um für die ökologische Bedeutung eines uralten Waldes zu sensibilisieren und später eine bewegende Ansprache an die Verantwortlichen richtet.
Der Film beobachtet, begleitet und mischt sich ein. Nebenbei erleben wir die Entstehung einer Bürgerbewegung, und diskutieren die Frage, wann die Rote Linie überschritten ist und ab welchem Punkt sich Menschen über die persönliche Betroffenheit engagieren.

Wo: Strand am Kulkwitzer See, (nahe Tauchschule Delphin)