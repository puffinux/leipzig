---
id: "2908943082472843"
title: Infoveranstaltung und Bezugsgruppenfindung
start: 2019-11-20 16:00
end: 2019-11-20 18:00
locationName: Handstand und Moral
address: Merseburger Straße 88 b, 04177 Leipzig
link: https://www.facebook.com/events/2908943082472843/
image: 75472840_552896945523366_6968110101571829760_n.jpg
teaser: Vom 29.11.- 1.12. findet die nächste Ende Gelände Massenaktion statt. Hier
  wird es noch mal Infos zur Aktion geben, sowie die Möglichkeit Fragen zu st
isCrawled: true
---
Vom 29.11.- 1.12. findet die nächste Ende Gelände Massenaktion statt. Hier wird es noch mal Infos zur Aktion geben, sowie die Möglichkeit Fragen zu stellen. Anschließend wird es für Menschen, die bisher alleine sind, die Möglichkeit geben eine passende Bezugsgruppe und Tandem zu finden.