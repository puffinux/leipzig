---
id: '529444187588137'
title: Öffentliches Aktionstraining
start: '2019-06-15 14:00'
end: '2019-06-15 18:00'
locationName: null
address: wird noch bekannt gegeben
link: 'https://www.facebook.com/events/529444187588137/'
image: 62308581_449093655903696_7079207582530797568_n.jpg
teaser: 'Du möchtest an der Massenaktion vom 19.-22.6 teilnehmen, hast so etwas aber noch nie gemacht? Komm doch einfach zu unserem öffentlichen Aktionstrainin'
recurring: null
isCrawled: true
---
Du möchtest an der Massenaktion vom 19.-22.6 teilnehmen, hast so etwas aber noch nie gemacht?
Komm doch einfach zu unserem öffentlichen Aktionstraining. Hier bekommst du alle Infos, die du unbedingt für die Vorbereitung brauchst! Offen für alle Menschis! 
Wir freuen uns auf euch!