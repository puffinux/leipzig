---
id: '1331101507068370'
title: 'Demo: Tanz und schwitz für Pödelwitz'
start: '2019-08-10 12:00'
end: '2019-08-10 17:00'
locationName: Bahnhof Neukieritzsch
address: 'Bahnhofstraße 1, 04575 Neukieritzsch'
link: 'https://www.facebook.com/events/1331101507068370/'
image: 67717742_769352866813426_4776297546353999872_n.jpg
teaser: 'ihr findet Latschdemos langweilig? Ihr wollt Körpereinsatz zeigen, eurem Protest kreativen Ausdruck verleihen und dabei laut und bunt sein? –Dann habe'
recurring: null
isCrawled: true
---
ihr findet Latschdemos langweilig? Ihr wollt Körpereinsatz zeigen, eurem Protest kreativen Ausdruck verleihen und dabei laut und bunt sein? –Dann haben wir genau das Richtige für euch! 

Am Samstag, den 10.08., laden wir euch alle herzlich zum Abschluss des Klimacamps in Pödelwitz zu unserer Tanzdemo*"Tanz und schwitz für Pödelwitz!" *ein! 

Wir treffen uns 12 Uhr am Bahnhof Neukieritzsch und tanzen dann gemeinsam laut und bunt zum Kraftwerk Lippendorf, wo wir um 15:30 Uhr ankommen. Dort wird es ein Abschlusstanzen mit einer kurzen Kundgebung geben.  Vom Kraftwerk aus kann es dann gegen 17 Uhr wieder mit der S-Bahn, mit dem Shuttle oder zu Fußzurückgehen (Für die Anreise aus Leipzig: dann lieber kein  Rad mitnehmen), oder ihr schaut danach noch in Pödelwitz vorbei. 

Für Verpflegung und kühle Getränke ist durch das Klimacamp gesorgt! Denkt daran, euch wettergerecht und nicht zu warm anzuziehen und Kopfbedeckung/ Schatten (bspw. Schirme) mitzunehmen. Gern könnt ihr noch eine eigene Choreografie einstudieren und während der Demo oder zum Abschluss präsentieren. 

Mehr Infos: https://www.klimacamp-leipzigerland.de/tanzdemo 

Hintergrund: Es gibt weiterhin viele Gründe für eine Demo: Trotz der Entscheidung der Kohlekomission und obwohl die Kohle unter Pödelwitz nicht gebraucht wird, um bis 2038 zu heizen, hält der Kohlekonzern MIBRAG daran fest, das Dorf abzubaggern. Außerdem sehen wir uns, besonders in Sachsen, mit dem Aufstieg rechter Politik konfrontiert. Die Leugnung des Klimawandels ist wieder im Trend. Der Kampf um Klimagerechtigkeit lässt sich nicht ohne einen Kampf gegen Rechts denken. 