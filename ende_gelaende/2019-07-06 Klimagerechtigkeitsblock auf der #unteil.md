---
id: '353733895335129'
title: 'Klimagerechtigkeitsblock auf der #unteilbar-Demo in Leipzig'
start: '2019-07-06 16:00'
end: '2019-07-06 19:00'
locationName: null
address: Windmühlenstraße
link: 'https://www.facebook.com/events/353733895335129/'
image: 62493091_2193370004080690_6842759488173244416_n.jpg
teaser: 'Antirassismus, Klimagerechtigkeit und die soziale Frage sind #unteilbar!  Die Temperaturen steigen, die Wetterextreme nehmen zu - die Klimakrise verän'
recurring: null
isCrawled: true
---
Antirassismus, Klimagerechtigkeit und die soziale Frage sind #unteilbar!

Die Temperaturen steigen, die Wetterextreme nehmen zu - die Klimakrise verändert die Lebenswelt auf unserem Planeten rasant.
Die Folgen sind dramatisch:
Viele Menschen müssen durch das Ansteigen der Meeresspiegel, wachsende Konflikte um Ressourcen oder die Zunahme von Extremwetterereignissen schon heute ihr Zuhause verlassen. Doch in den Ländern des globalen Nordens, die die Hauptverantwortung an der menschengemachten Klimakrise tragen, wird den Menschen auf der Flucht die Tür vor der Nase zugeschlagen - das Europäische Grenzregime ist wohl einer der sichtbarsten und erschütternsten Beweise dafür. Die globale Erwärmung ist ein weltweites Problem, dem wir uns nur gemeinsam annehmen können. Wir müssen jetzt handeln und aktiv werden; vielfältig, entschlossen und solidarisch! Die Folgen der globalen Erwärmung machen nicht Staatsgrenzen halt!

Trotzdem gibt es Gruppen, die die menschengemachte Klimakrise anzweifeln oder aktiv leugnen. Auch in Sachsen wird diese Meinung von der AFD vertreten. Damit verbunden ist eine Abschottung der Grenzen für all diejenigen, die wegen der Zerstörung ihrer Lebensgrundlagen ihr Zuhause verlassen müssen.

Doch nicht nur völkische Klimawandel-Leugner*innen stemmen sich gegen eine konsequente Klimapolitik. Auch von Seiten der sächsischen Staatsregierung wurden bisher keine konkreten Klimaschutzmaßnahmen ergriffen. Immer wieder werden wirtschaftliche, soziale und ökologische Fragen als Widerspruch dargestellt. Dies leistet keinen Beitrag zur Lösung der Krisen, sondern führt zur Verhärtung von Fronten. Das ist nicht länger tragbar! Stattdessen brauchen wir Klimagerechtigkeit, um ein gutes Leben für Alle zu ermöglichen.

In den letzten Jahren hat die Klimabewegung jedoch Fahrt aufgenommen: Mit Ende Gelände, den Großprotesten im Hambacher Forst 2018 und der Fridays for Future-Bewegung, werden Forderungen nach einer konsequenten Klimapolitik aus allen Teilen der Gesellschaft laut. Dabei geht es nicht nur um Klimaschutz sondern um globale Klimagerechtigkeit! Das bedeutet auch, dass wir uns jetzt für eine lebenswerte Welt, heute und für zukünftige Generationen einsetzen müssen!
Eine Forderung der Klimagerechtigkeitsbewegung ist der sofortige Kohleausstieg. Das betrifft auch Sachsen mit den Revieren in der Lausitz und südlich von Leipzig. Doch gegen diese Forderung wird immer wieder das Argument des Verlustes von Arbeitsplätzen angeführt. Konzerne und politische Parteien spielen die Interessen der Beschäftigten so gegen eine gerechte Klimapolitik aus, um sich selbst aus der Verantwortung zu ziehen. Dabei könnten und sollten die Beschäftigten in der Kohleindustrie für den Wegfall ihrer Arbeitsplätze angemessen entschädigt werden. Damit muss jetzt begonnen werden!
Die soziale und ökologische Frage dürfen nicht gegeneinander ausgespielt werden. Wir können es uns nicht mehr leisten, den Kohleausstieg immer weiter nach hinten zu verschieben! Nur ein Zusammendenken sozialer und klimapolitischer Ungerechtigkeit bringt uns einer Lösung näher.

Es braucht sozial-ökologische Transformationsprozesse in den Braunkohleregionen (und überall), ein Ende des europäischen Grenzregimes und eine entschlossene Klimapolitik.
Deshalb sagen wir: Antirassismus, Klimagerechtigkeit und die soziale Frage sind #unteilbar!

Dies wollen wir mit einem Klimagerechtigkeitsblock auf der #unteilbar-Demo zum Ausdruck bringen! Kommt alle, kommt alle in grün!