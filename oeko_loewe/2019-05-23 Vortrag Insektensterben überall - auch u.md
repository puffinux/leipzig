---
id: '2211095972270832'
title: 'Vortrag: Insektensterben überall - auch unter unseren Füßen?'
start: '2019-05-23 19:00'
end: '2019-05-23 21:00'
locationName: Ökolöwe - Umweltbund Leipzig e.V.
address: 'Bernhard-Göring-Str. 152, 04277 Leipzig'
link: 'https://www.facebook.com/events/2211095972270832/'
image: 60003365_10157281176967855_1231968883041107968_n.jpg
teaser: 'Im Boden leben zahlreiche Insekten als Larven. Das Insektensterben bedroht jedoch auch diesen Lebensraum. Was aber passiert mit dem Boden, wenn allmäh'
recurring: null
isCrawled: true
---
Im Boden leben zahlreiche Insekten als Larven. Das Insektensterben bedroht jedoch auch diesen Lebensraum. Was aber passiert mit dem Boden, wenn allmählich seine Bewohner verschwinden? 🐛
Prof. Nico Eisenhauer vom Deutschen Zentrum für integrative Biodiversitätsforschung (iDiv) widmet sich dieser Frage in einem Vortrag hier bei uns in der Umweltbibliothek Leipzig. 

Der Eintritt ist frei. Da die Platzkapazitäten begrenzt sind, ist eine Anmeldung erforderlich. 
👉 Zur Anmeldung geht's hier: https://www.oekoloewe.de/events/vortrag-insektensterben-ueberall-auch-unter-unseren-fuessen.html 

Die Veranstaltung findet im Rahmen der 63. Leipziger Naturschutzwoche (16. bis 23. Mai) statt. Das Programm findet ihr hier: https://bit.ly/307WWWS

--
Die Umweltbibliothek Leipzig ist eine öffentliche Einrichtung des Ökolöwe - Umweltbund Leipzig e.V. und wird gefördert von der Stadt Leipzig, Amt für Umweltschutz.

Bild © pixabay.de