---
id: '315981069123563'
title: Leipziger Ökofete 2019
start: '2019-06-16 12:00'
end: '2019-06-16 19:00'
locationName: Clara Zetkin Park Springbrunnen
address: 04107 Leipzig
link: 'https://www.facebook.com/events/315981069123563/'
image: 59694761_10157271428637855_175403067049508864_n.jpg
teaser: "\U0001F389 Am 16. Juni ist es wieder soweit: Wir Ökolöwen laden euch zu unserer Leipziger Ökofete ein. \U0001F468‍\U0001F466‍\U0001F466 Kommt vorbei, wenn wir die Anton-Bruckner-Alle"
recurring: null
isCrawled: true
---
🎉 Am 16. Juni ist es wieder soweit: Wir Ökolöwen laden euch zu unserer Leipziger Ökofete ein. 👨‍👦‍👦
Kommt vorbei, wenn wir die Anton-Bruckner-Allee im Clara-Zetkin-Park in die größte Umweltverbrauchermesse Mitteldeutschlands verwandeln! Und feiert mit uns einen gelungenen Abschluss der Leipziger Umwelttage 2019.
 
Euch erwartet die bunte Vielfalt des Natur- und Umweltschutzes:

💚 jede Menge Bio-Produkte, faire Klamotten und andere Textilien, Kosmetik, Lebensmittel, Ökostromanbieter und, und, und
💚 engagierte, tolle Vereine und Initiativen
💚 innovative Unternehmen mit guten Ideen
💚 leckeres Essen und Trinken in Bio-Qualität, darunter viele vegane & vegetarische Köstlichkeiten
💚 ganz viel Spannendes zum Mitmachen für Groß & Klein
💚 Gaukler und Clowns

Das ist die Gelegenheit, sich mit Engagierten und Interessierten aus dem Bereich Umweltschutz auszutauschen. Lasst euch inspirieren: "Eine andere Welt ist möglich"!

📣 Sagt es weiter. Bringt alle mit. Wir freuen uns auf euch! 😊

Weitere Infos gibt's hier: www.oekoloewe.de/ökofete 