---
id: "559537291480532"
title: "Upcycling: Weihnachtsbasteln mit 'Restlos'"
start: 2019-12-04 16:00
end: 2019-12-04 18:00
locationName: Ökolöwe - Umweltbund Leipzig e.V.
address: Bernhard-Göring-Str. 152, 04277 Leipzig
link: https://www.facebook.com/events/559537291480532/
image: 72695743_10157795341362855_6160453162395238400_n.jpg
teaser: "'Restlos', das Upcycling-Projekt des Mütterzentrum e.V. Leipzig, ist bei uns
  Ökolöwen zu Gast: Es weihnachtet sehr 🎄 und wir basteln in der Umweltbib"
isCrawled: true
---
'Restlos', das Upcycling-Projekt des Mütterzentrum e.V. Leipzig, ist bei uns Ökolöwen zu Gast: Es weihnachtet sehr 🎄 und wir basteln in der Umweltbibliothek Leipzig kleine Geschenke und Dekorationen aus Restmaterialien für diese besinnliche Zeit! Ob Sterne 🌟, Schneelandschaften oder Geschenkverpackungen - aus Pappe, Folie, Eierkarton und Co. lassen sich schöne Dinge gestalten. Denn: Nix mit ab in die Tonne 🚯! 

Für Kinder ab vier Jahren und ihre Familien.

Der Eintritt ist frei. Weitere Infos gibt's hier 👉 www.oekoloewe.de/events/upcycling-weihnachtsbasteln-mit-restlos.html

--
Die Umweltbibliothek Leipzig ist eine öffentliche Einrichtung des Ökolöwe - Umweltbund Leipzig e.V. und wird gefördert von der Stadt Leipzig, Amt für Umweltschutz.

📷 © Mütterzentrum e.V.
