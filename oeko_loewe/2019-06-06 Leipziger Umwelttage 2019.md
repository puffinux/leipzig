---
id: '1786793384759724'
title: Leipziger Umwelttage 2019
start: '2019-06-06 08:00'
end: '2019-06-06 22:00'
locationName: null
address: Leipzig
link: 'https://www.facebook.com/events/1786793384759724/'
image: 59968429_10157271420867855_3920765628931112960_n.jpg
teaser: "\U0001F49A Der Weltumwelttag am 5. Juni ist der Startschuss für die Leipziger Umwelttage 2019. Zwei Wochen lang bieten wir Ökolöwen euch ein buntes Programm m"
recurring: null
isCrawled: true
---
💚 Der Weltumwelttag am 5. Juni ist der Startschuss für die Leipziger Umwelttage 2019. Zwei Wochen lang bieten wir Ökolöwen euch ein buntes Programm mit über 100 Umweltveranstaltungen in und um Leipzig. Hier gibt es Filme und Exkursionen, Diskussionen, Führungen und vieles mehr – angeboten von engagierten LeipzigerInnen, von Vereinen, Bürgerinitiativen, der Stadt Leipzig, Forschungseinrichtungen und Unternehmen. 
Viel Spaß beim Erkunden, Erleben, Entdecken! 🤓💬

👉 Hier geht's zum Programm: www.oekoloewe.de/umwelttage

📣 Als phänomenalen Abschluss feiern wir mit euch die Leipziger Ökofete 2019 am 16. Juni im Clara-Zetkin-Park.

---
Ein herzliches Dankeschön für die Unterstützung der Leipziger Umwelttage geht an alle Engagierten, die eine Veranstaltung anbieten sowie an die Stadtverwaltung Leipzig, die Leipziger Gruppe, Biomare, UmweltBank, Bundesamt für Naturschutz BfN, Leipziger Zeitung und LEIPZIG FERNSEHEN!