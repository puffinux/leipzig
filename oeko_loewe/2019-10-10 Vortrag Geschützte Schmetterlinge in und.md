---
id: '352317462315792'
title: 'Vortrag: Geschützte Schmetterlinge in und um Leipzig'
start: '2019-10-10 19:00'
end: '2019-10-10 21:30'
locationName: Ökolöwe - Umweltbund Leipzig e.V.
address: 'Bernhard-Göring-Str. 152, 04277 Leipzig'
link: 'https://www.facebook.com/events/352317462315792/'
image: 69469142_10157572510627855_7424763050390978560_n.jpg
teaser: "Das Insektensterben macht auch vor Leipzigs Schmetterlingen \U0001F98B nicht halt!  Wie ist es möglich, sie zu schützen und die Artenvielfalt zu erhalten? Wir"
recurring: null
isCrawled: true
---
Das Insektensterben macht auch vor Leipzigs Schmetterlingen 🦋 nicht halt!

Wie ist es möglich, sie zu schützen und die Artenvielfalt zu erhalten? Wir Ökolöwen laden zusammen mit Ronald Schiller, wissenschaftlicher Mitarbeiter im Naturkundemuseum Leipzig, zu einem Vortrag in die Umweltbibliothek Leipzig ein. Der Referent zeigt Möglichkeiten und Grenzen zum Erhalt der heimischen Schmetterlinge auf und erläutert, welche unterschiedlichen Ansprüche die Tag- und Nachtfalter an ihren Lebensraum haben.

Der Eintritt ist frei. Aufgrund begrenzter Platzkapazitäten ist eine Anmeldung erforderlich.
👉 Zur Anmeldung geht's hier: www.oekoloewe.de/events/vortrag-geschuetzte-schmetterlinge-in-und-um-leipzig.html

--
Die Umweltbibliothek Leipzig ist eine öffentliche Einrichtung des Ökolöwe - Umweltbund Leipzig e.V. und wird gefördert von der Stadt Leipzig, Amt für Umweltschutz.

Bild © Jens Kipping