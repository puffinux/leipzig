---
id: "1483765238437931"
title: "Gesprächsrunde: Vielfalt im Naturgarten"
start: 2019-11-26 19:00
end: 2019-11-26 20:30
locationName: Ökolöwe - Umweltbund Leipzig e.V.
address: Bernhard-Göring-Str. 152, 04277 Leipzig
link: https://www.facebook.com/events/1483765238437931/
image: 75271491_10157762321367855_150445918330552320_n.jpg
teaser: Vielfalt, Schönheit und Nutzen durch naturnahe
  Gartengestaltung  Gesprächsrunde mit Daniel Jakumeit  Lust auf mehr
  Artenvielfalt im eigenen Garten? Da
isCrawled: true
---
Vielfalt, Schönheit und Nutzen durch naturnahe Gartengestaltung 
Gesprächsrunde mit Daniel Jakumeit

Lust auf mehr Artenvielfalt im eigenen Garten? Dann kommt zu unserer Gesprächsrunde rund um das Thema Naturgarten in die Umweltbibliothek Leipzig. Im Gespräch mit Ökolöwin Antje Osterland berichtet Daniel Jakumeit von seinen Erfahrungen aus 20 Jahren Naturgartengestaltung und ehrenamtlicher Mitarbeit im Hortus-Netzwerk. Anhand von Bildern aus seinem eigenen naturnahen Gartenreich im Leipziger Umland gibt Daniel Jakumeit praktische Tipps für Euren Naturgarten und beantwortet Eure Fragen.

Das Angebot richtet sich insbesondere an Aktive in Leipziger Gemeinschaftsgartenprojekten, steht aber auch Klein- und HobbygärtnerInnen offen.

Der Eintritt ist frei. Aufgrund begrenzter Platzkapazitäten ist eine Anmeldung erforderlich.
👉 Weitere Infos zur Gesprächsrunde sowie zur Anmeldung unter:
www.oekoloewe.de/naturnah-gaertnern.html

Die Gesprächsrunde findet im Rahmen des Projektes "Naturnah Gärtnern - für Artenvielfalt in Leipzig" statt und wird von der Stadt Leipzig, Amt für Stadtgrün und Gewässer, gefördert.

📷 © Daniel Jakumeit