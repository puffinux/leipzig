---
id: '145698943044759'
title: 'Leipzig liest: Schiff oder Schornstein'
start: '2019-03-23 19:00'
end: '2019-03-23 21:00'
locationName: Ökolöwe - Umweltbund Leipzig e.V.
address: 'Bernhard-Göring-Str. 152, 04277 Leipzig'
link: 'https://www.facebook.com/events/145698943044759/'
image: 51968963_10157076151187855_7212346672444080128_n.jpg
teaser: 'Zur Leipziger Buchmesse heißt es wieder: „Leipzig Liest“ und wir Ökolöwen sind mit dabei! Kommt zu uns in die Umweltbibliothek Leipzig, wenn Andrea St'
recurring: null
isCrawled: true
---
Zur Leipziger Buchmesse heißt es wieder: „Leipzig Liest“ und wir Ökolöwen sind mit dabei! Kommt zu uns in die Umweltbibliothek Leipzig, wenn Andrea Stift-Laube aus ihrem neuen Roman 'Schiff oder Schornstein' vorliest.

Für was lohnt es sich zu kämpfen? Warum werden manche Tiere von uns gestreichelt und andere gegessen? Geistreich, schräg und makaber stellt die Autorin das Verhältnis von Mensch und Tier in Frage.

Erschienen ist der Roman gerade im Verlag Kremayr & Scheriau.
Da die Platzkapazitäten begrenzt sind, ist eine Anmeldung erforderlich. 
👉Zur Anmeldung geht's hier: https://www.oekoloewe.de/events/leipzig-liest-schiff-oder-schornstein.html

--
Die Umweltbibliothek Leipzig ist eine öffentliche Einrichtung des Ökolöwe - Umweltbund Leipzig e.V. und wird gefördert von der Stadt Leipzig, Amt für Umweltschutz.

Bild © Verlag Kremayr & Scheriau