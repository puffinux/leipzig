---
id: '647517942338309'
title: 'Rundgang: Insektenvielfalt im Stadtgarten Connewitz'
start: '2019-05-21 16:00'
end: '2019-05-21 18:00'
locationName: Stadtgarten Connewitz
address: 'Kohrener Straße 4, 04277 Leipzig'
link: 'https://www.facebook.com/events/647517942338309/'
image: 59803557_10157273763812855_7133649217880326144_n.jpg
teaser: 'Die Ökolöwen zeigen Euch im Stadtgarten Connewitz, wie ihr Wildbienen ein Zuhause schaffen könnt. Gleichzeitig gibts Tipps für das insektenfreundliche'
recurring: null
isCrawled: true
---
Die Ökolöwen zeigen Euch im Stadtgarten Connewitz, wie ihr Wildbienen ein Zuhause schaffen könnt. Gleichzeitig gibts Tipps für das insektenfreundliche Bewirtschaften und Gestalten des Gartens. Seid gespannt, erfreut Euch an der Vielfalt unserer grünen Oase und holt Euch Anregungen für den eigenen Garten oder Hinterhof. 

Der Eintritt ist frei. Weitere Infos unter: https://www.oekoloewe.de/events/rundgang-insektenvielfalt-im-stadtgarten-connewitz.html  

Die Veranstaltung findet im Rahmen der 63. Leipziger Naturschutzwoche (16. bis 23. Mai) statt. Das Programm findet ihr hier: https://bit.ly/307WWWS 