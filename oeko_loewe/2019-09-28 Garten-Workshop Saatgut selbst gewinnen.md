---
id: '2688833484480574'
title: 'Garten-Workshop: Saatgut selbst gewinnen'
start: '2019-09-28 10:00'
end: '2019-09-28 13:00'
locationName: Stadtgarten Connewitz
address: 'Kohrener Straße 4, 04277 Leipzig'
link: 'https://www.facebook.com/events/2688833484480574/'
image: 69875355_10157595101837855_7237481482485235712_n.jpg
teaser: 'Garten-Workshop: Saatgut selbst gewinnen Vielfalt und Selbstbestimmtheit auf dem Beet  +++ Bitte beachten: kurzfristige Terminänderung +++   Saatgut v'
recurring: null
isCrawled: true
---
Garten-Workshop: Saatgut selbst gewinnen
Vielfalt und Selbstbestimmtheit auf dem Beet

+++ Bitte beachten: kurzfristige Terminänderung +++ 

Saatgut von eigenem Gemüse, Kräutern und Wildblumen zu ernten, gibt ein herrliches Gefühl von Selbstbestimmtheit und spart zugleich Geld. Darüber hinaus ist es ein wichtiger Beitrag zum Erhalt der Sortenvielfalt, denn viele alte, robuste Kulturpflanzensorten werden auf dem Saatgutmarkt nicht mehr angeboten. Jetzt ist Erntezeit! Wir Ökolöwen laden Euch zu einem Workshop in den Stadtgarten Connewitz ein, bei dem Ihr praktische Tipps zur Gewinnung von eigenem Saatgut bekommt. Ihr erfahrt außerdem, was es beim Kauf von Saatgut zu beachten gibt. Nebenbei tauschen wir Ideen aus, um die Saaten-Tauschkultur in Leipzig zu stärken.

Die Teilnahme ist kostenfrei. Das Angebot richtet sich insbesondere an Aktive in Leipziger Gemeinschaftsgartenprojekten, steht aber auch Klein- und HobbygärtnerInnen offen. 

Weitere Infos zum Workshop sowie zur Anmeldung unter: 
www.oekoloewe.de/naturnah-gaertnern.html 

Der Workshop findet im Rahmen des Projektes "Naturnah Gärtnern - für Artenvielfalt in Leipzig" statt und wird von der Stadt Leipzig, Amt für Stadtgrün und Gewässer, gefördert.
