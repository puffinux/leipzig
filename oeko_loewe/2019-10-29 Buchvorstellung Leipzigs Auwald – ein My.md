---
id: "2514619485256314"
title: "Buchvorstellung: Leipzigs Auwald – ein Mysterium"
start: 2019-10-29 19:00
end: 2019-10-29 21:00
locationName: Ökolöwe - Umweltbund Leipzig e.V.
address: Bernhard-Göring-Str. 152, 04277 Leipzig
link: https://www.facebook.com/events/2514619485256314/
image: 69525273_10157572628327855_5286528163491348480_n.jpg
teaser: "Auf einen Spaziergang der besonderen Art nimmt Euch die Leipziger Autorin
  Brigitta Lehmann mit: In literarischen 📖 Exkursionen präsentiert sie in der"
isCrawled: true
---
Auf einen Spaziergang der besonderen Art nimmt Euch die Leipziger Autorin Brigitta Lehmann mit: In literarischen 📖 Exkursionen präsentiert sie in der Umweltbibliothek Leipzig "ihren" Leipziger Auwald. 🌳

Das einzigartige Ökosystem bietet nicht nur für das Stadtklima einen unschätzbaren Wert, sondern auch wichtige Lebensräume für geschützte und vom Aussterben bedrohte Arten. Umrahmt wird die Buchvorstellung mit Fotografien und Gedichten.

Das Buch 'Leipzigs Auwald - ein Mysterium' ist 2019 im Engelsdorfer Verlag erschienen. 

Der Eintritt ist frei. Aufgrund begrenzter Platzkapazitäten ist eine Anmeldung erforderlich.
👉 Zur Anmeldung geht's hier: www.oekoloewe.de/events/buchvorstellung-leipzigs-auwald-ein-mysterium.html

--
Die Umweltbibliothek Leipzig ist eine öffentliche Einrichtung des Ökolöwe - Umweltbund Leipzig e.V. und wird gefördert von der Stadt Leipzig, Amt für Umweltschutz.