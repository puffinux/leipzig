---
id: '1105448699615873'
title: 'Unser Saatgut - Wir ernten, was wir säen'
start: '2019-03-16 17:00'
end: '2019-03-16 18:30'
locationName: Kinobar Prager Frühling
address: 'Bernhard-Göring-Straße 152, 04277 Leipzig'
link: 'https://www.facebook.com/events/1105448699615873/'
image: 50163198_10156244294284001_4599076587151818752_n.jpg
teaser: Wir freuen uns auf die Kooperation mit dem Ökolöwe - Umweltbund Leipzig e.V.! Die Filmvorführung findet im Rahmen der Saatgut-Tauschbörse statt.  Weni
recurring: null
isCrawled: true
---
Wir freuen uns auf die Kooperation mit dem Ökolöwe - Umweltbund Leipzig e.V.!
Die Filmvorführung findet im Rahmen der Saatgut-Tauschbörse statt.

Wenige Dinge auf unserer Erde sind so kostbar und lebensnotwendig wie Samen. Verehrt und geschätzt seit Beginn der Menschheit, sind sie die Quelle allen Lebens. Sie ernähren und heilen uns und liefern die wichtigsten Rohstoffe für unseren Alltag. In Wirklichkeit sind sie das Leben selbst. Doch diese wertvollste aller Ressourcen ist bedroht: Mehr als 90 % aller Saatgutsorten sind bereits verschwunden. Biotech-Konzerne wie Syngenta und Bayer/Monsanto kontrollieren mit genetisch veränderten Monokulturen längst den globalen Saatgutmarkt. Immer mehr passionierte Bauern, Wissenschaftler, Anwälte und indigene Saatgutbesitzer kämpfen daher wie David gegen Goliath um die Zukunft der Samenvielfalt.