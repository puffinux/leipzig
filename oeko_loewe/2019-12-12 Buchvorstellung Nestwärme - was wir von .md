---
id: "2623766281003365"
title: "Buchvorstellung: Nestwärme - was wir von Vögeln lernen können"
start: 2019-12-12 18:30
end: 2019-12-12 20:30
locationName: Ökolöwe - Umweltbund Leipzig e.V.
address: Bernhard-Göring-Str. 152, 04277 Leipzig
link: https://www.facebook.com/events/2623766281003365/
image: 77106973_10157795146912855_3431779968739180544_n.jpg
teaser: Was können wir Menschen von Vögeln 🐦 lernen? Eine Menge! Der vielfach
  ausgezeichnete Naturschützer Ernst Paul Dörfler berichtet bei uns Ökolöwen vom
isCrawled: true
---
Was können wir Menschen von Vögeln 🐦 lernen? Eine Menge! Der vielfach ausgezeichnete Naturschützer Ernst Paul Dörfler berichtet bei uns Ökolöwen vom geheimen Leben der Vögel und ihrem achtsamen Umgang miteinander. In einem interaktiven Vortrag stellt er sein Buch 'Nestwärme' in der Umweltbibliothek Leipzig vor - ein Plädoyer für einen nachhaltigen Umgang mit der Natur 🍃 Und eine augenzwinkernde Aufforderung, das eigene Leben hin und wieder aus einer neuen Perspektive zu betrachten. 

Das Buch 'Nestwärme – was wir von Vögeln lernen können' ist 2019 im Carl Hanser Verlag erschienen. 

Der Eintritt ist frei. Aufgrund begrenzter Platzkapazitäten ist eine Anmeldung erforderlich.
👉 Zur Anmeldung geht's hier: www.oekoloewe.de/events/buchvorstellung-nestwaerme.html

--
Die Umweltbibliothek Leipzig ist eine öffentliche Einrichtung des Ökolöwe - Umweltbund Leipzig e.V. und wird gefördert von der Stadt Leipzig, Amt für Umweltschutz.

 📷 © Peter-Andreas Hassiepen
