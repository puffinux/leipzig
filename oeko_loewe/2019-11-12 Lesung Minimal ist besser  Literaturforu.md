---
id: "728477590997867"
title: "Lesung: Minimal ist besser | Literaturforum Bibliothek"
start: 2019-11-12 19:00
end: 2019-11-12 21:00
locationName: Ökolöwe - Umweltbund Leipzig e.V.
address: Bernhard-Göring-Str. 152, 04277 Leipzig
link: https://www.facebook.com/events/728477590997867/
image: 72961855_10157682531812855_4957632220937322496_n.jpg
teaser: Er ist ein moderner Nomade. Der Schriftsteller Roman Israel hat alles
  aufgegeben und besitzt nur noch einen Koffer 🧳 voller Klamotten und seinen
  Lapt
isCrawled: true
---
Er ist ein moderner Nomade. Der Schriftsteller Roman Israel hat alles aufgegeben und besitzt nur noch einen Koffer 🧳 voller Klamotten und seinen Laptop 💻. So zieht er von Stadt zu Stadt, von Ort zu Ort, von Erfahrung zu Erfahrung. 

Wir Ökolöwen laden Euch zur Lesung 📖 'Minimal ist besser' von Roman Israel in die Umweltbibliothek Leipzig ein. Lasst Euch in die Welt des Minimalismus entführen und erfahrt, ob der Versuch nur eine Schnapsidee war oder in die große Freiheit führt.

Das E-Book 'Minimal ist besser' ist 2018 im Verlag mikrotext erschienen. Die Veranstaltung findet im Rahmen von 'Literaturforum Bibliothek' statt.

https://www.mikrotext.de/book/roman-israel-minimal-ist-besser-vom-suessen-leben-ohne-besitz/

Der Eintritt ist frei. Aufgrund begrenzter Platzkapazitäten ist eine Anmeldung erforderlich.
👉 Zur Anmeldung geht's hier: www.oekoloewe.de/events/lesung-minimal-ist-besser.html

Bild ©️ Jörg Singer
--
Die Umweltbibliothek Leipzig ist eine öffentliche Einrichtung des Ökolöwe - Umweltbund Leipzig e.V. und wird gefördert von der Stadt Leipzig, Amt für Umweltschutz.
