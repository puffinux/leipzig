---
id: "1296745117184421"
title: "Gesprächsrunde: Artenvielfalt auf dem Balkon"
start: 2019-12-03 18:00
end: 2019-12-03 20:00
locationName: Ökolöwe - Umweltbund Leipzig e.V.
address: Bernhard-Göring-Str. 152, 04277 Leipzig
link: https://www.facebook.com/events/1296745117184421/
image: 75424720_10157762715737855_4236637955038380032_n.jpg
teaser: Ernteglück, Artenschutz & Wohlfühloase – von den Freuden eines
  Bio-Balkons   Gesprächsrunde mit Birgit Schattling in der Umweltbibliothek
  Leipzig  Gär
isCrawled: true
---
Ernteglück, Artenschutz & Wohlfühloase – von den Freuden eines Bio-Balkons 

Gesprächsrunde mit Birgit Schattling in der Umweltbibliothek Leipzig

Gärtnern auf dem Balkon bedeutet Gärtnern auf kleinstem Raum. Und da geht so einiges! GU-Autorin Birgit Schattling zeigt im Gespräch mit Ökolöwin Antje Osterland, was mitten in der Stadt auf kleinstem Raum an Artenreichtum möglich ist. Auf ihrer preisgekrönten Berliner Balkon-Oase gedeihen auf neun Quadratmetern Gemüse, Obstgehölze, heimische Wildkräuter und Blühschönheiten - anstelle von trister Geranien-Monokultur. Kommt vorbei und lasst Euch für die kommende Balkonsaison inspirieren!

Der Eintritt ist frei. Aufgrund begrenzter Platzkapazitäten ist eine Anmeldung erforderlich.
👉 Weitere Infos zur Gesprächsrunde sowie zur Anmeldung unter:
 www.oekoloewe.de/naturnah-gaertnern.html
 
Die Gesprächsrunde findet im Rahmen des Projektes 'Naturnah Gärtnern - für Artenvielfalt in Leipzig' statt und wird von der Stadt Leipzig, Amt für Stadtgrün und Gewässer, gefördert.