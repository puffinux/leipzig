---
id: "2449828685100415"
title: "Vortrag: Abfall vermeiden fürs Klima"
start: 2019-11-19 19:00
end: 2019-11-19 21:00
locationName: Ökolöwe - Umweltbund Leipzig e.V.
address: Bernhard-Göring-Str. 152, 04277 Leipzig
link: https://www.facebook.com/events/2449828685100415/
image: 72345347_10157682647392855_1886189464187830272_n.jpg
teaser: Kunststoffe 🧴 fluten die Meere, in den Städten wachsen die Müllberge, immer
  mehr Mikroplastik gerät in die Erde - viele Menschen fühlen sich angesich
isCrawled: true
---
Kunststoffe 🧴 fluten die Meere, in den Städten wachsen die Müllberge, immer mehr Mikroplastik gerät in die Erde - viele Menschen fühlen sich angesichts dieser Entwicklungen überfordert und ratlos. ❓

Die AG Abfall der Ökolöwen lädt zu einem interaktiven Vortrag in die Umweltbibliothek Leipzig ein. Die ExpertInnen geben praktische Tipps, wie sich im eigenen Alltag weniger Abfall produzieren und damit die Umwelt und das Klima schonen lässt. Im Anschluss an den Vortrag besteht die Möglichkeit zum Austausch.

Der Eintritt ist frei. Aufgrund begrenzter Platzkapazitäten ist eine Anmeldung erforderlich.
👉 Zur Anmeldung geht's hier: www.oekoloewe.de/events/vortrag-abfall-vermeiden-fuers-klima.html

Die Veranstaltung findet im Rahmen der Europäischen Woche der Abfallvermeidung (16.-24. November) statt. Das Leipziger Bündnis Abfallvermeidung bietet in dieser Zeit viele weitere Informations- und Mitmachangebote an - schaut vorbei!

--
Die Umweltbibliothek Leipzig ist eine öffentliche Einrichtung des Ökolöwe - Umweltbund Leipzig e.V. und wird gefördert von der Stadt Leipzig, Amt für Umweltschutz.
