---
id: '349770362345827'
title: Leipziger Radnacht 2019
start: '2019-08-30 19:00'
end: '2019-08-30 22:00'
locationName: null
address: 'Clara-Zetkin-Park, 04178 Leipzig'
link: 'https://www.facebook.com/events/349770362345827/'
image: 60334796_10157288440577855_5548447282224955392_n.jpg
teaser: "\U0001F6B2\U0001F6B2 Lasst uns gemeinsam aufs Fahrrad steigen und ein Zeichen für ein fahrradfreundliches Leipzig setzen. Zur Leipziger RADNACHT erobern wir mit Tause"
recurring: null
isCrawled: true
---
🚲🚲 Lasst uns gemeinsam aufs Fahrrad steigen und ein Zeichen für ein fahrradfreundliches Leipzig setzen. Zur Leipziger RADNACHT erobern wir mit Tausenden RadfahrerInnen die autofreien Straßen Leipzigs! 🚲🚲

📍 Treffpunkt: 19:00 Uhr am Clara-Zetkin-Park Springbrunnen

📣 Sagt es weiter. Bringt alle mit. Wir freuen uns auf euch! 😊
Weitere Infos: www.oekoloewe.de/radnacht

Die Rundfahrt führt über den Leipziger Innenstadtring durch den Leipziger Süden und die B2 zurück zum Clara-Zetkin-Park. Zum Ausklang gibt es Musik, Speisen und Getränke. 

Die Teilnahme an der Leipziger RADNACHT ist kostenfrei. Eine Anmeldung ist nicht erforderlich.

Mit der Leipziger RADNACHT startet das STADTRADELN 2019. Meldet jetzt Euer Team an: www.oekoloewe.de/stadtradeln

---
Das Stadtradeln ist eine Kampagne des Klima-Bündnis e.V., organisiert von der Stadtverwaltung Leipzig und dem Ökolöwe – Umweltbund Leipzig e.V. 