---
id: '2067112026669335'
title: 'Sonderveranstaltung: Unsere Wiese - Ein Paradies nebenan'
start: '2019-04-30 17:00'
end: '2019-04-30 18:45'
locationName: Kinobar Prager Frühling
address: 'Bernhard-Göring-Straße 152, 04277 Leipzig'
link: 'https://www.facebook.com/events/2067112026669335/'
image: 54517426_10156358019764001_731496680708374528_n.jpg
teaser: Sonderveranstaltung mit Ökolöwe - Umweltbund Leipzig e.V. - Stadtgarten Connewitz  Nach einer  kurzen thematischen Einführung der Ökolöwen stellt Mich
recurring: null
isCrawled: true
---
Sonderveranstaltung mit Ökolöwe - Umweltbund Leipzig e.V. - Stadtgarten Connewitz 
Nach einer  kurzen thematischen Einführung der Ökolöwen stellt Michael Schulze, Amt für Stadtgrün und Gewässer der Stadt Leipzig, die Pläne zur Anlage von Blühstreifen in Leipziger Parks vor.

Der auf Naturfilme spezialisierte Regisseur Jan Haft befasst sich in seinem neuesten Dokumentar-film mit der Vielfalt von Flora und Fauna auf deutschen Wildwiesen. Nirgendwo gibt es mehr Far-ben zu sehen, als auf einer blühenden Wiese im Sommer. Hier tummeln sich täglich die ver-schiedensten Arten von Vögeln, Insekten und anderen Tieren zwischen den Gräsern und Kräutern der Wiese. Diese Vielfalt macht die bunte, saftige Sommerwiese zu einer faszinierenden Welt, in der ein Drittel unserer heimischen Pflanzen- und Tierarten sein zuhause hat. Jan Haft ermöglicht mit hohem technischen Aufwand, nie da gewesene Bilder. Eindrucksvoller Dokfilm über die Bedeu-tung von Natur.

Deutschland 2018 von Jan Haft, 93 min., Dokumentarfilm