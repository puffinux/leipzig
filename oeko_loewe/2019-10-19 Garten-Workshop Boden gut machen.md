---
id: "693166467818644"
title: "Garten-Workshop: Boden gut machen"
start: 2019-10-19 10:00
end: 2019-10-19 13:00
locationName: Stadtgarten Connewitz
address: Kohrener Straße 4, 04277 Leipzig
link: https://www.facebook.com/events/693166467818644/
image: 71689146_10157643943982855_5769256812247252992_n.jpg
teaser: "Garten-Workshop: Boden gut machen Gesunder Boden - gesunde Pflanzen  Wusstet
  Ihr, dass in einer Handvoll gesunder Gartenerde mehr Lebewesen zuhause si"
isCrawled: true
---
Garten-Workshop: Boden gut machen
Gesunder Boden - gesunde Pflanzen

Wusstet Ihr, dass in einer Handvoll gesunder Gartenerde mehr Lebewesen zuhause sind, als Menschen auf der Erde leben? Auf einem derart lebendigen Boden gedeihen kräftige Pflanzen. In dem Workshop lernt Ihr den Zustand Eures Gartenbodens einzuschätzen und erfahrt, wie Ihr die Bodenfruchtbarkeit ökologisch fördert. Wir stellen Methoden zur sanften Bodenbearbeitung, Mulchen, Kompost und Gründung in Theorie und Praxis vor. Für eine grobe Beurteilung Eures Gartenbodens bringt gern ein großes Weckglas mit einer Bodenprobe mit.

Die Teilnahme ist kostenfrei. Das Angebot richtet sich insbesondere an Aktive in Leipziger Gemeinschaftsgartenprojekten, steht aber auch Klein- und HobbygärtnerInnen offen.

Weitere Infos zum Workshop sowie zur Anmeldung unter:
www.oekoloewe.de/naturnah-gaertnern.html

Der Workshop findet im Rahmen des Projektes "Naturnah Gärtnern - für Artenvielfalt in Leipzig" statt und wird von der Stadt Leipzig, Amt für Stadtgrün und Gewässer, gefördert.