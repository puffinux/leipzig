---
id: "2467099143508361"
title: "Lesung: Die schmutzigen Deals der deutschen Müllmafia"
start: 2019-12-18 19:00
end: 2019-12-18 21:00
locationName: Felsenkeller Leipzig
address: Karl-Heine-Straße 32, 04229 Leipzig
link: https://www.facebook.com/events/2467099143508361/
image: 80652149_2892384757459315_4521166039316168704_n.jpg
teaser: Am kommenden Mittwoch stellt Leipziger Zeitung und Leipziger Internet
  Zeitung-Journalist Michael Billig sein Buch "Schwarz. Rot. Müll." vor. Von
  illeg
isCrawled: true
---
Am kommenden Mittwoch stellt Leipziger Zeitung und Leipziger Internet Zeitung-Journalist Michael Billig sein Buch "Schwarz. Rot. Müll." vor. Von illegalen Mülldeponien, Netzwerken und Millionengewinnen der Müllmafia. Denn alles Konsumierte muss ja irgendwo hin.

Lesung und Dikussion mit Michael Billig
Mittwoch, 18.Dezember, 19:00
im Felsenkeller (kleine Saal NAUMANNS), Karl-Heine-Straße 32

Freier Eintritt