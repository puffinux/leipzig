---
id: '553811318421924'
title: 'Leipzig liest: Sie zielen auf mein Herz, damit ich falle'
start: '2019-03-22 19:00'
end: '2019-03-22 21:00'
locationName: Ökolöwe - Umweltbund Leipzig e.V.
address: 'Bernhard-Göring-Str. 152, 04277 Leipzig'
link: 'https://www.facebook.com/events/553811318421924/'
image: 52020622_10157076194647855_6934995345373921280_n.jpg
teaser: 'Zur Leipziger Buchmesse heißt es wieder „Leipzig liest“ und wir Ökolöwen sind mit dabei! Kommt zu uns in die Umweltbibliothek Leipzig, wenn Anke Woger'
recurring: null
isCrawled: true
---
Zur Leipziger Buchmesse heißt es wieder „Leipzig liest“ und wir Ökolöwen sind mit dabei! Kommt zu uns in die Umweltbibliothek Leipzig, wenn Anke Wogersien aus ihrem Wolfsroman 'Sie zielen auf mein Herz, damit ich falle' vorliest.

🐺 Um die Rückkehr der Wölfe in Deutschland werden z.T. erhitzte Debatten geführt. Anke Wogersien lässt „ihren“ Wolf in einer Kleinstadt im Harz auftauchen und zeigt auf, wie Naturschutz und Gesellschaft zusammenhängen. 

Erschienen ist der Wolfroman 2018 im Mitteldeutscher Verlag.

Da die Platzkapazitäten begrenzt sind, ist eine Anmeldung erforderlich. 
👉 Zur Anmeldung geht's hier: https://www.oekoloewe.de/events/leipzig-liest-wolfsroman.html

--
Die Umweltbibliothek Leipzig ist eine öffentliche Einrichtung des Ökolöwe - Umweltbund Leipzig e.V. und wird gefördert von der Stadt Leipzig, Amt für Umweltschutz.

Bilder © Mitteldeutscher Verlag