---
id: '331035727411526'
title: Newcomertreffen
start: '2019-05-14 15:00'
end: '2019-05-14 16:30'
locationName: Interim
address: 'Demmeringstraße 32, 04177 Leipzig'
link: 'https://www.facebook.com/events/331035727411526/'
image: 52970921_264118144518355_6897813379700752384_n.jpg
teaser: 'Beim Newcomertreffen kannst du Fragen zur Bewegung allgemein, zu unserem lokalem Kollektiv und unseren Projekten in lockerer Atmosphäre stellen.'
recurring: null
isCrawled: true
---
Beim Newcomertreffen kannst du Fragen zur Bewegung allgemein, zu unserem lokalem Kollektiv und unseren Projekten in lockerer Atmosphäre stellen. 