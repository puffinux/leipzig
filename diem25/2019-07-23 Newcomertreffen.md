---
id: '843222869395374'
title: Newcomertreffen
start: '2019-07-23 18:00'
end: '2019-07-23 19:00'
locationName: null
address: Leipzig
link: 'https://www.facebook.com/events/843222869395374/'
image: 66168898_343473236582845_2805489856271089664_n.jpg
teaser: Hier kannst du in lockerer Atmosphäre mit jemandem von uns deine Fragen über unser Kollektiv oder die Bewegung im Allgemeinen stellen oder uns und uns
recurring: null
isCrawled: true
---
Hier kannst du in lockerer Atmosphäre mit jemandem von uns deine Fragen über unser Kollektiv oder die Bewegung im Allgemeinen stellen oder uns und unsere Arbeit etwas kennen lernen.