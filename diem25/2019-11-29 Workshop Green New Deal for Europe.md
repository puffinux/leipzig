---
id: "573280913482712"
title: 'Workshop: "Green New Deal for Europe"'
start: 2019-11-29 19:00
end: 2019-12-01 13:00
locationName: Pizza LAB
address: Georg-Schwarz-Straße 10, 04177 Leipzig
link: https://www.facebook.com/events/573280913482712/
image: 75233453_427342011529300_4709135373219921920_n.jpg
teaser: "Die Umweltkrise ist schon seit Jahrzehnten für viele Menschen weltweit
  spürbar, rückt aber nun wieder ins Blickfeld der Öffentlichkeit: Die
  Behäbigkei"
isCrawled: true
---
Die Umweltkrise ist schon seit Jahrzehnten für viele Menschen weltweit spürbar, rückt aber nun wieder ins Blickfeld der Öffentlichkeit: Die Behäbigkeit der Politik Antworten auf die Krise zu finden, löst bei vielen Protestierenden aktuell große Empörung aus. Die Pläne der konservativen Politik mögen in Bezug auf die Umweltkrise aber schon längst gemacht sein: die eigenen zentralen Wirtschaftsbereiche gegenüber dem Ausland schützen und in einigen die grüne Technologieführerschaft übernehmen. Die progressive politische Seite ist weiter gespalten zwischen den Koordinaten Sparpolitik ja oder nein, Green New Deal oder Postwachstum, national oder europäisch. Green New Deal Kampagnen wie die der Parlamentsabgeordneten Alexandria Ocasio-Cortez in den USA oder des Labour-Vorsitzenden Jeremy Corbyn in Großbritannien konnten in Kontinentaleuropa bisher kaum Fuß fassen, bis die neue EU-Kommission zuletzt einen Green Deal für diese Regierungsperiode ausrief. In all diesen Plänen sollen Investitionen in grüne Infrastruktur nicht etwa durch Steuererhöhungen, sondern mithilfe von Geldschöpfung finanziert werden. Nach Jahrzehnten von Sparpolitik löst dieser Vorschlag Verwunderung auf vielen Seiten aus. Der Vortrag am Freitag kontextualisiert die Vorschläge eines Green (New) Deals im internationalen und insbesondere im europäischen wirtschaftlichen Umfeld, zeigt die Hintergründe der Geldschöpfung sowie mögliche Stoßrichtungen auf, die Idee des Deals mit gerechtigkeitsorientierten Inhalten zu füllen. Im anschließenden Workshop wird durch die Beschäftigung mit dem New Deal von 1933-38 in den USA eine konzeptionelle und historische Perspektive aufgemacht. Darauf aufbauend werden heutige Verhältnisse und Herausforderungen analysiert und Lösungsansätze sowie ihre Implementierung in den Green New Deal diskutiert. Im letzten Teil des Workshops überlegen wir gemeinsam, welche Handlungsperspektiven wir als Aktivist*innen konkret haben.

Für die Teilnahme am Workshop meldet euch bitte unter anmeldung@diem25-leipzig.de an.