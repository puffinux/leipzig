---
id: "466151474009614"
title: Newcomertreffen
start: 2019-12-10 18:30
end: 2019-12-10 19:00
locationName: Interim
address: Demmeringstraße 32, 04177 Leipzig
link: https://www.facebook.com/events/466151474009614/
image: 76998712_431834721080029_1336425936538566656_n.jpg
teaser: Hier kannst du in lockerer Atmosphäre mit jemandem von uns deine Fragen über
  unser Kollektiv oder die Bewegung im Allgemeinen stellen oder uns und uns
isCrawled: true
---
Hier kannst du in lockerer Atmosphäre mit jemandem von uns deine Fragen über unser Kollektiv oder die Bewegung im Allgemeinen stellen oder uns und unsere Arbeit etwas kennen lernen.