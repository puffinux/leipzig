---
id: "2502360009859352"
title: Ein Green New Deal for Europe als progressiver politischer Plan
start: 2019-11-29 19:00
end: 2019-11-29 21:00
locationName: Interim
address: Demmeringstraße 32, 04177 Leipzig
link: https://www.facebook.com/events/2502360009859352/
image: 76613114_426560068274161_2371616911478751232_n.jpg
teaser: "Die Umweltkrise ist schon seit Jahrzehnten für viele Menschen weltweit
  spürbar, rückt aber nun wieder ins Blickfeld der Öffentlichkeit: Die
  Behäbigkei"
isCrawled: true
---
Die Umweltkrise ist schon seit Jahrzehnten für viele Menschen weltweit spürbar, rückt aber nun wieder ins Blickfeld der Öffentlichkeit: Die Behäbigkeit der Politik Antworten auf die Krise zu finden, löst bei vielen Protestierenden aktuell große Empörung aus. Die Pläne der konservativen Politik mögen in Bezug auf die Umweltkrise aber schon längst gemacht sein: die eigenen zentralen Wirtschaftsbereiche gegenüber dem Ausland schützen und in einigen die grüne Technologieführerschaft übernehmen. Die progressive politische Seite ist weiter gespalten zwischen den Koordinaten Sparpolitik ja oder nein, Green New Deal oder Postwachstum, national oder europäisch. Green New Deal Kampagnen wie die der Parlamentsabgeordneten Alexandria Ocasio-Cortez in den USA oder des Labour-Vorsitzenden Jeremy Corbyn in Großbritannien konnten in Kontinentaleuropa bisher kaum Fuß fassen, bis die neue EU-Kommission zuletzt einen Green Deal für diese Regierungsperiode ausrief. In all diesen Plänen sollen Investitionen in grüne Infrastruktur nicht etwa durch Steuererhöhungen, sondern mithilfe von Geldschöpfung finanziert werden. Nach Jahrzehnten von Sparpolitik löst dieser Vorschlag Verwunderung auf vielen Seiten aus. Dieser Vortrag kontextualisiert die Vorschläge eines Green (New) Deals im internationalen und insbesondere im europäischen wirtschaftlichen Umfeld, zeigt die Hintergründe der Geldschöpfung sowie mögliche Stoßrichtungen auf, die Idee des Deals mit gerechtigkeitsorientierten Inhalten zu füllen.