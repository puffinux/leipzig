---
id: '429108414582855'
title: Ohne uns steht die Welt still
start: '2019-03-06 19:00'
end: '2019-03-06 22:00'
locationName: Interim
address: 'Demmeringstraße 32, 04177 Leipzig'
link: 'https://www.facebook.com/events/429108414582855/'
image: 52602538_262734054656764_3749427885866221568_n.jpg
teaser: Sin nosotras se para el mundo.   Arbeitskämpfe und Organisation von Hausangestellten am Beispiel Spaniens.  Vortrag und Diskussion mit  Virginia Kimey
recurring: null
isCrawled: true
---
Sin nosotras se para el mundo. 

Arbeitskämpfe und Organisation von Hausangestellten am Beispiel Spaniens.

Vortrag und Diskussion mit  Virginia Kimey Pflücke.

Haushaltsarbeit kann als eine der ältesten Formen des Broterwerbs gesehen werden, hat aber in ihrer heutigen Form als Niedriglohnerwerb auch Wurzeln in der Sklaverei und Leibeigenschaft. Im Zuge der Industrialisierung stellten auch in vielen europäischen Ländern Hausangestellte weiterhin die größte Gruppe an Arbeiter*innen überhaupt. Gleichzeitig wurde der Sektor immer ausschließlicher zur weiblichen Lohnarbeit in einer geschlechtlich organisierten Produktionsweise. Die lange Geschichte der Ausgrenzung von Hausangestellten aus dem Arbeitsrecht und aus gewerkschaftlichen Organisationen in Spanien ist Gegenstand dieses Vortrags: Ausgehend von ihrer anarchistischen Organisierung Anfang der 1930er Jahre, über den Versuch einer Inklusion in die Klassengewerkschaften nach dem Ende des Franquismus bis hin zur heutigen feministisch-marxistischen Selbstorganisierung werden Schlaglichter in die Arbeitskämpfe von Hausangestellten und ihre Organisierung in- und außerhalb der Gewerkschaften geworfen. Dabei wird uns die Frage begleiten, was wir für eine feministische (Selbst-)Organisierung am Rande der Dienstleistungsgesellschaft heute lernen können.