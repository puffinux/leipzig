---
id: '2739651626259852'
title: Stammtisch
start: '2019-05-03 20:00'
end: '2019-05-03 23:00'
locationName: Frau Krause
address: 'Simildenstraße 8, 04277 Leipzig'
link: 'https://www.facebook.com/events/2739651626259852/'
image: 47025206_205969240333246_7866405685511061504_n.jpg
teaser: An jedem ersten Freitag des Monats treffen treffen wir uns zum Stammtisch im Frau Krause. Sollten zum gleichen Zeitpunkt andere Veranstaltungen stattf
recurring: null
isCrawled: true
---
An jedem ersten Freitag des Monats treffen treffen wir uns zum Stammtisch im Frau Krause. Sollten zum gleichen Zeitpunkt andere Veranstaltungen stattfinden, wird dort der Ort des Stammtisches genannt.