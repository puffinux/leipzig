---
id: '2039138919514747'
title: Solidarische Klassenperspektive statt nationaler Spaltung
start: '2019-03-16 19:00'
end: '2019-03-16 22:00'
locationName: Interim
address: 'Demmeringstraße 32, 04177 Leipzig'
link: 'https://www.facebook.com/events/2039138919514747/'
image: 51868854_255091538754349_1329233859182592000_n.jpg
teaser: Solidarische Klassenperspektive statt nationaler Spaltung  Intervention und Diskussion mit Kalle Kunkel (Gewerkschaftssekräter einer DGB Gewerkschaft)
recurring: null
isCrawled: true
---
Solidarische Klassenperspektive statt nationaler Spaltung

Intervention und Diskussion mit Kalle Kunkel (Gewerkschaftssekräter einer DGB Gewerkschaft)

16. März 2019 | 19:30 Uhr | Interim (Demmeringstr. 32 Leipzig)

Die Migrationspolitik hat sich zu einem der konfliktrechsten Themen innerhalb der Linken und der LINKEN entwickelt. „Migrationsskeptische“ Positionen warnen vor eine Konkurrenz von MigrantInnen mit den ärmeren und prekären Teilen der Bevölkerung und treten deshalb aus einer vermeintlich linken Klassenperspektive für eine starke Begrenzung von Migration ein, die ein Bleiberecht in Deutschland an Bedrohungen im Sinne des Asylrechts und der Genfer Konvention bindet. Diese Forderung würde angesichts der Verschärfungen des Asylrechts in den letzten Jahren eine Verbesserung zum Status Quo bringen. Die Forderung nach Beschränkung von Migration ist jedoch als solidarische Klassenperspektive unbrauchbar. Sie erkennt die Realitäten von Migration nicht an und fördert – entgegen der eigenen Selbstdarstellung – die Spaltung der abhängig Beschäftigten.

Nach einem etwa halbstündigen Input  wollen wir an diesem Abend Fragen nach einer solidarischen Klassenperspektive diskutieren und überlegen, inwiefern ein transnationaler gewerkschaftlicher und politischer Ansatz hierzu einen Beitrag leisten kann.

*** Bei Schlafplatzbedarf bitte rechtzeitig anfragen! ***
*** https://diem25-leipzig.de/contact/ ***