---
id: '1090043884528764'
title: Gegen Antisemitismus streiten
start: '2019-06-28 18:00'
end: '2019-06-30 18:00'
locationName: Interim
address: 'Demmeringstraße 32, 04177 Leipzig'
link: 'https://www.facebook.com/events/1090043884528764/'
image: 59189091_300314070898762_6259241902739030016_n.jpg
teaser: 'Workshop für politische Aktivist*innen und andere Multiplikator*innen  28.-30. Juni 2019 im Interim Demmeringstraße 32, 04177 Leipzig  Die deutliche Z'
recurring: null
isCrawled: true
---
Workshop für politische Aktivist*innen und andere Multiplikator*innen

28.-30. Juni 2019 im Interim
Demmeringstraße 32, 04177 Leipzig

Die deutliche Zunahme manifest-antisemitischer (Straf-)Taten in den letzten Jahren zwingt zu einer politischen Auseinandersetzung mit diesem gesamtgesellschaftlichen Phänomen, das in Europa und darüber hinaus wieder eine zunehmende Rolle als verschwörungsideologische Welterklärung spielt. Diese Welterklärung macht dabei keineswegs vor politischen Grenzen halt: So lassen sich in antisemitischen Äußerungen oder Handlungen oft völkisch-rassistische Motive erkennen. Aber auch christlich oder islamisch religiöse, sowie antizionistisch-antiimperialistische Argumentationsmuster spielen eine Rolle. Oftmals weisen die verschiedenen Motive und Argumentationsmuster starke inhaltliche Überschneidungen auf und sind in der sogenannten gesellschaftlichen Mitte ebenso Zuhause, wie an den vermeintlichen politischen Rändern.

Deshalb müssen zivilgesellschaftliche und andere politische Akteur*innen in die Lage versetzt werden, die verschiedenen Erscheinungsformen des Antisemitismus identifizieren, erkennen und organisations- und verbandsintern effektiv bekämpfen zu können. Hierfür braucht es einerseits eine inhaltliche Sensibilität für tradierte antisemitische Motive. Andererseits bedarf es ebenso einer verbindlichen Beschlusslage, in der geregelt ist, was die Mitglieder der jeweiligen Organisationen unter Antisemitismus verstehen. Der Workshop zielt darauf politische Aktivist*innen und andere Multiplikator*innen in Organisationen und Parteien für die Funktions- und Artikulationsweise des Antisemitismus zu sensibilisieren.

Konkret besteht der Workshop aus drei Teilen: Am Freitag Abend nähern sich die Teilnehmer*innen anhand einer Dokumentation dem Thema Antisemitismus auch mit Blick auf Perspektiven von Betroffenen an. 
Am Samstag steht die gemeinsame Lektüre und Diskussion zweier einführender Texte zu Geschichte und Motiven des klassischen Antisemitismus im Zentrum. 
Am Sonntag werden theoretische Zugänge zur politischen Einordnung des Phänomens vorgestellt. Ausgehend von dieser Auseinandersetzung wird am Beispiel der Antisemitismusdefinition der International Holocaust Remembrance Alliance gezeigt, wie eine politische Arbeitsdefinition für zivilgesellschaftliche Initiativen und politische Parteien aussehen kann.

bei Bedarf versuchen wir Schlafplätze zur Verfügung zu stellen.
