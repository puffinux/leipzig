---
id: "843222889395372"
title: Newcomertreffen
start: 2019-10-15 18:30
end: 2019-10-15 19:00
address: Leipzig
link: https://www.facebook.com/events/843222889395372/
image: 66168898_343473236582845_2805489856271089664_n.jpg
teaser: Hier kannst du in lockerer Atmosphäre mit jemandem von uns deine Fragen über
  unser Kollektiv oder die Bewegung im Allgemeinen stellen oder uns und uns
isCrawled: true
---
Hier kannst du in lockerer Atmosphäre mit jemandem von uns deine Fragen über unser Kollektiv oder die Bewegung im Allgemeinen stellen oder uns und unsere Arbeit etwas kennen lernen.