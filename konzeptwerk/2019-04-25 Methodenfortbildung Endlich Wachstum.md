---
id: '1258285264348015'
title: Methodenfortbildung "Endlich Wachstum"
start: '2019-04-25 16:00'
end: '2019-04-25 19:00'
locationName: null
address: 'Landesinstitut für Lehrerbildung und Schulentwicklung, Schulmuseum, Seilerstraße 42, 20359 Hamburg'
link: 'https://www.facebook.com/events/1258285264348015/'
image: 57128371_2031853470260346_6709793128959705088_o.jpg
teaser: Wirtschaftswachstum ist das vorherrschende Prinzip der Zukunftssicherung in unserer Gesellschaft. Es prägt maßgeblich unser Denken und Handeln. Dabei
recurring: null
isCrawled: true
---
Wirtschaftswachstum ist das vorherrschende Prinzip der Zukunftssicherung in unserer Gesellschaft. Es prägt maßgeblich unser Denken und Handeln. Dabei durchdringt der materielle Konsum viele unserer Lebensbereiche. Doch es sind durchaus Zweifel angebracht, ob diese Verbindung von Wachstum, Wohlstand und Glück so richtig ist. Zumal immer deutlicher die Auswirkungen von sozialen Ungerechtigkeiten, Klimawandel und zur Neige gehenden Ressourcen in Erscheinung treten.

In dieser Fortbildung werden entlang des Methodenhefts „Endlich Wachstum!“ innovative Methoden vorgestellt und erprobt, die eine abwechslungsreiche und kritische Auseinandersetzung mit Wirtschaft, Wachstum & Postwachstumsgesellschaft mit Schüler*innen ab 15 Jahren ermöglichen.

Mehr Infos und Anmeldung unter https://tis.li-hamburg.de/web/guest/catalog/detail?tspi=48512_