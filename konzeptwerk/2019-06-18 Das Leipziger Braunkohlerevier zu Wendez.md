---
id: '869061356804716'
title: Das Leipziger Braunkohlerevier zu Wendezeiten
start: '2019-06-18 19:00'
end: '2019-06-18 22:00'
locationName: null
address: 'Bürgerhaus Goldener Stern, am Markt 11'
link: 'https://www.facebook.com/events/869061356804716/'
image: 61864860_2116619581783734_7921980663945756672_n.jpg
teaser: Unsere zweite Veranstaltung  dreht sich um den Widerstand gegen den Braunkohleabbau im Leipziger Revier zu Wendezeiten.  In den 80er Jahren begannen e
recurring: null
isCrawled: true
---
Unsere zweite Veranstaltung  dreht sich um den Widerstand gegen den Braunkohleabbau im Leipziger Revier zu Wendezeiten.  In den 80er Jahren begannen einige BürgerInnen in der Region, sich gegen die Umweltbelastung durch die Braunkohleindustrie zu organisieren. Sie legten die Grundsteine für die größte Umwelt-Demonstration der DDR, am 02. April 1990,  wohin mehr als 10.000 Menschen Rande des Tagebaus Cospuden mobilisiert werden konnten. Sie protestierten gegen die Zerstörung weiterer Dörfer, gegen die Ausweitung des Tagebaus und gegen die Umweltschäden, die für die Anwohnenden deutlich sicht- und spürbar waren. 

Auf der Veranstaltung werden wir zwei Zeitzeugen zu Gast haben, die an den Protesten von „Stop Cospuden 90“ beteiligt waren und von den Schwierigkeiten und Erfolgen des Widerstands berichten werden. Vor welchen Herausforderungen standen sie? Wieso war der Protest erfolgreich? Welche Rolle spielte der politische Umbruch? Und was können wir noch heute aus dieser Zeit lernen? Diesen Frage gehen wir nach. Zur Veranstaltung sind insbesondere Menschen eingeladen, deren Leben durch die Braunkohle geprägt wurde oder wird. Es wird reichlich Möglichkeiten zum Austausch geben.

Sprecher: 
1. Thomas Thiel 
2. Wolfram Herwig (beide Stop Cospuden 90)

Moderation: Felix Wittmann, Josephine Kellert