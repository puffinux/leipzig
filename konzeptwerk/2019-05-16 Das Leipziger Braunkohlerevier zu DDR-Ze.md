---
id: '296926764571306'
title: Das Leipziger Braunkohlerevier zu DDR-Zeiten
start: '2019-05-16 17:00'
end: '2019-05-16 20:00'
locationName: Kulturhaus Böhlen
address: 'Leipziger Str. 40, 04564 Böhlen, Sachsen'
link: 'https://www.facebook.com/events/296926764571306/'
image: 60128175_2073692772743082_4939918298339868672_n.jpg
teaser: Seit der Industrialisierung prägt der Abbau der Braunkohle die mitteldeutsche Region. Im Jahr 2038 soll jetzt spätestens Schluss sein mit der Braunkoh
recurring: null
isCrawled: true
---
Seit der Industrialisierung prägt der Abbau der Braunkohle die mitteldeutsche Region. Im Jahr 2038 soll jetzt spätestens Schluss sein mit der Braunkohle – die letzten Schritte eines jahrhundertelangen Strukturwandels stehen bevor. Auf dieser Veranstaltung wollen wir einen Blick zurück werfen, auf die Geschichte der Region und ihrer Menschen zuzeiten der DDR. Wie hat die Braunkohle die Region verändert und die Schicksale der Anwohnenden geprägt? Dieser Frage gehen wir nach. Zu dieser Veranstaltung sind insbesondere Menschen eingeladen, deren Leben durch die Braunkohle geprägt wurde oder wird. Es wird reichlich Möglichkeiten für Austausch geben.

SprecherInnen: 
1. E. Landgraf, Neukieritzsch, Umsiedler aus Breunsdorf
2. Gisela Kallenbach, Leipzig, in den 1980er Jahren aktiv in der AG Umweltschutz, ehem. Abgeordnete für die Grünen im Landtag und Europaparlament

Moderation: Felix Wittmann, Josephine Kellert - Konzeptwerk Neue Ökonomie

Im Rahmen der anstehenden Strukturwandeldebatten im Mitteldeutschen Braunkohlerevier führt das Konzeptwerk Neue Ökonomie 2019-2020 eine Reihe von insgesamt acht Veranstaltungen im Leipziger Umland durch. 
Diese wird sich zu Beginn mit Umsiedlungserfahrungen aus der Vergangenheit befassen und  anschließend einen Blick auf die Gegenwart und die globale Perspektive richten. Ab der fünften Veranstaltung wollen wir uns intensiv mit den Herausforderungen und Möglichkeiten eines sozialen, ökologischen und demokratischen Strukturwandels befassen.