---
id: '593608291117682'
title: Was landet auf meinem Teller? Bildungsurlaub zur Landwirtschaft
start: '2019-07-08 11:00'
end: '2019-07-12 14:00'
locationName: Akademie Frankenwarte
address: 'Leutfresserweg 81-83, 97082 Würzburg'
link: 'https://www.facebook.com/events/593608291117682/'
image: 57602824_2035849193194107_8342925194536419328_n.jpg
teaser: 'Bildungsurlaub der Akademie Frankenwarte Würzburg, mitgeleitet von Susanne Brehm vom Konzeptwerk  Die Produktion unserer Lebensmittel gestaltet sich m'
recurring: null
isCrawled: true
---
Bildungsurlaub der Akademie Frankenwarte Würzburg, mitgeleitet von Susanne Brehm vom Konzeptwerk

Die Produktion unserer Lebensmittel gestaltet sich momentan weder ökologisch noch sozial verträglich. Zwar wird sehr viel produziert, trotzdem fehlt weltweit immer noch vielen Menschen ein ausreichender Zugang zu gesunden Lebensmitteln. Darüber hinaus trägt die Landwirtschaft stark zum Klimawandel und zum Verlust der Artenvielfalt bei und gefährdet so ihre eigenen Grundlagen. Doch es tut sich bereits viel in diesem Bereich: ökologische und solidarische Formen der Landwirtschaft verbreiten sich immer mehr und auch in den Städten entstehen viele Initiativen, die sich für eine gesunde hochwertige Ernährung und eine nachhaltige Landwirtschaft in den Regionen einsetzen. Im Seminar diskutieren wir die zentralen Probleme, lernen verschiedene nachhaltige Lösungsansätze kennen und besprechen deren Umsetzungsmöglichkeiten. Zudem besuchen wir Produzent_innen in der Region und sprechen mit Engagierten, die bereits an einer sozial-ökologischen Agrarwende arbeiten.

Anerkannt/anerkennungsfähig als Bildungsurlaub in den Bundesländern Baden-Württemberg, Berlin, Brandenburg, Bremen, Hamburg, Hessen, Niedersachsen, Nordrhein-Westfalen, Rheinland-Pfalz, Schleswig-Holstein und Thüringen auf Anfrage.

Weitere Infos und Anmeldung unter https://www.frankenwarte.de/unser-bildungsangebot/veranstaltung.html?id=870