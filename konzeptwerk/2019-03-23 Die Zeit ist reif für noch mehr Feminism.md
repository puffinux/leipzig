---
id: '1009464159258655'
title: Die Zeit ist reif für noch mehr Feminismus!
start: '2019-03-23 11:00'
end: '2019-03-23 17:00'
locationName: null
address: 'Karl-Wolf-Saal im Gewerkschaftshaus Essen, Teichstraße 4, 45127 Essen'
link: 'https://www.facebook.com/events/1009464159258655/'
image: 52856699_10157050610704120_6602643925536604160_n.jpg
teaser: 'The time is ripe for EVEN MORE feminism!! [ENGLISH BELOW]  Um Anmeldung wird gebeten – bitte schreibt an femfest@rosalux.org  Liebe Feminist*innen, Fr'
recurring: null
isCrawled: true
---
The time is ripe for EVEN MORE feminism!! [ENGLISH BELOW]

Um Anmeldung wird gebeten – bitte schreibt an femfest@rosalux.org

Liebe Feminist*innen, Freund*innen, Genoss*innen!

Das Netzwerk-Care-Revolution, das Konzeptwerk Neue Ökonomie und die Rosa-Luxemburg-Stiftung planen zusammen mit anderen Initiativen und Gruppen für September 2019 ein großes internationales „Feministisches Festival“, vom 12. Bis zum 15. September, voraussichtlich in der Zeche Zollverein. Das erste Vorbereitungstreffen hierfür fand am 26. Januar in Berlin statt. Ca. 30 Aktive verschiedener Initiativen haben einen ganzen Tag lang kreativ Ideen gesponnen und die Eckpfeiler für ein erfolgreiches Festival diskutiert:

Wir rechnen mit ca. 1000 Teilnehmenden.
Es ist eine Vielzahl an unterschiedlichen Formaten geplant: Performances, Podien, Workshops, Skill- Sharing, Räume für Vernetzung, Kunst- und Kulturbeiträge

Das Frauen*streik Bündnis  und viele andere Akteur*innen sozialer Bewegungen werden dabei sein.

Das Festival soll ein internationaler Vernetzungsort sein, mit dem wir eine feministische Bewegung stärken wollen, die sich gegen globalen, autoritären Neoliberalismus und Rechtspopulismus stellt und für ein gutes Leben für alle kämpft.

Anstelle eines vorausgesetzten feministischen Wir sind soziale Unterschiede der Ausgangspunkt für die gemeinsame Arbeit hin zu einem feministischen Wir.

Die Visionen und Ansprüche für dieses Festival sollen dabei die Vielfalt verschiedener Feminismen und Formen politischer Organisierung widerspiegeln.

Wir laden alle feministisch Interessierten und Engagierten herzlich ein, mitzumachen und sich einzubringen! Das nächste offene Orga – Kreis Treffen zum Kennenlernen und Mitmachen findet am 23. März in Essen stattfinden. Um Anmeldung wird gebeten – bitte schreibt an femfest@rosalux.org

Für Übersetzung und Kinderbetreuung/-begleitung wird gesorgt – bitte gebt uns Rückmeldung, falls gebraucht. Die Reisekosten können erstattet werden, insofern notwendig. Bitte auch hierfür schon vorher gern an femfest@rosalux.org schreiben.

Für die Vorbereitung soll es Arbeitsgruppen geben, mit denen Ihr auch gerne schon direkt Kontakt aufnehmen könnt: 

    AG Kultur und Kunst (Kontakt: femfest@rosalux.org)
    AG Die ganze Arbeit (Kontakt: m.korsonewski@knoe.org)AG
    AG feministische Internationale & Postkolonialismus (Kontakt: barbara.fried@rosalux.org)
    AG Reproduktive Gerechtigkeit , Familienpolitiken, Care (Kontakt: c.hitzfelder@knoe.org)
    AG Körperpolitiken & Gewaltverhältnisse (Kontakt: femfest@rosalux.org)
    AG Sozial-Ökolog. Transformation, feminist. Ökologie (Kontakt: m.smettan@knoe.org)
    AG Infrastruktur (Kontakt: rebecca.wandke@rosalux.org)
    AG Öffentlichkeitsarbeit (Kontakt: femfest@rosalux.org)

Zur Idee des Festivals

Feministische Kämpfe waren in den letzten Jahren die kraftvollsten Bewegungen, die für weitreichende gesellschaftliche Veränderungen und eine bessere Zukunft für alle eingetreten sind. Über Ländergrenzen hinweg ist es ihnen gelungen, gegen sexuelle Gewalt genauso zu protestieren, wie gegen prekäre Arbeitsverhältnisse, gegen Diskriminierung genauso wie für reproduktive Gerechtigkeit, für einen gleichberechtigten Zugang zu Gesundheitsversorgung und sozialen Infrastrukturen wie für die Aufwertung von Care-Arbeit. Sie treten lautstark und vielfältig in Erscheinung und markieren eine Alternative zu autoritärem Neoliberalismus und globalem Rechtspopulismus.

Die Proteste der letzten Jahre haben Beschränkungen eines liberalen Feminismus überschritten, indem sie eine grundlegende Kritik an kapitalistischen Verhältnissen üben, und haben es gleichzeitig geschafft, damit Hunderttausende anzusprechen, die bisher nicht links aktiv waren. Im Zuge der Anti-Trump Proteste in den USA ist einmal mehr klar geworden, dass ein linker Feminismus bedingungslos antirassistisch zu sein hat. Und auch hiesige Versuche, vermeintliche „Frauen*rechte“ für rechte und chauvinistische Propaganda zu instrumentalisieren, zeigen wie notwendig dies ist, um den Rechten etwas entgegen setzen zu können. Aber auch die Unterschiede zwischen Frauen*, Queers* und Trans* müssen Ausgangspunkt unserer gemeinsamen Kämpfe sein. Die Geschichte der Frauenbewegungen hat uns gelehrt, dass wir andernfalls gesellschaftliche Marginalisierung re-produzieren. In den aktuellen Protestbewegungen werden erste Schritte gegangen, die wir zu einem wirklich intersektionalen Feminismus, zu einer gemeinsamen Praxis entwickeln können, einer Praxis, die Einheit in Differenz möglich macht. Dafür brauchen wir Orte für gemeinsame Debatten und gegenseitiges voneinander Lernen.

Um einen solchen Raum zu schaffen, wollen wir im Herbst zu einem internationalen und pluralen feministischen Festival einladen. Eine solche Zusammenkunft ist nur so kraftvoll wie ihre Akteur*innen: Deshalb wollen wir von Anfang an viele einladen, sich an der Planung zu beteiligen.

So können sich Initiativen und Einzelpersonen in ganz unterschiedlicher Intensität einbringen, Ideen verfolgen, Räume gestalten, Referent*innen vorschlagen oder auch nur mitdiskutieren können. Bei dem Festival soll viel und in vielen Sprachen diskutiert werden. Aber wir wollen nicht nur sprechen. Es soll Raum und Zeit geben für Trainings und Skill-Sharings, wir wollen an ältere feministische Praxen der ‚Selbsterfahrung’ anschließen und über feministische Gesundheit genauso lernen, wie über Transformative Organizing. Auch unterschiedliche Formen künstlerischer und kultureller Beiträge sollen zentraler Bestandteil des Programms sein – wir wollen gemeinsam Filme sehen und darüber diskutieren, es wird Bühnen geben für Musik, wie für gesprochenes Wort und: wir wollen zusammen tanzen!

[ ENGLISH ]

Dear feminists*, friends*, interested folks!

The Care Revolution network, the Rosa Luxemburg Foundation and the Konzeptwerk Neue Ökonomie are joined by many initiative groups to organize a diverse and pluralistic feminist festival from September 12 - 15, 2019, expected to take place at the Zeche Zollverein in Essen. A first preparatory meeting took place on 26 January in Berlin. About 30 active groups spent a day brainstorming ideas and discussed the cornerstones of a successful festival:

    Approx. 1000 participants are expected.
    A large number of different formats are planned: Performances, podiums, workshops, skill sharing, spaces for networking, art and culture contributions.
    The Frauen*Streik Bündnis (Women* Strike Alliance) and many other actors* of social movements will be present.
    The festival should be a space for international networking with which we want to strengthen a feminist movement that opposes global authoritarian neo-liberalism and right-wing populism and fights for a good life for all.
    Instead of a presupposed feminist We, social differences are the starting point for the common work towards a feminist We.
    The visions and demands for this festival should reflect the diversity of different feminisms and forms of political organization

Get involved

The preparation for the festival needs support, and the involvement of many volunteers for topics and design is fundamental. Your ideas and skills are needed and welcome.  The next open organizational meeting will take place on March 23rd in Essen. Everyone is invited. Registration is requested. Please write to femfest@rosalux.org

Translation and childcare will be provided - please give us feedback if needed. Travel expenses can be reimbursed if necessary.

For the further preparation a division into working groups (AG) is proposed. The following AGs have already been founded. All AGs are open for participation:

    AG Culture and Art (Contact: femfest@rosalux.org)
    AG The entirety of work (Contact: m.korsonewski@knoe.org)
    AG Reproductive Justice , Family Policies, Care (Contact: c.hitzfelder@knoe.org)
    AG Transnational Feminism & Postcolonialism (Contact: barbara.fried@rosalux.org)
    AG Body-politics & Violence-relations (Contact: femfest@rosalux.org)
    AG Social-Ecolog. Transformation, feminist. Ecology (Contact: m.smettan@knoe.org)
    AG Infrastructure (Contact: rebecca.wandke@rosalux.org)
    AG Public Relations (Contact: femfest@rosalux.org)

Please contact us if you want to work with us or if you have any questions

Website:
https://nrw.rosalux.de/veranstaltung/es_detail/GEIDO/die-zeit-ist-reif-fuer-noch-mehr-feminismus/

Bild/Picture: Tim Löddemann, flickr, CC BY-NC-SA 2.0