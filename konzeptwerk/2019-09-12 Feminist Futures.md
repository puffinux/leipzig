---
id: '421719015043871'
title: Feminist Futures
start: '2019-09-12 10:00'
end: '2019-09-15 18:00'
locationName: Zeche Zollverein
address: 'Gelsenkirchener Straße 181, 45309 Essen'
link: 'https://www.facebook.com/events/421719015043871/'
image: 61203249_10157278110214120_3119556822940254208_n.jpg
teaser: 'DATUM VORMERKEN! Internationales Festival «FEMINIST FUTURES»  12.-15.9.2019 Zeche Zollverein, Essen  Weltweit gewinnen feministische Bewegungen an Stä'
recurring: null
isCrawled: true
---
DATUM VORMERKEN!
Internationales Festival «FEMINIST FUTURES»

12.-15.9.2019 Zeche Zollverein, Essen

Weltweit gewinnen feministische Bewegungen an Stärke und schlagen einen immer radikaleren Kurs ein: Lautstark und vielfältig stellen sie sich dem neoliberalen Ausverkauf des Gesundheitssystems und schlechten Arbeitsbedingungen entgegen. Sie kämpfen gegen sexuelle Gewalt, rassistische Ausgrenzungen und die Zerstörung der natürlichen Umwelt. Sie treten für soziale Gerechtigkeit als Grundlage für Selbstbestimmung über ihren Körper und ihr Leben ein. Damit gehören sie zu den wichtigsten Gegner*innen eines globalen Rechtspopulismus und bauen an einer besseren Zukunft für alle!

Es ist also alles in Bewegung: Feminist*innen mit verschiedenen Erfahrungen und Hintergründen kommen wieder oder das erste Mal zusammen. Es entstehen Handlungsweisen, die auf etwas Gemeinsames orientieren ohne Unterschiede zu verschweigen. Somit kann eine Einheit in der Differenz möglich werden. Sie verbinden feministische und queer-feministische Anliegen mit konsequenter Kapitalismuskritik und Klassenpolitik. Auch wir wollen in dieser Richtung weiter kommen. Um die Bewegungen zu stärken und weiterzuentwickeln, brauchen wir Orte für Debatten, um voneinander zu lernen.

Einen solchen Ort möchten die Rosa-Luxemburg-Stiftung, das Netzwerk Care Revolution und das Konzeptwerk Neue Ökonomie mit einem internationalen Festival im September bieten. Die Vorbereitung gestalten viele lokale und überregionale Unterstützer*innen aktiv und vielfältig mit. Es soll Podien und Workshops für theoretische ebenso wie praxisnahe Fragen geben. Es gibt Zeit für Trainings und Gesprächsrunden. Wir wollen an ältere feministische Praxen der ‚Selbsterfahrung‘ anschließen und über feministische Gesundheit genauso lernen, wie über Transformatives Organizing. Auch unterschiedliche Formen künstlerischer und kultureller Beiträge werden zentraler Bestandteil des Programms sein. Wir wollen gemeinsam Filme schauen und darüber diskutieren, es wird Bühnen geben für Musik, wie für gesprochenes Wort, Theater und Performances und: wir wollen zusammen tanzen!

Das wollt Ihr nicht verpassen! Tragt Euch den Termin also schon einmal in den Kalender ein. Nähere Infos folgen ganz bald unter: www.feministfutures.de

Das Festival ist kostenlos und die Verpflegung auf Spendenbasis. Außerdem bemühen wir uns um günstige Anreise- und Unterkunftsmöglichkeiten. Außerdem organisieren wir Kinderbetreuung.

Das Festival ist offen für alle Geschlechter. Es wird am Samstag einen räumlich getrennten Bereich für Frauen, Lesben, Trans, Inter und Queer geben. 

Wir freuen uns auf euch - bei Fragen oder Ideen meldet euch gerne bei uns: femfest@rosalux.org

++++++++++++++++++++++++++++++++++++++++++++++++

SAFE THE DATE:
International Festival «FEMINIST FUTURES»

12.-15.9.2019 Zeche Zollverein, Essen, Germany

Feminist movements around the world are gaining strength and taking an increasingly radical course: They are loud and diverse in their opposition to the neoliberal sell-out of the health system and poor working conditions. They fight against sexual violence, racist exclusion and the destruction of the natural environment. They advocate social justice as the basis for self-determination over their bodies and lives. Thus, they belong to the most important opponents of a global right-wing populism and build a better future for all!

Everything is in motion: Feminists* with different experiences and backgrounds come together again or for the first time. Actions emerge that focus on something common without concealing differences. Thus, a unity in difference becomes possible. They combine feminist and queer-feminist concerns with consistent critique of capitalism and class politics. We too want to make progress in this direction. In order to strengthen and further develop the movements, we need spaces for debates in order to learn from each other.

The Rosa Luxemburg Foundation, the Care Revolution network and the Konzeptwerk Neue Ökonomie would like to offer such a place with an international festival in September. Many supporters* are actively involved in the preparation of the festival in a variety of ways. There will be penals and workshops for theoretical as well as practical questions. There will be time for training sessions and discussion rounds. We want to connect with older feminist practices of 'self-awareness' and learn about feminist health as well as about Transformative Organizing. Different forms of artistic and cultural contributions will also be a central component of the program. We want to watch and discuss films together, there will be stages for music, spoken word, theatre and performances and: we want to dance together!

You don't want to miss this! So mark the date in your calendar. More information will follow soon at: www.feministfutures.de

The festival is free of charge and the catering is based on donations. In addition, we make every effort to provide inexpensive travel and accommodation. We also organise childcare.

The festival is open to all gender. On Saturday there will be a separate area for women, lesbians, trans, inter and queer.

We are looking forward to hearing from you - if you have any questions or ideas please contact us: femfest@rosalux.org
