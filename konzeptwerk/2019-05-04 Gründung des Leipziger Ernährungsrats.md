---
id: '2035211633448017'
title: Gründung des Leipziger Ernährungsrats
start: '2019-05-04 10:30'
end: '2019-05-04 16:00'
locationName: Volkshochschule Leipzig
address: 'Löhrstr. 3-7, 04105 Leipzig'
link: 'https://www.facebook.com/events/2035211633448017/'
image: 57726553_2037670309678662_6071801111805689856_n.jpg
teaser: 'Der Ernährungsrat Leipzig gründet sich und lädt ein zu  „Blitzlichter des Leipziger Ernährungssystem“  Samstag, 04. Mai 2019 10.30 - 16.00 Uhr Volksho'
recurring: null
isCrawled: true
---
Der Ernährungsrat Leipzig gründet sich und lädt ein zu 
„Blitzlichter des Leipziger Ernährungssystem“

Samstag, 04. Mai 2019
10.30 - 16.00 Uhr
Volkshochschule Leipzig, Löhrstraße 3 - 7

Im Februar 2018 fand das Forum „Gutes Essen für alle!“ in der Volkshochschule Leipzig großen Anklang und startete die Initiative für einen Ernährungsrat. Nach einem guten Jahr der Vorbereitung wird am 04. Mai 2019 der Leipziger Ernährungsrat gegründet. Der Ernährungsrat vernetzt Lebensmittelproduzent*innen und -händler*innen, Konsument*innen sowie Verwaltung und Politik in der Region – mit dem Ziel, Essen und Landwirtschaft in und um Leipzig ökologisch, fair und partizipativ zu gestalten.

Alle Interessierten sind herzlich eingeladen zu einem abwechslungsreichen Programm mit Einblicken in die Arbeit des Ernährungsrats. Inspirierende Projekte und neue Initiativen stellen sich und ihre Ideen vor und zeigen, wie „gutes Essen für Alle“ in Leipzig und nachhaltige Landwirtschaft in der Region funktionieren können. Bei einem gemeinsamen Mittagssnack gibt es die Möglichkeit zum Informieren, Austauschen und Ideen entwickeln. Anschließend wird der neue Verein offiziell gegründet.

Mehr Infos zum Ernährungsrat, der Gründungsveranstaltung und die Anmeldung dazu findet ihr auf: www.ernaehrungsrat-leipzig.org