---
id: '333952223920069'
title: 'Fortbildung: Endlich Wachstum ZWEI'
start: '2019-04-05 16:00'
end: '2019-04-07 15:00'
locationName: null
address: 'Haus 037, Alfred-Döblin-Platz 1, Freiburg-Vauban'
link: 'https://www.facebook.com/events/333952223920069/'
image: 53464741_2363807616982901_2300213544039219200_n.jpg
teaser: ANMELDUNG bis 22. März unter info@ewf-freiburg.de  Aufbauend auf dem Methodenheft "Endlich Wachstum Zwei" zu den Themenkomplexen Wirtschaft und Wachst
recurring: null
isCrawled: true
---
ANMELDUNG bis 22. März unter info@ewf-freiburg.de

Aufbauend auf dem Methodenheft "Endlich Wachstum Zwei" zu den Themenkomplexen Wirtschaft und Wachstum, das von Konzeptwerk Neue Ökonomie in Zusammenarbeit mit FairBindung entwickelt wurde, bietet die Fortbildung einen thematischen Einstieg in den Themenbereich Postwachstum und sozial-ökologische Transformation. Zudem liefert sie einen Überblick über vielfältige methodische Zugänge zum Thema.

Innerhalb der Fortbildung werden Methoden vorgestellt und gemeinsam erprobt. Die Teilnehmden leiten einzelne Übungen selbst an und reflektieren, wie sich die Methoden sinnvoll in ihrer Bildungspraxis anwenden lassen.

Die Fortbildung richtet sich an Interessierte am Thema sowie an Aktive, die zum Thema arbeiten. Darüberhinaus ist sie geeignet für Multiplikator*innen in der außerschulischen Bildungsarbeit, für Bildungsreferent*innen und für Lehrer*innen, die auf der Suche nach anregenden, erfahrungsorientierten und gut fundierten Methoden zur Arbeit mit Gruppen ab 15 Jahren sind (Jugendliche und Erwachsene).

Freitag, 5. April von 16 - 21 h
& Samstag, 6. April von 10 - 18 h
& Sonntag, 7. April von 10 - 15 h