---
id: 7433-1575396000-1575403200
title: Digitalisierung für alle, keine Zeit für niemand?
start: 2019-12-03 18:00
end: 2019-12-03 20:00
address: Galerie KUB, Kantstr. 18, Leipzig
link: https://konzeptwerk-neue-oekonomie.org/termin/digitalisierung-fuer-alle-keine-zeit-fuer-niemand/
teaser: "Die Ökonomie der Zeit ist einzigartig: alle haben davon jeden Tag gleich
  viel, aber Menschen müssen "
isCrawled: true
---
Die Ökonomie der Zeit ist einzigartig: alle haben davon jeden Tag gleich viel, aber Menschen müssen unterschiedlich viel Zeit für Arbeit einsetzen, um zu leben.  

Die Propheten der Digitalisierung versprechen allgegenwärtige Effizienz und Automatisierung in einer Zukunft, wo alles weniger Zeit kostet. Kann digitale Technik unsere Zeit von der Arbeit befreien? Im bestehenden digitalen Kapitalismus dringt Arbeit durch alle Lebensbereiche und wird eher beschleunigt und verdichtet als verkürzt. Das Smartphone klingelt am Wochenende für die Lohnarbeit. Im Coworking-Space produzieren Selbstständige lohnfrei Daten für Facebook. Kurz vorm Burnout werden online billige Flüge für den Kurzurlaub gebucht. Der Bedarf an Sorge-Arbeit für Mitmenschen und Umwelt steigt dementsprechend massiv. Wie wird Sorge-Arbeit durch digitale Technik verändert und neu verteilt? Und zu welchen ökologischen Kosten?  

Anstatt die durch Digitalisierung gewonnene Zeit zu nutzen, um unsere Gesellschaft gerechter zu machen, wird sie verwendet, um noch mehr Daten zu produzieren. Zum Beispiel wird durch alle möglichen Apps die Zeit für Liebe, Freundschaft und andere Beziehungen zu produktiver Zeit für’s Datengeschäft – mit riesigen negativen Folgen für die Umwelt. Heißt Digitalisierung mehr unbezahlte Arbeit für solche Datengeschäfte?

Gleichzeitig wird so viel Reproduktionsarbeit wie möglich über digitale Plattformen wie Foodora oder Helpling ausgelagert. Diese Plattformen funktionieren aufgrund von prekären Arbeitsbedingungen, besonders für Frauen und Migrant*innen. Heißt Digitalisierung die Befreiung von unsichtbarer Sorge-Arbeit auf Kosten anderer?

Darüber hinaus zwingen Apps wie Tinder oder Instagram menschlichen Beziehungen eine Marktlogik auf. Überlassen wir Algorithmen die Entscheidungen über unser Privatleben?  

Wir wollen mit dieser Podiumsdiskussion untersuchen, was (re)produktive Arbeitszeit und Freizeit im digitalen Kapitalismus bedeutet und wie sie verteilt wird – aus einer sozialen, feministischen und ökologischen Perspektive. 

Referent*innen:

Andrea Baier von die anstiftung

Veza Clute-Simon von netzforma*

Martha Dörfler von Heart of Code 

