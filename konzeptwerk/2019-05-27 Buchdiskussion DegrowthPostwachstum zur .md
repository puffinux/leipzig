---
id: '603349923517722'
title: Buchdiskussion "Degrowth/Postwachstum zur Einführung”
start: '2019-05-27 20:00'
end: '2019-05-27 23:00'
locationName: UNIVERSITÄT LEIPZIG
address: 'Augustusplatz 10, 04109 Leipzig'
link: 'https://www.facebook.com/events/603349923517722/'
image: 60616167_2091475114298181_8569378934355918848_n.jpg
teaser: Hiermit laden wir euch ein zur Buchdiskussion zum gerade erscheinen Buch "Degrowth/Postwachstum zur Einführung” (Junius Reihe “Zur Einführung”) von Ma
recurring: null
isCrawled: true
---
Hiermit laden wir euch ein zur Buchdiskussion zum gerade erscheinen Buch "Degrowth/Postwachstum zur Einführung” (Junius Reihe “Zur Einführung”) von Matthias Schmelzer und Andrea Vetter. 

Die Autor*innen stellen mit einem Vortrag die Inhalte des Buches vor, dieses wird von Prof. Felix Ekardt (Forschungsstelle Nachhaltigkeit und Klimapolitik) kommentiert. Danach gibt es eine Diskussion, moderiert von Nina Treu (Konzeptwerk Neue Ökonomie)

Wo: Universität Leipzig, WiFa-Gebäude, Grimmaische Straße 12

Wann: Montag, 27. Mai, 20 Uhr

Organisiert vom Konzeptwerk Neue Ökonomie in Kooperation mit dem dem netzwerk n, dem Netzwerk Plurale Ökonomik und dem DFG-Kolleg Postwachstumsgesellschaften.

Wir freuen uns auf eine spannende Diskussion mit euch!


***********************************
Degrowth/Postwachstum zur Einführung
Matthias Schmelzer/Andrea Vetter, Junius Verlag, 256 S., € 15,90

Degrowth oder Postwachstum ist ein dynamisches Forschungsfeld und Bezugspunkt vielfältiger sozial-ökologischer Bewegungen. Postwachstum ist nicht nur eine grundlegende Kritik an der Hegemonie des Wirtschaftswachstums. Es ist auch eine Vision für eine andere Gesellschaft, die angesichts von Klimawandel und globaler Ungleichheit Pfade für grundlegende Gesellschaftsveränderung skizziert. Dieser Band macht erstmals den Versuch einer systematischen Einführung. Er diskutiert die Geschichte von Wachstum und Wirtschaftsstatistiken und rekonstruiert die zentralen Formen der Wachstumskritik: ökologische, soziale, kulturelle, Kapitalismus-, feministische, Industrialismus- sowie Süd-Nord-Kritik. Und er gibt einen Überblick zu den wichtigsten Vorschlägen, Konzepten und Praktiken, die er zugleich politisch einordnet.

       
»Souverän, aber bündig, breit gefächert, aber nuanciert – eine solche Einführung in das Postwachstumsdenken hat gefehlt! Konkrete Utopien haben ihre heimlichen Schlüsselwörter: Obergrenze und selektives Wachstum, Gemeinwohl und Solidarität, Commons und Konvivalität. Es ist an der Zeit, sie auszuprobieren und einzuüben, bevor sie unversehens in aller Munde sind!«
Wolfgang Sachs, Herausgeber des Development Dictionary. A Guide to Knowledge as Power

»Kompakt, sorgfältig, inspirierend. Degrowth/Postwachstum zur Einführung bietet sowohl einen spannenden und differenzierten Einstieg für Anfänger_innen, als auch eine systematische, tiefgreifende und kritische Analyse der verschiedenen Strömungen, die auch Expert_innen überraschen wird. Von Kritik und Utopie bis hin zu aktivistischen Interventionen geben die Autor_innen einen umfassenden Überblick über die internationale Degrowth-Debatte in ihren Differenzen, Widersprüchen und Potentialen für eine radikale sozial-ökologische Transformation.«
Barbara Muraca, Autorin von Gut Leben: Eine Gesellschaft jenseits des Wachstums