---
id: '409572723209285'
title: Ernährungssouveränität und Klimagerechtigkeit in Bewegung!
start: '2019-04-23 19:00'
end: '2019-04-23 22:00'
locationName: Pöge-Haus
address: 'Hedwigstraße 20, 04315 Leipzig'
link: 'https://www.facebook.com/events/409572723209285/'
image: 57564702_2040724456039914_8525895452795600896_n.jpg
teaser: Zu den Themen Klimagerechtigkeit und Ernährungssouveränität haben in den letzten Jahren Bewegungen und Initiativen sowohl global als auch lokal auf vi
recurring: null
isCrawled: true
---
Zu den Themen Klimagerechtigkeit und Ernährungssouveränität haben in den letzten Jahren Bewegungen und Initiativen sowohl global als auch lokal auf vielfältige Weise gewirkt. Beide Themenfelder sind stark miteinander verknüpft und die Akteure nehmen durchaus aufeinander Bezug.

Die Klimagerechtigkeitsbewegung ist mit Klimacamps, den Aktionen von Ende Gelände, dem Kampf um den Hambacher Forst und ganz aktuell mit den Schulstreiks von FridaysForFuture überregional präsent. Im Sommer findet zum zweiten Mal das Klimacamp im Leipziger Land statt.
Im Bereich der Ernährungssouveränität wächst seit Jahren die Zahl Solidarischer Landwirtschaftsprojekte. Aktuell gründen sich zudem immer mehr Ernährungsräte mit dem Ziel, das lokale Ernährungssystem mitzugestalten. Auch in  Leipzig wird Anfang Mai ein Ernährungsrat gegründet.
Im Herbst 2019 wird zudem mit "Free the Soil" ein Landwirtschafts- und Klimagerechtigkeits-Camp geplant und erstmals zu massenhaften zivilen Ungehorsam gegen die industrielle Landwirtschaft in Deutschland aufrufen.

Wir wollen mit Vertreter*innen des weltweiten Netzwerks "La Via Campesina", des Ernährungsrats Leipzig, des Netzwerks Solidarische Landwirtschaft Leipzig Neuland sowie des Bündnisses "Pödelwitz bleibt" diskutieren, welche Strategien und Möglichkeiten der gegenseitigen Bezugnahme bestehen. Dabei wollen wir auch diskutieren, wie sich globale und lokale Perspektiven stärker miteinander verbinden lassen.

Die Veranstaltung findet im Rahmen des Aktionstags "La Via Campesina" statt und wird von der Werkstatt für nachhaltiges Leben und Arbeiten (Taucha) und dem Konzeptwerk Neue Ökonomie (Leipzig) organisiert.

Wann?  Dienstag, 23.4.2019, 19 Uhr

Wo? Pögehaus, Hedwigstr. 20 in Leipzig

Weitere Informationen zum Aktionstag und Frühlingsfest La Via Campesina 2019 „Für Ernährungssouveränität und Klimagerechtigkeit“ und zur Fahrraddemo am 27.4.2019 gibt es hier: www.schmiede4.net
