---
id: '630246767459465'
title: 'Café Décroissance: Degrowth for Future?'
start: '2019-09-19 19:00'
end: '2019-09-19 21:00'
locationName: Prinzessinnengarten
address: 'Prinzenstraße 35-38, 10969 Berlin'
link: 'https://www.facebook.com/events/630246767459465/'
image: 66122540_2969173156456102_2746969917545775104_o.jpg
teaser: 'Wachstumskritik in Zeiten von Klimanotstand und Fridays for Future  Unendliches Wachstum ist auf einem endlichen Planeten nicht möglich, so eine zentr'
recurring: null
isCrawled: true
---
Wachstumskritik in Zeiten von Klimanotstand und Fridays for Future

Unendliches Wachstum ist auf einem endlichen Planeten nicht möglich, so eine zentrale Einsicht der Kritik am Wirtschaftswachstum. Aber welche Rolle spielt Wachstumskritik in wissenschaftlichen Klimadebatten und Klimaprotesten? Sollten Fridays for Future eine Postwachstumsökonomie einfordern? Was heißt überhaupt "Degrowth" und wie kann eine Transformation zu einer Gesellschaft aussehen, in der das gute Leben für alle jenseits des Wachstums im Mittelpunkt steht?

In diesem Café Décroissance stellen die Autor*innen Matthias Schmelzer und Andrea Vetter ihr neues Buch “Degrowth/Postwachstum zur Einführung” vor. Nach kurzen Kommentaren von Gregor Hagedorn, Initiator der Initiative “Scientists for Future” und Emma Fuchs, aktiv bei Fridays for Future, kommt FairBindung mit euch gemeinsam ins Gespräch.

Der Eintritt ist frei, um Spende wird gebeten. Der Veranstaltungsort ist weitgehend barrierefrei.

Die Veranstaltung findet statt im Rahmen des NoPlanet B Programms, gefördert durch die Europäische Union.