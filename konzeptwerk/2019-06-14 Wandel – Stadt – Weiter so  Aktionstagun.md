---
id: '457787344994334'
title: Wandel – Stadt – Weiter so | Aktionstagung
start: '2019-06-14 12:45'
end: '2019-06-14 18:15'
locationName: null
address: 'Hallischer Saal (über Tulpe Mensa), Universität Halle, Uniring 5'
link: 'https://www.facebook.com/events/457787344994334/'
image: 60251235_2411764228867869_4244729883877441536_n.jpg
teaser: 'So kann es nicht weitergehen! Ökologische und soziale Krisen machen weltweit, aber auch vor der eigenen Haustür klar, dass wir mit unserer Lebensweise'
recurring: null
isCrawled: true
---
So kann es nicht weitergehen! Ökologische und soziale Krisen machen weltweit, aber auch vor der eigenen Haustür klar, dass wir mit unserer Lebensweise und der kapitalistischen Wirtschaftsstruktur zunehmend an Grenzen stoßen. Die Rufe nach einer umfassenden Transformation werden immer lauter. Unter dem Motto „Wandel - Stadt - Weiter so“ wollen wir die Gestaltungsmöglichkeiten für eine nachhaltige und sozialgerechte Stadt diskutieren, denn die Stadt ist Wohn- und Lebensraum ihrer Bewohner*innen.

Immer mehr Menschen hinterfragen das Wachstumsparadigma des größer-teurer-schneller. Sie kaufen lieber bei der Landwirtin aus der Region, anstatt im neu entstandenen Megasupermarkt am Stadtrand. Sie wandeln schmutzige Brachflächen zu summenden bunten Nutzgärten. Sie fahren mit dem Rad und teilen Autos über Sharingmodelle im Stadtgebiet. Sie reparieren kaputtgegangene Gegenstände anstatt immer Neues zu kaufen. Sie retten Lebensmittel, die als Folge von Überproduktion in den Müll wandern würden und setzen sich für lebenswerte städtische Räume ein, wenn weitere Flächenversiegelungen oder Großprojekte drohen.

Wie wollen wir in Zukunft leben? Mit der Tagung wollen wir uns über Ideen und Ansätze für einen sozial-ökologischen Umbau der Städte austauschen. Wie können urbane Räume und Gesellschaften abseits von ökonomischen Wachstumszwängen künftig gestaltet werden? Welche Akteur*innen des Wandels sind hierfür in der Zivilgesellschaft, Stadtplanung und Verwaltung aber auch in der Politik bedeutsam? Welche alternativen Visionen werden in unserer Region bereits erfolgreich erprobt und weiterentwickelt? Und welche Herausforderungen gilt es zu meistern, um eine lebenswerte Stadt für alle zu schaffen?

ANMELDUNG mit Namen und Angabe zum aktuellen Hintergrund unter gs-halle@rosaluxsa.de


Programm

12:45 Uhr Ankommen
13:00 Uhr Begrüßung und Einführung
Hendrik Lange | Vorstand Rosa Luxemburg Stiftung Sachsen-Anhalt, Stephan Arnold | Vorstand Heinrich-Böll-Stiftung Sachsen-Anhalt

13:15 Uhr Wem gehört die Stadt - 
Alternative Wirtschaftsweisen zum Kapitalismus
Dr. Friederike Habermann | Volkswirtin, Historikerin, Commons-Forscherin

14:00 Uhr Postwachstumsstadt - 
Sozialwissenschaftlicher Blick auf Stadtforschung
Prof. Dr. Frank Eckardt & Anton-Brokow-Loga M.Sc.  | Bauhaus-Universität Weimar

14:45 Uhr Pause und Vernetzung

15:15 Uhr Einblicke aus der Praxis

Wie wird die Stadt satt? - 
Stadtbevölkerung regional und solidarisch ernähren
Daniel Häfke & Florian Weber | Solidarische Gärtnerei Landsberg "Kleine Feldwirtschaft"

Carsharing als Modell gemeinsamen Nutzens und zur Schaffung von mehr Stadtraum
Marcel Greiner  | JEZ Mobil Carsharing in Stadt und Umland

Sozial gerecht wohnen - 
Wohnprojekte und gemeinschaftliches Eigentum
Dipl. Ing. Klaus Schotte | Haus und Wagenrat e.V. Leipzig

16:45 Uhr Auf dem Weg zur Postwachstumsstadt - 
Erste Schritte in Richtung Wandel wagen
Nina Treu | Konzeptwerk Neue Ökonomie

18:00 Uhr Ausblick
18:15 Uhr Ende der Veranstaltung
 
19:00 Uhr optional: Die Revolution der Selbstlosen | Film ohne Eintritt | Puschkino Halle: Wieso ist unsere Gesellschaft von Egoismus und Ungerechtigkeit durchzogen, wenn uns der Sinn für Fairness und Gerechtigkeit eigentlich schon in die Wiege gelegt wird? Eine beeindruckende Dokumentation über den Ursprung der Hilfsbereitschaft und kooperatives Verhalten. Weitere Infos unter: Die Revolution der Selbstlosen.

Die Konferenz richtet sich an Bürger*innen und zivilgesellschaftlich Aktive, an Politik und Verwaltung sowie an städtepolitisch Forschende und Studierende. Ziel ist es, einen Austausch zu Ansätzen von Postwachstumsstädten anzuregen und anhand vielfältiger Perspektiven erste regionalspezifische Schritte für Transformationsprozesse gemeinschaftlich zu entwickeln.

Freitag | 14.06.2019 | 12:45 bis 18:15 Uhr
Martin-Luther-Universität Halle-Wittenberg
Hallischer Saal, Burse zur Tulpe über der Mensa
Universitätsring 5 | 06108 Halle
Eintritt frei


ANMELDUNG: Zur Planung der Verpflegung bitten wir um schriftliche Anmeldung mit Namen und Angabe zum aktuellen Hintergrund unter gs-halle@rosaluxsa.de
 
 Die Veranstalter*innen:
 
Heinrich-Böll Stiftung Sachsen-Anhalt
Leipziger Straße 36 | 06108 Halle
www.boell-sachsen-anhalt.de

Rosa Luxemburg Stiftung Sachsen-Anhalt
Leitergasse 4 | 06108 Halle
http://st.rosalux.de

Konzeptwerk Neue Ökonomie
Klingenstraße 22 | 04229 Leipzig
www.knoe.org

AK Ökologie und Nachhaltigkeit
Studierendenrat der MLU
Universitätsplatz 7 | 06108 Halle
https://www.stura.uni-halle.de/ak-oekologie-nachhaltigkeit/

