---
id: 7430-1573842600-1573849800
title: "Das Ende der Arbeit. Oder: wie KI, Robotik & Co. Lohnarbeit neu definieren"
start: 2019-11-15 18:30
end: 2019-11-15 20:30
address: Café Koffij, Leipziger Str. 70, Halle (Saale)
link: https://konzeptwerk-neue-oekonomie.org/termin/das-ende-der-arbeit-oder-wie-ki-robotik-co-lohnarbeit-neu-definieren/
teaser: Die Digitalisierung stellt unsere Arbeitswelt auf den Kopf – KI, Robotik & Co.
  übernehmen immer mehr
isCrawled: true
---
Die Digitalisierung stellt unsere Arbeitswelt auf den Kopf – KI, Robotik & Co. übernehmen immer mehr Tätigkeiten, die früher von Menschen ausgeführt wurden. Gerade ArbeitnehmerInnen unterer Einkommenssegmente sind davon verstärkt betro_en. ZukunftsforscherInnen sprechen deshalb schon heute vom Ende der Arbeit. Diese Zukunftsprognose produziert naturgemäß Ängste und provoziert ganz unterschiedliche Reaktionen. Die einen sagen, dass Digitalisierung auch bedeuten müsse, Arbeitsplätze zu scha_en um den Menschen in der traditionellen Form der Erwerbstätigkeit zu halten. Die anderen sehen nun endlich die Chance gekommen, weniger arbeiten zu müssen und sich ihrer absoluten Selbstverwirklichung hingeben zu können. In diesem Zusammenhang wird auch immer wieder das Konzept des bedingungslosen Grundeinkommens ins Spiel gebracht. Doch was ist richtig: kann es eine Welt ohne Erwerbstätigkeit geben, in der Roboter all das übernehmen, was wir nicht gern machen? Kann Digitalisierung die Chance bedeuten, all das realisieren zu können, wofür sonst nie Zeit ist? Oder bleibt all das Utopie, weil es dem menschlichen Wesen nicht entspricht, nicht für Geld zu arbeiten? 

