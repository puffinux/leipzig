---
id: 7338-1574269200-1574283600
title: "Podiumsdiskussion: Die sozial-ökologische Frage nach der Datensintflut"
start: 2019-11-20 17:00
end: 2019-11-20 21:00
address: Pöge-Haus, Leipzig, Hedwigstr. 20, Leipzig, Sachsen, 04315, Deutschland
link: https://konzeptwerk-neue-oekonomie.org/termin/podiumsdiskussion-die-sozial-oekologische-frage-nach-der-datensintflut/
teaser: Im heutigen Zeitalter der permanenten digitalen Vernetzung werden riesige
  Datenmengen produziert. Im
isCrawled: true
---
Im heutigen Zeitalter der permanenten digitalen Vernetzung werden riesige Datenmengen produziert. Immer mehr Lebensbereiche werden digital gestaltet, dabei wird unsere Arbeit und unser Privatleben zum Mehrwert für Datenkonzerne wie Google, Amazon und Facebook gemacht. Für viele ist es klar, dass Arbeit und Freizeit angesichts der fortschreitenden Digitalisieriung neu definiert werden müssen. Nur, in welcher digitalen Gesellschaft wollen wir leben? 

Die pragmatische Perspektive bietet Hoffnung: die Tech-Bewegung arbeitet an freier Software, die für alle zugänglich und veränderbar ist. Auf dieser Basis bieten viele Initiativen und Kollektive datensichere Dienste an. Verschlüsselte Nachrichten sind alltäglicher geworden. Reicht dies, um unsere private Freiheit zu gewährleisten? Einige plädieren hingegen dafür, sich nicht vor dem Verlust der Privatsphäre zu scheuen, sondern die wachsende Datenproduktion und -verarbeitung als Chance für eine bessere Welt zu begreifen. 

Die Gestaltung von digitaler Technik bleibt aber in der Realität fest in den Händen von gigantischen Privatfirmen und deren Motivation heißt nicht Umweltschutz oder soziale Gerechtigkeit, sondern Profit. Angesichts der Klimakrise scheint Datensparsamkeit nötiger denn je. Auch in Hinblick auf die Arbeitswelt sowie in öffentlichen Debatten wächst die Macht der Datenkonzerne und bedarf einer stärkeren Regulierung. Welche Perspektive hat der Widerstand gegen Datenmonopole und die Umweltzerstörung durch allgegenwärtige digitale Geräte? 

Wir wollen auf diesem Podium diskutieren, welche Strategien und welcher Umgang mit massenhafter Datenproduktion einer sozial-ökologische Transformation dienen können. 

Referent*innen:

Friederike Rohde (Institut für ökologische Wirtschaftsforschung Berlin)

Daniel Grönefeld (Digitale Freiheit)

Carsten Ungewitter (Datenkollektiv Dresden) 

