---
id: '763402747378534'
title: Wake me up before you Koko!
start: '2019-02-19 19:00'
end: '2019-02-19 22:00'
locationName: Pöge-Haus
address: 'Hedwigstraße 20, 04315 Leipzig'
link: 'https://www.facebook.com/events/763402747378534/'
image: 51716703_1931726490273045_4087768023417487360_n.jpg
teaser: 'Wake me up before you Koko! Nach dem Ende der Kohlekommission: Perspektiven aus der Klimagerechtigkeitsbewegung  Referent*innen: Fabian Hübner, Refere'
recurring: null
isCrawled: true
---
Wake me up before you Koko!
Nach dem Ende der Kohlekommission: Perspektiven aus der Klimagerechtigkeitsbewegung

Referent*innen:
Fabian Hübner, Referent für Kohle- und Energiepolitik
Ruth Krohn, Konzeptwerk Neue Ökonomie und Bündnis "Pödelwitz Bleibt" 

Endlich hat die Kohlekommission ihre Ergebnisse veröffentlicht. Nach monatelangem Ringen um den Kohleausstieg liegen in Form des Berichts Zahlen und Empfehlungen an die Politik vor zur Umsetzung eines Kohleausstiegs. Doch was steckt hinter diesen Zahlen, wie sind diese zustande gekommen? Was bedeuten diese für die Kohlereviere in Deutschland, die Menschen, die dort leben und ggf. noch zwangsumgesiedelt werden sollen? Was bedeuten die Ergebnisse für die 1,5 Grad-Grenze, Klimagerechtigkeit und den Hambacher Forst?

Wir diskutieren mit Fabian Hübner, Referent für Kohle- und Energiepolitik, und Ruth Krohn, aktiv in den Bündnissen "Pödelwitz bleibt!" und "Alle Dörfer bleiben!" über die Hintergründe der Kohlekommission und was die Ergebnisse für die Klimagerechtigkeitsbewegung heißen können. Nach einem kurzen Überblick über die Entstehung, Arbeitsweise und Ergebnisse der Kohlekommission, werden wir uns der Frage widmen was eine bewegungsstrategische Perspektive sein kann.

Foto: Tim Wagner. https://www.flickr.com/photos/110931166@N08/albums
