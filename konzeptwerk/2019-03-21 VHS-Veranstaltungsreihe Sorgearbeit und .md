---
id: '289985621664176'
title: 'VHS-Veranstaltungsreihe: Sorgearbeit und Postwachstum'
start: '2019-03-21 18:00'
end: '2019-03-21 20:00'
locationName: null
address: 'Löhrstraße 3, 04105 Leipzig'
link: 'https://www.facebook.com/events/289985621664176/'
image: 51105817_2166835333631944_1390276064697122816_n.jpg
teaser: 'Hinter Postwachstum haben sich in den letzten 10 Jahren viele Menschen aus den verschiedensten Gesellschaftsbereichen versammelt, die den Zwang zu wir'
recurring: null
isCrawled: true
---
Hinter Postwachstum haben sich in den letzten 10 Jahren viele Menschen aus den verschiedensten Gesellschaftsbereichen versammelt, die den Zwang zu wirtschaftlichem Wachstum hinterfragen und nach praktischen Alternativen suchen. Sorgearbeit, als die grundlegendste Arbeit, die
erst alles andere möglich macht, soll dabei im Zentrum des Wirtschaftens stehen. Wie kann so eine Gesellschaft und Wirtschaft aussehen? Welche Möglichkeiten haben wir damit heute schon anzufangen?

Mit dem Konzeptwerk Neue Ökonomie