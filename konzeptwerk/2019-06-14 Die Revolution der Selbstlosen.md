---
id: '1061037667414345'
title: Die Revolution der Selbstlosen
start: '2019-06-14 19:00'
end: '2019-06-14 20:30'
locationName: Puschkino
address: 'Kardinal-Albrecht-Str. 6, 06108 Halle (Saale)'
link: 'https://www.facebook.com/events/1061037667414345/'
image: 60079880_2403729493004676_1502384580957569024_n.jpg
teaser: 'Der Mensch – ein selbstsüchtiges Wesen, das nur an sich selbst und sein Eigenwohl denkt? In einer Welt, in der nur Macht und Geld regieren, fällt es s'
recurring: null
isCrawled: true
---
Der Mensch – ein selbstsüchtiges Wesen, das nur an sich selbst und sein Eigenwohl denkt? In einer Welt, in der nur Macht und Geld regieren, fällt es schwer etwas anderes zu glauben. Doch Studien aus Psychologie, Neurowissenschaft und Primatenforschung zeigen, dass das Handeln aus selbstloser Motivation eine wesentliche Eigenschaft des Menschen ist. Schon Kleinkinder zeigen Hilfsbereitschaft ohne Gegenleistung und bereits Babys können zwischen Gut und Böse unterscheiden. 

Wenn uns der Sinn für Fairness und Gerechtigkeit also schon in die Wiege gelegt wird – wieso ist unsere Gesellschaft dann von so viel Egoismus und Ungerechtigkeit durchzogen? Die Doku „Die Revolution der Selbstlosen“ (90 min)  beantwortet diese und viele weitere Fragen. Kann man Selbstlosigkeit erlernen? Und können wir die Welt verändern und den Weg zu einer besseren Gesellschaft finden. 

Freitag | 14. Juni 2019 | 19:00 Uhr
Puschkino | Kardinal-Albrecht-Straße 6 | Halle
Eintritt frei

Film im Rahmen von „Wandel – Stadt – Weiter so“ - der Aktionstagung für Entwicklungsperspektiven einer Postwachstumsstadt. Eine gemeinsame Veranstaltung der Heinrich-Böll-Stiftung Sachsen-Anhalt mit der Rosa Luxemburg Stiftung Sachsen-Anhalt, dem Konzeptwerk für Neue Ökonomie und dem AK Ökologie & Nachhaltigkeit im Studierendenrat der MLU.
