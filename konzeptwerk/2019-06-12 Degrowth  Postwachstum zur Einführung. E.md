---
id: '372401176707846'
title: Degrowth / Postwachstum zur Einführung. Eine Buchdiskussion.
start: '2019-06-12 18:00'
end: '2019-06-12 19:30'
locationName: Klima-Pavillon
address: 'Rasenmühleninsel, Jena'
link: 'https://www.facebook.com/events/372401176707846/'
image: 57261101_631982290612100_1718653733547540480_n.jpg
teaser: „Postwachstum“ steht für eine Vision einer anderen Gesellschaft und skizziert angesichts von Klimawandel und globaler Ungleichheit Pfade für gesellsch
recurring: null
isCrawled: true
---
„Postwachstum“ steht für eine Vision einer anderen Gesellschaft und skizziert angesichts von Klimawandel und globaler Ungleichheit Pfade für gesellschaftliche Veränderungen. Das Buch „Degrowth/Postwachstum zur Einführung“ von Andrea Vetter und Matthias Schmelzer beleuchtet systematisch unterschiedliche Formen der Wachstumskritik und deren alternative Visionen, Konzepte und Praktiken. 
Die Autor*innen führen in ihr Buch ein und diskutieren es gemeinsam mit der ZEIT-Journalistin Elisabeth von Thadden.

 ► 12.06.2019 | Degrowth / Postwachstum zur Einführung | Andrea Vetter und Matthias Schmelzer (Konzeptwerk Neue Ökonomie) und Elisabeth von Thadden (DIE ZEIT) | Moderation: David J. Petersen (Plurale Ökonomik Jena) | 18:00 Uhr |  Klima-Pavillon | Rasenmühleninsel im Paradiespark | Jena | Eintritt frei
**********
Veranstaltet durch das Konzeptwerk Neue Ökonomie. In Kooperation mit Klima-Pavillon, Netzwerk Plurale Ökonomik,  DFG-Kolleg Postwachstumsgesellschaften der Universität Jena und netzwerk n.