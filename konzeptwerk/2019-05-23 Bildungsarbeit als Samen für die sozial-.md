---
id: '448615402545701'
title: Bildungsarbeit als Samen für die sozial-ökol. Transformation
start: '2019-05-23 11:00'
end: '2019-05-25 14:00'
locationName: Klostergut Schlehdorf eG
address: '15  Kirch Straße, 82444 Schlehdorf'
link: 'https://www.facebook.com/events/448615402545701/'
image: 57133736_2035820259863667_67751756179701760_n.jpg
teaser: 'Seminar von Ökoprojekt MobilSpiel e.V., NordSüdForum München e.V., Commit e.V., mit Susanne Brehm und Nadine Kaufmann vom Konzeptwerk als Referentinne'
recurring: null
isCrawled: true
---
Seminar von Ökoprojekt MobilSpiel e.V., NordSüdForum München e.V., Commit e.V., mit Susanne Brehm und Nadine Kaufmann vom Konzeptwerk als Referentinnen.

Bildung meint Triebkraft für einen sozial-ökologischen Wandel von Wirtschaft und Gesellschaft zu sein. Doch wie kann unsere Bildungsarbeit den „Samen“ zur Veränderung im Denken und Handeln „pflanzen“? Welche Zugänge haben sich bewährt? Und wo können wir unsere eigenen Ansätze zwischen Umweltbildung, Bildung für nachhaltige Entwicklung, Globalem Lernen und machtkritischer Bildungsarbeit noch erweitern? Das Seminar bietet eine Auseinandersetzung mit dem Ansatz der Bildung für die sozial-ökologische Transformation. Mit neuen Informationen und interaktiven Methoden vertiefen wir unser pädagogisches Handeln. Im Austausch reflektieren wir das Selbstverständnis unserer Bildungsarbeit und stärken unser Netzwerk. Das 3-Tages-Seminar im KlosterGut Schlehdorf ist auf gemeinsamer Initiative von Mitgliedern der BNE-Akteursplattform mit dem Wunsch nach inhaltlicher Vertiefung entstanden.

Mehr Infos und Anmeldung: https://www.oekoprojekt-mobilspiel.de/weiterbildung/fortbildungen