---
id: '367639980768410'
title: Buchdiskussion Degrowth/Postwachstum
start: '2019-05-23 18:00'
end: '2019-05-23 20:00'
locationName: null
address: 'Hausvogteiplatz 5, 10117 Berlin'
link: 'https://www.facebook.com/events/367639980768410/'
image: 60726779_1168620563310141_3339155223724687360_n.jpg
teaser: 'Welche Rolle spielen Wachstumskritik und Konzepte für eine Postwachstumsgesellschaft in wissenschaftlichen Debatten, und was bedeuten sie für die aktu'
recurring: null
isCrawled: true
---
Welche Rolle spielen Wachstumskritik und Konzepte für eine Postwachstumsgesellschaft in wissenschaftlichen Debatten, und was bedeuten sie für die aktuellen Klimaproteste? Was heißt überhaupt "Degrowth" und wie kann eine Transformation zu einer Gesellschaft aussehen, in der das gute Leben für alle Menschen im Mittelpunkt steht?

Hiermit laden wir euch ein zu Buchdiskussion & Fishbowl zum gerade erscheinen Buch "Degrowth/Postwachstum zur Einführung” (Junius Reihe “Zur Einführung”) von Matthias Schmelzer und Andrea Vetter. Das Inhaltsverzeichnis sowie der größte Teil der Einleitung kann hier heruntergeladen werden: https://konzeptwerk-neue-oekonomie.org/wp-content/uploads/2019/04/SchmelzerVetter_DegrowthPostwachstum_Teaser.pdf

Was: Die Autor_innen stellen mit einem Vortrag die Inhalte des Buches vor, dieses wird kurz von Olga A. Statnaia (IRI-THESys, Humboldt-Universität Berlin) und einer Person von Fridays for Future kommentiert.
Danach gibt es eine Fishbowl-Diskussion, moderiert von Lisa Weinhold (netzwerk n).

Wo: Humboldt-Universität, Hausvogteiplatz 5-7 im RAUM 007 (direkt am U-Bahnhof Hausvogteiplatz, U2)
Wann: Donnerstag, 23. Mai, 18 - 20 Uhr

Organisiert vom Konzeptwerk Neue Ökonomie in Kooperation mit dem dem netzwerk n, dem Nachhaltigkeitsbüro der Humboldt-Universität, dem Netzwerk Plurale Ökonomik und dem DFG-Kolleg Postwachstumsgesellschaften.

Wir freuen uns auf eine spannende Diskussion mit euch!

Degrowth/Postwachstum zur Einführung
Matthias Schmelzer/Andrea Vetter, Junius Verlag, 256 S., € 15,90

Degrowth oder Postwachstum ist ein dynamisches Forschungsfeld und Bezugspunkt vielfältiger sozial-ökologischer Bewegungen. Postwachstum ist nicht nur eine grundlegende Kritik an der Hegemonie des Wirtschaftswachstums. Es ist auch eine Vision für eine andere Gesellschaft, die angesichts von Klimawandel und globaler Ungleichheit Pfade für grundlegende Gesellschaftsveränderung skizziert. Dieser Band macht erstmals den Versuch einer systematischen Einführung. Er diskutiert die Geschichte von Wachstum und Wirtschaftsstatistiken und rekonstruiert die zentralen Formen der Wachstumskritik: ökologische, soziale, kulturelle, Kapitalismus-, feministische, Industrialismus- sowie Süd-Nord-Kritik. Und er gibt einen Überblick zu den wichtigsten Vorschlägen, Konzepten und Praktiken, die er zugleich politisch einordnet.

»Souverän, aber bündig, breit gefächert, aber nuanciert – eine solche Einführung in das Postwachstumsdenken hat gefehlt! Konkrete Utopien haben ihre heimlichen Schlüsselwörter: Obergrenze und selektives Wachstum, Gemeinwohl und Solidarität, Commons und Konvivalität. Es ist an der Zeit, sie auszuprobieren und einzuüben, bevor sie unversehens in aller Munde sind!«
Wolfgang Sachs, Herausgeber des Development Dictionary. A Guide to Knowledge as Power

»Kompakt, sorgfältig, inspirierend. Degrowth/Postwachstum zur Einführung bietet sowohl einen spannenden und differenzierten Einstieg für Anfänger_innen, als auch eine systematische, tiefgreifende und kritische Analyse der verschiedenen Strömungen, die auch Expert_innen überraschen wird. Von Kritik und Utopie bis hin zu aktivistischen Interventionen geben die Autor_innen einen umfassenden Überblick über die internationale Degrowth-Debatte in ihren Differenzen, Widersprüchen und Potentialen für eine radikale sozial-ökologische Transformation.«
Barbara Muraca, Autorin von Gut Leben: Eine Gesellschaft jenseits des Wachstums