---
id: /event/kew/2019/10/18/Das-Digitale-im-kapitalistischen-Zeitalter
title: Das Digitale im kapitalistischen Zeitalter
start: 2019-10-18 19:00
address: Hörsaal HS2, Campus Augustusplatz
link: https://ag-link.xyz/event/kew/2019/10/18/Das-Digitale-im-kapitalistischen-Zeitalter.html
teaser: Ob es darum geht, die ganze Erde zu kartieren oder alle Freundschaften der
  Welt zu organisieren – im digitalen Kapitalismus werden Algorithmen zur
  wichtigsten Maschine, Daten zum essenziellen Rohstoff und Informationen zur
  Ware Nummer eins. Zur Frage nach dem Neuen im alten Kapitalismus referiert
  Timo Daum Autor von „Das Kapital sind wir“, welches 2018 mit dem Preis des
  politischen Buches der Friedrich Ebert-Stiftung ausgezeichnet wurde.
isCrawled: true
---
Ob es darum geht, die ganze Erde zu kartieren oder alle Freundschaften der Welt zu organisieren – im digitalen Kapitalismus werden Algorithmen zur wichtigsten Maschine, Daten zum essenziellen Rohstoff und Informationen zur Ware Nummer eins. Zur Frage nach dem Neuen im alten Kapitalismus referiert Timo Daum Autor von „Das Kapital sind wir“, welches 2018 mit dem Preis des politischen Buches der Friedrich Ebert-Stiftung ausgezeichnet wurde.

Die Veranstaltungen findet am 18. Oktober um 19:00 Uhr im Hörsaal HS2, Campus Augustusplatz statt.

