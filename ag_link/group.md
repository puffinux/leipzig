---
name: AG Link
website: https://ag-link.xyz/
email: ag-link@riseup.net
scrape:
  source: aglink
---
Die AG Link ist eine Gruppe Studierender unterschiedlichster Fachrichtungen, die sich kritisch mit den Wechselprozessen zwischen IT und Gesellschaft beschäftigen. 