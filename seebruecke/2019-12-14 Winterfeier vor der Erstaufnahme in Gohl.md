---
id: "2845774225454590"
title: Winterfeier vor der Erstaufnahme in Gohlis
start: 2019-12-14 14:00
end: 2019-12-14 18:00
address: Max-Liebermann-Straße 36C, 04159 Leipzig
link: https://www.facebook.com/events/2845774225454590/
image: 78671026_973979326310063_7023725310965186560_n.jpg
teaser: Wir befinden uns mitten in der Weihnachtszeit. Lasst sie uns nutzen, um
  Grenzen und Abschottung schon in unserer Nachbarschaft zu überwinden und gemei
isCrawled: true
---
Wir befinden uns mitten in der Weihnachtszeit. Lasst sie uns nutzen, um Grenzen und Abschottung schon in unserer Nachbarschaft zu überwinden und gemeinsam ein Fest der Solidarität feiern! Am 14. Dezember 2019 kommen wir von 14 bis 18 Uhr vor der Erstaufnahmeeinrichtung in der Max-Liebermann-Straße in Leipzig-Gohlis zusammen. Wir wollen Begegnungen schaffen und Kontakte knüpfen und zeigen, dass wir zusammenhalten.

Zusammenhalten müssen wir aber nicht allein, weil Weihnachten vor der Tür steht. Zusammenhalten müssen wir immer. Wir sind gegen das Konzept von "Ankerzentren", in denen Geflüchtete für lange Zeit zentral untergebracht werden, um sie danach einfacher abschieben zu können. Geflüchtete sollen frühzeitig dezentral wohnen können. Darüber hinaus sind Asylbewerber*innen an die Residenzpflicht gebunden. Viele von ihnen müssen mit der ständigen Bedrohung von Abschiebung leben - selbst in Kriegsregionen wie Afghanistan. Wir wollen diesen Zuständen entschieden entgegentreten! Wir wollen zeigen, dass Geflüchtete willkommen sind!

Am 14. Dezember werden wir ein buntes Fest feiern. Neben allem, was wir zum Feiern brauchen, wird es einen Kleider- und Kindersachenbasar geben, Musik, ein kleines Kinderprogramm und zahlreiche Möglichkeiten, um ins Gespräch zu kommen. Verschiedene Initiativen werden vor Ort sein, um ihre Arbeit vorzustellen und Möglichkeiten zum eigenen Engagement aufzuzeigen. Darüber hinaus wollen wir gemeinsam überlegen, wie wir uns organisieren können, um etwas an den politischen Verhältnissen in Leipzig, Sachsen und ganz Deutschland zu ändern.

-----------------------------

We celebrate together! We stand together! 

Initial reception centre Leipzig

In the midst of Christmas time, let's tear down barriers in our neighbourhood! We want to have a celebration of community in front of the refugee centre in which we can meet face to face.

Christmas is not the only occasion to stand together. It's essential to unite against the concept of "Ankerzentrum" that keeps refugees isolated and makes their deportation invisible. Many of them have to live under an constant fear of deportation even to unsafe countries as Afghanistan and there are more and more further repressions. We - instead - want to show that refugees are welcome!

There will be music, hot drinks, a playcorner for children, a free bazaar and room for interactions. Local groups will present their work in the field of refugee support. We also want to use our gathering to bring together problems refugees face in their daily lifes in order to give them a platform.

------------------------------

Festivité rime avec solidarité ! 

C’est la période des fêtes et l’occasion de dépasser dans notre voisinage les frontières et l’isolement en célébrant une fête de la solidarité ! On se retrouve le 14 décembre 2019 de 14 à 18 heures dans la Max-Liebermann-Straße à Leipzig-Gohlis. Nous voulons créer un espace de rencontre et montrer qu’on se serre les coudes.

Il faut se montrer solidaire, non seulement parce que c’est bientôt Noël mais toujours. Nous nous opposons au concept des centres d’accueil style « AnkER » où les réfugié·es doivent obligatoirement vivre au centre-ville pour pouvoir plus facilement les expulser un moment donné. Les réfugié·es devraient avoir le droit d’habiter le plus tôt possible dans des logements décentralisés. En outre, on impose aux demandeur·euses d’asile l’obligation de résidence. Beaucoup d’entre eux doivent vivre sous la menace permanente d’une possible expulsion – même dans des régions en conflit comme l’Afghanistan. Nous sommes déterminé·es à nous opposer contre ces pratiques ! Nous voulons montrer que les réfugié·es sont bienvenu·es !

Le 14 décembre, nous fêtons une fête de la diversité. À part de tout dont on a besoin pour célébrer, il y aura une vente de charité de vêtements et d’affaires d’enfant, de la musique, un petit programme pour les enfants et plein de possibilités d’entrer en contact. Il y aura de différentes associations qui présenteront leur travail et informeront sur les moyens de participation. De plus, nous voulons réfléchir ensemble comment nous pouvons nous organiser pour changer la situation politique à Leipzig, en Saxe et dans toute l’Allemagne.

---------------------------------

¡Celebramos juntos! ¡Nos mantenemos unidos! 

Estamos en medio de la temporada navideña. ¡Utilicémosla para superar fronteras, aislamiento en nuestro vecindario y tener una celebración solidaria! El 14 de diciembre de 2019 nos reuniremos de 2 a 6 pm frente al centro de primera acogida en Max-Liebermann-Straße en Leipzig-Gohlis. Vamos a tener encuentros, establecer nuevos contactos y demostrar que nos mantenemos unidos.

Pero no solo porque la Navidad está a la vuelta de la esquina tenemos que estar juntos, sino siempre hay que mantenernos unidos. Nos oponemos al concepto de "centros de anclaje", donde los refugiados están alojados en un lugar central durante mucho tiempo para poder deportarlos más fácilmente después. Los refugiados deberían poder vivir descentralizado desde una fase temprana. Además, los solicitantes de asilo tienen la obligación de residencia. Muchos de ellos tienen que vivir con la amenaza constante de deportación, incluso en zonas de guerra como Afganistán. ¡Queremos oponernos decisivamente a estas condiciones! ¡Queremos mostrar que los refugiados son bienvenidos!

El 14 de diciembre celebraremos una divertida fiesta. Además de todo lo necesario para tener una buena fiesta, habrá un bazar de ropa y cosas para niños, música, un pequeño programa para niños y numerosas oportunidades para conversar. Varias iniciativas estarán disponibles para presentar su trabajo y mostrar posibilidades para el compromiso propio de cada uno. Además, queremos pensar juntos en cómo nos podemos organizar para cambiar la situación política en Leipzig, Sajonia y toda Alemania.

--------------------------------

دعونا نتخذ من أعياد الميالد فرصة لنتجاوز الحدود والعزل في أحيائنا 
باالحتفال في مهرجان التضامن! في ١٤.كانون الأول ٢٠١٩ من الساعة ٢ ت ظهرا ح
الساعة ٦ َ مساء أمام مكان االستقبال الأول في Straße-Liebermann-Max
في Straße-Liebermann-Max . َ نود أن نحدث لقاء وتواصالَ ونريد أن نظهر 
عياد 
يجب أن نتماسك دائماَ. نحن ضد ”مراكز المراساة جيث ي ي تم وضع الالجئن أننا نتماسك سويا.ولك ي ننا لسنا ملزمن بالتماسك لوحدنا فهو وقت الأ
لوقت طويل في مك ي ان معن لي ي تم بعد ذلك تهجهم من جديد. يجب أن 
يكون بإمك ي ان الالجئن السكن في المكان الذي يريدون السكن فيه. أي ب ي ضأ طال
ي اللجوء ملزمن بالبقاء في المكان الذي سصلون له. الك يث منهم يعيشون 
ي خائفن ي من التهج إىل أماك ي ن خط كأفغانستان. نحن نريد أن نواجه هذه 
الأ ي وضاع بثقة! نود أن نظهر أن الالجئن مرحب بهم!
ي نحتاجها 
في ١٤ دسي بم سنحتفل في مهرجان متعدد. إلي جانب الأ ت مور ال
لالحتفال سيكون هناك بازار للمالبس وحاجات الأطفال و الموسيقى وفعالية 
يصغة للأطفال و الك يث من ا إلمكانيات للدخول في الحوار. الك يث من 
المبادرات ستك ض ون حاة وستقوم بتقديم نفسها وبعرض ا إلمكانيات للعمل 
فيها. با إلضافة إىل ذلك نود أن نفكر مع بعضنا, كيف نود أن ننظم أنفسنا 
لتغيي الأحوال السياسية في اليزيغ, ساكسونيا وكل ألمانيا.

----------------------------

In the midst of Christmas time, let’s tear down barriers 
in our neighbourhood! We want to have a celebration 
of community in front of the refugee centre in which we 
can meet face to face.
Christmas is not the only occasion to stand 
together. It’s essential to unite against the concept 
of “Ankerzentrum” that keeps refugees isolated and 
makes their deportation invisible. Many of them have 
to live under an constant fear of deportation even to 
unsafe countries as Afghanistan and there are more 
and more further repressions. We - instead - want to 
show that refugees are welcome!
There will be music, hot drinks, a playcorner for 
children, a free bazaar and room for interactions. Local 
groups will present their work in the field of refugee 
support. We also want to use our gathering to bring 
together problems refugees face in their daily lives in 
order to give them a platform.

--------------------------------
 با هم جشن میگیریم و با هم متحد میشویم!

اواسط جشن کریسمس است. بیایید که از مرزها و مرزبندیها
بگذریم و همه با هم کریسمس را در روز ۱۴ام ماه دسامبر بین
ساعت ۱۴ تا ۱۸ روبهروی کمپ/هایم مهاجران (کمپ اولیه) در
خیابان Liebermann-Max در محلهی Gohlis لایپزیگ جشن
بگیریم. ما میخواهیم آغاز کنندهی ارتباطی باشیم که نشان
میدهد ما با هم متحد هستیم.
اما اتحاد فقط برای اکنون نیست که کریسمس پشت در خانههاست.
ما باید همیشه متحد باشیم. ما مخالف سیستمی هستیم که پناهجوها
را برای مدت طولانی در یک کمپ مرکزی نگهداری میکند تا بتواند در
صورت لزوم آنها را به آسانی برگرداند (دیپورت کند).
پناهجویان باید از ابتدا حق زندگی مستقل داشته باشند. همچنین
برای پناهجویان قانون سکونت و حرکت در مناطق خاص در نظر
گرفته شده است. بسیاری از آنها را به طور دائم به دیپورت تهدید
میکنند، حتی آنهایی که از مناطق جنگی مثل افغانستان میآیند.
ما میخواهیم که این قوانین تغییر کند. ما میخواهیم به پناهجوها
و پناهندگان خوش آمد بگوئیم.
در ۱۴ دسامبر ما جشنی رنگارنگ خواهیم داشت. در کنار همهی
چیزهایی که برای جشن گرفتن وجود دارد، بازار دستدوم(فلومارکت)،
موزیک و برنامههای مفرح برای کودکان ترتیب داده شده است.
گروههای مختلف مشاوره در لایپزیگ که برای حمایت از پناهجویان
شکل گرفتهاند، در این جشن حضور دارند، خدمات خود را به
پناهجویان معرفی میکنند و به آنها مشاوره میدهند.
در کنار همهی اینها، میخواهیم با هم مشورت کنیم که چطور
میتوانیم برای تغییر موضع قانونگذارها در لایپزیگ، زاکسن و حتی
همهی آلمان وارد عمل شویم.

-----------------------------

Мы празднуем вместе! Мы держимся вместе! 

Макс-Либерман-штрассе, Лейпциг, 14.12.2019

Рождественский сезон в самом разгаре. Самое время встретится с соседями преодолеть все границы в нашем районе и отпраздновать праздник солидарности! Мы встретимся 14 декабря 2019 года с 14 до 18 часов перед центром первичного приема на улице Макс-Либерман-штрассе в лейпцигском районе Голис. Давайте встретимся, пообщаемся и покажем нашу сплоченность.

Но не только в рождественское время мы должны быть солидарны, а всегда. Мы против концепции «якорных центров», где беженцы долгое время живут централизованно, чтобы потом было легче их выслать. По нашему мнению, беженцы должны иметь возможность децентрализованного проживания. Многим из них приходится жить с постоянной угрозой депортации - даже в зоны военных действий, таких как Афганистан. Мы решительно выступаем против этой политики. Мы беженцев приветствуем!

14 декабря мы отпразднуем нашу вечеринку. Вы рядом с лакомствами, музыкой и программой для детей, также найдете базар одежды. Но думаем главное это многочисленные возможности общения. Различные инициативы представят свою работу. Кроме того, мы хотим вместе обсудить каковы возможности самоорганизации, чтобы изменить политические условия в Лейпциге, Саксонии и всей Германии.

-----------------------------

ერთად ვზეიმობთ! ერთად ვდგავართ! მაქს ლიბერმანის ქუჩა ლაიფციგი 14.12.2019
მალე შობა დაგვიდგება! მოდით გამოვიყენოთ წინასაშობაო პერიოდი ჩვენსავე
სამეზობლოში საზღვრებისა და იზოლაციის გადასალახად და ერთად ავღნიშნოთ
სოლიდარობის ზეიმი!
2019 წლის 14 დეკემბერს 14 საათიდან 18 საათამდე ვიკრიბებით მაქს
ლიბერმანის ქუჩაზე ლაიფციგში, გოლისში, პირველად მიმღებ დაწესებულებასთან.
ჩვენ გვსურს შეხვედრები, კონტაქტების დამყარება და ჩვენი ერთად დგომის
ჩვენება.
მარტო არა, ერთად უნდა დავდგეთ, რადგან შობა კარს მოგვადგა. მუდამ ერთად
ვიდგეთ. ჩვენ ვეწინააღმდეგებით ანკერცენტრების (Ankerzentren - ჩამოსვლის,
გადაწყვეტილების და რეპატრიაციის ცენტრები) კონცეფციას, იქ სადაც
ლტოლვილებს დიდი ხნის განმავლობაში ცენტრალიზებულ საცხოვრებელში
აცხოვრობენ, რათა შემდეგ ისინი უფრო მარტივად გააძევონ ქვეყნიდან.
ლტოლვილებს უნდა შეეძლოთ ადრეულ ეტაპზევე დეცეტრალიზებულად ცხოვრება.
გარდა ამისა თავშესაფრის მაძიებლები ვალდებულები არიან დაიცვან კონკრეტულ
ადგილას ცხოვრების ვალდებულება. ბევრი მათგანი იძულებულია იცხოვროს
ქვეყნიდან გაძევების მუდმივი საშიშროების ქვეშ - მათ შორის ომის
რეგიონებში, ავღანეთში. ჩვენ გვსურს ამ გარემოებებს გადამწყვეტად
დავუპირისპირდეთ! გვსურს ვაჩვენოთ, რომ ლტოლვილებს მივესალმებით!
14 დეკემბერს აღვნიშნავთ ფერად ზეიმს. ამ აღნიშვნის გარდა, მოეწყობა
ტანსაცმლის და საბავშვო ნივთების ბაზრობა, იქნება მუსიკაც, მცირე საბავშვო
პროგრამა და უამრავი შესაძლებლობა საუბრის დასაწყებად.
ადგილზე იქნება სხვადასხვა ინიციატივა, საკუთარი სამუშაოს და ჩართულობის
წარმოსაჩენად. გარდა ამისა, გვსურს ერთად განვიხილოთ, თუ როგორ შეგვიძლია
თვითორგანიზება, ლაიფციგში, საქსონიაში და მთელს გერმანიაში პოლიტიკური
დამოკიდებულებების შესაცვლელად