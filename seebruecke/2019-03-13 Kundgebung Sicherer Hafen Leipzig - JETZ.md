---
id: '267685390792503'
title: Kundgebung Sicherer Hafen Leipzig - JETZT !
start: '2019-03-13 13:15'
end: '2019-03-13 14:00'
locationName: Neues Rathaus Leipzig
address: 'Martin-Luther-Ring 4, 04109 Leipzig'
link: 'https://www.facebook.com/events/267685390792503/'
image: 53110549_323742551609297_8263466272335855616_n.jpg
teaser: Achtung liebe Menschen!   Am 13. März stimmt der Leipziger Stadtrat über den Antrag zur Aufnahme von aus Seenot geretteten Menschen ab! Doch vorher so
recurring: null
isCrawled: true
---
Achtung liebe Menschen! 

Am 13. März stimmt der Leipziger Stadtrat über den Antrag zur Aufnahme von aus Seenot geretteten Menschen ab! Doch vorher sollen die Abgeordneten durch uns erneut an ihre Pflicht erinnert werden, endlich Stellung gegen das anhaltende Sterbenlassen im Mittelmeer zu beziehen.

Seit Anfang 2019 sind bereits mehr als 300 Menschen im Mittelmeer gestorben oder werden vermisst: Darunter 170 Menschen, die bei einem Bootsunglück vor der libyschen Küste am 19. Januar ertrunken sind. Gleichzeitig wurden seit Januar zahlreiche Menschen nach Libyen zurück geschifft, von wo sie vor Folter, Versklavung oder Mord geflohen waren.

Dennoch leistet derzeit nur das Rettungsschiff ,,Alan Kurdi'' der NGO Sea Eye Hilfe im Mittelmeer. Rettungsschiffe anderer Organisationen wie Sea Watch, Open Arms oder SOS Mediterrane wurden und werden von europäischen Behörden aus politischen Gründen festgehalten. Immer wieder kommt es daher zu langen Zeiträumen, in denen gar kein Rettungsschiff Hilfe leistet.

Auch in diesem Jahr will die EU hunderte Menschen im Mittelmeer ertrinken lassen! 
Dazu zählt auch der perfide Versuch der CDU  im Leipziger Stadtrat Seenotrettung als Förderung illegaler Schlepperei zu kriminalisieren und den Antrag zur Aufnahme von aus Seenot geretteten Menschen zu verhindern.

Doch am 13. März treten wir diesem Vorhaben mit einer lauten Kundgebung für Seenotrettung und Solidarität klar entgegen. Mehr als 1000 Aufnahmeplätze stehen in Leipzig bereit und die Stadtverwaltung hat öffentlich bestätigt, dass eine zusätzliche Aufnahme von Geflüchteten möglich ist. 

Kämpfen wir also noch einmal dafür, dass Leipzig zum sicheren Hafen wird! 

Treffpunkt: 13:15 vor dem neuen Rathaus. Kommt in Orange und pünktlich, damit die Stadräte euch nicht verpassen! ;)