---
id: "2538235006400295"
title: Stadtrundgang Solidarity City
start: 2019-10-24 15:00
end: 2019-10-24 17:00
address: "Treffpunkt: Innenhof / Campus Augustusplatz / Uni Leipzig"
link: https://www.facebook.com/events/2538235006400295/
image: 71324630_1032201460283386_8004109312047710208_o.jpg
teaser: "Do. 24.10.19 / 15.00  Stadtrundgang Solidarity City  Seebrücke Leipzig /
  Treffpunkt: Innenhof Campus Augustusplatz   An den Grenzen Europas entscheide"
isCrawled: true
---
Do. 24.10.19 / 15.00

Stadtrundgang Solidarity City

Seebrücke Leipzig / Treffpunkt: Innenhof Campus Augustusplatz 

An den Grenzen Europas entscheidet die europäische Migrations- und Asylpolitik über das Leben tausender Menschen, die Asyl suchen. Die Seebrücke Bewegung zeigt, wie hoch das Bedürfnis nach Veränderung ist. Welche Einflussmöglichkeiten gibt es auf lokaler Ebene? Was zeichnet eine solidarische Stadt aus? Ein Spaziergang durch die Innenstadt zeigt an verschiedenen Stationen, welchen Spielraum Institutionen haben, um solidarische Orte zu schaffen. Und was wir tun können!