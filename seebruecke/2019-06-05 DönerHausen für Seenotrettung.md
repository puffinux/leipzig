---
id: '1125882234262445'
title: DönerHausen für Seenotrettung
start: '2019-06-05 18:30'
end: '2019-06-05 21:30'
locationName: null
address: SchönerHausen
link: 'https://www.facebook.com/events/1125882234262445/'
image: 61525302_1672503289560168_2819456123550040064_n.jpg
teaser: Wir machen leckere selbstbefüllte Döner mit verschiedenen Soßen und Auberginen-Saitan aus der Pfanne. Als freshen Sommer-Drink dazu gibt es einen Soli
recurring: null
isCrawled: true
---
Wir machen leckere selbstbefüllte Döner mit verschiedenen Soßen und Auberginen-Saitan aus der Pfanne.
Als freshen Sommer-Drink dazu gibt es einen Soli-Cocktail mit und ohne Alkohol. 
Alle Einnahmen gehen an die Seenotrettung.
Bei schönem Wetter draußen im Garten mit Lagerfeuer. 

Im Anschluss ans Essen gibt es einen Vortrag über Seenotrettung im Hinterhaus. 

ca. 20:00 Vortrag

Die Krise auf dem Mittelmeer ist alles andere als vorbei. Die Route von Nordafrika nach Südeuropa ist noch immer die tödlichste Grenze der Welt. Während die Zahl der Menschen, die in Europa ankommen sinkt – aufgrund tief in den afrikanischen Kontinent hineinreichender EU-Grenzsicherungsmaßnahmen – steigt die Todesrate auf dem Mittelmeer. Im Jahr 2018 starb jeder sechste Mensch bei dem Versuch, das Meer zu überqueren. Anfang 2019 stieg diese Quote auf jeden vierten Menschen an. 

Was dort passiert ist weder ein “Unglück” noch eine “Flüchtlingskrise” – es ist kaltes, gezieltes, politisch motiviertes Sterbenlassen.

Emily und Neeske erzählen von ihrer Arbeit im Airborne Department von Sea-Watch und bieten einen Einblick in die derzeitige Situation im zentralen Mittelmeer und die so notwendige, aber immer schwieriger bis unmöglich werdende Arbeit der  Seenotrettungsorganisationen.
_________
Wir freuen uns auf euch!





** Dies ist eine Veranstaltung von Sea Watch und der Leipziger Seebrücke bei SchönerHausen**