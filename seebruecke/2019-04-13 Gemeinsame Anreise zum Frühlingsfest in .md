---
id: '428323577923032'
title: Gemeinsame Anreise zum Frühlingsfest in Pödelwitz!
start: '2019-04-13 13:40'
end: '2019-04-13 20:00'
locationName: null
address: Hauptbahnhof Leipzig / Gleis 1
link: 'https://www.facebook.com/events/428323577923032/'
image: 56973908_337464830237069_8717847325189341184_n.jpg
teaser: Seebrücken statt Kohlegruben!  Treffpunkt 13.40 am HBF GL 1 ;)  Am Samstag wollen wir mit euch allen zum Frühlingsfest nach Pödelwitz fahren und uns d
recurring: null
isCrawled: true
---
Seebrücken statt Kohlegruben!  Treffpunkt 13.40 am HBF GL 1 ;)

Am Samstag wollen wir mit euch allen zum Frühlingsfest nach Pödelwitz fahren und uns dort mit allen Menschen solidarisieren, die seit vielen Jahren gegen die Klima zerstörende Kohleföderung und Vernichtung von Dörfern durch die MIBRAG kämpfen! Die deutschen Kohlekonzerne tragen maßgeblich dazu bei, dass millionen Menschen jährlich aufgrund der Folgen des Klimawandels fliehen müssen. Viele von Ihnen verlieren dabei ihr Leben. Was dabei an Leiden und Sterben im Mittelmeer sichtbar wird, ist nur ein kleiner Teil der durch den Klimawandel mitgeschaffenen Not und Flucht. Um das Sterben auf den Meeren zu beenden, müssen auch die Fluchursachen hier vor Ort bekämpft werden. Daher kommt mit nach Pödelwitz und zeigt euch solidarisch im Kampf für Klimagerechtigkeit und ein Leben aller Menschen in Würde und Sicherheit.

Mit mit einem Stand für gemeinsames Basteln und Transpimalen werden wir uns von der Leipziger Seebrücke am Fest beteiligen und laden euch ein gerne mitzumachen! ;)

Treffpunkt: 
Samstag (13.4) um 13.40 im Leipziger Hauptbahnhof am Gleis 1.   Abfahrt 13.54 nach Neukiertsch und von dort dann 20 Min. mit dem Fahrrad nach Pödelwitz!

Das Fest geht von 15-20 Uhr. Am Ende soll es dann auch eine gemeinsame Rückreise geben für alle, die möchten. ;) 


Aufruf des Naturschutzbundes Sachsen:

Keinen Meter der Kohle: Frühlingsaktion in Pödelwitz!
Wann: Samstag, 13.4.2019, 15 Uhr bis ca. 20 Uhr
Wo: In Pödelwitz vor der Kirche  
Wenn der Beschluss der Kohlekommission ernst genommen wird, dann müssen keine Dörfer mehr der Kohle weichen. RWE, MIBRAG, LEAG sowie die Landesregierungen in Nordrhein-Westfalen, Sachsen und Brandenburg sehen das aber anders und arbeiten mit ihrer Politik des verzögernden Kohleausstiegs aktiv gegen das 1,5-Grad-Ziel. Zu diesem haben sich neben Deutschland zahlreiche Staaten im Pariser Abkommen verpflichtet. Zum Einhalten der 1,5-Grad-Grenze ist ein schnelles Handeln in den nächsten 10 Jahren erforderlich, sonst ist es zu spät - so der Weltklimarat in seiner Studie vom November 2018. Der politische Umgang mit dem Kohleausstieg und den Menschen in den von Abbaggerung bedrohten Dörfern ist inakzeptabel, daher rufen wir auf:

Kommt am 13. April zur Frühlingsaktion in Pödelwitz: "Keinen Meter der Kohle!"
Gemeinsam machen wir deutlich, dass wir uns der Abbaggerung der Dörfer entgegen stellen und keinen Meter abgeben werden! 

Dazu bauen wir diesmal kein Baumhaus für den Hambacher Forst, sondern zimmern gemeinsam einen bunten Wald von Widerstandsversprechen gegen die Abbaggerungen - mit Farbe, Stoffen, Fäden und mehr - mit Groß und Klein. Am Abend entzünden wir gemeinsam ein Widerstands-X aus Kerzen für den Erhalt aller Dörfer!

Unsere Widerstandsversprechen sehen sich in der Tradition des Widerstands in der DDR, als der Cospudener Tagebau im Leipziger Südraum gestoppt wurde, im Anti-Atom-Widerstand v.a. im Wendland, in den Besetzungsaktionen von Ende Gelände und Kohle ErSetzen, in der Verteidigung des Hambacher Forsts und in den Bagger-Besetzungen der letzten Monate - und unsere Versprechen sind auch Versprechen für die Zukunft!

Dazu gibt es ein buntes Rahmenprogramm mit Kaffee und Kuchen, Infostände, eine Bühne mit Musik, Beiträge von Menschen aus den betroffenen Dörfern in Nordrhein-Westfalen, der Lausitz, dem Leipziger Land und aus dem Globalen Süden. Junge Menschen erwartet u.a. eine Hüpfburg, Wimmelbilder, Anstecker-Basteln, u.a.m.
Auf einem Dorfspaziergang zeigen wir, wie die MIBRAG große Teile von Pödelwitz leer stehen lässt und die Dorfentwicklung blockiert, ganz ohne rechtliche Grundlage, und schauen auch zum Betriebsgelände. Für alle der mehr als 60 abgebaggerten Dörfer und Städte der Region um Leipzig bauen wir Gedenk-Schilder, mit denen wir auch an die vielen unsichtbar gewordenen Schicksale erinnern. Abends werden wir grillen und den Tag gemütlich ausklingen lassen.

Wir laden euch ein, nach Pödelwitz zu kommen um zu zeigen, dass wir die Zukunft der Dörfer nicht den Kohlekonzernen überlassen und uns nicht mit Kompromissen der kohlefreundlichen Regierungen abspeisen lassen. 

Keinen Meter der Kohle! Damit alle Dörfer bleiben - im Leipziger Umland und überall!
                                   
Weitere Informationen auch zur Anreise unter:  www.alle-doerfer-bleiben.de
