---
id: '615025985657858'
title: Chillen für den Feminismus
start: '2019-06-29 15:30'
end: '2019-06-29 17:30'
locationName: null
address: 'vor dem Hotel Fürstenhof Leipzig, Tröndlinring 8, 04105 Leipzig'
link: 'https://www.facebook.com/events/615025985657858/'
image: 65188189_2779106345494230_1722790401233911808_n.jpg
teaser: Und wieder möchte der Ableger aus neurechten Verstrickungen "Frauen fordern" in Leipzig sein Glück versuchen. Schon am 29. Juli 2018 hatten sie zu e
recurring: null
isCrawled: true
---
	
Und wieder möchte der Ableger aus neurechten Verstrickungen "Frauen fordern" in Leipzig sein Glück versuchen. Schon am
29. Juli 2018 hatten sie zu einer Demonstration zu mobilisiert. Auch wenn das Motto “Keine Gewalt” zu breiter Beteiligung aufrief, kamen damals nur etwa 50 Ewiggestrige zusammen, die plötzlich Frauenrechte als ein wichtiges Thema für sich entdeckt hatten. Daran änderte auch die Mobilisierung vorrangig aus den bekannten Strukturen in Dresden und Meißen sowie über eine ebenfalls neue Gruppe “Biker for Womens” (welche auch in diesem Jahr wieder dabei sein wollen) nichts. 

Dem Aufwärmen von Gewaltmythen gegen Geflüchtete und überhaupt alle, die nicht der Halluzination eines homogenen deutschen Volkes entsprechen, setzt das Aktionsnetzwerk einen Aufruf unter dem "Chillen für den Feminismus" entgegen.

Einen guten Einblick in die Strukturen, die hinter “Frauen fordern” stehen, findet ihr im blog http://purecoincidence.blogsport.de/2018/07/25/rechte-pflegerevolution-perspektive-zum-rechten-frauen-und-bikermarsch/

Wir wollen die Truppe nicht aufwerten und trotzdem klar unseren Standpunkt kommunizieren. Im Hinblick auf die tropischen Temperaturen, haben wir das Format mit Liegestühlen und Planschbecken gewählt - bringt also eure Sitzgelegenheiten mit.

Wie auch im vergangenen Jahr werden uns MC Kuhle Wampe Pleißenburg unterstützen.

Besonders freuen wir uns auf den Redebeitrag des Bündnisses Feministischer Streik Leipzig