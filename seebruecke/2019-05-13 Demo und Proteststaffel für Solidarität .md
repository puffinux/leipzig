---
id: '476261326526396'
title: Demo und Proteststaffel für Solidarität und Humanität
start: '2019-05-13 15:00'
end: '2019-05-13 17:00'
locationName: Markt Leipzig
address: 'Markt 1, 04109 Leipzig'
link: 'https://www.facebook.com/events/476261326526396/'
image: 58761446_345866826063536_599519353678856192_n.jpg
teaser: 'PROTESTSTAFFEL DER SEEBRÜCKE gegen die Abschottungspolitik der EU: Wir tragen ein Fluchtboot vom Markt zum Neuen Rathaus  Aufruf zur Aktion am 13.05.2'
recurring: null
isCrawled: true
---
PROTESTSTAFFEL DER SEEBRÜCKE gegen die Abschottungspolitik der EU: Wir tragen ein Fluchtboot vom Markt zum Neuen Rathaus

Aufruf zur Aktion am 13.05.2019 in Leipzig der Seebrücke - Schafft sichere Häfen 

Die europäische Politik nimmt tausendfach Sterben in Kauf, macht Flucht zu einem Verbrechen und verleumdet Seenotretter als kriminelle Handlanger der Schlepper. Die SEEBRÜCKE transportiert auf der Proteststaffel für Humanität und Solidarität von Tag zu Tag ein Fluchtboot und ein Transparent in verschiedene Städte. Helft uns, das Proteststaffel-Transparent mit vielen Unterschriften zu füllen! Wir möchten erneut mit euch durch die Stadt zum Neuen Rathaus ziehen und klar machen: Leipzig steht FÜR Seenotrettung und GEGEN die Abschottungspolitik der EU!

Das Schlauchboot wurde für einen Fluchtversuch von Libyen nach Europa eingesetzt. Verzweifelte Menschen vertrauten diesem Stück Gummi ihr Leben und das Leben ihrer Kinder an und versuchten, dem Schrecken der Lager in Libyen über das offene Meer zu entkommen. Ihr Boot wurde von der durch die EU finanzierten, sogenannten libyschen Küstenwache abgefangen. Die schutzsuchenden Menschen wurden unter europäischer Koordination aus internationalen Gewässern nach Libyen zurückgebracht. Ob sie mit anderen Booten die Flucht geschafft haben, noch in Libyen oder tot sind, werden wir nie erfahren.

Wir fordern sichere Fluchtwege, Entkriminalisierung der Seenotrettung und eine humanitäre Aufnahme der Menschen, die fliehen mussten oder auf der Flucht sind.
Wir fordern einen Notfallplan für Geflüchtete, die Möglichkeit der freiwilligen zusätzlichen Aufnahme von Schutzsuchenden in Städten und Gemeinden, die sich zu Sicheren Häfen erklärt haben, Landesaufnahmeprogramme für aus Seenot gerettete Menschen und keine Rückführung nach Libyen.


Montag, 13.05.2019 | 17:00 Uhr auf dem Marktplatz in der Innenstadt | ab 18:00 Uhr Zug zum kleinen Wilhelm-Leuschner-Platz (vor dem dm) | Neues Rathaus


https://seebruecke.org/proteststaffel-fur-humanitat-und-solidaritat/