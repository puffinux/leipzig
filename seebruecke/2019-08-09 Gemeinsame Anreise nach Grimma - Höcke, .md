---
id: '468621737270452'
title: 'Gemeinsame Anreise nach Grimma - Höcke, hau ab!'
start: '2019-08-09 16:45'
end: '2019-08-09 21:00'
locationName: null
address: Leipzig Hauptbahnhof
link: 'https://www.facebook.com/events/468621737270452/'
image: 67520888_2878215185583345_1863260124148137984_n.jpg
teaser: 'Liebe Menschen,  am 09.08. möchte der thüringische AfD-Landeschef Bernd Höcke im Rathaus von Grimma eine Veranstaltungen abhalten. Außerdem soll es au'
recurring: null
isCrawled: true
---
Liebe Menschen,

am 09.08. möchte der thüringische AfD-Landeschef Bernd Höcke im Rathaus von Grimma eine Veranstaltungen abhalten. Außerdem soll es auf dem dortigen Marktplatz eine AfD-Kundgebung geben. Wir wollen das nicht unwidersprochen lassen und rufen dazu auf, gemeinsam den Gegenprotest der Menschen vor Ort zu unterstützen. 

Wir treffen uns dazu um 16:45 Uhr am Gleis 19 im Leipziger Hauptbahnhof. Unser Zug fährt um 17:06 Uhr.

Wichtiger Hinweis: Da Grimma im Mitteldeutschen Verkehrsverbund liegt, können Menschen mit einem Student*innen-Ticket kostenlos fahren.

Weitere Informationen folgen. Wir halten euch auf dem Laufenden. Informiert eure Freund*innen!

Hintergrundfoto: Danny Sotzny (CC BY-SA 2.0) 
