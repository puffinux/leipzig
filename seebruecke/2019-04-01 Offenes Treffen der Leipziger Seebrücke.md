---
id: '3136264063065923'
title: Offenes Treffen der Leipziger Seebrücke
start: '2019-04-01 19:00'
end: '2019-04-01 20:30'
locationName: linXXnet
address: 'Brandstr. 15, 04277 Leipzig-Connewitz, Sachsen'
link: 'https://www.facebook.com/events/3136264063065923/'
image: 55908440_332566034060282_2921638648266883072_o.png
teaser: 'Liebe Menschen :)  Am 1. April findet unser nächstes Plenum/Planungstreffen statt.  Es gibt einiges zu besprechen.  * Wie soll es nun mit dem Stadtrat'
recurring: null
isCrawled: true
---
Liebe Menschen :)

Am 1. April findet unser nächstes Plenum/Planungstreffen statt.  Es gibt einiges zu besprechen. 
* Wie soll es nun mit dem Stadtratsbeschluss weitergehen
* Wie beteiligen wir uns an anstehenden Bündnissen und Demos
* usw. 

Wir freuen uns, wenn ihr kommt. Wie gesagt, wir sind eine offene Gruppe und ihr seid herzlich willkommen. 

- ist kein Aprilscherz, ne ;)