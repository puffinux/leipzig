---
id: "2776455709079493"
title: Soli Küfa + Filmvorführung
start: 2019-12-18 18:30
end: 2019-12-18 22:00
address: Eisenbahnstraße 176, 04315 Leipzig
link: https://www.facebook.com/events/2776455709079493/
image: 78959768_465330467450504_2514153608879013888_o.jpg
teaser: Wir laden euch herzlich zu einer Seenotrettungs-Soli-Küfa im @SchoenerHausenLE
  ein! Gemeinsam mit euch wollen wir den Film "Operation Moonbird" schau
isCrawled: true
---
Wir laden euch herzlich zu einer Seenotrettungs-Soli-Küfa im @SchoenerHausenLE ein!
Gemeinsam mit euch wollen wir den Film "Operation Moonbird" schauen und uns einen Einblick in die Arbeit der zivilen Luftaufklärung von Sea-Watch verschaffen. Dazu gibt's was leckeres veganes zu Essen und ein paar Getränke an der Bar (inklusive Glühwein!!). 

Ablauf:
18.30 Küfa
19.30 Film & Vortrag
anschließend Q&A mit Aktivist*innen von Sea-Watch und Alarmphone.

Kommt vorbei, wir freuen uns auf Euch!


Alle Erlöse des Abends gehen an zivile Seenotrettungsorganisationen.