---
name: Leipziger Seebrücke - Schafft sichere Häfen
website: https://www.seebruecke.org
email: seebrueckeleipzig@riseup.net
scrape:
  source: facebook
  options:
    page_id: 216399132343640
---
internationale Bewegung: Wir fordern: sichere Fluchtwege, Entkriminalisierung der Seenotrettung und menschenwürdige Aufnahme von Geflüchteten!
