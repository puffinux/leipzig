---
id: '414652045777255'
title: Offenes Treffen der Leipziger Seebrücke
start: '2019-03-18 19:00'
end: '2019-03-18 21:00'
locationName: Interim
address: 'Demmeringstraße 32, 04177 Leipzig'
link: 'https://www.facebook.com/events/414652045777255/'
image: 54518500_327475801235972_1790249610011213824_n.jpg
teaser: 'Liebe Menschen :)  Wie ihr ihr wisst hat sich Leipzig am Mittwoch zur Aufnahme von Menschen in Seenot bereiterklärt. Wir danken euch allen für eure Un'
recurring: null
isCrawled: true
---
Liebe Menschen :)

Wie ihr ihr wisst hat sich Leipzig am Mittwoch zur Aufnahme von Menschen in Seenot bereiterklärt. Wir danken euch allen für eure Unterstützung, durch die wir dieses Ziel erreichen konnten! =) Dennoch bleibt viel zu tun, um die Notlage der Seenot NGOs zu beenden und gerettete Menschen wirklich in Leipzig willkommen zu heißen. Deshalb kommt gerne zu unserem nächsten Treffen am Montag. Wir freuen uns auf euch! ;)