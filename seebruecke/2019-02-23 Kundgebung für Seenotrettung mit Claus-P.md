---
id: '223354635172684'
title: Kundgebung für Seenotrettung mit Claus-Peter Reisch
start: '2019-02-23 10:00'
end: '2019-02-23 15:00'
locationName: null
address: 'Friedrich-Wieck-Straße, 01326 Dresden'
link: 'https://www.facebook.com/events/223354635172684/'
image: 52765534_522537774819433_4814234434133819392_n.jpg
teaser: Am 23.02.19 um 10 Uhr veranstalten Martin Wosnitza und MISSION LIFELINE an der „Senfbüchse“ (Joseph-Herrmann-Denkmal) auf der Friedrich-Wieck-Straße e
recurring: null
isCrawled: true
---
Am 23.02.19 um 10 Uhr veranstalten Martin Wosnitza und MISSION LIFELINE an der „Senfbüchse“ (Joseph-Herrmann-Denkmal) auf der Friedrich-Wieck-Straße eine Kundgebung für die Seenotrettung. MISSION LIFELINE wird mit Kapitän Claus-Peter Reisch vor Ort sein und über die Situation auf dem Mittelmeer berichten. Ihr seid alle herzlich zu der Veranstaltung eingeladen. Kommt vorbei und setzt euch gemeinsam mit uns gemeinsam für die Einhaltung der Menschenrechte ein. 

Foto: Henning Schlottmann • Lizenz: CC BY-SA 4.0