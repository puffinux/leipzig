---
id: '415719328998891'
title: Gemeinsame Anreise zur Frühlingsaktion in Pödelwitz
start: '2019-04-13 13:45'
end: '2019-04-13 14:00'
locationName: null
address: Leipzig Hbf Gleis 1
link: 'https://www.facebook.com/events/415719328998891/'
image: 56806349_2090889200995438_593607120742514688_n.jpg
teaser: 'Keinen Meter der Kohle! Damit alle Dörfer bleiben – im Leipziger Umland und überall  Das Bündnis Alle Dörfer bleiben, welches sich für den Erhalt alle'
recurring: null
isCrawled: true
---
Keinen Meter der Kohle!
Damit alle Dörfer bleiben – im Leipziger Umland und überall

Das Bündnis Alle Dörfer bleiben, welches sich für den Erhalt aller durch Abbaggerung bedrohten Dörfer einsetzt, organisiert gemeinsam mit dem Bündnis Pödelwitz bleibt! am 13. April eine Frühlingsaktion in Pödelwitz. (→ weitere Infos https://www.alle-doerfer-bleiben.de/aktionen/keinen-meter-der-kohle/)

Wir wollen mit euch gemeinsam anreisen, um zu zeigen, dass auch wir aus Leipzig die Kämpfe der Menschen in Pödelwitz und anderswo unterstützen. Machen wir deutlich, dass wir uns der Abbaggerung der Dörfer entgegenstellen und keinen Meter abgeben werden! Machen wir deutlich, dass globale Klimagerechtigkeit mit den lokalen Kämpfen gegen die Braunkohle zusammen gehört!

Wir treffen uns um 13:45 am Leipziger Hauptbahnhof auf Gleis 1 um gemeinsam nach  Neukieritzsch zu fahren. Die letzten 9 km nach Pödelwitz legen wir mit dem Bus oder mit dem Rad zurück. Also bringt Fahrräder mit! Die Frühlingsaktion geht von 15-20Uhr und es gibt reichlich Programm. #Klimagerechtigkeit #AlleDörferbleiben #FridaysforFuture

Wer keine/kaum Knete für das Ticket hat kommt bitte ein paar Minuten früher, daran soll die Teilnahme nicht scheitern und wir finden eine Lösung!
