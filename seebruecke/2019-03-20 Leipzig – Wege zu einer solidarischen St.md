---
id: '923317444725658'
title: Leipzig – Wege zu einer solidarischen Stadt –– Stadtrundgang
start: '2019-03-20 17:30'
end: '2019-03-20 19:30'
locationName: null
address: Innenhof der Universität Campus Augustusplatz
link: 'https://www.facebook.com/events/923317444725658/'
image: 50299053_304636616853224_989864411244003328_n.jpg
teaser: '-- Im Rahmen der Internationen Wochen gegen Rassismus --  An den Grenzen Europas entscheidet die europäische Migrations- und Asylpolitik über das Lebe'
recurring: null
isCrawled: true
---
-- Im Rahmen der Internationen Wochen gegen Rassismus --

An den Grenzen Europas entscheidet die europäische Migrations- und Asylpolitik über das Leben tausender Menschen, die Asyl suchen.Die Seebrücke Bewegung zeigt, wie hoch das Bedürfnis nach Veränderung ist.
Welche Missstände befinden sich in unserer Stadt? Welche Einflussmöglichkeiten gibt es auf lokaler Ebene und welche Initiativen sind bereits vorhanden? Ein Spaziergang durch die Innenstadt zeigt an verschiedenen Stationen, welchen Spielraum Institutionen haben, um solidarische Orte zu schaffen.
