---
id: '391557774977687'
title: Offenes Treffen der Leipziger Seebrücke
start: '2019-02-25 19:00'
end: '2019-02-25 21:00'
locationName: linXXnet
address: 'Brandstr. 15, 04277 Leipzig-Connewitz, Sachsen'
link: 'https://www.facebook.com/events/391557774977687/'
image: 52608605_319755048674714_2126962637857095680_n.jpg
teaser: 'Liebe Menschen, :)  am 13.März  wird der Leipziger Stadtrat nun wirklich über die Aufnahmebereitschaft  von Menschen entscheiden, die aus Seenot geret'
recurring: null
isCrawled: true
---
Liebe Menschen, :)

am 13.März  wird der Leipziger Stadtrat nun wirklich über die Aufnahmebereitschaft  von Menschen entscheiden, die aus Seenot gerettet wurden! Diese bedeutende Abstimmung soll nicht ohne breite Aufmerksamkeit stattfinden. Deshalb wollen wir gerne mit euch überlegen, wie wir noch einmal mit kreativen Aktionen Druck auf die Stadträte ausüben können. Dafür seid ihr herzlich eingeladen unser offenes Treffen zu besuchen und auch sonst, wenn ihr gerne mitmachen wollt, jederzeit willkommen! ;) 