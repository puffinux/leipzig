---
id: "599520814149522"
title: Keine Weihnachtslieder dem Faschismus!
start: 2019-12-15 11:45
end: 2019-12-15 18:45
locationName: Leipzig Hauptbahnhof
address: Willy-Brandt-Platz 5, Leipzig
link: https://www.facebook.com/events/599520814149522/
image: 78498606_3217880401616820_5875587832990400512_o.jpg
teaser: Das Aktionsnetzwerk „Leipzig nimmt Platz“ organisiert am 15. Dezember eine
  gemeinsame Anreise nach Dresden zum kreativen Protest gegen die rassistisch
isCrawled: true
---
Das Aktionsnetzwerk „Leipzig nimmt Platz“ organisiert am 15. Dezember eine gemeinsame Anreise nach Dresden zum kreativen Protest gegen die rassistische, menschenverachtende und islamfeindliche Gruppierung PEGIDA, welche an dem Tag die Dresdner Innenstadt erneut mit schrägen hasserfüllten Tönen beschallen möchte. #DD1512

Wir rufen alle Leipziger*innen dazu auf, mit uns am Sonntag, den 15.12.2019 ab 11:45 Uhr (Treffpunkt am Querbahnsteig) gemeinsam nach Dresden zu fahren, um gegen die völkisch-rassistische Gruppierung Pegida und insbesondere ihr absurdes Religionsverständnis auf die Straße zu gehen. Für uns ist klar: das „christliche Abendland“ muss nicht gerettet werden, wohl aber demokratische Grundwerte und die pluralistische Gesellschaft. Keine Weihnachtslieder dem Faschismus!