---
id: "458560851511627"
title: Helfer*innenplenum zur Ende Gelände Massenaktion
start: 2019-11-19 20:00
end: 2019-11-19 22:00
address: Tipi, Karl-Heine-Straße 87
link: https://www.facebook.com/events/458560851511627/
image: 74406484_553750105438050_4027753869864861696_n.jpg
teaser: Vom 29.11.-1.12 wird die nächste Ende Gelände Massenaktion stattfinden. Dabei
  wird Leipzig als einer von drei Anreiseorten fungieren. Um das stemmen z
isCrawled: true
---
Vom 29.11.-1.12 wird die nächste Ende Gelände Massenaktion stattfinden. Dabei wird Leipzig als einer von drei Anreiseorten fungieren. Um das stemmen zu können brauchen wir eure Unterstützung!

Wenn ihr Zeit und Lust habt die Aktion hier in Leipzig zu supporten kommt vorbei um Infos über anstehende Aufgaben und so weiter zu bekommen.

wir freuen uns auf euch!