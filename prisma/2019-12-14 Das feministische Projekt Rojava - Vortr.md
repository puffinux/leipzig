---
id: "534638353758540"
title: Das feministische Projekt Rojava - Vortrag von DRIFT in Leipzig
start: 2019-12-14 19:00
end: 2019-12-14 22:00
address: Mariannenstraße 101, 04315 Leipzig
link: https://www.facebook.com/events/534638353758540/
image: 78432426_2590743811016734_6539218902231023616_o.jpg
teaser: Das feministische Projekt Rojava - Ein Vortrag von Ferda Berse und Hevidar
  Mert.  2011 gingen die Bilder des arabischen Frühlings um die Welt. Erst in
isCrawled: true
---
Das feministische Projekt Rojava - Ein Vortrag von Ferda Berse und Hevidar Mert.

2011 gingen die Bilder des arabischen Frühlings um die Welt. Erst in Ländern wie Algerien und Tunesien, später auch in Syrien. Allerdings mündete die Hoffnung auf Veränderung rasch in Kämpfen, die wir heute als den syrischen Bürgerkrieg kennen. Vor diesem Hintergrund haben kurdische Organisationen im Juli 2012 auf friedlicher Basis die Leitung der Städte und Dörfer in Rojava – dem kurdischen Teil Syriens – übernommen. Durch den Rückzug des Assad-Regimes aus Rojava hatten die Menschen erstmalig wieder Raum zum Aufatmen. Sie riefen die Autonomie in den drei Kantonen Cizre, Afrin und Kobane aus, die zusammen das Gebiet Rojava bilden. Unter TEV-DEM (basisdemokratische Dachorganisation selbstverwalteter Kommunen) beschloss die PYD (Volksverteidigungseinheiten) gemeinsam mit der christlich-syrischen Einheitspartei eine Übergangsregierung aufzustellen. Dies markierte den offiziellen Beginn der Selbstverwaltung, die schrittweise zur radikalen Demokratisierung der Region führte. Unter anderem wurden Schulen errichtet, die in verschiedenen Sprachen unterrichten, um den Zugang zu Bildung für alle ethnischen Gruppen zu gewährleisten. Entlang der Prinzipien der kurdischen Frauenbewegung wurden Akademien errichtet, um die autochthone Gesellschaft über patriarchale Strukturen zu informieren und zu bilden. Darüber hinaus wurden das Prinzip der Doppelspitze und eine 40% Quote eingeführt, um die Teilhabe von Frauen* an Politik und Gesellschaft zu gewährleisten. Zudem wurden ökonomische Kooperativen gegründet, die eine Alternative zu den etablierten Wirtschaftssystemen darstellen sollen. Vor kurzem gründete sich in der Region auch eine eigene Fridays For Future Ortsgruppe.

Ab Sommer 2013 wurde dieser Aufbau jedoch wiederholt durch islamistische Milizen – am populärsten der sogenannte Islamische Staat, kurz IS – angegriffen und das vor allem mit der Unterstützung des türkischen Staates. So war es den islamistischen Kämpfern möglich über die Türkei nach Syrien und in den Irak zu reisen, um sich dem IS anzuschließen. Durch den Genozid an den Ezid*innen im August 2014, aber auch durch den Angriff auf Kobane im selben Jahr zogen die Volks- und Frauenverteidigungseinheiten der YPG/J enorme mediale Aufmerksamkeit auf sich. Somit auch das Gesellschaftsmodell, das für Basisdemokratie, Frauen*befreiung und ethnische Vielfalt steht. Seit 2013 ist die Selbstverwaltung Rojavas wiederholt Angriffen ausgesetzt: 2014 als der IS Kobane belagerte, 2016 als die Türkei erstmals in syrisches Staatsgebiet einmarschierte, 2018 als Bilder annektierter Olivenhaine im Kanton Afrin, bereits auf türkische Expansionsbestrebungen hindeuteten bis hin zum aktuellen Angriffskrieg der Türkei.

Die Angriffe auf Rojava sind auch Angriffe gegen das dort gelebte Gesellschaftsmodell und die Frauen*befreiung. Sie dienen zur Erhaltung von patriarchalen und strukturellen Machtverhältnissen.
In diesem Vortrag soll gerade deswegen ein tieferes Verständnis für das politische System Rojavas – unter besonderer Berücksichtigung der geschichtlichen Ereignisse – geschaffen werden. Außerdem soll der Demokratische Konföderalismus als ein alternatives Gesellschaftsmodell vorgestellt werden, in dem feministische Theorien Anwendung finden.