---
id: '2286996911514292'
title: Interreligiöses Fastenbrechen
start: '2019-05-11 17:00'
end: '2019-05-11 20:00'
locationName: null
address: 'Floßplatz 32, 04107 Leipzig'
link: 'https://www.facebook.com/events/2286996911514292/'
image: 58419202_1265176270304860_7446840230264438784_o.jpg
teaser: 'As-Salamu alaikum wa rahmatullah liebe Geschwister und Freunde,  anlässlich des Fastenmonats Ramadan veranstalten wir mit dem VASA und den christliche'
recurring: null
isCrawled: true
---
As-Salamu alaikum wa rahmatullah liebe Geschwister und Freunde,

anlässlich des Fastenmonats Ramadan veranstalten wir mit dem VASA und den christlichen Hochschulgemeinden/-gruppen ESG, KSG, SMD, Theokreis, sowie Campus Connect, auch in diesem Jahr ein interreligiöses Fastenbrechen (Iftar).

Ihr seid alle herzlich willkommen, bei leckeren Speisen und einer gemütlichen Atmosphäre, gemeinsam mit uns das Fasten zu brechen.

Einlass: 19.00 Uhr
Programmbeginn: 19.30 Uhr

Wir freuen uns über Leckereien für das Buffet. Bitte achtet darauf, dass die Speisen vegetarisch oder halal sind.

ANMELDUNG an iftar2019@mhgleipzig.de bis zum 05.05.2019 unter Angabe der teilnehmenden Personen.

Wir freuen uns auf Euer zahlreiches Erscheinen!

Eure MHG Leipzig