---
id: '1238017499707456'
title: Hochschulwahlen
start: '2019-06-04 09:00'
end: '2019-06-04 16:00'
locationName: UNIVERSITÄT LEIPZIG
address: 'Augustusplatz 10, 04109 Leipzig'
link: 'https://www.facebook.com/events/1238017499707456/'
image: 62016091_2220486764655212_8416770499991830528_n.jpg
teaser: Bei den Hochschulwahlen werden  - GruppenvertreterInnen aller Mitgliedsgruppen in den Fakultätsräten  - Studentische SenatorInnen - Gleichstellungsbea
recurring: null
isCrawled: true
---
Bei den Hochschulwahlen werden 
- GruppenvertreterInnen aller Mitgliedsgruppen in den Fakultätsräten 
- Studentische SenatorInnen
- Gleichstellungsbeauftragte und Vertretung in vielen Fakultäten
gewählt. 

Parallel finden die Wahlen zu
- den Fachschaftsräten
- dem Referat Ausländischer Studierender, und
- dem Promovierendenrat statt.

Alle Informationen:
https://uni-l.de/wahlen
https://stura.uni-l.de/wahlen