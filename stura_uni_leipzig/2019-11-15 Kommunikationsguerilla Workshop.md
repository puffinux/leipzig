---
id: "2175486462751904"
title: Kommunikationsguerilla Workshop
start: 2019-11-15 13:00
end: 2019-11-15 16:00
address: StuRa Uni Leipzig
link: https://www.facebook.com/events/2175486462751904/
image: 72875954_2418198871563288_2023006858582687744_n.jpg
teaser: Werbebotschaften oder PR-Aktionen von Konzernen, Lobbyvereinigungen oder
  Parteien zu verfremden, zu überspitzen oder anderweitig kreativ zu verändern
isCrawled: true
---
Werbebotschaften oder PR-Aktionen von Konzernen, Lobbyvereinigungen oder Parteien zu verfremden, zu überspitzen oder anderweitig kreativ zu verändern – ohne dass dies dem Betrachter vielleicht auf den ersten Blick auffällt – das ist Kommunikationsguerilla. Dabei müssen es nicht unbedingt teure Hochglanz-Aktionen sein. Denn Kommunikationsguerilla hat das Potenzial, auch mit einfachen Mitteln und begrenzten Ressourcen Zugang zum öffentlichen Diskurs zu schaffen. 

Wie das funktionieren kann, soll hier in einem stark auf Mitarbeit der Teilnehmenden setzenden Workshop mit vielen bunten lustigen Aktionsbildern und Berichten gezeigt werden. Nach der Einführung (Vorstellung, Erwartungshaltungs-Runde, kurzer Input zur politischen Bedeutung des Themas) lernt ihr in Kleingruppen, anhand von „Aktionstütchen“ (Bilder, kurze Filmclips, Pressemitteilungen, Aktionsberichte, Zeitungsartikel), Kommunikationsguerilla-Aktionen kennen.

Anmeldung nicht erforderlich!
