---
id: "559663067909010"
title: Lesung von the Great Nowitzki
start: 2019-12-12 20:00
address: Hörsaal Nord
link: https://www.facebook.com/events/559663067909010/
image: 72955789_142450983763460_2601321456210018304_n.jpg
teaser: Lesung zum Buch "The Great Nowitzki" von Thomas Pletzinger.
isCrawled: true
---
Lesung zum Buch "The Great Nowitzki" von Thomas Pletzinger.