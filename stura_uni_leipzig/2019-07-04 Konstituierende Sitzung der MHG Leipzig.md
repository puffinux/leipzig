---
id: '2257357684579083'
title: Konstituierende Sitzung der MHG Leipzig
start: '2019-07-04 17:00'
end: '2019-07-05 21:00'
locationName: null
address: Universität Leipzig
link: 'https://www.facebook.com/events/2257357684579083/'
image: 64710337_1309040749251745_5018024546360360960_n.jpg
teaser: 'As-Salamu alaikum liebe muslimische Mitglieder der Hochschulgemeinden in Leipzig und Umgebung,   wir veranstalten in Kooperation mit dem Student_innen'
recurring: null
isCrawled: true
---
As-Salamu alaikum liebe muslimische Mitglieder der Hochschulgemeinden in Leipzig und Umgebung, 

wir veranstalten in Kooperation mit dem Student_innenRat der Universität Leipzig (StuRa) und dem Referat für ausländische Studierende (RAS) am 04. und 05. Juli 2019 eine konstituierende Sitzung, in der wir die MHG Grundordnung neu gestalten und abstimmen. Danach findet eine Wahl der Arbeitsgruppe statt.

Alle Muslime, die an der Universität Leipzig, sowie an der HTWK oder an der Universität Halle und Umgebung studieren, promovieren oder arbeiten, sind als Mitglieder der Hochschulgemeinden, herzlich eingeladen an dieser Sitzung teilzunehmen. 

Für die Teilnahme ist eine Anmeldung an gf@stura.uni-leipzig.de erforderlich. Bitte gibt hierfür euren Namen und eure Matrikelnummer, sowie die Hochschule an, an der Ihr studiert, promoviert oder arbeitet. 

Bis zum 01. Juli 2019 hat jeder die Möglichkeit seine Vorschläge für eine neue MHG Grundordnung an gf@stura.uni-leipzig.de zu senden. Teilt uns daher Eure Ideen, Wünsche und Erwartungen an die MHG Leipzig mit. Wir freuen uns auch über Satzungstexte, die Eurer Meinung nach in der MHG Grundordnung stehen sollten. 

Ziel ist es, an den beiden Tagen eine verbindliche MHG Grundordnung aufzustellen auf der die weitere MHG Arbeit basiert. 

Weitere Informationen findet ihr in der Satzung des Student_innenRates der Universität Leipzig unter §15 zu den Arbeitsgruppen. 

Der Ort der Veranstaltung wird noch bekannt gegeben. 
An beiden Tagen findet sie jeweils um 17-21 Uhr statt. 

Wir freuen uns auf zwei produktive Tage mit Euch!

Eure MHG Leipzig