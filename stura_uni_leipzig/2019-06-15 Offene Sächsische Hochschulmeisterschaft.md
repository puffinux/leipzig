---
id: '2048718268589638'
title: Offene Sächsische Hochschulmeisterschaften 2019
start: '2019-06-15 09:00'
end: '2019-06-16 17:00'
locationName: Jahnallee
address: 'Am Sportforum, 04105 Leipzig'
link: 'https://www.facebook.com/events/2048718268589638/'
image: 60679536_436012363800331_3773514328750161920_n.jpg
teaser: '***Offene Sächsiche Hochschulmeisterschaften finden statt!***  IHR KÖNNT EUCH NOCH BIS ZUM 10.06.19 BEI UNS ANMELDEN!  Das Campusfest der Universität'
recurring: null
isCrawled: true
---
***Offene Sächsiche Hochschulmeisterschaften finden statt!***

IHR KÖNNT EUCH NOCH BIS ZUM 10.06.19 BEI UNS ANMELDEN!

Das Campusfest der Universität Leipzig wurde leider abgesagt, doch lasst euch nicht beirren, die Offenen Sächsischen Hochschulmeisterschaften werden wie geplant stattfinden.

Also macht euch schon mal warm, sucht euer Team zusammen und bereitet euch körperlich und geistig vor, denn ihr bekommt die Chance euch mit den Besten der Besten in Sachsen und Umgebung zu messen. Ihr dürft gespannt sein auf die Offenen Sächsischen Hochschulmeisterschaften.

Dazu laden wir recht herzlich alle Sportfreaks, Sportaspiranten und alle die es noch werden wollen zu unseren Wettbewerben ein. Teilnehmen können alle Studierende sowie Mitarbeiter_innen und Dozierende an Hochschulen und ähnlichen Bildungseinrichtungen, die sich in der Lage fühlen, den Wettkampfbestimmungen entsprechend, im Schwimmen die jeweilige Strecke zu absolvieren, im Basketball auch mal den Ball im Korb versenken zu können sowie im Volleyball eben diesen nicht allzu oft zu verfehlen . Es werden Wettbewerbe in den Sportarten Streetball (3 vs 3), Volleyball-Mixed, Schwimmen und Schach durchgeführt.

Ist die passende Sportart für euch dabei? Dann schickt einfach den ausgefüllten Meldebogen an oshm@gmx.de.
Die Ausschreibungen inkl. Meldebogen findet ihr auf der Homepage der LHS: https://lhs-sachsen.de/twettkampf.html

Wir freuen uns auf alle Teilnehmenden sowie Zuschauer_innen. 

                  -           weitere Infos folgen in Kürze       -  