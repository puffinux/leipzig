---
id: '2113693762012788'
title: 'Women* in Capitalism, Women* in Science'
start: '2019-05-07 17:00'
end: '2019-05-07 20:00'
locationName: null
address: 'Ziegenledersaal des StudierendenRates der Universität Leipzig, Universitätsstraße 1'
link: 'https://www.facebook.com/events/2113693762012788/'
image: 56490394_1273980119425521_5572348826766802944_n.jpg
teaser: 'Women* in Capitalism, Women* in Science: Zur Marginalisierung von Frauen* in unserem Wirtschaftssystem, den Wirtschaftswissenschaften und in der Forsc'
recurring: null
isCrawled: true
---
Women* in Capitalism, Women* in Science: Zur Marginalisierung von Frauen* in unserem Wirtschaftssystem, den Wirtschaftswissenschaften und in der Forschung 


Frauen* und überwiegend von Frauen* verrichtete Arbeit erfahren eine viel zu geringe gesellschaftliche Wertschätzung. Dies schlägt sich u.a. in der überproportionalen Arbeitsbelastung von Frauen*, in Lohnunterschieden aber insbesondere auch in wirtschaftspolitischen Zielsetzungen und der wirtschaftswissenschaftlichen Beachtung nieder. Ein Grund hierfür könnte sein, dass Frauen* an entscheidenden ökonomischen Stellschrauben und in den Wissenschaften massiv unterrepräsentiert sind. Der Anteil weiblicher Studierender in den Wirtschaftswissenschaften sowie den machtvollsten Ämtern von Volkswirt*innen ist marginal.

In diesem zweiteiligen Vortrag soll es zunächst eine Einführung in die feministische Ökonomik geben. Diese Denkschule untersucht dezidiert die Rolle von Frauen* und die von ihnen verrichtete Arbeit in unserem Wirtschaftssystem. Im zweiten Teil treten dann die Rolle von Frauen* in den Wirtschaftswissenschaften und der Forschung in den Fokus.


*Bei Bedarf an Kinderbetreuung, bitte Mail an rgl@stura.uni-leipzig.de
*Spielsachen vor Ort vorhanden
*Dont hesitate to bring your babies