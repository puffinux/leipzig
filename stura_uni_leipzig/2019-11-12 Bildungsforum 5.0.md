---
id: "2428350307408739"
title: Bildungsforum 5.0
start: 2019-11-12 09:00
end: 2019-11-12 17:00
address: Campus Jahnallee
link: https://www.facebook.com/events/2428350307408739/
image: 75643585_559431171497317_6206667998342676480_n.jpg
teaser: Bock auf Bildung? Austausch? Input? Demokratie? Kritik und Mitgestalten? Komm
  vorbei und nimm Teil am Bildungsforum 5.0! Workshops und Open Space biet
isCrawled: true
---
Bock auf Bildung? Austausch? Input? Demokratie? Kritik und Mitgestalten? Komm vorbei und nimm Teil am Bildungsforum 5.0! Workshops und Open Space bieten Raum für Diskussion und DEIN ENGAGEMENT. 

Folgende Workshops erwarten uns (detailliertere Workshop-Infos auf unserer offiziellen BiFo Seite) :

1. Sexuelle Orientierung und Geschlechtlichkeit als Thema in der Schule
2. Service Learning
3. Gendergerechte Sprache
4. Kita-Revolution
5. Nachhaltige Entwicklung
6. Benotungs-, Konkurrenz- und Selektionsdruck in Bildungsstätten
7. Verbeamtung Lehrer*innen Pro und Contra
8. Schule in Demokratie - Demokratie in Schule
9. Arbeitsorientierte Bildung
10. Ein Schritt hin zu inklusiveren Hochschule
11. Zirkuspädagogik
12. Soziokratie
13. Forschungsethische Grundprinzipien

Macht mit und (werdet) inspiriert!