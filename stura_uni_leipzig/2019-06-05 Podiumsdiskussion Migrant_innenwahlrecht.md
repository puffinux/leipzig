---
id: '790283884705962'
title: 'Podiumsdiskussion: Migrant_innenwahlrecht'
start: '2019-06-05 19:00'
end: '2019-06-05 21:00'
locationName: GWZ- Das Geisteswissenschaftliche Zentrum
address: 'Beethovenstr. 15, 04107 Leipzig'
link: 'https://www.facebook.com/events/790283884705962/'
image: 61052810_713104609108276_6367156245995978752_n.jpg
teaser: 'Das Wahlrecht ist nicht nur ein politisches Recht, es ist auch eine Form der Anerkennung als vollwertiges Mitglied der Gemeinschaft. In viele Länder d'
recurring: null
isCrawled: true
---
Das Wahlrecht ist nicht nur ein politisches Recht, es ist auch eine Form der Anerkennung als vollwertiges Mitglied der Gemeinschaft. In viele Länder der EU dürfen Migrant_innen aus nicht EU Mitgliedstaaten in kommunal Eben ihre Repräsentant_innen wählen, in Deutschland ist ist dieses Recht ausschlieslich für deutsche Staatsbürger_innen und EU-Migrant_innen. Warum ist das so? Was können wir dagegen tun? Und, wenn wir nicht wählen dürfen, welche andere Möglichkeiten der politische Partizipation haben wir?

In diesen Podiumsdiskussion wollen wir uns mit diese und weitere Fragen auseinandersetzen.

Habt ihr Fragen, dass ihr gerne an den Podiumsteilnehmer_innen stellen möchtet? 
Schreibt uns an: yes.weareauslaender@stura.uni-leipzig.de

Veranstaltungsort: Hörsaal GWZ (ebenerdig ohne Treppe zugänglich)