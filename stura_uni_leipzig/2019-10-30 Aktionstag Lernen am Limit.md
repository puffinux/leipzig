---
id: "2481332875317865"
title: Aktionstag Lernen am Limit
start: 2019-10-30 11:00
end: 2019-10-30 17:00
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/2481332875317865/
image: 74565400_2433555150027660_3628918374920093696_n.jpg
teaser: "Jedes Jahr der gleiche Schreck: Student*innen sitzen auf dem Boden des
  Hörsaals,finden keine Wohnung und von welchem Geld sollen sie eigentlich
  leben?"
isCrawled: true
---
Jedes Jahr der gleiche Schreck: Student*innen sitzen auf dem Boden des Hörsaals,finden keine Wohnung und von welchem Geld sollen sie eigentlich leben? Die prekären Rahmenbedingungen sind bekannt und werden Jahr für Jahr schlimmer!

Deshalb wird es an diesem Mittwoch auf dem Campusinnenhof der Uni den Aktionstag geben. Das vorläufige Programm sieht folgendermaßen aus:

Um 11 Uhr wird die wunderbare Lina Wedemeyer den Aktiontags mit einem Beitrag über ihr Leben in einer Leipziger WG eröffnen.

danach wird es einen Mitbringflohmarkt, WG-Börse, Kaffee und nettes Beisammensitzen geben!

ab 16 Uhr laden wir euch zu einer gemeinsamen Kundgebung ein, bei der wir unseren Unmut über die immer schlechter werdenden Lebens- und Lernbedingungen von Studierenden laut machen werden!


Mehr Informationen zur Kampagne Lernen am Limit und die Kernforderungen findet ihr hier: https://lernenamlimit.de/