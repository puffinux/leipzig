---
id: '646124675863144'
title: 'Selbstbestimmte Norm. Feminismus, Pränataldiagnostik, Abtreibung'
start: '2019-06-19 19:00'
end: '2019-06-19 21:00'
locationName: null
address: 'Hauptcampus Augustusplatz, Hörsaal 12'
link: 'https://www.facebook.com/events/646124675863144/'
image: 61101894_1309009485922584_222921210822918144_n.jpg
teaser: Kirsten Achtelik ist Diplom-Sozialwissenschaftlerin und arbeitet als freie Journalistin und Autorin. Ihre Arbeitsschwerpunkte sind u.a. feministische
recurring: null
isCrawled: true
---
Kirsten Achtelik ist Diplom-Sozialwissenschaftlerin und arbeitet als freie Journalistin und Autorin. Ihre Arbeitsschwerpunkte sind u.a. feministische Theorien und Bewegungen, Schnittstellen mit andern sozialen Bewegungen v.a. der Behindertenbewegung und Kritik der Gen- und Reproduktionstechnologien. Außerdem recherchiert sie zu Abtreibungsgegner*innen und „Lebensschutz“-Bewegung.

2015 erschien ihr Buch "Selbstbestimmte Norm. Feminismus, Pränataldiagnostik, Abtreibung". 

Aus dem Klappentext: Sollen Feministinnen jede Art von Abtreibung verteidigen? Können Entscheidungen überhaupt selbstbestimmt getroffen werden? Welche Art von Wissen entsteht durch pränatale Untersuchungen? Dienen sie der Vorsorge oder sind sie behindertenfeindlich?
Kirsten Achtelik lotet in ihrem Buch das Spannungsfeld zwischen den emanzipatorischen und systemerhaltenden Potenzialen des feministischen Konzepts „Selbstbestimmung“ in Bezug auf Abtreibung aus. So mischt sie sich in die aktuellen feministischen Debatten um reproduktive Rechte ein, die mit den zunehmenden Aktivitäten und Demonstrationen von „Lebensschützern“ wieder aufgeflammt sind.
Zugleich ist es ihr Anliegen, einer neuen Generation von Aktivistinnen und Aktivisten die Gemeinsamkeiten und Konflikte der Frauen- und Behindertenbewegung sowie die inhaltlichen Differenzen zwischen Frauen mit und ohne Behinderung verständlich zu machen. Vor allem aber stellt sich Achtelik der dringend zu klärenden Frage, wie ein nicht selektives und nicht individualisiertes Konzept von Selbstbestimmung gedacht und umgesetzt werden kann.