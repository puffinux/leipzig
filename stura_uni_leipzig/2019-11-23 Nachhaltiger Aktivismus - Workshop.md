---
id: "405195393769109"
title: Nachhaltiger Aktivismus - Workshop
start: 2019-11-23 14:00
end: 2019-11-23 16:00
address: StuRa Uni Leipzig
link: https://www.facebook.com/events/405195393769109/
image: 73497925_2485926244790550_5654784534965649408_n.jpg
teaser: Workshop -----------------  Nachhaltiger Aktivismus - Hochschulpolitik für
  Alle gestalten!  In diesem Workshop lernt ihr, wie ihr eure politische Stru
isCrawled: true
---
Workshop
-----------------

Nachhaltiger Aktivismus - Hochschulpolitik für Alle gestalten!

In diesem Workshop lernt ihr, wie ihr eure politische Struktur noch verbessern könnt. Von Transformativen Aktivismus, Resilienz und Widerstandskraft über Awareness bis hin zu inkludierenden Plenumsstrukturen bietet der Workshop viele Ansatzpunkte, wie ihr euren eigenen Aktivismus so gestaltet, dass Alle daran teilhaben können!

14:00 - 16:00 Uhr
im Ziegenleedersaal des StuRa
direkt neben dem Hörsaalgebäude, Zugang über den Campusinnenhof!

