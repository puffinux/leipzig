---
id: "1000347233635999"
title: Vorstellungsstraße - AGs, Vereine und Initiativen stellen sich vor!
start: 2019-11-04 10:00
end: 2019-11-04 15:00
address: StuRa Uni Leipzig
link: https://www.facebook.com/events/1000347233635999/
image: 74419500_2456056261110882_4035028221614358528_n.jpg
teaser: Auf der Vorstellungsstraße des Student_innenRats hast du die Möglichkeit, dich
  in entspannter Atmosphäre über die verschiedensten Leipziger Vereine, A
isCrawled: true
---
Auf der Vorstellungsstraße des Student_innenRats hast du die Möglichkeit, dich in entspannter Atmosphäre über die verschiedensten Leipziger Vereine, AGs und Initiativen zu informieren. Du erhältst einen Überblick über Leipzigs vielseitige Kulturlandschaft, lernst spannende Vereine der Stadt kennen und erfährst, wie du dich selber einbringen kannst! Zu Beginn eines jeden Semesters organisiert der StuRa diese Vorstellungsstraße. Sie findet zwischen 10:00 Uhr und 15:00 Uhr im Foyer des Hörsaalgebäudes statt.