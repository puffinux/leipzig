---
id: '375515669960296'
title: Mietsprechstunde
start: '2019-04-25 12:00'
end: '2019-04-25 14:00'
locationName: null
address: 'Seminargebäude Leipzig, Raum S 001'
link: 'https://www.facebook.com/events/375515669960296/'
image: 57839874_628842997537029_8105306670051098624_n.jpg
teaser: 'Neben dem Mieterverein, dem Miettreff Leipziger Osten, der Verbraucherzenzrale oder der Mietsprechstunde gibt es nun eine neue Möglichkeit sich in Sac'
recurring: null
isCrawled: true
---
Neben dem Mieterverein, dem Miettreff Leipziger Osten, der Verbraucherzenzrale oder der Mietsprechstunde gibt es nun eine neue Möglichkeit sich in Sachen Wohnen und Mieten beraten zu lassen:

Immer am letzten Donnerstag eines Monats können Menschen nun sowohl in rechtlichen Fragen Hilfe bekommen, als auch Unterstützung erhalten von einer Person, die in Mietstreitigkeiten bereits persönlich Erfahrungen gesammelt hat.

Wenn die eigene Wohnung als /der/ private Rückzugsraum bedroht wird, gilt es auch Ängste aufzufangen. Die Mietberatung soll daher neben der juristischen Erstberatung auch dabei helfen, mutig zu bleiben, indem beispielsweise Tipps zur Stärkung der Mietergemeinschaft im Haus oder der Kontakt zu solidarischen Gruppen vermittelt werden.

Die erste Mietberatung findet am 25. April von 12:00 - 14:00 Uhr im Beratungsraum des StuRa im Seminargebäude, S 001 am Hauptcampus, Universitätsstraße 1 statt.

Bei Interesse und Bedarf komm vorbei!

Dieses Angebot wird in Kooperation von Leipzig für Alle: Aktionsbündnis Wohnen, DGB- Hochschulgruppe, GEW-Hochschulgruppe und dem StuRa der Uni Leipzig ermöglicht.