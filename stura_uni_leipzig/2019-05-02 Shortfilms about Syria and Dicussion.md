---
id: '414426739388507'
title: Shortfilms about Syria and Dicussion
start: '2019-05-02 19:00'
end: '2019-05-02 21:00'
locationName: null
address: Hörsaalgebäude der Universität Leipzig Hörsaal 10
link: 'https://www.facebook.com/events/414426739388507/'
image: 58419662_696731067412297_7479689127986724864_n.jpg
teaser: 'Before I Forget  by Razan Hassan What if you could never return to the place where your memories were made? Razan Hassan''s documentary short, Before I'
recurring: null
isCrawled: true
---
Before I Forget 
by Razan Hassan
What if you could never return to the place where your memories were made? Razan Hassan's documentary short, Before I Forget, is an autobiographical exploration of how memory and identity are fractured by forced displacement. Following her journey from Syria to the Netherlands, Razan begins to question the veracity of her recollections of home, family, and childhood. She tries to recapture a fragment of this lost past through digitizing the sole memento she carried with her from Syria: a tape recording of a poem she recited as a 3 year-old child - only to find that the tape has been corrupted and its contents are forever lost. Razan takes us along on an inner journey of emotional trauma and conflict visualized through psychedelic club scenes, archived family footage, and a news clip of her family home in Damascus collapsing during a raid by regime forces. While her daily life in Amsterdam remains unfamiliar, she is able to forge new memories in unexpected places.

People of the Wasteland 
by Heba Khaled
People of the Wasteland is a short-film of 24 minutes that focuses on the concept of war. It presents footage filmed and gathered during more than two years through a GoPro camera placed on the heads of different Syrian fighters in the enemy region. The film presents the violence and the horrors of war in a first-perspective point-of-view. The editing uses reality and experimental styles in order to reflect the absurdity of war. The location of the film is intentionally unclear, appearing only as the "wasteland", with the aim of making the concept of war a global concept, that not only affects Syrians but all humans.

Discussion with Razan Hassan about Syria 