---
id: '475739436499129'
title: Warum noch Schulen und Universitäten? Es gibt doch das Internet!
start: '2019-06-26 19:15'
end: '2019-06-26 20:45'
locationName: null
address: Neues Seminargebäude Universität Leipzig - Raum 126
link: 'https://www.facebook.com/events/475739436499129/'
image: 61644854_2535123229854550_7462704271792799744_o.jpg
teaser: 'Vortrag und Gespräch mit Christoph Türcke, Prof. em. für Philosophie der Hochschule für Grafik und Buchkunst Leipzig sowie Autor zahlreicher Sachbüche'
recurring: null
isCrawled: true
---
Vortrag und Gespräch mit Christoph Türcke, Prof. em. für Philosophie der Hochschule für Grafik und Buchkunst Leipzig sowie Autor zahlreicher Sachbücher wie dem diesjährigen Buch »Lehrerdämmerung. Was die neue Lernkultur in den Schulen anrichtet«. Am 26.06. wird er an deiner Universität zu Gast sein und über folgende Inhalte mit uns sprechen -

Überall werden Bildungsinhalte zu messbaren Kompetenzen formalisiert. Nur um maximale Vergleichbarkeit und Gerechtigkeit im Bildungswesen herzustellen? Keineswegs. Zu offenkundig dient die Messbarkeit der Herstellung arbeitsmarktrelevanter, geldwerter, jobfähiger Verhaltensweisen. Und je messbarer Bildung wird, desto entbehrlicher wird pädagogische Urteilskraft. Software kann die Auswahl der Lerninhalte, das Feedback, die Benotung übernehmen. Lehrer werden zu Kompetenzerzeugungsgehilfen. Wo alle Bildungsinhalte online stehen, sind zudem (Hoch-)Schulgebäude als Orte regelmäßiger Zusammenkunft überflüssig. Das Fernstudium wird zum Normalfall des Lernens – vom Grundschulalter an.