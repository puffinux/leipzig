---
id: '685495125225118'
title: Diskussionsrunde mit den Kandidierenden für den Senat
start: '2019-06-03 19:00'
end: '2019-06-03 21:00'
locationName: null
address: 'HS 6, Augustusplatz'
link: 'https://www.facebook.com/events/685495125225118/'
image: 61279985_2177892288927282_7604037678270513152_n.jpg
teaser: Die Hochschulwahlen stehen vor der Tür und neben den Fachschaftsräten könnt ihr auch die Studierendenvertreter*innen im Akademischen Senat wählen. Der
recurring: null
isCrawled: true
---
Die Hochschulwahlen stehen vor der Tür und neben den Fachschaftsräten könnt ihr auch die Studierendenvertreter*innen im Akademischen Senat wählen. Der Senat ist sozusagen das Uniparlament. Dort beraten Profs, wissenschaftliches und technisches Personal, Studierende und das Rektorat über Fragen, die die ganze Universität betreffen. Leider ist der Senat kein echtes Parlament und hat vor allem im Zuge der letzten Überarbeitung des Hochschulgesetzes viel an Macht gegenüber dem Rektorat verloren. Außerdem sind Studierende stark unterrepräsentiert. Nichtsdestotrotz ist es die einzige Möglichkeit, dass wir als gesamte Studierendenschaft Vertreter*innen auf höchster Universitätsebene wählen. Diese Möglichkeit sollten wir nutzen und gleichzeitig gemeinsam weiter an der Demokratisierung der Universität arbeiten. Die Kandidierenden der zur Wahl stehenden Listen werden sich am Montag den 3.6. 19 Uhr der Studierendenschaft vorstellen. Kommt vorbei, informiert euch und stellt fleißig Rückfragen.

Folgende Studierende werden ihre Wahllisten vertreten und stehen für Rückfragen bereit:

Jenny Schumann (Liste Freier Campus)

Paul Reinhardt (SolAR - Solidarisch. Antifaschistisch. Radikal.)

Matthias Bohlmann (CampusUnion - Die Realos)

Christopher Hermes (Offene Uni: Juso Hochschulgruppe)

Nathalie Steinert (Feministisch. Antirassistisch. Klimagerecht.)