---
id: '264984867722139'
title: 'Verfassung schützen, Verfassungsschutz abschaffen?'
start: '2019-05-06 15:00'
end: '2019-05-06 17:00'
locationName: null
address: Campus Augustusplatz / HS 11
link: 'https://www.facebook.com/events/264984867722139/'
image: 54729337_907066462796887_4598776931578544128_o.jpg
teaser: 'Mo, 06.05., 17 – 19 Uhr:  Verfassung schützen, Verfassungsschutz abschaffen?  KEW / Campus Augustusplatz / HS 11  Die letzten Jahre haben deutlich gez'
recurring: null
isCrawled: true
---
Mo, 06.05., 17 – 19 Uhr:

Verfassung schützen, Verfassungsschutz abschaffen?

KEW / Campus Augustusplatz / HS 11

Die letzten Jahre haben deutlich gezeigt, der Verfassungsschutz (VS) verfolgt eine eigene politische Agenda. Seien es die Verstrickungen in den NSU-Komplex, die Verbindung zur extremen Rechten oder die Causa Maaßen, so kann nicht mehr von einer neutralen Position dieser Institution ausgegangen werden. Die Einmischung in die KEW 2018 oder der im November 2018 enttarnte V-Mann des niedersächsischen Landesamtes für Verfassungsschutz an der Universität Göttingen zeigen, wie sehr der Einfluss des VS auch Studierende betrifft. Diese Problematik möchten wir mit Expert*innen und Betroffenen erörtern:

* Felix Fink (StuRa Uni Leipzig)
* Vertreter*in Basisdemokratische Linke Göttingen - IL
* Frank Schubert (Engagierte Wissenschaft e.V., Leipzig)
* Prof. Dr. Rebecca Pates (Institut für Politikwissenschaft, Uni Leipzig)

Im Anschluss an die Veranstaltung wird die Ausstellung "Versagen mit System - Geschichte und Wirken des Verfassungsschutzes" des Forums für kritische Rechtsextremismusforschung (FKR) eröffnet. Die Ausstellung zeigt auf, dass es sich bei den Skandalen des Verfassungsschutzes nicht um Einzelfälle handelt, sondern dass sein systematisches Versagen in seiner Geschichte, ideologischen Ausrichtung und undemokratischen Arbeitsweise angelegt ist. Mit Hintergrundinformationen und Beispielen wird die Entstehung und Entwicklung des VS als politische Behörde, seine Verstrickungen in den NSU-Komplex sowie die lange Skandalgeschichte des Geheimdienstes dargestellt, die ihn im Gesamtbild selbst als Gefahr für die Demokratie erscheinen lassen.

Die Ausstellung ist  bis zum 17.5. im Hörsaalgebäude, 2. Etage zu besichtigen. Sie kann bei Weiterdenken - Heinrich-Böll-Stiftung Sachsen kostenfrei entliehen werden. Infos dazu unter: http://www.weiterdenken.de/de/versagen-mit-system

Eine Veranstaltung in Kooperation mit dem Forum für kritische Rechtsextremismusforschung bei Engagierte Wissenschaft e.V. und Weiterdenken - Heinrich-Böll-Stiftung Sachsen.