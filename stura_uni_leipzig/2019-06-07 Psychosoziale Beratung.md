---
id: '630568587407848'
title: Psychosoziale Beratung
start: '2019-06-07 13:00'
end: '2019-06-07 14:00'
locationName: null
address: 'Universitätsstraße 1, 04109 Leipzig, Deutschland, Raum S. 001'
link: 'https://www.facebook.com/events/630568587407848/'
image: 60336109_706904399726991_1150353814243508224_o.jpg
teaser: 'Beatrix Stark ist eure Psychosoziale Beraterin, sie bietet eine erste Anlaufstelle für alle Studierenden, die Hilfe und Unterstützung bei der Gestaltu'
recurring: null
isCrawled: true
---
Beatrix Stark ist eure Psychosoziale Beraterin,
sie bietet eine erste Anlaufstelle für alle Studierenden, die Hilfe und Unterstützung bei der Gestaltung und Bewältigung ihres universitären Alltags suchen oder ein persönliches Problem mit sich herum tragen.
Fragen sowie eine Terminabsprachen können gerne vorab per e-mail an: beatrix.stark@stura.uni-leipzig.de oder ps.b@stura.uni-leipzig.de gerichtet werden.
Am Mittwoch trefft ihr Sie von 10:00 bis 11:00 Uhr im Raum S001 auch ohne Termin an. In dieser Zeit erreicht ihr sie telefonisch unter: 0341 - 97 37 869.

Wichtig: Dieses Beratungsangebot ersetzt kein professionelles Therapieangebot oder Vergleichbares.

Unter anderen könnt ihr mit folgenden Themen zu ihr kommen:
- Suchtprobleme
- Lernstress
- Prüfungsangst
- oder anderen psychischen Problemen

Kontakt:
ps.b@stura.uni-leipzig.de
+49 341 97 37 869