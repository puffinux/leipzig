---
id: '652649185157442'
title: Rechtsberatung
start: '2019-06-17 19:00'
end: '2019-06-17 21:00'
locationName: null
address: 'Universitätsstraße 1, 04109 Leipzig, Deutschland, S 001'
link: 'https://www.facebook.com/events/652649185157442/'
image: 43649909_595215450895887_1293430225824120832_o.jpg
teaser: Tom Hanke und Christoph Naundorf sind euer Rechtsberater. Tom ist Rechtsanwalt und hat bis 2013 an der Uni Leipzig Jura studiert. Christoph hat ebenfa
recurring: null
isCrawled: true
---
Tom Hanke und Christoph Naundorf sind euer Rechtsberater. Tom ist Rechtsanwalt und hat bis 2013 an der Uni Leipzig Jura studiert. Christoph hat ebenfalls Jura studiert und befindet sich gerade im Referendariat. Er wird in seiner Beratung von Tom als Volljuristen betreut.

Die Rechtsberatung bietet Hilfe in juristischen Angelegenheiten.

Die Beratung umfasst zivilrechtliche (u.a. Mietrecht, Arbeitsrecht,Verkehrsrecht, familienrechtliche Probleme etc.), strafrechtliche (z.B. polizeiliche Vorladung, Verkehrskontrolle, Hausdurchsuchung etc.) oder auch verwaltungsrechtliche (v.a. Rundfunkbeitrag, Prüfungsanfechtungen, Widerspruchsverfahren) Problemstellungen.

Die Beratung vor Ort (SG 001) findet Montags zwischen 19-21 Uhr  und jeden 2. und 4. Mittwoch im Monat von 17 -19 Uhr statt . Hierfür ist i.d.R. kein Termin erforderlich. Email-Anfragen werden jedoch durchgehend beantwortet, soweit dies möglich ist.
Tom Hanke ist euer Rechtsberater. Er ist Rechtsanwalt und hat bis 2013 an der Uni Leipzig Jura studiert.
