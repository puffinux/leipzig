---
id: '664406757343215'
title: 'Ein Europa für Alle – Deine Stimme gg Nationalismus: Leipzig'
start: '2019-05-19 10:00'
end: '2019-05-19 15:00'
locationName: null
address: 'Wilhelm-Leuschner-Platz, 04107 Leipzig'
link: 'https://www.facebook.com/events/664406757343215/'
image: 54434818_858846071132291_2543713858732687360_n.jpg
teaser: 'Auch wir in #Leipzig finden es wichtig, direkt vor den #Europawahlen gegen Nationalismus zu protestieren. Am Sonntag, den 19. Mai, gehen wir gemeinsam'
recurring: null
isCrawled: true
---
Auch wir in #Leipzig finden es wichtig, direkt vor den #Europawahlen gegen Nationalismus zu protestieren. Am Sonntag, den 19. Mai, gehen wir gemeinsam für ein ANDERES Europa demonstrieren. Denn unser Europa der Zukunft...

● verteidigt Humanität und Menschenrechte. Statt seine Grenzen zur Festung auszubauen und Menschen im Mittelmeer ertrinken zu lassen, garantiert es sichere Fluchtwege, das Recht auf Asyl und faire Asylverfahren für Schutzsuchende.

● steht für Demokratie, Vielfalt und Meinungsfreiheit. Statt vor allem auf mächtige Wirtschaftslobbys hört es auf die Stimmen seiner Bürger*innen. Es verteidigt den Rechtsstaat, wird demokratischer und gibt dem Europaparlament mehr Einfluss. Es fördert Toleranz und gewährleistet die Vielfalt an Lebensentwürfen, Geschlechtergerechtigkeit, die Freiheit von Kunst, Kultur und Presse sowie eine lebendige Zivilgesellschaft.

● garantiert soziale Gerechtigkeit. Statt Privatisierung, Deregulierung und neoliberale Handelsabkommen voranzutreiben, wird es ein Gegengewicht zum massiven Einfluss der Konzerne. Es baut auf Solidarität und sichert Arbeitnehmer*innenrechte. Allen Menschen wird das Recht auf Bildung, Wohnen, medizinische Versorgung und soziale Absicherung sowie ein Leben frei von Armut garantiert. Europa muss hier seiner Verantwortung gerecht werden - bei uns und weltweit.

● treibt einen grundlegenden ökologischen Wandel und die Lösung der Klimakrise voran. Statt auf fossile und nukleare Energien setzt es auf erneuerbare Energien. Es ermöglicht eine bäuerliche, klimagerechte Landwirtschaft. Gleichzeitig sorgt es dafür, dass der Wandel sozial abgefedert und gute Arbeit geschaffen wird.

Deine Stimme ist wichtig! Wir sehen uns am 19. Mai auf der Strasse!

#1EuropaFürAlle
#DeineStimmeGegenNationalismus

Den Aufruf findest du hier:  www.ein-europa-fuer-alle.de
Alle Infos: www.ein-europa-fuer-alle.de/leipzig
Folge uns auf Twitter: www.twitter.com/1Europa_leipzig