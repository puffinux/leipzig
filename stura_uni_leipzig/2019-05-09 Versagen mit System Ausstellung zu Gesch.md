---
id: '314252446172651'
title: 'Versagen mit System: Ausstellung zu Geschichte und Wirken des VS'
start: '2019-05-09 05:00'
end: '2019-05-09 20:00'
locationName: null
address: 'Campus Augustusplatz, Hörsaalgebäude, 2. Stock'
link: 'https://www.facebook.com/events/314252446172651/'
image: 57221411_921147124722154_4244693372360458240_n.jpg
teaser: Der Verfassungsschutz (VS) gilt als »Frühwarnsystem« gegen Bedrohungen der verfassungsmäßigen Ordnung in der Bundesrepublik. Seit der Gründung der VS-
recurring: null
isCrawled: true
---
Der Verfassungsschutz (VS) gilt als »Frühwarnsystem« gegen Bedrohungen der verfassungsmäßigen Ordnung in der Bundesrepublik. Seit der Gründung der VS-Ämter werden jedoch immer wieder Skandale, Kompetenzüberschreitungen und Grundrechtsverletzungen bekannt. Im November 2018 musste der damalige Präsident des VS, Hans-Georg Maaßen, seinen Hut nehmen, nachdem er sich rechter Verschwörungstheorien bedient hatte. Spätestens mit der Selbstenttarnung des Terrornetzwerks Nationalsozialistischer Untergrund (NSU) ist deutlich geworden: Der VS hat als Frühwarnsystem versagt. Durch den Einsatz sogenannter V-Leute förderte und finanzierte er sogar indirekt den Aufbau neonazistischer Strukturen.

Affären des VS werden in den öffentlichen Debatten oft als Pannen behandelt und geraten schnell wieder in Vergessenheit. Eine Betrachtung über einzelne Fälle hinaus zeigt jedoch, dass sich bestimmte Muster wiederholen. Tatsächliche Bedrohungen für die Demokratie, etwa durch militante Neonazis, verfolgt die Behörde nur ungenügend. Gleichzeitig werden an anderen Stellen Gefahren konstruiert, etwa wenn der VS es als seine Aufgabe betrachtet, linke Punkbands zu überwachen.

Die Fokussierung der Behörde auf die Beobachtung von »Verfassungsfeinden« an den »Rändern« der Gesellschaft folgt der Logik des viel kritisierten Extremismusmodells – der fragwürdigen Arbeitsgrundlage des VS. Dies führt dazu, dass Alltags­rassismus und menschenfeindliche Einstellungen in der »Mitte« der Gesellschaft für den VS keine Rolle spielen. Wer ihrer Ansicht nach als Gefahr für die Demokratie gilt und wer nicht, vermittelt die Behörde zudem immer häufiger im Rahmen von Bildungsangeboten – und greift so als politischer Akteur in den gesellschaftlichen Diskurs ein.

Im Kern bleibt der VS ein Geheimdienst und entzieht sich dadurch einer wirksamen demokratischen Kontrolle. Unter diesen Umständen ist der nächste große VS-Skandal nur eine Frage der Zeit. Ein Blick zurück zeigt: Das Handeln des VS schadet der Demokratie mehr als es ihr nützt. Zeit, ihn abzuschaffen.

Gliederung der Ausstellung:

- Daten und Fakten
- Entstehung des Verfassungsschutzes
- Theorie und Praxis des Verfassungsschutzes
- V-Leute und der NSU-Komplex
- Der Geheimdienst in der politischen Bildung
- Perspektiven: Wie weiter mit dem VS?

Die Ausstellung zeigt auf, dass es sich bei den Skandalen des VS nicht um Einzelfälle handelt, sondern dass sein systematisches Versagen in seiner Geschichte, ideologischen Ausrichtung und undemokratischen Arbeitsweise angelegt ist. Mit Hintergrundinformationen und Beispielen wird die Entstehung und Entwicklung des VS als politische Behörde, seine Verstrickungen in den NSU-Komplex sowie die lange Skandalgeschichte des Geheimdienstes dargestellt, die ihn im Gesamtbild selbst als Gefahr für die Demokratie erscheinen lassen.

Die Ausstellung ist ein Kooperationsprojekt des Forums für kritische Rechtsextremismusforschung (FKR) des Engagierte Wissenschaft e.V. in Leipzig und Weiterdenken - Heinrich-Böll-Stiftung Sachsen.  Sie besteht aus 24 Stoffbannern und einem Stellsystem aus Holzrahmen. Die Ausstellung  kann kostenfrei entliehen werden. Informationen dazu unter: http://www.weiterdenken.de/de/versagen-mit-system