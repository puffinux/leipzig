---
id: "980318242310066"
title: Konstituierende Sitzung der MHG Leipzig
start: 2019-10-21 18:00
end: 2019-10-21 21:00
address: Universität Leipzig, neues Seminargebäude Raum S015
link: https://www.facebook.com/events/980318242310066/
image: 72647673_1409681865854299_6287835458533588992_n.jpg
teaser: As-Salamu alaikum liebe muslimische Studierende und Interessierte in Leipzig
  und Umgebung,  wir veranstalten in Kooperation mit dem Student_innenRat d
isCrawled: true
---
As-Salamu alaikum liebe muslimische Studierende und Interessierte in Leipzig und Umgebung,

wir veranstalten in Kooperation mit dem Student_innenRat der Universität Leipzig (StuRa) und dem Referat für ausländische Studierende (RAS) am 21. und 22. Oktober 2019 eine konstituierende Sitzung, in der wir die MHG Grundordnung neu gestalten und abstimmen. Im Anschluss daran findet die Wahl der Ämter statt.

Alle Muslime, die an der Universität Leipzig, sowie an der HTWK oder an der Universität Halle und Umgebung studieren, promovieren oder arbeiten, sind als Mitglieder der Hochschulgemeinde, herzlich eingeladen an dieser Sitzung teilzunehmen.

Wir treffen uns am 21. und 22. Oktober 2019 jeweils um 18 Uhr. Es besteht die Möglichkeit sich vor Ort für die Teilnahme anzumelden.

Ziel ist es, an den beiden Tagen eine verbindliche MHG Grundordnung aufzustellen auf der die weitere MHG Arbeit basiert. Unser Vorschlag für die Grundordnung ist jetzt auf der MHG Leipzig Seite. Wir würden uns sehr über eure Änderungsvorschläge freuen.

Die Wahl der Ämter findet im Anschluss an die Abstimmung der Grundordnung statt.
Gerne könnt ihr vor Ort für die vorgeschlagenen Ämter kandidieren. Falls ihr es nicht schafft, an der Abstimmung vor Ort teilzunehmen, könnt ihr uns hier auf Facebook kontaktieren um eine gemeinsame Lösung zu finden.

Unser Vorschlag für die Ämterverteilung:
Sprecher: 3 Personen
Finanzen: 2 Personen
E-Mail Konto: 2 Personen
Social-Media: 2-4 Personen
Veranstaltungsorganisatoren: 2-4 Personen

Weitere Informationen findet ihr in der Satzung des Student_innenRates der Universität Leipzig unter §15 zu den Arbeitsgruppen.

Wir hoffen auf eure tatkräftige Unterstützung, damit die MHG auch in Zukunft erfolgreich arbeiten kann und freuen uns auf zwei produktive Tage mit euch !

Eure MHG Leipzig