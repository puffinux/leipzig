---
id: '596991127477804'
title: Eltern-Kind-Café
start: '2019-07-03 16:30'
end: '2019-07-03 18:30'
locationName: Mensa Am Park
address: 'Universitätsstraße 5, 04109 Leipzig'
link: 'https://www.facebook.com/events/596991127477804/'
image: 64709629_2223152161137109_7951316029311811584_n.jpg
teaser: 'Heraus zum kommenden studentischen Eltern- Kind Café!  Wann? Mittwoch, 3. Juli 2019 ab 16.30.  Wo?  Innenhof Campus Augustusplatz (vor dem StuRa)  Wer'
recurring: null
isCrawled: true
---
Heraus zum kommenden studentischen Eltern- Kind Café!

Wann? Mittwoch, 3. Juli 2019 ab 16.30.

Wo?  Innenhof Campus Augustusplatz (vor dem StuRa)

Wer? Alle die Lust haben auf (kritischen) Austausch zum Thema
"Studieren mit Kind"

Was? Bastel/ Spieleecke, Hörspielteppich, Malkreide, Transpis malen,
Kaffe& Kuchen u.A-

Warum? Weil ein halbjähriges Familienfrühstück nicht ausreicht um gleiche Partizipationsmöglichkeiten an der Uni zu schaffen!
Weil es vielmehr tiefgreifende strukturelle Veränderung braucht um Studierenden mit
Kind ein stressfreieres Studium zu ermöglichen! Weil wir es selbst in
die Hand nehmen müssen, für diese Veränderung zu streiten!

Darum wollen wir mit euch ins Gespräch kommen, Ideen austauschen,
Pläne schmieden und über unsere Forderungen sprechen:

1. STILL- UND RUHERÄUME FÜR ALLE DIE SIE DRINGEND BRAUCHEN!

2. KINDERFREUNDLICHE ZIMMER AM HAUPTCAMPUS, DEN FAKULTÄTEN UND BIBLIOTHEKEN!

3. BETREUNGSANGEBOTE BEI ABENDVERANSTALTUNGEN UND VORTRAGSREIHEN AN DER UNI!

4. VERBESSERUNGEN DER STUDIENORGANISATION FÜR STUDIS MIT KIND (VORZUGSBELEGUNGSRECHT AUF VERANSTALTUNGEN WÄHREND DER KERNBERTREUNGSZEITEN)!

5. VERBESSERTE PARTIZIPATIONSMÖGLICHKEITEN FÜR STUDIS MIT KIND AM KULTURELLEN, SOZIALEN UND HOCHSCHULPOLITISCHEN LEBEN DER UNI!"
