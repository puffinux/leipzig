---
id: '578385382671712'
title: Eltern-Kind-Café
start: '2019-05-08 14:30'
end: '2019-05-08 16:30'
locationName: Mensa Am Park
address: 'Universitätsstraße 5, 04109 Leipzig'
link: 'https://www.facebook.com/events/578385382671712/'
image: 58610189_2135078109944515_778590009266536448_o.jpg
teaser: Der Verein studentischer Eltern und Pro Kids laden dieses Semester wieder gemeinsam zum Eltern-Kind-Café ein.  Die Termine dafür sind  Mittwoch 08.05.
recurring: null
isCrawled: true
---
Der Verein studentischer Eltern und Pro Kids laden dieses Semester wieder gemeinsam zum Eltern-Kind-Café ein.

Die Termine dafür sind
 Mittwoch 08.05. ab 16.30 Uhr
 Mittwoch 05.06. ab 16.30 Uhr
 Mittwoch 03.07. ab 16.30 Uhr 
in der Spielecke der Mensa am Park und bei gutem Wetter draußen.

Das Eltern-Kind-Café bietet studentischen Eltern die Möglichkeit, sich bei Café und Kuchen auszutauschen und untereinander zu Vernetzen, während ihr Kinder sich bei Spiel und Spaß vergnügen

Schaut vorbei! Wir freuen uns auch euch! :). 
