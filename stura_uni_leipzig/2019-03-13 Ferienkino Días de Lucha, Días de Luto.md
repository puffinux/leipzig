---
id: '412520499480792'
title: 'Ferienkino: Días de Lucha, Días de Luto'
start: '2019-03-13 16:00'
end: '2019-03-13 19:00'
locationName: null
address: 'Hörsaalgebäude der Universität Leipzig, HS4'
link: 'https://www.facebook.com/events/412520499480792/'
image: 53800860_2049912558391923_8860301550627586048_n.jpg
teaser: Das Plastikmeer von Almería (Spanien) ist Europas größte Anbaufläche für Treibhausgemüse. Etwa 100.000 migrantische Landarbeiter*innen arbeiten unter
recurring: null
isCrawled: true
---
Das Plastikmeer von Almería (Spanien) ist Europas größte Anbaufläche für Treibhausgemüse. Etwa 100.000 migrantische Landarbeiter*innen arbeiten unter widrigsten Bedingungen in den dortigen Gewächshäusern und Abpackhallen. Ausbeutung, Rassismus und Diskriminierung sind an der Tagesordnung. Ein großer Teil des produzierten Gemüses landet in deutschen Supermärkten. Doch immer wieder regt sich Widerstand…
Der Film ist im Rahmen Brigade Berta Cáceres 2017 entstanden und begleitet 22 Arbeiter*innen bei ihrem Protest mit der lokalen Landarbeitergewerkschaft SOC-SAT. Er gibt so einen tiefen Einblick in die Zustände der landwirtschaftlichen Produktion im Süden Spaniens und die Organisation von Widerstand gegen diese extreme Form der Ausbeutung.

Darauf folgt eine kleine Filmbesprechung und die Möglichkeit, Fragen zu stellen.