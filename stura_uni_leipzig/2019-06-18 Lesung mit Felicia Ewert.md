---
id: '2372253096383458'
title: Lesung mit Felicia Ewert
start: '2019-06-18 19:00'
end: null
locationName: null
address: Campus Augustusplatz Hörsaal 11
link: 'https://www.facebook.com/events/2372253096383458/'
image: 61757833_1315017388655127_4334647910437224448_n.jpg
teaser: Vortrag und Lesung werden sich mit offen transfeindlichen Feminismen und deren Strukturen und Organisationen beschäftigen. Die Referentin verweist auf
recurring: null
isCrawled: true
---
Vortrag und Lesung werden sich mit offen transfeindlichen Feminismen und deren Strukturen und Organisationen beschäftigen. Die Referentin verweist auf die Widersprüchlichkeiten im vermeintlich radikalen Anspruch von transfeindlichen Feminismen, auf biologistische und cisnormative Argumentationen. Zusätzlich stellt die Referentin dar, wie fest Biologismen und normierte Zweigeschlechtlichkeit in Gesellschaft und deshalb auch in Feminismen verwurzelt sind und dass ein Fokus auf offene Transfeindlichkeit zu kurz greift. So zeigt sie auch, was die vermeintlich respektvolle „Sex / Gender“ Aufteilung von Geschlecht, die in universitären Kontexten gegenwärtig ist, für transgeschlechtliche Menschen bedeutet. Wie tief deutsches Recht in die Leben von trans Personen eingreift, zeigt sie mit einem Überblick über das sogenannte „Transsexuellengesetz“ und widmet sich der Frage, was *wir alle* tun müssen, um bestehende geschlechtliche Vorstellungen zu realisieren und abzubauen.

Einlass 18:00 Uhr, Beginn 19:00 Uhr.