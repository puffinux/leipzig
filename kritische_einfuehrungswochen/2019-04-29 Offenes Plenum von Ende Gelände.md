---
id: '2369141173125689'
title: Offenes Plenum von Ende Gelände
start: '2019-04-29 18:00'
end: '2019-04-29 21:00'
locationName: null
address: Ery
link: 'https://www.facebook.com/events/2369141173125689/'
image: 56174978_906562312847302_1439763923422150656_o.jpg
teaser: Wir sind Teil des bundesweiten Ende Gelände Bündnisses und fordern mit Massenaktionen des zivilen Ungehorsams Klimagerechtigkeit und den sofortigen Ko
recurring: null
isCrawled: true
---
Wir sind Teil des bundesweiten Ende Gelände Bündnisses und fordern mit Massenaktionen des zivilen Ungehorsams Klimagerechtigkeit und den sofortigen Kohleausstieg! Als relativ neue Ortsgruppe wollen wir uns regional stärker vernetzen und vermehrt Aktionen in Leipzig und Umgebung planen - Du willst selbst aktiv werden? - Dich mit Klimagerechtigkeit beschäftigen, Demos organisieren oder Bagger blockieren? Dann komm vorbei!

Falls ihr nicht wisst, wo das Erytrosin ist, könnt ihr den genauen Ort auf Nachfrage erfahren.