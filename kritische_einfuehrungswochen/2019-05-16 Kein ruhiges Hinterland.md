---
id: '2272942799430617'
title: Kein ruhiges Hinterland
start: '2019-05-16 18:00'
end: '2019-05-16 21:00'
locationName: null
address: Campus Augustusplatz / S 017
link: 'https://www.facebook.com/events/2272942799430617/'
image: 56119741_907080906128776_2672013826173435904_o.jpg
teaser: 'Do, 16.05., 18 – 21 Uhr:  Kein ruhiges Hinterland  Sächsischer Flüchtlingsrat e.V. / Campus Augustusplatz / S 017  Eine Vielzahl sächsischer Kommunen'
recurring: null
isCrawled: true
---
Do, 16.05., 18 – 21 Uhr:

Kein ruhiges Hinterland

Sächsischer Flüchtlingsrat e.V. / Campus Augustusplatz / S 017

Eine Vielzahl sächsischer Kommunen steht für rassistische Ausschreitungen. Ein Blick hinter die Kulissen offenbart den Aktivismus vieler, die in diesen Kommunen gegen menschen-verachtende Ideologien einstehen, Aktionen organisieren, in der Geflüchtetenarbeit tätig sind. Diesen Blick wagt das Jahres-Magazin „Querfeld“ des Sächsischen Flüchtlingsrats mit seinem Fokus auf die Aktivitäten in Bautzen. Bei der Lesung wird es im Anschluss eine Diskussion geben.
