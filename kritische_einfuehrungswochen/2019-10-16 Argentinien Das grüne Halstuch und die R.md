---
id: "463837204231763"
title: "Argentinien: Das grüne Halstuch und die Revolution der Frauen"
start: 2019-10-16 19:00
end: 2019-10-16 21:00
address: HS8 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/463837204231763/
image: 71718693_1032196676950531_3197586332780068864_n.jpg
teaser: "Mi. 16.10.19 / 19.00  Argentinien: Das grüne Halstuch und die Revolution der
  Frauen  SDS Leipzig / fem*med / HS8   Als Reflexion der diesjährigen Erei"
isCrawled: true
---
Mi. 16.10.19 / 19.00

Argentinien: Das grüne Halstuch und die Revolution der Frauen

SDS Leipzig / fem*med / HS8 

Als Reflexion der diesjährigen Ereignisse im Rahmen des 8. März wollen wir in einem mit persönlichen Erfahrungen bereicherten Vortrag über aktuelle, historische und organisatorische Prozesse der Frauen*bewegung in Argentinien berichten. Am Ende soll Zeit bleiben für Fragen und Gespräch. Gerne möchten wir gemeinsam diskutieren, ob und inwiefern die Prozesse in Argentinien uns hier in unserem feministischen Engagement inspirieren können.