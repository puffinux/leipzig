---
id: '3067313969949021'
title: 'Vortrag: Karl Marx und die Demokratie'
start: '2019-04-25 18:00'
end: '2019-04-25 21:00'
locationName: null
address: 'HS 16, HSG'
link: 'https://www.facebook.com/events/3067313969949021/'
image: 53673344_911528722350661_1178996294010535936_o.jpg
teaser: 'Datum: 25.04, 18-21 Uhr Art der Veranstaltung: Vortrag Titel der Veranstaltung: Karl Marx und die Demokratie Ort/Raum: HS 16, HSG  Freie Wahlen werden'
recurring: null
isCrawled: true
---
Datum: 25.04, 18-21 Uhr
Art der Veranstaltung: Vortrag
Titel der Veranstaltung: Karl Marx und die Demokratie
Ort/Raum: HS 16, HSG

Freie Wahlen werden als Hauptbestandteil der Demokratie gewürdigt. Der Souverän wird nicht einfach regiert, er erteilt den Auftrag zur Wahrnehmung der Staatsgeschäfte. In diesem Vortrag soll es darum gehen herauszufinden, welche Aufgabe die bürgerliche Demokratie in unserem Wirtschaftssystem zuteil wird und welche Interessen hierbei zum Schaden welcher Menschen vertreten werden. Inhalte des Vortrages sind hierbei so manch aktuelle Analysen von Karl Marx.