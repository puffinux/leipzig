---
id: "634295357095424"
title: Hairpolitics and Braiding Class *Black/Afropeople only*
start: 2019-10-18 17:00
end: 2019-10-18 19:00
address: S122 / Campus Augustuspaltz / Uni Leipzig
link: https://www.facebook.com/events/634295357095424/
image: 72086574_1029202183916647_4921843207647526912_o.jpg
teaser: Fr. 18.10.19 / 17.00  Hairpolitics and Braiding Class  BPoC Hochschulgruppe /
  S122  *Black/Afropeople only* Bring along another head to braid on and t
isCrawled: true
---
Fr. 18.10.19 / 17.00

Hairpolitics and Braiding Class

BPoC Hochschulgruppe / S122

*Black/Afropeople only* Bring along another head to braid on and things you want to be braiding with. Deutsch: Wir reden über Haare und lernen, wie wir sie flechten können. Bring deine Idee/Bilder mit und wir finden gemeinsam heraus wie das geht. 