---
id: '417049615835637'
title: Einführung in den Feminismus - den es nicht gibt
start: '2019-10-08 11:00'
end: '2019-10-08 13:00'
locationName: null
address: S112 / Campus Augustusplatz / Uni Leipzig
link: 'https://www.facebook.com/events/417049615835637/'
image: 71348231_1021526594684206_2841662878291853312_o.jpg
teaser: 'Di. 08.10.19 / 11.00  Einführung in den Feminismus - den es nicht gibt  Referat für Gleichstellung & Lebensweisenpolitik / S112   Feminismus, Nebenwid'
recurring: null
isCrawled: true
---
Di. 08.10.19 / 11.00

Einführung in den Feminismus - den es nicht gibt

Referat für Gleichstellung & Lebensweisenpolitik / S112


Feminismus, Nebenwiderspruch oder Hauptdebatte? Es sollen Grundlagen geschaffen, ein historischer Überblick gegeben und die großen Streitpunkte zwischen TERF und Queerfront angerissen werden. Der Workshop richtet sich sowohl an Betroffene (und erklärt, wer eigentlich betroffen ist) als auch an jene, die das Thema "eigentlich nur ganz spannend" finden. Alle dazwischen sind auch willkommen. Wer noch nicht weiß, was TERF überhaupt meint und warum Personen bei dem Begriff "Queerfront" aggressiv werden, kann es hier erfahren.