---
name: Kritischen Einführungswochen
website: https://kew-leipzig.de/
email: kontakt@kew-leipzig.de
scrape:
  source: facebook
  options:
    page_id: 124650284371846
---
Alternativ zu den „normalen“ Einführungswochen stellen wir bereits seit einigen Jahren eine kritische Einführungswoche auf die Beine. Es soll ein Raum geschaffen werden, in dem sich Studis über politische und gesellschaftskritische Inhalte austauschen können. An linken Themen mangelt es in der Universität meist – deswegen versuchen wir durch ein progressives Alternativprogramm zu einer Politisierung des Campus beizutragen.