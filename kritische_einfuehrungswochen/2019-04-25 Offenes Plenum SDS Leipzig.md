---
id: '262693884616162'
title: Offenes Plenum SDS Leipzig
start: '2019-04-25 19:15'
end: '2019-04-25 22:15'
locationName: null
address: Institut für Psychologie / Z007
link: 'https://www.facebook.com/events/262693884616162/'
image: 55516236_908639459306254_3148837753890799616_o.jpg
teaser: 'Der Kapitalismus ist nicht das Ende der Geschichte. Es gibt Alternativen zu Sozialabbau, Krieg, Sexismus, Rassismus und Umweltzerstörung. Komm deswege'
recurring: null
isCrawled: true
---
Der Kapitalismus ist nicht das Ende der Geschichte. Es gibt Alternativen zu Sozialabbau, Krieg, Sexismus, Rassismus und Umweltzerstörung. Komm deswegen zu unseren offenen Plena und werde im SDS aktiv! Die offenen Plena sind als guter Einstiegspunkt gedacht, Du kannst aber auch an jedem anderen Donnerstag um 19:15 bei uns vorbeikommen. Unsere Treffen sind offen für alle, die mit uns die Welt verändern wollen.