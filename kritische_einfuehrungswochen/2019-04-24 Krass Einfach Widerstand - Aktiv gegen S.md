---
id: '333833744002917'
title: Krass Einfach Widerstand - Aktiv gegen Schwarz-Blau. 2. Treffen
start: '2019-04-24 17:00'
end: '2019-04-24 19:00'
locationName: null
address: 'SR 4, Wirtschaftswissenschaftliche Fakultät (Campus Augustusplatz)'
link: 'https://www.facebook.com/events/333833744002917/'
image: 56870051_918411228329077_1093449826898542592_n.jpg
teaser: In drei ostdeutschen Bundesländern stehen 2019 Landtagswahlen an. Die vorhersehbaren Erfolge für die AFD und zunehmende rassistische Straßenmobilisier
recurring: null
isCrawled: true
---
In drei ostdeutschen Bundesländern stehen 2019 Landtagswahlen an. Die vorhersehbaren Erfolge für die AFD und zunehmende rassistische Straßenmobilisierung sind beides Anzeichen zunehmender Akzeptanz für rechte Ideen in der Breite der Bevölkerung. Wir wollen ausgehend von der Uni gemeinsam mit euch dagegen aktiv werden!