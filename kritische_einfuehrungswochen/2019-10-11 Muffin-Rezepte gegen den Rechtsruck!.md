---
id: '2439171123017841'
title: Muffin-Rezepte gegen den Rechtsruck?!
start: '2019-10-11 11:00'
end: '2019-10-11 13:00'
locationName: null
address: S103 / Campus Augustusplatz / Uni Leipzig
link: 'https://www.facebook.com/events/2439171123017841/'
image: 71181582_1025007874336078_8991017986928148480_o.jpg
teaser: Fr. 11.10.19 / 11.00  Muffin-Rezepte gegen den Rechtsruck?!  Tschop!Tschop!-Redaktion / S103  Mit Gartenreportagen gegen rechte Hetze immunisieren? Be
recurring: null
isCrawled: true
---
Fr. 11.10.19 / 11.00

Muffin-Rezepte gegen den Rechtsruck?!

Tschop!Tschop!-Redaktion / S103

Mit Gartenreportagen gegen rechte Hetze immunisieren? Bei Tschop! Tschop! hat sich eine Handvoll Menschen, die mehr Motivation als Erfahrung mitbringen, genau das zum Ziel gemacht. Ausgerechnet in Sachsen. Statt Pamphleten und Institutionalisierung entsteht bei Tschop! Tschop! inklusiver Journalismus, der das Politische im Alltag unterhaltsam darstellt. Und auch ihr könnt mitmachen...