---
id: '1142487152598707'
title: Solidarität oder Rechtspopulismus in der Platte?
start: '2019-05-16 17:00'
end: '2019-05-16 20:00'
locationName: null
address: Campus Augustusplatz / HS 11
link: 'https://www.facebook.com/events/1142487152598707/'
image: 55470365_907080532795480_6879196534718595072_o.jpg
teaser: 'Do, 16.05., 17 – 20 Uhr:  Solidarität oder Rechtspopulismus in der Platte?  be:gründet // Kritische Psychologie Leipzig / Campus Augustusplatz / HS 11'
recurring: null
isCrawled: true
---
Do, 16.05., 17 – 20 Uhr:

Solidarität oder Rechtspopulismus in der Platte?

be:gründet // Kritische Psychologie Leipzig / Campus Augustusplatz / HS 11

Die ehemals gefragten Stadtteile ostdeutscher Städte gelten längst als ,Platte' und haben meist ein negatives Image. Die unterschiedlichen Lebenswirklichkeiten und Perspektiven der heterogenen Bevölkerung kommen in der öffentlichen Debatte dagegen kaum zur Geltung. Und weil viele nicht wählen oder kein Wahlrecht haben, sind ihre Interessen im politischen Raum lange nicht  präsent gewesen. kaum vertreten. Letzteres scheint sich mit den Wahlerfolgen der AfD auch in diesen Stadtteilen teils geändert zu haben, wenngleich gerade nicht unter progressiven Vorzeichen.
Ein an der Hochschule Magdeburg-Stendal angesiedeltes Handlungsforschungsprojekt untersucht am Beispiel eines solchen Stadtteils, wie Prekarisierung, (mangelnde) politische Teilhabe und Rechtspopulismus sich sozialräumlich verdichten. Neben dem offenkundigen Trend einer rechten Formierung interessiert aus subjektwissenschaftlicher und hegemonietheoretischer Sicht besonders, welche individuellen und kollektiven Ansätze eines solidarischenh Umgangs vor Ort existieren und wie sie gestärkt werden können.

Katrin Reimer-Gordinskaya ist Professorin für Kindliche Entwicklung, Bildung und Sozialisation in den Angewandten Kindheistwissenschaften und Redaktionsmitglied des Forum Kritische Psychologie - Neue Folge.

Veranstaltet von be:gründet // Kritische Psychologie.