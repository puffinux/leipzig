---
id: '1544029745730261'
title: Eine Wende um 180° - Wie die AfD versucht die Erinnerungspolitik
start: '2019-06-18 17:00'
end: '2019-06-18 19:00'
locationName: null
address: Campus Augustusplatz / HS 17
link: 'https://www.facebook.com/events/1544029745730261/'
image: 55521090_907508656086001_8346871947358896128_o.jpg
teaser: 'Di, 18.06., 17 Uhr:  Eine Wende um 180° - Wie die AfD versucht die Erinnerungspolitik umzuwälzen  Erich Zeigner Haus e.V. / Campus Augustusplatz / HS'
recurring: null
isCrawled: true
---
Di, 18.06., 17 Uhr:

Eine Wende um 180° - Wie die AfD versucht die Erinnerungspolitik umzuwälzen

Erich Zeigner Haus e.V. / Campus Augustusplatz / HS 17

Seitdem die AfD im Bundes- und im sächsischen Landtag sitzt, sind erinnerungspolitische Skandale fast an der Tagesordnung. Relativierung und Leugnung von NS-Unrecht ist allerdings nicht nur plumpe Provokation, sondern gezielte Politik der AfD. Diese soll an Hand von Beispielen in Sachsen und Anderswo erörtert werden.
