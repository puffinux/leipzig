---
id: '3203107329861080'
title: 1848 und Marxismus
start: '2019-04-26 15:00'
end: '2019-04-26 17:00'
locationName: null
address: Campus Augustusplatz / S 126
link: 'https://www.facebook.com/events/3203107329861080/'
image: 55575752_908639572639576_966972952815861760_o.jpg
teaser: 'In der gescheiterten Revolution von 1848 erkannte Marx die Grundzüge eines neuen, historisch beispiellosen Zeitalters: die Krise des Industriekapitals'
recurring: null
isCrawled: true
---
In der gescheiterten Revolution von 1848 erkannte Marx die Grundzüge eines neuen, historisch beispiellosen Zeitalters: die Krise des Industriekapitals, die bürgerliche Demokratie, den autoritären Staat und die Notwendigkeit eines proletarischen Sozialismus. Was ist Marxismus?