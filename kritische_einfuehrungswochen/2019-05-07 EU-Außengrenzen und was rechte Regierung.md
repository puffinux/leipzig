---
id: '384041735512478'
title: EU-Außengrenzen und was rechte Regierungen damit zu tun haben
start: '2019-05-07 19:00'
end: '2019-05-07 22:00'
locationName: Helmut
address: 'Kohlgartenstraße 51, 04315 Leipzig'
link: 'https://www.facebook.com/events/384041735512478/'
image: 55470320_907068039463396_4608692752639590400_o.jpg
teaser: 'Di, 07.05., 19 Uhr:      EU-Außengrenzen und was rechte Regierungen damit zu tun haben      Border Violence Monitoring (Rigardu e.V.) / Helmut, Kohlga'
recurring: null
isCrawled: true
---
Di, 07.05., 19 Uhr:
    
EU-Außengrenzen und was rechte Regierungen damit zu tun haben
    
Border Violence Monitoring (Rigardu e.V.) / Helmut, Kohlgartenstraße 51

Das Watchdog-Netzwerk Border Violence Monitoring dokumentiert seit 2017 Menschenrechtsverletzungen an den EU-Außengrenzen. Der Vortrag widmet sich der Sicherung dieser und der offensichtlichen Systematik der Menschenrechtsverletzungen. Dabei betrachten wir die Aufrüstung der serbisch-ungarischen Grenze unter Orbán und andere Beispiele. Außerdem: Was hat Grenzsicherung mit rechtsnationalen Tendenzen zu tun? Und was bedeutet das für die kommenden Landtagswahlen?