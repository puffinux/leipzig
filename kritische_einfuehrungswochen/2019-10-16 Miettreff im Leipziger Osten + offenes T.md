---
id: "657341764674732"
title: Miettreff im Leipziger Osten + offenes Treffen
start: 2019-10-16 18:00
end: 2019-10-16 21:00
address: Eisenbahnstraße 125, 04315 Leipzig
link: https://www.facebook.com/events/657341764674732/
image: 71472453_1028111710692361_3696333689131630592_o.jpg
teaser: Mi.16.10.19 / 18.00  Miettreff im Leipziger Osten + offenes Treffen  Miettreff
  Leipziger Osten / Eisenbahnstraße 125  Probleme mit Vermieter*innen, Ha
isCrawled: true
---
Mi.16.10.19 / 18.00

Miettreff im Leipziger Osten + offenes Treffen

Miettreff Leipziger Osten / Eisenbahnstraße 125

Probleme mit Vermieter*innen, Hausverwaltung oder Miete? Der Miettreff ist ein Ort zum Austausch und zur gegenseitigen Unterstützung und - wenn nötig - der Organisation von Protest. Bitte bringt, wenn ihr ein Anliegen habt, all eure Unterlagen mit (Mietvertrag, Nebenkostenabrechnung, Briefwechsel mit der Hausverwaltung etc.). Im Anschluss findet für Interessierte ein offenes Kennlern-Treffen statt.
