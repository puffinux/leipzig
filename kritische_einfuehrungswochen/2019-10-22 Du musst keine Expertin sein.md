---
id: "386916242210043"
title: Du musst kein*e Expert*in sein
start: 2019-10-22 11:00
end: 2019-10-22 15:00
address: HS16 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/386916242210043/
image: 71765757_1030514293785436_2869391204334698496_o.jpg
teaser: Di. 22.10.19 / 11.00-15.00  Du musst kein*e Expert*in sein - Wie man die
  Argumente der Klimawandelleugner*innen unschäd-lich macht  Scientists for Fut
isCrawled: true
---
Di. 22.10.19 / 11.00-15.00

Du musst kein*e Expert*in sein - Wie man die Argumente der Klimawandelleugner*innen unschäd-lich macht

Scientists for Future / HS16

Leute, die den menschengemachten Klimawandel bestreiten, trifft man überall. Wo zwischen Wissenschaftler*innen Konsens besteht, wissen viele Menschen zu wenig über den Klimawandel, leugnen oder ignorieren die Fakten. Wie erreicht man diese Menschen? Wir als Scientists for Future wollen das “Handwerkszeug” vermitteln, um sich in solchen Diskussionen zu behaupten und zeigen, dass man eben kein*e Expert*in sein muss, um sachlich richtig zu argumentieren.