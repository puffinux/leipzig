---
id: "765666950528738"
title: Offenes Plenum des Kritischen Lehramts X Kopfsalat
start: 2019-10-23 19:00
end: 2019-10-23 22:00
address: Kopfsalat Gästeservice
link: https://www.facebook.com/events/765666950528738/
image: 71146584_1032194756950723_5821769925676498944_o.jpg
teaser: Mi. 23.10.19 / Kopfsalat (Stannebeinplatz 13)  Offenes Plenum der Kritischen
  Lehrer*innen  Kritische Lehrer*innen / 19.00 (jeden Mittwoch)  Die Kritis
isCrawled: true
---
Mi. 23.10.19 / Kopfsalat (Stannebeinplatz 13)

Offenes Plenum der Kritischen Lehrer*innen

Kritische Lehrer*innen / 19.00 (jeden Mittwoch)

Die Kritischen Lehrer*innen Leipzig existieren nun seit bereits mehr als vier Jahren als Initiative, bei der sich Studierende des Lehramts aller Fächer und Richtungen zusammensetzen und alles, was im Spannungsfeld Schule – Lernen – Lehramtsstudium liegt, kritisch diskutieren, reflektieren und konstruktiv angehen. 



Hinweis: Es handelt sich um eine Vereinsveranstaltung. 