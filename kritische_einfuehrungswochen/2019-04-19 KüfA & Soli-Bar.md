---
id: '412341786222788'
title: KüfA & Soli-Bar
start: '2019-04-19 19:00'
end: '2019-04-19 23:00'
locationName: Das Molekühl
address: 'Hedwigstraße 6, 04315 Leipzig'
link: 'https://www.facebook.com/events/412341786222788/'
image: 55509711_907075779462622_397122292619935744_o.jpg
teaser: Es gibt leckeres Essen & Drinks gegen Spende bzw. günstige Preise. Ab und an finden verschiedene politische Events statt - organisiert von linken Grup
recurring: null
isCrawled: true
---
Es gibt leckeres Essen & Drinks gegen Spende bzw. günstige Preise.
Ab und an finden verschiedene politische Events statt - organisiert von linken Gruppen aus Leipzig. Für aktuelle Infos schaut bei Facebook vorbei: facebook.com/Das-Molekühl-357583868041792 Kommt rum zum schmausen, diskutieren oder abhängen.
