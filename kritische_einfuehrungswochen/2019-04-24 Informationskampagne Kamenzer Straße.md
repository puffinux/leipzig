---
id: '2215254945470100'
title: Informationskampagne Kamenzer Straße
start: '2019-04-24 18:00'
end: '2019-04-24 20:00'
locationName: null
address: Campus Augustusplatz / S 420
link: 'https://www.facebook.com/events/2215254945470100/'
image: 56286707_908639242639609_5227721355310399488_o.jpg
teaser: Seit mehr als 10 Jahren befindet sich ein Gebäude des ehemaligen Frauen-Außenlagers des KZ Buchenwald im Besitz eines Neonazis. Seither fanden dort di
recurring: null
isCrawled: true
---
Seit mehr als 10 Jahren befindet sich ein Gebäude des ehemaligen Frauen-Außenlagers des KZ Buchenwald im Besitz eines Neonazis. Seither fanden dort diverse Rechtsrockkonzerte und Elektroparties statt. Organisierte Neonazistrukturen haben dort ihre Räume.  Ein würdiges Erinnern ist unmöglich. Über die Geschichte des Außenlagers in der Kamenzer Str. 12, deren aktuelle Entwürdigung und Handlungsperspektiven wollen wir Euch informieren und in Austausch kommen.