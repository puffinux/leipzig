---
id: "1660911510705403"
title: Kundgebung - In Gedenken an Kamal K. - Rassismus tötet!
start: 2019-10-24 18:00
end: 2019-10-24 21:00
address: "Treffpunkt: Park vor dem Leipziger Hauptbahnhof"
link: https://www.facebook.com/events/1660911510705403/
image: 71313654_1032205946949604_5369164008576253952_o.jpg
teaser: 'Do. 24.10.19 / 18.00  Kundgebung - In Gedenken an Kamal K.  "Rassismus
  tötet!" Leipzig / Treffpunkt: Park vor dem Leipziger Hauptbahnhof  Am 24.
  Oktob'
isCrawled: true
---
Do. 24.10.19 / 18.00

Kundgebung - In Gedenken an Kamal K.

"Rassismus tötet!" Leipzig / Treffpunkt: Park vor dem Leipziger Hauptbahnhof

Am 24. Oktober 2019 jährt sich der rassistische Mord an Kamal K. zum neunten Mal. Er wurde von den verurteilen Neonazis Daniel K. und Marcus E. im C.-W.-Müller-Park, gegenüber des Hauptbahnhofes angegriffen und verstarb kurz darauf im Krankenhaus an seinen Verletzungen. Wir wollen, dass Menschen wie Kamal K. nicht vergessen werden. Menschen, die nicht ins Weltbild von deutschen TäterInnen passten und deshalb ihr Leben lassen mussten. Niemand wird vergessen, nichts ist vergeben.