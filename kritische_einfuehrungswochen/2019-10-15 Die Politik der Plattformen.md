---
id: "428597801388103"
title: Die Politik der Plattformen
start: 2019-10-15 11:00
end: 2019-10-15 13:00
address: S017 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/428597801388103/
image: 70998283_1027070977463101_2050844598917398528_o.jpg
teaser: "Di. 15.10.19 / 11.00  Die Politik der Plattformen: digitale
  Möglichkeits-räume und Herrschaftskritik im Cyberspace  Link - AG für
  kritische Informatik"
isCrawled: true
---
Di. 15.10.19 / 11.00

Die Politik der Plattformen: digitale Möglichkeits-räume und Herrschaftskritik im Cyberspace

Link - AG für kritische Informatik an der Uni Leipzig  / S017

Handelt es sich beim Internet noch um einen freiheitlichen und demokratischen Raum, in dem ein*e jede*r nach eigenem Belieben handeln kann? Wir geben einen Einblick in die Politik der Plattformen: Ausgehend von der Darstellung zentraler Akteure des kommerzialisierten Internets betrachten wir die Konstituierung von (Möglichkeits-)Räumen und Dezentralisierungsansätzen im Cyberspace. Diese wollen wir herrschaftskritisch hinterfragen und ihre Alltagstauglichkeit mit euch diskutieren. Keine Vorkenntnisse notwendig!