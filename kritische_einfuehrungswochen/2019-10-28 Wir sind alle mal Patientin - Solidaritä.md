---
id: "414124125900857"
title: Wir sind alle mal Patient*in - Solidarität im Arbeitskampf
start: 2019-10-28 19:00
end: 2019-10-28 21:00
address: HS 5 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/414124125900857/
image: 71857673_2852118284840267_7185457372557475840_n.jpg
teaser: Mo. 28.10.19 / 19.00  Wir sind alle mal Patient*in  - Solidarität im
  Arbeitskampf   DGB Hochschulgruppe / ver.di Jugend Leipzig-Nordsachsen / HS
  5  St
isCrawled: true
---
Mo. 28.10.19 / 19.00

Wir sind alle mal Patient*in  - Solidarität im Arbeitskampf 

DGB Hochschulgruppe / ver.di Jugend Leipzig-Nordsachsen / HS 5

Streiks in Care-Berufen, besonders in Krankenhäusern und Pflege-einrichtungen, haben im letzten Jahr zunehmend an Bedeutung gewonnen. Gründe dafür liegen in der massiven Privatisierung, den miserablen Arbeitsbedingungen und fehlender Wertschätzung. Was machen diese Streiks so besonders? Warum bedarf es hier einer breiten, öffentlichen Solidarisierung? Wo verortest Du Dich im Spannungsfeld zwischen Patient*in sein und Solidarität? Welche Formen der Unterstützung gibt es?