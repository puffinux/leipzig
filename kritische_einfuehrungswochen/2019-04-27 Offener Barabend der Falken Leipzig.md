---
id: '1062816360595286'
title: Offener Barabend der Falken Leipzig
start: '2019-04-27 19:30'
end: '2019-04-27 22:00'
locationName: null
address: 'Ladenlokal der Falken, Dimpfelstraße 33'
link: 'https://www.facebook.com/events/1062816360595286/'
image: 55624176_906559922847541_8734704758293528576_o.jpg
teaser: 'Wir Falken sind ein sozialistischer Kinder- und Jugendverband. Wir sind parteiunabhängig, aber parteiisch gegen Herrschaft und Ausbeutung. Wir bilden'
recurring: null
isCrawled: true
---
Wir Falken sind ein sozialistischer Kinder- und Jugendverband. Wir sind parteiunabhängig, aber parteiisch gegen Herrschaft und Ausbeutung. Wir bilden uns selbst und andere und arbeiten pädagogisch mit Kindern und Jugendlichen. Wenn Ihr Lust habt uns und unser Ladenlokal in Leipzig kennen zu lernen, kommt doch zu unserem offen Barabend. Wir freuen uns auf Euch!