---
id: '313030919414378'
title: Wohlfahrtsstaat und die Krise des Neoliberalismus
start: '2019-05-22 15:00'
end: '2019-05-22 17:00'
locationName: null
address: Campus Augustusplatz / HS 17
link: 'https://www.facebook.com/events/313030919414378/'
image: 55458184_907085226128344_4035472428606947328_o.jpg
teaser: 'Mi, 22.05., 15 – 17 Uhr:  Wohlfahrtsstaat und die Krise des Neoliberalismus  Platypus / Campus Augustusplatz / HS 17  In der gegenwärtigen Krise des N'
recurring: null
isCrawled: true
---
Mi, 22.05., 15 – 17 Uhr:

Wohlfahrtsstaat und die Krise des Neoliberalismus

Platypus / Campus Augustusplatz / HS 17

In der gegenwärtigen Krise des Neoliberalismus, fordern viele Linke (und Rechte) die Rückkehr in den Sozialstaat der Nachkriegszeit. Was sind seine Ursprünge? Warum kehrt sein Gespenst jetzt zurück?
