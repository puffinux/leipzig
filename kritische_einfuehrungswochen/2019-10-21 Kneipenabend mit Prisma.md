---
id: "540621349813665"
title: Kneipenabend mit Prisma
start: 2019-10-21 19:00
end: 2019-10-21 23:59
locationName: Helmut
address: Kohlgartenstraße 51, 04315 Leipzig
link: https://www.facebook.com/events/540621349813665/
image: 70994004_1030515487118650_8028921269397225472_o.jpg
teaser: Mo. 21.10.19 / 19.00  Kneipenabend mit Prisma    Prisma - Interventionistische
  Linke Leipzig / / Helmut (Kohlgartenstraße 51)  Kneipenabend mit Prisma
isCrawled: true
---
Mo. 21.10.19 / 19.00

Kneipenabend mit Prisma  

Prisma - Interventionistische Linke Leipzig / / Helmut (Kohlgartenstraße 51)

Kneipenabend mit Prisma - interventionistische Linke Leipzig. Wir stellen uns und unseren Politikansatz vor und wollen mit euch über unsere Politik, Aktionen und Beteiligungsmöglichkeiten in entspannter  Runde  ins Gespräch kommen. Ihr könnt aber auch einfach nur so zu unserem Solitresen kommen und Getränken mit alten Freund*innen und neuen Bekannten trinken (Geld geht an antirassistische Pojekte).
