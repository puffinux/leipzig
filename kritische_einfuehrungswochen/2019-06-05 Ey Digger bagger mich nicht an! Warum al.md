---
id: '2234600663242667'
title: Ey Digger bagger mich nicht an! Warum alle Dörfer bleiben müssen
start: '2019-06-05 15:00'
end: '2019-06-05 17:00'
locationName: null
address: Campus Augustusplatz / HS 8
link: 'https://www.facebook.com/events/2234600663242667/'
image: 56158278_907504802753053_2204764033393885184_o.jpg
teaser: 'Mi, 05.06., 15 – 17 Uhr:  Ey Digger, bagger mich nicht an! Warum alle Dörfer bleiben müssen  Ende Gelände Leipzig / Campus Augustusplatz / HS 8  Nach'
recurring: null
isCrawled: true
---
Mi, 05.06., 15 – 17 Uhr:

Ey Digger, bagger mich nicht an! Warum alle Dörfer bleiben müssen

Ende Gelände Leipzig / Campus Augustusplatz / HS 8

Nach der Verkündung des Berichts der Kohlekommission begann RWE im Rheinland die Vorbereitung der Zerstörung der Dörfer. Obwohl der Klimawandel immer drastischer wird, will RWE weiter Familien vertreiben. Wir wollen mit euch über die Perspektiven im rheinischen Revier diskutieren und fragen: Warum soll die Abbaggerung der Dörfer verhindert werden und was können wir tun? Welche Aktionen sind geplant? Am Ende wird es Raum für Austausch und Fragen geben.
