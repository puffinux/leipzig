---
id: "380290816228187"
title: Durch deine, meine, unsere Augen
start: 2019-10-18 15:00
end: 2019-10-18 17:00
address: S202 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/380290816228187/
image: 71645685_1030209740482558_2127440762206420992_o.jpg
teaser: Fr. 18.10.19 / 15.00  „Durch deine, meine, unsere Augen." Perspektiven von
  Dozierenden und Studierenden auf das Studium am Beispiel Lehramt  Referat f
isCrawled: true
---
Fr. 18.10.19 / 15.00

„Durch deine, meine, unsere Augen." Perspektiven von Dozierenden und Studierenden auf das Studium am Beispiel Lehramt

Referat für Lehramt des StuRa / S202

Wir wollen uns mit den unterschiedlichen Perspektiven von Lehrenden und Lernenden auf Studieninhalte, Prüfungen etc. auseinandersetzen. Dabei soll sowohl ein Einblick in Arbeitsbedingungen unserer Dozent*innen als auch der Situation von Studis gewonnen und diskutiert werden. Ziel ist eine Perspektivübernahme zwischen den Gruppen, welche in (gemeinsames) Engagement münden kann. Nur zusammen können wir Manches verbessern. 