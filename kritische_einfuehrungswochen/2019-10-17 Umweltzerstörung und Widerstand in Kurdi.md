---
id: "851688021899127"
title: Umweltzerstörung und Widerstand in Kurdistan
start: 2019-10-17 19:00
end: 2019-10-17 21:00
address: HS12 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/851688021899127/
image: 71781015_517513249068141_2830064228914692096_n.jpg
teaser: Do. 17.10.19 / 19.00  Mesopotamia Water Forum - Umweltzerstörung und
  Widerstand gegen einen türkischen Staudamm-Bau in Kurdistan  AG Kurdistan /
  HS12
isCrawled: true
---
Do. 17.10.19 / 19.00

Mesopotamia Water Forum - Umweltzerstörung und Widerstand gegen einen türkischen Staudamm-Bau in Kurdistan

AG Kurdistan / HS12

Der Bauingenieur Ercan Ayboga berichtet von der Initiative des "Mesopotamia Water Forums", von der ein Hauptaugenmerk der Staudammbau der türkischen Regierung am Tigris ist. Nach den Staudamm-Projekten am größten Strom Vorderasiens, dem Euphrat ab 1990 wird nun seit 2019 versucht den zweiten wichtigen Fluss Vorderasiens, den Tigris durch die Türkei zu nutzen.
Hierzu werden die seit mind. 8.000 Jahren dauerhaft besiedelte Stadt Hasankeyf überflutet und rund 70.000 Menschen zwangsweise evakuiert.
Während die türkische Regierung sich Investitionen aus dem Ausland verspricht hat das Projekt immense Folgen für die Ökologie und die Wirtschaft der Menschen nicht nur in der Türkei, sondern auch in Syrien und dem Irak.