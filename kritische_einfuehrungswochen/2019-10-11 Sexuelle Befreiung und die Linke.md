---
id: '370350667237049'
title: Sexuelle Befreiung und die Linke
start: '2019-10-11 17:00'
end: '2019-10-11 19:00'
locationName: null
address: S101 / Campus Augustusplatz / Uni Leipzig
link: 'https://www.facebook.com/events/370350667237049/'
image: 71317424_1025458630957669_5362598124567134208_o.jpg
teaser: 'Fr. 11.10.19 / 17.00  Sexuelle Befreiung und die Linke  Platypus Leipzig / S101  Die politische & kulturelle Linke, deren Ziel die individuelle Freihe'
recurring: null
isCrawled: true
---
Fr. 11.10.19 / 17.00

Sexuelle Befreiung und die Linke

Platypus Leipzig / S101

Die politische & kulturelle Linke, deren Ziel die individuelle Freiheit ist, hat historisch ihre Positionen zur Sexualität verändert. So stellte sie beispielsweise einmal die Ehetradition der Gesellschaft in Frage, während sie sich später mit der Ehe für alle für sie einsetzt. Inwiefern betreffen Fragen der sexuellen Freiheit gesamtgesellschaftliche Probleme und nicht nur die sexueller Minderheiten und Subkulturen? Was kann die Geschichte der Linken darüber lehren?