---
id: "2691310654221441"
title: Fahrradtour zu historischen Orten der Arbeiter*innenbewegung
start: 2019-10-19 14:00
end: 2019-10-19 16:00
address: "Start: Augustusplatz am Paulinum unter dem großen Fenster"
link: https://www.facebook.com/events/2691310654221441/
image: 70990199_1032190310284501_1339662074137018368_n.jpg
teaser: "Sa. 19.10.19 / 14.00  Fahrradtour zu historischen Orten der
  ArbeiterInnen-Bewegung  SDS Leipzig / Start: Augustusplatz am Paulinum unter
  dem großen Fe"
isCrawled: true
---
Sa. 19.10.19 / 14.00

Fahrradtour zu historischen Orten der ArbeiterInnen-Bewegung

SDS Leipzig / Start: Augustusplatz am Paulinum unter dem großen Fenster

Du hattest ja keine Ahnung – was in Leipzig nicht alles für couragierte Menschen gelebt haben, was hier schon alles passiert ist! Gut dass du mitgekommen bist und jetzt spannende Ausschnitte von Leipzigs kämpferischer Vergangenheit kennst. Dass du weißt, wo Rosa Luxemburg gearbeitet hat, wie die Menschen hier solide Barrikaden gebaut haben und dass die Leipziger Volkszeitung mal richtig revolutionär war.