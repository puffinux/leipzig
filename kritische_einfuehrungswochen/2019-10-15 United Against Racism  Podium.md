---
id: "997705623914076"
title: United Against Racism // Podium
start: 2019-10-15 19:00
end: 2019-10-15 21:00
locationName: Ost-Passage Theater
address: Konradstr. 27, 04315 Leipzig
link: https://www.facebook.com/events/997705623914076/
image: 71047509_1029227600580772_8805217980876062720_n.jpg
teaser: "Di.15.10.19 / 19.00  United Against Racism: Antirassistische Kämpfe stärken
  und verbinden  SDS Leipzig / Ost-Passage Theater (Konradstraße 27)  Rassis"
isCrawled: true
---
Di.15.10.19 / 19.00

United Against Racism: Antirassistische Kämpfe stärken und verbinden

SDS Leipzig / Ost-Passage Theater (Konradstraße 27)

Rassismus bedeutet auf unterschiedlichen Ebenen Diskriminierung und Gewalt: Im Alltag, vor den Behörden oder auf der Suche nach Job und Wohnung. Aus diesen Verhältnissen ergeben sich unterschiedliche Formen und Strategien von antirassistischer Selbstorganisierung und Protest. Gemeinsam wollen wir mit Vertreter*innen von antirassistischen Initiativen die Frage diskutieren:

Was muss eine gesellschaftliche Linke leisten, um BIPOCs (Black/Indigenous People of Color) und ihre Kämpfe zu stärken?

Auf dem Podium diskutieren:

• Anne-Christin Tannhäuser 
(ISD – Initiative Schwarze Menschen in Deutschland)

• Delia Youssef 
(DaMigra - Dachverband der Migrantinnenorganisationen)

• Hami Hadad Shakeri 
(Kontaktstelle Wohnen)

• Hussein Morad 
(SDS, Referat Ausländische Studierende)
