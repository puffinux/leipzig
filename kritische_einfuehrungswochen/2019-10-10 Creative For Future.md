---
id: '430389924263681'
title: Creative For Future
start: '2019-10-10 15:00'
end: '2019-10-10 17:00'
locationName: null
address: Couchcafé / Campus Augustusplatz / Uni Leipzig
link: 'https://www.facebook.com/events/430389924263681/'
image: 70778013_1025006421002890_8002320599902846976_o.jpg
teaser: Do. 10.10.19 / 15.00  Creative For Future  Students For Future Leipzig / Couchcafé   Jeden Freitag gehen wir für Klimagerechtigkeit auf die Straße. In
recurring: null
isCrawled: true
---
Do. 10.10.19 / 15.00

Creative For Future

Students For Future Leipzig / Couchcafé 

Jeden Freitag gehen wir für Klimagerechtigkeit auf die Straße. In diesem Workshop zeigen wir euch, wie man große Transparente und Plakate für die nächste Demo baut und werden direkt mit euch loslegen - denkt euch also schonmal coole Sprüche aus! Außerdem erfahrt ihr, wie man ein eigenes Bienenwachstuch herstellt und damit Alufolie überflüssig macht. Zum Ausklang gibt's Getränke und Verpflegung gegen Spende.