---
id: '2208409922808472'
title: '„Ich bin ja kein Nazi, aber"'
start: '2019-05-24 10:00'
end: '2019-05-24 18:00'
locationName: null
address: 'Dietrichring 5-7, SR 405'
link: 'https://www.facebook.com/events/2208409922808472/'
image: 54730858_907087919461408_931820359190052864_o.jpg
teaser: 'Fr, 24.05., 10 – 18 Uhr:  „Ich bin ja kein Nazi, aber..." – Basiswissen zu Alltagsdiskriminierung, reaktionär-autoritären Strömungen und Neonazismus'
recurring: null
isCrawled: true
---
Fr, 24.05., 10 – 18 Uhr:

„Ich bin ja kein Nazi, aber..." – Basiswissen zu Alltagsdiskriminierung, reaktionär-autoritären Strömungen und Neonazismus

Kritische Lehrer*innen / Dietrichring 5-7, SR 405

Diese Fortbildung vom Netzwerk für Demokratie und Courage ist ausgelegt für Lehramtsstudierende und andere Multiplikator*innen im Bildungsbereich. Sie vermittelt inhaltliche Grundlagen zu Vorurteilen und Diskriminierung, betrachtet Wirkungsweisen von reaktionär-autoritären und neonazistischer Ideologien und zeigt Möglichkeiten und Grenzen des eigenen Handelns gegen Diskriminierung und Neonazismus auf.

Da die Plätze für den Workshop leider begrenzt sind, schick bei Interesse bitte eine informelle Anmeldung an krile-leipzig@inventati.org
