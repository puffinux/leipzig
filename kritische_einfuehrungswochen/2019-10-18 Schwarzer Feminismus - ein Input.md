---
id: "905615083171228"
title: Schwarzer Feminismus - ein Input
start: 2019-10-18 11:00
end: 2019-10-18 13:00
address: HS16 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/905615083171228/
image: 70977111_1029196823917183_4243165424860004352_o.jpg
teaser: Fr. 18.10.19 / 11.00  Schwarzer Feminismus - ein Input  BPoC Hochschulgruppe /
  HS16  Wie leisten wir Widerstand gegen vorherrschende menschenfeindlich
isCrawled: true
---
Fr. 18.10.19 / 11.00

Schwarzer Feminismus - ein Input

BPoC Hochschulgruppe / HS16

Wie leisten wir Widerstand gegen vorherrschende menschenfeindliche Systeme ohne Unterdrückungsmechanismen zu reproduzieren und zu manifestieren? Rassismus, Sexismus, Klassismus und Heteronormativität greifen als Unterdrückungsmechanismen auch in heutigen feministischen Strukturen. Die Theorien des black feminism stellen uns Wissensschätze, die schon vor mehr als 150 Jahren, formuliert und geteilt wurden. Eine Einführung.