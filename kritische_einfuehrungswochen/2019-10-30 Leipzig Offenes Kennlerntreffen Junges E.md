---
id: "2469575839955892"
title: "Leipzig: Offenes Kennlerntreffen Junges Engagement"
start: 2019-10-30 18:00
end: 2019-10-30 19:00
address: Universität Leipzig, Campus Augustusplatz, S015
link: https://www.facebook.com/events/2469575839955892/
image: 69633208_2405666142978298_768077192046313472_n.jpg
teaser: "Einladung zum Treffen vom Jungen Engagement:  Vielleicht ... ...treibt Dich
  schon seit Langem eine Idee oder ein Thema um, worauf Du die Öffentlichkei"
isCrawled: true
---
Einladung zum Treffen vom Jungen Engagement:

Vielleicht ...
...treibt Dich schon seit Langem eine Idee oder ein Thema um, worauf Du die Öffentlichkeit aufmerksam machen möchtest?
...suchst Du noch andere junge Menschen in deiner Stadt, die motiviert sind, sich politisch zu engagieren?
...suchst Du nach Inspiration um in dem Dschungel von Möglichkeiten für Engagement das zu finden, was Du suchst?
...hast du schon eine ausgefeilten Projektidee, weißt aber nicht, wie Du sie umsetzen oder finanzieren sollst?
...hast du eine politische Idee oder Überzeugung die du in die Gesellschaft tragen willst? 

Beim Jungen Engagement Mitteldeutschland hast du die Gelegenheit all das umzusetzen. 

Das Junge Engagement unterstützt das Aktiv-Werden junger Menschen zu entwicklungspolitischen Themen z.B. durch...

...Aktionen um Menschen für Klimagerechtigkeit zu gewinnen. 
...Bildungsveranstaltungen zum Globalen Lernen und globalen Ungerechtigkeiten. 
... Veranstaltungen die schon jetzt solidarische Lebensweisen und nachhaltige Alternativen aufzeigen.
...das Fördern von kritischem Denken. 
...das Sensibilisieren zum Thema Diskriminierung und kolonialen Strukturen. 

Und vieles mehr was auch von dir kommen kann!

Falls du an dem Termin keine Zeit hast, schreib uns gerne eine E-Mail (junges-engagement@ewnt.org) oder Nachricht bei Facebook (facebook.com/JungesEngagementBtE/). 

Wir freuen uns auf euch!