---
id: '407463696468646'
title: Rojava HealthCare Network - med. Aufbau in Rojava/Nordsyrien
start: '2019-04-30 19:00'
end: '2019-04-30 21:00'
locationName: null
address: Campus Augustusplatz / HS 10
link: 'https://www.facebook.com/events/407463696468646/'
image: 56308641_404785640340903_6889789418719150080_n.jpg
teaser: 'Vortrag & Diskussion: Rojava Health Care Network – medizinischer Aufbau in Rojava/Nordsyrien (mit Dr. Swaantje Illig)  Dr. Swantje Illig arbeitet rege'
recurring: null
isCrawled: true
---
Vortrag & Diskussion: Rojava Health Care Network – medizinischer Aufbau in Rojava/Nordsyrien (mit Dr. Swaantje Illig)

Dr. Swantje Illig arbeitet regelmäßig seit 2015 als Ärztin in Rojava/Westkurdistan in Nordsyrien.
Als Teil ihrer Arbeit vor Ort initiierte Sie das Projekt "Health Care Network", in dem Menschen vor Ort zu Mediziner_Innen ausgebildet werden.
Dabei sind sie Schnittstelle zwischen dem im Aufbau befindenden selbstverwalteten Gesundheitssystem in Rojava/Nordsyrien und Menschen in Deutschland.

Sie vertreten vor Ort mit ihrer Arbeit das Prinzip des solidarischen Gesellschaftsentwurfes, in dem die Bedürfnisse des Menschen Kern der Versorgung sein sollen.

Rojava/Nordsyrien bietet dabei für das Projekt eine konkrete Utopie, die durch das "Health Care Network" unterstützt wird.
Dr. Swantje Illig wird in ihrem Vortrag darüber hinaus über die Situation vor Ort, speziell seit dem völkerrichtswidrigen Angriff der Türkei auf die Region Afrin berichten und informieren, wie die Situation der geflüchteten Familien aus Afrin ist, die derzeit in Flüchtlingslagern in der Region Shebha leben.

Sie wird aufbauend auf ihren Erfahrungen in Rojava die Möglichkeit geben über Gesundheit im Verhältnis zu Gesellschaft zu diskutieren und über die Frage eines Auswegs aus der profitorientierten Medizin.