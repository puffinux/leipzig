---
id: '418593072248724'
title: Einsteiger*innen Workshop von Rhythms of Resistance
start: '2019-05-02 19:00'
end: '2019-05-02 21:00'
locationName: null
address: Campus Augustusplatz / Innenhof
link: 'https://www.facebook.com/events/418593072248724/'
image: 55593272_907062102797323_3417841734711771136_o.jpg
teaser: Wir sind Magda (Ministerium für AGitation und PropaganDA) und Trommeln auf Demos und anderen politischen Aktionen. Wir möchten uns und das Netzwerk Rh
recurring: null
isCrawled: true
---
Wir sind Magda (Ministerium für AGitation und PropaganDA) und Trommeln auf Demos und anderen politischen Aktionen. Wir möchten uns und das Netzwerk Rhythms of Resistance vorstellen, Euch kennenlernen und gemeinsam auf die Trommel hauen. Keine musikalischen Vorkenntnisse nötig.