---
id: '380907965827280'
title: Die KPD und die Weimarer Republik
start: '2019-05-03 15:00'
end: '2019-05-03 17:00'
locationName: null
address: Campus Augustusplatz / S 202
link: 'https://www.facebook.com/events/380907965827280/'
image: 55704575_907062916130575_2237572773171953664_o.jpg
teaser: Die Geburt der Weimarer Republik basierte auf dem Tod der deutschen Arbeiterrevolution. Die junge Republik war jedoch immer noch das Weltepizentrum fü
recurring: null
isCrawled: true
---
Die Geburt der Weimarer Republik basierte auf dem Tod der deutschen Arbeiterrevolution. Die junge Republik war jedoch immer noch das Weltepizentrum für die Widersprüche der bürgerlichen Demokratie im Kapitalismus. Was war die Deutsche Kommunistische Partei, warum hat sie versagt?