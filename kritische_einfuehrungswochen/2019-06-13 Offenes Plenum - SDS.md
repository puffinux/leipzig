---
id: '388257132006751'
title: Offenes Plenum - SDS
start: '2019-06-13 19:15'
end: '2019-06-13 22:15'
locationName: null
address: Institut für Psychologie / Z007
link: 'https://www.facebook.com/events/388257132006751/'
image: 55508110_907508042752729_2906187610793705472_o.jpg
teaser: 'Do, 13.06., 19.15 Uhr:  Offenes Plenum  SDS Leipzig / Institut für Psychologie / Z007  Der Kapitalismus ist nicht das Ende der Geschichte. Es gibt Alt'
recurring: null
isCrawled: true
---
Do, 13.06., 19.15 Uhr:

Offenes Plenum

SDS Leipzig / Institut für Psychologie / Z007

Der Kapitalismus ist nicht das Ende der Geschichte. Es gibt Alternativen zu Sozialabbau, Krieg, Sexismus, Rassismus und Umweltzerstörung. Komm deswegen zu unseren offenen Plena und werde im SDS aktiv! Die offenen Plena sind als guter Einstiegspunkt gedacht, Du kannst aber auch an jeden anderen Donnerstag um 19:15 bei uns vorbeikommen. Unsere Treffen sind offen für alle, die mit uns die Welt verändern wollen.
