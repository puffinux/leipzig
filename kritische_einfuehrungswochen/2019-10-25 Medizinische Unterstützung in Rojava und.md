---
id: "387939628785406"
title: Medizinische Versorgung & gesellschaftliche Situation in Rojava
start: 2019-12-13 19:00
end: 2019-12-13 21:00
address: HS6 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/387939628785406/
image: 74784749_557038641782268_9067461215021694976_o.jpg
teaser: Der Termin steht jetzt fest. Es ist der Freitag, 13.12. Ursprünglich war der
  Termin am 25.10. geplant. Wegen einer kurzfristigen Reise des Referenten
isCrawled: true
---
Der Termin steht jetzt fest. Es ist der Freitag, 13.12.
Ursprünglich war der Termin am 25.10. geplant. Wegen einer kurzfristigen Reise des Referenten Dr. Michael Wilk nach Rojava auf Grund der türkischen Militärinvasion wurde die Veranstaltung nach hinten verschoben.

Fr. 13.12.19
Medizinische Versorgung und gesellschaftliche Situation in Rojava/Nordsyrien (mit Dr. Michael Wilk)

19 Uhr, Uni Campus Augustusplatz, Universitätsstr.1-5, Hörsaal 6

Der Wiesbadener Notarzt und Psychotherapeut Dr. Michael Wilk berichtet über seine Erfahrung mit der Gesundheitsversorgung in Rojava/Demokratische Föderation Nord/Ostsyrien. Er war seit 2014 vielfach in dem Gebiet und unterstützt dort den Kurdischen Roten Halbmond.
Der Bürgerkrieg, die Angriffspolitik der Türkei und das Embargo gegen die Selbstverwaltung durch angrenzende Staaten und Regime lassen die medizinische Versorgung in dem nordsyrischen Gebiet zu einer fast unlösbaren Aufgabe werden. Trotz schwierigster Bedingungen zeigen sich jedoch erste Erfolge einer basisdemokratischen Reorganisierung des Gesundheitswesens.
Dr. Wilk berichtet von den Schwierigkeiten des Wiederaufbaus eines Gesundheitssystems unter Kriegsbedingungen, aber auch von positiven Erfahrungen mit hochmotivierten Menschen, die alles daran setzen, ihre Region wieder lebenswert zu gestalten.
