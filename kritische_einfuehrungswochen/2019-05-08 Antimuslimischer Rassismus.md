---
id: '2282378195341349'
title: Antimuslimischer Rassismus
start: '2019-05-08 19:00'
end: '2019-05-08 21:00'
locationName: null
address: Campus Augustusplatz / HS 11
link: 'https://www.facebook.com/events/2282378195341349/'
image: 55508086_907069056129961_5688437910127247360_o.jpg
teaser: 'Mi, 08.05., 19 – 21 Uhr:  Antimuslimischer Rassismus  SDS Leipzig / Campus Augustusplatz / HS 11  „AfD! Nein zur Moschee!" skandiert Björn Höcke 2016'
recurring: null
isCrawled: true
---
Mi, 08.05., 19 – 21 Uhr:

Antimuslimischer Rassismus

SDS Leipzig / Campus Augustusplatz / HS 11

„AfD! Nein zur Moschee!" skandiert Björn Höcke 2016 auf einer Demo in Erfurt, worauf die Menge antwortet mit: „Widerstand! Widerstand!". Weiter führt er fort: „Und wenn ein Muslim in diesem Land dieses Nein nicht akzeptieren will, dann steht es ihm frei, seinen Gebetsteppich einzurollen, ihn sich unter den Arm zu klemmen und dieses Land zu verlassen!"