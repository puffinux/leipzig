---
id: '307519059863064'
title: Algorithmic Decision Making
start: '2019-05-10 14:30'
end: '2019-05-10 17:00'
locationName: null
address: Campus Augustusplatz / S 126
link: 'https://www.facebook.com/events/307519059863064/'
image: 54727370_907070326129834_3226968852592066560_o.jpg
teaser: 'Fr, 10.05., 14.30 Uhr: Algorithmic Decision Making  AG Link - kritische Informatik Leipzig / Campus Augustusplatz / S 126  Software trifft immer häufi'
recurring: null
isCrawled: true
---
Fr, 10.05., 14.30 Uhr: Algorithmic Decision Making

AG Link - kritische Informatik Leipzig / Campus Augustusplatz / S 126

Software trifft immer häufiger Entscheidungen mit drastischen Auswirkungen auf unser Leben, ob bei der Kreditvergabe, in der Medizin oder vor Gericht. Dabei wird sich oft maschineller Lernverfahren bedient, deren Entscheidungen stark von Daten abhängen, die bestehende gesellschaftliche Verhältnisse reproduzieren. Wir wollen den aktuellen Stand, Probleme und Möglichkeiten von Algorithmic Decision Making diskutieren.