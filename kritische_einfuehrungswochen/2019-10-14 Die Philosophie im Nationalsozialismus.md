---
id: "383634935652165"
title: Die Philosophie im Nationalsozialismus
start: 2019-10-14 17:00
end: 2019-10-14 19:00
address: HS17 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/383634935652165/
image: 70537732_1026273924209473_3959976103363215360_o.jpg
teaser: Mo. 14.10.19 / 17.00  Die Philosophie im Nationalsozialismus  AK Aufarbeitung
  des FSR Philosophie  / S202  Welche Rolle spielte die universitäre Philo
isCrawled: true
---
Mo. 14.10.19 / 17.00

Die Philosophie im Nationalsozialismus

AK Aufarbeitung des FSR Philosophie  / S202

Welche Rolle spielte die universitäre Philosophie in der Zeit des Nationalsozialismus? Dr. George Leaman, Leiter des Philosophy Documentation Center in Charlottesville (USA), wird einen einführenden Überblick über diese Frage geben. Die Veranstaltung besteht aus einem Vortrag (ca. 1h) und einer anschließenden Frage- und Diskussionsrunde.