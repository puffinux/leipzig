---
id: "396070081069292"
title: Alle in einem Boot? Von 1. Klasse Passagier*innen und der Crew
start: 2019-10-22 15:00
end: 2019-10-22 17:00
address: S203 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/396070081069292/
image: 71255724_1032054586964740_5504039223054827520_o.jpg
teaser: "Di. 22.10.19 / 15.00  Alle in einem Boot? Von erste Klasse Passagier*innen
  und der Crew: Klimagerechtigkeit und globale Ungleichheiten   Ende Gelände"
isCrawled: true
---
Di. 22.10.19 / 15.00

Alle in einem Boot? Von erste Klasse Passagier*innen und der Crew: Klimagerechtigkeit und globale Ungleichheiten 

Ende Gelände Leipzig / S203

Der Klimawandel ist heute schon weltweit spürbar und könnte bei gegenwärtiger Entwicklung zukünftig katastrophale Ausmaße annehmen. Doch trifft diese Entwicklung nicht alle Menschen und Regionen gleichschnell und gleichstark. Die daraus resultierenden Ungerechtigkeiten, globalen Machtverhältnisse und Lösungsperspektiven unter dem Stichwort Klimagerechtigkeit wollen wir im Workshop genauer beleuchten.