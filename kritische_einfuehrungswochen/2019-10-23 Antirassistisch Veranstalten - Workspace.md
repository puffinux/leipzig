---
id: "479241766251553"
title: Antirassistisch Veranstalten - Workspace mit EinstiegsInput
start: 2019-10-23 13:00
end: 2019-10-23 15:00
address: S203 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/479241766251553/
image: 71694656_1032188673617998_7918566877910007808_o.jpg
teaser: Mi. 23.10.19 / 13.00  Anti-rassistisch Veranstalten - Workspace mit
  Einstiegs-Input  BPoC-Hochschulgruppe / S203  Eine Veranstaltung umzusetzen
  erford
isCrawled: true
---
Mi. 23.10.19 / 13.00

Anti-rassistisch Veranstalten - Workspace mit Einstiegs-Input

BPoC-Hochschulgruppe / S203

Eine Veranstaltung umzusetzen erfordert Vorbereitung und Tatkraft. Welche Ressourcen stehen zur Verfügung? Wie sieht die Vision aus, wie kann sie verwirklicht werden? usw. So ein Prozess profitiert von zielorientiertem Arbeiten. Leicht wird genau in diesem Rahmen u.A. Rassismus reproduziert. Dieser Workshop widmet sich der Frage: Wie lässt sich eine ‘Null-Toleranz für Rassismus’ Police realisieren?