---
id: "454096375192924"
title: KEW-Küfa - Küche für Alle
start: 2019-10-15 20:00
end: 2019-10-15 23:00
address: Kopfsalat Gästeservice
link: https://www.facebook.com/events/454096375192924/
image: 71166983_1027085870794945_5902633615715467264_o.jpg
teaser: Di. 15.10.19 / 20.00  KEW-Küfa - Küche für Alle  KEW / Kopfsalat Gästeservice
  (Gorkistraße 135, Haltestelle Stannebeinplatz)  Wir - der Kopfsalat e.V.
isCrawled: true
---
Di. 15.10.19 / 20.00

KEW-Küfa - Küche für Alle

KEW / Kopfsalat Gästeservice (Gorkistraße 135, Haltestelle Stannebeinplatz)

Wir - der Kopfsalat e.V. - möchten alle Studierenden, Nachbar*innen und Interessierte am Stannebeinplatz im selbstverwalteten Freiraum, KGS, willkommen heißen. Die KEW-Küfa wollen wir nicht nur zum Schmausen nutzen, sondern auch, um uns mal vorzustellen. Wir freuen uns drauf! 