---
id: '754255668308804'
title: 'Vortrag: Das Spektakel der Wahlen am Beispiel von Guy Debord.'
start: '2019-05-23 18:00'
end: '2019-05-23 21:00'
locationName: null
address: NSG 202
link: 'https://www.facebook.com/events/754255668308804/'
image: 56509917_911529252350608_564184709274271744_o.jpg
teaser: 'Datum: 23.05., 18-21 Uhr Art der Veranstaltung: Vortrag Titel der Veranstaltung: Das Spektakel der Wahlen am Beispiel von Guy Debord. Ort/Raum: NSG 20'
recurring: null
isCrawled: true
---
Datum: 23.05., 18-21 Uhr
Art der Veranstaltung: Vortrag
Titel der Veranstaltung: Das Spektakel der Wahlen am Beispiel von Guy Debord.
Ort/Raum: NSG 202

Guy Debords Schrift „Die Gesellschaft des Spektakels“ beinhaltet auch eine Analyse zur Demokratie des Spektakels. Eine bürgerliche Herrschaftsform des Scheins, der simulierten Opposition, der Polit-Stars, der demokratischen Warenwelt. Um dies zu prüfen und zu analysieren, wird in das Hauptwerk Debords eingeführt und anhand aktueller Beispiele illustriert.