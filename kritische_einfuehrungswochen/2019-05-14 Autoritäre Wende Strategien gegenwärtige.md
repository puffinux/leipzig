---
id: '412310069547654'
title: Autoritäre Wende? Strategien gegenwärtiger Polizeiarbeit
start: '2019-05-14 19:00'
end: '2019-05-14 21:00'
locationName: null
address: Campus Augustusplatz / HS 11
link: 'https://www.facebook.com/events/412310069547654/'
image: 55485153_907078809462319_4506340430685667328_o.jpg
teaser: 'Di, 14.05., 19 Uhr:  Autoritäre Wende? Strategien gegenwärtiger Polizeiarbeit  Copwatch Leipzig / Campus Augustusplatz / HS 11  Ob Hamburg, Katalonien'
recurring: null
isCrawled: true
---
Di, 14.05., 19 Uhr:

Autoritäre Wende? Strategien gegenwärtiger Polizeiarbeit

Copwatch Leipzig / Campus Augustusplatz / HS 11

Ob Hamburg, Katalonien, Paris oder sonstwo: ständig und immer häufiger wird radikaler Protest von hochgerüsteten Robocops angegriffen. Anschließend heißt es: "Polizeigewalt hat es nicht gegeben". Ist das die autoritäre Wende? Oder machen die das nicht schon immer so? Wir beleuchten gegenwärtige Polizeistrategien und sprechen darüber, was das mit liberaler Demokratie, krisenhaftem Kapitalismus und allgemeinem Sicherheitsparadigma zu tun hat. 
