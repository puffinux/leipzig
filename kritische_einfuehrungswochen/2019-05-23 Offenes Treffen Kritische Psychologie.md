---
id: '304814736862385'
title: Offenes Treffen Kritische Psychologie
start: '2019-05-23 17:00'
end: '2019-05-23 19:00'
locationName: null
address: Mersebruger Straße 88b
link: 'https://www.facebook.com/events/304814736862385/'
image: 54519647_907086399461560_6389592098588327936_o.jpg
teaser: 'Do, 23.05., 17 – 19 Uhr:  Offenes Treffen  be:gründet // Kritische Psychologie Leipzig / Merseburger Straße 88b  Wir sind eine offene Gruppe, die sich'
recurring: null
isCrawled: true
---
Do, 23.05., 17 – 19 Uhr:

Offenes Treffen

be:gründet // Kritische Psychologie Leipzig / Merseburger Straße 88b

Wir sind eine offene Gruppe, die sich kollektiv kritisch mit Psychologie befasst. Die Kritische Psychologie nach Klaus Holzkamp ist eine Theorietradition, die sich aus der Kritik am psychologischen Mainstream als eine „marxistische Perspektive auf den Zusammenhang von Psychologie- und Gesellschaftskritik“ (Markard) entwickelt hat. Um uns diese Theorie anzueignen, finden wir uns im Kern als Lesekreis zusammen. Komm gern vorbei und melde Dich bitte vorher via facebook
