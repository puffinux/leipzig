---
id: '578208139325635'
title: Rechte Strukturen in Polizei und Bundeswehr
start: '2019-05-10 15:00'
end: '2019-05-10 17:00'
locationName: null
address: Campus Augustusplatz / S 302
link: 'https://www.facebook.com/events/578208139325635/'
image: 54731122_907070832796450_7625374551811031040_o.jpg
teaser: 'Fr, 10.05., 15 – 17 Uhr:  Rechte Strukturen in Polizei und Bundeswehr  SDS Leipzig / Campus Augustusplatz / S 302  Immer häufiger liest man in Medien'
recurring: null
isCrawled: true
---
Fr, 10.05., 15 – 17 Uhr:

Rechte Strukturen in Polizei und Bundeswehr

SDS Leipzig / Campus Augustusplatz / S 302

Immer häufiger liest man in Medien von rechtsextremen Netzwerken innerhalb der Bundeswehr, Drohungen durch Nazis in der Polizei und Soldaten, die der Reichsbürgerbewegung angehören. In dieser Veranstaltung wollen wir die Hintergründe dieser Strukturen beleuchten und über Probleme, Auswirkungen und Gründe dafür diskutieren.