---
id: '577614936037744'
title: Neue Rechte und Klimawandel
start: '2019-06-11 19:00'
end: '2019-06-11 21:00'
locationName: null
address: Campus Augustusplatz / S 202
link: 'https://www.facebook.com/events/577614936037744/'
image: 55526932_907505949419605_2319547073034190848_o.jpg
teaser: 'Di, 11.06., 19 – 21 Uhr:  Neue Rechte und Klimawandel  SDS Leipzig, Ende Gelände Leipzig / Campus Augustusplatz / S 202  Neue Rechte, das ist nicht nu'
recurring: null
isCrawled: true
---
Di, 11.06., 19 – 21 Uhr:

Neue Rechte und Klimawandel

SDS Leipzig, Ende Gelände Leipzig / Campus Augustusplatz / S 202

Neue Rechte, das ist nicht nur drohender Faschismus, sondern auch ein reaktionärer Bezug zum Klimawandel. Vom Erhalt der Braunkohleabbaugebiete zur vermeintlichen Arbeitsplatz- und Versorgungssicherung, bis hin zu Klimawandelleugnung. Von Repressionen gegen Klimaaktivist*innen und der Aufrechterhaltung kapitalistischer Herrschaftsverhältnisse. Die Veranstaltung erörtert Folgen und Perspektiven für die Klimagerechtigkeitsbewegung in Sachsen unter schwarz-blau.
