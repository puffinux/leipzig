---
id: '2661237007233290'
title: Die verschwiegenen Toten – Todesopfer rechter Gewalt in Leipzig
start: '2019-10-10 13:00'
end: '2019-10-10 15:00'
locationName: null
address: HS19 / Campus Augustusplatz / Uni Leipzig
link: 'https://www.facebook.com/events/2661237007233290/'
image: 70440868_1022403127929886_7275923003152531456_o.jpg
teaser: Do. 10.10.19 / 13.00  Die verschwiegenen Toten – Todesopfer rechter Gewalt in Leipzig seit 1990  Initiativkreis Antirassismus / HS19  Seit 1990 zählt
recurring: null
isCrawled: true
---
Do. 10.10.19 / 13.00

Die verschwiegenen Toten – Todesopfer rechter Gewalt in Leipzig seit 1990

Initiativkreis Antirassismus / HS19

Seit 1990 zählt die Amadeu-Antonio-Stiftung mindestens 184 Todesopfer „rechter Gewalt“ in Deutschland. In Leipzig wurden mindestens acht Menschen umgebracht, hinzu kommen zwei Verdachtsfälle. Damit weist Leipzig im bundesweiten Vergleich die zweithöchste Zahl an rechtsmotivierten Morden auf. Mit dem Vortrag soll an die Opfer gedacht und erinnert werden. Die Veranstaltung will über die Dimension rechter Gewalt in Leipzig aufklären.