---
id: "2533387620032620"
title: „Ein Herz für (Haus-)Tiere“ & „Der Planet brennt, was tun?
start: 2019-10-29 17:00
end: 2019-10-29 19:00
address: S205 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/2533387620032620/
image: 71708634_1034511660052366_3217091222380216320_o.jpg
teaser: Di. 29.10.19 / 17.00  „Ein Herz für (Haus-)Tiere“ & „Der Planet brennt, was
  tun?- ein Ansatz“  V-Change (vegan-vegetarische Hochschulgruppe)/ Campus A
isCrawled: true
---
Di. 29.10.19 / 17.00

„Ein Herz für (Haus-)Tiere“ & „Der Planet brennt, was tun?- ein Ansatz“

V-Change (vegan-vegetarische Hochschulgruppe)/ Campus Augustusplatz/ S205

Eine Erläuterung dazu, warum wir bestimmte Tiere lieben und andere essen (mit Bezug auf Nutztiere in Fleisch-,Milch- und Eierindustrie) & Einführung über die Zusammenhänge der „Tierproduktion“ und deren Auswirkungen auf die Umwelt.