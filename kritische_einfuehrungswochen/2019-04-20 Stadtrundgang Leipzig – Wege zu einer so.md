---
id: '602386950186442'
title: 'Stadtrundgang: Leipzig – Wege zu einer solidarischen Stadt'
start: '2019-04-20 17:30'
end: '2019-04-20 19:30'
locationName: null
address: 'Treffpunkt: Innenhof Campus Augustusplatz'
link: 'https://www.facebook.com/events/602386950186442/'
image: 55935620_908638975972969_3478909005655441408_o.jpg
teaser: 'An den Grenzen Europas entscheidet die europäische Migrations- und Asylpolitik über das Leben tausender Menschen, die Asyl suchen. Die Seebrücke-Beweg'
recurring: null
isCrawled: true
---
An den Grenzen Europas entscheidet die europäische Migrations- und Asylpolitik über das Leben tausender Menschen, die Asyl suchen. Die Seebrücke-Bewegung zeigt, wie hoch das Bedürfnis nach Veränderung ist. Welche Einflussmöglichkeiten gibt es auf lokaler Ebene und welche Initiativen sind bereits vorhanden? Ein Spaziergang durch die Innenstadt zeigt an verschiedenen Stationen, welchen Spielraum Institutionen haben, um solidarische Orte zu schaffen.