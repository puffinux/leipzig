---
id: '716593155419109'
title: Europe is for everyone?
start: '2019-10-09 13:00'
end: '2019-10-09 15:00'
locationName: null
address: S114 / Campus Augustusplatz / Uni Leipzig
link: 'https://www.facebook.com/events/716593155419109/'
image: 70486194_1022391407931058_4651844662724657152_o.jpg
teaser: Mi. 09.10.19 / 13.00  Europe is for everyone? - Fortress Europe und ihre Menschrechtsverletzungen  InEUmanity / Vortrag mit Diskussion / S114  Der Beg
recurring: null
isCrawled: true
---
Mi. 09.10.19 / 13.00

Europe is for everyone? - Fortress Europe und ihre Menschrechtsverletzungen

InEUmanity / Vortrag mit Diskussion / S114

Der Begriff "Festung Europa" ist eine oft genutzte Phrase. Wir wollen gemeinsam mit euch den Begriff mit Inhalt füllen und uns anschauen, wie die EU Menschenrechte verletzt und durch ihr Grenzregime die Lebensgrundlage vieler Menschen zerstört, während sie es immer unmöglicher macht, die EU lebend zu erreichen. Wir erarbeiten uns eine Argumentationsgrundlage, mit der wir die Einflussnahme und das Handeln der EU kritisieren können. 