---
id: '366581357529630'
title: Wie intervenieren in diskriminierende Polizeikontrollen?
start: '2019-05-29 19:00'
end: '2019-05-29 21:00'
locationName: null
address: Campus Augustusplatz / S 202
link: 'https://www.facebook.com/events/366581357529630/'
image: 54729708_907091272794406_7270818404456464384_o.jpg
teaser: 'Mi, 29.05., 19 – 22 Uhr:  Wie intervenieren in diskriminierende Polizeikontrollen?  CopWatch LE / Campus Augustusplatz / S 202  Die Polizei darf an so'
recurring: null
isCrawled: true
---
Mi, 29.05., 19 – 22 Uhr:

Wie intervenieren in diskriminierende Polizeikontrollen?

CopWatch LE / Campus Augustusplatz / S 202

Die Polizei darf an sog. "gefährlichen Orten" verdachtsunabhängig Kontrollen durchführen, ist aber weiterhin an Gesetze gebunden, die ihre Befugnisse einschränken. Wir wollen uns damit beschäftigen, welches Handeln der Polizei zulässig ist und wie man intervenieren kann, wenn man diskriminierende Kontrollen beobachtet. Neben rechtlichen Grundlagen wollen wir uns durch Austausch von Erfahrungen Argumentationshilfen erarbeiten.
