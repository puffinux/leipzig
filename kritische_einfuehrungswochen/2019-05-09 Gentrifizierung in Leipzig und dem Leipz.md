---
id: '1109084765938097'
title: Gentrifizierung in Leipzig und dem Leipziger Osten
start: '2019-05-09 13:00'
end: '2019-05-09 17:00'
locationName: null
address: Campus Augustusplatz / S 015
link: 'https://www.facebook.com/events/1109084765938097/'
image: 55790109_907069612796572_6534257592233885696_o.jpg
teaser: 'Do, 09.05., 13 – 17 Uhr:  Gentrifizierung in Leipzig und dem Leipziger Osten  Organize! / Campus Augustusplatz / S 015  Dieser Workshop ist ein Einfüh'
recurring: null
isCrawled: true
---
Do, 09.05., 13 – 17 Uhr:

Gentrifizierung in Leipzig und dem Leipziger Osten

Organize! / Campus Augustusplatz / S 015

Dieser Workshop ist ein Einführungsworkshop zum Thema Gentrifizierung. Thematisiert werden die Entwicklung des Leipziger Ostens in den letzten Jahren, die jeweilig eigene Rolle innerhalb dieser Prozesse sowie zugrunde zu legende Theorien.