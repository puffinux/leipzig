---
id: "2276285562470756"
title: Gewalterfahrungen von lSBTTIQ* in Sachsen
start: 2019-10-16 17:00
end: 2019-10-16 19:00
address: S202 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/2276285562470756/
image: 71687726_2848029731915789_4573174360944148480_n.jpg
teaser: Mi. 16.10.19 / 17.00  Gewalterfahrungen von LSBTTIQ* in Sachsen  DGB
  Hochschulgruppe / ver.di Bezirk Leipzig-Nordsachsen / S202  Trotz
  gesellschaftlic
isCrawled: true
---
Mi. 16.10.19 / 17.00

Gewalterfahrungen von LSBTTIQ* in Sachsen

DGB Hochschulgruppe / ver.di Bezirk Leipzig-Nordsachsen / S202

Trotz gesellschaftlicher und rechtlicher Fortschritte gehören Anfeindungen, Ausgrenzung und Gewalt für viele Lesben, Schwule, Bisexuelle, Trans*- und Inter*-Personen und queere Menschen (LSBTTIQ*) in Sachsen zum Alltag. Eine erste fragebogenbasierte Erhebung für den Freistaat verdeutlicht das bestehende Dunkelfeld vorurteilsbezogener Gewalt bzw. Hasskriminalität gegen LSBTTIQ*. Referentin: Vera Ohlendorf, LAG Queeres Netzwerk Sachsen e.V.