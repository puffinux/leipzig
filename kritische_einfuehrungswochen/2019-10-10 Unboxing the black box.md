---
id: '930795420614132'
title: Unboxing the black box
start: '2019-10-10 11:00'
end: '2019-10-10 13:00'
locationName: null
address: S114 / Campus Augustusplatz / Uni Leipzig
link: 'https://www.facebook.com/events/930795420614132/'
image: 70814592_1022401067930092_4358870503784448000_o.jpg
teaser: 'Do. 10.10.19 / 11.00  Unboxing the black box: Algorithmen und ihre gesellschaftlichen Auswirkungen  Link - AG für kritische Informatik an der Uni Leip'
recurring: null
isCrawled: true
---
Do. 10.10.19 / 11.00

Unboxing the black box: Algorithmen und ihre gesellschaftlichen Auswirkungen

Link - AG für kritische Informatik an der Uni Leipzig / S114

Die heutige soziale Infrastruktur basiert zunehmend auf Algorithmen. Gleichzeitig fungieren sie als „Black Boxes“: Ihre Funktionsweise ist für die allgemeine Bevölkerung unklar, was es schwierig macht, deren Auswirkungen auf verschiedene soziale Gruppen zu erkennen. Gemeinsam werden wir uns theoretisch und praktisch damit beschäftigen, was ein Algorithmus ist, wie er funktioniert und welchen Einfluss er auf unser gesellschaftliches Zusammenleben hat. Keine Vorkenntnisse notwendig!