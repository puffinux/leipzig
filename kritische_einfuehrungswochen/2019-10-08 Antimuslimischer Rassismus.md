---
id: '2425915501024894'
title: Antimuslimischer Rassismus
start: '2019-10-08 15:00'
end: '2019-10-08 17:00'
locationName: null
address: S127 / Campus Augustusplatz / Uni Leipzig
link: 'https://www.facebook.com/events/2425915501024894/'
image: 70767036_1021540218016177_4188726911951699968_o.jpg
teaser: Di.08.10.19 / 15.00  Antimuslimischer Rassismus  Referat ausländische Studierende / S127  Der Hass gegenüber Muslime grassiert unverhohlen. Moscheen w
recurring: null
isCrawled: true
---
Di.08.10.19 / 15.00

Antimuslimischer Rassismus

Referat ausländische Studierende / S127

Der Hass gegenüber Muslime grassiert unverhohlen. Moscheen werden angegriffen und auf offener Straße werden Kopftücher von den Köpfen von Muslima heruntergerissen. Der Rassismus wird dabei nicht nur von der extremen Rechten auf die Straße getragen, sondern auch von der „Mitte“ der Gesellschaft aus geschürt. In diesem Workshop wird das Aufkommen des antimuslimischen Rassismus analysiert und die Frage diskutiert, wie diesem entschieden begegnet werden kann.