---
id: '264984867722139'
title: 'Verfassung schützen, Verfassungsschutz abschaffen?'
start: '2019-05-06 17:00'
end: '2019-05-06 19:00'
locationName: null
address: Campus Augustusplatz / HS 11
link: 'https://www.facebook.com/events/264984867722139/'
image: 54729337_907066462796887_4598776931578544128_o.jpg
teaser: 'Mo, 06.05., 17 – 19 Uhr:  Verfassung schützen, Verfassungsschutz abschaffen?  KEW / Campus Augustusplatz / HS 11  Die letzten Jahre haben deutlich ge'
recurring: null
isCrawled: true
---

Mo, 06.05., 17 – 19 Uhr:

Verfassung schützen, Verfassungsschutz abschaffen?

KEW / Campus Augustusplatz / HS 11

Die letzten Jahre haben deutlich gezeigt, der Verfassungsschutz verfolgt eine eigene politische Agenda. Seien es die Verstrickungen in den NSU Komplex, die Verbindung zur extremen Rechten oder die Causa Maaßen, so kann nicht mehr von einer neutralen Position dieser Institution ausgegangen werden. Die Einmischung in die KEW 2018 hat gezeigt wie sehr der Einfluss des VS auch Studierende betrifft. Diese Problematik möchten wir mit Expert*innen erörtern.