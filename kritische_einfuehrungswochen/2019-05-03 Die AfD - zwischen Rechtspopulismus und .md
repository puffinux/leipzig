---
id: '2263881063869002'
title: Die AfD - zwischen Rechtspopulismus und Neo-Faschismus
start: '2019-05-03 19:00'
end: '2019-05-03 21:00'
locationName: null
address: Campus Augustusplatz / HS 4
link: 'https://www.facebook.com/events/2263881063869002/'
image: 54799885_907063539463846_6762586493299982336_o.jpg
teaser: 'Die AfD - zwischen Rechtspopulismus und Neo-Faschismus  SDS Leipzig / Campus Augustusplatz / HS 4  Seit ihrer Gründung 2013, als Anti-Euro-Partei, hat'
recurring: null
isCrawled: true
---
Die AfD - zwischen Rechtspopulismus und Neo-Faschismus

SDS Leipzig / Campus Augustusplatz / HS 4

Seit ihrer Gründung 2013, als Anti-Euro-Partei, hat sich die AfD
immer mehr zum parlamentarischen Arm für die extreme Rechte entwickelt. Die Partei "häutet" sich, wirft alte Kader und Führungspersonen heraus und zeigt wie stark der völkisch-autoritäre Flügel den Diskurs in der Partei dominiert. Wir wollen diskutieren, wo die Partei heute steht und wie ein breiter und entschlossener Widerstand gegen die rechte Mobilmachung aussehen kann. 