---
id: "386974728666455"
title: Kritische Männlichkeitsreflektion
start: 2019-10-14 15:00
end: 2019-10-14 19:00
address: S204 / Uni Leipzig / Campus Augustusplatz
link: https://www.facebook.com/events/386974728666455/
image: 70609236_1026271314209734_4576157967350497280_o.jpg
teaser: Mo. 14.10.19 / 15.00-19.00  Kritische Männlichkeitsreflektion  Prisma -
  Interventionistische Linke Leipzig / S204  Männlichkeit stellt in der patriarc
isCrawled: true
---
Mo. 14.10.19 / 15.00-19.00

Kritische Männlichkeitsreflektion

Prisma - Interventionistische Linke Leipzig / S204

Männlichkeit stellt in der patriarchalen Gesellschaft die Norm dar und wird höher bewertet als Weiblichkeit - dadurch entstehende Privilegien werden jedoch meist übersehen. Im Workshop setzen wir uns mit Männlichkeiten in unseren Lebenswirklichkeiten auseinander. Wir reflektieren, wie wir auf Partys, in Politgruppen, in Freundschaften und Beziehungen selbst Männlichkeiten herstellen, stützen und wahrnehmen. Voraussetzung: Bereitschaft zur kritischen Selbstreflexion.