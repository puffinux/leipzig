---
id: "914720102232645"
title: Taggen für die Umwelt
start: 2019-10-18 13:00
end: 2019-10-18 15:00
address: S122 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/914720102232645/
image: 71349632_1029191160584416_1798670945662009344_o.jpg
teaser: Fr. 18.10.19 / 13.00  Taggen für die Umwelt   AG Nachhaltige Uni / S122  Du
  willst an der Uni für ein Thema aus dem Bereich der Nachhaltigkeit Bewusst
isCrawled: true
---
Fr. 18.10.19 / 13.00

Taggen für die Umwelt 

AG Nachhaltige Uni / S122

Du willst an der Uni für ein Thema aus dem Bereich der Nachhaltigkeit Bewusstsein schaffen, welches dir am Herzen liegt? Dann entwirf mit uns einen Sticker für dein Anliegen und tagge ihn auf alle Uniklos etc. und hinterlasse so eine kleine grüne Botschaft im Unialltag! Die Verwirklichung deiner Tag-Ideen erfolgt natürlich auf umweltfreundlichen Klebepapier!