---
id: '298959887448372'
title: Erfahrungsberichte schulischen Engagements gegen Rechts
start: '2019-05-02 17:00'
end: '2019-05-02 19:00'
locationName: null
address: Campus Augustusplatz / S 015
link: 'https://www.facebook.com/events/298959887448372/'
image: 54798140_907060699464130_4484941198514978816_o.jpg
teaser: In einer offenen Gesprächsrunde berichten Schüler*innen und Akteur*innen aus dem Bildungsbereich von ihrem Engagement gegen Diskriminierung und rechte
recurring: null
isCrawled: true
---
In einer offenen Gesprächsrunde berichten Schüler*innen und Akteur*innen aus dem Bildungsbereich von ihrem Engagement gegen Diskriminierung und rechte Tendenzen an Schulen. Nach einer kurzen Vorstellung und den Erfahrungsberichten wird der Raum schnell für Fragen und eigene Erfahrungen geöffnet. Insbesondere angehende Lehrkräfte sollen dabei Anregungen für die zukünftige berufliche Praxis bekommen.