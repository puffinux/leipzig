---
id: '290123984989947'
title: Türkische Kommunist*innen in Leipzig und der DDR
start: '2019-04-29 19:00'
end: null
locationName: null
address: Campus Augustusplatz / Hörsaal 6
link: 'https://www.facebook.com/events/290123984989947/'
image: 53361285_390747605078040_7989696724652261376_n.jpg
teaser: Türkische Kommunist*innen in Leipzig und der DDR  (Nachholtermin für die urspr.  am Mo. 08.04. geplante Veranstaltung)  Vortrag und Diskussion über ei
recurring: null
isCrawled: true
---
Türkische Kommunist*innen in Leipzig und der DDR

(Nachholtermin für die urspr.  am Mo. 08.04. geplante Veranstaltung)

Vortrag und Diskussion über ein bis heute unbekanntes Kapitel migrantischer Geschichte in Leipzig

Mehr als 30 Jahre war Leipzig der Exil-Hauptsitz der Kommunistischen Partei der Türkei (TKP), deren Mitglieder in der Türkei verfolgt wurden. Mitarbeiter der Leipziger Parteizentrale der TKP lebten in der Stadt, ihre Kinder besuchten Leipziger Schulen – all dies geschah allerdings unter strenger Geheimhaltung.

Von Leipzig aus organisierte die TKP bis zur Auflösung des Exil-Hauptsitzes (Ende der 1980er Jahre) unter anderem Aktivitäten in Westdeutschland sowie in der Türkei.

Nelli Tügel, Journalistin beim „neuen deutschland“, ist selbst Tochter eines der türkischen Exil-Kommunisten, die in die DDR emigriert sind und dort gelebt haben. Sie hat aus verschiedenen Archiven umfangreiches Material über das Leben von türkischen politischen Emigrant*innen in der DDR zusammengetragen und recherchiert, wie sich türkische Linke in den 1970er und 1980er Jahren zwischen der Türkei, der Bundesrepublik und der DDR organisierten und vernetzten.
