---
id: "980501875635090"
title: Offenes Plenum - Copwatch Leipzig
start: 2019-10-31 19:30
end: 2019-10-31 22:30
locationName: Helmut
address: Kohlgartenstraße 51, 04315 Leipzig
link: https://www.facebook.com/events/980501875635090/
image: 71517742_1034526576717541_8037062860943130624_o.jpg
teaser: 31.10.19 / 19.30   Offenes Plenum  Copwatch Leipzig / Helmut (Kohlgartenstr.
  51)  Copwatch Leipzig beschäftigt sich mit autoritärer Entwicklung, Poliz
isCrawled: true
---
31.10.19 / 19.30 

Offenes Plenum

Copwatch Leipzig / Helmut (Kohlgartenstr. 51)

Copwatch Leipzig beschäftigt sich mit autoritärer Entwicklung, Polizeigewalt, racial profiling und dem neuem Sächs. Polizeigesetz. Ein Schwerpunkt unserer Arbeit liegt auch auf der Waffenverbotszone Eisenbahnstraße. Wir geben Workshops und Material zur Aufklärung über die Befugnisse der Cops, eigene Rechte, wie man Polizeikontrollen kritisch begleiten und mit Polizeigewalt umgehen kann. Komm gern vorbei, wenn du dich für unsere Arbeit interessierst.