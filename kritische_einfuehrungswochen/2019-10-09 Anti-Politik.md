---
id: '467802743834886'
title: Anti-Politik
start: '2019-10-09 17:00'
end: '2019-10-09 19:00'
locationName: null
address: HS19 / Campus Augustusplatz / Uni Leipzig
link: 'https://www.facebook.com/events/467802743834886/'
image: 70440751_1022394651264067_6418249271438475264_o.jpg
teaser: Mi. 09.10.19 / 17.00  (Anti-)Politik  ASJ Leipzig / HS19   Schon auf den ersten Blick scheint ein anarchistischer Begriff von Politik ambivalent ( = i
recurring: null
isCrawled: true
---
Mi. 09.10.19 / 17.00

(Anti-)Politik

ASJ Leipzig / HS19 

Schon auf den ersten Blick scheint ein anarchistischer Begriff von Politik ambivalent ( = in sich widersprüchlich, zwiespältig) zu sein. Geht es im Anarchismus also letztendlich um die völlige Abschaffung der Politik oder die Politisierung aller Lebensbereiche? Ist der Weg dazu eine politische Organisierung und sollte dieser explizit anti-politisch sein?