---
id: "363202857921818"
title: Benotungs-, Selektions- und Konkurrenzdruck in Bildungsstätten
start: 2019-10-14 11:00
end: 2019-10-14 13:00
address: S203 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/363202857921818/
image: 70455554_1026268694209996_6496091885776404480_o.jpg
teaser: Mo. 14.10.19 / 11.00  Benotungs-, Selektions- und Konkurrenzdruck in
  Bildungsstätten  Kritische Lehrer*innen / S203  Während unserer gesamten
  (Aus-)Bi
isCrawled: true
---
Mo. 14.10.19 / 11.00

Benotungs-, Selektions- und Konkurrenzdruck in Bildungsstätten

Kritische Lehrer*innen / S203

Während unserer gesamten (Aus-)Bildung werden wir durch Noten bewertet. Doch inwiefern nützen diese pädagogischen Zielsetzungen? Wie bestimmt eine ständige Bewertung unser Selbstbild? Und inwiefern verstärken Noten gesellschaftliche Ungleichheiten und tragen zur Leistungsgesellschaft bei? Über diese Fragen wollen wir ins Gespräch kommen und mögliche Alternativen finden, um nachhaltiges Lernen zu fördern.