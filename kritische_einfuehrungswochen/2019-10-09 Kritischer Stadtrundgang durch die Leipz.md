---
id: '693367921131984'
title: Kritischer Stadtrundgang durch die Leipziger Innenstadt
start: '2019-10-09 15:00'
end: '2019-10-09 17:00'
locationName: null
address: Couchcafé / Campus Augustusplatz / Uni Leipzig
link: 'https://www.facebook.com/events/693367921131984/'
image: 71017792_1022396531263879_4358439946197925888_o.jpg
teaser: 'Mi. 09.10.19 / 15.00  Kritischer Stadtrundgang durch die Leipziger Innenstadt  Prisma - IL Leipzig / Startpunkt: Couchcafé   Der Stadtrundgang themati'
recurring: null
isCrawled: true
---
Mi. 09.10.19 / 15.00

Kritischer Stadtrundgang durch die Leipziger Innenstadt

Prisma - IL Leipzig / Startpunkt: Couchcafé 

Der Stadtrundgang thematisiert einzelne Aspekte aktueller Stadtentwicklung in Leipzig. Aktuell kommt die Gentrifizierungsmaschinerie in der Stadt so richtig in Gang und große Immobilienfirmen bauen an allen Ecken der Stadt und wir fragen uns: wer soll da zu den aufgerufenen Preisen eigentlich wohnen? Wir wollen mit euch durch die (erweiterte) Innenstadt spazieren und dabei über die Entwicklungen in Leipzig ins Gespräch kommen. Neben Gentrifizierung im generellen, wollen wir über das neue Stadtviertel am Barischen Bahnhof, die Rolle von Vonovia als Großvermieter, die Versicherheitlichung sozialer Themen und möglichen Linken Strategien für eine Solidarische Stadt sprechen.