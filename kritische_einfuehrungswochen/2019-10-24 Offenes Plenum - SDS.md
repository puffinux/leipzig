---
id: "688184268362092"
title: Offenes Plenum - SDS
start: 2019-10-24 19:00
end: 2019-10-24 22:00
address: Z007 / Institut für Psychologie / Uni Leipzig (Neumarkt 9-19)
link: https://www.facebook.com/events/688184268362092/
image: 72316977_1032234766946722_4377860774698156032_n.jpg
teaser: Do. 24.10.19 / 19.00  Offenes Plenum   SDS Leipzig / Z007 - Institut für
  Psychologie   Der Kapitalismus ist nicht das Ende der Geschichte. Es gibt Alt
isCrawled: true
---
Do. 24.10.19 / 19.00

Offenes Plenum 

SDS Leipzig / Z007 - Institut für Psychologie 

Der Kapitalismus ist nicht das Ende der Geschichte. Es gibt Alternativen zu Sozialabbau, Krieg, Sexismus, Rassismus und Umweltzerstörung. Komm deswegen zu unseren offenen Plena und werde im SDS aktiv! Die offenen Plena sind als guter Einstiegspunkt gedacht, Du kannst aber auch an jedem anderen Donnerstag um 19:15 bei uns vorbeikommen. Unsere Treffen sind offen für alle, die mit uns die Welt verändern wollen.