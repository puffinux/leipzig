---
id: "987417821602152"
title: FÄLLT AUS - Was ist Klassenbewusstsein?
start: 2019-10-23 15:00
end: 2019-10-23 17:00
address: S203 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/987417821602152/
image: 71185101_1032191160284416_7026279914563174400_o.jpg
teaser: Aufgrund von einer Erkrankung muss der Vortrag "Was ist Klassenbewusstsein?"
  leider ausfallen. Es wird versucht werden ihn zu einem späteren Termin na
isCrawled: true
---
Aufgrund von einer Erkrankung muss der Vortrag "Was ist Klassenbewusstsein?" leider ausfallen. Es wird versucht werden ihn zu einem späteren Termin nachzuholen. 

---

"Es handelt sich nicht darum, was dieser oder jener Proletarier oder selbst das ganze Proletariat, als Ziel sich einstweilen vorstellt. Es handelt sich darum, was es ist, und was diesem Sein gemäß geschichtlich zu tun gezwungen sein wird“ (Marx: Die heilige Familie). Wir wollen uns dem zentralen Problem des Klassenbewusstseins innerhalb der marxistischen Theorie nähern. Was hat es historisch bedeutet und welche Relevanz ergibt sich für heute?