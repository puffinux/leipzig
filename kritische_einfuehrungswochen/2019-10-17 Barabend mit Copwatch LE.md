---
id: "408052303221060"
title: Barabend mit Copwatch LE
start: 2019-10-17 21:00
end: 2019-10-18 02:00
address: WRZ
link: https://www.facebook.com/events/408052303221060/
image: 73063579_1044350409068491_712331398467289088_n.jpg
teaser: Am 05.11.19 jährt sich die Einrichtung der Waffenverbotszone im Leipziger
  Osten. Damit beginnt auch ihre „Evaluation“ seitens der zuständigen Behörden
isCrawled: true
---
Am 05.11.19 jährt sich die Einrichtung der Waffenverbotszone im Leipziger Osten. Damit beginnt auch ihre „Evaluation“ seitens der zuständigen Behörden. Hierbei wird entschieden, ob die Zone bleibt oder wieder abgeschafft wird. Unsere Auswertung ist bereits abgeschlossen: Die WVZ ist ein repressives Kontrollinstrument, das demokratische Grundrechte einschränkt, Racial Profiling legitimiert und die Willkür der Polizei befördert. Deshalb starten wir am Donnerstag die Kampagne 
*WVZ abschießen – Soziale Sicherheit stärken – Unsere Waffe heißt Solidarität*
Es wird einen Input zu den Ereignissen des vergangenen Jahres geben, außerdem fancy Drinks und Mini-Games!
Cheers, Copwatch

Donnerstag 17.10. ab 21 Uhr
in der WRZ

Den kompletten Aufruf zur Kampagne findet ihr unter: 
https://copwatchleipzig.home.blog/wvz-abschiesen/

Aktuelle Infos unter:
https://twitter.com/copwatch_le