---
id: '347806909174792'
title: 'Klagen gegen Abschiebungen: Input vom Peperoncini e.V.'
start: '2019-06-04 16:00'
end: '2019-06-04 18:00'
locationName: null
address: Campus Augustusplatz / S 015
link: 'https://www.facebook.com/events/347806909174792/'
image: 55492918_907501206086746_3817427049342042112_o.jpg
teaser: 'Di, 04.06., 16 – 18 Uhr:  Klagen gegen Abschiebungen: Input vom Peperoncini e.V.  Peperoncini e.V.  / Campus Augustusplatz / S 015  Der Peperoncini e.'
recurring: null
isCrawled: true
---
Di, 04.06., 16 – 18 Uhr:

Klagen gegen Abschiebungen: Input vom Peperoncini e.V.

Peperoncini e.V.  / Campus Augustusplatz / S 015

Der Peperoncini e.V. finanziert seit 2015 Klagen gegen Abschiebungen. Aktuell ist der Verein der einzig aktive Leipziger Rechtshilfefonds. Am 05.06.2019 möchten wir unsere Arbeit vorstellen und von unseren Erfahrungen bei Klagen gegen das BAMF berichten. Nach einem kurzen Input gibt es die Möglichkeit in einen lockeren Austausch zu gehen und Fragen zu stellen.
