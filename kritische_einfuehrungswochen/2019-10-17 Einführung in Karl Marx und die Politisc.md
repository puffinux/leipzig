---
id: "426896714623672"
title: Einführung in Karl Marx und die Politische Ökonomie
start: 2019-10-17 13:00
end: 2019-10-17 15:00
address: HS8 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/426896714623672/
image: 71515700_1032244560279076_8461737411716579328_n.jpg
teaser: Do. 17.10.19 / 13.00  Einführung in Karl Marx und die Politische Ökonomie  SDS
  Leipzig / HS8  Die Kritik am kapitalistischen Wirtschaftssystem ist in
isCrawled: true
---
Do. 17.10.19 / 13.00

Einführung in Karl Marx und die Politische Ökonomie

SDS Leipzig / HS8

Die Kritik am kapitalistischen Wirtschaftssystem ist in aller Munde. Gegenwärtig stellen Bewegungen für Klimagerechtigkeit und bezahlbares Wohnen verstärkt die Frage nach dem System. In einem einführenden Workshop wollen wir uns mit den grundlegenden marxistischen Ideen beschäftigen und über Ausbeutung und Unterdrückung im Kapitalismus diskutieren.