---
id: "1679728972158212"
title: Das Digitale im kapitalistischem Zeitalter
start: 2019-10-18 19:00
end: 2019-10-18 21:00
address: HS2 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/1679728972158212/
image: 70903081_1030498853786980_6710764938324869120_o.jpg
teaser: Fr. 18.10.19 / 19.00  Das Digitale im kapitalistischem Zeitalter  Link - AG
  für kritische Informatik / HS2  Ob es darum geht, die ganze Erde zu kartie
isCrawled: true
---
Fr. 18.10.19 / 19.00

Das Digitale im kapitalistischem Zeitalter

Link - AG für kritische Informatik / HS2

Ob es darum geht, die ganze Erde zu kartieren oder alle Freundschaften der Welt zu organisieren – im digitalen Kapitalismus werden Algorithmen zur wichtigsten Maschine, Daten zum essenziellen Rohstoff und Informationen zur Ware Nummer eins. Zur Frage nach dem Neuen im alten Kapitalismus referiert Timo Daum Autor von „Das Kapital sind wir“, welches 2018 mit dem Preis des politischen Buches der Friedrich Ebert-Stiftung ausgezeichnet wurde.
In Kooperation mit der Rosa-Luxemburg-Stiftung Sachsen.