---
id: "527506417821480"
title: Lass dich nicht abziehen - Kenne deine Rechte im Nebenjob
start: 2019-10-21 11:00
end: 2019-10-21 13:00
address: S203 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/527506417821480/
image: 72551948_2099903860109861_3922225801847635968_n.jpg
teaser: Mo. 21.10.19 / 11.00  Lass dich nicht abziehen - Kenne deine Rechte im
  Nebenjob und Praktikum!  Students at work / DGB Hochschulgruppe /
  S203   Studie
isCrawled: true
---
Mo. 21.10.19 / 11.00

Lass dich nicht abziehen - Kenne deine Rechte im Nebenjob und Praktikum!

Students at work / DGB Hochschulgruppe / S203 

Studierende arbeiten häufig zur Studienfinanzierung unter prekären Bedingungen. Wir klären darüber auf, welche Rechte du am Arbeitsplatz oder im Praktikum hast und wie du sie einfordern kannst: Mindestlohn, Urlaub, Lohnfortzahlung bei Krankheit, Kündigungsschutz. Was musst du beachten, wenn du gleichzeitig studierst und arbeitest? Wer zahlt die Sozialversicherung? Nur wenn wir anfangen uns zu wehren, schaffen wir am Ende bessere Arbeitsbedingungen für alle.