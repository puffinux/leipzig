---
id: '2338642096459808'
title: Universität Leipzig – zukunftsfähig?!
start: '2019-05-21 17:00'
end: '2019-05-21 19:00'
locationName: null
address: Campus Augustusplatz / S 420
link: 'https://www.facebook.com/events/2338642096459808/'
image: 54798444_907084306128436_1252191465971384320_o.jpg
teaser: 'Di, 21.05., 17 – 19 Uhr:  Universität Leipzig – zukunftsfähig?!  AG Nachhaltige Uni / Campus Augustusplatz / S 420  Fishbowl-Diskussion mit Akteur*inn'
recurring: null
isCrawled: true
---
Di, 21.05., 17 – 19 Uhr:

Universität Leipzig – zukunftsfähig?!

AG Nachhaltige Uni / Campus Augustusplatz / S 420

Fishbowl-Diskussion mit Akteur*innen der Uni Leipzig, geleitet durch netzwerk n. Ziel ist es über den aktuellen Stand von Nachhaltigkeit an der Uni Leipzig zu informieren, Entwicklungsmöglichkeiten aufzuzeigen und Handlungen abzuleiten. Organisiert durch die AG Nachhaltige Uni, die sich dafür einsetzt, die Hochschule zu einer nachhaltigeren Institution zu transformieren, weil angesichts des Klimawandels Nachhaltigkeit auch an der Uni von großer Relevanz ist.
