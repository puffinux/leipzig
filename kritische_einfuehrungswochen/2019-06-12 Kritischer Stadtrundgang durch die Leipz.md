---
id: '2013594602277670'
title: Kritischer Stadtrundgang durch die Leipziger Innenstadt
start: '2019-06-12 17:00'
end: '2019-06-12 19:00'
locationName: null
address: 'Treffpunkt: Campus Augustusplatz Innenhof'
link: 'https://www.facebook.com/events/2013594602277670/'
image: 55480096_907506372752896_2996042265111035904_o.jpg
teaser: 'Mi, 12.06., 17 – 19 Uhr:  Kritischer Stadtrundgang durch die Leipziger Innenstadt  Prisma – IL Leipzig / Treffpunkt: Campus Augustusplatz Innenhof  De'
recurring: null
isCrawled: true
---
Mi, 12.06., 17 – 19 Uhr:

Kritischer Stadtrundgang durch die Leipziger Innenstadt

Prisma – IL Leipzig / Treffpunkt: Campus Augustusplatz Innenhof

Der Stadtrundgang thematisiert einzelne Aspekte aktueller Stadtentwicklung in Leipzig. Aktuell kommt die Gentrifizierungsmaschinerie in der Stadt so richtig in Gang. Wir wollen mit euch durch die Innenstadt spazieren und dabei über die Entwicklungen in Leipzig ins Gespräch kommen. Neben Gentrifizierung im generellen, wollen wir über die Themen Luxuswohnen, die LWB als Akteurin des kommunalen Wohnungsbaus sowie die Versicherheitlichung sozialer Themen sprechen.
