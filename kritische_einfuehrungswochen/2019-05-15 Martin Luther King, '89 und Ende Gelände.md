---
id: '315930725792198'
title: 'Martin Luther King, ''89 und Ende Gelände'
start: '2019-05-15 17:00'
end: '2019-05-15 19:00'
locationName: null
address: Campus Augustusplatz / HS 17
link: 'https://www.facebook.com/events/315930725792198/'
image: 55600721_907079759462224_546849126050955264_o.jpg
teaser: 'Mi, 15.05., 17 Uhr:  Martin Luther King, ''89 und Ende Gelände. Ziviler Ungehorsam als demokratisches Mittel und Beispiele aus der Klimagerechtigkeitsb'
recurring: null
isCrawled: true
---
Mi, 15.05., 17 Uhr:

Martin Luther King, '89 und Ende Gelände. Ziviler Ungehorsam als demokratisches Mittel und Beispiele aus der Klimagerechtigkeitsbewegung

Ende Gelände Leipzig, Prisma – IL Leipzig / Campus Augustusplatz / HS 17

In der Demokratie treffen unterschiedliche Interessen aufeinander, doch die Ressourcen der politischen Kontrahent*innen sind oft ungleich verteilt. Industrie und Regierung verteidigen ihre Interessen, während den Betroffenen oft nicht mehr bleibt als rechtliche Grenzen zu überschreiten und sich dem selbst entgegenzustellen. Wir wollen uns ansehen, warum ungehorsames Verhalten geschichtlich und demokratietheoretisch legitim und notwendig ist.
