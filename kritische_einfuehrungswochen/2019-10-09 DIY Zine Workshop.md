---
id: '481299306051938'
title: DIY Zine Workshop
start: '2019-10-09 11:00'
end: '2019-10-09 13:00'
locationName: null
address: S114 / Campus Augustusplatz / Uni Leipzig
link: 'https://www.facebook.com/events/481299306051938/'
image: 71045420_1021547828015416_1442861324987006976_o.jpg
teaser: 'Mi. 09.10.19 / 11.00  diy zine workshop  KEW / S114  zines (selbst veröffentlichte, nicht-kommerzielle Hefte in kleiner Auflage) eignen sich wunderbar'
recurring: null
isCrawled: true
---
Mi. 09.10.19 / 11.00

diy zine workshop

KEW / S114

zines (selbst veröffentlichte, nicht-kommerzielle Hefte in kleiner Auflage) eignen sich wunderbar, um politische Ansichten kreativ auszudrücken und in anschaulicher Weise zu verbreiten. Sie sind schnell gebastelt, noch schneller kopiert und daraufhin schnell in der ganzen Stadt verteilt! Also kommt vorbei, bringt gerne eigene Bilder/Materialen mit oder nutzt unsere, sammelt schon vorher coole Ideen oder lasst Euch vor Ort inspirieren, und: bastelt drauf los!