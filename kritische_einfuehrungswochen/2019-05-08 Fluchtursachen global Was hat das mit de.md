---
id: '615654908908766'
title: Fluchtursachen global? Was hat das mit der EU zu tun?
start: '2019-05-08 16:30'
end: '2019-05-08 19:00'
locationName: null
address: to be announced
link: 'https://www.facebook.com/events/615654908908766/'
image: 55674641_907068566130010_8851173259064901632_o.jpg
teaser: 'Mi, 08.05., 16.30 – 19 Uhr:  Fluchtursachen global? Was hat das mit der EU zu tun?  InEUmanity / Datum kann sich evtl. noch ändern – aktuelle Infos gi'
recurring: null
isCrawled: true
---
Mi, 08.05., 16.30 – 19 Uhr:

Fluchtursachen global? Was hat das mit der EU zu tun?

InEUmanity / Datum kann sich evtl. noch ändern – aktuelle Infos gibt es auf facebook sowie unserer Webseite.
Weltweit sind etwa 65 Millionen Menschen auf der Flucht. Die Ursachen dafür sind vielfältig und komplex. Wir wollen gemeinsam versuchen uns einem Verständnis von Fluchtursachen anzunähern, welches sowohl geschichtliche als auch politische Zusammenhänge mitdenkt. Vor allem aber soll die Frage beantwortet werden: Was hat das denn alles mit uns im globalen Norden zu tun?