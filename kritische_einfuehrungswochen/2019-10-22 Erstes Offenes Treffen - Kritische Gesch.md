---
id: "514827735754248"
title: Erstes Offenes Treffen - Kritische Geschichtsstudierende
start: 2019-10-22 19:00
end: 2019-10-22 22:00
address: S205 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/514827735754248/
image: 71776487_1032186390284893_5993168472593399808_o.jpg
teaser: Di. 22.10.19 / 19.00  Erstes Offenes Treffen - Kritische
  Geschichtsstudierende   Kritische Geschichtsstudierende / S205  Wir wollen
  einen unabhängigen
isCrawled: true
---
Di. 22.10.19 / 19.00

Erstes Offenes Treffen - Kritische Geschichtsstudierende 

Kritische Geschichtsstudierende / S205

Wir wollen einen unabhängigen, solidarischen und leistungsbefreiten Ort schaffen zur Erarbeitung von Inhalten unseres Geschichtsstudiums, die zu kurz kommen - für Vernetzung, Austausch und Kritik. Über allem soll dabei eine kritische Betrachtung der Geschichtswissenschaft als solcher stehen. Unsere Gruppe steht noch ganz am Anfang. Kommt zu unserem ersten offenen Treffen, vernetzt euch, gestaltet mit uns zusammen Strukturen! Alle Semester sind willkommen!