---
id: '2134735319974387'
title: Alle in einem Boot? Von 1.Klasse Passagier*innen und der Crew
start: '2019-04-23 15:00'
end: '2019-04-23 17:00'
locationName: null
address: Campus Augustusplatz / Ziegenledersaal
link: 'https://www.facebook.com/events/2134735319974387/'
image: 55837790_908639059306294_5877880490827972608_o.jpg
teaser: Der Klimawandel ist heute schon weltweit spürbar und könnte bei gegenwärtiger Entwicklung zukünftig katastrophale Ausmaße annehmen. Doch trifft diese
recurring: null
isCrawled: true
---
Der Klimawandel ist heute schon weltweit spürbar und könnte bei gegenwärtiger Entwicklung zukünftig katastrophale Ausmaße annehmen. Doch trifft diese Entwicklung nicht alle Menschen und Regionen gleichschnell und gleichstark. Die daraus resultierenden Ungerechtigkeiten, globalen Machtverhältnisse und Lösungsperspektiven unter dem Stichwort Klimagerechtigkeit wollen wir im Workshop genauer beleuchten.