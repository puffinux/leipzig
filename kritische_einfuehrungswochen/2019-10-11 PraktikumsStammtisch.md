---
id: '2585443458165222'
title: PraktikumsStammtisch
start: '2019-10-11 19:00'
end: '2019-10-11 22:00'
locationName: null
address: S127 / Campus Augustusplatz / Uni Leipzig
link: 'https://www.facebook.com/events/2585443458165222/'
image: 70645409_1025459944290871_1239130702352482304_o.jpg
teaser: 'Fr. 11.10.19 / 19.00  PraktikumsStammtisch   Kritische Lehrer*inne / S127  In unseren Schulpraktika sind wir immer wieder an unsere Grenzen gestoßen:'
recurring: null
isCrawled: true
---
Fr. 11.10.19 / 19.00

PraktikumsStammtisch 

Kritische Lehrer*inne / S127

In unseren Schulpraktika sind wir immer wieder an unsere Grenzen gestoßen: Konflikte mit Mentor*innen und Lehrer*innen, problematische Unterrichtsinhalte und -situationen, unzumutbarer Umgang mit Schüler*innen und mehr. Kennt ihr das auch? Damit diese Erfahrungen uns nicht davon abbringen, Lehrer*in werden zu wollen, finden wir es wichtig, darüber in Austausch zu treten und uns gegenseitig zu unterstützen. Dafür laden wir euch zu unserem PraktikumsStammtisch ein!