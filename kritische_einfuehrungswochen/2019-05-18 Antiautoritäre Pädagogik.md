---
id: '2013038455666765'
title: Antiautoritäre Pädagogik
start: '2019-05-18 12:00'
end: '2019-05-18 17:00'
locationName: null
address: Ladenlokal der Falken - Dimpfelstr. 33
link: 'https://www.facebook.com/events/2013038455666765/'
image: 55528723_907082956128571_1530296044405915648_o.jpg
teaser: 'Sa, 18.05., 12 Uhr:  Antiautoritäre Pädagogik  SJD – Die Falken Leipzig / Ladenlokal  Wir wollen etwas in die Geschichte der Erziehung schauen um hera'
recurring: null
isCrawled: true
---
Sa, 18.05., 12 Uhr:

Antiautoritäre Pädagogik

SJD – Die Falken Leipzig / Ladenlokal

Wir wollen etwas in die Geschichte der Erziehung schauen um herauszufinden, wie es vorher war und wie antiautoritäre Pädagogik später umgesetzt wurde. Dazu befassen wir uns mit Summerhill und der Kinderladenbewegung in Deutschland. Vielleicht gewinnen wir daraus auch Anreize über unser eigenes pädagogisches Handeln nachzudenken.
