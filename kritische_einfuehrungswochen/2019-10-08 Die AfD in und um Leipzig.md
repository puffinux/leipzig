---
id: '387739028806043'
title: Die AfD in und um Leipzig
start: '2019-10-08 17:00'
end: '2019-10-08 19:00'
locationName: null
address: HS19 / Campus Augustusplatz / Uni Leipzig
link: 'https://www.facebook.com/events/387739028806043/'
image: 70901101_1021536534683212_5791091923468943360_o.jpg
teaser: Di. 08.10.19 / 17.00   Die AfD in und um Leipzig  chronik.LE / HS 19  Im Mai fanden in Sachsen Kommunalwahlen statt. In Leipzig erreichte die AfD dabe
recurring: null
isCrawled: true
---
Di. 08.10.19 / 17.00 

Die AfD in und um Leipzig

chronik.LE / HS 19

Im Mai fanden in Sachsen Kommunalwahlen statt. In Leipzig erreichte die AfD dabei lediglich 14,9%, in den beiden benachbarten Landkreisen 19,4% (Nordsachsen) und 21,7% (Landkreis Leipzig). Im Fokus des Vortrages werden die (lokale) Entwicklung der AfD, ihre Themenwahl sowie ihre politischen Aktivitäten stehen. Auch das Ergebnis der Landtagswahl soll diskutiert werden. 