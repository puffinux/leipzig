---
id: '420141235422856'
title: Digitale Selbstverteidigung - Wie schütze ich mich im Internetz?
start: '2019-05-06 18:00'
end: '2019-05-06 21:00'
locationName: null
address: Campus Augustusplatz / S 202
link: 'https://www.facebook.com/events/420141235422856/'
image: 55932365_907065586130308_6046235649688731648_o.jpg
teaser: 'Mo, 06.05., 18 – 21 Uhr:  Digitale Selbstverteidigung - Wie schütze ich mich in diesem INTERNETZ?  AG Link / Campus Augustusplatz / S 202  Aktivist_i'
recurring: null
isCrawled: true
---

Mo, 06.05., 18 – 21 Uhr:

Digitale Selbstverteidigung - Wie schütze ich mich in diesem INTERNETZ?

AG Link / Campus Augustusplatz / S 202

Aktivist_innen und Journalist_innen erleben aktuell eine stärker werdende Repression, z.B. durch Polizeigesetze, auch auf digitaler Ebene. Dazu kommen die durch Edward Snowden bekannt gewordenen schier unbegrenzten Überwachungsprogramme von Geheimdiensten. In diesem Workshop sollen auch Unerfahrenen einige grundlegende Techniken vermittelt werden, um sich zumindest teilweise davor schützen. Das Mitbringen von Laptop und Smartphone wird empfohlen.