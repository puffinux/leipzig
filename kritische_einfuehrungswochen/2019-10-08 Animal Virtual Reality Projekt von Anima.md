---
id: '2428609627428225'
title: Animal Virtual Reality Projekt von "Animal Equality"
start: '2019-10-08 11:00'
end: '2019-10-08 17:00'
locationName: null
address: Neues Augusteum / Campus Augustusplatz / Uni Leipzig
link: 'https://www.facebook.com/events/2428609627428225/'
image: 70996977_1021530828017116_1614206395266105344_o.jpg
teaser: Di. 08.10.19 / 11.00-17.00  Animal- Virtual-Reality-Projekt von "Animal Equality"  V-Change (vegan-vegetarische Hochschulgruppe) / Neues Augusteum
recurring: null
isCrawled: true
---
Di. 08.10.19 / 11.00-17.00

Animal- Virtual-Reality-Projekt von "Animal Equality"

V-Change (vegan-vegetarische Hochschulgruppe) / Neues Augusteum 