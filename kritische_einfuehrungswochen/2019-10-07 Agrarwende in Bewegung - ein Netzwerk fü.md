---
id: '391394101550774'
title: Agrarwende in Bewegung - ein Netzwerk für Ernährungssouveränität
start: '2019-10-07 15:00'
end: '2019-10-07 17:00'
locationName: null
address: 'Campus Augustusplatz, S105'
link: 'https://www.facebook.com/events/391394101550774/'
image: 69976320_1017321045104761_3154907320069128192_o.jpg
teaser: Mo. 07.10.19 / 15.00  Agrarwende in Bewegung - ein Netzwerk für Ernährungssouveränität in Leipzig?  Leipziger Gruppe für Ernährungssouveränität / S.10
recurring: null
isCrawled: true
---
Mo. 07.10.19 / 15.00

Agrarwende in Bewegung - ein Netzwerk für Ernährungssouveränität in Leipzig?

Leipziger Gruppe für Ernährungssouveränität / S.105


Für eine klimagerechte Welt braucht es eine Veränderung von Ernährungs- und Agrarpolitik. Deshalb wollen wir die Vernetzung, das Wissen und die Organisierung rund um das Thema „Ernährungssouveränität“ im Raum Leipzig verstetigen. Was können wir tun, z.B. anknüpfend an die Nyéléni-Bewegung und andere Bündnisse? Als ersten Schritt schlagen wir einen Lesekreis, inkl. Lektüre-Ideen, vor, der als Grundlage für eine Organisation dienen kann.