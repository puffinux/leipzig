---
id: '3025121220848067'
title: Der (un)sichtbare rechte Terror in Deutschland
start: '2019-10-11 15:00'
end: '2019-10-11 17:00'
locationName: null
address: S101 / Campus Augustusplatz/ Uni Leipzig
link: 'https://www.facebook.com/events/3025121220848067/'
image: 70537697_1025456170957915_2921987352366678016_o.jpg
teaser: Fr. 11.10.19 / 15.00  Der (un)sichtbare rechte Terror in Deutschland  "Rassismus tötet!" Leipzig / S101  Anders als es in großen Teilen der Öffentlich
recurring: null
isCrawled: true
---
Fr. 11.10.19 / 15.00

Der (un)sichtbare rechte Terror in Deutschland

"Rassismus tötet!" Leipzig / S101

Anders als es in großen Teilen der Öffentlichkeit wahrgenommen wird, hat rechter Terror in Deutschland eine jahrzehntelange Tradition, die nahtlos an den Nationalsozialismus anschließt. Statt gezielte Attentate eines neonazistischen Netzwerkes zu sehen, sucht die deutsche Öffentlichkeit deren Hintergründe akribisch bei den Betroffenen. Wir beschäftigen uns im Vortrag mit dem gesellschaftlichen Umgang und den Konsequenzen für die Betroffenen.