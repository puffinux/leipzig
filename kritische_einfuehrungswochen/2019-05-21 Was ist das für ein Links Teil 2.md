---
id: '350832495540125'
title: Was ist das für ein Links? Teil 2
start: '2019-05-21 19:00'
end: '2019-05-21 21:00'
locationName: null
address: Campus Augustusplatz / S 017
link: 'https://www.facebook.com/events/350832495540125/'
image: 54730083_907084779461722_4749228583690436608_o.jpg
teaser: 'Di, 21.05., 19 – 21 Uhr:  Was ist das für ein Links? Woran die real-existierende Linke versagt und wie Wege zur ''befreiten Gesellschaft'' gefunden werd'
recurring: null
isCrawled: true
---
Di, 21.05., 19 – 21 Uhr:

Was ist das für ein Links? Woran die real-existierende Linke versagt und wie Wege zur 'befreiten Gesellschaft' gefunden werden könnten, Teil 2

Alfred Brosen / Campus Augustusplatz / S 017

Im Vortrag wird dafür plädiert, Fragen nach einer kommunistischen Produktion neu zu stellen und die Ökonomie wieder als Herzstück linker Auseinandersetzung zu begreifen. Dies funktioniert freilich nur auf der Basis von Aufklärung und Universalismus sowie in Abgrenzung zum umfassend gescheiterten autoritären Staatssozialismus.
