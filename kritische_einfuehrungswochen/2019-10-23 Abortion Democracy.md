---
id: "2416874391968894"
title: Abortion Democracy
start: 2019-10-23 17:00
end: 2019-10-23 19:00
address: HS16 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/2416874391968894/
image: 69446511_1032193280284204_2072763586555412480_o.jpg
teaser: 23.10.19 / 17.00  "Abortion Democracy"  Referat ausländischer Studierende /
  HS16  Warum ist der Zugang zu einer illegalen Abtreibung in Polen leichter
isCrawled: true
---
23.10.19 / 17.00

"Abortion Democracy"

Referat ausländischer Studierende / HS16

Warum ist der Zugang zu einer illegalen Abtreibung in Polen leichter als zu einer legalen in Südafrika? Der Film vergleicht die politischen, legislativen und gesellschaftlichen Entwicklungen bezüglich des Schwangerschafts-abbruchs in den Ländern Polen und Südafrika. Anhand dieser beiden Entwicklungen wird deutlich, wie bestimmte Demokratisierungsprozesse die Wahrnehmung von Frauen*rechten bezüglich ihrer Selbstbestimmung und körperlicher Integrität beeinflussen.