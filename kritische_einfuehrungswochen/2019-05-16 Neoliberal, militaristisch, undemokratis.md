---
id: '416049472291880'
title: 'Neoliberal, militaristisch, undemokratisch:Wie weiter mit der EU'
start: '2019-05-16 16:00'
end: '2019-05-16 19:00'
locationName: null
address: Campus Augustusplatz / S 015
link: 'https://www.facebook.com/events/416049472291880/'
image: 54798816_907080056128861_6885259800179900416_o.jpg
teaser: 'Do, 16.05., 16 – 19 Uhr:  Neoliberal, militaristisch, undemokratisch: Wie weiter mit der EU?  SDS Leipzig / Campus Augustusplatz / S 015  Einstiegsvor'
recurring: null
isCrawled: true
---
Do, 16.05., 16 – 19 Uhr:

Neoliberal, militaristisch, undemokratisch: Wie weiter mit der EU?

SDS Leipzig / Campus Augustusplatz / S 015

Einstiegsvortrag & Workshop: In Südeuropa herrschen teils nach wie vor humanitäre Katastrophen, verursacht durch eine Sparpolitik, die nur den Interessen von Banken und Konzernen dient. Mit PESCO schreitet der Wahnsinn einer europäischen Interventionsarmee voran. Jährlich sterben tausende Menschen auf der Flucht an den hochgerüsteten EU-Außengrenzen. In Workshops wollen wir linke Alternativen zur Europäischen Union diskutieren.
