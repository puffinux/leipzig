---
id: "792055227880627"
title: Offenes Plenum der GEW und DGB Hochschulgruppen
start: 2019-10-15 18:30
end: 2019-10-15 21:30
address: S017 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/792055227880627/
image: 70655699_1027073860796146_6288765125679644672_o.jpg
teaser: Di. 15.10.19 / 18.30  Offenes Plenum der GEW und DGB Hochschulgruppen  DGB und
  GEW Hochschulgruppe / S017  Die Hochschulgruppen der Gewerkschaft für E
isCrawled: true
---
Di. 15.10.19 / 18.30

Offenes Plenum der GEW und DGB Hochschulgruppen

DGB und GEW Hochschulgruppe / S017

Die Hochschulgruppen der Gewerkschaft für Erziehung und Wissenschaft und des Deutschen Gewerkschaftsbundes laden euch ein! Hier bekommt ihr einen Einblick, mit welchen Aktionen und Inhalten wir uns für Studierende und Arbeitnehmer*innen innerhalb und außerhalb der Uni Leipzig einsetzen.