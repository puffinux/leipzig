---
id: "2710580912285257"
title: Die EU als autoritärer Wettbewerbsetatismus
start: 2019-10-16 13:00
end: 2019-10-16 15:00
address: S205 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/2710580912285257/
image: 70886442_1027082440795288_2192252428588417024_o.jpg
teaser: Mi. 16.10.19 / 13.00  Die EU als autoritärer Wettbewerbsetatismus - am
  Beispiel der Migrationspolitik  InEUmanity / S205  Die EU ist in der Krise.
  Wie
isCrawled: true
---
Mi. 16.10.19 / 13.00

Die EU als autoritärer Wettbewerbsetatismus - am Beispiel der Migrationspolitik

InEUmanity / S205

Die EU ist in der Krise. Wie genau sich diese Krise beschreiben lässt und welche politischen Entwicklungen zu der jetzigen Situation geführt haben, wollen wir gemeinsam mit euch erarbeiten. Dafür greifen wir zurück auf Gramscis Hegemonietheorie und den Begriff des Wettbewerbsetatismus, den Lukas Oberndorfer entwickelt hat. Anschließend schauen wir uns an, wie sich die theoretischen Erkenntnisse beispielhaft auf die europäische Migrationspolitik anwenden lassen und diskutieren, was das für widerständige Perspektiven auf die europäische Union bedeuten kann.