---
id: '245999516346591'
title: Offenes Miettreff-Plenum
start: '2019-06-05 18:00'
end: '2019-06-05 20:00'
locationName: null
address: 'Eisenbahnstraße 125, 04315 Leipzig'
link: 'https://www.facebook.com/events/245999516346591/'
image: 52161617_2402039120058657_2402695775868944384_n.jpg
teaser: Wir sind eine Plattform zum Austausch und zur gegenseitigen Unterstützung unter Mieter*innen. In unserem Plenum organisieren wir den monatlich stattfi
recurring: null
isCrawled: true
---
Wir sind eine Plattform zum Austausch und zur gegenseitigen Unterstützung unter Mieter*innen. In unserem Plenum organisieren wir den monatlich stattfindenden Miettreff und diskutieren, was wir gegen steigende Mieten und Verdrängung tun können.

Komm rum, wenn du Lust hast aktiv zu werden für unser Recht auf Stadt!