---
id: '1919394414839412'
title: Was ist das für ein Links? Teil 1
start: '2019-05-13 19:00'
end: '2019-05-13 21:00'
locationName: null
address: Campus Augustusplatz / S 015
link: 'https://www.facebook.com/events/1919394414839412/'
image: 55549736_907078036129063_8476482120343093248_o.jpg
teaser: 'Mo, 13.05., 19 Uhr:  Was ist das für ein Links? Woran die real-existierende Linke versagt und wie Wege zur ''befreiten Gesellschaft'' gefunden werden kö'
recurring: null
isCrawled: true
---
Mo, 13.05., 19 Uhr:

Was ist das für ein Links? Woran die real-existierende Linke versagt und wie Wege zur 'befreiten Gesellschaft' gefunden werden könnten Teil 1

Jean-Luc Biedermeyer-Plath / Campus Augustusplatz / S 015

Linke Bemühungen gibt es viele.  Im parlamentarischen Raum den grünen Staatsfetischismus, den sozialdemokratischen Opportunismus und eine Linkspartei, die nicht so genau weiß, wo sie als Ganze hinwill. Außerparlamentarisch treibt sich unter verschiedenem Namen zwischen radikal-ideologischer Revolutionsromantik und bewegungslinken Parkbesetzern ziemlich viel rum. Jean-Luc Biedermeyer-Plaths kurzer Input soll auf eines der Herzstücke linker Praxis, nämlich das Formulieren und Fordern von Selbstkritik, fokussiert sein. Die Diskussion wird auf eine auf fünfzig Jahre begrenzte Verfahrensutopie gelenkt.
