---
id: "2708946722462706"
title: Rosa Luxemburg - Einführung in Leben und Werk
start: 2019-10-25 13:00
end: 2019-10-25 17:00
address: S 127 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/2708946722462706/
image: 72698630_1049673475202851_773267818937319424_n.jpg
teaser: Fr. 25.10.19 / Campus Augustusplatz / S 127  Rosa Luxemburg - Einführung in
  Leben und Werk - Workshop mit Miriam Pieschke  Kritische Einführungswochen
isCrawled: true
---
Fr. 25.10.19 / Campus Augustusplatz / S 127

Rosa Luxemburg - Einführung in Leben und Werk - Workshop mit Miriam Pieschke

Kritische Einführungswochen / 13.00-17.00

Rosa Luxemburg gilt vielen linken Menschen als nicht-diskreditierte Ikone der linken Geschichte und Bewegung. Sie steht für einen demokratischen Sozialismus, für ein Verhältnis von „Masse und Führung“ auf Augenhöhe, für eine Vision einer befreiten Gesellschaft. In diesem Workshop soll daher Gelegenheit sein, sich einen ersten Überblick über Luxemburgs Leben und Werk eingebettet in die historischen Ereignisse ihrer Zeit zu verschaffen. Zudem wollen wir an einem Beispieltext kennenlernen, wie Luxemburg argumentierte.

Die Veranstaltung wird durchgeführt mit Unterstützung der Rosa-Luxemburg-Stiftung Sachsen. Diese Steuermittel werden auf Grundlage von den Abgeordneten des Sächsischen Landtags beschlossenen Haushaltes zur Verfügung gestellt.