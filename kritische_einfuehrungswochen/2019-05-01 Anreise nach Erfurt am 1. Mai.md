---
id: '628734610923994'
title: Anreise nach Erfurt am 1. Mai
start: '2019-05-01 07:00'
end: '2019-05-01 10:00'
locationName: null
address: 'Hauptbahnhof Leipzig, Gleis 7'
link: 'https://www.facebook.com/events/628734610923994/'
image: 56949223_2098390566911968_914180851195969536_o.jpg
teaser: Am 1. Mai 2019 plant die AfD einen Aufmarsch mit mehreren tausend  Menschen in Erfurt. Diese Großdemonstration soll den Wahlkampfauftakt  für die drei
recurring: null
isCrawled: true
---
Am 1. Mai 2019 plant die AfD einen Aufmarsch mit mehreren tausend  Menschen in Erfurt. Diese Großdemonstration soll den Wahlkampfauftakt  für die drei ostdeutschen Landesverbände Brandenburg, Sachsen und  Thüringen darstellen. Dass diese drei Landesverbände, die dem  völkisch-nationalistischen „Flügel“ der AfD um Björn Höcke zugeordnet  werden, genau am 1. Mai – dem Arbeiter*innenkampftag – nach Erfurt  mobilisieren, ist kein Zufall: Die AfD greift die Frage nach sozialer Gerechtigkeit auf und bietet für gegenwärtige Krisen und Missstände völkische und nationalistische Lösungen an.

Wir wollen mit ganz vielen Menschen gemeinsam nach Erfurt fahren und die Demonstration von Alles muss man selber machen - feministisch, solidarisch, klimagerecht gegen den Wahlkampfauftakt der AfD unterstützen. 

Sagt euren Freund*innen, Nachbar*innen, Kolleg*innen und eurer Familie Bescheid! Lasst uns gemeinsam zeigen, dass soziale Gerechtigkeit niemals völkisch und nationalistisch sein kann! 

Unser Zug fährt um 7:19 auf Gleis 7, seid ein bisschen früher da, falls ihr euch noch Tickets holen wollt. Wir werden aus Erfurt auch gemeinsam wieder zurückreisen. Achtet aufeinander! 