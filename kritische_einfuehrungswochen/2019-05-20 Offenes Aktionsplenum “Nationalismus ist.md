---
id: '352216995636271'
title: Offenes Aktionsplenum “Nationalismus ist keine Alternative" LE
start: '2019-05-20 19:00'
end: '2019-05-20 21:00'
locationName: null
address: GWZ / S 420
link: 'https://www.facebook.com/events/352216995636271/'
image: 55483105_907083676128499_1226344232524972032_o.jpg
teaser: 'Mo, 20.05., 19 – 21 Uhr:  Offenes Aktionsplenum “Nationalismus ist keine Alternative – Leipzig”  Nationalismus ist keine Alternative – Leipzig / GWZ /'
recurring: null
isCrawled: true
---
Mo, 20.05., 19 – 21 Uhr:

Offenes Aktionsplenum “Nationalismus ist keine Alternative – Leipzig”

Nationalismus ist keine Alternative – Leipzig / GWZ / S 420

‘Nationalismus ist keine Alternative’ ist eine bundesweite Kampagne, die gegen Rechtsruck und explizit die sogenannte Alternative für Deutschland kämpft. Besonders in NRW, Bayern und Berlin wehrt sich die Mitmachkampagne kreativ gegen die autoritäre Formierung. Kommt zum offenen Treffen und lasst uns gemeinsam, auch mit Blick auf die Landtagswahl, Strategien und Aktionsideen entwickeln.
