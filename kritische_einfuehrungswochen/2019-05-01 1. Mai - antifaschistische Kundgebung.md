---
id: '291437438164172'
title: 1. Mai - antifaschistische Kundgebung
start: '2019-05-01 10:00'
end: null
locationName: Bahnhof Plauen  ob Bf
address: 'Rathenauplatz 2, 08525 Plauen'
link: 'https://www.facebook.com/events/291437438164172/'
image: 53013978_380392749408363_5172120770280685568_n.png
teaser: '1.Mai - Tag der Arbeiterbewegung – Position beziehen GEGEN NAZI-HETZE 10:00 Uhr! Oberer Bahnhof Plauen!! Bahnhofsvorplatz!!! Am 01.Mai 2019 will der n'
recurring: null
isCrawled: true
---
1.Mai - Tag der Arbeiterbewegung – Position beziehen GEGEN NAZI-HETZE
10:00 Uhr! Oberer Bahnhof Plauen!! Bahnhofsvorplatz!!!
Am 01.Mai 2019 will der neonazistische III.Weg in Plauen nach 2014 & 2016 eine 1. Mai Demo durchführen.

Der Tag der Arbeit wird in Plauen von den Neonazis für deren rassistische und antisemitische Propaganda als Vorwand genutzt. 
Auch im Hinblick auf die anstehenden Kommunalwahlen unerträglich und sollte jeden Menschen in Plauen, im Vogtland, in Sachsen und darüber hinaus dazu motivieren, den 01. Mai zu dem zu machen was diesen Tag ausmacht!

Wir rufen deshalb alle Antifaschist*innen dazu auf, an diesem Tag nicht still zu bleiben, sondern zahlreich nach Plauen zu kommen und gegen diese vermeintliche rechte Hegemonie der sich nun schon seit Jahren in unserer Gesellschaft breit macht zu widersprechen! Gerade hier in Plauen, wo sich seit 2017 deren Parteibüro befindet und es leider in großen Teilen der Bevölkerung Zustimmung für diese NS-Ideologie gibt.
AKTIV WERDEN!

Initiative Nie Wieder!