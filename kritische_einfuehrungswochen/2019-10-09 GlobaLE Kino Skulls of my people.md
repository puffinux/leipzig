---
id: '605874103236127'
title: 'GlobaLE Kino: Skulls of my people'
start: '2019-10-09 20:00'
end: '2019-10-09 22:30'
locationName: Ost-Passage Theater
address: 'Konradstr. 27, 04315 Leipzig'
link: 'https://www.facebook.com/events/605874103236127/'
image: 64239356_3092175617461058_2547905669504172032_o.jpg
teaser: 'Wann: Mi 09 Oktober 2019 20:00 - 22:30  Skulls of my people  Südafrika, Namibia / 2016 / 67 min / Vincent Moloi / original mit dt. UT / Anschließend D'
recurring: null
isCrawled: true
---
Wann: Mi 09 Oktober 2019 20:00 - 22:30

Skulls of my people

Südafrika, Namibia / 2016 / 67 min / Vincent Moloi / original mit dt. UT / Anschließend Diskussion mit dem Aktivisten Israel Kaunatjike. Eintritt frei.

Nach einer Besprechung der südafrikanischen Zeitung „Mail & Guardian“ von „Skulls of my people“ war es „eine harmlose Frage“, die den südafrikanischen Filmemacher Vincent Moloi auf einen Kampf um Landrechte und Reparationszahlungen im Nachbarland Namibia aufmerksam machte. Er sah ein Foto von einem traditionellen Herero-Kleid und fragte sich, wieso dieses an europäische Moden erinnerte. So stieß er auf die Geschichte des Genozids, bei dem Anfang des 20. Jahrhundert vier von fünf Herero und die Hälfte der Nama unter deutscher Kolonialherrschaft ermordet wurden.

Moloi musste feststellen, dass niemand von seinen Freunden jemals davon gehört hatte. Damit stand für ihn fest, dass „diese Geschichte erzählt werden muss“. Acht Jahre lang dokumentierte er die Bemühungen der Herero und Nama, die deutsche Regierung zur Anerkennung der historischen Verbrechen und zu Entschädigungszahlungen zu bewegen, und die namibische Regierung zur Rückgabe des damals gestohlenen Landes, das bis heute mehrheitlich im Besitz weißer Siedler ist.

Wo: Ost-Passage Theater, (Konradstraße 27) 