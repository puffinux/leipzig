---
id: '2472785309657866'
title: 'Klimakrise und Rechtsruck: Eine Einführung'
start: '2019-10-08 13:00'
end: '2019-10-08 15:00'
locationName: null
address: S127 / Campus Augustusplatz / Uni Leipzig
link: 'https://www.facebook.com/events/2472785309657866/'
image: 70740507_1021533668016832_7021867909408358400_o.jpg
teaser: 'Di. 08.10.19 / 14.00  Klimakrise und Rechtsruck: Eine Einführung  Prisma - IL Leipzig / S127  Trump steigt beim Klimaschutz aus, Bolsonaro fällt den R'
recurring: null
isCrawled: true
---
Di. 08.10.19 / 14.00

Klimakrise und Rechtsruck: Eine Einführung

Prisma - IL Leipzig / S127

Trump steigt beim Klimaschutz aus, Bolsonaro fällt den Regenwald, die AfD fördert fleißig Diesel und Kohle. Ob die Rechten jetzt den Klimawandel leugnen, für sich entdecken oder ein eigenes Verhältnis zur Umwelt haben: Darüber wollen wir mit euch im Workshop ins Gespräch kommen. Dabei fragen wir uns: Welche Perspektive gibt es in der Rechten auf Klimathemen und wie sind diese ideologisch verknüpft? Und welchen Bezug haben Antifa- und Klimabewegung eigentlich?