---
id: "394438381231051"
title: Vortrag von "Ärzte gegen Tierversuche"
start: 2019-10-25 19:00
end: 2019-10-25 21:00
address: HS5 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/394438381231051/
image: 72051207_1034017126768486_5795267851451367424_o.jpg
teaser: Fr. 25.10.19 / 19.00  Vortrag von "Ärzte gegen Tierversuche"  V-Change
  (vegan-vegetarische Hochschulgruppe) / HS5  „Medizinischer Fortschritt ist
  wich
isCrawled: true
---
Fr. 25.10.19 / 19.00

Vortrag von "Ärzte gegen Tierversuche"

V-Change (vegan-vegetarische Hochschulgruppe) / HS5

„Medizinischer Fortschritt ist wichtig - Tierversuche sind der falsche Weg!“ - Unter diesem Motto setzen sich die Ärzte gegen Tierversuche e. V. seit 1979 für eine tierversuchsfreie Medizin ein. Ziel ist die Abschaffung aller Tierversuche und damit eine ethisch vertretbare, am Menschen orientierte Medizin - eine Wissenschaft, die durch moderne, tierversuchsfreie Testmethoden zu relevanten Ergebnissen gelangt.