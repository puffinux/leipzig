---
id: '243130746574176'
title: Y?dentity – Popkultur & Identität (in Merseburg)
start: '2019-10-11 10:00'
end: '2019-10-11 17:00'
locationName: null
address: 'Merseburger Ständehaus, Oberaltenburg 2, 06217 Merseburg'
link: 'https://www.facebook.com/events/243130746574176/'
image: 71114399_1025454477624751_6397845389348700160_o.jpg
teaser: Fr. 11.10.19 / 10.00-17.00  Y?dentity – Popkultur & Identität   Studierende der Hochschule Merseburg (Fachbereich Soziale Arbeit.Medien.Kultur) im Rah
recurring: null
isCrawled: true
---
Fr. 11.10.19 / 10.00-17.00

Y?dentity – Popkultur & Identität 

Studierende der Hochschule Merseburg (Fachbereich Soziale Arbeit.Medien.Kultur) im Rahmen des Merseburger Kulturgesprächs / Merseburger Ständehaus, Oberaltenburg 2, 06217 Merseburg

Die Fachtagung beschäftigt sich mit den Themenkomplexen Heimat und Identität, digitale Jugendkultur und Gender. In den aktuellen Debatten um Migration, Europapolitik und Nationalismus eignet sich die Neue Rechte zunehmend die gängigen Codes der digitalen Jugendkultur an, produziert ideologisch aufgeladene Memes und artikuliert gewaltvollen Antifeminismus. Zu Gast sind u.a. Frank Apunkt Schneider, Veronika Kracher, Valerie-Siba Rousparast und Tobias Prüwer.