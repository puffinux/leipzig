---
id: "513003216152159"
title: Kapital in der Geschichte
start: 2019-10-24 19:00
end: 2019-10-24 21:00
address: S205 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/513003216152159/
image: 71189727_1032209163615949_1591596162706571264_o.jpg
teaser: Do. 24.10.19 / 19.00  Kapital in der Geschichte  Platypus Leipzig /
  S205  Durch den Eintritt in die bürgerliche Gesellschaft ist die Menschheit
  mit ei
isCrawled: true
---
Do. 24.10.19 / 19.00

Kapital in der Geschichte

Platypus Leipzig / S205

Durch den Eintritt in die bürgerliche Gesellschaft ist die Menschheit mit einer neuen Welt konfrontiert. Befreit von den Ketten der feudalen und kirchlich begründeten kosmologischen Ordnung, einhergehend mit dem neuen bürgerlichen Subjekt und einer Entfesselung der Produktivkräfte, in der industriellen Revolution, entsteht die Idee des Werdens und die Möglichkeit zur Veränderung. Kurz: das Potenzial zur Freiheit der gesamten Menschheit. 