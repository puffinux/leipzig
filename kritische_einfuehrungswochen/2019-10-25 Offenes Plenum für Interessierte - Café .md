---
id: "863323704064985"
title: Offenes Plenum für Interessierte - Café Connect
start: 2019-10-25 18:00
end: 2019-10-25 21:00
address: Bäckerei (Josephstraße 12)
link: https://www.facebook.com/events/863323704064985/
image: 71026736_1032223010281231_5323289531607154688_o.jpg
teaser: Fr. 25.10.19 / 18.00  Offenes Plenum für Interessierte   Café Connect /
  Bäckerei (Josephstraße 12)  Das Cafe Connect ist ein offenes Treffen für alle
isCrawled: true
---
Fr. 25.10.19 / 18.00

Offenes Plenum für Interessierte 

Café Connect / Bäckerei (Josephstraße 12)

Das Cafe Connect ist ein offenes Treffen für alle an Antifaschismus und linker Politik und Gesellschaftskritik Interessierte. Es finden Vorträge zu verschiedenen Themen statt, es gibt Raum für Austausch, Vernetzung und Planung gemeinsamer Aktionen. Wir verstehen uns nicht als Anhänger*innen einer bestimmten linksradikalen Strömung oder Ideologie, vielmehr sind wir an verschiedenen Perspektiven interessiert. Kommt vorbei, werdet aktiv!