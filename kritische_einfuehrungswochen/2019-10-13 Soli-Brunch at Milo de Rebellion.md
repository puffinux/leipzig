---
id: "725437767907128"
title: Soli-Brunch at Rebel Milo
start: 2019-10-20 09:00
end: 2019-10-20 15:00
address: Rebel Milo (Bornaische Straße 95)
link: https://www.facebook.com/events/725437767907128/
image: 72651094_1041649759338556_8026405827311042560_n.jpg
teaser: So. 20.10.19 / 09.00  Soli-Brunch   Kritische Einführungswochen / Milo de
  Rebellion (Bornaische Straße 95)   Soli-Brunch für selbstorganisierte antira
isCrawled: true
---
So. 20.10.19 / 09.00

Soli-Brunch 

Kritische Einführungswochen / Milo de Rebellion (Bornaische Straße 95)


Soli-Brunch für selbstorganisierte antirassistische Projekte in Leipzig.

