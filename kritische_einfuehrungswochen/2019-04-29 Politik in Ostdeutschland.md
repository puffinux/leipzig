---
id: '2532781163458581'
title: Politik in Ostdeutschland
start: '2019-04-29 15:00'
end: '2019-04-29 17:00'
locationName: null
address: Campus Augustusplatz / S 202
link: 'https://www.facebook.com/events/2532781163458581/'
image: 54727835_906560469514153_6995720333193904128_o.jpg
teaser: 30 Jahre '89 - welche Erfahrungen haben die Ostdeutschen gemacht? Worin besteht das ostdeutsche Spezifikum? Und was heißt das für linke Politik im Ost
recurring: null
isCrawled: true
---
30 Jahre '89 - welche Erfahrungen haben die Ostdeutschen gemacht? Worin besteht das ostdeutsche Spezifikum? Und was heißt das für linke Politik im Osten? Gemeinsam wollen wir darüber ins Gespräch kommen. Gemeinsam wollen wir uns mit der politischen Situation in Ostdeutschland befassen und dabei auch ganz bewusst einen Blick auf die Geschichte werfen. Den Schwerpunkt unserer Betrachtung werden hierbei die vergangenen drei Jahrzehnte einnehmen.