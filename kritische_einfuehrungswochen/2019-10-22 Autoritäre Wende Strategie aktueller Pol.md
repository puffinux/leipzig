---
id: "845768942486205"
title: Autoritäre Wende? Strategie aktueller Polizeiarbeit
start: 2019-10-22 17:00
end: 2019-10-22 19:00
address: HS16 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/845768942486205/
image: 71282406_1032138020289730_5207429026406203392_o.jpg
teaser: "Di. 22.10.19 / 17.00  Autoritäre Wende? Strategie aktueller
  Polizeiarbeit  Copwatch Leipzig / HS16   Ob Hamburg, Katalonien, Paris oder
  sonstwo: ständ"
isCrawled: true
---
Di. 22.10.19 / 17.00

Autoritäre Wende? Strategie aktueller Polizeiarbeit

Copwatch Leipzig / HS16 

Ob Hamburg, Katalonien, Paris oder sonstwo: ständig und immer häufiger wird radikaler Protest von hochgerüsteten Robocops angegriffen. Anschließend heißt es: „Polizeigewalt hat es nicht gegeben“. Ist das die autoritäre Wende? Oder machen die das nicht schon immer so? Wir beleuchten gegenwärtige Polizeistrategien und sprechen darüber, was das mit liberaler Demokratie, krisenhaftem Kapitalismus und allgemeinem Sicherheitsparadigma zu tun hat.