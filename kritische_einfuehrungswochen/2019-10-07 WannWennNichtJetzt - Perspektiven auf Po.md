---
id: '512683365943641'
title: WannWennNichtJetzt - Perspektiven auf Politik in Ostdeutschland
start: '2019-10-07 19:00'
end: '2019-10-07 21:00'
locationName: null
address: HS 2 / Campus Augustusplatz / Uni Leipzig
link: 'https://www.facebook.com/events/512683365943641/'
image: 69744072_1021522264684639_5197626888834514944_o.jpg
teaser: 'Mo. 07.10.19 / 19.00   #WannWennNichtJetzt - Perspektiven auf Politik in Ostdeutschland  #WannWennNichtJetzt / tba   Auf 14 Marktplätzen Ostdeutschlan'
recurring: null
isCrawled: true
---
Mo. 07.10.19 / 19.00 

#WannWennNichtJetzt - Perspektiven auf Politik in Ostdeutschland

#WannWennNichtJetzt / tba 

Auf 14 Marktplätzen Ostdeutschlands findet die #WannWennNichtJetzt Tour statt. Das Ziel? Rechte Hegemonien durchbrechen und Vernetzung linker Akteur*innen aus Land und Stadt fördern. Nun soll eine Zwischenbilanz gezogen werden: Welche Erfahrungen haben die Aktiven im Rahmen von #wwnj gemacht? Was bedeutet es, Politik im ländlichen Raum zu machen? Welche Perspektiven ergeben sich daraus für ein Ostdeutschland nach den Landtagswahlen?