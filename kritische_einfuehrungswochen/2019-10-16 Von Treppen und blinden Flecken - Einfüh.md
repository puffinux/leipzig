---
id: "2167233983569737"
title: Von Treppen und blinden Flecken - Einführungsworkshop zu Ableism
start: 2019-10-16 15:00
end: 2019-10-16 18:00
address: S015 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/2167233983569737/
image: 71145448_1027081267462072_7168870934709796864_o.jpg
teaser: Mi.16.10.19 / 15.00  Von Treppen und blinden Flecken - Einführungs-workshop zu
  Ableism  Referat für Inklusion - StuRa Universität Leipzig / S015  Der
isCrawled: true
---
Mi.16.10.19 / 15.00

Von Treppen und blinden Flecken - Einführungs-workshop zu Ableism

Referat für Inklusion - StuRa Universität Leipzig / S015

Der Alltag von behinderten und chronischkranken Menschen steckt voller Ausschlüsse und Barrieren. Diese Diskriminierung wird auch mit dem Begriff „Ableism“ bezeichnet. In dem Workshop wollen wir Form und Funktion von Ableism genauer betrachten. Gemeinsam werden wir ein Verständnis von Ableism erarbeiten und dessen verschiedene Varianten kennenlernen. Der Workshop richtet sich an Menschen ohne Vorerfahrungen und ist für Menschen ohne eigene Ableism-Erfahrung konzipiert.