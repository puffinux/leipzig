---
id: '401406747183689'
title: Johannes Agnoli und die ApO - 1968 und die Folgen
start: '2019-10-07 17:00'
end: '2019-10-07 19:00'
locationName: null
address: 'HS 5, Campus Augustusplatz, Uni Leipzig'
link: 'https://www.facebook.com/events/401406747183689/'
image: 69982638_1017314835105382_7118003133057335296_o.jpg
teaser: 'Mo. 07.10.19 / 17.00  Johannes Agnoli und die ApO - 1968 und die Folgen  Ali ma. / tba  1968 entstanden Protest- und Organisationsformen, die zur Ents'
recurring: null
isCrawled: true
---
Mo. 07.10.19 / 17.00

Johannes Agnoli und die ApO - 1968 und die Folgen

Ali ma. / tba

1968 entstanden Protest- und Organisationsformen, die zur Entstehung der Neuen Sozialen Bewegungen beitrugen und für die Linke bis heute prägend sind. Johannes Agnoli war maßgeblich an der Außerparlamentarischen Opposition beteiligt. In seinem Buch 1968 und die Folgen wirft er einen kritischen Blick auf die Revolte. Der Vortrag soll einen ersten Einstieg in Agnolis Theorien bieten und zu einer kritischen Diskussion über die 68er Ereignisse beitragen.