---
id: '1147496325430394'
title: KEW Das Internet und seine Mythen - Kneipengespräch
start: '2019-04-24 20:00'
end: '2019-04-24 21:30'
locationName: null
address: Kopfsalat Gästeservice
link: 'https://www.facebook.com/events/1147496325430394/'
image: 55506838_2015137388612355_8190518232793219072_n.jpg
teaser: 'Du überlegst Deine Kamera abzukleben, weißt aber nicht warum? Hört Google bei jedem Deiner Gespräche mit? Warum nutzen eigentlich alle Telegram und wa'
recurring: null
isCrawled: true
---
Du überlegst Deine Kamera abzukleben, weißt aber nicht warum? Hört Google bei jedem Deiner Gespräche mit? Warum nutzen eigentlich alle Telegram und was ist Signal? Wie kannst Du dich vor Cyberstalking schützen? Und überhaupt, was sind eigentlich diese Daten? 
Fast täglich begegnen uns diese oder ähnliche Fragen und meistens stehen wir ohne Antwort da. Dabei betreffen diese Themen doch wirklich Jede*n. Gemeinsam wollen wir die Hürde, uns zu informieren, überwinden! 
Dafür möchten wir uns in einem entspannten Kontext über diese und andere Themen austauschen. 

Es wird keine Expert*innenrunde! Wir sind Interessierte, die sich im voraus mit einigen Themen beschäftigen und dann in einem offenen Format Ergebnisse vorstellen und neue Fragen mitnehmen möchten.  
Dir fällt direkt noch eine Frage ein oder du hast Lust uns bei der Recherche zu unterstützen? Dann schreibt uns gerne eine Email (juniwein@mailbox.org) oder "Nomo" hier bei Facebook !