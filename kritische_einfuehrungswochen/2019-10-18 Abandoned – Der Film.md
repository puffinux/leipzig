---
id: "928819597473418"
title: Abandoned – Der Film
start: 2019-10-18 19:00
end: 2019-10-18 22:00
address: HS4 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/928819597473418/
image: 70942223_1030210560482476_6693800156808085504_o.jpg
teaser: Fr. 18.10.19 / 19.00  ABANDONED – Der Film  fem*med / HS4  Vorführung des
  Dokumentarfilms "Abandoned" von Patricia Josefine Marchart mit anschließende
isCrawled: true
---
Fr. 18.10.19 / 19.00

ABANDONED – Der Film

fem*med / HS4

Vorführung des Dokumentarfilms "Abandoned" von Patricia Josefine Marchart mit anschließender Diskussionsrunde. Der Film zeigt die Geschichte von Frauen in Europa, denen ÄrztInnen einen legalen und medizinisch notwendigen Schwangerschaftsbruch verweigerten.  