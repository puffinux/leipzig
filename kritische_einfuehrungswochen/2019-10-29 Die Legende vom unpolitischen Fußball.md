---
id: "674078969778167"
title: Die Legende vom unpolitischen Fußball
start: 2019-10-29 19:00
end: 2019-10-29 21:00
address: HS8 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/674078969778167/
image: 72235203_2517560795003816_3351264398684454912_n.jpg
teaser: Di. 29.10.19 / 19.00  Die Legende vom unpolitischen Fußball  SDS Leipzig /
  HS8  // "Bier, Bratwurst und einfach mal meine Ruhe..." // "Klar gibt es da
isCrawled: true
---
Di. 29.10.19 / 19.00

Die Legende vom unpolitischen Fußball

SDS Leipzig / HS8

// "Bier, Bratwurst und einfach mal meine Ruhe..."
// "Klar gibt es da auch ein paar Nazis in der Kurve, aber sollen wir am Eingang Gesinnungskontrolle machen?"
// "Jetzt hört doch mal auf mit eurem Politik-Rotz, das ist immer noch Fußball!" 

Alles tausendmal gehört und tausendmal für falsch befunden. Aber was hat Fußball nun mit Männlichkeit, Feminismus, Rassismus und Nazis zu tun? Und inwiefern kann er ein Raum für gesellschaftliche Veränderung sein?

Es diskutieren:

• Katharina Dahme
(Aufsichtsrat SV Babelsberg 03, Verlagskoordination 11 FREUNDE)

• Friederike Möller Bhering
(DFC Kreuzberg, DISCOVER FOOTBALL)