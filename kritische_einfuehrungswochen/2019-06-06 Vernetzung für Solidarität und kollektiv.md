---
id: '2191835691081034'
title: Vernetzung für Solidarität und kollektive Selbstermächtigung
start: '2019-06-06 12:00'
end: '2019-06-06 16:00'
locationName: null
address: Campus Augustusplatz / S 015
link: 'https://www.facebook.com/events/2191835691081034/'
image: 55624235_907505516086315_4347502408251736064_o.jpg
teaser: 'Do, 06.06., 12 – 16 Uhr:  Vernetzung für Solidarität und kollektive Selbstermächtigung  Organize! / Campus Augustusplatz / S 015  Vernetzung und Selbs'
recurring: null
isCrawled: true
---
Do, 06.06., 12 – 16 Uhr:

Vernetzung für Solidarität und kollektive Selbstermächtigung

Organize! / Campus Augustusplatz / S 015

Vernetzung und Selbstverwaltung mit emanzipatorischem Anspruch im Leipziger Osten sind Themen, mit denen wir uns auseinandersetzen. Wie gehören sie zusammen? Wie sehen konkrete politische Handlungsansätze aus? Und warum sind sie (gerade) für (den) Leipzig(er Osten) so wichtig? Das wollen wir mit euch diskutieren.
