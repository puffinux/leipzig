---
id: "2695229850521311"
title: Rechte Gewalt in Leipzig
start: 2019-10-16 11:00
end: 2019-10-16 13:00
address: "Treffpunkt: Couchcafé am Campus Augustusplatz"
link: https://www.facebook.com/events/2695229850521311/
image: 71396623_1027084320795100_4482068883301203968_o.jpg
teaser: "Mi. 16.10.19 / 11.00  Treffpunkt: Couchcafé am Campus   Initiativkreis
  Antirassismus / Stadtrundgang / Treffpunkt: Couchcafé am Campus Augustusplatz"
isCrawled: true
---
Mi. 16.10.19 / 11.00

Treffpunkt: Couchcafé am Campus 

Initiativkreis Antirassismus / Stadtrundgang / Treffpunkt: Couchcafé am Campus Augustusplatz

In Leipzig wurden mindestens acht Menschen von Rechten umgebracht, hinzu kommen zwei Verdachtsfälle. Die meisten Morde ereigneten sich im "öffentlichen Raum". Wir möchten mit euch einige dieser Orte besuchen und auf die Alltäglichkeit rechter Gewalt eingehen. Der Stadtrundgang bezieht sich auf den Vortrag "Todesopfer rechter Gewalt in Leipzig seit 1990".