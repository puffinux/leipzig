---
id: '419998148774026'
title: Plenum Kritische Lehrer*innen
start: '2019-05-01 19:00'
end: '2019-05-01 22:00'
locationName: null
address: to be announced
link: 'https://www.facebook.com/events/419998148774026/'
image: 55514086_907511729419027_1941347946758930432_o.jpg
teaser: 'Jeden Mittwoch, 19 Uhr:  Plenum Kritische Lehrer*innen  Kritische Lehrer*innen / Raum H4 1.16 im GWZ  Die kritischen Lehrer*innen Leipzig existieren n'
recurring: null
isCrawled: true
---
Jeden Mittwoch, 19 Uhr:

Plenum Kritische Lehrer*innen

Kritische Lehrer*innen / Raum H4 1.16 im GWZ

Die kritischen Lehrer*innen Leipzig existieren nun seit bereits mehr als vier Jahren als Initiative, bei der sich Studierende des Lehramts aller Fächer und Richtungen zusammensetzen und alles, was im Spannungsfeld Schule – Lernen – Lehramtsstudium liegt, kritisch diskutieren, reflektieren und konstruktiv angehen. Wir treffen uns immer Mittwoch, ab 19.00 Uhr. 

Bei Fragen könnt ihr uns gerne hier bei facebook anschreiben oder eine Mail an krile-leipzig@inventati.org schicken.
