---
id: "390241441880248"
title: Was ist Klassenbewusstsein?
start: 2019-10-30 15:00
end: 2019-10-30 17:00
address: HS 16 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/390241441880248/
image: 73420359_711530902655041_6506283473802625024_n.jpg
teaser: Mi. 30.10.19 / 15.00  Was ist Klassenbewusstsein?  Platypus Leipzig /
  HS16  "Es handelt sich nicht darum, was dieser oder jener Proletarier oder
  selbs
isCrawled: true
---
Mi. 30.10.19 / 15.00

Was ist Klassenbewusstsein?

Platypus Leipzig / HS16

"Es handelt sich nicht darum, was dieser oder jener Proletarier oder selbst das ganze Proletariat, als Ziel sich einstweilen vorstellt. Es handelt sich darum, was es ist, und was diesem Sein gemäß geschichtlich zu tun gezwungen sein wird“ (Marx: Die heilige Familie). Wir wollen uns dem zentralen Problem des Klassenbewusstseins innerhalb der marxistischen Theorie nähern. Was hat es historisch bedeutet und welche Relevanz ergibt sich für heute?