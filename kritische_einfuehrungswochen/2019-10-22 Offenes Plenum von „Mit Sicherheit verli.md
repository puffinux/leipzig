---
id: "562634287811236"
title: Offenes Plenum von „Mit Sicherheit verliebt“
start: 2019-10-22 19:00
end: 2019-10-22 22:00
address: S203 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/562634287811236/
image: 72208173_1032184886951710_2776512351875104768_o.jpg
teaser: Di. 22.10.19 / 19.00  Offenes Plenum von „Mit Sicherheit verliebt“   Mit
  Sicherheit Verliebt (MSV) / S203  Wir sind eine Gruppe von Studierenden, die
isCrawled: true
---
Di. 22.10.19 / 19.00

Offenes Plenum von „Mit Sicherheit verliebt“ 

Mit Sicherheit Verliebt (MSV) / S203

Wir sind eine Gruppe von Studierenden, die durch Schulbesuche Schüler*innen bei der Entwicklung einer selbstbestimmten und reflektierten Sexualität unterstützen. Zudem wollen wir auch zur Sensibilisierung in unserem Umfeld beitragen, indem wir Vortragsreihen und Workshops im Themenfeld Sexualität(en) organisieren und uns selbst weiterbilden. Wir möchten euch in dem Plenum unsere Arbeit vorstellen und darüber ins Gespräch kommen und freuen uns auf neue Gesichter! 