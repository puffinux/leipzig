---
id: "2124774971160447"
title: Neonazitreff schließen! - ein würdiges Gedenken ermöglichen!
start: 2019-10-17 17:00
end: 2019-10-17 19:00
address: HS17 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/2124774971160447/
image: 71069841_1029115803925285_4705008493764018176_o.jpg
teaser: Do. 17.10.19 / 17.00   Neonazitreff schließen! - ein würdiges Gedenken
  ermöglichen!  Ladenschlussbündnis / HS17  Seit mehr als 10 Jahren befindet
  sich
isCrawled: true
---
Do. 17.10.19 / 17.00 

Neonazitreff schließen! - ein würdiges Gedenken ermöglichen!

Ladenschlussbündnis / HS17

Seit mehr als 10 Jahren befindet sich ein Gebäude des ehemaligen Frauen-Außenlagers des KZ Buchenwald im Besitz eines Neonazis. Seither fanden dort diverse Rechtsrockkonzerte und Elektroparties statt. Organisierte Neonazistrukturen haben dort ihre Räume. Ein würdiges Erinnern ist unmöglich. Über die Geschichte des Außenlagers in der Kamenzer Str. 12, deren aktuelle Entwürdigung und Handlungsperspektiven wollen wir Euch informieren und in Austausch treten. 