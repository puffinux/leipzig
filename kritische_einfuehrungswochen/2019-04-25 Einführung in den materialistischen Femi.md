---
id: '630841944003971'
title: Einführung in den materialistischen Feminismus
start: '2019-04-25 18:00'
end: '2019-04-25 21:00'
locationName: null
address: Campus Augustusplatz / S 015
link: 'https://www.facebook.com/events/630841944003971/'
image: 55759941_908639342639599_6773947364697702400_o.jpg
teaser: 'Mit einer materialistisch-feministischen Kritik der Produktionsverhältnisse wollen wir die Zusammenhänge von Patriarchat und Ökonomie, sowie die Wider'
recurring: null
isCrawled: true
---
Mit einer materialistisch-feministischen Kritik der Produktionsverhältnisse wollen wir die Zusammenhänge von Patriarchat und Ökonomie, sowie die Widersprüche zwischen Emanzipation und fortbestehenden Geschlechterrollen als Notwendigkeiten des Spätkapitalismus verstehen und kritisieren. Die Falken Leipzig laden Euch deshalb zu einem Workshop ein, in dem wir gemeinsam Begrifflichkeiten, Konzepte und Ideen kennenlernen und disku-tieren können. (Anmeldung unter marxfem@falken-sachsen.de)