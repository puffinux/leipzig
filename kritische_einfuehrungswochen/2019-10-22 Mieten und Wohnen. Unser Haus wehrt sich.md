---
id: "2379017645526812"
title: Mieten und Wohnen. Unser Haus wehrt sich!
start: 2019-10-22 19:00
end: 2019-10-22 21:00
address: HS16 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/2379017645526812/
image: 71864806_2852115204840575_7615548199213727744_n.jpg
teaser: Di. 22.10.19 / 19.00  Mieten und Wohnen. Unser Haus wehrt sich!  Leipzig für
  Alle!/ DGB-Hochschulgruppe/ GEW-Hochschulgruppe / HS16  Verklagt – Gestri
isCrawled: true
---
Di. 22.10.19 / 19.00

Mieten und Wohnen. Unser Haus wehrt sich!

Leipzig für Alle!/ DGB-Hochschulgruppe/ GEW-Hochschulgruppe / HS16

Verklagt – Gestritten – Gewonnen. Die Kündigung der eigenen Wohnung ist vielleicht das angenommen Gravierendste, das einer*m Mieter*in vorstellbar ist und das Angst machen kann. Doch es gibt rechtlich umfangreiche Möglichkeiten, sich zur Wehr zu setzen und sich somit kommunal in die Wohnungspolitik einzumischen. Stefan Lange berichtet über vielfältige Mittel und Strategien sowie langjährige Erfahrungen aus dem Rechtsstreit nach seiner Wohnungskündigung.