---
id: '646689142446760'
title: Handlungsfähigkeit durch Faschisierung?!
start: '2019-04-29 17:00'
end: '2019-04-29 19:00'
locationName: null
address: Campus Augustusplatz / HS 16
link: 'https://www.facebook.com/events/646689142446760/'
image: 55439714_906561389514061_5283747863656923136_o.jpg
teaser: 'In den Debatten um die politischen Erfolge des autoritären Populismus sind Erklärungsansätze allgegenwärtig, die dessen Anhänger_innen vorhalten, mani'
recurring: null
isCrawled: true
---
In den Debatten um die politischen Erfolge des autoritären Populismus sind Erklärungsansätze allgegenwärtig, die dessen Anhänger_innen vorhalten, manipuliert (worden) zu sein, verblendet zu sein, grundsätzlich autoritär gestrickt zu sein oder aber gar nicht anders zu können, als aus Notwehr rechtsradikal zu wählen.
Wir wollen demgegenüber einen kritisch-psychologischen Blick auf das Problem werfen, der davon ausgeht, dass Subjekte von ihrem Standpunkt aus »gute Gründe« für ihr Handeln haben.Mit der kritisch-psychologischen Kategorie der Handlungsfähigkeit wollen wir versuchen zu verstehen, warum der autoritäre Populismus in der aktuellen gesellschaftlichen Umbruchsituation für seine Anhänger_innen gegenüber anderen Politiken attraktiver erscheint – ohne diesen abzusprechen, von ihrem Standpunkt aus begründet zu handeln.
Dazu wollen wir uns durch Theorien und Überlegungen hinsichtlich der Produktion von »politischen Meinungen« arbeiten, wobei unser Fokus auf der Schnittstelle von Alltagsverstand und Ideologie liegt. Es geht uns darum, aus Subjektperspektive zu verstehen, warum »rechte« gegenüber »linken« Ansätzen attraktiver erscheinen und warum linksliberale Allianzen so wenig dagegen ausrichten können.

Daniel Schnur, studiert Psychologie an der Universität Klagenfurt/Celovec. Seit 2016 im Vorbereitungsteam der Ferienuni Kritische Psychologie. Schreibt aktuell seine Masterarbeit zu autoritärem Populismus. Gastherausgeber eines Heftes von Psychologie & Gesellschaftskritik, »kritische Psychotherapie«.
 
Till Manderbach, studiert Psychologie an der Universität Klagenfurt. Seit 2016 im Vorbereitungsteam der Ferienuni Kritische Psychologie. Im Rahmen seiner Masterarbeit forscht er zu Produktionsweisen politischer Meinung im Kontext des Rechtspopulismus.

Veranstaltet von be:gründet // Kritische Psychologie.