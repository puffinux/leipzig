---
id: '1010045239191695'
title: Nicht-/Behinderung in digitalen Spielen
start: '2019-05-24 19:00'
end: '2019-05-24 21:00'
locationName: null
address: Campus Augustusplatz / HS 16
link: 'https://www.facebook.com/events/1010045239191695/'
image: 54458058_907504262753107_6940424403079397376_o.jpg
teaser: 'Fr, 24.05., 19 – 21 Uhr:  Nicht-/Behinderung in digitalen Spielen – zwischen normalistischer Biopolitik und ableistischem Transhumanismus  AK Barriere'
recurring: null
isCrawled: true
---
Fr, 24.05., 19 – 21 Uhr:

Nicht-/Behinderung in digitalen Spielen – zwischen normalistischer Biopolitik und ableistischem Transhumanismus

AK Barrierefrei / Campus Augustusplatz / HS 16
Vortrag von Simon Ledder (Uni Köln)

Das Thema 'Behinderung' wird in der Öffentlichkeit diskutiert. Dies findet sich in Parteiprogrammen wie auch in digitalen Spielen. Aber nicht alle favorisieren den Abbau von Barrieren; manche wünschen sich den Umbau von Individuen. Anhand von Beispielen wie Mass Effect 2, in denen körperliche Utopien inszeniert werden, erläutert der Vortrag, wie 'Behinderung' und 'Normalität' in Spielen konstruiert und biotechnologische Veränderungen des Körpers beworben werden.

