---
id: '750546698714417'
title: Soli-Bar at Milo de Rebellion
start: '2019-10-11 20:00'
end: '2019-10-11 23:59'
locationName: null
address: Milo de Rebellion (Bornaische Straße 95)
link: 'https://www.facebook.com/events/750546698714417/'
image: 71185763_1025461767624022_5104340551352713216_o.jpg
teaser: Fr. 11.10.19 / 20.00  Soli-Bar   Kritische Einführungswochen / Milo de Rebellion (Bornaische Straße 95)  Soli-Bar-Abend für selbstorganisierte antiras
recurring: null
isCrawled: true
---
Fr. 11.10.19 / 20.00

Soli-Bar 

Kritische Einführungswochen / Milo de Rebellion (Bornaische Straße 95)

Soli-Bar-Abend für selbstorganisierte antirassistische Projekte in Leipzig.