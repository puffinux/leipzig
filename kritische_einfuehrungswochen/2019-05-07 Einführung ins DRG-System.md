---
id: '1258002297698680'
title: Einführung ins DRG-System
start: '2019-05-07 19:00'
end: '2019-05-07 21:00'
locationName: null
address: Campus Liebigstraße / Kleiner HS im Carl-Ludwig-Institut
link: 'https://www.facebook.com/events/1258002297698680/'
image: 55564489_907067096130157_4909017005077561344_o.jpg
teaser: 'Di, 07.05., 19 – 21 Uhr:  Einführung ins DRG-System  KritMed / Campus Liebigstraße / Kleiner HS im Carl-Ludwig-Institut  Das DRG System (diagnosis rel'
recurring: null
isCrawled: true
---
Di, 07.05., 19 – 21 Uhr:

Einführung ins DRG-System

KritMed / Campus Liebigstraße / Kleiner HS im Carl-Ludwig-Institut

Das DRG System (diagnosis related groups) bildet seit 2003 die verbindliche Grundlage der Krankenhausfinanzierung. Dabei ist es jedoch nur die Spitze einer seit Jahrzehnten zunehmenden Privatisierung und Öffnung des Gesundheitssektors für privates Kapital. Wir wollen dieses Finanzierungsmodell erklären und gemeinsam diskutieren, um Chancen und Grenzen dessen aufzeigen.