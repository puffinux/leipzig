---
id: "2575082545882272"
title: Unterricht trotz U-Haft?!
start: 2019-10-22 17:00
end: 2019-10-22 19:00
address: S203 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/2575082545882272/
image: 72314143_1032182586951940_7325448623230025728_o.jpg
teaser: Di. 22.10.19 / 17.00  Unterricht trotz U-Haft - lässt sich Aktivismus mit dem
  Lehrberuf vereinen?  Kritische Lehrer*innen Leipzig / S203   In einem of
isCrawled: true
---
Di. 22.10.19 / 17.00

Unterricht trotz U-Haft - lässt sich Aktivismus mit dem Lehrberuf vereinen?

Kritische Lehrer*innen Leipzig / S203 

In einem offenen Diskussionsformat wollen wir mit Interessierten und Betroffenen gemeinsam darüber sprechen, ob sich Aktivismus mit dem Lehrberuf vereinen lässt. Dabei wollen wir einen Blick auf die rechtlichen Grundlagen werfen, über die Einsichtsmöglichkeiten in Führungszeugnis und Bundeszentralregister sprechen und anhand einiger Präzedenzfälle mögliche Folgen für den Berufseinsteig ableiten.