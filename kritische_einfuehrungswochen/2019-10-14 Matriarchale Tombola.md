---
id: "2378426862433135"
title: Matriarchale Tombola - wird in Kneipenabend verwandelt, tba
start: 2019-10-14 22:00
end: 2019-10-15 01:00
address: tba
link: https://www.facebook.com/events/2378426862433135/
image: 71560071_1026272884209577_3014625265750900736_o.jpg
teaser: Mo. 14.10.19 / 15.00  Matriarchale Tombola - wird in Kneipenabend verwandelt -
  mehr Infos folgen  Burschenschaft Lascivia zu Leipzig  / Hörsaalgebäude
isCrawled: true
---
Mo. 14.10.19 / 15.00

Matriarchale Tombola - wird in Kneipenabend verwandelt - mehr Infos folgen

Burschenschaft Lascivia zu Leipzig  / Hörsaalgebäude im Erdgeschoss

Auf, auf zum goldenen Matriarchat! Die Burschenschaft Lascivia verlost glorreiche Preise für karikative Zwecke (Aufstockung des hauseigenen Biervorrats). In der ältesten Burschenschaft Sachsens finden sich Freunde fürs Leben – Treue, Ehre und Vaterlandsverrat zeichnen unsere starke Gemeinschaft aus. Eine einzigartige Chance die Burschen persönlich kennenzulernen.