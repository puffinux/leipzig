---
id: '2061334827440512'
title: Miettreff Leipziger Osten
start: '2019-05-15 18:00'
end: '2019-05-15 19:30'
locationName: null
address: Eisenbahnstr. 125
link: 'https://www.facebook.com/events/2061334827440512/'
image: 45611434_2330842970511606_1897726201611419648_n.jpg
teaser: 'Probleme mit Vermieter*innen, Verwaltung oder der Miete?  Wir sind eine Plattform zum Austausch und zur gegenseitigen Unterstützung unter Mieter*innen'
recurring: null
isCrawled: true
---
Probleme mit Vermieter*innen, Verwaltung oder der Miete?

Wir sind eine Plattform zum Austausch und zur gegenseitigen Unterstützung unter Mieter*innen.

Beim unserem Miettreffen treffen wir uns mit allen, die Lust haben sich gegenseitig bei diesen Problem zu unterstützen, um so gemeinsam etwas gegen steigende Mieten und Verdrängung zu tun.

Bitte bringt, wenn ihr ein Anliegen habt, alle eure Unterlagen zum Miettreff mit (also Mietvertrag, ggf. Nebenkostenabrechnung oder Mieterhöhung und all euren Briefe, die ihr von eurer*m Vermieter*in bekommen habt).
______________________________________________________
Do you have any problems with your landlord/landlady, administration or rent?

We are a platform for exchange and mutual assistance between tenants.

We meet with everyone who is interested in helping each other with these problems to do something against rising rents and displacement.

If you have a problem, please bring all your documents to the meeting (i.e. tenancy agreement, ancillary cost accounting or rent increase, and all letters you got from your landlord/landlady).