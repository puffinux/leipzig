---
id: '1099156503603139'
title: Psychotherapeut*innen in Ausbeutung – Demo
start: '2019-05-17 17:00'
end: '2019-05-17 20:00'
locationName: null
address: 'Startpunkt: Augustusplatz'
link: 'https://www.facebook.com/events/1099156503603139/'
image: 54798765_907082346128632_6106679184006840320_o.jpg
teaser: 'Fr, 17.05., 17 Uhr:  Psychotherapeut*innen in Ausbeutung – Demo  FSR Psychologie / Startpunkt: Augustusplatz  Wir wollen mit Euch gegen die ausbeuteri'
recurring: null
isCrawled: true
---
Fr, 17.05., 17 Uhr:

Psychotherapeut*innen in Ausbeutung – Demo

FSR Psychologie / Startpunkt: Augustusplatz

Wir wollen mit Euch gegen die ausbeuterischen Bedingungen der PiAs (Psychotherapeut*innen in Ausbildung) demonstrieren! Wir fordern bessere Arbeitsbedingungen in den Kliniken und angemessene Entlohnung unserer Arbeit: PiAs werden hier als günstiger Ersatz für approbierte Psychotherapeut*innen eingestellt. Diese Ausbeutung schlägt sich nicht nur in einem destruktiven Arbeitsklima nieder, sondern auch in einer mangelhaften Patient*innenversorgung im Gesundheitssystem!
