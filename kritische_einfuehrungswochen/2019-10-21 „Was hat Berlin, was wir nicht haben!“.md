---
id: "970572883280788"
title: „Was hat Berlin, was wir nicht haben?!“
start: 2019-10-21 13:00
end: 2019-10-21 15:00
address: S203 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/970572883280788/
image: 73038549_2852113274840768_1952716415953797120_n.jpg
teaser: Mo. 21.10.19 / 13.00  „Was hat Berlin, was wir nicht haben?! - Wie Studierende
  einen Tarifvertrag erkämpfen  DGB Hochschulgruppe / S203  Referentinnen
isCrawled: true
---
Mo. 21.10.19 / 13.00

„Was hat Berlin, was wir nicht haben?! - Wie Studierende einen Tarifvertrag erkämpfen

DGB Hochschulgruppe / S203

Referentinnen: Laura Haßler und Julia Bringmann, TVStud Kampagne Berlin

Bericht und Workshop zur TVStud-Kampagne, die zu den landesweit besten Bedingungen für studentische Beschäftigte an universitären Einrichtungen führte. Wodurch wurde in Berlin dieser Erfolg ermöglicht? Welche Potenziale für eine erfolgreiche TV-Stud-Kampagne gibt es in Leipzig? Teilnahme mit oder ohne gewerkschaftliche Erfahrungen empfohlen.