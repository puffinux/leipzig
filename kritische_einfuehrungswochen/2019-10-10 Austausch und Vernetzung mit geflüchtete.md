---
id: '2231243273652343'
title: Austausch und Vernetzung mit geflüchteten Pädagog*innen
start: '2019-10-10 17:00'
end: '2019-10-10 19:00'
locationName: null
address: S114 / Campus Augustusplatz / Uni Leipzig
link: 'https://www.facebook.com/events/2231243273652343/'
image: 70439604_1025009131002619_2692872717098549248_o.jpg
teaser: 'Do. 10.10.19 / 17.00  Schulsysteme International: Austausch und Vernetzung mit geflüchteten Pädagog*innen  Kritische Lehrer*innen / S114  Vor einiger'
recurring: null
isCrawled: true
---
Do. 10.10.19 / 17.00

Schulsysteme International: Austausch und Vernetzung mit geflüchteten Pädagog*innen

Kritische Lehrer*innen / S114

Vor einiger Zeit lernten wir bei den Kritischen Lehrer*innen einen Lehrer aus Syrien kennen. Er ist einer der wenigen, der hier in Sachsen als Lehrer arbeiten darf. In seinem Netzwerk gibt es viele weitere geflüchtete Pädagog*innen, die hoffen, ihre Berufung hier in Deutschland ausführen zu können. Wir laden euch ein, miteinander ins Gespräch zu kommen, Spannendes über Bildungssysteme anderer Länder zu erfahren und vieles mehr! 