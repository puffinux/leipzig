---
id: '268901427385621'
title: Offenes Aktionsplenum Nationalismus ist keine Alternative
start: '2019-04-23 19:00'
end: '2019-04-23 21:00'
locationName: null
address: S 202
link: 'https://www.facebook.com/events/268901427385621/'
image: 55576838_908639145972952_1784151310501675008_o.jpg
teaser: '‘Nationalismus ist keine Alternative’ ist eine bundesweite Kampagne, die gegen Rechtsruck und explizit die sogenannte Alternative für Deutschland kämp'
recurring: null
isCrawled: true
---
‘Nationalismus ist keine Alternative’ ist eine bundesweite Kampagne, die gegen Rechtsruck und explizit die sogenannte Alternative für Deutschland kämpft. Besonders in NRW, Bayern und Berlin wehrt sich die Mitmachkampagne kreativ gegen die autoritäre Formierung. Kommt zum offenen Treffen und lasst uns gemeinsam, auch mit Blick auf die Landtagswahl, Strategien und Aktionsideen entwickeln.