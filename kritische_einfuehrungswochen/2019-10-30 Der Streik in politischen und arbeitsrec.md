---
id: "3088130987905592"
title: Der Streik in politischen und arbeitsrechtlichen Kollisionen
start: 2019-10-29 17:00
end: 2019-10-29 19:00
address: S203 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/3088130987905592/
image: 73372271_2544212628981953_8352262865754783744_n.jpg
teaser: Di. 29.10.19 / 17.00  Der Streik in politischen und arbeitsrechtlichen
  Kollisionen  GEW Hochschulgruppe / S203  Was ist Streik? Was sind historisch
  wi
isCrawled: true
---
Di. 29.10.19 / 17.00

Der Streik in politischen und arbeitsrechtlichen Kollisionen

GEW Hochschulgruppe / S203

Was ist Streik? Was sind historisch wichtige Beispiele und was waren die Folgen? Worin besteht der Unterschied zwischen politischem Streik und Tarifstreik? Was kennzeichnet die feministischen Streiks in Spanien und Deutschland?
Dies möchten wir nach kurzen Inputs gerne zusammen mit Dir diskutieren.