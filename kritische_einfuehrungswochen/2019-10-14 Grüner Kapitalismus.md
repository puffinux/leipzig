---
id: "520805425158709"
title: Grüner Kapitalismus
start: 2019-10-14 19:00
end: 2019-10-14 21:00
address: S202 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/520805425158709/
image: 71693512_1032213723615493_6399575565384286208_n.jpg
teaser: Mo. 14.10.19 / 19.00  Grüner Kapitalismus  SDS Leipzig / S202   Kann es einen
  grünen Kapitalismus geben oder stehen Profitlogik und Wachstumzwang gene
isCrawled: true
---
Mo. 14.10.19 / 19.00

Grüner Kapitalismus

SDS Leipzig / S202 

Kann es einen grünen Kapitalismus geben oder stehen Profitlogik und Wachstumzwang generell im Widerspruch zur Natur und ihren Ressourcen? Reichen einzelne Reformen aus, um die CO2-Emissionen im großen Maße zu begrenzen und die Klimakrise zu stoppen, oder brauchen wir ein grundlegendes systemisches Umdenken? In der Veranstaltung wollen wir diesen Fragen auf den Grund gehen und herausarbeiten, wie eine Wirtschaftsweise aussehen müsste, die den sozialen und ökologischen Bedürfnissen einer nachhaltige Gesellschaft gerecht wird.