---
id: '502036853953450'
title: Neues aus dem Fullfillment-Center - Die Arbeitskämpfe bei Amazon
start: '2019-10-11 11:00'
end: '2019-10-11 13:00'
locationName: null
address: HS19 / Campus Augustusplatz / Uni Leipzig
link: 'https://www.facebook.com/events/502036853953450/'
image: 71109666_1025209050982627_2125424889241272320_o.jpg
teaser: Fr. 11.10.19 / HS19  Neues aus dem Fullfillment-Center - Die Arbeitskämpfe bei Amazon  Streik Soli-Bündnis Leipzig und Amazon Vertrauensleute / HS19
recurring: null
isCrawled: true
---
Fr. 11.10.19 / HS19

Neues aus dem Fullfillment-Center - Die Arbeitskämpfe bei Amazon

Streik Soli-Bündnis Leipzig und Amazon Vertrauensleute / HS19

Die Arbeitskämpfe bei Amazon gehen ins siebte Jahr und kein Ende ist am Horizont absehbar.  Doch haben sich die Umstände geändert. So schreitet die Automatisierung immer weiter voran, zugleich entsteht auf globaler Ebene eine Klassenbewegung der Amazon-Arbeiter*innen, die sich vernetzen und sich zusammen gegen die Ausbeutung wehren wollen. Die Amazon-Vertrauensleute würden gerne mit euch über die neuen Entwicklungen der Arbeit und der Kämpfe ins Gespräch kommen.