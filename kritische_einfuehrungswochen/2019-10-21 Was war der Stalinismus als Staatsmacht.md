---
id: "506245526608159"
title: Was war der Stalinismus als Staatsmacht?
start: 2019-10-21 15:00
end: 2019-10-21 17:00
address: S203 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/506245526608159/
image: 71171194_1030509030452629_1541174951753547776_o.jpg
teaser: Mo. 21.10.19 / 15.00  Was war der Stalinismus als Staatsmacht?  Platypus
  Leipzig / S203   Unter dem Banner des Marxismus eroberte das Proletariat erst
isCrawled: true
---
Mo. 21.10.19 / 15.00

Was war der Stalinismus als Staatsmacht?

Platypus Leipzig / S203 

Unter dem Banner des Marxismus eroberte das Proletariat erstmalig in der Geschichte die Staatsmacht in Russland. Das Ausbleiben der internationalen proletarischen Revolution beeinflusste die Entwicklung des russischen Arbeiterstaates. „Die Zivilisatoren sind selbst in eine Sackgasse verrannt und versperren den sich Zivilisierenden den Weg.“ (Verratene Revolution, Leo Trotzki). Was war Stalinismus? Welchen Einfluss hatte und hat der Stalinismus auf die Linke?