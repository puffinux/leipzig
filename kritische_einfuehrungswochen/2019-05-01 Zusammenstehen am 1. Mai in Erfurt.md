---
id: '1917779338333731'
title: Zusammenstehen am 1. Mai in Erfurt
start: '2019-05-01 09:00'
end: '2019-05-01 21:00'
locationName: null
address: Erfurt
link: 'https://www.facebook.com/events/1917779338333731/'
image: 55760000_167862104101019_3697135924598013952_o.jpg
teaser: 'Am 1. Mai 2019 veranstalten wir, das #Bündnis „Zusammenstehen – vielfältig solidarisch“ eine „Versammlung der Vielen“ in Erfurt.   Neben einer #Demons'
recurring: null
isCrawled: true
---
Am 1. Mai 2019 veranstalten wir, das #Bündnis „Zusammenstehen – vielfältig solidarisch“ eine „Versammlung der Vielen“ in Erfurt. 

Neben einer #Demonstration lädt das Bündnis zu einem großen #Fest und #Konzert in den #Beethovenpark am Thüringer #Landtag ein. Die #Aktionen sollen ein kraftvolles und deutliches Zeichen für #Demokratie und #Solidarität sowie gegen soziale Spaltung und Rassismus setzen. 

Hintergrund ist der anhaltende #Rechtsruck sowie die anstehenden #Europawahlen und die #Landtagswahlen in Brandenburg, Sachsen und #Thüringen. Für die in der #AfD organisierten Rechten sind diese #Wahlen eine wichtige Etappe in ihrem Kampf gegen #Menschlichkeit und Demokratie.  Dagegen setzen wir auf eine #Versammlung der Vielen, deren #Gemeinsamkeit solidarische Antworten auf soziale Fragen im Interesse der #Vielfalt sind. 

Die Demonstration am 1. Mai 2019 beginnt um 10.00 Uhr an der #Staatskanzlei (Regierungsstraße 73, 99084 Erfurt), hält um 11.00 Uhr für eine #Zwischenkundgebung auf dem #Anger und kommt gegen 12.00 Uhr am Beethovenpark an.

Im #Vorfeld des 1. Mai gibt es eine Vielzahl von #Veranstaltungen, Details dazu gibt es unter http://zusammenstehen.eu/veranstaltungen/ 

In dem Ende 2018 gegründeten Bündnis „Zusammenstehen – vielfältig solidarisch“ sind momentan über 80 Vereine, kulturelle Initiativen, Gewerkschaften, Verbände des öffentlichen Lebens, religiöse und politische Institutionen sowie Privatpersonen engagiert.

Wir freuen uns auf euren Beitrag! 

Line-up und Ablauf folgen.

#vielfältigsolidarisch #zusammenstehen #ef0105