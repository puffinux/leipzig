---
id: '165505847673217'
title: Rassismustheorie und Kritisches Weißsein
start: '2019-05-13 15:00'
end: '2019-05-13 19:00'
locationName: null
address: Campus Augustusplatz / S 015
link: 'https://www.facebook.com/events/165505847673217/'
image: 55515121_907077492795784_7559242578122506240_o.jpg
teaser: 'Mo, 13.05., 15 – 19 Uhr:  Rassismustheorie und Kritisches Weißsein  Protest LEJ, InEUmanity / Campus Augustusplatz / S 015  Der Hass gegenüber Muslime'
recurring: null
isCrawled: true
---
Mo, 13.05., 15 – 19 Uhr:

Rassismustheorie und Kritisches Weißsein

Protest LEJ, InEUmanity / Campus Augustusplatz / S 015

Der Hass gegenüber Muslimen grassiert unverhohlen. Moscheen sehen sich Angriffen ausgesetzt und auf offener Straße werden Kopftücher von den Köpfen von Muslima heruntergerissen. Der Rassismus wird dabei nicht nur von der extremen Rechten auf die Straße getragen, sondern zur gleichen Zeit auch von der „Mitte" der Gesellschaft aus geschürt. AfD, Pegida, Bild, Spiegel oder die CSU betreiben unnachgiebig geistige Brandstiftung, füllen die Titelseiten hierzulande mit der angeblichen Bedrohung durch „den Islam" und mobilisieren zu rassistischen Straßenaufmärschen. Die Veranstaltung wird das Aufkommen des antimuslimischen Rassismus analysieren und die Frage diskutieren, wie diesem entschieden zu begegnen ist.
