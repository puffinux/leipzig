---
id: '2671447329542161'
title: 'BIPoC Vernetzung #1 - BIPOC Meeting'
start: '2019-10-07 11:00'
end: '2019-10-07 13:00'
locationName: null
address: 'Campus Augustusplatz, S112'
link: 'https://www.facebook.com/events/2671447329542161/'
image: 69770858_1017318018438397_6346663642766770176_o.jpg
teaser: 'Mo. 07.10.19 / 11.00  BIPoC Vernetzung #1 - BIPOC Meeting – Living in the Diaspora, get together & was bisher geschah  BPoC Hochschulgruppe / S112  De'
recurring: null
isCrawled: true
---
Mo. 07.10.19 / 11.00

BIPoC Vernetzung #1 - BIPOC Meeting – Living in the Diaspora, get together & was bisher geschah

BPoC Hochschulgruppe / S112

Der Workshop soll einen Raum des Austausches bieten, deshalb öffnen wir diese Veranstaltung speziell nur für BIPoCs (Black/Indigenous People of Color). Wir stehen zur Verfügung, Fragen zu beantworten, eigene Erfahrungen zu teilen und aktiv zuzuhören. Die Selbstbezeichnung BIPoC entspringt aus Erfahrungen der Gruppenmitglieder und reflektiert unsere Lebensrealitäten in der Diaspora. Da es ein zweites Meeting im Rahmen der KEW geben wird, können wir gemeinsam Ideen für dessen Gestaltung sammeln.