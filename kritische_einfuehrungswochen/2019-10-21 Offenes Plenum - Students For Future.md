---
id: "490387125139797"
title: Offenes Plenum - Students For Future
start: 2019-10-21 17:00
end: 2019-10-21 20:00
address: S302 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/490387125139797/
image: 71092892_1030512177118981_4665465945345490944_o.jpg
teaser: Mo. 21.10.19 / 17.00  Offenes Plenum - Students For Future  Students For
  Future Leipzig / S302  Wie jede Woche halten wir unser offenes Plenum ab, wo
isCrawled: true
---
Mo. 21.10.19 / 17.00

Offenes Plenum - Students For Future

Students For Future Leipzig / S302

Wie jede Woche halten wir unser offenes Plenum ab, wo immer alle willkommen sind. Damit euch der Einstieg in unsere Arbeit und die Möglichkeiten des Mitwirkens besonders leicht fallen, haben wir ein Plenum vorbereitet, auf dem ihr einen Rückblick über vergangene Aktionen sowie eine Vorstellung unserer Strukturen erwarten könnt.