---
id: "425358905005773"
title: Unteilbar Sachsen nach den Wahlen
start: 2019-10-17 15:00
end: 2019-10-17 17:00
address: S203 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/425358905005773/
image: 71043127_1029108787259320_2949707148604473344_o.jpg
teaser: "Do. 17.10.19 / 15.00  #unteilbar Sachsen nach den Wahlen - zu Kontext und
  historischen Anknüpfungspunkten der Bewegung  #unteilbar / S203  #Unteilbar"
isCrawled: true
---
Do. 17.10.19 / 15.00

#unteilbar Sachsen nach den Wahlen - zu Kontext und historischen Anknüpfungspunkten der Bewegung

#unteilbar / S203

#Unteilbar hat in diesem Jahr vor den sächsischen Landtagswahlen zusammen mit #wannwennnichtjetzt den Sommer der Solidarität begangen. Wie knüpft #unteilbar in Sachsen an den demokratischen Aufbruch zur Wendezeit an? Welche Besonderheiten gibt es in Sachsen vor allem im Kontext des Rechtsrucks und einer starken AfD? Wir wollen mit euch diskutieren, welche Kämpfe in #unteilbar vereint sind und wie diese gemeinsam gedacht werden können und was der Ausgang der Wahlen für die  Zukunft bedeutet.