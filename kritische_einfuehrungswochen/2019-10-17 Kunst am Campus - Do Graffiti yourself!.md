---
id: "1401335510007505"
title: Kunst am Campus - Do Graffiti yourself!
start: 2019-10-17 17:00
end: 2019-10-17 19:00
address: Couchcafé / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/1401335510007505/
image: 71008041_1029187893918076_4835757846693937152_o.jpg
teaser: Do. 17.10.19 / 17.00   Kunst am Campus - Do Graffiti yourself!  Kritische
  Einführungswochen / Couchcafé   Ob als nichtkommerzielle Kunst oder als Verw
isCrawled: true
---
Do. 17.10.19 / 17.00

 Kunst am Campus - Do Graffiti yourself!

Kritische Einführungswochen / Couchcafé 

Ob als nichtkommerzielle Kunst oder als Verwirklichung des Rechts auf Stadt ist Grafitti eine wichtige Form politischen Ausdrucks. Wir wollen dafür einen offenen Raum am Campus schaffen. Wer mag, kann sich eine Einführung ins Sprühen geben lassen, andere können gleich an den dafür  gespannten Folien loslegen. Bringt gern eure eigenen Dosen mit, wir haben aber auch welche da.