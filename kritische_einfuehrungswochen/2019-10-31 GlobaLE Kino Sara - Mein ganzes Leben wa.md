---
id: "449317262548697"
title: "GlobaLE Kino: Sara - Mein ganzes Leben war ein Kampf"
start: 2019-10-31 20:00
end: 2019-10-31 23:00
address: Neues Schauspiel Leipzig
link: https://www.facebook.com/events/449317262548697/
image: 64413392_3092243627454257_1665557436573941760_o.jpg
teaser: "Wann: Do 31 Oktober 2019 20:00 - 23:00  Sara - Mein ganzes Leben war ein
  Kampf  Türkei / 2015 / 95 min / Dersim Zerevan / kurdisch, türkisch mit dt. U"
isCrawled: true
---
Wann: Do 31 Oktober 2019 20:00 - 23:00

Sara - Mein ganzes Leben war ein Kampf

Türkei / 2015 / 95 min / Dersim Zerevan / kurdisch, türkisch mit dt. UT / Im Anschluss Diskussion mit dem Rechtsanwalt Lukas Theune über die Problematik politischer Repression in der Bundesrepublik gegen die kurdische Linke. Eintritt frei.

Sakine Cansiz ist Gründungsmitglied der Arbeiterpartei Kurdistans PKK und schloss sich als eine der ersten Frauen dem bewaffneten Widerstand für die Befreiung Kurdistans an. Sie ist eine der wichtigsten Symbol- und Identifikationsfiguren für die kurdische Befreiungsbewegung, insbesondere für kurdische Frauen.
Der Film erzählt ihre Geschichte in chronologischer Abfolge: Kindheit, Jugend, Gründung der PKK 1978, 10 Jahre Gefängnisaufenthalt bis hin zu dem Tag, an dem sie und zwei ihrer Genossinnen, Fidan Dogan und Leyla Saylemez, am 9. Januar 2013 in Paris ermordet worden sind.

Wo: Neues Schauspiel Leipzig, (Lützner Straße 29)