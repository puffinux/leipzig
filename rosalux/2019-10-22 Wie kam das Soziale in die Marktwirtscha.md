---
id: 8WW1C
title: Wie kam das Soziale in die Marktwirtschaft?
start: 2019-10-22 19:00
end: 2019-10-22 21:00
locationName: Volkshaus
address: "Karl-Liebknecht-Straße 30, 04107 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/8WW1C/
isCrawled: true
---
Mit *Dr. Uwe Fuhrmann* (Historiker)

Eine Veranstaltung im Rahmen des DGB-Zukunftsdialogs des DGB Nordsachsen in Kooperation mit der RLS Sachsen

Der genaue Ort wird noch auf [www.rosalux-sachsen.de](<http://www.rosalux-sachsen.de/>) bekannt gegeben.

Die „Soziale Marktwirtschaft“ sei - so ist die gängige Erzählung - am 20. Juni 1948 von Ludwig Erhard eingeführt worden. Doch das ist falsch, noch im Herbst 1948 streikten Millionen gegen Erhards Politik des freien Marktes. Die jahrzehntelange Praxis in Westdeutschland, irgendwie „soziale“ Maßnahmen in die Marktwirtschaft einzubauen, begann erst als Reaktion auf diese Proteste.

Die Geschichte der Jahre 1948/1949 und was über sie (nicht) erzählt wird, kann viel verraten über soziale Proteste, deren Wirkungen – und über Geschichtspolitik. Darum soll es an diesem Abend gehen.

