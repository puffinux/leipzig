---
id: 2YY1J
title: Das Wintermärchen
start: '2019-04-07 15:00'
end: '2019-04-07 17:00'
locationName: cineding
address: 'Karl-Heine-Straße 83, 04229 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/2YY1J/'
image: null
teaser: Schriftsteller erzählen die Bayerische Revolution und die Münchner Räterepublik
recurring: null
isCrawled: true
---
