---
id: 96NNK
title: "“Handeln! Handeln! Das ist es, wozu wir da sind!” "
start: 2019-10-29 18:00
end: 2019-10-29 20:00
locationName: RLS Sachsen
address: "Demmeringstraße 32, 04177 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/96NNK/
teaser: Fichtes Geschichtsphilosophische Positionen im Kontext der ‘Entdeckung’ der
  Zukunft
isCrawled: true
---
REIHE: Philosophische Dienstagsgesellschaft

Mit *Dr. Jürgen Stahl* (Philosoph), Moderation: *Dr. Monika Runge*



Fichtes Blick auf den sich vollziehenden historischen Prozess bedingt die Kritik gegebener Verhältnisse und der sie verbrämenden Theorien. Zeit ist ihm nicht mehr eine leere Form des Nacheinanders von Geschehenem, sondern ein Denkkonstrukt, konstituiert im und durch das Handeln des Subjekts. Dadurch gelangt das handelnde Subjekt in ein anderes, neues Verhältnis zur Geschichte.

