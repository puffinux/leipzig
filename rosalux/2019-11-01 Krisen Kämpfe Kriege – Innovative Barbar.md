---
id: N4CSH
title: Krisen Kämpfe Kriege – Innovative Barbarei gegen soziale Revolution –
  Kapitalismus und Massengewalt im 20. Jahrhundert
start: 2019-11-01 19:00
end: 2019-11-01 21:00
locationName: Selbstorganisierte Bibliothek Index
address: "Breite Straße/Wurzner Straße, 04315 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/N4CSH/
teaser: "REIHE: radical bookfair"
isCrawled: true
---
Mit *Detlef Hartmann* (Autor)

Eine Veranstaltung der radical bookfair in Kooperation mit der RLS Sachsen

Die Kriege, Massaker und Völkermorde des 20. Jahrhunderts gelten bis heute sowohl im bürgerlich-wissenschaftlichen als auch im linken und linksradikalen Verständnis zumeist als sinnloses Geschehen. Zu Unrecht. Ihr „Sinn“ enthüllt sich, wenn wir Kapitalismus nicht mehr nur als tauschförmig organisierte Ausbeutung begreifen, sondern als historischen Prozess gewaltsamer Inwertsetzung. Die Gewalt zerstört alte Arbeits- und Lebensformen und treibt auf der Suche nach neuen Wertressourcen innovatorische Technologien und Managementstrategien in die Gesellschaften hinein. Im Umbruch zum 20. Jahrhundert steht für diese innovatorischen Strategien das, was später Taylorismus bzw. Fordismus genannt wurde. Der Taylorismus/Fordismus griff tief in die tradierte Arbeits- und Lebenswelt ein, um sie zu zerstören und aus ihrer Reorganisation – orientiert am Ideal einer Arbeits- und Gesellschaftsmaschine – Gewinne zu schöpfen. Eine „schöpferische Zerstörung“, wie Joseph Schumpeter, einer der führenden Ökonomen der damaligen Zeit, dies nannte. Kriege, Massaker und Völkermorde waren die Mittel dieser Zerstörung. Und zugleich verhalfen sie den neuen Technologien zum Durchbruch. Die kapitalistischen Agenturen der Länder organisierten dies als Innovationskonkurrenz und trieben sich darin gegenseitig an. Zum Ende des Ersten Weltkriegs stiegen auch die Bolschewiki in diesen Prozess ein, mit fatalen Folgen, wie ihre Hungerpolitik, die großen Massaker und Völkermorde der 1920er und 1930er Jahre in der Sowjetunion vor Augen führen. Dieser Gewalt standen weltweit Bewegungen der sozialen Revolution gegenüber. Sie trugen ihre an einer „moralischen Ökonomie“ orientierten Lebens- und Arbeitsvorstelllungen aus den Dörfern bis in die Fabriken und Quartiere auch der Metropolen.

In seinem vor einigen Monaten erschienen Buch hat Detlef Hartmann diese Prozesse anschaulich nachgezeichnet. Wichtig ist ihm vor allem, die daraus gewonnenen Erkenntnisse zum Ausgangspunkt einer Diskussion darüber zu nehmen, mit welchen Formen und Exzessen der Gewalt wir in diesem Jahrhundert unter dem Aufprall eines neuen technologischen Angriffs rechnen müssen und welche Chancen die soziale Revolution im 21. Jahrhundert dagegen hat.

