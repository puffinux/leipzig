---
id: OK7M8
title: Christel-Hartinger-Preis für Zivilcourage und beherztes Engagement
start: '2019-08-31 14:00'
end: '2019-08-31 16:00'
locationName: PAN
address: 'Lindenauer Markt 21, 04177 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/OK7M8/'
image: null
teaser: Preisverleihung
recurring: null
isCrawled: true
---
