---
id: B2R4K
title: Zine-Workshop „Wem gehört die Hood?“
start: '2019-05-04 11:00'
end: '2019-05-04 18:00'
locationName: Verein zur Stärkung einer guten Sache e.V.
address: 'Eisenbahnstraße 127, 04315 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/B2R4K/'
image: null
teaser: null
recurring: null
isCrawled: true
---
