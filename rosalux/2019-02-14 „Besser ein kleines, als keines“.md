---
id: R2UR4
title: '„Besser ein kleines, als keines“'
start: '2019-02-14 18:00'
end: '2019-02-14 20:00'
locationName: Reclam Museum
address: 'Kreuzstraße 12, 04103 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/R2UR4/'
image: null
teaser: 'das Reclam-Museum im Grafischen Viertel '
recurring: null
isCrawled: true
---
