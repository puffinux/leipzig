---
id: 9WGVA
title: Kolja Mensing mit seinem Buch Fels
start: 2019-10-19 15:00
end: 2019-10-19 17:00
locationName: Selbstorganisierte Bibliothek Index
address: "Breite Straße/Wurzner Straße, 04315  "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/9WGVA/
teaser: "REIHE: radical bookfair"
isCrawled: true
---
Mit *Kolja Mensing* (Autor) 

Eine Veranstaltung der radical bookfair in Kooperation mit der RLS Sachsen



Kolja Mensing hat die romantische Geschichte der heimlichen Verlobung seiner Großeltern Heiligabend 1943 schon oft gehört. Als seine Großmutter ins Krankenhaus kommt, lässt er sie sich noch einmal erzählen. Diesmal erwähnt seine Großmutter auch Albert Fels, einen jüdischen Viehhändler von nebenan. Zu Kriegsbeginn wird er in eine Heil- und Pflegeanstalt eingewiesen und kehrt nie zurück. Man weiß ja, was damals passiert ist, sagt die Großmutter, und damit fallen die dunklen Schatten der Euthanasie und des Holocaust auch auf die Geschichte von der großen Liebe ihres Lebens. Ein Buch über Familiengeschichten – und über Macht, die die Erinnerungen anderer Menschen über uns haben.

