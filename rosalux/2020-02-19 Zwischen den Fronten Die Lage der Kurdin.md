---
id: MKLBR
title: "Zwischen den Fronten: Die Lage der Kurd*innen in Nordsyrien"
start: 2020-02-19 19:00
end: 2020-02-19 21:00
locationName: Der Ort
address: "wird noch bekannt gegeben., 04177 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/MKLBR/
isCrawled: true
---
Mit *Murat Çakır* (Regionalbüroleiter der RLS in Hessen) 



Wir haben den Regionalbüroleiter der RLS in Hessen, Murat Çakır, eingeladen und wollen mit ihm über die Lage der basisdemokratischen Kantone in Nordsyrien, über die Situation nach dem türkischen Einmarsch, Perspektiven in dem Konfliktgebiet und über die Interessen der am Konflikt beteiligten Parteien diskutieren. Wir wollen auch der Frage nachgehen, warum die Solidarität mit den basisdemokratischen Kräfte in der Türkei und in Syrien im ureigenen Interesse der Menschen in Deutschland liegt und was wir tun können, außer nächtliche Demonstrationen zu besuchen.



*Diese Steuermittel werden auf Grundlage des von den Abgeordneten des Sächsischen Landtags beschlossenen Haushaltes zur Verfügung gestellt.*

