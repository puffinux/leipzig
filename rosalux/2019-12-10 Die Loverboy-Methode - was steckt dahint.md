---
id: TV2AN
title: "Die Loverboy-Methode - was steckt dahinter? "
start: 2019-12-10 17:00
end: 2019-12-10 19:00
locationName: Volkshochschule
address: "Löhrstraße 3-7, 04105 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/TV2AN/
teaser: Einblick in eine Parallelwelt
isCrawled: true
---
Eine Veranstaltung von TERRE DES FEMMES - Menschenrechte für die Frau e.V. Leipzig in Kooperation mit der VHS Leipzig und der RLS Sachsen

Loverboys gaukeln Mädchen die große Liebe vor - und schicken sie dann auf den Strich. Dass Frauen in die Prostitution geraten, weil sie sich in ihren Zuhälter verlieben, hat es immer schon gegeben. Aber dass Männer gezielt Schülerinnen ansprechen, emotional abhängig machen und schließlich prostituieren, ist ein jüngeres Phänomen. Die meisten der betroffenen Mädchen stammen aus oberen sozialen Schichten, sind gebildet, gehen aufs Gymnasium. Das Prinzip funktioniert hier so gut, weil in diesen Familien niemand damit rechnet und man entsprechend weniger darauf achtet.

Als Fachexpertin steht an diesem Abend Sandra Norak zur Verfügung. Als Fachexpertin steht an diesem Abend Sandra Norak zur Verfügung.

