---
id: QQP74
title: Transformation2SocialGreen
start: 2019-10-18 19:00
end: 2019-10-19 21:00
locationName: Interim
address: "Demmeringstraße 32, 04177 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/QQP74/
teaser: Herausforderungen, Pfade und Gestaltung sozialökologischer Transformationen
isCrawled: true
---
Eine gemeinsame Veranstaltung der Abgeordnetenbüros linXXnet und INTERIM mit der RLS Sachsen

Wie lassen sich Energie-, Verkehrs- oder Agrarwende fördern? Wie können komplexe Transformationsprozesse, die mit den notwendigen Nachhaltigkeitszielen verbunden sind, trotz Herausforderungen und Widerständen gestaltet werden?

Theoretische Inputs z.B. über Neosozialismus und Kapitalismuskritik aus queer-feministischer Perspektive sollen einen Einblick in die Diskussion um sozialökologische Transformation geben. In einem Workshopteil werden eigene Ideen, Konzepte und Strategien diskutiert, die in eine nachhaltige Prozess- und Projektstruktur in Leipzig überführt werden sollen.

**Programm**

▸18. Oktober, Freitag

19\.00 Uhr

Podiumsdiskussion

**Zeit umzudenken! Die Plurale Ökonomie als Türöffner für die sozialökologische Transformation? 

Beiträge der wissenschaftlichen Lehre für eine sozialökologische Wirtschaft

**Mit *Dr. Dirk Ehnts* (heterodoxer Ökonom, TU Chemnitz), *Dr. Karin Schönpflug* (heterodoxe Ökonomin, Universität Wien), *Marius Ewert* (Plurale Ökonomie Gruppe Halle/Leipzig), Moderation: *Dr. Claudia Rauhut* (AG Postkolonial Leipzig)

Abgeordnetenbüro INTERIM, Demmeringstraße 32, 04177 Leipzig

In der Podiumsdiskussion wird der Ist-Zustand in der momentanen wirtschaftlichen Ausbildung an den Universitäten besprochen und diskutiert, wie die sozialökologischen Krisenerscheinungen und deren Beschreibungen in der Zukunft ihre Geltung finden müssen. Zudem sollen die Möglichkeiten aber auch Herausforderungen sozial-ökologischen Wirtschaftens in der akademischen Ausbildung aufgezeigt und deren Umsetzung in die Praxis erörtert werden.



▸19. Oktober, Sonnabend

Tagung mit Workshops



**Herausforderungen, Pfade und Gestaltung sozialökologischer Transformationen

**Mit *Prof. Dr. Klaus Dörre* (Soziologe), *Dr. Karin Schönpflug* (Ökonomin), *Dr. Alexander B. Voegele* (Lehrbeauftragter an der HWR-Berlin) und *Dr. Axel Troost* (Arbeitsgruppe Alternative Wirtschaftspolitik)

10\.30 Uhr: 

**Notwendigkeit und politische Herausforderungen für sozialökologische Transformationen im Kontext des Neosozialismus**

Mit *Prof. Dr. Klaus Dörre *(Soziologe, Friedrich-Schiller-Universität Jena): 



11\.30-18.30 Uhr: 

Workshop-Phase: Herausforderungen, Pfade und Gestaltungsansätze in aktuellen Transformationsprozessen 

**Feministische und postkoloniale Reflexion auf sozialökologische Transformationen**

Mit *Dr. Karin Schönpflug* (Ökonomin, Universität Wien)

**Postwachstum**

Mit *Prof. Dr. Klaus Dörre *(Soziologe, FSU Jena)

**Euro, Wert und Kilojoule. Privateigentum als Öko-Killer**

Mit *Dr. Alexander B. Voegele* (Lehrbeauftragter an der HWR-Berlin)

**Sozial-ökologischer Umbau - das Konzept der Arbeitsgruppe Alternative Wirtschaftspolitik**

Mit *Dr. Axel Troost* (Arbeitsgruppe Alternative Wirtschaftspolitik)



17\.30-18.30 Uhr: 

Abschlussdiskussion mit Präsentation der Workshopergebnisse

Workshopbeschreibungen:

Die Workshops finden in den Räumen der RLS Sachsen, des Vereins Rosa Linde e.V., des Vereins Drug Scouts e.V. und des Abgeordnetenbüros INTERIM in der Demmeringstraße 32, 04177 Leipzig statt.

Dr. Karin Schönpflug (Universität Wien): Feministische und postkoloniale Reflexion auf sozialökologische Transformationen 

Hierarchische Binaritäten wie gender, race, class bestimmen nicht nur unseren Standpunkt, wenn es um die Entwicklung alternativer Ökonomiekonzepte geht. Vielmehr sind diese Kategorien aber auch als jene historischen Mechanismen zu erkennen, welche einen kolonialistisch getriebenen Kapitalismus erst ermöglichen konnten.

Prof. Dr. Klaus Dörre (Friedrich-Schiller-Universität Jena): Postwachstum 

In diesem Workshop wird über Leitbilder, politische Maßnahmen und Instrumente diskutiert, die zu einer Reduktion der Wirtschaftsleistung führen. Dies soll im Zusammenhang mit konkreten Vorstellungen stehen die in eine sozialökologische Transformation integriert werden können.

Dr. Alexander B. Voegele (Lehrbeauftragter an der HWR-Berlin): Euro, Wert und Kilojoule. Privateigentum als Öko-Killer

Kann der Preis unter den Bedingungen des Privateigentums an Produktionsmitteln den Verbrauch an stofflichen Ressourcen (ökologischer Fußabdruck) korrekt abbilden?

Dieser Workshop versteht sich als ein Input mit theoretischen und praktischen Vorstellungen zur sozialökologischen Transformation.

Dr. Axel Troost (Arbeitsgruppe Alternative Wirtschaftspolitik): Sozial-ökologischer Umbau - das Konzept der Arbeitsgruppe Alternative Wirtschaftspolitik 

Ökologisch sinnvolle Politik, Wohlstand und Beschäftigung sind keine Gegensätze. Den für ambitionierten Klimaschutz notwendigen Investitionen stehen ähnlich hohe oder sogar noch größere Einsparungen beim Import fossiler Energieträger gegenüber. Für den Klimaschutzplan 2050 des Bundesumweltministeriums errechneten das Öko-Institut und das Fraunhofer ISI für das Jahr 2030 deutschlandweit hunderttausende zusätzliche Jobs und ein höheres Wirtschaftswachstum.

Ein grüner Anstrich für eine ansonsten unveränderte Wirtschaftspolitik ist allerdings viel zu wenig. Eine neue „grüne“ Industrieproduktion in Deutschland muss in eine neue Wirtschaftspolitik eingebettet sein.



