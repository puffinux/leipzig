---
id: X5L7N
title: Monis Rache. Wie weiter?
start: 2019-11-24 11:00
end: 2019-11-24 17:00
locationName: Selbstorganisierte Bibliothek Index
address: "Breite Straße/Wurzner Straße, 04315 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/X5L7N/
isCrawled: true
---
Eine Veranstaltung der Gruppe “Monis Rache”

Als Grundlage für unsere weitere Zusammenarbeit wollen wir an einem politischen Selbstverständnis arbeiten und dafür Fragen zum politischen Werteverständnis, kollektiver Organisation, basisdemokratische Entscheidungsfindung sowie Visionen und Zielen unser Arbeit auseinandersetzen.Der Workshop dient als Reflexionsprozess und zur Weiterentwicklung unser Gruppe und unseres Netzwerks. Interessierte Personen sind herzlich eingeladen.Anmeldung über: [hanna@monisrache.wtf](<mailto:hanna@monisrache.wtf>)

