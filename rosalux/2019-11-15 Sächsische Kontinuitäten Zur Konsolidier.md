---
id: 14H9S
title: Sächsische Kontinuitäten? Zur Konsolidierung rechter Hegemonie seit 1989
start: 2019-11-15 19:00
end: 2019-11-15 21:00
locationName: Handstand und Moral
address: "Merseburger Straße 88b, 04177 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/14H9S/
isCrawled: true
---
Mit *Tino Heim* (Soziologe); Moderation: *Paul Lißner*

Die erstarkten menschenverachtenden Einstellungen in Sachsen reichen von alltäglichen Beschimpfungen bis zu Hetzjagden auf Menschen, die anders aussehen. Mit der Landtagswahl 2019 spiegelt sich dies auch auf der politischen Bühne. Der Soziologe Tino Heim von der TU Dresden referiert über die Konsolidierung rechter Hegemonie in Sachsen seit 1989. So vielfältig die Angriffe von rechts sind, so unterschiedlich könnten auch Reaktionen darauf sein. Wie man sich gegen diese Entwicklung organisieren und für eine solidarische Gesellschaft kämpfen kann, wollen wir im Anschluss mit Euch diskutieren.

„Was wir brauchen ist Nüchternheit: einen Pessimismus des Verstandes, einen Optimismus des Willens.“ (Gramsci)

