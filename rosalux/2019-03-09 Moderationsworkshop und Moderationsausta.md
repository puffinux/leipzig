---
id: 3VP6Q
title: Moderationsworkshop und Moderationsaustausch
start: '2019-03-09 12:00'
end: '2019-03-09 18:00'
locationName: Verein zur Stärkung einer guten Sache e.V.
address: 'Eisenbahnstraße 127, 04315 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/3VP6Q/'
image: null
teaser: null
recurring: null
isCrawled: true
---
