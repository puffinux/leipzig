---
id: 8GU32
title: Zine-Workshop „Wem gehört die Hood?“
start: '2019-06-15 11:00'
end: '2019-06-15 18:00'
locationName: Verein zur Stärkung einer guten Sache e.V.
address: 'Eisenbahnstraße 127, 04315 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/8GU32/'
image: null
teaser: null
recurring: null
isCrawled: true
---
