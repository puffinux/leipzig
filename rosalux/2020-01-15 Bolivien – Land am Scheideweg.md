---
id: EH446
title: Bolivien – Land am Scheideweg?
start: 2020-01-15 19:00
end: 2020-01-15 20:00
locationName: RLS Sachsen
address: "Demmeringstraße 32, 04177 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/EH446/
isCrawled: true
---
 Mit *Walter Magne Veliz* (erster indigener Botschafter Boliviens in Deutschland)



Die sozialen und Wachstums-Indikatoren Boliviens waren die Besten Südamerikas. Die Regierung unter Evo Morales versuchte die sozialen Unterschiede, vor allem der indigenen Bevölkerung, abzubauen und eine neue Verfassung zu erarbeiten.



Nach den Präsidentschaftwahlen im Oktober 2019 überschlagen sich in Bolivien die Ereignisse, jeden Tag verschieben sich die Kräfteverhältnisse und bringen unsichere Zukunftsperspektiven für Bolivien mit sich. Evo Morales ist nach Mexiko ins Exil gegangen und hat dort politisches Asyl bekommen. Rechte Gruppen und Teile der Polizei greifen Protestierende an. Die Opposition, die sich teilweise um extrem Rechte schart, kann keine verfassungsgemäße Regierung bilden, da dafür eine reguläre Sitzung des Parlaments mit dem notwendigen Quorum stattfinden müsste. Dennoch hat sich die oppositionelle Senatorin Jeanine Añez selbst zur Präsidentin erklärt. Unterdessen erlebt Bolivien neue Mobilisierungen für eine Rückkehr von Evo Morales und gegen die Oppositionsführer, die zum Beispiel von der Bevölkerung von El Alto zu unerwünschten Personen erklärt wurden.



Walter Magne Veliz war erster indigener Botschafter Boliviens in Deutschland und beleuchtet, was in Bolivien aktuell geschieht. 



*Diese Steuermittel werden auf Grundlage des von den Abgeordneten des Sächsischen Landtags beschlossenen Haushaltes zur Verfügung gestellt.*