---
id: E5R7G
title: SQUEEZIE-Queer-Zinefest
start: '2019-05-11 12:00'
end: '2019-05-12 19:00'
locationName: Deutsches Literaturinstitut Leipzig
address: 'Wächterstraße 11, 04107 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/E5R7G/'
image: null
teaser: 'Workshops, Lesungen und Vieles mehr'
recurring: null
isCrawled: true
---
