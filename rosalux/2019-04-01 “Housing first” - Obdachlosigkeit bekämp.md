---
id: BYEZY
title: “Housing first” - Obdachlosigkeit bekämpfen
start: '2019-04-01 10:00'
end: '2019-05-31 18:00'
locationName: RLS Sachsen
address: 'Demmeringstraße 32, 04177 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/BYEZY/'
image: null
teaser: 'Eine Ausstellung von “FiftyFifty”, erstellt von  Katharina Mayer, Denise Tombers und Wohnungslosen'
recurring: null
isCrawled: true
---
