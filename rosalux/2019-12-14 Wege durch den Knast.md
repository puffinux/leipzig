---
id: WGZ49
title: Wege durch den Knast
start: 2019-12-14 15:00
end: 2019-12-14 17:00
locationName: Selbstorganisierte Bibliothek Index
address: "Breite Straße/Wurzner Straße, 04315 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/WGZ49/
teaser: "REIHE: radical bookfair"
isCrawled: true
---
Mit *Vertreter\*innen des Redaktionskollektivs*

Eine Veranstaltung der radical bookfair in Kooperation mit der RLS Sachsen

»Wege durch den Knast« ist ein umfassendes Standardwerk für Betroffene, Angehörige und Interessierte. Es vermittelt tiefe Einblicke in den Knastalltag, informiert über die Rechte von Inhaftierten und zeigt Möglichkeiten auf, wie diese durchgesetzt werden können. Das Buch basiert auf einer Ausgabe aus den 1990er Jahren und wurde von Anwält\*innen, Gefangenen, Ex-Gefangenen und Bewegungsaktivist\*innen überarbeitet und aktualisiert.

Im Vortrag werden die Entstehungsgeschichte sowie Erfolge und Misserfolge nach vier Jahren Buchverschickung in die Knäste beleuchtet. Im Anschluss kann gern diskutiert werden.

