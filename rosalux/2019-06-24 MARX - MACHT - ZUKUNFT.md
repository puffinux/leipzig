---
id: IL1DN
title: MARX - MACHT - ZUKUNFT
start: '2019-06-24 18:00'
end: '2019-06-24 20:00'
locationName: Galerie KUB
address: 'Kantstraße 18, 04275 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/IL1DN/'
image: null
teaser: Kritische Gesellschaftstheorie und emanzipatorische Lebenspraxis
recurring: null
isCrawled: true
---
