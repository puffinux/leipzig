---
id: 59XRD
title: Women in Germany’s genocide against the Ovaherero and Namas
start: 2019-12-19 19:30
end: 2019-12-19 21:30
locationName: Ost-Passage Theater
address: "Konradstraße 27, 04365 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/59XRD/
teaser: And in the ongoing struggle for reparations
isCrawled: true
---
With Esther Muinjangue (Head of Ovaherero Genocide Foundation) and Sima Luipert (Head of Genocide Technical Committee of the Nama Traditional Leaders Association)

Moderation: Dr. Claudia Rauhut (Leipzig Postkolonial)

Germany's colonial genocide (1904-08) in what is today Namibia affected not only the resistant men but also women and children who died of thirst in the Omaheke/Kalahari or were worked and tortured to death in concentration camps. The Ovaherero and Nama activists Esther Miunjangue (chair of the Ovaherero Genocide

Foundation) and Sima Luipert (vice chair of the Genocide Technical Committee of the Nama Traditional Leaders Association) talk about women's resistance and their

continuing struggle for recognition of the genocide by the German government. They explain why the affected communities demand an official apology and reparations.

With this event, Leipzig Postkolonial would like to contribute to a stronger public awareness of Germany’s colonial legacies, and about historical and political

responsibility.

The panel will be in English.

