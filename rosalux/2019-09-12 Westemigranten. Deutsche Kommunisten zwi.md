---
id: 99L3M
title: Westemigranten. Deutsche Kommunisten zwischen USA-Exil und DDR
start: '2019-09-12 18:00'
end: '2019-09-12 20:00'
locationName: Reclam-Museum
address: 'Kreuzstraße 12, 04103  '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/99L3M/'
image: null
teaser: 'REIHE: Jour Fixe - Ein unkonventioneller Gesprächskreis'
recurring: null
isCrawled: true
---
