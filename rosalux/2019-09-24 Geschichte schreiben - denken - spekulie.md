---
id: 6VN97
title: 'Geschichte schreiben - denken - spekulieren? '
start: '2019-09-24 18:00'
end: '2019-09-24 20:00'
locationName: RLS Sachsen
address: 'Demmeringstraße 32, 04177 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/6VN97/'
image: null
teaser: Erkundungen zur Geschichtsphilosophie bei Kant und Marx
recurring: null
isCrawled: true
---
