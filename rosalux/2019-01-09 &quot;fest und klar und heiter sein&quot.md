---
id: 6DK2E
title: '&quot;fest und klar und heiter sein&quot;'
start: '2019-01-09 18:00'
end: '2019-01-09 19:30'
locationName: 'Rosa-Luxemburg-Stiftung Sachsen '
address: 'Harkortstraße 10, 04107 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/6DK2E/'
image: null
teaser: eine Hommage an Rosa Luxemburg und Karl Liebknecht aus Anlass des 100. Todestages
recurring: null
isCrawled: true
---
