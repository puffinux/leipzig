---
id: IL2J8
title: 'Er-innerung, Macht, Geschichte'
start: '2019-05-28 18:00'
end: '2019-05-28 20:00'
locationName: RLS Sachsen
address: 'Demmeringstraße 32, 04177 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/IL2J8/'
image: null
teaser: 'Überlegungen zu einer Kunst der Erinnerung nach Nietzsche REIHE: Philosophische Dienstagsgesellschaft'
recurring: null
isCrawled: true
---
