---
id: OMS8X
title: Theoretische Arbeit und Geschlechterverhältnis
start: 2019-10-16 19:00
end: 2019-10-16 21:00
locationName: Selbstorganisierte Bibliothek Index
address: "Breite Straße/Wurzner Straße, 04315 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/OMS8X/
isCrawled: true
---
Mit *Cordula Trunk* (Speakerin Gesellschaftskritik, Marxismus, Queer-Feminismus)

Eine Veranstaltung des Emanzipatorischen Blocks in Kooperation mit der RLS Sachsen



Wieso ist das Geschlechterverhältnis so unausgewogen, wenn es um theoretische Arbeit geht? Sei es in der Uni, bei Diskussionen oder dem linken Lesekreis. Was hemmt Frauen\* daran, sich in die theoretische Arbeit zu stürzen? Wieso brauchen wir überhaupt Theorie? Welche Probleme gibt es von Seiten der Theorie selbst? Und was hat das mit Reproduktions- und Produktionssphäre und dem Subjekt-Objekt-Verhältnis zu tun? Aus einer theoretisch-praktischen Perspektive wird sich diesen Phänomenen und Fragen genähert und im Anschluss daran praktische Vorschläge zur Verbesserung der Situation diskutiert.

