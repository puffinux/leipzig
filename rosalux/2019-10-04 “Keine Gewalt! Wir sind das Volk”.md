---
id: 4JFA6
title: “Keine Gewalt! Wir sind das Volk”
start: 2019-10-04 18:00
end: 2019-10-04 20:00
locationName: Alte Börse
address: "Naschmarkt 1, 04109  "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/4JFA6/
teaser: Die historische Tat der „Leipziger Sechs“ am 9. Oktober 1989
isCrawled: true
---
REIHE: Jour Fixe - Ein unkonventioneller Gesprächskreis

Mit *Dr. Kurt Meyer*, *Dr. Roland Wötzel, Prof. Dr. Cornelius Weiss* und *Michael Zock*, Moderation: *Gerd-Rüdiger Stephan* (RLS, Historiker)



Vor dem Hintergrund der aktuellen Debatten über die friedliche Revolution wird das dramatische Geschehen am 9. Oktober 1989 nochmals rekonstruiert, die Zivilcou­ra­ge der „Leipziger Sechs“ gewürdigt und der Mut der Demonstranten herausgestellt.

