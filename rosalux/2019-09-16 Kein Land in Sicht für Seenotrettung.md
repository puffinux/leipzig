---
id: KCCO5
title: Kein Land in Sicht für Seenotrettung
start: 2019-09-16 10:00
end: 2019-10-18 15:00
locationName: RLS Sachsen
address: "Demmeringstraße 32, 04177 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/KCCO5/
teaser: Eine Ausstellung der Tageszeitung Neues Deutschland, veranstaltet vom
  Abgeordnetenbüro INTERIM und der RLS Sachsen
isCrawled: true
---
Das Mittelmeer war 2018 erneut die gefährlichste Seeroute der Welt. Laut UN-Flüchtlingshilfswerk lag dies an der restriktiven Flüchtlingspolitik der Europäischen Union. Während die EU-Staaten die Häfen schlossen, entstand eine Vielzahl an privaten Initiativen, die sich der Rettung von Menschen in Seenot annahmen. Auch wenn die Rettung Schiffbrüchiger im internationalen Recht verankert ist, werden viele Seenotrettungsorganisationen mit konstruierten Anklagen überzogen. Die Ausstellung porträtiert die Arbeit der Seenotretter\*innen.

Die Ausstellung befindet sich in den Räumen der RLS Sachsen und des Abgeordnetenbüro INTERIM.

Öffnungszeit: Zu den jeweiligen Büroöffnungszeiten und nach Vereinbarung

