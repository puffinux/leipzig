---
id: PLN9O
title: Eine Geschichte des Scheiterns? Der linke Antisemitismus aus jüdischer
  Perspektive
start: 2019-10-22 19:00
end: 2019-10-22 21:00
locationName: Atari
address: "Kippenbergstraße 20, 04317 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/PLN9O/
isCrawled: true
---
Mit *Miklós Klaus Rózsa* (Fotograf und Politaktivist), *Angela Fuc*hs und *Alexandra Bandl* (Bildungsreferentin, Initiative Mündigkeit durch Bildung); Moderation: *Tina Sanders*

Eine Veranstaltung der Initiative Mündigkeit durch Bildung (IMdB) mit Unterstützung des StuRa der Uni Leipzig und der RLS Sachsen



Vor 70 Jahren beschämte der Staat Israel durch seine Gründung die zum Schutz der Juden unfähige Weltgemeinschaft, so die Hamburger Studienbibliothek im Mai 2018. Auch die Arbeiterbewegung müsste sich angesichts der Shoah ihr Scheitern eingestehen. Bis heute haben weite Teile sowohl der sozialdemokratischen, als auch radikalen Linken Israel nicht verziehen, dass sich die bewaffnete jüdische Staatsgewalt als wirksamster Schutz gegen Antisemitismus bewährt.

Die Auswirkungen auf das eigene jüdische Selbstverständnis sowie die politische Praxis werden im Spannungsfeld von partikularen Interessen und universalistischem Anspruch beleuchtet.

