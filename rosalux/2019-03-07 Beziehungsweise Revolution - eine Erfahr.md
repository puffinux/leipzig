---
id: WV5FV
title: 'Beziehungsweise Revolution - eine Erfahrung, die gerettet werden will'
start: '2019-03-07 18:00'
end: '2019-03-07 21:00'
locationName: Institut fuer Zukunft
address: 'An den Tierkliniken 38-40, 04103 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/WV5FV/'
image: null
teaser: 'Feministischer Empfang mit Buchbesprechung, Diskussion und anschließender Musik'
recurring: null
isCrawled: true
---
