---
id: 59NPL
title: 'Geschichtspolitiken und Erinnerungskultur: Die Archive der Stasi'
start: '2019-09-24 19:00'
end: '2019-09-24 21:00'
locationName: Galerie für zeitgenössische Kunst - Auditorium
address: 'Karl-Tauchnitz-Straße 09-11, 04107 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/59NPL/'
image: null
teaser: Im Rahmen des Ausstellungsprojekts &quot;Bewußtes Unvermögen - Das Archiv Gabriele Stötzer&quot;
recurring: null
isCrawled: true
---
