---
id: GCII6
title: Das ‘Digitale&#039; im kapitalistischen Zeitalter
start: 2019-10-18 19:00
end: 2019-10-18 21:00
locationName: Universität Leipzig
address: "Augustusplatz, 04107 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/GCII6/
isCrawled: true
---
Mit *Timo Daum* (Hochschullehrer in den Bereichen Online, Medien und Digitale Ökonomie)

Eine Veranstaltung der AG Link - kritische Informatik an der Uni Leipzig im Rahmen der Kritischen Einführungswochen des Student\_innenRates der Universität Leipzig in Kooperation mit der RLS Sachsen



Ob es darum geht, die ganze Erde zu kartieren oder alle Freundschaften der Welt zu organisieren – im digitalen Kapitalismus werden Algorithmen zur wichtigsten Maschine, Daten zum essenziellen Rohstoff und Informationen zur Ware Nummer eins. Zur Frage nach dem Neuen im alten Kapitalismus referiert Timo Daum Autor von „Das Kapital sind wir“, welches 2018 mit dem Preis des politischen Buches der Friedrich Ebert-Stiftung ausgezeichnet wurde.

