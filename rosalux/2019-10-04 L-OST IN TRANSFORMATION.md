---
id: OWDGC
title: L-OST IN TRANSFORMATION
start: 2019-10-04 19:30
end: 2019-10-05 18:00
locationName: Galerie KUB
address: "Kantstraße 18, 04275 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/OWDGC/
teaser: Aktuelle Analysen der ostdeutschen Gesellschaft
isCrawled: true
---
Eine Veranstaltung des Engagierte Wissenschaft e.V. und des AK Kritische Geographie Leipzig in Kooperation mit weiterdenken. Heinrich-Böll-Stiftung Sachsen und der RLS Sachsen

Galerie KUB, Kantstraße 18, 04275 Leipzig

Was ist eigentlich im Osten los? Seit 2014/15 hat die Dynamik der Ereignisse - Wahlergebnisse für die AfD, gewalttätige Auseinandersetzungen, verbale Entgrenzungen - insbesondere in Ostdeutschland ständig zugenommen. Es scheint eine neue Phase der Nachwendegeschichte begonnen zu haben, in der das bisherige, stille Einverständnis mit den Regierenden von einem beträchtlichen Teil der Bevölkerung aufgekündigt wird.

Mit den gegenwärtigen Debatten um die Anerkennung der „ostdeutschen Lebensleistung“, einen „Gerechtigkeitsfonds“, eine verbindliche Einstellungsquote für Ostdeutsche, aber auch das Sprechen über biographische Brüche rückt die Zeit der „friedlichen Revolution“ erneut in den Fokus der Debatten. Deutungen über den Charakter der ostdeutschen Teilgesellschaft 30 Jahre nach dem Mauerfall haben wieder Hochkonjunktur.

Die Konferenz „Lost in Transformation“ möchte geläufige und weniger geläufige Analysen über die Eigenarten Ostdeutschlands versammeln und mit einer interessierten Öffentlichkeit debattieren. Neben aktuell diskutierten Fragen zum ostdeutschen Wohnungsmarkt, zum Rechtsruck und zur spezifischen ostdeutschen Wirtschaftsweise werden auch die ideologischen Hinterlassenschaften der DDR-Gesellschaft und die Widersprüchlichkeiten der Debatten zu Migration damals und heute beleuchtet.

Die Konferenz versteht sich dabei als Debattenforum, richtet sich explizit auch an ein nicht-wissenschaftliches Publikum und möchte zivilgesellschaftliche Initiativen zum Austausch einladen.

**<u>Programm</u>

**

**▸4. Oktober, Freitag**



19\.30 Uhr 

Lesung und Diskussion

"Das Zauberwort heißt Doppelleben"

Mit *Carolin Krahl* (Autorin)

Die Leipziger Autorin Carolin Krahl liest aus ihrem Romanmanuskript, in dem die Erfahrungen jener Generation von Frauen reflektiert werden, die in der Zeit der Wende in Ostdeutschland aufwuchs und damit früh durch die Veränderungen der gesellschaftlichen Stellung der Frau zwischen DDR und BRD geprägt wurden.

20\.30 Uhr 

Wie schauen wir heute zurück? Feministische Perspektiven auf die Wiedervereinigung

Mit N.N.

Podiumsdiskussion zu Geschlechterverhältnissen im Transformationsprozess

**▸5. Oktober, Sonnabend**



Tagung

10\.00 Uhr

„Lost in Transformation“ - aktuelle Forschung und Debatten in und über Ostdeutschland

Mit *Elisa Gerbsch* und *Dominik Intelmann* (Humangeograph\*innen)

11\.00 Uhr 

Die Aporien der SED-Ideologie und ihre Auswirkungen

Mit *Jeanne Franke* (Soziologin)

In den öffentlichen Auseinandersetzungen um Ostdeutschland wird die DDR häufig pauschal als eine zweite deutsche Diktatur und autoritäres Regime bestimmt. Ziel des Vortrags ist es, einen differenzierteren Blick auf die politische Herrschaft der SED zu werfen und die Idee eines sozialistischen Staates ernst zu nehmen. Im Mittelpunkt stehen dabei das Verhältnis zwischen Demokratie und Diktatur sowie Internationalismus und Antiimperialismus.

11\.00 Uhr

Kapitalismus in der Peripherie – die Politische Ökonomie Ostdeutschlands

Mit *Dominik Intelmann* (Humangeograph)

Die Politische Ökonomie Ostdeutschlands ist geprägt durch eine strukturelle Abhängigkeit vom westdeutschen Landesteil. Dabei schlägt sich das Fehlen einer lokalen Eigentümer\*innenklasse in einer dauerhaften Transferabhängigkeit nieder. Im Beitrag wird diese bis heute andauernde Konstellation anhand der politischen Richtungsentscheidungen im Wiedervereinigungsprozess rekonstruiert.

14\.00 Uhr

Die Wohnungsfrage(n) in Ostdeutschland zwischen politischem Autoritarismus und sozialer Ungleichheit (Vortrag)

Mit *Elisa Gerbsch* (Humangeographin) und *Paul Zschocke* (Politikwissenschaftler) 

Die Suche nach Antworten auf die Wohnungsfrage aus sozialistischer Perspektive fand mit der Wende ihr jähes Ende. In den 1990er Jahren breitete sich eine Landschaft schrumpfender Städte aus. Erst in den 2000er Jahren gelang es der politischen Ökonomie Ostdeutschlands Städten wie Dresden, Leipzig oder Jena eine neue Anziehungskraft zu verleihen. Die revitalisierten Wohnungsmärkte leben jedoch von einem dauerhaften Verdrängungsdruck. In der Folge entsteht in den ostdeutschen Städten eine Wohnungsfrage von neuem Charakter.

Die Großwohnsiedlung galt ihren Erbauern in der DDR als architektonische visionäre Umsetzung sozialistischen Wohnens und Lebens. Nach 30 Jahren Transformation erfüllt sie jedoch eine gänzlich andere Funktion im städtischen Gefüge ostdeutscher Groß- und Mittelstädte. Am Fallbeispiel Leipzig-Grünaus wird verdeutlicht, wie dieser Funktionswandel einherging mit einer Abwertung von Lebensweisen, dem Aufkommen neuer städtischer Konflikte und der Zunahme gegenwärtiger autoritär-populistischer Potentiale.

14\.00 Uhr 

Migration in Ostdeutschland (Workshop)

Mit *Nhi Le* (freie Journalistin und Bloggerin) und *Emiliano Chaimite* (Vorsitzender des Dachverbands sächsischer Migrantenorganisationen e.V.)

In einem Gespräch mit anschließender Diskussion werden die Erfahrungen unterschiedlicher Generationen von Menschen mit Migrationserfahrung in Ostdeutschland in den Blick genommen. Welche spezifischen Veränderungen brachte die Wiedervereinigung und inwiefern knüpfen aktuelle Widersprüchlichkeiten innerhalb der Debatten um Migration an diejenigen der Wendezeit an? Welche Rolle spielen die Kategorien Ost und West heute?

17\.30 Uhr: Abschlusspodium “Ostdeutschland und kein Ende? Lebensverhältnisse 30 Jahre nach der Wende”

Ausführlichere und aktuelle Informationen zum Programm: 

[https://www.facebook.com/Lost-in-Transformation-Aktuelle-Analysen-der-ostdeutschen-Gesellschaft-110480283621826/](<https://www.facebook.com/Lost-in-Transformation-Aktuelle-Analysen-der-ostdeutschen-Gesellschaft-110480283621826/>)



Eine Anmeldung ist nicht erforderlich.



