---
id: MKLNN
title: Wirklich, ich lebe in finsteren Zeiten…
start: 2019-11-27 19:00
end: 2019-11-27 21:00
locationName: Abgeordneten-Kulturbüro Franz Sodann
address: "Mariannenstraße 101, 04315 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/MKLNN/
teaser: "- eine Brecht-Lesung"
isCrawled: true
---
Mit *Mike Melzer* (RLS Sachsen)

Eine Veranstaltung der RLS Sachsen in Kooperation mit dem Abgeordnetenbüro Franz Sodann

Die titelgebende Zeile von Bertolt Brecht (1898 - 1956) scheint fast prophetisch. Diese, unsere Zeiten brauchen gerade deshalb viel mehr Streitbares, Nachdenkenswertes und Mutmachendes. Und da scheint der widersprüchliche und oft verleumdende Brecht eine reichhaltige Fundgrube seinen “Nachgeborenen” hinterlassen zu haben. Diese Schätze will das Programm heben, um die Zuhörenden an zu stacheln, zum Streiten, zum (nach-) Denken und Mut machen zur Solidarität als auch zum Kämpfen für eine gerechte Welt.

