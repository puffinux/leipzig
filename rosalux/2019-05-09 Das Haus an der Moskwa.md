---
id: 3EITQ
title: Das Haus an der Moskwa
start: '2019-05-09 19:00'
end: '2019-05-09 21:00'
locationName: Horns Erben
address: 'Arndtstraße 33, 04275 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/3EITQ/'
image: null
teaser: 'REIHE: Jour Fixe - Ein unkonventioneller Gesprächskreis'
recurring: null
isCrawled: true
---
