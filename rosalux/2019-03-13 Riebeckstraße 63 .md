---
id: GGKVA
title: 'Riebeckstraße 63 '
start: '2019-03-13 15:00'
end: '2019-03-13 18:00'
locationName: Straße
address: 'Riebeckstraße 13, 04317 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/GGKVA/'
image: null
teaser: „Verfolgung – Ausgrenzung – Verwahrung. Die ehemalige städtische Arbeitsanstalt von 1892 bis heute.“
recurring: null
isCrawled: true
---
