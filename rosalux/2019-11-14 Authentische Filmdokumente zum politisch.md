---
id: PX7MW
title: Authentische Filmdokumente zum politischen Umbruch im November/ Dezember 1989
start: 2019-11-14 18:00
end: 2019-11-14 20:00
locationName: Seminarzentrum Arbeit &amp; Leben Sachsen e.V.
address: "Löhrstraße 17, 04105 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/PX7MW/
teaser: "REIHE: Jour Fixe - Ein unkonventioneller Gesprächskreis"
isCrawled: true
---
Mit *Hans-Rüdiger Minow *(Regisseur), *Volker K*ülow (Politiker) und *Meigl Hoffmann* (Kabarettist); Moderation: *Gerd-Rüdiger Stephan* (Historiker, RLS Berlin)

In einer Filmmatinee mit Podiumsdiskussion stellen Regisseur Hans-Rüdiger Minow, Hauptdarsteller Volker Külow und Kabarettist Meigl Hoffmann die Filmdokumentation des WDR zum Leipziger Demonstrationsgeschehen und dem politischen Umbruch im November und Dezember 1989 aus dem Jahre 1990 vor.

