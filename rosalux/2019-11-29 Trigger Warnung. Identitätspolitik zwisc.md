---
id: QPT5C
title: Trigger Warnung. Identitätspolitik zwischen Abwehr, Abschottung und Allianzen
start: 2019-11-29 19:00
end: 2019-11-29 21:00
locationName: Selbstorganisierte Bibliothek Index
address: "Breite Straße/Wurzner Straße, 04315 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/QPT5C/
teaser: "REIHE: radical bookfair"
isCrawled: true
---
Mit *Saba-Nur Cheema* & *Meron Mendel* (Autor\*innen) 

Eine Veranstaltung der radical bookfair in Kooperation mit der RLS Sachsen

Identitätspolitik steckt in der Sackgasse: Empowerment wird auf Gender-Sternchen und die Vermeidung des N-Worts verkürzt. Überall sollen Minderheiten vor möglichen Verletzungen geschützt werden – in Uniseminaren, Kunst und Mode, im Netz und bei öffentlichen Events. Für alle, die Politik nicht mit eigener Betroffenheit belegen, schließt sich die Debatte. Wer mit der anspruchsvollen Pflichtlektüre nicht hinterherkommt, ist raus. Die solidarische Kritik an diesen Exzessen wird zum Dilemma in einer Zeit, in der Rechte gegen Unisextoiletten und die »Ehe für alle« hetzen – und Linke darin »Pipi fax« oder den Aufstieg von Trump begründet sehen. Zwischen Abwehr und Abschottung richtet der Band den Blick auf die Fallstricke der Identitätspolitik und sucht nach Allianzen jenseits von Schuldzuweisungen und Opferkonkurrenz.

