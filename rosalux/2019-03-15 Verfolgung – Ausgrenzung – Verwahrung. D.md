---
id: 6OGOO
title: Verfolgung – Ausgrenzung – Verwahrung. Die ehemalige städtische Arbeitsanstalt von 1892 bis heute
start: '2019-03-15 13:00'
end: '2019-03-16 16:00'
locationName: Strietzsaal
address: 'Stötteritzer Straße 26 , 04317 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/6OGOO/'
image: null
teaser: null
recurring: null
isCrawled: true
---
