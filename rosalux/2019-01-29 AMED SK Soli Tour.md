---
id: 5HA8W
title: AMED SK Soli Tour
start: '2019-01-29 19:00'
end: '2019-01-29 20:30'
locationName: UT Connewitz
address: 'Wolfgang-Heinze-Str. 12a, 04277 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/5HA8W/'
image: null
teaser: Geschichte eines Fußballvereins zwischen kurdischer Identität und türkischer Repression
recurring: null
isCrawled: true
---
