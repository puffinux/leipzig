---
id: YRUSX
title: The New Tech Worker Movement
start: '2019-06-12 19:00'
end: '2019-06-12 22:00'
locationName: Basislager Coworking Leipzig
address: 'Peterssteinweg 14, 04107 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/YRUSX/'
image: null
teaser: Lecture by Moira Weigel and Ben Tarnoff  (Logic Magazine)
recurring: null
isCrawled: true
---
