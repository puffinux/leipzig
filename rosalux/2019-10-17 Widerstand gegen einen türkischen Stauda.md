---
id: Y248V
title: Widerstand gegen einen türkischen Staudamm-Bau
start: 2019-10-17 19:00
end: 2019-10-17 21:00
locationName: Universität Leipzig
address: "Hörsaalgebäude auf dem Campus, Augustusplatz, 04109 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/Y248V/
isCrawled: true
---
Mit *Ercan Ayboga* (Bauingenieur)

Eine Kooperation der AG Kurdistan und RLS Sachsen



Der Bauingenieur Ercan Ayboga berichtet von der Initiative des "Mesopotamia Water Forums", das für die türkische Regierung Staudämme plant. Auch der Tigris, der zweitgrößte Fluss Vorderasiens, soll zum Staudammbau genutzt werden. Hierzu wird die seit mind. 8.000 Jahren dauerhaft besiedelte Stadt Hasankeyf überflutet und rund 70.000 Menschen zwangsweise evakuiert.

Während die türkische Regierung sich Investitionen aus dem Ausland verspricht hat das Projekt immense Folgen für die Ökologie und die Wirtschaft der Menschen nicht nur in der Türkei, sondern auch in Syrien und dem Irak.

