---
id: BDHRD
title: Zine-Workshop „Wem gehört die Hood?“ - Präsentation
start: '2019-06-22 11:00'
end: '2019-06-22 18:00'
locationName: Verein zur Stärkung einer guten Sache e.V.
address: 'Eisenbahnstraße 127, 04315 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/BDHRD/'
image: null
teaser: 'Präsentation: „Zufällig Osten“'
recurring: null
isCrawled: true
---
