---
id: 5JZN6
title: Mensch und Familie in der globalisierten Arbeitswelt
start: 2020-01-29 19:30
end: 2020-01-29 22:00
locationName: Passage Kinos Leipzig
address: "Hainstraße 19a, 04109 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/5JZN6/
teaser: Gespräch zum Film SORRY WE MISSED YOU
isCrawled: true
---
Mit *Martin Du*lig (Minister für Wirtschaft, Arbeit und Verkehr Sachsen), *Bernd Riexinger* (Parteivorsitzender DIE LINKE und VER.DI Gewerkschaftssekretär) und *Alexander Jorde* (Azubi zum Gesundheits- und Krankenpfleger), Moderation: Gundula Lasch (Journalistin)

Eine Veranstaltung des NFP marketing & distribution\*, Arbeit und Leben Sachsen e.V., der Gewerkschaft VER.Di und dem Kulturforum mit Unterstützung der RLS Sachsen



In der Preview-Veranstaltung des Films SORRY WE MISSED YOU von Regisseur Ken Loach werden nach dem Film in einem Podiumsgespräch zum Thema „Mensch und Familie in der globalisierten Arbeitswelt“ Martin Dulig, der sächsische Minister für Wirtschaft, Arbeit und Verkehr, Bernd Riexinger, der Vorsitzende der Partei DIE LINKE und der Gesundheits- und Krankenpflegerazubi Alexander Jorde mit Gundula Lasch über die Veränderungen in der Arbeitswelt und die Herausforderungen für unsere Gesellschaft sprechen.

Details zur Anmeldung unter [www.sachsen.rosalux.de](<http://www.sachsen.rosalux.de>).



*Diese Steuermittel werden auf Grundlage des von den Abgeordneten des Sächsischen Landtags beschlossenen Haushaltes zur Verfügung gestellt.*

