---
id: IFT6F
title: Zur Kritik geschichtsphilosophischer Konzeptionen und das Konstrukt der Gesellschaftsformationen
start: '2019-02-26 18:00'
end: '2019-02-26 19:30'
locationName: 'Rosa-Luxemburg-Stiftung Sachsen '
address: 'Harkortstraße 10, 04107 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/IFT6F/'
image: null
teaser: null
recurring: null
isCrawled: true
---
