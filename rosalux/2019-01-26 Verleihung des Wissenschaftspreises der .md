---
id: W1FCZ
title: Verleihung des Wissenschaftspreises der Rosa-Luxemburg-Stiftung Sachsen mit anschließendem Neujahrsempfang
start: '2019-01-26 14:00'
end: '2019-01-26 16:00'
locationName: 'Rosa-Luxemburg-Stiftung Sachsen '
address: 'Harkortstraße 10, 04107 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/W1FCZ/'
image: null
teaser: null
recurring: null
isCrawled: true
---
