---
id: KHX2B
title: Und wer kümmert sich um die Kohle?
start: 2019-12-11 16:00
end: 2019-12-11 19:00
locationName: Abgeordneten-Kulturbüro Franz Sodann
address: "Mariannenstraße 101, 04315 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/KHX2B/
isCrawled: true
---
Mit *Netzwerk Selbsthilfe* und *AK Netzwerk Leipzig der RLS Sachsen

*Die Teilnehmer\*innenzahl ist begrenzt. Bitte meldet euch deshalb unter akleipzig [at] rosalux-sachsen.de an.

Neben guten und innovativen Ideen in der politischen (Bildungs-)Arbeit, brauchen diese Projekte auch immer Geld. Förderanträge zu schreiben und die Finanzierung auf die Beine zu stellen, gehört nicht zu den beliebtesten Aufgaben. Mit dem Netzwerk Selbsthilfe werden wir im Workshop darüber sprechen, wie ein guter Projektförderantrag aussieht und worauf ihr bei der Finanzierung achten müsst. Es wird auf verschiedene Fördertöpfe in Leipzig und Sachsen eingegangen und deren Besonderheiten vorgestellt.



*Diese Steuermittel werden auf Grundlage des von den Abgeordneten des Sächsischen Landtags beschlossenen Haushaltes zur Verfügung gestellt.*

