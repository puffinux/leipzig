---
id: 1IK89
title: Bewahrung der natürlichen Umwelt als Grundlage einer nachhaltigen Entwicklung
start: '2019-05-24 09:00'
end: '2019-05-24 18:00'
locationName: Universität Leipzig
address: 'Augustusplatz 10, 04109 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/1IK89/'
image: null
teaser: 3. Fachkonferenz “Zusammenarbeit mit dem globalen Süden”
recurring: null
isCrawled: true
---
