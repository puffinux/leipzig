---
id: DCZ15
title: “Frühlingskinder”
start: '2019-09-26 19:00'
end: '2019-09-26 21:00'
locationName: Interim
address: 'Demmeringstraße 32, 04177 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/DCZ15/'
image: null
teaser: Im Rahmen der Ausstellung “Kein Land in Sicht für die Seenotrettung”
recurring: null
isCrawled: true
---
