---
id: 2XFTS
title: Westsahara – Menschenrechte und Entwicklungszusammenarbeit
start: 2019-11-25 18:00
end: 2019-11-25 20:00
locationName: RLS Sachsen
address: "Demmeringstraße 32, 04177 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/2XFTS/
teaser: Mit Vorführung des Films „EL Problema“ und anschließender Diskussion
isCrawled: true
---
Mit *Salama Brahim* (Generalsekretär den saharauischen Gewerkschaften) und *Mohamed Abba Badati* (Vertreter der Polisario in Sachsen), Moderation: *Wolf-Dieter Seiwert* (ZEOK e.V.)

Die Westsahara ist die letzte Kolonie Afrikas und das weltweit größte Territorium, das bis heute auf seine Dekolonisierung wartet. Das Land ist seit 1975 von Marokko besetzt. Seit 43 Jahren wird die Entwicklung eines menschenwürdigen Lebens im eigenen Land den Saharauis verwehrt. Das Bemühen der UNO um eine Durchführung eines beschlossenen Referendums war bislang erfolglos. Die europäische Politik ist durch wirtschaftlichen Interessen an Marokko und Angst von Migrant\*innen aus Afrika erpressbar. Menschenrechtsverletzungen in den besetzten Gebieten sind an der Tagesordnung.

Mit Vorführung des Films „EL Problema“ und anschließender Diskussion.

