---
id: 41UXJ
title: RosaKunstpreis-Verleihung
start: 2020-01-25 14:00
end: 2020-01-25 17:00
locationName: Galerie D21
address: "Demmeringstraße 21, 04177 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/41UXJ/
teaser: mit anschließendem Neujahrsempfang
isCrawled: true
---
Mit den *Preisträger\*innen der Gruppe tag, PS Schreiben* und *Stefanie Schröder*



Zum zweiten Mal wird der RosaKunstpreis der RLS Sachsen verliehen. Die diesjährigen Preisträger\*innen werden durch Mitglieder des Kuratoriums für den Kunstpreis gewürdigt und stellen ihre Arbeiten in einer kleinen Gesprächsrunde vor.

Nach der traditionellen Neujahrsrede besteht die Möglichkeit bei Saft und Sekt sowie kleinen Speisen ins Gespräch zu kommen.



*Diese Steuermittel werden auf Grundlage des von den Abgeordneten des Sächsischen Landtags beschlossenen Haushaltes zur Verfügung gestellt.*

