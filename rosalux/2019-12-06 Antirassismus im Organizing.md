---
id: E7EJA
title: Antirassismus im Organizing
start: 2019-12-06 13:00
end: 2019-12-06 17:00
locationName: Pöge-Haus
address: "Hedwigstraße 20, 04315 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/E7EJA/
isCrawled: true
---
Mit *Susanne Feustel* (Kulturbüro Sachsen)

Eine Veranstaltung des Solidarischen Gesundheitszentrums Leipzig e.V. und der RLS Sachsen

Anmeldung (**bitte bis spätestens 29.11.2019**) und Kontakt: [gesundheitszentrum-leipzig@riseup.net](<mailto:gesundheitszentrum-leipzig@riseup.net>)

Organizing ist der systematische Versuch, politische Handlungsfähigkeit im Alltag zu entwickeln. Dabei steht der Aufbau persönlicher Beziehungen zwischen Aktivist\*innen und Menschen im Zentrum, die politisch kaum oder wenig Erfahrungen haben. Auch wenn der Organizing-Ansatz die Transformation gesellschaftlicher Grundlagen nicht aus den Augen verliert, geht er doch davon aus, dass Veränderung nicht von der Radikalität und Kompromisslosigkeit der Gesellschaftsanalyse abhängt. Besondere Beachtung finden stattdessen die Erfahrungen, die in gemeinsamen Kämpfen gemacht werden. Sie führen Menschen zusammen, die sehr verschiedene soziale und kulturelle Hintergründe haben. Aktivist\*innen stoßen dabei immer wieder auf widersprüchliche Haltungen. 

So kann etwa der Protest gegen den Mietenwahnsinn oder gegen prekäre Beschäftigung mit Ressentiments gegen Geflüchtete einhergehen. Im Workshop wollen wir den Umgang damit diskutieren und erproben. Neben Argumentationsstrategien gegen rassistische Stereotype beschäftigen wir uns mit Möglichkeiten, Situationen einzuschätzen, Auseinandersetzungen zu führen, Position zu beziehen und Grenzen zu setzen. Als Referentin und Teamerin des Workshops konnten wir Susanne Feustel vom Kulturbüro Sachsen gewinnen.

