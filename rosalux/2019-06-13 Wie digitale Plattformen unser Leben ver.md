---
id: 1U2WI
title: Wie digitale Plattformen unser Leben verändern
start: '2019-06-13 18:00'
end: '2019-06-13 20:00'
locationName: Reclam-Museum
address: 'Kreuzstraße 12, 04103 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/1U2WI/'
image: null
teaser: 'REIHE: Jour Fixe - Ein unkonventioneller Gesprächskreis'
recurring: null
isCrawled: true
---
