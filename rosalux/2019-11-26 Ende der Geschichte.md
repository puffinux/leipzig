---
id: RELMI
title: Ende der Geschichte?
start: 2019-11-26 18:00
end: 2019-11-26 20:00
locationName: RLS Sachsen
address: "Demmeringstraße 32, 04177 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/RELMI/
teaser: "REIHE: Philosophische Dienstagsgesellschaft"
isCrawled: true
---
Mit *Dr. Peter Fischer* (Philosoph), Moderation: *Dr. Jürgen Stahl

*Francis Fukuyama argumentiert in seinem Buch „Das Ende der Geschichte“ aus dem Jahre 1992 für die These, dass ein kohärenter und zielgerichteter Verlauf der Menschheitsgeschichte letztlich in der liberalen Demokratie und in der kapitalistischen Marktwirtschaft enden wird, wobei es zwei unterschiedliche Prozesse seien, die zu diesem Resultat führten. Bereits Jahrzehnte vor Fukuyama, nämlich in den 50er und 60er Jahren des 20. Jahrhunderts, spricht Arnold Gehlen vom Zeitalter des Posthistoire. Der Vortrag diskutiert einige aktuell bedeutsame Aspekte dieser geschichtsphilosophischen Positionen.

