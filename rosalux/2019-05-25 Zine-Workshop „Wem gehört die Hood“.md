---
id: Z73RY
title: Zine-Workshop „Wem gehört die Hood?“
start: '2019-05-25 11:00'
end: '2019-05-25 18:00'
locationName: Verein zur Stärkung einer guten Sache e.V.
address: 'Eisenbahnstraße 127, 04315 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/Z73RY/'
image: null
teaser: null
recurring: null
isCrawled: true
---
