---
id: 1QOV7
title: Klimakrise und Klimapolitik als aktuelle Konflikt- und Kampffeldern
start: 2019-12-12 18:30
end: 2019-12-12 20:30
locationName: Reclam-Museum
address: "Kreuzstraße 12, 04103 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/1QOV7/
teaser: "REIHE: Jour Fixe - Ein unkonventioneller Gesprächskreis"
isCrawled: true
---
 Mit *Dr. André Leisewitz* (Biologe und Redakteur von „Z. Zeitschrift Marxistische Erneuerung“), Moderation: *Prof. Dr. Manfred Neuhaus*