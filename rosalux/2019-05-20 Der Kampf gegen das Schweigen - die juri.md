---
id: CESCE
title: Der Kampf gegen das Schweigen - die juristische Aufarbeitung der NS-Verbrechen
start: '2019-05-20 19:00'
end: '2019-05-20 21:00'
locationName: Ariowitsch-Haus
address: 'Hinrichsenstraße 14, 04105 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/CESCE/'
image: null
teaser: null
recurring: null
isCrawled: true
---
