---
id: VQHUQ
title: Türkische Kommunist*innen in Leipzig und der DDR
start: '2019-04-29 19:00'
end: '2019-04-29 21:00'
locationName: Uni Leipzig
address: 'Augustusplatz 10, 04109 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/VQHUQ/'
image: null
teaser: 'Ein bis heute unbekanntes Kapitel migrantischer Geschichte in Leipzig '
recurring: null
isCrawled: true
---
