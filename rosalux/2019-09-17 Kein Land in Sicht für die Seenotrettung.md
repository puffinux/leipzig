---
id: LV64L
title: Kein Land in Sicht für die Seenotrettung? Über die Kriminalisierung der Helfenden
start: '2019-09-17 19:00'
end: '2019-09-17 21:00'
locationName: Interim
address: 'Demmeringstraße 32, 04177 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/LV64L/'
image: null
teaser: Im Rahmen der Ausstellung “Kein Land in Sicht für die Seenotrettung”
recurring: null
isCrawled: true
---
