---
id: K4KI7
title: 'Geschichte, Gefahren, Konflikte und Krisenszenarien in der EU'
start: '2019-04-30 18:00'
end: '2019-04-30 20:00'
locationName: RLS Sachsen
address: 'Demmeringstraße 32, 04177 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/K4KI7/'
image: null
teaser: 'REIHE: Philosophische Dienstagsgesellschaft'
recurring: null
isCrawled: true
---
