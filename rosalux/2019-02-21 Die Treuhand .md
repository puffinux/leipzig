---
id: KN531
title: 'Die Treuhand '
start: '2019-02-21 19:00'
end: '2019-02-21 20:30'
locationName: Tipi im Westwerk
address: '1. OG, Karl-Heine-Straße 89, 04229 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/KN531/'
image: null
teaser: Fluch oder Segen im Osten Deutschlands?
recurring: null
isCrawled: true
---
