---
id: 4QXM5
title: Seenotrettung – was kommt danach? Über Verteilmechanismen der EU
start: 2019-10-08 19:00
end: 2019-10-08 21:00
locationName: Interim
address: "Demmeringstraße 32, 04177 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/4QXM5/
teaser: Im Rahmen der Ausstellung “Kein Land in Sicht für die Seenotrettung”
isCrawled: true
---
Eine gemeinsame Veranstaltung des Abgeordnetenbüros INTERIM und der RLS Sachsen

Mit *Clara Anne Bünger* (Equal Rights Beyond Borders), *Aktivist\*innen der Seenotrettung* (angefragt); Moderation: *Sebastian Baehr* (Journalist, Neues Deutschland)



Seit Juni 2018 irrten wiederholt Seenotrettungsschiffe wegen der Weigerung Italiens und Maltas Gerettete aufzunehmen tagelang auf dem Mittelmeer umher. Nur die Zusage anderer EU-Staaten die Zuständigkeit für die Asylverfahren der Schutzsuchenden zu übernehmen, half.

Die EU verfolgt derzeit eine Kombination aus „Hotspot Approach“ und „Relocation Ansatz“, was in der Praxis seit dem Inkrafttreten des EU-Türkei Deals in Griechenland beobachtet werden kann. In der Veranstaltung wird auf das inhaltliche Konzept der EU eingegangen und die Beteiligung europäischer Agenturen wie Frontex und des Europäischen Unterstützungsbüro für Asylfragen (EASO) aber auch des Verfassungsschutzes, der Sicherheitsüberprüfungen vornimmt, betrachtet.

Aktueller Text von Clara Anne Bünger zum Thema: “Erst Haft, dann „Cherry-Picking“? Der EU-Verteilmechanismus nach Seenotrettung” [https://verfassungsblog.de/erst-haft-dann-cherry-picking/](<https://verfassungsblog.de/erst-haft-dann-cherry-picking/>)

