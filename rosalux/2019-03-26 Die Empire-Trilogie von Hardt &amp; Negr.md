---
id: GSY66
title: Die Empire-Trilogie von Hardt &amp; Negri - Manifest des 21. Jahrhunderts?
start: '2019-03-26 18:00'
end: '2019-03-26 20:00'
locationName: RLS Sachsen
address: 'Demmeringstraße 32, 04177 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/GSY66/'
image: null
teaser: 'REIHE: Philosophische Dienstagsgesellschaft'
recurring: null
isCrawled: true
---
