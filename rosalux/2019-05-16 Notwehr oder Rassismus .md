---
id: 2CZJI
title: 'Notwehr oder Rassismus? '
start: '2019-05-16 19:00'
end: '2019-05-16 21:00'
locationName: Conne Island
address: 'Koburger Straße 3, 04277 '
link: 'https://sachsen.rosalux.de/veranstaltung/es_detail/2CZJI/'
image: null
teaser: Warum der Rechtspopulismus heute so stark ist
recurring: null
isCrawled: true
---
