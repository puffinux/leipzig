---
id: "558549228309025"
title: Video in Aktion & Social Media-Workshop 1/2 in der HALLE 14 e.V.
start: 2019-11-08 16:00
end: 2019-11-08 20:00
locationName: HALLE 14 - Zentrum für zeitgenössische Kunst
address: Spinnereistraße 7, 04179 Leipzig
link: https://www.facebook.com/events/558549228309025/
image: 74472199_905541056513366_2558017315304112128_n.jpg
teaser: Workshop in der Halle 14  Offen für alle Kinder & Jugendliche. Die Technik
  wird gestellt. Falls vorhanden, könnt ihr gern eure eigene Technik mitbring
isCrawled: true
---
Workshop in der Halle 14

Offen für alle Kinder & Jugendliche.
Die Technik wird gestellt. Falls vorhanden, könnt ihr gern eure eigene Technik mitbringen.