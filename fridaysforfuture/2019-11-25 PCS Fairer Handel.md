---
id: "2540104329546427"
title: "PCS: Fairer Handel"
start: 2019-11-25 11:15
end: 2019-11-25 14:45
address: Dittrichring 5-7, 04109 Leipzig
link: https://www.facebook.com/events/2540104329546427/
image: 75328649_439001283702021_121139011417800704_n.jpg
teaser: "Titel: Fairer Handel Referent*in: Matthias Raus (mohio e.V. Halle) Ort:
  Dittrichring 5-7, Raum 018 Zeit: 11:15-14:4  Interaktives Seminar im Rahmen
  ei"
isCrawled: true
---
Titel: Fairer Handel
Referent*in: Matthias Raus (mohio e.V. Halle)
Ort: Dittrichring 5-7, Raum 018
Zeit: 11:15-14:4

Interaktives Seminar im Rahmen eines Moduls für 
Lehramtsstudierende. Geöffnet für alle Interessierten! 
Schokolade – nicht für alle ein Zuckerschlecken: Gerade die Erzeuger des Grundstoffs Kakao arbeiten meist unter ausbeuterischen Bedingungen. Welche Missstände gibt es konkret? Wie, wo und von wem wird Schokolade überhaupt hergestellt? Wer verdient an Schokolade – und wer nicht? Wie kann der Faire Handel zu einer Verbesserung beitragen?
Um Antworten zu finden, nehmen wir die gesamte Produktionskette unter die Lupe, vom Kakaobaum bis zum Konsumenten. Die Teilnehmenden gewinnen dabei u. a. Einblicke in Anbauländer und Produktionsbedingungen, Kostenverteilung, Kriterien und Effekte des Fairen Handels sowie in Handlungsmöglichkeiten für jede(n) Einzelne(n).


Diese Veranstaltung ist teil der Public Climate School. 
Das vollständige Programm findet ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 