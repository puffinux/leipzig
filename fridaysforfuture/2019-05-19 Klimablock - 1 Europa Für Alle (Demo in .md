---
id: '479023446174475'
title: Klimablock - 1 Europa Für Alle (Demo in Leipzig)
start: '2019-05-19 10:00'
end: '2019-05-19 14:00'
locationName: null
address: 'Wilhelm-Leuschner-Platz, 04107 Leipzig'
link: 'https://www.facebook.com/events/479023446174475/'
image: 59767256_779640392436767_1939154648694784000_n.jpg
teaser: 'Europa betrifft uns alle, egal welche Herkunft wir haben, welche Sprache wir sprechen oder welche Farbe unsere Haut hat.   Durch die Klimakrise ist di'
recurring: null
isCrawled: true
---
Europa betrifft uns alle, egal welche Herkunft wir haben, welche Sprache wir sprechen oder welche Farbe unsere Haut hat. 

Durch die Klimakrise ist die Lebensgrundlage aller Menschen weltweit in Gefahr. Auch in #Europa werden die Folgen des menschgemachten #Klimawandel spüren. Seit längerer bzw. kürzerer Zeit kämpfen verschiedene Organisationen unter anderem für klimagerechtere Politik und ein klimafreundliches Europa. 

Da angesichts der anstehenden #Europawahl besonders die Europäische Klimapolitik wegweisend für die kommenden Jahre sein wird, demonstrieren BUND Leipzig, BUNDjugend Leipzig, Naturfreundejugend Sachsen, Greenpeace Leipzig und Fridays For Future Leipzig gemeinsam auf der #1EuropaFürAlle Demo im #Klimablock.

Kommt am 19. Mai 2019 mit auf die Straßen und demonstriert für #1EuropaFürAlle. 

12:30 Uhr auf dem Wilhelm-Leuschner-Platz!
Solidarisch, gegen Nationalismus und für's Klima! 

#FridaysForFuture #voteclimate #togetherforfuture