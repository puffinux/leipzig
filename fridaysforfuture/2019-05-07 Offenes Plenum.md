---
id: '335663157093563'
title: Offenes Plenum
start: '2019-05-07 18:00'
end: '2019-05-07 20:00'
locationName: Städtisches Kaufhaus
address: 'Neumarkt 9, 04109 Leipzig'
link: 'https://www.facebook.com/events/335663157093563/'
image: 58419300_769769890090484_322380636079783936_o.png
teaser: 'Beim Plenum organisieren wir alle gemeinsam die nächsten Aktionen, tauschen uns über Ideen aus und vernetzen uns untereinander.   Jede Unterstützung u'
recurring: null
isCrawled: true
---
Beim Plenum organisieren wir alle gemeinsam die nächsten Aktionen, tauschen uns über Ideen aus und vernetzen uns untereinander. 

Jede Unterstützung und alle Ideenstifter*innen sind herzlich willkommen!