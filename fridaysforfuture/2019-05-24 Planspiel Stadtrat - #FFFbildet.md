---
id: '419457762221757'
title: 'Planspiel Stadtrat - #FFFbildet'
start: '2019-05-24 10:00'
end: '2019-05-24 12:00'
locationName: null
address: Schulmuseum Leipzig
link: 'https://www.facebook.com/events/419457762221757/'
image: 60142686_783383792062427_5400812003377807360_n.jpg
teaser: 'FridaysForFuture Leipzig Bildungsveranstaltung vor dem großen europäischen Klimastreik am 24. Mai um 12:30 auf dem Simsonplatz.  Titel: "Planspiel Sta'
recurring: null
isCrawled: true
---
FridaysForFuture Leipzig Bildungsveranstaltung vor dem großen europäischen Klimastreik am 24. Mai um 12:30 auf dem Simsonplatz.

Titel: "Planspiel Stadtrat"

Für Schüler*innen ab Klasse 8.

Zeit: 10:00 - 12:00 Uhr
Ort: Schulmuseum
Referent: Jugendparlament Leipzig

Kurzbeschreibung:

Lerne die Abläufe im Stadtrat kennen und lerne wie du deine Interessen in der Kommunalpolitik auch mit Hilfe des Jugendparlamentes durchsetzen kannst! Wir freuen uns auf spannende Diskussionen zu Themen aus dem Alltag unserer Stadt.

Anmeldung per Mail unter Leipzig@FridaysForFuture.de

#FridaysForFuture #voteclimate #togetherforfuture #FFFbildet