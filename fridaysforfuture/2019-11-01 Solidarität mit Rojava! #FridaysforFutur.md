---
id: "568876027273458"
title: "Solidarität mit Rojava! #FridaysforFuture"
start: 2019-11-01 13:00
end: 2019-11-01 14:00
locationName: Leipzig Augustusplaz
address: 15  Augustus platz, 04109 Leipzig
link: https://www.facebook.com/events/568876027273458/
image: 73126392_901172653616873_4899584896118292480_n.jpg
teaser: "#Klimagerechtigkeit kann es nur geben, wenn Frieden herrscht. Die Türkei
  beschießt & besetzt völkerrechtswidrig die nordsyrische Provinz Rojava. Unser"
isCrawled: true
---
#Klimagerechtigkeit kann es nur geben, wenn Frieden herrscht. Die Türkei beschießt & besetzt völkerrechtswidrig die nordsyrische Provinz Rojava. Unsere Solidarität gilt den Menschen vor Ort, die dem türkischen #Angriff schutzlos ausgeliefert sind!

Freitag, 13 Uhr, Augustusplatz