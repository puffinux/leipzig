---
id: '2515584181787426'
title: 'Lebensmittel & Klimawandel: lokale Ernährungssyst. - #FFFbildet'
start: '2019-05-24 10:00'
end: '2019-05-24 12:00'
locationName: null
address: vor Ort in einer Schule (siehe Anmeldung)
link: 'https://www.facebook.com/events/2515584181787426/'
image: 60478929_783396725394467_6248453627791278080_n.jpg
teaser: 'FridaysForFuture Leipzig Bildungsveranstaltung vor dem großen europäischen Klimastreik am 24. Mai um 12:30 auf dem Simsonplatz.  Titel: "Lebensmittel'
recurring: null
isCrawled: true
---
FridaysForFuture Leipzig Bildungsveranstaltung vor dem großen europäischen Klimastreik am 24. Mai um 12:30 auf dem Simsonplatz.

Titel: "Lebensmittel und Klimawandel: Konzepte für eine Transformation des lokalen Ernährungssystems"

Für Schüler*innen ab Klasse 10.

Zeit: 10:00 - 12:00 Uhr
Ort: vor Ort in einer Schule (Anfragen bitte an Leipzig@FridaysForFuture.de)
Referenten: Lukas und Raphael (Helmholtz-Zentrum für Umweltforschung)

Kurzbeschreibung:

Kern des Workshops ist der Zusammenhang zwischen unserer Ernährung und dem Klimawandel. Nach einem inhaltlichen Einstieg zu globalen Gesichtspunkten möchten wir insbesondere auf die regionale und lokale Ebene eingehen. Woher bezieht eine Stadt wie Leipzig oder Berlin ihre Nahrung und wie viel Fläche,
Ressourcen, CO2-Emissionen sind damit verbunden? Vor allem aber: Wie ginge es anders? Was ist der Mehrwert kleinteiliger regionaler Landwirtschaft oder sogar Anbau innerhalb der Stadt? Was hat es auf sich mit Konzepten wie der Solidarischen Landwirtschaft, der essbaren Stadt oder Foodsharing? Mit kreativen Methoden entwickeln wir gemeinsam in Kleingruppen Konzepte für ein nachhaltigeres, klimafreundlicheres Ernährungssystem für die Stadt und vielleicht auch ganz konkret für die eigene Schule.

Anmeldung per Mail unter Leipzig@FridaysForFuture.de

#FridaysForFuture #voteclimate #togetherforfuture #FFFbildet