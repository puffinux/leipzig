---
id: '315006276110131'
title: Dark Eden - Film und Diskussion
start: '2019-07-31 18:00'
end: '2019-07-31 21:00'
locationName: Pöge-Haus
address: 'Hedwigstraße 20, 04315 Leipzig'
link: 'https://www.facebook.com/events/315006276110131/'
image: 64839871_2306685639401321_872704446507778048_n.jpg
teaser: 'Der Dokumentarfilm zeigt die Kehrseite der drittgrößten Ölindustrie der Welt auf. Die kanadische Ressourcenstadt Fort McMurray, in der Nähe der Athaba'
recurring: null
isCrawled: true
---
Der Dokumentarfilm zeigt die Kehrseite der drittgrößten Ölindustrie der Welt auf. Die kanadische Ressourcenstadt Fort McMurray, in der Nähe der Athabascan Tar Sands, hat mit der Welle an arbeitswilligen Zugezogenen aus der ganzen Welt einen unglaublichen Boom erlebt. Das Wachstum von (Ölsand-) Industrie und Kapital hat dabei die sozialen und ökologischen Bedürfnisse von Mensch und Natur vernachlässigt. Die Leipziger Regisseurin Jasmin Herold, die über mehrere Jahre vor Ort gelebt hat, zeigt die Verwicklungen von Wirtschaft und Gesellschaft auf, ohne dabei explizit mit dem Finger auf Verantwortliche zu zeigen, die es auch direkt nicht gibt. Das Ölzeitalter ist das Ergebnis einer Wachstumsgesellschaft von mehreren Generationen, die zeitnah Verantwortung übernehmen müssen. Der Film bietet tiefe und persönliche Einblicke in kanadische Gesellschaftsstrukturen dieser besonderen Region unter dem Einfluss der Ölindustrie.  

Im Anschluss findet eine offene Diskussion mit der Regisseurin und Leipziger Vertreter*innen von Umweltorganisationen (BUND, Greenpeace, Fridays For Future (angefragt) und OIKOS Leipzig (angefragt)) statt.

Film-Trailer: https://youtu.be/TjmunaPEixI 

🔥 Mittwoch, 31. Juli 2019 18:00 Uhr
🔨 Pöge-Haus, Hedwigstr. 20, 04315 Leipzig

Eine Veranstaltung der Junge GEW Sachsen/GEW Hochschulgruppe Leipzig und Referat Ökologie StuRa Leipzig in Zusammenarbeit mit der DGB Hochschulgruppe Leipzig, BUND Leipzig, Greenpeace Leipzig, Fridays For Future Leipzig (angefragt) und Oikos Leipzig (angefragt).