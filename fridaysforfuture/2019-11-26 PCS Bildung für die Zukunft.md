---
id: "506780326590493"
title: "PCS: Bildung für die Zukunft"
start: 2019-11-26 09:15
end: 2019-11-26 10:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/506780326590493/
image: 75285819_441297736805709_7281687059717160960_n.jpg
teaser: "Titel: Bildung für die Zukunft Referent*in: Otto Herz Ort: NSG S212,
  Hauptcampus Zeit: 9.15-10.45 Uhr  (Teaser folgt)  Diese Veranstaltung ist teil
  de"
isCrawled: true
---
Titel: Bildung für die Zukunft
Referent*in: Otto Herz
Ort: NSG S212, Hauptcampus
Zeit: 9.15-10.45 Uhr

(Teaser folgt)

Diese Veranstaltung ist teil der Public Climate School. 
Das vollständige Programm findet ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 