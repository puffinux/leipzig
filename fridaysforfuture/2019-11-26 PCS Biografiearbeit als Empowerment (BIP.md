---
id: "452457128788780"
title: "PCS: Biografiearbeit als Empowerment (BIPoC only)"
start: 2019-11-26 15:15
end: 2019-11-26 16:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/452457128788780/
image: 75439363_439087603693389_7403948423398817792_n.jpg
teaser: "Titel: Biografiearbeit als Empowerment (BIPoC only) Referent*in: Trang Nguyen
  Raum: Seminarraum 205, Neues Seminargebäude Uhrzeit: 15:15 - 16:45  Wer"
isCrawled: true
---
Titel: Biografiearbeit als Empowerment (BIPoC only)
Referent*in: Trang Nguyen
Raum: Seminarraum 205, Neues Seminargebäude
Uhrzeit: 15:15 - 16:45

Wer erzählt für wen? In diesem kurzweiligen Workshop wollen wir sehen welche Geschichten unter Rassismus und Migration verborgen sind und erzählen sie vor allem selbst!

Diese Veranstaltung ist Teil der Public Climate School.
Das vollständige Programm ist auf studentsforfuture.info zu finden.
Wir freuen uns auf euch!