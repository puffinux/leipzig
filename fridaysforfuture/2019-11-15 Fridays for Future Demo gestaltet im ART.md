---
id: "788026651647312"
title: Fridays for Future Demo gestaltet im ART SPACE 3
start: 2019-11-15 14:00
end: 2019-11-15 16:00
address: Fridays For Future Leipzig
link: https://www.facebook.com/events/788026651647312/
image: 73515621_905538886513583_3424408945865785344_n.jpg
teaser: Diese Demo wird das Ergebnis der beiden ART SPACE 3 zu Flashmob bzw.
  Choreographie bzw. Video in Aktion und Social Media.  Wenn ihr die Aktion mit
  ges
isCrawled: true
---
Diese Demo wird das Ergebnis der beiden ART SPACE 3 zu Flashmob bzw. Choreographie bzw. Video in Aktion und Social Media.

Wenn ihr die Aktion mit gestanten wollt, kommt einfach zu den beiden ART SPACE mit Marianne Cebulle und Marian Luft von der HALLE 14 - Zentrum für zeitgenössische Kunst.