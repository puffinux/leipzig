---
id: "1429916450517552"
title: "PCS: Biodiversitätswandel in Zeiten des Klimawandels"
start: 2019-11-26 15:15
end: 2019-11-26 16:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/1429916450517552/
image: 75636050_441558126779670_4365697637769281536_n.jpg
teaser: "Titel: Biodiversitätswandel in Zeiten des Klimawandels Referent*in: Marten
  Winter, David Eichenberg, Michael Wohlwend (iDiv) Raum: Hörsaal Kavous Seye"
isCrawled: true
---
Titel: Biodiversitätswandel in Zeiten des Klimawandels
Referent*in: Marten Winter, David Eichenberg, Michael Wohlwend (iDiv)
Raum: Hörsaal Kavous Seyed-Emami (HS17), Hörsaalgebäude
Uhrzeit: 15:15 - 16:45 Uhr


Martin Winter wird den Vortrag mit den Zusammenhängen und Unterschieden von Klimawandel und Biodiversitätswandel beginnen.
David Eichenberg wird anschließend den Biodiversitätswandels und die Problematik der Wertung und Erfassung desselben in Deutschland näher beleuchten.
Michael Wohlwend wird dies abschließend mit der Ausbreitung von invasiven Arten und deren Ursachen anhand globaler Beispiele abschließen.


Diese Veranstaltung ist Teil der Public Climate School.
Das vollständige Programm ist auf studentsforfuture.info zu finden.
Wir freuen uns auf euch!