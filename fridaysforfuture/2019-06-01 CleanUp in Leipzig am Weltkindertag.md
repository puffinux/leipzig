---
id: '423167891837428'
title: CleanUp in Leipzig am Weltkindertag
start: '2019-06-01 12:00'
end: '2019-06-01 16:00'
locationName: Musikpavillon Leipzig
address: 'Anton-Bruckner-Allee 11, 04107 Leipzig'
link: 'https://www.facebook.com/events/423167891837428/'
image: 60695489_456333538271469_896216690918948864_n.jpg
teaser: 'Zu viel Müll – verschmutztes Wasser – verschmutzte Natur?  Dagegen möchte ich etwas tun und lade alle ein, die mitmachen wollen.  Also schnappt euch e'
recurring: null
isCrawled: true
---
Zu viel Müll – verschmutztes Wasser – verschmutzte Natur?

Dagegen möchte ich etwas tun und lade alle ein, die mitmachen wollen. 
Also schnappt euch einen Snack, ein paar Freunde und kommt zum Clarapark! :) 

Die Stadtreinigung Leipzig wird uns unterstützen – sie leiht uns Greifzangen, Handschuhe und Müllsäcke und holt den gesammelten Müll mit einem Fahrzeug ab. 

Wer selbst Handschuhe hat – gerne mitbringen! Vorrat ist begrenzt. 

Der 1. Juni ist Weltkindertag – Zukunftstag!
Dem Beispiel von #fridaysforfuture folgend soll diese Aktion vor allem den jungen Menschen und der nahen und fernen Zukunft dienen. 

Unter dem Motto #tatenfuermorgen veranstaltet der Rat für Nachhaltige Entwicklung in ganz Deutschland vom 30. Mai bis 5. Juni die so genannten 
"Deutschen Aktionstage Nachhaltigkeit" 
Schaut doch mal auf der Homepage vorbei und stöbert nach weiteren Aktionen:
https://www.tatenfuermorgen.de/deutsche-aktionstage-nachhaltigkeit/

