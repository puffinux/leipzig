---
id: "785156351919121"
title: "PCS: Indigene Lebenswelten und Widerstände gegen den Klimawandel"
start: 2019-11-26 17:15
end: 2019-11-26 18:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/785156351919121/
image: 78035951_439980900270726_4273286855140573184_n.jpg
teaser: "Titel: Indigene Lebenswelten und Widerstände aus Mesoamerika gegen den
  Klimawandel Referent*in: LATINX EN LEIPZIG Ort: NSG S205, Hauptcampus Zeit:
  17:"
isCrawled: true
---
Titel: Indigene Lebenswelten und Widerstände aus Mesoamerika gegen den Klimawandel
Referent*in: LATINX EN LEIPZIG
Ort: NSG S205, Hauptcampus
Zeit: 17:15 - 18:45 

Indigene Lebenswelten und Widerstände aus Mesoamerika gegen den Klimawandel

Der Vortrag thematisiert eine gelungene Beziehung von Kultur und Umwelt am Beispiel Mesoamerikas. Sowohl kulturelle Praxen, als auch wissenschaftliche Kenntnisse ermöglichen es dort im Gleichgewicht mit der Natur zu leben. Der Bezug zur Klimakrise wird hergestellt. 

Diese Veranstaltung ist Teil der Public Climate School.
Das vollständige Programm gibt es auf studentsforfuture.info
Wir freuen uns auf euch!