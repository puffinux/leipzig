---
id: '449829289108160'
title: FridaysForFuture Leipzig - "Konsum tötet?"
start: '2019-05-31 12:30'
end: '2019-05-31 15:00'
locationName: Richard-Wagner-Platz
address: Leipzig
link: 'https://www.facebook.com/events/449829289108160/'
image: 61116325_792184544515685_7315473858037809152_n.jpg
teaser: Fridays For Future Leipzig ist auch am kommenden Freitag wieder mit einer Aktion am Start!  "Konsum tötet?"  Dieser Frage gehen wir am 31. Mai 2019 in
recurring: null
isCrawled: true
---
Fridays For Future Leipzig ist auch am kommenden Freitag wieder mit einer Aktion am Start!

"Konsum tötet?"

Dieser Frage gehen wir am 31. Mai 2019 in Leipzig nach. 
Wir treffen uns 12:30 Uhr auf dem Richard-Wagner-Platz.

Anschließend werden wir auf einer Route durch die Innenstadt verschiedene Unternehmen besuchen und uns kritisch mit deren Klima- und Umweltbilanz auseinander setzen bzw. legen. An jeder Station zeigen wir symbolisch die Folgen mangelnden Handelns seitens Politik und Wirtschaft durch mehrere DIE INs. Natürlich spielen auch Achtung von Menschenrechten und Arbeitsbedingungen eine Rolle!

Unsere Route verläuft vom Richard-Wagner-Platz über die Hainstraße, den Markt und die Petersstraße zum Wilhelm-Leuschner-Platz. Dort findet eine Abschlusskundgebung gegenüber der Deutschen Bank statt. 

Informiert eure Freund*innen und kommt am Freitag mit!
Jeder Freitag ist Klimastreik! #everydayforfuture #FridaysForFuture #climatejustice