---
id: "604770470264621"
title: "PCS: Workshop - feministischer Hiphop als Aktionsgestaltung"
start: 2019-11-26 11:15
end: 2019-11-26 12:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/604770470264621/
image: 75485934_439529823649167_6281158326281568256_n.jpg
teaser: "Titel: Workshop - feministischer Hiphop als Aktionsgestaltung Ort: tba
  Uhrzeit: 11:15 - 12:45   Tanzen als empowerment! Lasst uns alle zusammen
  groove"
isCrawled: true
---
Titel: Workshop - feministischer Hiphop als Aktionsgestaltung
Ort: tba
Uhrzeit: 11:15 - 12:45


Tanzen als empowerment! Lasst uns alle zusammen grooven und die Kontrolle über unseren Körper und was wir damit vermitteln wollen zurückerobern und entschieden einsetzten.
In dem Workshop werden ein paar Hip Hop Basics vermittelt, wir wollen zusammen tanzen und darüber reden wie Tanz als Aktionsform im Protest genutzt werden kann.
Der Kurs ist für alle Menschen offen, ob mit oder ohne Erfahrung.

Die Veranstaltung ist Teil der Public Climate School.
Das vollständige Programm findet ihr unter: studentsforfuture.info 

Wir freuen uns auf euch! 