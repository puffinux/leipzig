---
id: "533550667193289"
title: "PCS: CO2-Bepreisung: ökologische, ökonomische u. soziale Aspekte"
start: 2019-11-26 13:15
end: 2019-11-26 14:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/533550667193289/
image: 76661776_439077033694446_3492297556883406848_n.jpg
teaser: "Referent*in: Prof. Dr. Bruckner  Ort: Seminarraum 4,
  Wirtschaftswissenschaftliche Fakultät, Grimmaische Str. 12 Zeit: 13:15 - 14:45
  Uhr   *Geöffnete V"
isCrawled: true
---
Referent*in: Prof. Dr. Bruckner 
Ort: Seminarraum 4, Wirtschaftswissenschaftliche Fakultät, Grimmaische Str. 12
Zeit: 13:15 - 14:45 Uhr 

*Geöffnete Veranstaltung

Reguläre Vorlesung der Veranstaltung „Energiemanagement“ (Bachelor-Wiwi), für alle Menschen geöffnet

Diese Veranstaltung ist Teil der Public Climate School.
Das vollständige Programm findet ihr unter: studentsforfuture.info 

Wir freuen uns auf euch! 
