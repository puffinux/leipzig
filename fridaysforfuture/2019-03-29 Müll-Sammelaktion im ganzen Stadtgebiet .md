---
id: '881227152230469'
title: Müll-Sammelaktion im ganzen Stadtgebiet FridaysForFuture Leipzig
start: '2019-03-29 12:30'
end: '2019-03-29 16:00'
locationName: null
address: 04177 Leipzig
link: 'https://www.facebook.com/events/881227152230469/'
image: 55489183_752837158450424_2670476511349309440_n.jpg
teaser: 'Der Frühling kommt. Also ist es höchste Zeit für einen Frühjahrsputz!  Wir machen den Dreck weg.  Am 29.03.2019 starten 12:30 dezentral in ganz Leipzi'
recurring: null
isCrawled: true
---
Der Frühling kommt. Also ist es höchste Zeit für einen Frühjahrsputz!

Wir machen den Dreck weg.

Am 29.03.2019 starten 12:30 dezentral in ganz Leipzig Müllsammel-Trupps und bereinigen Natur und Umwelt von Allem, was da nicht hingehört!

Du willst mitmachen? Kein Problem!

Suche dir einfach eine Dreckige Ecke in deiner Umgebung und sammle den ganzen Müll. 

Der Ökolöwe - Umweltbund Leipzig e.V. hilft uns beim Müll sammeln. Genauere Infos folgen noch!

Ab 16:00 Uhr treffen wir uns mit dem Müll (!) auf dem Augustusplatz zur Abschlusskundgebung, wo die Stadtreinigung den Müll abholt:
https://www.facebook.com/events/602892266894690/