---
id: "2583817515187243"
title: "PCS: Abendpodium 1: Die Hütte brennt!"
start: 2019-11-25 19:00
end: 2019-11-25 21:00
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/2583817515187243/
image: 77053388_444667466468736_3413431211320672256_n.jpg
teaser: "Die Hütte brennt - Klimakrise, Politisches Versagen und der Kampf für eine
  andere Welt  Ort: HS3 / Isabel und Melati Wijsen      Zeit: 19.00 Uhr - 21:"
isCrawled: true
---
Die Hütte brennt - Klimakrise, Politisches Versagen und der Kampf für eine andere Welt

Ort: HS3 / Isabel und Melati Wijsen     
Zeit: 19.00 Uhr - 21:00

Während 1,4 Mio. Menschen in Deutschland beim letzten globalen Klimastreik demonstrieren, beschließt die Bundesregierung ein Klimapaket, das die Krise in keinster Weise aufhalten wird. Wie kann das Versagen erklärt werden? Und welche Schritte sollte die Klimabewegung nun gehen?

mit:

Heike Wex (Scientists for Future)
Kathrin Hartmann (Autorin "Die Grüne Lüge")
Dennis Eversberg (Kolleg Postwachstumsgesellschaften)

Diese Veranstaltung ist Teil der Public Climate School.
Das vollständige Programm gibt es auf studentsforfuture.info
Wir freuen uns auf euch!