---
id: "739873969844927"
title: "PCS: Workshop - Die Energieversorgung der Zukunft"
start: 2019-11-26 13:15
end: 2019-11-26 17:30
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/739873969844927/
image: 76720842_440603586875124_1018872903829028864_n.jpg
teaser: "Titel: Workshop - Die Energieversorgung der Zukunft Referent*in: Prof.
  Dr.-Ing. Anne Schierenbeck  Sie ist Professorin für Energiemanagement an der
  Ho"
isCrawled: true
---
Titel: Workshop - Die Energieversorgung der Zukunft
Referent*in: Prof. Dr.-Ing. Anne Schierenbeck 
Sie ist Professorin für Energiemanagement an der Hochschule Osnabrück, Geschäftsführerin eines Ingenieurbüros und politisch aktiv bei Bündnis 90/Die Grünen sowie Scientists4Future. Mehr zu der Methodik unter www.ernes.de
Ort: Seminarraum 203, Neues Seminargebäude, Universitätsstraße 1
Zeit: 13:15 - 17.30 Uhr 

Zukunftsfähige Energieversorgung mit Sektorkopplung: In dem Workshop wird von den Teilnehmer*innen ein Energie-Szenario für die Bereitstellung von Energie, für die Energienutzung und die Kopplung der Sektoren Strom, Wärme und Mobilität für Deutschland erstellt. Die Methodik wird durch ein Simulations-Werkzeug auf Excel-Basis (100prosimX) unterstützt. Sie ermöglicht so eine rasche Entwicklung solcher Szenarien. Die Ergebnisse werden transparent und können ad-hoc modifiziert werden. Bei der Erarbeitung des Energie-Szenarios wird von folgenden Leitplanken ausgegangen:

Nur mit 100% erneuerbaren Energien werden die Einhaltung der Klimaschutzziele und die Sicherung der Energieversorgung möglich. Das dafür nutzbare Angebot an natürlichen Energieströmen ist begrenzt.
Durch die Versorgung mit erneuerbaren Energien ist Versorgungssicherheit erreichbar. Die gewinnbaren Energiemengen werden jedoch unter dem heutigen Verbrauchsniveau liegen. Der Ausgleich zwischen Angebot und Nachfrage wird durch sparsameren und durch effizienteren Umgang mit Energie zu erreichen sein.
Nur noch wenige Jahre bleiben, um den notwendigen Umbau der Energieversorgung zu vollziehen. Die Spanne ist zu kurz, um noch Zeit und Mittel mit ‚Brückentechnologien‘ zu verschwenden. Erfolgsentscheidend ist ein zielgerichtetes Vorgehen. Grundlegend dafür sind realistische Vorstellungen zu den Möglichkeiten und Grenzen.

Teilnehmer*innen brauchen eigene Computer + Excel Programm herunterladen

Diese Veranstaltung ist Teil der Public Climate School.
Das vollständige Programm findet ihr unter: studentsforfuture.info 

Wir freuen uns auf euch! 