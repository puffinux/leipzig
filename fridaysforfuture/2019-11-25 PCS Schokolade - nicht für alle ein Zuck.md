---
id: "2438340969748948"
title: "PCS: Schokolade - nicht für alle ein Zuckerschlecken"
start: 2019-11-25 11:15
end: 2019-11-25 14:45
locationName: Erziehungswissenschaftliche Fakultät
address: Dittrichring 5-7, 04109 Leipzig
link: https://www.facebook.com/events/2438340969748948/
image: 74413084_440657743536375_1432953093133172736_n.jpg
teaser: "Titel: Schokolade - nicht für alle ein Zuckerschlecken Referent*in: Matthias
  Rauh Ort: Dittrichring 5-7, Raum 018 Zeit: 11.15 Uhr - 14.45 Uhr  *Geöffn"
isCrawled: true
---
Titel: Schokolade - nicht für alle ein Zuckerschlecken
Referent*in: Matthias Rauh
Ort: Dittrichring 5-7, Raum 018
Zeit: 11.15 Uhr - 14.45 Uhr

*Geöffnete Veranstaltung

Gerade die Erzeuger des Grundstoffs Kakao arbeiten meist unter ausbeuterischen Bedingungen. Welche Missstände gibt es konkret? Wie, wo und von wem wird Schokolade überhaupt hergestellt? Wer verdient an Schokolade – und wer nicht? Wie kann der Faire Handel zu einer Verbesserung beitragen?
Um Antworten zu finden, nehmen wir die gesamte Produktionskette unter die Lupe, vom Kakaobaum bis zum Konsumenten. Die Teilnehmenden gewinnen dabei u. a. Einblicke in Anbauländer und Produktionsbedingungen, Kostenverteilung, Kriterien und Effekte des Fairen Handels sowie in Handlungsmöglichkeiten für jede(n) Einzelne(n).

Diese Veranstaltung ist teil der Public Climate School. 
Das vollständige Programm findet ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 