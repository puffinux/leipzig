---
id: '524878784922790'
title: Klimastreik zur Landtagswahl in Brandenburg und Sachsen
start: '2019-08-30 11:57'
end: '2019-08-30 16:00'
locationName: Richard-Wagner-Platz
address: Leipzig
link: 'https://www.facebook.com/events/524878784922790/'
image: 68934797_839486016452204_8467792851747471360_n.jpg
teaser: "Klimastreik zur Landtagswahl in Brandenburg und Sachsen   landesweit, auch in Leipzig:  \U0001F4C6 30.08.2019  \U0001F55B 3 vor 12 Uhr (11:57)  \U0001F4CD Richard-Wagner-Plat"
recurring: null
isCrawled: true
---
Klimastreik zur Landtagswahl in Brandenburg und Sachsen


landesweit, auch in Leipzig:

📆 30.08.2019

🕛 3 vor 12 Uhr (11:57)

📍 Richard-Wagner-Platz Leipzig


Schließt euch uns an!

Aufruf:
Wir streiken in Deutschland, wie auch in Brandenburg und Sachsen, schon seit über einem halben Jahr. Doch leider scheint unser Anliegen und die Dringlichkeit des Klimaschutzes noch immer nicht genügend bei den Politiker*innen Brandenburgs und Sachsens angekommen zu sein. Und das, obwohl bei Treffen zwischen den Entscheidungsträger*innen und Fridays For Future Aktivist*innen von der Politik ein höheres Engagement im Klimaschutz zugesichert wurde. Trotzdem war es in der Folgezeit für die regierende Koalition und den Landtag in Brandenburg nicht einmal möglich, einen so symbolpolitischen Akt wie den Ausruf des brandenburgischen Klimanotstands durchzuführen. Ein wahres Armutszeugnis für die Brandenburger Politik! Leider sieht es auch in Sachsen nicht besser aus. Das wollen und dürfen wir nicht länger zulassen!
Brandenburg und Sachsen sind nämlich nicht irgendwelche Bundesländer, sondern die, in denen noch immer Braunkohletagebaue betrieben werden, um Kohle gemeinsam mit unserer Zukunft in den Kraftwerken der Lausitz zu verfeuern. Genau diese zwei Bundesländer sind es auch, die sich in die Opferrolle begeben und beim Kohleausstieg kräftig auf die Bremse treten. Auch in anderen Sektoren sind Anstrengungen seitens der Politik nicht erkennbar. Zum Beispiel beim Verkehr steigen Emissionen anstatt zu fallen!
Doch trotz der miesen Bilanz sehen wir noch Hoffnung: Die Landtagswahlen in beiden Bundesländern am 1. September 2019! Zu diesem Termin müssen wir allen Wählenden klarmachen, dass es auch bei diesen Wahlen um die Zukunft der Jugend, die Zukunft aller geht. Nur so können wir den Druck auf die politische Landschaft in Brandenburg erhöhen, um sie nicht zu einer Tagebaufolgenlandschaft verkommen lassen!
Also sei dabei und streike mit uns am Freitag vor den Wahlen, dem 30. August! An diesem Tag werden wir in ganz Sachsen und Brandenburg die Straßen mit unseren Streiks füllen. Pünktlich um #3vor12 werden wir unsere Zukunft zu dem entscheidenden Wahlkampfthema machen und die Landespolitik endlich zu einer klaren, nachhaltigen und zukunftsweisenden Haltung im Klimaschutz bewegen. Als kleine Gedankenstütze werden wir vorher sogar einen Spickzettel mit ein paar Forderungen an die Politik veröffentlichen. Und darauf könnt ihr euch verlassen: Spicker schreiben können wir!