---
id: '2507070485977800'
title: 'Biodiversität und pflanzliche Interaktion - #FFFbildet'
start: '2019-05-24 10:00'
end: '2019-05-24 11:00'
locationName: null
address: UNIVERSITÄT LEIPZIG
link: 'https://www.facebook.com/events/2507070485977800/'
image: 60338860_783353792065427_3753306748140650496_n.jpg
teaser: '+++ ANMELDUNG ERFORDERLICH +++  FridaysForFuture Leipzig Bildungsveranstaltung vor dem großen europäischen Klimastreik am 24. Mai um 12:30 auf dem Sim'
recurring: null
isCrawled: true
---
+++ ANMELDUNG ERFORDERLICH +++

FridaysForFuture Leipzig Bildungsveranstaltung vor dem großen europäischen Klimastreik am 24. Mai um 12:30 auf dem Simsonplatz.

Für Klasse 6 - 9.

Titel: "Biodiversität und pflanzliche Interaktion"

Zeit: 10:00 - 11:00 Uhr
Ort: Universität Leipzig
Referent: Christian Ristok

Kurzbeschreibung:

Vor Kurzem warnte der Weltbiodiversitätsrat (IPBES) in einer bisher noch nie dagewesenen Deutlichkeit: „Menschliche Eingriffe haben die Natur inzwischen fast rund um den Globus erheblich verändert. Die überwiegende Mehrheit der Indikatoren, die Aufschluss über den Zustand der Ökosysteme und der biologischen Vielfalt geben, verschlechtern sich rasch.“
Doch wie funktioniert das Überhaupt? Biodiversität? Christian Ristok, Doktorand für Molekulare Interaktionsökologie (iDiv) möchte euch verschiedene Aspekte der Biodiversität am Fallbespiel pflanzlicher Interaktionen näherbringen. Euer Wissen könnt ihr in einem anschließenden Quiz überprüfen.

Anmeldung unter Leipzig@FridaysForFuture.de

#FridaysForFuture #voteclimate #togetherforfuture #FFFbildet