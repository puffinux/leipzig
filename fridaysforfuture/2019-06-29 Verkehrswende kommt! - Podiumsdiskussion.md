---
id: '399448887444862'
title: Verkehrswende kommt?! - Podiumsdiskussion mit den Ratsfraktionen
start: '2019-06-29 14:00'
end: '2019-06-29 16:00'
locationName: null
address: Leipzig Hauptbahnhof
link: 'https://www.facebook.com/events/399448887444862/'
image: 64432998_2744183708929809_3070742754389655552_n.jpg
teaser: 'Im April 2017 beschloss die Ratsversammlung 3 Prüfaufträge für eine Verkehrslösung vor dem Hauptbahnhof, bei der mindestens zwischen Hauptbahnhof und'
recurring: null
isCrawled: true
---
Im April 2017 beschloss die Ratsversammlung 3 Prüfaufträge für eine Verkehrslösung vor dem Hauptbahnhof, bei der mindestens zwischen Hauptbahnhof und Straßenbahnhaltestelle kein Kfz-Verkehr mehr den Fußverkehr und ÖPNV behindert. Hierzu sollte sowohl ein Trog (CDU-Antrag) als auch eine geänderte Verkehrsführung (Grüne) zum gewünschten Ziel führen. Die Ergebnisse der technischen Machbarkeit hätten im III. Quartal 2017 vorliegen sollen, sodass im Jahr 2019 eine
Beratung und Beschlussfassung des Verkehrskonzeptes möglich ist.

Fridays For Future Leipzig und der ADFC Leipzig möchten das Thema Klimaschutz und Verkehrswende mit den Ratsfraktionen u.a. an diesem Beispiel und Ort diskutieren. 