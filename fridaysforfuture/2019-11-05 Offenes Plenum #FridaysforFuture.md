---
id: "427886157872792"
title: "Offenes Plenum #FridaysforFuture"
start: 2019-11-05 18:00
end: 2019-11-05 20:00
address: Universität Leipzig, Seminargebäude Augustusplatz, Raum 302
link: https://www.facebook.com/events/427886157872792/
image: 73171864_904679999932805_6659833395062767616_n.jpg
teaser: Wie jede Woche treffen wir uns auch diesen Dienstag zum offenen Plenum um die
  anstehenden Aktionen zu beraten und uns gegenseitig zu vernetzen!  Ihr s
isCrawled: true
---
Wie jede Woche treffen wir uns auch diesen Dienstag zum offenen Plenum um die anstehenden Aktionen zu beraten und uns gegenseitig zu vernetzen!

Ihr seid alle herzlich eingeladen! 

Dienstag, 05.11.
18:00 Uhr 
Seminargebäude Uni Leipzig, Raum 302