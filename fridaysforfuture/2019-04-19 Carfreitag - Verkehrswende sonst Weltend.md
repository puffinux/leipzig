---
id: '851711291847366'
title: Carfreitag - Verkehrswende sonst Weltende
start: '2019-04-19 12:30'
end: '2019-04-19 15:00'
locationName: Richard-Wagner-Platz
address: Leipzig
link: 'https://www.facebook.com/events/851711291847366/'
image: 57402264_766721513728655_6118628126621696000_n.jpg
teaser: 'Der Klimawandel existiert unabhängig von Feiertagen, so auch wir!  Daher gehen wir auch diesen Freitag wieder auf die Straße: Unter dem Motto "Verkehr'
recurring: null
isCrawled: true
---
Der Klimawandel existiert unabhängig von Feiertagen, so auch wir!

Daher gehen wir auch diesen Freitag wieder auf die Straße: Unter dem Motto "Verkehrswende sonst Weltende" wollen wir anprangern, dass in Deutschland immer noch knapp 20% der Treibhausgasemissionen aus dem Verkehrssektor stammen.

Streikt mit! 
Freitag, 19.04.2019, 12.30 Uhr
Route: Richard-Wagner-Platz, Markt, Grimmaische Straße, Augustusplatz, Wilhelm-Leuschner-Platz