---
id: "582572289172325"
title: "PCS: Hamburger Gitter, Filmvorführung mit Diskussion"
start: 2019-11-25 17:15
end: 2019-11-25 18:45
locationName: mephisto 97.6
address: Universitätsstr. 3, 04109 Leipzig
link: https://www.facebook.com/events/582572289172325/
image: 75588111_441280830140733_7180581167337308160_n.jpg
teaser: "Titel: Hamburger Gitter, Filmvorführung mit anschließender Diskussionen
  Referent*in: Polynja von Knut Spangenberg (Regie) Ort: Mephisto,
  Hörsaalgebäud"
isCrawled: true
---
Titel: Hamburger Gitter, Filmvorführung mit anschließender Diskussionen
Referent*in: Polynja von Knut Spangenberg (Regie)
Ort: Mephisto, Hörsaalgebäude, Hauptcampus
Zeit: 17:15-18:45 Uhr

170 Ermittler arbeiten an hunderten Verfahren gegen militante Demonstranten und Menschen, die sich an Ausschreitungen und Plünderungen beteiligten. Harte Strafen wurden gefordert und in bisher über 40 Fällen auch verhängt. Der Staat verlor im Sommer 2017 die Kontrolle in Hamburg und versucht sie nun zurück zu gewinnen.

Die Dokumentation konzentriert sich auf den Umgang mit den Protesten und die staatliche Sicherheitspolitik. Versammlungsfreiheit, Bewegungsfreiheit und Pressefreiheit spielen in den Betrachtungen eine ebenso große Rolle wie die Veränderung der polizeilichen Strategien. Konnten während der Proteste Grundrechte außer Kraft gesetzt werden? Gibt es in der Judikative und Legislative Akteure und Überzeugungen, welche auf gewaltsame Proteste anders reagieren als in der Vergangenheit? Welche Methoden der „Ausnahmesituation G20"" in Hamburg könnten zur Normalität werden?

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findest du auf studentsforfuture.info/leipzig
  
 Wir freuen uns auf euch!