---
id: '2290096774537077'
title: 'Climate Warriors (Filmvorführung) - #FFFbildet'
start: '2019-05-24 10:00'
end: '2019-05-24 12:00'
locationName: HTWK Leipzig
address: 'Karl-Liebknecht-Str. 132, 04277 Leipzig'
link: 'https://www.facebook.com/events/2290096774537077/'
image: 60529723_783393168728156_3360333746115969024_n.jpg
teaser: 'FridaysForFuture Leipzig Bildungsveranstaltung vor dem großen europäischen Klimastreik am 24. Mai um 12:30 auf dem Simsonplatz.  Titel: "Climate Warri'
recurring: null
isCrawled: true
---
FridaysForFuture Leipzig Bildungsveranstaltung vor dem großen europäischen Klimastreik am 24. Mai um 12:30 auf dem Simsonplatz.

Titel: "Climate Warriors (Filmvorführung)"

Für alle Schüler*innen (Englischkentnisse erforderlich).

Zeit: 10:00 - 12:00 Uhr
Ort: HTWK
Referent: -

Kurzbeschreibung:

100 Prozent erneuerbare Energie – das ist technisch längst möglich. Doch die weltweite Energiewende stockt, denn mit ihr versiegen die Geldströme von Kohle, Gas und Erdöl. Wie können wir der Gier der Energiekonzerne trotzen und den Blick auf die Zukunft des Planeten richten? Eine globale Veränderung kann nur durch eine Bewegung von unten entstehen! In seinem neuen Dokumentarfilm „Climate Warriors“ verbindet Vordenker Carl-A. Fechner die stärksten Szenen aus seiner Erfolgs-Doku „Power to Change“ mit neu entdeckten mitreißenden Geschichten von Klimakriegern aus Deutschland und den USA. Darunter so
unterschiedliche Persönlichkeiten wie der junge Hip-Hop-Künstler Xiuhtezcatl Martinez, Youtuberin Joylette-Portlock oder Hollywood-Actionstar Arnold Schwarzenegger. Sie alle kämpfen für das gleiche Ziel: eine saubere, gerechte und sichere Welt durch erneuerbare Energien.
„Climate Warriors“ zeigt, wie die Energiewende tatsächlich gelingen kann – und zwar weltweit. Der Dokumentarfilm von Carl-A. Fechner gibt den Menschen eine Stimme, die unermüdlich für eine nachhaltige und gerechte Zukunft kämpfen. Sie glauben an die Möglichkeit einer Energie-Revolution, wenn sich jeder Einzelne engagiert. Ein ermutigendes Plädoyer für Frieden und soziale Gerechtigkeit.
https://www.wfilm.de/climate-warriors/

Anmeldung per Mail unter Leipzig@FridaysForFuture.de

#FridaysForFuture #voteclimate #togetherforfuture #FFFbildet