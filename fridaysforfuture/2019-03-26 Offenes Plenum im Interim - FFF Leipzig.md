---
id: '302640583743413'
title: Offenes Plenum im Interim - FFF Leipzig
start: '2019-03-26 18:00'
end: '2019-03-26 20:00'
locationName: Interim
address: 'Demmeringstraße 32, 04177 Leipzig'
link: 'https://www.facebook.com/events/302640583743413/'
image: 54424858_752832338450906_1035782331807301632_n.jpg
teaser: 'Es ist bald wieder Dienstag, also ist auch wieder Plenum!  Dienstag, 26.03.2019 18:00 Uhr Interim (Demmeringstraße 32 am Lindenauer Markt)  Wir laden'
recurring: null
isCrawled: true
---
Es ist bald wieder Dienstag, also ist auch wieder Plenum!

Dienstag, 26.03.2019
18:00 Uhr
Interim (Demmeringstraße 32 am Lindenauer Markt)

Wir laden euch alle herzlich ein, Teil #FridaysForFuture Leipzig Bewegung zu werden. Kommt zum Plenum, bringt eure Ideen ein und wirkt mit in der FFF Orgsgruppe Leipzig.