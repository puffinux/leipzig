---
id: '2377469908965121'
title: Müll-Sammelaktion im Clarapark
start: '2019-03-01 12:00'
end: '2019-03-01 15:00'
locationName: Sachsenbrücke
address: 'Anton-Bruckner-Allee, 04107 Leipzig'
link: 'https://www.facebook.com/events/2377469908965121/'
image: 53110593_739070109827129_27538475603460096_n.jpg
teaser: 'Auch in den Ferien ist FridaysForFuture Leipzig aktiv und kommenden Freitag treffen wir uns 12:00 Uhr an der Sachsenbrück im Clara-Zetkin-Park um eine'
recurring: null
isCrawled: true
---
Auch in den Ferien ist FridaysForFuture Leipzig aktiv und kommenden Freitag treffen wir uns 12:00 Uhr an der Sachsenbrück im Clara-Zetkin-Park um einen Frühjahrsputz zu veranstalten und den ganzen Müll aufzusammeln.

Klima- und Umweltschutz ist Handarbeit und wir fangen vor unserer Tür an!

Kommt zu uns und unterstützt uns. Die Ausrede mit dem "Unterricht verpassen" zählt eh nicht, es sind Ferien! ;)