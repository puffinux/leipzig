---
id: '2106339946146727'
title: Europaweiter Klimastreik
start: '2019-05-24 12:30'
end: '2019-05-24 16:00'
locationName: null
address: 'Simsonplatz, 04107 Leipzig'
link: 'https://www.facebook.com/events/2106339946146727/'
image: 59476522_776556752745131_8064247920867672064_o.png
teaser: 'Wir machen die Europawahl zur Klimawahl!   Deshalb geht Fridays For Future europaweit am Freitag, den 24.05.2019, zwei Tage vor der Europawahl, auf di'
recurring: null
isCrawled: true
---
Wir machen die Europawahl zur Klimawahl! 

Deshalb geht Fridays For Future europaweit am Freitag, den 24.05.2019, zwei Tage vor der Europawahl, auf die Straße. 

Nähere Infos zum Streik in Leipzig und zu Veranstaltungen innerhalb dieser Woche folgen.