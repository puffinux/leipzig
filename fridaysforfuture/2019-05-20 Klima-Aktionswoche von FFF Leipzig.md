---
id: '1350269188447786'
title: Klima-Aktionswoche von FFF Leipzig
start: '2019-05-20 00:00'
end: '2019-05-24 23:59'
locationName: null
address: Leipzig
link: 'https://www.facebook.com/events/1350269188447786/'
image: 59900111_779254985808641_3658454963798933504_o.png
teaser: 'In der Woche vor dem großen europaweiten #Klimastreik am 24.05. veranstaltet Fridays For Future Leipzig eine Klima-Aktionswoche zusammen mit anderen #'
recurring: null
isCrawled: true
---
In der Woche vor dem großen europaweiten #Klimastreik am 24.05. veranstaltet Fridays For Future Leipzig eine Klima-Aktionswoche zusammen mit anderen #Klima- und #Umweltorganisationen.

Hier folgt zeitnah das Programm für die gesammte Woche. Schreibt uns eure Fragen und Anregungen. Wir suchen auch noch Locations und Dozent*innen für potentielle Veranstaltungen.