---
id: "400652627284486"
title: "Demo: Klimanotstand für Leipzig"
start: 2019-10-16 13:00
end: 2019-10-16 16:00
locationName: BUND Leipzig
address: Bernhard-Göring-Straße 152, 04277 Leipzig
link: https://www.facebook.com/events/400652627284486/
image: 73446259_2783263835046679_7922788955316027392_n.jpg
teaser: Trotz vieler Demonstrationen, einer Petition und öffentlicher Aufrufe ist
  bisher noch nicht der Klimanotstand für Leipzig ausgerufen worden.  Deswegen
isCrawled: true
---
Trotz vieler Demonstrationen, einer Petition und öffentlicher Aufrufe ist bisher noch nicht der Klimanotstand für Leipzig ausgerufen worden.

Deswegen laden wir Sie ein, am 16.10.2019 um 13 Uhr gemeinsam mit Fridays For Future vor dem Haupteingang des Neuen Rathauses zu demonstrieren.

Hintergrund für diesen Termin ist, dass der Stadtrat an diesem Tag eine mögliche Sitzung nicht wahrnimmt.



Foto: © Thomas Puschmann