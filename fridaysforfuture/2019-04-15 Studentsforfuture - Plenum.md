---
id: '343687709604665'
title: Studentsforfuture - Plenum
start: '2019-04-15 17:00'
end: '2019-04-15 19:00'
locationName: UNIVERSITÄT LEIPZIG
address: 'Augustusplatz 10, 04109 Leipzig'
link: 'https://www.facebook.com/events/343687709604665/'
image: 56835750_762093284191478_428025230854717440_o.png
teaser: 'Der Klimawandel wartet nicht bis dein Bachelor fertig ist!  Untätige Regierungen, ein wachstumsabhängiges zerstörerisches Wirtschaftssystem... die Kli'
recurring: null
isCrawled: true
---
Der Klimawandel wartet nicht bis dein Bachelor fertig ist!

Untätige Regierungen, ein wachstumsabhängiges zerstörerisches Wirtschaftssystem... die Klimakatastrophe kommt auf uns zu, während die Klimakrise weltweit schon längst begonnen hat und die Lebensgrundlagen von Menschen zerstört. Unsere Zukunft ist bedroht.

Dem stellen wir uns entgegen. Mit Fridays for Future ist in den letzten Monaten eine globale Schüler*innen-Bewegung für Klimagerechtigkeit entstanden, wir fordern entschlossenes Handeln und das Recht auf Zukunft für Alle ein. Viele haben sich uns inzwischen angeschlossen, wie Scientists for Future, Parents for Future, Artists for Future...

Das ist der richtige Weg! Denn die Klimakrise lässt sich nur gemeinsam aufhalten. Deshalb laden wir euch Leipziger Studierende ein, mit uns zu diskutieren und euch zu organisieren. Kommt vorbei!