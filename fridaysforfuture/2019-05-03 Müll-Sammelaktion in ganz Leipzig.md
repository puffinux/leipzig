---
id: '601136607032324'
title: Müll-Sammelaktion in ganz Leipzig
start: '2019-05-03 12:30'
end: '2019-05-03 15:00'
locationName: null
address: Leipzig
link: 'https://www.facebook.com/events/601136607032324/'
image: 59339687_775593119508161_8237596546145714176_n.jpg
teaser: 'Wir treffen uns am Freitag, 03.05. zum gemeinsamen Müllsammeln 12:30 Uhr an den vier Startpunkten:  - Spielplatz Connewitz (am Kreuz) - Festwiese (Els'
recurring: null
isCrawled: true
---
Wir treffen uns am Freitag, 03.05. zum gemeinsamen Müllsammeln 12:30 Uhr an den vier Startpunkten:

- Spielplatz Connewitz (am Kreuz)
- Festwiese (Elsterflutbecken)
- Lindenauer Markt
- Lene-Voigt-Park (Beachvolleyballplatz)

Ihr könnt aber auch gern selbst Müll sammeln und diesen 16 Uhr zur Abschlusskundgebung auf dem Augustusplatz bringen. 
https://www.facebook.com/events/2247207862159204/

Müll-Säcke und Greifer bringen wir mit! Wir freuen uns über eure Unterstützung!