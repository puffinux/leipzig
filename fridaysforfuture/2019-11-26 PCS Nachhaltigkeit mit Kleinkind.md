---
id: "2516049322005550"
title: "PCS: Nachhaltigkeit mit Kleinkind"
start: 2019-11-26 15:15
end: 2019-11-26 16:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/2516049322005550/
image: 75394806_439108460357970_3751045009772642304_n.jpg
teaser: "Titel: Nachhaltigkeit mit Kleinkind Referent*in: Charlotte Gümbel Raum:
  Seminarraum 13, Wirtschaftsfakultät Grimmaische Str. 12 Uhrzeit: 15:15 -16:45"
isCrawled: true
---
Titel: Nachhaltigkeit mit Kleinkind
Referent*in: Charlotte Gümbel
Raum: Seminarraum 13, Wirtschaftsfakultät
Grimmaische Str. 12
Uhrzeit: 15:15 -16:45

Workshop: Tipps bzgl. Möglichkeiten der nachhaltigen Erstausstattung für Eltern

Wenn man bei dm oder Rossmann in die Babyabteilung schaut, scheint es einem kaum möglich, einen nachhaltigen Lebensstil und ein Baby zu vereinbaren. Bis zum Trockenwerden verbrauchen die meisten Kinder etwa fünf- bis sechstausend Wegwerfwindeln. Hinzu kommen noch Feuchttücher, Stilleinlagen und vieles mehr. Dabei werden unsere Kinder den Klimawandel noch deutlich stärker zu spüren bekommen als unsere Generation, wenn sich nichts verändert. In meiner Schwangerschaft habe ich nachAlternativen gesucht und habe mir dadurch nicht nur jede Menge Müll, sondern auch Geldgespart. Von der Babyerstausstattung über Wickeln mit Stoffwindeln bis hin zur Methode Windelfrei gebe ich euch in diesem Workshop einen Überblick über Nachhaltigkeit mit Kleinkind und meine Erfahrungen damit.

Diese Veranstaltung ist Teil der Public Climate School.
Das vollständige Programm ist auf studentsforfuture.info zu finden.
Wir freuen uns auf euch!