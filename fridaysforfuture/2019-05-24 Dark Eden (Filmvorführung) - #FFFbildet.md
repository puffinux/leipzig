---
id: '306090313619710'
title: 'Dark Eden (Filmvorführung) - #FFFbildet'
start: '2019-05-24 10:00'
end: '2019-05-24 12:00'
locationName: Kinobar Prager Frühling
address: 'Bernhard-Göring-Straße 152, 04277 Leipzig'
link: 'https://www.facebook.com/events/306090313619710/'
image: 60250941_783394488728024_8930433680761946112_n.jpg
teaser: 'FridaysForFuture Leipzig Bildungsveranstaltung vor dem großen europäischen Klimastreik am 24. Mai um 12:30 auf dem Simsonplatz.  Titel: "Biodiversität'
recurring: null
isCrawled: true
---
FridaysForFuture Leipzig Bildungsveranstaltung vor dem großen europäischen Klimastreik am 24. Mai um 12:30 auf dem Simsonplatz.

Titel: "Biodiversität und Ökosysteme am Beispiel von subtropischen, chinesischen Wäldern"

Für Schüler*innen ab Klasse 8.

Zeit: 10:00 - 12:00 Uhr
Ort: Kinobar Prager Frühling
Referent: -

Kurzbeschreibung:

Im kanadischen Fort McMurray liegt eines der größten und letzten Ölvorkommen unseres Planeten. Wie magisch zieht das „schwarze Gold“ Menschen aus aller Welt an. Denn mit dem Ölsand lässt sich so viel Geld verdienen wie nirgend woanders. Doch der Preis ist hoch: Die aufwändige Gewinnung des Öls aus
dem Teersand setzt lebensgefährliche Stoffe frei, die Natur, Tiere und Menschen vergiften. Alles andere also als ein Paradies! Ausgerechnet an diesem verlorenen Ort findet Regisseurin Jasmin Herold die große Liebe, ihren späteren Co-Regisseur Michael Beamish. Doch als Michael schwer erkrankt, sind die beiden plötzlich unmittelbar betroffen. Ihr eigener Albtraum beginnt.
„Dark Eden“ ist ein existenzielles Drama über Segen und Fluch der Erdölgewinnung. Jasmin Herold und Michael Beamish erleben hautnah große Hoffnungen, zerplatzte Träume und eines der größten Umweltverbrechen unserer Zeit. Ihr sehr persönlicher Dokumentarfilm wurde auf dem Filmfestival Braunschweig mit dem Green Horizons Award als bester Film zum Thema Nachhaltigkeit und mit dem Artistic Vision Award auf dem Big Sky Dokumentarfilmfestival in den USA ausgezeichnet.
https://www.wfilm.de/dark-eden/

Anmeldung per Mail unter Leipzig@FridaysForFuture.de

#FridaysForFuture #voteclimate #togetherforfuture #FFFbildet