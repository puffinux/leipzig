---
id: "3223770167697865"
title: "PCS: Der Einfluss von Netzwerken für Klimabildung und BNE"
start: 2019-11-26 13:15
end: 2019-11-26 14:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/3223770167697865/
image: 76775119_439075273694622_2029895324700508160_n.jpg
teaser: "Titel: Der Einfluss von Netzwerken für Klimabildung und BNE Referent*in: Dr.
  Nina Kollek  Ort: Hörsaal Joël Imbangola (HS 10), Hörsaalgebäude, Augustu"
isCrawled: true
---
Titel: Der Einfluss von Netzwerken für Klimabildung und BNE
Referent*in: Dr. Nina Kollek 
Ort: Hörsaal Joël Imbangola (HS 10), Hörsaalgebäude, Augustusplatz 10 
Uhrzeit: 13:15 - 14:45 Uhr 


Der Vortrag befasst sich mit dem Einfluss von Netzwerken für Bildung für nachhaltige Entwicklung (BNE) und Klimabildung. Dabei werden unterschiedliche Ebenen im politischen System fokussiert. Auf der regionalen und nationalstaatlichen Ebene wird gezeigt, wie BNE auf der nationalstaatlichen und regionalen Ebene umgesetzt wurde und welchen Einfluss Netzwerke dabei spielten. Es wird u.a. demonstriert, dass Schulen bei der Implementierung bisher kaum eine Rolle bei der Implementierung von BNE in Deutschland spielen. Auf der globalen Ebene werden Einflussnahmen im Zuge der bildungsspezifischen Verhandlungen im Rahmen der Klimarahmenkonvention der Vereinten Nationen (UNFCCC) untersucht. Insbesondere wird gezeigt, wie Sekretariate die Klimaverhandlungen beeinflussen.

Die Veranstaltung ist Teil der Public Climate School. 
Das vollständige Programm findet ihr unter: studentsforfuture.info 

Wir freuen uns auf euch! :) 