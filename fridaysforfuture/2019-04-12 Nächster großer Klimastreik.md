---
id: '596421324204728'
title: Nächster großer Klimastreik
start: '2019-04-12 12:30'
end: '2019-04-12 16:00'
locationName: null
address: Fridays For Future Leipzig
link: 'https://www.facebook.com/events/596421324204728/'
image: 56567268_761567597577380_8675916177953783808_o.png
teaser: 'Nach über 2500 Teilnehmenden am 15.03.2019 wollen wir noch mehr Leute mobilisieren! Mobilisieren, um gegen den Klimawandel und für mehr Umwelt- und Kl'
recurring: null
isCrawled: true
---
Nach über 2500 Teilnehmenden am 15.03.2019 wollen wir noch mehr Leute mobilisieren! Mobilisieren, um gegen den Klimawandel und für mehr Umwelt- und Klimaschutz auf die Straße zu gehen!

Start: 12.30 Uhr 

Unsere Route:
Richard-Wagner-Platz
Ring und Goethestraße
Augustusplatz
Grimmaische Straße und Wilhelm-Leuschner-Platz
Simsonplatz

voraussichtliches Ende: 16 Uhr

Bei Fragen oder anderen Anliegen bitte uns per PN oder unter leipzig@fridaysforfuture.de kontaktieren.