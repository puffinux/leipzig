---
id: "494192408105687"
title: "PCS: Diskussion über nachhaltige sozio-ökonomische Systeme"
start: 2019-11-26 17:15
end: 2019-11-26 18:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/494192408105687/
image: 75594646_439109660357850_491039212905168896_n.jpg
teaser: "Titel: Diskussion über alternative nachhaltige sozio-ökonomische
  gesellschaftliche Systeme Referent*in: Alexander Ohligschläger Raum:
  Seminarraum 114,"
isCrawled: true
---
Titel: Diskussion über alternative nachhaltige sozio-ökonomische gesellschaftliche Systeme
Referent*in: Alexander Ohligschläger
Raum: Seminarraum 114, Neues Seminargebäude
Uhrzeit: 17:15 - 18:45

Sind Kapitalismus oder Sozialismus bzw. Kommunismus die einzigen denkbaren Systeme, mit denen wir unsere Gesellschaft gestalten können? Können diese existierende Ideologien angepasst werden, um eine nachhaltige Zukunft zu erreichen? Oder ist es sinnvoller, das Wissen und die Werkzeuge unseres Zeitalters einzusetzen, um ein neues  nachhaltigeres sozio-ökonomisches gesellschaftliches System aufzubauen?  

Das Ziel dieser Veranstaltung ist es, darüber zu diskutieren, wie unseres Gesellschaftssystem aufgebaut ist, wie ein nachhaltiges sozio-ökonomisches gesellschaftliches system aussehen könnte und wie wir dahin gelangen können.

Diese Veranstaltung ist Teil der Public Climate School.
Das vollständige Programm ist auf studentsforfuture.info zu finden.
Wir freuen uns auf euch!