---
id: "990206918045453"
title: "PCS: Straßenbahn und Busfahrer*innen als Klimaretter?"
start: 2019-11-26 11:15
end: 2019-11-26 12:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/990206918045453/
image: 76651712_439974680271348_8355880937614999552_n.jpg
teaser: "Titel: Straßenbahn und Busfahrer*innen als Klimaretter?  Warum wir auf den
  ÖPNV setzen müssen!  Referent*innen: Christopher Scymula (Straßenbahnfahrer"
isCrawled: true
---
Titel: Straßenbahn und Busfahrer*innen als Klimaretter? 
Warum wir auf den ÖPNV setzen müssen! 
Referent*innen: Christopher Scymula (Straßenbahnfahrer der LVB und Geschäftsführer der ver.di Jugend (SAT), Student der TU Dresden)
Ort: Pua Lay Peng /HS 8
Zeit: 11:15 - 12:45 

Warum brauchen wir einen guten ÖPNV um die Klimakrise zu bewältigen?

Diese Veranstaltung ist Teil der Public Climate School.
Das vollständige Programm gibt es auf studentsforfuture.info
Wir freuen uns auf euch!