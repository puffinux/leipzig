---
id: '636117893461132'
title: Sprayen für den Welt Mädchen*tag!
start: '2019-10-09 15:00'
end: '2019-10-09 18:00'
locationName: HALLE 14 - Zentrum für zeitgenössische Kunst
address: 'Spinnereistraße 7, 04179 Leipzig'
link: 'https://www.facebook.com/events/636117893461132/'
image: 71538382_2677600788931254_1739041316236427264_o.jpg
teaser: 'Sprühen für den Welt Mädchen*tag!  Halle 14 // ART SPACE #2  Wir entwerfen Stencils mit unseren eigenen Motiven zu Feminismus und Girl* Power. Die ent'
recurring: null
isCrawled: true
---
Sprühen für den Welt Mädchen*tag!

Halle 14 // ART SPACE #2

Wir entwerfen Stencils mit unseren eigenen Motiven zu Feminismus und Girl* Power.
Die entworfenen Stencils und Transpis werden direkt am 11.10  auf der Demo "Girls for Future" eingesetzt. (Kreide) Spray vorhanden, gerne T-Shirts und andere Stoffe mitbringen. 