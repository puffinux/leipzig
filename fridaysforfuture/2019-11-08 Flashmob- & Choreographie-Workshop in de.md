---
id: "2500287793359890"
title: Flashmob- & Choreographie-Workshop 1/2 in der HALLE 14 e.V.
start: 2019-11-08 16:00
end: 2019-11-08 20:00
locationName: HALLE 14 - Zentrum für zeitgenössische Kunst
address: Spinnereistraße 7, 04179 Leipzig
link: https://www.facebook.com/events/2500287793359890/
image: 73153816_905527989848006_3990218531462447104_n.jpg
teaser: Workshop in der Halle 14  Bringt bequeme Kleidung mit! Offen für alle Kinder &
  Jugendliche!
isCrawled: true
---
Workshop in der Halle 14

Bringt bequeme Kleidung mit!
Offen für alle Kinder & Jugendliche!