---
id: '2742484415767991'
title: 'Klimawandel = Handlungsbedarf: Forum f. Diskussionen - FFFbildet'
start: '2019-05-24 09:30'
end: '2019-05-24 12:00'
locationName: null
address: HTWK Leipzig
link: 'https://www.facebook.com/events/2742484415767991/'
image: 59972995_783377338729739_3277251074108424192_n.jpg
teaser: '+++ ANMELDUNG ERFORDERLICH +++  FridaysForFuture Leipzig Bildungsveranstaltung vor dem großen europäischen Klimastreik am 24. Mai um 12:30 auf dem Sim'
recurring: null
isCrawled: true
---
+++ ANMELDUNG ERFORDERLICH +++

FridaysForFuture Leipzig Bildungsveranstaltung vor dem großen europäischen Klimastreik am 24. Mai um 12:30 auf dem Simsonplatz.

Titel: "Klimawandel = Handlungsbedarf: Forum für Erläuterungen und Diskussionen"

Für Schüler*innen ab Klasse 8.

Zeit: 09:30 - 12:00 Uhr
Ort: HTWK
Referent*innen: 
- Prof. Stephan Schönfelder (HTWK)
- Prof. Manfred Wendisch (Uni Leipzig)
- Dr. Jan Bauer (Fraunhofer CSP)
- Kai Tischer (Aktivist der Klimagerechtigkeitsbewegung)
- Dr. Martin Schubert (HTWK)

Kurzbeschreibung:

Während gesellschaftlich noch diskutiert wird, ob etwas getan werden muss, wollen wir in unserer Veranstaltung den Handlungsbedarf als Konsens annehmen. Ausgehend davon kann nun besprochen werden, was uns konkret zukünftig erwarten kann und wie man darauf reagieren sollte. Dafür stehen wir mit Wissenschaftlern der HTWK und Uni Leipzig zur Verfügung, um nach einer kurzen Einführung die Fragen der Schüler aufzugreifen, diese zu beantworten oder auch zielführend zu diskutieren.

Anmeldung per Mail unter Leipzig@FridaysForFuture.de

#FridaysForFuture #voteclimate #togetherforfuture #FFFbildet