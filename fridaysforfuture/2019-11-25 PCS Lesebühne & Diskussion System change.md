---
id: "499342607337838"
title: 'PCS: Lesebühne & Diskussion: "System change, not climate change"'
start: 2019-11-25 16:00
end: 2019-11-25 17:30
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/499342607337838/
image: 74912294_442411173361032_1892424558405746688_n.jpg
teaser: 'Titel: Lesebühne & Diskussion: "System change, not climate change" mit
  Jonathan Fei und friends Referent*Innen: Jonathan Fei und friends Ort: NSG SR
  2'
isCrawled: true
---
Titel: Lesebühne & Diskussion: "System change, not climate change" mit Jonathan Fei und friends
Referent*Innen: Jonathan Fei und friends
Ort: NSG SR 205, Hauptcampus
Zeit: 16:00 - 17:30 Uhr


Das Leipziger Schriftsteller*innenkollektiv Apropos lädt zu einer Lesung mit anschließender Diskussion ein. Vorgelesen werden Kurzgeschichten mit explizitem oder implizitem Bezug zum Kimawandel und Klimapolitik. Anschließend soll in einer offenen Runde darüber diskutiert werden, wie man der Klimakrise schreibend begegnen kann oder sogar sollte. 

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findet Ihr in Kürze unter: https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 