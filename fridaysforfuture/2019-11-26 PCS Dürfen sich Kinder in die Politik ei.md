---
id: "2428997193893596"
title: "PCS: Dürfen sich Kinder in die Politik einmischen?"
start: 2019-11-26 15:15
end: 2019-11-26 16:45
address: Marschnerstraße 31, 04109 Leipzig
link: https://www.facebook.com/events/2428997193893596/
image: 77276015_441570386778444_6758723075727949824_n.jpg
teaser: "Titel: Dürfen sich Kinder in die Politik einmischen? Referent*in: Prof.
  Wagner Ort: Marschnerstraße 31, Haus 3 Raum 126 Zeit: 15.15-16.45
  Uhr  *Geöffn"
isCrawled: true
---
Titel: Dürfen sich Kinder in die Politik einmischen?
Referent*in: Prof. Wagner
Ort: Marschnerstraße 31, Haus 3 Raum 126
Zeit: 15.15-16.45 Uhr

*Geöffnete Veranstaltung

Reihe: Erschließung und Anwendung fachwissenschaftlicher und didaktischer Grundlagen des Sachunterrichts  
Dürfen Kinder sich in Politik einmischen? Ja, denn die Kinderrechte, ein wichtiges Thema der sozialwissenschaftlichen Perspektive im Sachunterricht, sehen vor, dass Kinder an politischen Entscheidungsprozessen beteiligt werden sollen. Dies gilt vor allem, wenn ihre Angelegenheiten betroffen sind. Grundschullehrkräfte können Generationenverhältnisse thematisieren und die weitgehenden Kinderrechte, die im Alltag von Institutionen noch nicht angekommen sind, mit Grundschulkindern einüben. Es geht darum, die Umsetzung von rechtlich verankerten und weitgehend zugesicherten kommunalen, nationalen und internationalen Partizipationsrechten im gesellschaftlichen Alltag zu ermöglichen.

Diese Veranstaltung ist teil der Public Climate School. 
Das vollständige Programm findet ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 