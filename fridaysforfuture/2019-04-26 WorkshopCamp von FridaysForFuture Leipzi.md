---
id: '366776720846801'
title: WorkshopCamp von FridaysForFuture Leipzig
start: '2019-04-26 12:00'
end: '2019-04-26 17:00'
locationName: null
address: 'Clara-Zetkin-Park, 04107 Leipzig'
link: 'https://www.facebook.com/events/366776720846801/'
image: 57650910_769200296814110_4566202838748758016_o.png
teaser: Am Freitag in den Osterferien veranstaltet Fridays For Future Leipzig ein WorkshopCamp im Clara-Zetkin-Park!   Von 12-17 Uhr wird es verschiedene Work
recurring: null
isCrawled: true
---
Am Freitag in den Osterferien veranstaltet Fridays For Future Leipzig ein WorkshopCamp im Clara-Zetkin-Park! 

Von 12-17 Uhr wird es verschiedene Workshops rund um das Thema Klima geben - offen für alle, von engagierten Menschen verschiedener Initiativen, mit veganem Essen und einem bunten Rahmenprogramm aus Musik, Spielen und einem ArtSpace.

Kontaktiert uns, wenn ihr Fragen habt.