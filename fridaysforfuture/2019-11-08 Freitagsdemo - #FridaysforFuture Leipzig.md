---
id: "2543464992410716"
title: "Freitagsdemo - #FridaysforFuture Leipzig"
start: 2019-11-08 12:00
end: 2019-11-08 15:00
address: Fridays For Future Leipzig
link: https://www.facebook.com/events/2543464992410716/
image: 76956511_905532976514174_6460901582337736704_n.jpg
teaser: "Immer wieder freitags... ... gehen wir für #Klimagerechtigkeit und
  verantwortlungsvolle Politik auf die Straße.  Was wird es wohl diesmal
  werden.   La"
isCrawled: true
---
Immer wieder freitags...
... gehen wir für #Klimagerechtigkeit und verantwortlungsvolle Politik auf die Straße.

Was wird es wohl diesmal werden. 

Lass euch überraschen, auf unserem Plenum am 05.11. um 18 Uhr werden wir es entscheiden. https://www.facebook.com/events/427886157872792/

Also kommt vorbei und entscheidet mit!