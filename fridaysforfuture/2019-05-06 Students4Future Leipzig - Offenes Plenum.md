---
id: '2174816132594318'
title: Students4Future Leipzig - Offenes Plenum
start: '2019-05-06 17:00'
end: '2019-05-06 19:15'
locationName: null
address: 'Uni Leipzig, Campus Augustusplatz, Seminargebäude, Raum 202'
link: 'https://www.facebook.com/events/2174816132594318/'
image: 59380853_772755036458636_8692844250385088512_o.png
teaser: 'Der Klimawandel wartet nicht bis dein Bachelor fertig ist!  Untätige Regierungen, ein wachstumsabhängiges zerstörerisches Wirtschaftssystem... die Kli'
recurring: null
isCrawled: true
---
Der Klimawandel wartet nicht bis dein Bachelor fertig ist!

Untätige Regierungen, ein wachstumsabhängiges zerstörerisches Wirtschaftssystem... die Klimakatastrophe kommt auf uns zu, während die Klimakrise weltweit schon längst begonnen hat und die Lebensgrundlagen von Menschen zerstört. Unsere Zukunft ist bedroht.

Dem stellen wir uns entgegen. Mit Fridays for Future ist in den letzten Monaten eine globale Schüler*innen-Bewegung für Klimagerechtigkeit entstanden, wir fordern entschlossenes Handeln und das Recht auf Zukunft für Alle ein. Viele haben sich uns inzwischen angeschlossen, wie Scientists for Future, Parents for Future, Artists for Future...

Das ist der richtige Weg! Denn die Klimakrise lässt sich nur gemeinsam aufhalten. Deshalb laden wir euch Leipziger Studierende ein, mit uns zu diskutieren und euch zu organisieren. Kommt vorbei!