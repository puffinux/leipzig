---
id: '2247207862159204'
title: Abschlusskundgebung Müll-Sammelaktion
start: '2019-05-03 16:00'
end: '2019-05-03 18:00'
locationName: null
address: Augustusplatz (Opernseite)
link: 'https://www.facebook.com/events/2247207862159204/'
image: 59244022_775596596174480_3991165477261934592_n.jpg
teaser: 'Nachdem wir von 12:30 Uhr beginnend Müll gesammelt haben (https://www.facebook.com/events/601136607032324/), treffen wir uns 16 Uhr mit dem gefundenen'
recurring: null
isCrawled: true
---
Nachdem wir von 12:30 Uhr beginnend Müll gesammelt haben (https://www.facebook.com/events/601136607032324/), treffen wir uns 16 Uhr mit dem gefundenen Unrat auf dem Leipziger Augustusplatz zur Abschlusskundgebung. 

Von dort holt die Stadtreinigung Leipzig den Müll ab.