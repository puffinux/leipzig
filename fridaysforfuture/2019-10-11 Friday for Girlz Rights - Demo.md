---
id: '424785578146816'
title: Friday for Girlz Rights - Demo
start: '2019-10-11 14:00'
end: '2019-10-12 17:30'
locationName: Leipzig Augustusplaz
address: '15  Augustus platz, 04109 Leipzig'
link: 'https://www.facebook.com/events/424785578146816/'
image: 72180610_2677999192224747_2646864048171253760_n.jpg
teaser: Wir rufen auf zur Mädchen*demo im Rahmen des 8. Welt-Mädchen*tag!  In Leipzig arbeiten die Fridays for Future Bewegung und der Arbeitskreis Mädchen* z
recurring: null
isCrawled: true
---
Wir rufen auf zur Mädchen*demo im Rahmen des 8. Welt-Mädchen*tag!

In Leipzig arbeiten die Fridays for Future Bewegung und der Arbeitskreis Mädchen* zusammen an einer Streik-Demo für geschlechterübergreifende Gerechtigkeit und für Mädchen*rechte!

Ihr alle seid herzlichst eingeladen, euch am 11. Oktober um 14 Uhr mit uns auf dem Augustusplatz zu treffen und zusammen werden wir mit Finna  Cäptin Yolo und RAHSA( vom Kollektiv Fe.Male Treasure) dafür sorgen, dass uns niemand überhören kann!

Wir wollen auf die Situation von Mädchen* weltweit aufmerksam machen. Ganz besonders wollen wir aber auch klarstellen, dass Frauen* und Mädchen* überall auf der Welt - auch hier in Deutschland - strukturell benachteiligt, diskriminiert und unterdrückt werden.
Strukturell: das heißt, wir können uns nicht als einzelne Person aus dieser Ungerechtigkeit befreien, sondern wir müssen als ganze Gesellschaft mit ehrlichem Willen dafür kämpfen, dass Mädchen* (bzw. Menschen) in eine Welt geboren werden, in der sie sich frei und wohl fühlen können.

Ein paar Beispiele für strukturelle Unterdrückung von Mädchen*, die wir sicher alle kennen:

•“Mädchen*” als Beleidigung genutzt wird
•Mädchen* und junge Frauen* aufgrund dessen ausgeschlossen warden 
•Bevormundung durch die Eltern bei der Frage, ob ihr allein oder spät abends aus dem Haus dürft.
•In der Schule aufgeteilt werden in “die Jungen” und “die Mädchen”.
•Die Frage, ob mal eben ein paar “starke Jungs” beim Tragen von schweren Sachen helfen können.
•Die Annahme, dass “Mädchen” grundsätzlich schöner schreiben oder malen können.
•Der Sexualkundeunterricht, in dem lediglich Geschlechtsverkehr zwischen einem Cis-Mann und einer Cis-Frau erklärt wird (und viel zu wenig über andere und individuelle sexuelle Orientierungen geredet wird).
•Und nirgendwo im Lehrplan steht etwas davon, dass Liebe auch ohne Sex funktioniert und Sex alles Mögliche heißen kann (und nicht zwangsläufig etwas mit Penis und Vulva zu tun hat).

Schlimmere und gewaltvolle Erfahrungen, die viele Mädchen* und junge Frauen*machen müssen, sind 
•dumme Anmachsprüche, 
•körperliche Übergriffe sexistische Beleidigungen, Rufe auf der Straße
•Vergewaltigungen 
•und das ungefragte Kommentieren des Körpers von Mädchen*

Wir wollen darauf aufmerksam machen, wie wichtig es ist, dass auch Mädchen* und junge Frauen selbstbewusst leben und in allen Bereichen mitmischen können. In Deutschland - und überall auf der Welt.

Denn Gleichberechtigung ist kein Satz auf einem Stück Papier. Genausowenig wie die Versprechungen der Bundesregierung etwas daran ändern, dass die Erhöhung des Meeresspiegels in den nächsten 30 Jahren mehr als eine Milliarde Menschen bedrohen wird. Oder daran, dass wir uns vermutlich auf das größte Artensterben der Geschichte seit dem Aussterben der Dinosaurier zubewegen.

Der Klimawandel betrifft ganz besonders Frauen*/ Mädchen* und BIWOC und Mädchen* - und natürlich alle anderen Menschen, die in irgendeiner Weise (strukturell) diskriminiert werden. 
Auch deshalb ist es unserer Meinung nach eine großartige Idee, am Freitag, den 11. Oktober 2019 mit vereinten Kräften auf die Straße zu gehen:

Für die reale Emanzipation und einen wirksamen Kampf gegen die Zerstörung der Welt!

Für eine gerechte Gesellschaft, in der wir uns eines Tages hoffentlich alle wohlfühlen können!

#FridayForGirlzRights


1 das Sternchen benutzen wir, um zu signalisieren, dass es natürlich viele Menschen gibt, die von unserer Gesellschaft als Mädchen gelesen werden, obwohl sie sich selbst anders oder nur manchmal oder auf eine bestimmte Weise als Mädchen verstehen. Wir wollen diese Menschen mitdenken, wenn wir über Mädchen*rechte sprechen, ohne sie dabei schon wieder fremdzubestimmen.
2 “Cis-” ist ein Begriff der immer häufiger verwendet wird. Er wird benutzt, wenn von Menschen geredet wird, deren biologisches Geschlecht eindeutig “Mann” oder “Frau” ist und die sich auch als solche verstehen
3 BIWOC Black/Indigenous/Women* of color

Grafik: Danke Slinga illustration für die Bereitstellung!
Technik: Danke Gothic Pogo für die Bereitstellung

Finna Instagram: https://www.instagram.com/finnaluxus/?hl=de
Cäptin Yolo Instagram: https://www.instagram.com/caeptin_yolo/?hl=de
Fe.male Treasure Instagram: https://www.instagram.com/fe.male.treasure/?hl=de

