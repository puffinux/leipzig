---
id: '2949399495131913'
title: 'Solidarisches Kaffeetrinken: Fairer Handel - mehr als nur Kaffee'
start: '2019-06-30 16:00'
end: '2019-06-30 18:00'
locationName: null
address: Leipzig Hauptbahnhof
link: 'https://www.facebook.com/events/2949399495131913/'
image: 64886580_2777196958989489_4430426823599325184_n.jpg
teaser: 'Zur Demo: Klimaschutz ernst nehmen - autofreien Platz schaffen trinken wir mit Euch, dem ADFC Leipzig e.V., Fridays For Future Leipzig u.v.a. gemeinsa'
recurring: null
isCrawled: true
---
Zur Demo: Klimaschutz ernst nehmen - autofreien Platz schaffen trinken wir mit Euch, dem ADFC Leipzig e.V., Fridays For Future Leipzig u.v.a. gemeinsam fair gehandelten Kaffee, berichten was zum Fairen Handel in Leipzig und der Welt geschieht und was noch fehlt!

Bring Dir ein Stück Kuchen und eine Kaffeetasse mit, Kaffee gibt es vor Ort für Euch - zusammen mit Agitation und Information zum fairen, nachhaltigen Leben in einer global gerechten Welt.