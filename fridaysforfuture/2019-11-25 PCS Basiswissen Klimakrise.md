---
id: "1281685698698605"
title: "PCS: Basiswissen Klimakrise"
start: 2019-11-25 11:15
end: 2019-11-25 12:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/1281685698698605/
image: 76620519_440521663549983_6911332648258371584_n.jpg
teaser: 'Titel: "Basiswissen Klimakrise" Referent*innen: Scientist 4 Future Leipzig
  Ort: HS 8 / Pua Lay Peng (Malaysia) Zeit: 11:15 - 12:45 Uhr  Das 1x1 der Kl'
isCrawled: true
---
Titel: "Basiswissen Klimakrise"
Referent*innen: Scientist 4 Future Leipzig
Ort: HS 8 / Pua Lay Peng (Malaysia)
Zeit: 11:15 - 12:45 Uhr

Das 1x1 der Klimakrise - erklärt von Scientist for Future

Diese Veranstaltung ist Teil der Public Climate School.
Das vollständige Programm gibt es auf studentsforfuture.info
Wir freuen uns auf euch!