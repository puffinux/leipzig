---
id: "1260743354119398"
title: "PCS: Questions of global justice and responsibility"
start: 2019-11-25 13:00
end: 2019-11-25 15:00
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/1260743354119398/
image: 75233826_440688306866652_1670682646818062336_n.jpg
teaser: "Titel: Climate crises, climate damage - questions of global justice and
  responsibility Referent*in: Dr. Juliane Schumacher  Ort: HS 16 / Samir Flores"
isCrawled: true
---
Titel: Climate crises, climate damage - questions of global justice and responsibility
Referent*in: Dr. Juliane Schumacher 
Ort: HS 16 / Samir Flores Soberanes (Mexiko) 
Zeit: 13.00-15.00 Uhr
(Workshop für 20 Personen)

In the UN climate negotiations but also in international law people have been discussing how to deal with these climate-related losses: can affected people seek compensation? But can everything been compensated or replaced? This workshop will give an overview of these questions of global responsibilites and justice. We will have a look on how the topic is been debated in the UN and in international law. But, more important, we want to use the opportunity of the workshop to think and discuss about the questions of justice and responsibility in and around global warming, and how they can be addressed and dealt with.

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findet Ihr in Kürze unter: https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 