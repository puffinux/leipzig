---
id: '2279960495604697'
title: Offenes Plenum
start: '2019-06-11 18:00'
end: '2019-06-11 21:00'
locationName: null
address: 'Uni Leipzig, Seminargebäude, Raum S127'
link: 'https://www.facebook.com/events/2279960495604697/'
image: 60323305_782952852105521_8594261307694252032_o.png
teaser: 'Beim Plenum organisieren wir alle gemeinsam die nächsten Aktionen, tauschen uns über Ideen aus und vernetzen uns untereinander.   Jede Unterstützung u'
recurring: null
isCrawled: true
---
Beim Plenum organisieren wir alle gemeinsam die nächsten Aktionen, tauschen uns über Ideen aus und vernetzen uns untereinander. 

Jede Unterstützung und alle Ideenstifter*innen sind herzlich willkommen!