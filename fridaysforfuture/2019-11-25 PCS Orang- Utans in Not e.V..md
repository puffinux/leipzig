---
id: "2537138073266660"
title: "PCS: Orang- Utans in Not e.V."
start: 2019-11-25 13:15
end: 2019-11-25 15:15
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/2537138073266660/
image: 76945679_440694490199367_4952406456918867968_n.jpg
teaser: "Titel: Orang- Utans in Not e.V. Referent*in: Orang- Utans in Not e.V. Ort:
  NSG S112, Hauptcampus Zeit: 13.15-14.45 Uhr   Der Orang-Utans in Not e.V. l"
isCrawled: true
---
Titel: Orang- Utans in Not e.V.
Referent*in: Orang- Utans in Not e.V.
Ort: NSG S112, Hauptcampus
Zeit: 13.15-14.45 Uhr


Der Orang-Utans in Not e.V. lädt Groß und Klein dazu ein, Orang-Utans als liebenswerte Waldmenschen etwas näher kennen zu lernen und über deren Bedrohung zu diskutieren. Seit über zehn Jahren verfolgt der Verein die Vision, den tropischen Regenwald der Inseln Borneo und Sumatra vor der Abholzung für Palmölplantagen zu bewahren und damit den Lebensraum der vom Aussterben bedrohten Orang-Utans zu schützen. Es wird ein besonderer Blick darauf geworfen werden, warum für unser Essen und unsere Haushaltsprodukte der Regenwald der Orang-Utans zerstört wird und was wir gegen die Zerstörung des einzigartigen Ökosystems Regenwald unternehmen können.

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findet Ihr in Kürze unter: https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 