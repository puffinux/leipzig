---
id: '2684389031589823'
title: 'Globale Klimagerechtigkeit - #FFFbildet'
start: '2019-05-24 09:15'
end: '2019-05-24 10:45'
locationName: null
address: VILLA Leipzig
link: 'https://www.facebook.com/events/2684389031589823/'
image: 60605831_783401315394008_7355590385885773824_o.png
teaser: 'FridaysForFuture Leipzig Bildungsveranstaltung vor dem großen europäischen Klimastreik am 24. Mai um 12:30 auf dem Simsonplatz.  Titel: "Globale Klima'
recurring: null
isCrawled: true
---
FridaysForFuture Leipzig Bildungsveranstaltung vor dem großen europäischen Klimastreik am 24. Mai um 12:30 auf dem Simsonplatz.

Titel: "Globale Klimagerechtigkeit"

Für Schüler*innen ab Klasse 8.

Zeit: 09:15 - 10:45 Uhr
Ort: Großer Saal, Die Villa, Lessingstraße 7
Referentin: Franziska Krasemann (Helmholtz-Zentrum für Umweltforschung)

Kurzbeschreibung:

Globale Klimagerechtigkeit - Was heißt das eigentlich für die Welt, für Deutschland und mich? Welche Prinzipien der Gerechtigkeit und Forderungen gibt es zur Erreichung einer klimagerechten Welt?

Anmeldung per Mail unter Leipzig@FridaysForFuture.de

#FridaysForFuture #voteclimate #togetherforfuture #FFFbildet