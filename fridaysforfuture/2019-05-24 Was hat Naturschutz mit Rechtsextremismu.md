---
id: '351423685379428'
title: 'Was hat Naturschutz mit Rechtsextremismus zu tun? - #FFFbildet'
start: '2019-05-24 10:00'
end: '2019-05-24 11:30'
locationName: Zeitgeschichtliches Forum Leipzig
address: 'Grimmaische Str. 6, 04109 Leipzig'
link: 'https://www.facebook.com/events/351423685379428/'
image: 60627427_783381958729277_6529118483475595264_n.jpg
teaser: 'FridaysForFuture Leipzig Bildungsveranstaltung vor dem großen europäischen Klimastreik am 24. Mai um 12:30 auf dem Simsonplatz.  Titel: "Rechte Ökolog'
recurring: null
isCrawled: true
---
FridaysForFuture Leipzig Bildungsveranstaltung vor dem großen europäischen Klimastreik am 24. Mai um 12:30 auf dem Simsonplatz.

Titel: "Rechte Ökologen? Was hat Naturschutz mit Rechtsextremismus zu tun?"

Für Schüler*innen ab Klasse 8.

Zeit: 10:00 - 11:30 Uhr
Ort: Zeitgeschichtliches Forum
Referent: Dr. Nils Franke

Kurzbeschreibung:

Ökologie, Umweltschutz, Naturschutz, sind das nicht linke Bewegungen? Überrascht es da nicht, dass RechtsextremistInnen und RechtspopulistInnen sich bei diesen Themen gerne politisch aufstellen? Aber das tun sie, und sie machen es sehr geschickt. Denn sie verfügen über eine lange Tradition in diesem Bereich, die historisch sogar bis Auschwitz führt. Der Workshop soll sichern, dass man nicht verwirrt ist, wenn man plötzlich eine rechtsextremistische Zeitung mit dem Titel „Umwelt und Aktiv“ in die Hand bekommt, die NPD einmal mehr für den „Artenschutz“ eintritt oder die AFD blumige Worte zum Umweltschutz formuliert. Nils Franke ist Historiker, spezialisiert auf die Geschichte des Natur- und Umweltschutzes und auf Rechtsextremismus. Er ist außerdem Mitglied des Fördervereins der internationalen Jugendaustauschstätte in Auschwitz. Er leitet den SPD-Ortsverein Leipzig Nord.

Anmeldung per Mail unter Leipzig@FridaysForFuture.de

#FridaysForFuture #voteclimate #togetherforfuture #FFFbildet