---
id: '362586371052679'
title: 'Der Klimawandel vor unserer Haustür - #FFFbildet'
start: '2019-05-24 10:00'
end: '2019-05-24 12:00'
locationName: null
address: vor Ort in einer Schule (siehe Anmeldung)
link: 'https://www.facebook.com/events/362586371052679/'
image: 60358124_783374708730002_2072158339764060160_n.jpg
teaser: '+++ ANMELDUNG ERFORDERLICH +++  FridaysForFuture Leipzig Bildungsveranstaltung vor dem großen europäischen Klimastreik am 24. Mai um 12:30 auf dem Sim'
recurring: null
isCrawled: true
---
+++ ANMELDUNG ERFORDERLICH +++

FridaysForFuture Leipzig Bildungsveranstaltung vor dem großen europäischen Klimastreik am 24. Mai um 12:30 auf dem Simsonplatz.

Titel: "It’s getting hot in here: Der Klimawandel vor unserer Haustür"

Für Schüler*innen ab Klasse 8.

Zeit: 10:00 - 12:00 Uhr
Ort: vor Ort in einer Schule (Anfragen bitte an Leipzig@FridaysForFuture.de)
Referentin: Sarah Grawe (Leibniz-Institut für Troposphärenforschung)

Kurzbeschreibung:

Anhand von Wetterbeobachtungen der letzten Jahrzehnte wollen wir mit Euch erkunden, inwiefern der Klimawandel in Mitteldeutschland Einzug gehalten hat. Welche Auswirkungen haben Dürreperioden und extreme Wetterereignisse auf Tier- und Pflanzenarten in der Region? Welche Maßnahmen müssen bereits ergriffen werden, um z.B. landwirtschaftliche Erträge zu sichern? Und wie könnten sich Wetter und Klima in Zukunft weiter verändern?

Anmeldung per Mail unter Leipzig@FridaysForFuture.de

#FridaysForFuture #voteclimate #togetherforfuture #FFFbildet