---
id: "488562201747887"
title: "PCS: Basiswissen Klimakrise"
start: 2019-11-26 11:15
end: 2019-11-26 12:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/488562201747887/
image: 74803116_440511453551004_362699478362226688_n.jpg
teaser: "Titel: Public Climate School - Basiswissen Klimakrise Referent*in: Scientists
  4 Future   Raum: HS 4 - Sunita Narain (Indien) Uhrzeit: 11:15 - 12:45 Uh"
isCrawled: true
---
Titel: Public Climate School - Basiswissen Klimakrise
Referent*in: Scientists 4 Future  
Raum: HS 4 - Sunita Narain (Indien)
Uhrzeit: 11:15 - 12:45 Uhr

Das 1x1 der Klimakrise - erklärt von Scientists for Future

Diese Veranstaltung ist Teil der Public Clmate School.
Das vollständige Programm findet ihr unter studentsforfuture.info
Wir freuen uns auf euch!
