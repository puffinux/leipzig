---
id: '350193588910231'
title: Bundesweiter Klimastreik
start: '2019-03-15 10:00'
end: '2019-03-15 14:00'
locationName: Deutschlandweit
address: 'Burgdorf, Niedersachsen'
link: 'https://www.facebook.com/events/350193588910231/'
image: 51172497_2088961501196551_7803914637203734528_n.jpg
teaser: Am 15.03 wird auf der ganzen Welt für das Klima gestreikt.  Auch wir organisieren deutschlandweit dezentrale Aktionen und Demonstrationen für den schn
recurring: null
isCrawled: true
---
Am 15.03 wird auf der ganzen Welt für das Klima gestreikt.

Auch wir organisieren deutschlandweit dezentrale Aktionen und Demonstrationen für den schnellstmöglichen Kohleausstieg und für eine konsequente und radikale Klimapolitik, die uns eine Zukunft sichert. 
Dafür gehen wir nicht zur Schule, nicht zur Uni und nicht zur Ausbildung - stattdessen ziehen wir vor die Parlamente, vor die Rathäuser und auf die Straßen Deutschlands, um der Klimapolitik den Spiegel vorzuhalten, den sie verdient. Denn der Klimawandel wartet nicht, bis wir unseren Abschluss haben. 

Es wird groß! Kommt dazu, in über 200 Orten in Deutschland und 50 Ländern weltweit. 

Wo und wann in deiner Stadt demonstriert wird, erfährst du auf unserer Website: https://fridaysforfuture.de/ - die Zeiten variieren von Ort zu Ort. 

Alle Generationen sind willkommen! 