---
id: "412647746097076"
title: Flashmob- & Choreographie-Workshop 2/2 in der HALLE 14 e.V.
start: 2019-11-09 11:00
end: 2019-11-09 17:00
locationName: HALLE 14 - Zentrum für zeitgenössische Kunst
address: Spinnereistraße 7, 04179 Leipzig
link: https://www.facebook.com/events/412647746097076/
image: 74267308_905526276514844_2395880513173716992_n.jpg
teaser: Workshop in der HALLE 14  Bringt bequeme Kleidung mit! Offen für alle Kinder &
  Jugendliche!
isCrawled: true
---
Workshop in der HALLE 14

Bringt bequeme Kleidung mit!
Offen für alle Kinder & Jugendliche!