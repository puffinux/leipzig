---
id: '1037959236593748'
title: Große Klima-Demo für alle Generationen
start: '2019-09-20 15:00'
end: '2019-09-20 18:00'
locationName: null
address: 'Augustusplatz, 04103 Leipzig'
link: 'https://www.facebook.com/events/1037959236593748/'
image: 67146551_831571317243674_1462416246661185536_n.jpg
teaser: 'Am 20. September 2019, drei Tage vor dem 51. Treffen des Welt-Klimarates (IPCC), ruft Fridays for Future weltweit zum Klimastreik auf. Wie überall auf'
recurring: null
isCrawled: true
---
Am 20. September 2019, drei Tage vor dem 51. Treffen des Welt-Klimarates (IPCC), ruft Fridays for Future weltweit zum Klimastreik auf. Wie überall auf der erde werden wir auch in Leipzig demonstrieren un rufen zur großen Klima-Demo mit allen Generationen auf!

Klimagerechtigkeit und der Kampf gegen den Klimawandel gehen alle Menschen etwas an! Die Jüngeren haben mit #FridaysForFuture vorgelegt, jetzt laden wir erneut alle Menschen dazu ein gemeinsam mit uns auf die Straße zu gehen und für #Klimagerechrigkeit zu kämpfen!

Extra generationenfreundlich starten wir die Demo erst 15 Uhr, damit möglichst viele Menschen daran teilnehmen können. 

Bleibt dran und merkt euch den Termin vor! 

Es folgt noch eine ÜBERRASCHUNG!