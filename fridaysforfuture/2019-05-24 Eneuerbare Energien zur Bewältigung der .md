---
id: '2112712295492943'
title: 'Eneuerbare Energien zur Bewältigung der Klimakrise? - #FFFbildet'
start: '2019-05-24 09:00'
end: '2019-05-24 11:00'
locationName: null
address: UNIVERSITÄT LEIPZIG
link: 'https://www.facebook.com/events/2112712295492943/'
image: 60820956_783367222064084_5588890442335780864_n.jpg
teaser: '+++ ANMELDUNG ERFORDERLICH +++  FridaysForFuture Leipzig Bildungsveranstaltung vor dem großen europäischen Klimastreik am 24. Mai um 12:30 auf dem Sim'
recurring: null
isCrawled: true
---
+++ ANMELDUNG ERFORDERLICH +++

FridaysForFuture Leipzig Bildungsveranstaltung vor dem großen europäischen Klimastreik am 24. Mai um 12:30 auf dem Simsonplatz.

Titel: "Der Beitrag der Erneuerbaren Energien zur Bewältigung der Klimakrise"

Für Schüler*innen ab Klasse 8.

Zeit: 09:00 - 11:00 Uhr
Ort: Universität Leipzig
Referent: Dr. Christoph Gerhards

Kurzbeschreibung:

Durch Energieumwandlung werden derzeit ca 2/3 der Treibhausgasemission erzeugt. Deshalb ist ein Wandel des Energiesystems nötig, um die Klimakrise zu bewältigen. Kann man die Welt mit Erneuerbaren Energien versorgen ? Ist "CCS" die Lösung und was bedeutet diese Abkürzung ? Impulsvortrag mit anschließend viel Zeit für Fragen und Diskussion.

Anmeldung per Mail unter Leipzig@FridaysForFuture.de

#FridaysForFuture #voteclimate #togetherforfuture #FFFbildet