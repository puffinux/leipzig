---
id: '329314731080271'
title: 'How to change the world and not just talk about it - #FFFbildet'
start: '2019-05-24 09:00'
end: '2019-05-24 12:00'
locationName: VILLA Leipzig
address: 'Lessingstrasse 7, 04109 Leipzig'
link: 'https://www.facebook.com/events/329314731080271/'
image: 60285759_783399865394153_806814361082396672_n.jpg
teaser: 'FridaysForFuture Leipzig Bildungsveranstaltung vor dem großen europäischen Klimastreik am 24. Mai um 12:30 auf dem Simsonplatz.  Titel: "How to change'
recurring: null
isCrawled: true
---
FridaysForFuture Leipzig Bildungsveranstaltung vor dem großen europäischen Klimastreik am 24. Mai um 12:30 auf dem Simsonplatz.

Titel: "How to change the world (and not just talk about it)"

Für Schüler*innen ab Klasse 8.

Zeit: 09:00 - 10:00 Uhr; 10:00 - 11:00 Uhr; 11:00 - 12:00 Uhr; 
Ort: Zeitgeschichtliches Forum
Referent: xStarters

Kurzbeschreibung:

Du hast genug von in Plastik eingeschweißten Gurken, Pappbechern mit einer Lebensdauer von weniger als 10 Minuten und vor allem hast du keinen Bock mehr auf „Man müsste mal…“?
Mit xStarters bieten wir jungen Menschen mit Weltverbesserer-Gen eine Plattform. Wir zeigen dir konkrete Tools und digitale Möglichkeiten, wie du soziale Probleme mit ungewöhnlichen Ideen angehen kannst. Deine Kreativität ist gefragt; für den Rest sorgen wir. In unserer Session beschäftigen wir uns mit einem Thema, von dem wir alle im Alltag umgeben sind: Plastikmüll. Lass uns gemeinsam hinterfragen, welche Möglichkeiten es gibt, um Plastik zu vermeiden und welche Rolle Smartphones dabei spielen. Um teilzunehmen brauchst du nicht unbedingt eine Idee. Glaub uns: die kommt dir noch schnell genug

Anmeldung per Mail unter Leipzig@FridaysForFuture.de

#FridaysForFuture #voteclimate #togetherforfuture #FFFbildet