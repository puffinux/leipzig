---
id: "431535107532797"
title: "PCS: Climate Justice: 101"
start: 2019-11-26 13:15
end: 2019-11-26 14:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/431535107532797/
image: 77375739_443881299880686_6455703172965990400_n.jpg
teaser: "Titel: Climate Justice: 101 Referent*in: Tonny Nowshin  Raum: HS 8 / Pua Lay
  Peng (Malaysia) Zeit: 13.15-14.45 Uhr   The lecture focuses on the concep"
isCrawled: true
---
Titel: Climate Justice: 101
Referent*in: Tonny Nowshin 
Raum: HS 8 / Pua Lay Peng (Malaysia)
Zeit: 13.15-14.45 Uhr


The lecture focuses on the concept of climate justice! The experience of movement in Bangladesh will be part of the discussion.

Tonny Nowshin is a degrowth-activist from Bangladesh, mobilizing to save the world’s largest mangrove forest. She believes- we need to radically change the way we live on this planet in order to avert the climate crisis we are facing today.

Languages: English

This event is part of the public climate school. 
You can find the programm here: studentsforfuture.info 

See you.