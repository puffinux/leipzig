---
id: "1104295363281422"
title: Video in Aktion & Social Media-Workshop 2/2 in der HALLE 14 e.V.
start: 2019-11-09 11:00
end: 2019-11-09 17:00
locationName: HALLE 14 - Zentrum für zeitgenössische Kunst
address: Spinnereistraße 7, 04179 Leipzig
link: https://www.facebook.com/events/1104295363281422/
image: 75082268_905541529846652_8633795369257402368_n.jpg
teaser: Workshop in der Halle 14  Offen für alle Kinder & Jugendliche. Die Technik
  wird gestellt. Falls vorhanden, könnt ihr gern eure eigene Technik mitbring
isCrawled: true
---
Workshop in der Halle 14

Offen für alle Kinder & Jugendliche.
Die Technik wird gestellt. Falls vorhanden, könnt ihr gern eure eigene Technik mitbringen.