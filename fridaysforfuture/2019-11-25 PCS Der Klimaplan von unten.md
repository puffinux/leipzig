---
id: "550946399059893"
title: "PCS: Der Klimaplan von unten"
start: 2019-11-25 11:15
end: 2019-11-25 14:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/550946399059893/
image: 76674822_440639276871555_4093983287771398144_n.jpg
teaser: "Titel: Der Klimaplan von unten Referent*in: Gerechte1Komma5 Ort: HS 17 /
  Kavous Seyed-Emami (Iran) Zeit: 11.15-14:45 Uhr  Wir von der Kampagne Gerecht"
isCrawled: true
---
Titel: Der Klimaplan von unten
Referent*in: Gerechte1Komma5
Ort: HS 17 / Kavous Seyed-Emami (Iran)
Zeit: 11.15-14:45 Uhr

Wir von der Kampagne Gerechte1komma5 – der Klimaplan von unten, sammeln Maßnahmen, die die Treibhausgasemissionen massiv reduzieren und die soziale und globale Gerechtigkeit massiv erhöhen. Doch welche sind das? Welche gibt es schon? Welche müssen wir anpassen, damit sie unsere Werte widerspiegeln? Diese Diskussion wollen wir mit euch bei dem Write-In führen.

Wir laden euch deshalb ein, gemeinsam und im Angesicht des gescheiterten Klimakabinetts der Bundesregierung an sozial gerechten Maßnahmen zu arbeiten, die notwendig sind, um unter 1,5 Grad globaler Erwärmung zu bleiben.

In einem offenen und basisdemokratischen Prozess werden wir Maßnahmen für die Bereiche Mobilität, Energie, Produktion und Reproduktion, globale Gerechtigkeit, Wohn- und Raumplanung und Landwirtschaft erarbeiten. Diese sollen später mit in den „Klimaplan von unten“ einfließen.

Diese Veranstaltung ist teil der Public Climate School. 
Das vollständige Programm findet ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 