---
id: "978978499144567"
title: "PCS: Zivilgesellschaft und Biodiversität auf den Philippinen"
start: 2019-11-26 13:15
end: 2019-11-26 14:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/978978499144567/
image: 77090642_439978600270956_4779996470973562880_n.jpg
teaser: "Titel: Zivilgesellschaft und Biodiversität auf den Philippinen Referent*in:
  Charlotte Bast Ort: SR 229 Zeit: 13:15 - 14:45 Uhr  Zivilgesellschaft und"
isCrawled: true
---
Titel: Zivilgesellschaft und Biodiversität auf den Philippinen
Referent*in: Charlotte Bast
Ort: SR 229
Zeit: 13:15 - 14:45 Uhr

Zivilgesellschaft und Biodiversität auf den Philippinen

Was können wir von lokalen Umweltaktivist*innen aus Palawan (Philippinen) lernen? Die Philippinen gehören zu den Ländern, die am stärksten von den Auswirkungen des Klimawandels betroffen sein werden. Auch andere Gefahren bedrohen die reichhaltige Biodiversität der Region, besonders Nickelabbau und Extraktivismus. Die Insel Palawan gehört zu den Gebieten mit der größten Artenvielfalt in Flora und Fauna weltweit und beherbergt unzählige endemische Arten.  In einem Vortrag werden lokale zivilgesellschaftliche Projekte zum Erhalt der Biodiversität in Puerto Princesa City auf Palawan, vorgestellt und die Erfahrungen der lokalen Aktivist*innen wiedergegeben. Im Anschluss sollen in einer Diskussion gemeinsam herausgefunden werden wie wir diese Erfahrungen mit Umweltkonflikten hier Vergleichen können und wie wir sie für unseren Aktivismus nutzen können.

Diese Veranstaltung ist Teil der Public Climate School.
Das vollständige Programm gibt es auf studentsforfuture.info
Wir freuen uns auf euch!