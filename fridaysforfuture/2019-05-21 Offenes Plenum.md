---
id: '2279960485604698'
title: Offenes Plenum
start: '2019-05-21 18:00'
end: '2019-05-21 21:00'
locationName: null
address: 'Uni Leipzig, Seminargebäude, Raum S127'
link: 'https://www.facebook.com/events/2279960485604698/'
image: 60323305_782952852105521_8594261307694252032_o.png
teaser: 'Beim Plenum organisieren wir alle gemeinsam die nächsten Aktionen, tauschen uns über Ideen aus und vernetzen uns untereinander.   Jede Unterstützung u'
recurring: null
isCrawled: true
---
Beim Plenum organisieren wir alle gemeinsam die nächsten Aktionen, tauschen uns über Ideen aus und vernetzen uns untereinander. 

Jede Unterstützung und alle Ideenstifter*innen sind herzlich willkommen!