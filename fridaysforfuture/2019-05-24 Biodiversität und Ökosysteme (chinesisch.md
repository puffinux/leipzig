---
id: '2650670068307433'
title: 'Biodiversität und Ökosysteme (chinesische Wälder) - #FFFbildet'
start: '2019-05-24 10:00'
end: '2019-05-24 11:00'
locationName: null
address: UNIVERSITÄT LEIPZIG
link: 'https://www.facebook.com/events/2650670068307433/'
image: 60133821_783363558731117_5593942127034761216_n.jpg
teaser: '+++ ANMELDUNG ERFORDERLICH +++  FridaysForFuture Leipzig Bildungsveranstaltung vor dem großen europäischen Klimastreik am 24. Mai um 12:30 auf dem Sim'
recurring: null
isCrawled: true
---
+++ ANMELDUNG ERFORDERLICH +++

FridaysForFuture Leipzig Bildungsveranstaltung vor dem großen europäischen Klimastreik am 24. Mai um 12:30 auf dem Simsonplatz.

Titel: "Biodiversität und Ökosysteme am Beispiel von subtropischen, chinesischen Wäldern"

Für Schüler*innen ab Klasse 11.

Zeit: 10:00 - 11:00 Uhr
Ort: Universität Leipzig
Referent: Dr. David Eichenberg

Kurzbeschreibung:

Vor Kurzem warnte der Weltbiodiversitätsrat (IPBES) in einer bisher noch nie dagewesenen Deutlichkeit: „Menschliche Eingriffe haben die Natur inzwischen fast rund um den Globus erheblich verändert. Die überwiegende Mehrheit der Indikatoren, die Aufschluss über den Zustand der Ökosysteme und der
biologischen Vielfalt geben, verschlechtern sich rasch.“ Doch wie funktioniert das Überhaupt? Biodiversität? Vielen ist klar, dass z.B. Monokulturen „schlechter“ sind als artenreiche Mischwälder. Doch warum ist das so? Wie funktioniert Biodiversität überhaupt? Diesen Fragen geht der Vortrag von Dr. David Eichenberg (Deutsches Zentrum für Integrative Biodiversitätsforschung (iDiv) Halle-Jena-Leipzig) auf den Grund. Anhand ausgewählter Beispiele aus einem der größten Baum-Biodiversitätsprojekte der Welt im subtropischen Teil Chinas wird gezeigt, aufgrund welcher Mechanismen Biodiversität miteinander interagiert und Arten Ökosysteme beeinflussen. Wachstum, Nährstoffkreisläufe, pflanzenfressende Insekten und deren Fressfeinde - all das hängt in komplexer Art und Weise zusammen. Einen Versuch diese Zusammenhänge zu (er)klären unternimmt dieser Vortrag.

Anmeldung per Mail unter Leipzig@FridaysForFuture.de

#FridaysForFuture #voteclimate #togetherforfuture #FFFbildet