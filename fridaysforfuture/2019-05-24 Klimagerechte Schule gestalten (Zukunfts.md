---
id: '304742267119060'
title: Klimagerechte Schule gestalten (Zukunftswerkstatt) - FFFbildet
start: '2019-05-24 08:00'
end: '2019-05-24 12:00'
locationName: null
address: vor Ort in einer Schule (siehe Anmeldung)
link: 'https://www.facebook.com/events/304742267119060/'
image: 60269956_783388675395272_3329758956267503616_n.jpg
teaser: 'FridaysForFuture Leipzig Bildungsveranstaltung vor dem großen europäischen Klimastreik am 24. Mai um 12:30 auf dem Simsonplatz.  Titel: "Klimagerechte'
recurring: null
isCrawled: true
---
FridaysForFuture Leipzig Bildungsveranstaltung vor dem großen europäischen Klimastreik am 24. Mai um 12:30 auf dem Simsonplatz.

Titel: "Klimagerechte Schule gestalten - Eine Zukunftswerkstatt"

Für Schüler*innen in den Klassen 5 - 12.

Zeit: 08:00 - 12:00 Uhr
Ort: vor Ort in einer Schule (Anfragen bitte an Leipzig@FridaysForFuture.de)
Referent: Julian (Konzeptwerk Neue Ökonomie)

Kurzbeschreibung:

Durch die Fridays for Future Bewegung rückt auch die Schule als möglicher Ort der Veränderung in den Fokus. Wie tragen Schulen - direkt und indirekt - zum Klimawandel bei? Was stört euch dabei an eurer Schule ganz konkret? Was könnte dort anders oder besser gemacht werden? Aber auch: wie und was wollt ihr lernen, um mutige Zukunftsgestalter*innen zu werden?
Am 24. Mai von 08:00 bis etwa 12:00 kommen wir zu euch an die Schule und veranstalten mit euch eine Zukunftswerkstatt. Wir planen die Zukunftswerkstatt klassenübergreifend, für alle interessierten Schüler*innen - wenn das 20 Schüler*innen sind, ist das super, wenn es 100 werden, umso besser. Dafür benötigen wir einen großen Raum wie eine Schulaula oder ähnliches. Genaueres können wir in der Planung absprechen, wenn feststeht, wo wir die Zukunftswerkstatt durchführen. Das Konzeptwerk Neue Ökonomie ist ein gemeinnütziger Verein aus Leipzig. Wir sind erfahrene Bildner*innen und geben seit mehreren Jahren Workshops und Seminare zu Themen wie Wachstumskritik, Klimagerechtigkeit oder sozial-ökologischer Wandel der Gesellschaft. 
https://konzeptwerk-neue-oekonomie.org/ 
Kontakt: Julian Wortmann - j.wortmann@knoe.org

Anmeldung per Mail unter Leipzig@FridaysForFuture.de

#FridaysForFuture #voteclimate #togetherforfuture #FFFbildet