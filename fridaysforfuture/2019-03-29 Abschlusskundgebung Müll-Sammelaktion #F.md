---
id: '602892266894690'
title: 'Abschlusskundgebung Müll-Sammelaktion #FridaysForFuture Leipzig'
start: '2019-03-29 16:00'
end: '2019-03-29 18:00'
locationName: null
address: 'Augustusplatz, 04103 Leipzig'
link: 'https://www.facebook.com/events/602892266894690/'
image: 54423247_752839475116859_9050712109582647296_n.jpg
teaser: 'Nach der Müll-Sammelaktion am Freitag treffen wir uns 16:00 zur Abschlusskundgebung auf dem Augustusplatz.  Der gefundene Müll wird gesammelt und vom'
recurring: null
isCrawled: true
---
Nach der Müll-Sammelaktion am Freitag treffen wir uns 16:00 zur Abschlusskundgebung auf dem Augustusplatz.

Der gefundene Müll wird gesammelt und vom Ordnungsamt abtransportiert. Dazu gibt es Musik, Redebeiträge und vieles mehr.

Hier gibts die Infos zur Müll-Sammelaktion:
https://www.facebook.com/events/881227152230469/