---
id: '421333595380714'
title: 'Vernetzungstreffen #unteilbar-Klimagerechtigkeitsblock'
start: '2019-07-03 19:00'
end: '2019-07-03 23:00'
locationName: Projektwohnung "krudebude"
address: 'Stannebeinplatz 13, 04347 Leipzig'
link: 'https://www.facebook.com/events/421333595380714/'
image: 64879437_334060230862794_4117450791508246528_n.jpg
teaser: Wir laden euch ganz herzlich zu einem gemütlichen Abend in der Krudebude ein. Am 03.07.19 wird ein Kennenlern- und Vernetzungsabend der einzelnen Grup
recurring: null
isCrawled: true
---
Wir laden euch ganz herzlich zu einem gemütlichen Abend in der Krudebude ein. Am 03.07.19 wird ein Kennenlern- und Vernetzungsabend der einzelnen Gruppierungen des Klimagerechtigkeitsblocks auf der #unteilbar-Demo stattfinden und jede*r ist natürlich eingeladen. Das ganze startet ab 19 Uhr und beginnt mit Essen und Bier auf Soli-Basis! 
Der Höhepunkt dieses Abends ist es gemeinschaftlich unsere Sprüche für die #unteilbar-Demo einzuüben. Für neu-gedichtete Sprüche sind wir natürlich offen und sind auf jeden Fall schon gespannt auf euch!