---
id: "1011178049234781"
title: "PCS: We don't shut up - Workshop Ende Gelände"
start: 2019-11-26 15:15
end: 2019-11-26 16:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/1011178049234781/
image: 78344307_443869736548509_850115688784723968_n.jpg
teaser: "Titel: We don't shut up - Workshop Ende Gelände Referent*in: Ende Gelände
  Leipzig Ort: HS 14 / Phyllis Omido (Kenia)  Zeit: 15.15-16.45 Uhr  2017 bloc"
isCrawled: true
---
Titel: We don't shut up - Workshop Ende Gelände
Referent*in: Ende Gelände Leipzig
Ort: HS 14 / Phyllis Omido (Kenia) 
Zeit: 15.15-16.45 Uhr

2017 blockierten eine handvoll Aktivist*innen das Braunkohle-Kraftwerk Weisweiler und zwangen RWE das Großkraftwerk über mehrere Stunden auf Sparflamme herunterzufahren. Nun verklagt RWE fünf Aktivist*innen und einen Journalisten auf zwei Millionen Euro Schadensersatz. In diesem Input sprechen wir über die vielfältigen Aktionsformen der Klimagerechtigkeitsbewegung, über die Aktion, ihre Wirkung und ihre Konsequenzen. Über die Frage wie es danach weitergeht und wie Menschen evtl. mit so einem Schuldenberg umgehen können. http://wedontshutup.org/ueber- uns/

Diese Veranstaltung ist teil der Public Climate School. 
Das vollständige Programm findet ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 