---
id: '717993462008398'
title: Großes Plenum zur Vorbereitung des globalen Klimaaktionstag
start: '2019-10-08 18:00'
end: '2019-10-08 20:00'
locationName: null
address: 'UNIVERSITÄT LEIPZIG, Seminargebäude, Raum S102'
link: 'https://www.facebook.com/events/717993462008398/'
image: 71588189_881284342272371_1973297886351327232_o.png
teaser: 'Der 20.09. war ein riesiger Erfolg für uns! 25.000 Menschen waren allein in Leipzig dabei, 1,4 Millionen bundesweit und über 4 Millionen weltweit. Den'
recurring: null
isCrawled: true
---
Der 20.09. war ein riesiger Erfolg für uns! 25.000 Menschen waren allein in Leipzig dabei, 1,4 Millionen bundesweit und über 4 Millionen weltweit. Denoch hat es nicht gereicht, und das Klimakabinett hat ein mehr als ungenügendes Klimapaket vorgestellt. 
Das zeigt, wöchentliche Schulstreiks reichen nicht, deshalb ist für uns klar: wir müssen noch größer, noch lauter, noch krasser werden! Deshalb ist bereits der nächste globale Streik in Planung: am 29.11., kurz vor der UN Klimakonferenz in Chile, soll es global zu Massenaktionen kommen. 

Dafür wollen wir uns nächste Woche Dienstag, den 8.10., 18 Uhr im Seminarraum S102 treffen. 

Dort wollen wir Ideen für den Streiktag sammeln, Aufgaben verteilen und gemeinsam den Prozess für die nächste Leipziger Großdemo starten! Kurzum, wir brauchen jeden kreativen Kopf und jede helfende Hand.