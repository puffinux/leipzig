---
id: "860026947727839"
title: "PCS: Das Konzept der Bürger*innenversammlung"
start: 2019-11-28 13:15
end: 2019-11-28 14:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/860026947727839/
image: 76730645_442151626720320_8091882298795884544_n.jpg
teaser: "Titel: Das Konzept der Bürger*nnen Bewegung: Endlich ein realpolitischer
  Durchbruch? Referent*in: Fritzi& Emily von XR Leipzig Ort: NSG S127, Haupcamu"
isCrawled: true
---
Titel: Das Konzept der Bürger*nnen Bewegung: Endlich ein realpolitischer Durchbruch?
Referent*in: Fritzi& Emily von XR Leipzig
Ort: NSG S127, Haupcamus Augustusplatz
Uhrzeit: 13.15 - 14.45 Uhr 

Der wissenschaftliche Konsens ist überall präsent. Breite gesellschaftliche Schichten sehen drastischen Handlungsbedarf. Trotzdem werden die notwendige Maßnahmen von der Regierung nicht getroffen! Unser politisches System scheint zu träge dafür. Das Konzept der Bürger:innenversammlung ist ein spannender Lösungsansatz. Es sollen
die Kernaspekte und Hintergründe der Bürger:innenversammlung erläutert werden, um Vor- und Nachteil diskutieren zu können. Auch ein Blick in andere Länder mit Bürger:innenversammlungen soll die Diskussion über Chancen und Risiken ermöglichen. Ob dieses Konzept eine Ergänzung oder Alternative zu anderen Ideen wie dem  „Klimaplan von unten“ oder den Klimaräten ist, kann danach auch weiterführend diskutiert
werden.

Diese Veranstaltung ist Teil der Public Climate School.
Das komplette Programm findet ihr auf studentsforfuture.info
Wir freuen uns auf euch!