---
id: '2285821555026986'
title: Offenes Treffen am Biodiversitätstag
start: '2019-05-22 18:30'
end: '2019-05-22 21:30'
locationName: null
address: 'Georg-Schwarz-Straße 10, 04177 Leipzig'
link: 'https://www.facebook.com/events/2285821555026986/'
image: 60258589_2398987727013187_916018929694932992_n.jpg
teaser: 'Am 22.05 ist internationaler Tag der Artenvielfalt. Während eine Millionen Arten vom Aussterben bedroht sind, organisieren wir uns für die Rebellion g'
recurring: null
isCrawled: true
---
Am 22.05 ist internationaler Tag der Artenvielfalt. Während eine Millionen Arten vom Aussterben bedroht sind, organisieren wir uns für die Rebellion gegen dieses Massensterben. Der Name ist Programm! #ExtinctionRebellion

Falls du dabei sein möchtest, dann komm einfach um 18:30 Uhr in die Ladenfläche der Georg-Schwarz-Straße 10 (Pizza Lab). Wir geben allen, die neu sind, vor dem Treffen gerne eine kurze Einführung zu der internationalen Bewegung und der Ortsgruppe in Leipzig. Um 19:00 Uhr geht dann das Haupttreffen los. :)

Wir haben einiges zu besprechen, wollen aber das Treffen bis 21 Uhr beendet haben, um danach noch in Arbeitsgruppen und kleineren Runden zu arbeiten oder einfach zu quatschen. Klingt nett? Dann sehen wir uns, oder? ;)