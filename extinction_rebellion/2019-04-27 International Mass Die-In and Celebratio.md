---
id: '313121782709779'
title: International Mass Die-In and Celebration
start: '2019-04-27 12:05'
end: null
locationName: null
address: Worldwide
link: 'https://www.facebook.com/events/313121782709779/'
image: 56990182_316545522377567_7155241710694432768_n.jpg
teaser: 'Climate change presents us with one of the biggest global disasters in history. Our ecosystem is threatened by collapse, which will not only lead to m'
recurring: null
isCrawled: true
---
Climate change presents us with one of the biggest global disasters in history. Our ecosystem is threatened by collapse, which will not only lead to mass extinction of countless species, the loss of soil fertility and more extreme weather but will also bring with it the social crises of famine, war and migration.

The small efforts we are doing each and every day (e.g. using less packaging, buying organic food and clothes, stopping drinking with plastic straws) are clearly not enough. 
We need our governments to take their responsibilities seriously in order to ensure a future worth living to the inhabitants of our world.
Therefore, we decided to enter into rebellion, to reclaim what is due: action and transparency from the government on global climate collapse 

Since last autumn, groups all over the world have been organising non-violent rebellion acts to draw attention to the climate collapse and to address political inaction . 

Join the rebellion for life!

On the 27th of April at the symbolic time of 5 past 12pm rebels from all around the world will join a decentralised international mass die-in. The die-in is a form of non-violent resistance. In doing so, demonstrators lay on the ground in public 'as if dead', in order to demonstrate that our current situation is life-threatening. Afterwards we rise up together and celebrate life... and the rebellion :) !

To find your local group, click on this map for contact info: https://rebellion.earth/act-now/local-groups/



+++ Summary: +++
What? Decentralised International Mass Die-In and Celebration
Why? Because we’re sleepwalking into a catastrophe.
Where? Worldwide. Decentralised. Plan your action in your city!
When? April 27th at 12:05pm
