---
id: "459266218277722"
title: Offenes Treffen // XR Leipzig Ost
start: 2019-11-18 19:00
end: 2019-11-18 21:00
locationName: Pöge-Haus
address: Hedwigstraße 20, 04315 Leipzig
link: https://www.facebook.com/events/459266218277722/
image: 74915571_530044177839822_2587817580815187968_n.jpg
teaser: Der Stadtrat hat den Klimanotstand in Leipzig beschlossen. Nur müssen den
  Worten auch Taten folgen! Gleichzeitig wissen wir, dass die Klimaneutralität
isCrawled: true
---
Der Stadtrat hat den Klimanotstand in Leipzig beschlossen. Nur müssen den Worten auch Taten folgen! Gleichzeitig wissen wir, dass die Klimaneutralität bis 2050 viel zu spät ist und nicht mit dem Pariser Klimaabkommen vereinbar. Es steht also fest: Es gibt noch einiges zu tun!

Ihr habt Lust für das Überleben auf diesem Planeten aktiv zu werden und euch mit uns gemeinsam gegen die Klimakatastrophe zu stemmen? Dann kommt doch einfach zu unserem offenen Treffen, jeden Montag um 19 Uhr im Pöge-Haus. Dies sollte nicht länger als zwei Stunden dauern, danach treffen wir uns noch in AGs oder trinken noch gemeinsam ein Getränk.

Aktuell arbeiten wir bei XR Leipzig in Stadtteilgruppen, Arbeitsgruppen und Projektgruppen. Es gibt eine Gruppe im...
- Westen (Diese trifft sich meist Mittwochs um 19 Uhr im Pizza LAB)
- Osten (Diese trifft sich meist Montags um 19 Uhr im Pöge-Haus)
- Süden (Diese trifft sich am 13.11 um 19 Uhr im ADFC Leipzig e.V.)

Zudem gibt es viele Arbeits- und Projektgruppen. Wenn du aktiv werden möchtest, dann komme gerne auf eines der Treffen vorbei (egal ob west, ost, süd) Zur Einführung in unsere Arbeitsweise gibt es zusätzlich offene Kennenlern // Onboarding-Treffen, die regelmäßig angeboten werden. Gerne kannst du auch eine Mail an leipzig@extinctionrebellion.de schreiben, um weitere Fragen los zu werden oder unseren Newsletter zu bekommen.

Wir freuen uns auf dich! Alle sind willkommen, so wie sie sind!

*** English ***

The city council has decided on a climate emergency in Leipzig. But words must be followed by deeds! At the same time, we know that climate neutrality by 2050 is far too late and not compatible with the Paris Climate Agreement. It is therefore clear that there is still a lot to be done.

You want to become active for survival on this planet and work together with us against the climate catastrophe? Then come to our open meeting, every Monday at 7 p.m. in the Pöge House. This should not last longer than two hours, afterwards we meet in AGs or have a drink together.

Currently we are working at XR Leipzig in district groups, working groups and project groups. There is one group in the...
- West (They meet mostly on Wednesdays at 19 o'clock in the Pizza LAB)
- East (These usually meet on Mondays at 19 o'clock in the Pöge-Haus)
- South (This meets on 13.11 at 19 o'clock in the ADFC Leipzig e.V.)

In addition, there are many working and project groups. If you want to become active, then come to one of the meetings (no matter if west, east, south). For an introduction to our way of working there are additional open meetings // Onboarding meetings, which are offered regularly. You are also welcome to send an email to leipzig@extinctionrebellion.de to get rid of further questions or to receive our newsletter.

We are looking forward to you! Everyone is welcome as they are!