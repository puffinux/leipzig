---
id: '418605712058590'
title: Aktionswoche Rebel For Life
start: '2019-05-20 00:00'
end: '2019-05-27 18:00'
locationName: null
address: Extinction Rebellion Deutschland
link: 'https://www.facebook.com/events/418605712058590/'
image: 59973956_2393373307574629_1623126333670293504_n.jpg
teaser: 'Das sechste Massenaussterben ist im vollen Gange. Eine Million Tierarten, 1/7 aller Arten, sind vom Aussterben bedroht. Die Biodiversität schwindet im'
recurring: null
isCrawled: true
---
Das sechste Massenaussterben ist im vollen Gange. Eine Million Tierarten, 1/7 aller Arten, sind vom Aussterben bedroht. Die Biodiversität schwindet im rasenten Tempo, vor allem das Bienensterben bereitet den Forscher:innen große Sorgen. Die Klimakatastrophe bedroht das Leben auf diesem Planeten.

Dies wollen wir nicht tatenlos hinnehmen! Zum 20.05, dem Internationalen Bienentag, werden wir eine Aktionswoche ausrufen. Am internationalen Tag der Biodiversität, 22.05 werden weitere Aktionen folgen. Denn Artensterben hat Gründe: Landnutzungsveränderungen, Überfischung, Übermäßiger Pestizideinsatz, Umweltverschmutzung, die Klimakatastrophe. Dies wollen wir bunten vielfältigen Aktionen thematisieren. Am 24.05 sind wir natürlich auch beim europaweiten Streik gegen die Klimakrise dabei.

In Leipzig wird es mehrere Aktionen in dieser Woche geben, mehr Informationen folgen bald. Und vielleicht werden sich auch andere Städte anschließen... 