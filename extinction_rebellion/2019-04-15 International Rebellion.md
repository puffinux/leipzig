---
id: '126622414898833'
title: International Rebellion
start: '2019-04-15 12:00'
end: '2019-04-29 01:00'
locationName: null
address: Worldwide
link: 'https://www.facebook.com/events/126622414898833/'
image: null
teaser: 'This is an emergency.  From Monday 15 April, Extinction Rebellion is taking action on the streets of cities all over the world - from Auckland to Accr'
recurring: null
isCrawled: true
---
This is an emergency.

From Monday 15 April, Extinction Rebellion is taking action on the streets of cities all over the world - from Auckland to Accra, Mexico City to Vancouver - over multiple days to demand that governments takes necessary action on the global climate and ecological emergency.

Get more information here: https://rebellion.earth/international-rebellion/ Check page for updates!

In London, we will be blocking 4 iconic locations - Marble Arch, Oxford Circus, Waterloo Bridge and Parliament Square. We  will peacefully block traffic around the clock. This will be a full-scale festival of creative resistance with, art, stage performances, talks, workshops, food and family spaces. Extinction Rebellion Youth will gather at Hyde Park Corner and make their way to Piccadilly Circus.

Hundreds of groups have organised and are taking action in countries and communities throughout the world. 

Find and join a group organising near you! https://www.google.com/maps/d/viewer?mid=11jUqqjTHMThksd4KbvGGzb3I3Cr3PkBl&usp=sharing

If there isn't one yet, start it. Bring together those who will rise up, and together defend a vision for a better world and a brighter future - one where life matters. 

There is no greater cause than to protect life on this planet, our only home.

Join us and #rebelforlife