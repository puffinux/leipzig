---
id: '587196628429358'
title: International Mass Die-In Leipzig
start: '2019-04-27 11:30'
end: '2019-04-27 12:30'
locationName: Leipzig Neues Augusteum
address: Leipzig
link: 'https://www.facebook.com/events/587196628429358/'
image: 58779014_394059981438243_2210104387139796992_n.jpg
teaser: 'Aktuell sagen wissenschaftliche Institutionen das Aussterben der Menschheit voraus, da es das wissenschaftlich, realistischste Szenario ist. Der Club'
recurring: null
isCrawled: true
---
Aktuell sagen wissenschaftliche Institutionen das Aussterben der Menschheit voraus, da es das wissenschaftlich, realistischste Szenario ist. Der Club of Rome hält das Aussterben der Menschheit für realistischer als einen rechtzeitigen Politikwechsel. Das ESPAS, ein europäische Risikoforschungsinstitut warnt vor dem "Aussterben der Menschheit". Bundesamt für Bevölkerungsschutz und Katastrophenhilfe warnt vor dem Zusammenbrechen der Trinkwasserversorgung, der Stromversorgung und der Agrarwirtschaft in Deutschland innerhalb  der nächsten Jahren bei anhaltender Dürre. Schon jetzt bilden sich weltweit immer mehr Wüsten und Hitzetodeszonen. Auch in Deutschland, bspw. in Brandenburg. (s. Quellen unten verlinkt)

Das Bewusstsein einer drohenden Katastrophe wollen wir von Extinction Rebellion Leipzig schaffen, um unsere Rebellion gegen da Aussterben gemeinsam zu beginnen. Um dieses Bewusstsein für das tatsächliche Ausmaß zu verbreiten, wollen wir uns am Samstag um 11:30 Uhr als Trauergemeinschaft, welche den Verlust des Ökosystems, der Grundlage der Menschheit, betrauert, am Neuen Augusteum (dieser futuristischen Kirche an der Universität) treffen. In schwarz gekleidet (am besten in Anzügen oder Blazern, zumindest aber schwarz!) werden wir als Trauergemeinde durch die Innenstadt ziehen und Flyer verteilen, um schließlich am Hauptbahnhof um 5 nach 12 zum International Mass Die-In and Celebration gemeinsam (wie weltweit gleichzeitig viele andere XR-Gruppen) einen Die-In veranstalten und auf das drohende Ende von Natur und Menschheit hinzuweisen.

Die Eskalation dieser Katastrophe ist den meisten Menschen nicht bewusst, da Medien und Regierung sie nicht wissenschaftlich adäquat thematisieren. Aus diesem Grund nehmen wir es nun selbst in die Hand! Wir sind die Rebellion gegen das Aussterben! Wir fordern:
1. #TellTheTruth: Regierung und Medien müssen die Katastrophe wissenschaftlich wahr kommunizieren!
2. #ActNow: Regierung muss jetzt handeln und die Nettotreibhausgasemission bis 2025 auf NULL reduzieren.
3. #BeyondPolitics: Eine Bürger*innenversammlung, welche über die Klimaschutzmaßnahmen entscheidet, sie überwacht und kontrolliert.

Doch es gibt einige Lichtblicke: Die Bewegung Extinction Rebellion wächst aktuell weltweit im rasanten Tempo! Im mehr Menschen realisieren, dass wir einer tödlichen Bedrohung gegenüberstehen. In London blockierten tausende Klimaktivist*innen über 10 Tage zentrale Orte in der Stadt, weil "business as usual" leider nicht so weiter gehen kann, wenn die Erde gerade zu auf eine Klimakatastrophe und ökologische Krise zurast.

Auch wenn wir aktuell noch nicht 10 Tage die Stadt lahmlegen können, um die Regierung zum Handeln zu zwingen, wie es in London funktioniert hat, haben wir die Entschlossenheit diese Kraft in ein paar Monaten zu haben. Großbritannien hat vorgemacht, dass es möglich ist, wenn viele Menschen zusammen entschlossen handeln. Die Polizei ist machtlos, da wir friedlich und entschlossen sind. (s. Quelle unten) Werde auch du Teil unserer Bewegung in Leipzig und komm einfach am Samstag vorbei, am besten in schwarz, denn es gibt eine Welt zu betrauern. ;-) Falls du Fragen hast, schreibe einfach eine Mail an leipzig@extinctionrebellion.de Vielleicht hast du sogar einen Sarg zum mitbringen? ;-)

Wir freuen uns auf Euch!
XR Leipzig

Quelle:
- Club of Rome: https://www.spiegel.de/wissenschaft/natur/klimawandel-club-of-rome-gibt-prognose-fuers-jahr-2052-ab-a-831905.html
- ESPAS: https://utopia.de/eu-studie-warnt-aussterben-menschheit-134586/?utm_source=notification&utm_medium=push&utm_campaign=Utopia+Benachrichtigungen&fbclid=IwAR2dKPXNxgsFw9H2L1wqGPRt2Hmg6my2xo26nmYc9oV8L-s0Ea2rl8BMEX8
- Bundesumweltamt für Bevölkerungsschutz und Katastrophenhilfe: https://www.emderzeitung.de/deutschland-und-die-welt/topthemen_artikel,-regen-lindert-trockenheit-kaum-_arid,2011731.html
- Wüstenbildung in Brandenburg: https://www.facebook.com/spiegelonline/videos/2945691138802610/?v=2945691138802610
- Polizei ist machtlos: https://www.neues-deutschland.de/artikel/1117178.extinction-rebellion-entwaffnend-freundlich.html