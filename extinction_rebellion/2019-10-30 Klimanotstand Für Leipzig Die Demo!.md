---
id: "470316340499046"
title: "Klimanotstand Für Leipzig: Die Demo!"
start: 2019-10-30 13:00
end: 2019-10-30 18:00
locationName: Neues Rathaus Leipzig
address: Martin-Luther-Ring 4, 04109 Leipzig
link: https://www.facebook.com/events/470316340499046/
image: 73395408_516318205879086_6663059263394414592_n.jpg
teaser: "Seit einigen Monaten schon kämpfen wir zusammen mit anderen Bewegungen dafür,
  nun fällt endlich die Entscheidung: Bei der Stadtratssitzung am 30.10. w"
isCrawled: true
---
Seit einigen Monaten schon kämpfen wir zusammen mit anderen Bewegungen dafür, nun fällt endlich die Entscheidung:
Bei der Stadtratssitzung am 30.10. wird darüber abgestimmt, ob in Leipzig der Klimanotstand ausgerufen wird! Und dazu noch, ob die Stadt weiterhin Fernwärme aus dem Braunkohlekraftwerk Lippendorf bezieht.
Deshalb demonstrieren wir zusammen mit den örtlichen Gruppen von Fridays For Future, Students For Future, Health for Future, der Grünen Jugend, der Linksjugend, dem BUND und dem Ökolöwen:
Gemeinsam fordern wir den Ausruf des Klimanotstands für Leipzig, direkt darauf folgende konkrete Maßnahmen und das Ende der Unterstützung der Braunkohle, dem größten CO2-Emittenten im Leipziger Land!
Kommt zahlreich, bringt eure Freunde und Familien mit!
Es wird bunt, laut und informativ!
Bis dahin, wir freuen uns auf euch 🌻

XR Leipzig 💚