---
id: "469964573640585"
title: XR Kleidertauschparty
start: 2019-12-15 14:00
end: 2019-12-15 18:00
locationName: Pizza LAB
address: Georg-Schwarz-Straße 10, 04177 Leipzig
link: https://www.facebook.com/events/469964573640585/
image: 77425975_540748426769397_3588847944956116992_o.jpg
teaser: XR Leipzig lädt ein zur Kleidertauschparty! Du hast zu viele Klamotten und du
  weißt nicht wohin damit? Du hast zu wenig Klamotten, aber kannst dir ger
isCrawled: true
---
XR Leipzig lädt ein zur Kleidertauschparty!
Du hast zu viele Klamotten und du weißt nicht wohin damit? Du hast zu wenig Klamotten, aber kannst dir gerade keine neuen leisten? Du hast einfach mal Lust auf was Anderes, willst aber nicht extra neue Klamotten kaufen?
Dann komm zur XR-Kleidertauschparty!
Die Herstellung von Kleidung belastet die Umwelt und verbraucht Ressourcen. Baumwollplantagen benötigen viel Wasser und Düngemittel und Pestizide werden in großen Mengen verwendet. Das ist nicht nur eine Gefahr für die Umwelt, die so verseucht wird, sondern hat auch gesundheitliche Folgen für die Arbeiterinnen. Beim Färben der Textilien gelangen ebenfalls viele schädliche Chemikalien in die Natur. Die Näherinnen arbeiten für Hungerlöhne, haben viel zu lange Arbeitstage und erhalten keine soziale Absicherung. In Ländern wie Deutschland werden Kleidungsstücke viel zu billig verkauft und auch teure Kleidung bietet keine Garantie für eine faire Entlohnung der Arbeiter*innen oder eine ökologische Herstellungsweise.
Als Individuum kann man auf faire Herstellung achten (z.B. mithilfe von verschiedenen Fair-Trade-Siegeln), so wenig wie möglich kaufen und es vermeiden, Kleidungsstücke wegzuwerfen. Anstatt dessen kannst du sie reparieren, verschenken oder tauschen.
Also bringt eure Klamotten mit, tauscht sie bei Kaffee und Kuchen und kommt mit uns und anderen Kleidertauscher*innen ins Gespräch. Gemeinsam setzen wir ein Zeichen gegen die Verschwendung von Textilien!
Wir freuen uns auf euch!
Love & Rage
XRLE

Mehr Infos zu fairer Kleidung findet ihr auf www.saubere-kleidung.de

--

XR Leipzig invites you to our Clothing Swap Party!
You own too many clothes and you don’t know what to do with them? You don’t have enough clothes but can’t afford to buy new ones right now? You simply want different clothes but don’t want to buy new ones?
Then come join us at our XR Clothing Swap Party!
The production of clothes puts a strain on the environment and uses up resources. Cotton plantations need a lot of water and fertiliser and pesticides are used in great quantities. This isn’t just a danger for the environment, which is being contaminated, but has consequences for the health of workers as well. When dyeing the textiles, harmful chemicals are being released into nature. The sewers work for very little money, their working hours are too long and they don’t receive any social security. In countries like Germany those clothes are sold way too cheap and even expensive clothes can’t guarantee fair wages or an ecological manufacturing. 
As an individual you can pay attention to fair manufacturing (e.g. by paying attention to different Fair Trade seals), you can buy as few clothes as possible and you can avoid throwing away clothes. Instead you can repair, gift oder swap them.
So bring your clothes, swap them while having coffee and cake and start conversations with us and fellow clothes swappers. Together we will take a stand against the wasting of textiles!
We look forward to seeing you!

Love & Rage
XRLE

You can find more information on fair clothing on cleanclothes.org

--

Quelle/Source: www.going-green.info/themen/konsum/beispiel-kleidung/