---
id: "461744821055312"
title: "XR - Aufstand gegen das Aussterben: Start 07. Okt Berlin"
start: 2019-10-07 10:00
end: 2019-10-14 20:00
locationName: Extinction Rebellion Berlin
address: Berlin
link: https://www.facebook.com/events/461744821055312/
image: 71489973_402103740488411_9103894164168245248_n.jpg
teaser: "***Eine neue globale Rebellionswelle - Rebel without borders***  English
  below  Das Leben auf der Erde befindet sich in einer beispiellosen, globalen"
isCrawled: true
---
***Eine neue globale Rebellionswelle - Rebel without borders***

English below

Das Leben auf der Erde befindet sich in einer beispiellosen, globalen Krise: Wissenschaftler*innen sind sich einig - uns drohen Überschwemmungen, Waldbrände, extreme Wetterbedingungen, Ernteausfälle und der Zusammenbruch von Gesellschaften. Die Zeit des Leugnens ist vorbei. Es ist an der Zeit zu handeln.

Die Politik und herkömmliche Ansätze politischen Engagements wie Abstimmungen, Lobbying, Petitionen und Demonstrationen scheitern dabei dieser Krise zu begegnen. Die Geschichte zeigt uns ein erfolgsversprechendes, demokratisches Mittel um gesellschaftliche Veränderung herbeizuführen: gewaltfreier, ziviler Ungehorsam. 

Ab dem 7. Oktober werden wir durch eine internationale Rebellion Städte und somit die alltäglichen Routinen, die unsere Lebensgrundlagen zerstören, blockieren. Neben Berlin stehen auch Amsterdam, London, Paris und New York im Zentrum des friedlichen Aufstandes. Wir werden Tausende sein, die sich in den gewaltfreien zivilen Ungehorsam begeben, damit die Regierung endlich angemessen auf die Klimakrise reagiert. 

Wir werden gemeinsam auf Straßen und Plätzen rebellieren und verharren – und das friedlich, kreativ und bunt. Sei auch du dabei!

Bisher bekannt gegebene Aktionen im Rahmen der Blockaden "Aufstand gegen das Aussterben"
07. Oktober: Arche "Rebella" https://www.facebook.com/2278826269029334/posts/2501786473399978/
07. Oktober: "Jetzt Handeln!", Potsdamer Platz https://www.facebook.com/events/1356066371237699/?ti=as
07. Oktober: Animal Rebellion Berlin!:
https://www.facebook.com/events/505907756647827
09. Oktober: XR auf der Marschallbrücke: https://www.facebook.com/events/405210660182721/
09. Oktober: Shut Down Kudamm: https://www.facebook.com/events/2378166345838471/
09. Oktober: Standing With The Earth - Meditieren für das Überleben
https://www.facebook.com/events/2352316758324951/

Jetzt beitreten: Infochannels für Echtzeit-Infos während der Aktionen  direkt auf dein Smartphone
https://extinctionrebellion.de/berlinblockieren/broadcasts

Alle weiteren Infos zu Anreise, Ablauf und Vorbereitung:
https://extinctionrebellion.de/berlinblockieren/
 
Manche Informationen können wir hier auf Facebook nicht im Detail posten, deswegen ist es sehr wichtig, dass du deinen Kontakt im Rebel Survey hinterlässt. Folge jetzt einfach dem Link und klick auf "Bist du dabei?". Stay tuned!  💚 https://0710.extinctionrebellion.de/index.php/01


English

Life on Earth is experiencing an unprecedented global crisis: Scientists agree - we face flooding, forest fires, extreme weather conditions, crop failure and the collapse of societies. The time of denial is over. It is time to act.

Politics and conventional approaches to political engagement such as voting, lobbying, petitions and demonstrations fail to address this crisis. History shows us a promising, democratic means to bring about social change: nonviolent, civil disobedience.

From the 7th of October we will block cities through an international rebellion and thus halt everyday routines that destroy our livelihoods. Besides Berlin, Amsterdam, London, Paris and New York are at the center of the peaceful uprising. We will be thousands embarking on non-violent civil disobedience so that the government can finally respond appropriately to the climate crisis.

We will rebel together in streets and squares and remain there- peacefully, creatively and colorfully. Be part of it!

Already announced actions during Aufstand gegen das Aussterben:
07. October: Arch "Rebella" https://www.facebook.com/2278826269029334/posts/2501786473399978/
07. October: Demonstration "Jetzt Handeln!", Potsdamer Platz
07. October: Animal Rebellion Berlin!:
https://www.facebook.com/events/505907756647827
09. October: XR on the Marschallbridge: https://www.facebook.com/events/405210660182721/
09. October: Shut Down Kudamm: https://www.facebook.com/events/2378166345838471/
09. October: Standing With The Earth - Meditate for Survival
https://www.facebook.com/events/2352316758324951/

Sign up for the Telegram Info Channels to get all news and updates about actions directly to your phone
https://t.me/joinchat/AAAAAEZ8XmbFkSqvFkJQxg

Alle weiteren Infos zu Anreise, Ablauf und Vorbereitung:
https://extinctionrebellion.de/berlinblockieren/en/

Some information can of course not be shared in detail on Facebook. If you want to make sure to receive all information and help us with planning the rebellion please fill out the Rebel Survey:
 https://0710.extinctionrebellion.de/index.php/01