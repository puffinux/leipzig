---
id: '1118036441702648'
title: 'Clean-Up Aktion: Sachsenbrücke'
start: '2019-04-22 14:00'
end: '2019-04-22 17:00'
locationName: null
address: Sachsenbrücke
link: 'https://www.facebook.com/events/1118036441702648/'
image: 57155117_390251528485755_3953027554947891200_n.png
teaser: Wir treffen uns an der Sachsenbrücke und sammeln gemeinsam in den Sonnenschein Müll aus unseren Parks. Damit endet der Spaß allerdings nicht. Mit den
recurring: null
isCrawled: true
---
Wir treffen uns an der Sachsenbrücke und sammeln gemeinsam in den Sonnenschein Müll aus unseren Parks. Damit endet der Spaß allerdings nicht. Mit den gesammelten Materialien treffen wir uns wieder um 15:45 an der Sachsenbrücke und machen daraus eine temporäre Installation ;) 

Was wir mitbringen: 
Tüten zum Sammeln 
Handschuhe

Was ihr mitbringt:
Gute Laune
Gute Schuhe
Wasser und Getränke! 
Sonnenschutz! 