---
id: '321942735162481'
title: Museumsnacht aufmischen? Die-In Performance
start: '2019-05-11 17:30'
end: '2019-05-11 18:30'
locationName: Museum der bildenden Künste Leipzig
address: 'Katharinenstr. 10, 04109 Leipzig'
link: 'https://www.facebook.com/events/321942735162481/'
image: 59892339_403304503847124_784204334681292800_o.jpg
teaser: 'Wir, die Extinction Rebellion, wollen die Museumsnacht am Samstag, den 11.05.2019 ordentlich aufmischen und dazu ein Die-In vor dem Museum der bildend'
recurring: null
isCrawled: true
---
Wir, die Extinction Rebellion, wollen die Museumsnacht am Samstag, den 11.05.2019 ordentlich aufmischen und dazu ein Die-In vor dem Museum der bildenden Künste (MdbK) veranstalten. Damit machen wir aufmerksam auf die Umweltkatastrophe, die uns unmittelbar bevorsteht, wenn wir nicht sofort handeln und globale Veränderungen veranlassen. 

Wir treffen uns 19:30 Uhr am Haupteingang und 20:00 Uhr soll es losgehen. Für die Aktion ist etwa eine Viertelstunde eingeplant. Je mehr Menschen erscheinen, desto eindrucksvoller wird es. Wir laden euch daher ganz herzlich ein, sich dem Die-In anzuschließen, auch wenn ihr bisher noch nicht bei der XR dabei seid! Wir alle sind Teil dieses Planeten. Zeit aufzustehen und zu rebellieren! Love & Rage, XR Leipzig 