---
id: "2511669735827132"
title: Onboarding für neue Rebell:innen
start: 2020-01-06 18:30
end: 2020-01-06 20:30
address: Georg Schwarz Straße 10 Eckladen des Pizzalabs
link: https://www.facebook.com/events/2511669735827132/
image: 80990784_566743887503184_1316102125524090880_o.jpg
teaser: Ideal für alle die Extinction Rebellion kennen lernen und mehr über die
  Prinzipien und Forderungen erfahren wollen.
isCrawled: true
---
Ideal für alle die Extinction Rebellion kennen lernen und mehr über die Prinzipien und Forderungen erfahren wollen.