---
id: '1149984571851658'
title: Offenes Treffen XR Leipzig
start: '2019-05-08 16:30'
end: '2019-05-08 19:00'
locationName: null
address: 'Georg-Schwarz-Straße 10, 04177 Leipzig'
link: 'https://www.facebook.com/events/1149984571851658/'
image: 59782205_398255247685383_3940901505061617664_n.jpg
teaser: 'Das britische Parlament hat den #climateemergency beschlossen. Die Bewegung Extinction Rebellion störte während der International Rebellion über 10 Ta'
recurring: null
isCrawled: true
---
Das britische Parlament hat den #climateemergency beschlossen. Die Bewegung Extinction Rebellion störte während der International Rebellion über 10 Tage das Londoner Alltagsleben, indem fünf zentrale Brücken und Plätze friedlich blockiert wurden. Sie drängten die Regierung endlich zu handeln. Nun der Erfolg! Die erste von drei zentralen Forderungen wurde beschlossen: #TellTheTruth - Die Klimakrise ist ein Ausnahmezustand, ein Notfall! Unser Haus brennt - wie Greta Thunberg sagen würde.

In Deutschland und in Leipzig befindet sich die Bewegung noch im Aufbau. Doch auch wir haben schon einige Aktionen (Die-In mit und ohne Blockaden) durchgeführt, waren bei der Brückenblockade in Berlin und planen weitere Aktionen. Zudem vernetzen wir uns in der Klimabewegung und als internationale Bewegung Extinction Rebellion immer mehr, um auch größere Aktionen zu planen.

Um nächste Aktionen, Vernetzung und unsere Struktur soll es bei unserem nächsten offenen Treffen gehen. Falls du dabei sein möchtest, dann komm einfach um 18:30 Uhr in die Ladenfläche der Georg-Schwarz-Straße 10 (Pizza Lab). Wir geben allen, die neu sind, vor dem Treffen gerne eine kurze Einführung zu der Bewegung und der Ortsgruppe in Leipzig. Um 19:00 Uhr geht dann das Haupttreffen los. :)

Die Hauptthemen:
- Einführung in den Gruppenprozess, AG-Struktur und Kommunikation in der Gruppe
- News aus der Bewegung (in Deutschland und International)
- Aktueller Stand der AGs und kurze AG-Phase
- Gemeinsame Aktionen im Mai und Juni (+ Aktionsplanung)

Wir freuen uns auf euch!