---
id: "460257408180972"
title: "XRLE West: Open Space"
start: 2019-11-20 11:30
locationName: Pizza LAB
address: Georg-Schwarz-Straße 10, 04177 Leipzig
link: https://www.facebook.com/events/460257408180972/
image: 72133513_536193867224853_574338789351096320_n.jpg
teaser: "Der OPEN SPACE ist zurück! 💚  NOTE: Wir machen das ganze über den gesamten
  Mittwochanhcmittag und Abend, kommt rum, wann ihr am besten Zeit habt! :)"
isCrawled: true
---
Der OPEN SPACE ist zurück! 💚

NOTE: Wir machen das ganze über den gesamten Mittwochanhcmittag und Abend, kommt rum, wann ihr am besten Zeit habt! :)

Da diese Woche am Montag, dem 18.11. um 19.00 im Pögehaus das erste Leipzigweite Großplenum stattfindet, wird es diese Woche kein Westplenum geben. Stattdessen kommen wir alle gemütlich zusammen um für den globalen Klimastreik zu basteln, Banner und Transpis zu malen, uns über alles so grad präsente auszutauschen oder einfach nur um zu entspannen und Pizza zu essen. Kommt gerne mit dazu und bringt Farben und Stoff für Banner und Fahnen mit, falls ihr noch was übrig habt. Wir freuen uns auf euch! 

Love & rage,
XR West Leipzig