---
id: '2313975688891967'
title: Extinction Rebellion // Rebellionserklärung & Protestaktion
start: '2019-04-15 12:05'
end: '2019-04-15 22:00'
locationName: null
address: Extinction Rebellion Deutschland
link: 'https://www.facebook.com/events/2313975688891967/'
image: 56203719_315731839125602_2011390994976604160_n.jpg
teaser: '**for English description, see below**  Liebe Rebell*Innen,   Es ist soweit. Die internationale(n) Rebellionswoche(n) starten. Weltweit werden Mensche'
recurring: null
isCrawled: true
---
**for English description, see below**

Liebe Rebell*Innen, 

Es ist soweit. Die internationale(n) Rebellionswoche(n) starten. Weltweit werden Menschen in über 40 Ländern im Namen von Extinction Rebellion auf die Straßen gehen um die Klimakatastrophe und andere ökologische Krisen zu adressieren. Gemeinsam wollen wir mit Menschen überall auf der Welt am 15. April auch hier in Berlin der Regierung die Rebellion erklären und zivilen Ungehorsam leisten. 

INFOABEND VORHER: https://www.facebook.com/events/340600759902870/

ABLAUF DES TAGES: 

***5 nach 12**: Beginn der angemeldeten Demo - Erklärung des Klimanotstandes und Rebellion am Reichstag 

**14:00**: Beginn der angemeldeten Versammlung & Treffpunkt an der Jannowitzbrücke - Motto "Soli-Brücke - If I can't dance it's not my rebellion -autofrei und Spaß dabei!"

Inputs von
Noah Klaus
DariaDaria 
Tadzio Müller, Rosa Luxemburg Stiftung 

Music
Sound Metaphors
Drums over Knives 

**15:00**: Aktion Spreebrücken  - Friedlich, zivil & ungehorsam!

Über den Tag hinaus wird es verschiedene Arten und Weisen geben sich einzubringen. Von den angemeldeten Versammlungen zu den Aktionen zivilen Ungehorsams ist für jeden was dabei. Genauere Infos zum Programm der Demo und Versammlung folgen in kurzer Zeit. Es erwarten euch verschiedene Redner*Innen, interessante Beiträge, kreative Performances und Musik.

Für den 15. April suchen wir außerdem noch Helfer*Innen. Für mehr Info schaut bitte regelmäßig auf unserer Webseite nach: Extinctionrebellion.de

WARUM REBELLIEREN? 

Die Klimakrise ist real.  Der Klimawandel stellt uns vor eine der größten globalen Katastrophen in der Geschichte der Menschheit. Unseren Ökosystemen droht der Kollaps, welcher nicht nur zu massenhaftem Artensterben, Unfruchtbarkeit der Böden und Wetterextremen führen würde, sondern auch zu soziale Krisen in Form von Hunger, Krieg und Flucht. 

Wenn sich unser Politik- und Wirtschaftssystem nicht in kürzester Zeit drastisch verändert, steuert die Erde auf eine Erwärmung von mehr als 3°C zu, die katastrophale Folgen mit sich bringt. Aber obwohl wir wissen, dass die Klimakrise zur größten Katastrophe der Menschheitsgeschichte werden könnte, verharren unsere Regierungen in verantwortungsloser Untätigkeit, während die meisten von uns ihren gewohnten Lebensstandard fortführen.

Jahr für Jahr steigen die Mengen an Treibhausgas Emissionen weltweit an. Auch Deutschland verfehlt deutlich seine selbst gesetzten Klimaziele und ist somit Mitverursacher einer weltweiten Zerstörung der Lebensgrundlagen jetziger und zukünftiger Generationen.

HANDELN, WIE ES DER SITUATION ANGEMESSEN IST:

Wir sind überzeugt, dass wir nicht länger darauf hoffen können, dass die Regierung rechtzeitig von selbst die nötigen Schritte gehen wird. Es ist an der Zeit, sie unter massiven, gesellschaftlichen Druck zu setzen. Lange genug wurden trotz langwieriger, politischer Verhandlungen und Abkommen keine angemessenen Lösungsansätze in die Tat umgesetzt. Lange genug haben wir dies versucht mit Demonstrationen, Petitionen und Gesprächen zu erreichen. Doch diese Wege lassen sich zu leicht ignorieren. Die Geschichte der Zivilbewegungen des 20. Jahrhunderts zeigt uns ein effektives Mittel selbst Veränderung herbeizuführen: den gewaltfreien, zivilen Ungehorsam. Es ist daher Zeit für eine massenhafte, gewaltfreie Rebellion. 

DIE AKTION AM 15. APRIL: 

Diese internationale, gewaltfreie Rebellion beginnt am 15. April 2019. Aus ganz Deutschland kommen wir dafür nach Berlin. Wir erklären der Regierung die Rebellion. Später fluten wir die Straßen und bilden mit unseren Körpern friedliche Straßenblockaden auf Spreebrücken. Wir bringen den normalen Stadtbetrieb auf den Straßen zum Erliegen, um die alltäglichen Routinen und gesellschaftlichen Abläufe zu unterbrechen, welche unser aller Lebensgrundlagen gefährden. Wir setzen damit ein Zeichen, dass die Klimakrise nicht weiter ignoriert werden kann. Die Aktion wird bunt, vielfältig und gewaltfrei. Wir gehen bewusst in den zivilen Ungehorsam und tragen die Konsequenzen gemeinsam und solidarisch. 
Wir begehren auf, weil das Leben es uns wert ist. Wir bleiben bis wir weggetragen werden. Wir kommen wieder bis wir uns gemeinsam als Gesellschaft den Herausforderungen der Zukunft stellen. Wir rebellieren friedlich, gewaltfrei und respektvoll - so lange wie es notwendig ist. 

EXTINCTION REBELLION FORDERT:

1. Die Klimakrise muss ernst genommen werden. Die Regierung muss gemeinsam mit den Medien die ungeschönte Dringlichkeit der Krise kommunizieren. 
2. Die Regierung muss verbindliche Maßnahmen ergreifen um die Netto-Treibhausgasemissionen bis 2025 auf null zu senken und gesamtgesellschaftlich Konsum zu reduzieren. 
3. Eine Bürger*Innenversammlung muss einberufen werden, welche die Erarbeitung und Umsetzung der Klimaschutzmaßnahmen mitbestimmt und sicherstellt. 

Wir sind uns darüber bewusst, dass diese Forderungen notwendige, erhebliche Veränderungen unserer Lebensstile und -standards sowie des vorherrschenden Systems bedeuten. 

Wir sehen uns auf den Straßen, liebe Rebell*Innen.

Falls du noch einen Schlafplatz brauchst, schreibe eine Nachricht an:
betten@extinctionrebellion.de 

Hier der Link zum Infoabend am 14.05.: 
https://www.facebook.com/events/340600759902870/

Hier die Links zu den Aktionstrainings vor dem 15.
13.04 https://www.facebook.com/events/256713031898093/
14.04 https://www.facebook.com/events/2178522535547183/

Weitere Workshops:
Kreative Aktionsimpulse https://www.facebook.com/events/619509278558364/

Zum kennenlernen:
Jeden Dienstag treffen wir uns zum XR-Cafe auf eine gemütliche Plausch- & Kennenlernen Runde im Kafe Kommune in der Reichenbergerstraße. Uhrzeit: 18.30 Uhr

** ENGLISH VERSION ** 

Dear Rebels, 
The time has come to rebel against government inaction on the climate emergency. Together with people all over the world we will openly declare our rebellion on April 15th in Berlin and begin actions of non-violent civil disobedience.

WHAT'S HAPPENING ON THE 15th OF APRIL:

**5 past 12pm** Declaration of rebellion at the Reichstag

**14:00 pm** Meeting at Jannowitzbrücke

Inputs von
Noah Klaus
DariaDaria 
Tadzio Müller, Rosa Luxembourg Stiftung 

Music
Sound Metaphors
Drums over Knives 

**15:00 pm**  Rebellion  

WHY REBEL?

The climate crisis is real. Climate change is the biggest global catastrophe in human history, and limiting the damage presents us with our greatest challenge yet. Our ecosystems are threatened with collapse, which will not only lead to the mass extinction of species, infertility of the soil and more extreme weather conditions but also to social crises in the form of famine, war and the mass migration of tens of millions of refugees.

Much of this is already underway.

If our political and economic system does not change drastically in the shortest possible timeframe, the Earth is heading for a catastrophic warming of more than 3°C. But although we know that the climate crisis is becoming the greatest catastrophe in human history, our governments have done little to take action, meaning most of us continue as normal.

Year after year, global greenhouse gas emissions are increasing. Germany, too, is failing to meet its self-declared climate targets. The current German government is thus contributing to the destruction of life on Earth, and that of future generations.

THIS IS AN EMERGENCY

We can no longer hope that the government will take the necessary steps of its own accord and in good time. The hour is already very, very late. It is time to put them under social pressure. Despite protracted political negotiations and agreements, no adequate solutions have been put into practice. We have tried long enough to achieve this through demonstrations, petitions and talks. But these are all far too easy to ignore. The history of the successful movements of the 20th century shows us an effective way to bring about change: non-violent civil disobedience. It is, in fact, the only logical next step. It is, therefore, time for a mass, non-violent rebellion.

THE ACTION ON 15 APRIL:

This international, non-violent rebellion begins on April 15, 2019. We will come from all over Germany to Berlin. We will explain the rebellion to the government. Later, we will flood the streets and form with our bodies peaceful road blockades on Spreebrücken. We aim to bring the normal city operation on the streets to a standstill, so that the crisis cannot be further ignored. The action becomes colourful, diverse and non-violent. We consciously go into civil disobedience and bear the consequences together and in solidarity. 
We rise up, because all life on Earth is worth it to us. We stay until we are carried away. We return again and again until we all are ready to face the challenges of the future together as one society. We rebel peacefully, non-violently and respectfully - as long as it is necessary.

EXTINCTION REBELLION DEMANDS:

1) The climate crisis must be taken seriously. The government, together with the media, must communicate the stark urgency of the crisis. 
2) The government must take binding measures to reduce net greenhouse gas emissions to zero by 2025 and to reduce overall social consumption. 
3). A Citizens' Assembly must be convened to help determine and ensure the development and implementation of climate protection measures.

We are aware that these demands mean necessary, significant changes in our lifestyles and standards as well as in the prevailing system. 

ONE STRUGGLE - ONE FIGHT:

We fight together with many others for a sustainable, just and viable world into the future. It is important for us to create an awareness of the diverse scope and spectrum of our movement and to declare solidarity with our fellow campaigners. We know that many of us in the German context operate within a privileged framework and conditions for activism. We consciously opt for non-violent civil disobedience as one of various forms of action, attitudes and tactics within the climate movement.

See you on the streets, dear rebels :).

For the info-evening on the 14th of April, which further informs about the plans, click here: https://www.facebook.com/events/340600759902870/

Here links to action trainings before the 15.
06.04 https://www.facebook.com/events/328976851083060/
13.04 https://www.facebook.com/events/256713031898093/
14.04 TBA
THERE WILL BE A FEW 2.5h trainings as well

Other workshops
Creative action impulses https://www.facebook.com/events/619509278558364/

Meet / chill out 
Every Thuesday at Kafe Kommune in the Reichenbergerstr starting at 6.30pm