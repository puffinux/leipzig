---
id: '362971624334761'
title: Nationales Extinction Rebellion Treffen
start: '2019-06-07 18:00'
end: '2019-06-12 16:00'
locationName: null
address: Berlin
link: 'https://www.facebook.com/events/362971624334761/'
image: 60314590_609916376193925_5843802300451127296_n.jpg
teaser: 'Anmeldung unter https://extinctionrebellion.de/4-bundesweites-treffen/?et_fb=1'
recurring: null
isCrawled: true
---
Anmeldung unter https://extinctionrebellion.de/4-bundesweites-treffen/?et_fb=1