---
id: '407557006502414'
title: Die letzte Chance? Extinction Rebellion!
start: '2019-05-30 12:00'
end: '2019-05-30 14:00'
locationName: Goldene Rose
address: 'Rannische Straße 19, 06108 Halle, Saxony-Anhalt'
link: 'https://www.facebook.com/events/407557006502414/'
image: 61263754_770643423337159_4409624235400495104_n.jpg
teaser: Das grundlegende physikalische Phänomen des Treibhauseffektes ist seit fast 200 Jahren bekannt. Vor über 40 Jahren fand die erste Klimakonferenz statt
recurring: null
isCrawled: true
---
Das grundlegende physikalische Phänomen des Treibhauseffektes ist seit fast 200 Jahren bekannt. Vor über 40 Jahren fand die erste Klimakonferenz statt, um als Weltgemeinschaft gemeinsam gegen die Klimakrise zu handeln. Gleichzeitig entstanden große ökologische Bewegungen, NGOs und Verbände. Nach 40 Jahren Petionen, Demonstrationen, Konferenzen und Protest im weitesten Sinne sind wir keinen Schritt weiter. Die globalen Emissionen steigen weiter, scheinbar unaufhaltsam.

Englische Wissenschaftler:innen setzen jetzt alles auf eine Karte. Auf Grundlage wissenschaftlicher Erkenntnisse von sozialem Wandel gründeten sie die internationale Bewegung Extinction Rebellion, kurz XR, und riefen die International Rebellion Week am 15. April 2019 aus. Mit Erfolg! In wenigen Wochen verbreitet sich die Bewegung über den gesamten Globus. In London legten die Aktivist:innen über 10 Tage große Teile der Stadt lahm, indem öffentliche Plätze und Infrastruktur besetzt wurden. Über 1000 Menschen ließen sich bereitwillig verhaften, darunter auch Prominente. Nur eine Woche später wurde der "climate emergency" durch die Regierung ausgerufen und die Klimakatastrophe wurde zum wichtigsten Thema für die Menschen in Großbritannien.

Wie Extinction Rebellion dies geschafft hat, was das "Geheimnis" der Bewegung bzw. die Erfolgsstrategie ist, soll im ersten Teil des Workshops gemeinsam erarbeitet werden. Im zweiten Teil des Workshops wollen wir praktischer und kreativer werden. Wie können wir im lokalen Kontext die Strategie von XR umsetzen. Welche Aktionsformen bieten sich außerhalb der International Rebellion Week an? Und welche Aktion wolltet ihr schon immer mal umsetzen?

Bei XR gilt immer: Alle sind willkommen, so wie sie sind! Jede Person kann sich einbringen.