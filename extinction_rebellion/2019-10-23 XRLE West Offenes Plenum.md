---
id: "551701665649623"
title: "XRLE West: Offenes Plenum"
start: 2019-10-23 18:30
end: 2019-10-23 21:00
locationName: Pizza LAB
address: Georg-Schwarz-Straße 10, 04177 Leipzig
link: https://www.facebook.com/events/551701665649623/
image: 72978428_513383242839249_5987203248350560256_n.jpg
teaser: "Open Plenum  Note: the meeting will be in English to encourage people whose
  mother tongue is not German to come. If everyone is comfortable with Germa"
isCrawled: true
---
Open Plenum

Note: the meeting will be in English to encourage people whose mother tongue is not German to come. If everyone is comfortable with German, that will be the language spoken!

You are warmly invited to XR Leipzig West's next Plenum on Wednesday, 23rd of October. Come along, get to know us and get active! At 6:30 p.m., there will be an introduction to Extinction Rebellion for new people. If you have already been to an XR event, you are welcome to join us at 19:00. We will mention important news, have time to share emotions and criticisms, and discuss working groups. We look forward to seeing you there!
⠀⠀
With Love & Rage,
XR Leipzig West
⠀⠀
PS. Pizza Lab offers a wide selection of delicious vegan pizzas!
⠀⠀
—
⠀⠀
Offenes Plenum
⠀⠀
Hinweis: Das Treffen findet auf Englisch statt, um Menschen, deren Muttersprache nicht Deutsch ist, zu ermutigen, zu kommen. Wenn alle mit Deutsch zufrieden sind, dann ist das die Sprache, die gesprochen wird!

Du bist herzlich eingeladen zum nächsten Plenum von XR Leipzig West am Mittwoch, den 23. Oktober. Komm' vorbei, lerne uns kennen und werde aktiv! Um 18:30 Uhr wird es eine Einführung in die Extinction Rebellion für neue Menschen geben. Wenn du bereits an einem XR-Event teilgenommen hast, kannst du um 19:00 Uhr bei uns einsteigen. Wir werden wichtige Neuigkeiten besprechen; Zeit haben, Emotionen und Kritik auszutauschen und Arbeitsgruppen diskutieren. Wir freuen uns auf dich!
⠀⠀
With Love & Rage,
XR Leipzig West
⠀⠀
PS. Das Pizza Lab bietet eine große Auswahl an leckeren veganen Pizzen!