---
id: "2548977111829287"
title: Grundlagen Workshop Klimagerechtigkeit - Alle in einem Boot?
start: 2019-11-03 20:00
end: 2019-11-03 22:00
address: Georg-Schwarz-Straße 19, 04177 Leipzig
link: https://www.facebook.com/events/2548977111829287/
image: 74614148_521843128659927_5809953779155468288_n.jpg
teaser: "Grundlagen Workshop Klimagerechtigkeit - Alle in einem Boot? Von erste Klasse
  Passagier*innen und der Crew: Klimagerechtigkeit und globale Ungleichhei"
isCrawled: true
---
Grundlagen Workshop Klimagerechtigkeit - Alle in einem Boot? Von erste Klasse Passagier*innen und der Crew: Klimagerechtigkeit und globale Ungleichheiten

Der Klimawandel ist heute schon weltweit spürbar und wird bei gegenwärtiger Entwicklung immer katastrophalere Ausmaße annehmen. Doch trifft diese Entwicklung nicht alle Menschen und Regionen gleich schnell und gleichstark. Die daraus resultierenden Ungerechtigkeiten, globalen Machtverhältnisse und Lösungsperspektiven unter dem Stichwort Klimagerechtigkeit wollen wir im Workshop genauer beleuchten:

Was ist Klimagerechtigkeit? Welche Forderungen Leiten sich daraus ab? Wie kommen wir gemeinsam dahin?

Wir laden alle herzlich dazu ein, an diesem spannenden Workshop teilzunehmen und freuen uns auf euch!