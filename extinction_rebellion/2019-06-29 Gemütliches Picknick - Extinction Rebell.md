---
id: '410905849638689'
title: Gemütliches Picknick - Extinction Rebellion Leipzig
start: '2019-06-29 15:00'
end: '2019-06-29 20:00'
locationName: Johannapark
address: 04109 Leipzig
link: 'https://www.facebook.com/events/410905849638689/'
image: 64374299_428938491283725_600439971328819200_n.jpg
teaser: Rebellion für das Leben! Und was wäre das Leben ohne eine starke Gemeinschaft und Regenerationskultur? Die Community von XR Leipzig lädt zu einem Pick
recurring: null
isCrawled: true
---
Rebellion für das Leben! Und was wäre das Leben ohne eine starke Gemeinschaft und Regenerationskultur? Die Community von XR Leipzig lädt zu einem Picknick am 29. Juni ab 15 Uhr im Johannapark ein. Bringt gern eure Kinder, Freunde, Haustiere, Musikinstrumente, Hula Hoop- Reifen oder was auch immer euch am Herzen liegt mit und verbringt mit uns gemeinsam einen wunderbaren Nachmittag im Park: Ein Nachmittag zum Austausch, Kennenlernen und Sonnegenießen. 
Und wie es sich für ein gemeinsames Picknick gehört, bringt jede(r) noch ein paar kulinarische Spezialitäten und Getränke mit :)
Alle sind Willkommen!

Rebell for life! And what would life be without a strong community and regeneration culture? The community of XR Leipzig invites to a picnic on 29.06 at 3pm in Johanna Park. Bring your kids, friends, pets, instruments, hula hoops or whatever else you care about and spend a wonderful afternoon in the park with us. Let's get to know each other and exchange ideas. And as befits a picnic, everyone brings along some culinary specialties and drinks. We welcome everyone to join us!