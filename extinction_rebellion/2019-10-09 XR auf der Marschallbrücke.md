---
id: "405210660182721"
title: XR auf der Marschallbrücke
start: 2019-10-09 10:00
end: 2019-10-17 13:00
locationName: Marschallbrücke
address: Berlin
link: https://www.facebook.com/events/405210660182721/
image: 71749958_2502241290021163_7049593791614812160_n.jpg
teaser: UPDATE!!!  Wir sind seit heute morgen auf der Brücke und haben schon mal ein
  paar Überraschungen für euch vorbereitet! Wir freuen uns auf euch! The bl
isCrawled: true
---
UPDATE!!!

Wir sind seit heute morgen auf der Brücke und haben schon mal ein paar Überraschungen für euch vorbereitet! Wir freuen uns auf euch! The blockade has started so please come to the Bridge directly now.

**************************

Beginn der Blockade "XR auf der Marschallbrücke - Uns steht das Wasser bis zum Hals" am Mittwoch, 09. Oktober 2019.
Diese Aktion schließt sich der Reihe von Protesten an, die Extinction Rebellion (XR) mit eurer Unterstützung ab dem 07. Oktober in Berlin und weltweit starten wird. Beginn der Rebellion gegen das Aussterben und der Blockaden in Berlin: Montag, 07. Oktober 2019! Zum Event: https://www.facebook.com/events/461744821055312/.  

 ➜➜➜➜ CHANGE IS COMING!

Die Wälder brennen, das Wasser wird knapp, Insekten sterben - dass die Klimakatastrophe unser Überleben auf dem Planeten bedroht, wird mittlerweile für uns alle körperlich erlebbar. Der letzte Weltklimarat- Bericht prognostiziert, dass unsere Welt in Wasser und Salz versinken wird. Die Versauerung der Meere nimmt zu und der Sauerstoffgehalt verringert sich drastisch. Das für die gesamte Nahrungskette auf dem Planeten so notwendige Seetang und Seegras sind stark gefährdet. 75% des Volumens der arktischen Eiskappe sind in den letzten 30 Jahren weggeschmolzen. Bis ca. 2025 wird der Nordpol im Sommer komplett eisfrei sein.

Immer schneller rasen wir auf die drohende Katastrophe zu und das macht uns Angst. Es macht uns Angst, weil Wissenschaftler:innen seit über 30 Jahren vor den verheerenden Folgen des Klimawandels warnen, aber die weltweiten CO2-Emissionen jedes Jahr steigen. Deutschland verpasst seine Klimaziele krachend und fossile Brennstoffe werden weiterhin mit Milliarden subventioniert. Das Versäumnis unserer Regierung, die Bürgerinnen und die nächsten Generationen vor dem unvorstellbarem Leid durch den klimabedingten sozialen Zusammenbruch zu schützen, ist nicht akzeptabel. Die Hoffnung auf Besserung haben wir nicht mehr. "I don't want your hope" sagt Greta Thunberg. Deswegen: Wir brauchen endlich Mut!

Wir werden nicht mehr tatenlos zusehen und die Zerstörung von allem zulassen, was wir lieben und was uns als Zivilisation ausmacht. Wir sind bereit, unsere Freiheit zu riskieren, um endlich eine wirksame Klimapolitik zu erreichen. Wir handeln im Sinne unseres Gewissens und unserer Pflicht gegenüber unseren Kindern, dieser Gemeinschaft und diesem Planeten.  "Wo Unrecht zu Recht wird, wird Widerstand zur Pflicht" (Bertolt Brecht). 

Unser Protest wird stören, unbequem sein und damit endlich hörbar werden. Die Geschichte zeigt, dass ziviler Ungehorsam die effektivste Aktionsform ist, um Regierungen zum Handeln zu bewegen. Dabei bleiben wir immer friedlich und gewaltfrei – der Kern unseres zivilen Ungehorsams. Denn wir stehen mit unserem Gesicht für die Proteste. Diese Rebellion ist Teil unserer Bürgerpflicht.

In einem Festival des kreativen Widerstands auf der Marschallbrücke mit Performances, Familienräumen und Vorträgen setzen wir ab dem 9. Oktober die erste Forderung von Extinction Rebellion um: Die Regierung muss die existenzielle Bedrohung durch die ökologische Krise anerkennen und den Klimanotstand ausrufen. Wir protestieren, bis unsere Forderungen umgesetzt werden und halten die Brücke solange wie möglich - bunt, kreativ, inklusiv, gewaltfrei und mit langem Atem. Polizei und Rettungsdienste sind informiert. 

➜➜➜➜ WAS DU WISSEN UND TUN MUSST.

DEIN ERSTES MAL? DAS SOLLTEST DU TUN:
➜ Erzähl Freund*innen, Bekannten, der Bäckerin und dem netten Herrn aus dem Blumenladen vom Protest!
➜ Teile diese Facebookveranstaltung!
➜ Teile Artikel, Videobeiträge und Bilder rund um die Blockade in den sozialen Medien! #berlinblockieren #tellthetruth #aufstandoderaussterben
➜ Komm ab dem 07. Oktober nach Berlin-Mitte! Bring Kind, Hund, Gitarre, Kekse, Plakate, Seifenblasen mit! Stell oder setz dich dazu! Auch wenn du nicht so viel Zeit hast. Auch wenn du noch nie vorher protestiert hast.

DU MÖCHTEST AM 09. OKTOBER MIT UNS BLOCKIEREN?
➜ 1. Nimm an unserem Bezugsgruppenfindungstreffen am Sonntag um 16:30 Uhr am Sovjetischen Ehrenmal im Tiergarten und an einem Aktionstraining teil, damit du dich sicher fühlst und Spaß hast, weil du über mögliche Konsequenzen informiert bist .
➜ 2. Komm am 09. Oktober um 10 Uhr zu einem der Informationspunkte (Liste unten). Dort erhältst du alle Informationen, die du brauchst. Zieh dich gerne schick an! 
➜ 3. Gemeinsam wandern wir dann in kleinen Gruppen auf die Marschallbrücke. Wir spielen Tourist*innen: machen Selfies, bewundern die Aussicht. Wir versammeln uns NICHT in einer großen Gruppe. Warte auf das Signale oder Anweisungen! Lass dich überraschen, was passiert! 

Sobald unsere Blockade steht, beginnt das Programm. Es wird Performances, Kunst, Musik, Essen, Workshops, Gespräche und Vorträge geben. Wenn du Lust hast, etwas einzubringen, oder jemanden kennst, melde dich gerne bei uns unter leipzig@extinctionrebellion.de mit dem Betreff "Unterhaltung Berlin". Du bist herzlich eingeladen, auch spontan deiner Stimme Gehör zu verschaffen. Alle sind willkommen. 

Informationspunkte: 

- Karlsplatz
- Vor dem Brandenburger Tor

➜➜➜➜ FAMILIEN, SICHERHEIT, POLIZEI UND GEWALTLOSIGKEIT

Extinction Rebellion Deutschland hat bereits im April erfolgreich die Oberbaumbrücke in Berlin blockiert (Video: https://www.youtube.com/watch?v=ArZ9yDocklg). Die Aktion lief gewaltfrei ab, sowohl von Seiten der Teilnehmer*innen als auch der Polizei – es war eine riesige bunte Party. Die Polizei hat bei der Aktion niemanden verhaftet und sich gewaltlos verhalten. Auch im Hinblick auf unsere Blockade im Oktober halten wir es für unwahrscheinlich, dass die Polizei tausende Menschen verhaften wird, nicht zuletzt aufgrund ihrer begrenzten Kapazitäten. Die Polizei weiß auch, dass eine gewalttätige Räumung friedlich protestierender Bürger*innen ein schlechtes Licht auf sie selbst werfen wird. 

Wir gehen deshalb nicht von Gewalt seitens der Polizei aus – können aber selbstverständlich keine Garantie dafür geben. Wir können nur darauf hinweisen, was sich als Erfolgsrezept bewährt hat: Ruhe bewahren, keine Gewalt provozieren, und allen Menschen die individuelle Entscheidungsmöglichkeit zugestehen, die Blockade jederzeit zu verlassen. Wenn du mitmachen willst, informier dich bitte über unseren Aktionskonsens: https://extinctionrebellion.de/mitmachen/rebellionskonsens/. Die Polizei ist verpflichtet, eine geplante Räumung dreimal anzukündigen. Es gibt viele Menschen, die bereit sind, sich für die Rebellion verhaften zu lassen. Wenn du dir das auch vorstellen kannst, solltest du unbedingt bei einem Aktionstraining teilnehmen – in deiner Stadt oder während der Rebellionswoche in Berlin.

Wenn du dich entscheidest, mit der Polizei zu sprechen, empfehlen wir dir, ihnen erstmal weder deinen Namen noch irgendwelche Details mitzuteilen und nach dem Grund der Aufforderung zu fragen. Bitte gib auch keine Details anderer Personen weiter. Sprich gerne mit der Polizei über das Wetter, Gehaltskürzungen und die Notwendigkeit, manchen Anweisungen "von oben" nicht zu folgen. Denk jedoch immer daran, dass sie für ihren Job bezahlt werden, und dass Teil dieses Jobs sein könnte, dich zu provozieren und verärgern. Wir wollen mit unserem Gesicht zu unseren Aktionen stehen, und wir empfehlen letztlich Personalien anzugeben - mit dem wichtigen Hinweis, dass sich Menschen mit beispielsweise unsicherem Aufenthaltsstatus möglicher Konsequenzen bewusst sind. Wir empfehlen allen an einem Aktionstraining teilzunehmen.

➜➜➜➜ PACKLISTE

DINGE, DIE IHR MITBRINGEN SOLLTET:
        ➜ Snacks, Essen, genügend Wasser (lieber keine Glasflaschen)
        ➜ warme/winddichte Kleidung; ein Regenschirm (gerne verziert!)
        ➜ etwas zum Sitzen; ggf. etwas zum Schlafen, da wir die Blockade solange wie möglich halten wollen
        ➜ ein selbstgebasteltes Transparent/Schild, oder Kreide
        ➜ Spiele, Lieder und andere Dinge, die Spaß machen
        ➜ Pflanzen zum Verschönern der Brücke!
        ➜ und vor allem gute Laune und Leidenschaft!

DINGE, DIE IHR ZU HAUSE LASSEN SOLLTET:
         ➜ Alkohol und Drogen
         ➜ über Telefone in Blockaden solltet ihr euch vorher informieren, entsprechende Vorkehrungen treffen oder diese einfach nicht mitnehmen
         ➜ Glasflaschen

➜➜➜➜ LINKS

Forderungen Extinction Rebellion: https://extinctionrebellion.de/wer-wir-sind/unsere-forderungen/
Aktionskonsens Extinction Rebellion: https://extinctionrebellion.de/mitmachen/rebellionskonsens/

--

Start of the blockade "XR on the Marschall Bridge – We are up to our neck in water" on Wednesday, October 9th, 2019.
This action is part of the protests of Extinction Rebellion (XR) which will start on October 7th worldwide and in Berlin. 
Find the event here: https://www.facebook.com/ events/461744821055312/. 
➜➜➜➜ CHANGE IS COMING! 
Forests are burning, water is rare, insects are dying – the fact that the climate catastrophe is threatening our survival on this planet is visible to all of us by now. The last IPCC report forecasts that our world will be sunk by water and salt. The increasing acidity within our oceans and the decreasing level of oxygen mark the intensity of this crisis. Seaweed and eal grass are endangered although they are crucial for the entire food chain of our planet. 75% of the artic ice has melted in the last 30 years. The Arctic will be completely ice free in Summers until 2025. 
The space that the climate collapse is approaching is ever-accelerating and we are afraid. We afraid because scientists have been warning about the devestating consequences of climate change for 30 years already and still C02 emmissions are increasing worldwide each year. Germany is missing its climate goals by far and fossil fuels keep being subsidised by billions. The government has failed to protect us citizens and the next generations from the unimaginable suffering cause by climate induced social collapse. This is not acceptable. We no longer have hope for improvements. "I don't want your hope", says Greta Thunberg. This is why, we finally need courage! 
We will no longer stand back and accept the destruction of all that we love and makes up our civilisation. We are willing to risk our own freedom to finally achieve effective climate 
politics. We operate with our consciensous in mind and with the reponsibility towards our children, this society and our planet. "Where injustice becomes law, resistance becomes duty." (Bertolt Brecht). 
Our protest will disrupt and it won’t be comfortable so that we finally will be listened to. History shows that civil disobedience is the most effective form of action to pressure government into action. We will always stay peaceful and nonviolent – the core of our civil disobedience. We will show our faces as part of these protests. This rebellion is our duty as citizens. 
A festival of creative resistance on the Marschall Bridge with performances, space for families and speeches will mark the implementation of Extinction Rebellion’s first demand: The government has to tell the truth about the existential threats of the ecological crisis and declare climate emergency. We will protest until our demand is met. We will hold the bridge for as long as possible – colourful, creative, inclusive, nonviolent and with a lot of perserverance. Police and emergency services have been informed. 
➜➜➜➜ WHAT YOU NEED TO KNOW AND DO. 
YOUR FIRST TIME? THIS IS WHAT YOU SHOULD DO:
➜ Tell a friend, neighbour, your local backery and the nice guy from the flower shop! 
➜ Share this facebook event!
➜ Share articles, videos and pictures of the blockade on social media! #berlinblockieren #tellthetruth #aufstandoderaussterben
➜ Come to Berlin-Mitte from October 7th! Bring your children, dog, guitar, cookies, posters and soap bubbles! Stand or sit with us! Even if you don’t have a lot of time. Even if you have never protested before. 
YOU WANT TO BLOCKADE THE BRIDGE WITH US ON OCTOBER 9TH? 
➜ 1. Take part in our affinity group matching on Sunday, 4.30pm at the „Sowjetisches Ehrenmal im Tiergarten“ and take part in an NVDA training (non violent direct action training) so that you can feel comfortable, have fun and are informed about the possible consequences. 
➜ 2. On October 9th at 10am, come to one of the information points (list below). There you will get all information you need. Feel free to dress up! 
➜ 3. Together in small groups we will make our way to Marschall Bridge. We pretend to be tourists: taking selfies and admiring the sights. We do not assemble as a big group. We wait until the sign and further instructions follow! Let yourself be surprised by what will happen! 
As soon as the blockade is established the programme will commence. There will be performances, art, music, food, workshops, talks and speeches. If you feel like contributing or you know someone, reach out via leipzig@extinctionrebellion.de with the subject line "Unterhaltung Berlin". You are also welcome to bring in your voice in a spontaneous manner. All are welcome! 
Information points: 

- Karlsplatz
- In front of Friedrichstadtpalast
- At the corner of Charlottenstraße/Dorotheenstraße - Dorothea-Schlegel-Platz
- In front of Brandenburger Tor 
➜➜➜➜ FAMILIES, SAFETY, POLICE AND NONVIOLENCE 
Extinction Rebellion Germany has already successfully blocked the Oberbaum Bridge in Berlin in April (Video: https://www.youtube.com/watch? v=ArZ9yDocklg). This action was nonviolent from both sides of activists and police and was a massice colourful party. The police did not arrest anyone during this action and was nonviolent too. Looking towards the October rebellion we consider it unlikely that the police will arrest thousands of people, not least because of their limited capacity. The police also know that a violent removal of nonviolent citizens will bring a bad light upon themselves. 
We are therefore not expecting violence from the police, but cannot guarantee this. We would like to bring attention to what has worked so far: stay calm and do not provoke violence in order to enable all people to decide for themselves how to act and when to leave the blockade. If you want to take part, please read our action consensus: https://extinctionrebellion.de/mitmachen/rebellionskonsens/. Police is required to inform you three times of a possible removal. There are many people that are willing to get arrested for the rebellion. If you are willing to get arrested a well, it is necessary for you to receive a non violent direct action training. These will be held at different locations across the city throughout the rebellion week in Berlin.  
If you decide to talk to police, we recommend you to not give any personal details and if they want to know to question their reason. Please do not give any details about other people to the police. You can talk to the police about the weather, cuts in their salaries and that not all orders have to be followed. Please remember that the police is paid to do their job and that part of their job could be to provoke or annoy you. We want to stand by our actions with our faces and will give our personal details to police if absolutely necessary. This is important to know for people with a different resident status to be aware of possible consequences. We urge everyone to participate in a non violent direct action training! 
➜➜➜➜ PACKING LIST 
THINGS YOU SHOULD BRING:
➜ Snacks, food, enough water (no glass bottles) ➜ Warm and wind proof clothing, an umbrella (feel free to decorate it!)
➜ Something to sit on; possibly something for sleeping so that we can hold the blockade for as long as possible
➜ Your own poster or sign and chalk
➜ Games, songs and other things that are fun ➜ Plants to bring beauty and life to the bridge ➜ Most of all: come with high spirits and passion! 
THINGS TO LEAVE AT HOME:
➜ Alcohol and drugs
➜ Please consider if you want to bring your phone to the bridge or not and inform yourself what kind of preparations you can take to prepare your phone
➜ Glass bottles 

➜➜➜➜ LINKS 
Extinction Rebellion’s Demands: 
https://extinctionrebellion.de/wer-wir-sind/unser e-forderungen/

Extinction Rebellion’s Action Consensus: https://extinctionrebellion.de/mitmachen/rebellionskonsens/   