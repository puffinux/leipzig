---
id: '621655821633885'
title: Großer Trauermarsch für aussterbende Arten
start: '2019-06-10 13:00'
end: '2019-06-10 15:00'
locationName: S Leipzig Bayerischer Bahnhof
address: 'Bayrischer Platz, Leipzig'
link: 'https://www.facebook.com/events/621655821633885/'
image: 61863750_2299862856769000_7772495254921412608_n.jpg
teaser: Auch dieses Jahr verwandelt sich Leipzig zu Pfingsten wieder in die größte schwarze Party des Jahres. Doch angesichts der sich anbahnenden Klimakatast
recurring: null
isCrawled: true
---
Auch dieses Jahr verwandelt sich Leipzig zu Pfingsten wieder in die größte schwarze Party des Jahres. Doch angesichts der sich anbahnenden Klimakatastrophe und des fortschreitenden Massensterbens fällt es uns zunehmend schwerer ausgelassen zu feiern. Daher möchten wir dieses Jahr zum WGT gemeinsam mit Euch ein Zeichen setzen -- auf unsere Art: Wir laden Euch ein, gemeinsam mit uns in einem großen Trauermarsch für das Artensterben durch die Leipziger Innenstadt zu ziehen.

Gemeinsam betrauern wir die mehr als hundert Arten, welche tagtäglich den Folgen des Klimawandels und weiteren menschgemachten Umweltveränderungen zum Opfer fallen und für immer von unserer Erde verschwinden. Symbolisch werden wir einen Sarg vorweg tragen. Der Trauermarsch beginnt am Bayrischen Bahnhof und endet am Leipziger Hauptbahnhof. Dabei passieren wir einige klassische WGT-Locations und werden dort für kurze Redebeiträge halt machen.

Wer möchte kann im Anschluss an einem sogenannten „Die-In“ teilnehmen. Dabei liegen alle Beteiligten für einige Minuten wie tot auf dem Boden und machen so auf das akute Massensterben aufmerksam, dass von unserer Gesellschaft nur allzu gerne ignoriert wird.

Organisiert wird die Aktion vom WGT-Guide Team und Extinction Rebellion. Ganz besonders freuen wir uns auf die Beiträge von Mark Benecke, Oswald Henke (Goethes Erben), Anke (Mila Mar) und Feline & Strange!

Dresscode: Schwarz, idealerweise in Trauerflor mit schwarzem Schleier etc. Die Aktion ist beim Ordnungsamt angemeldet und genehmigt.

Wir würden uns freuen, viele von Euch zu sehen. Lasst uns unsere Aktion unvergesslich und unübersehbar machen!

Siehe auch aktuelle Berichterstattung zum Thema Artensterben:
- https://www.spiegel.de/wissenschaft/natur/artensterben-uno-bericht-beschreibt-dramatischen-verlust-der-artenvielfalt-a-1265482.html
- https://www.zeit.de/wissen/umwelt/2019-05/artenvielfalt-kernaussagen-welt-bericht-paris-weltbiodiversitaetsrat-artensterben

~~~

This pentecost once more Leipzig turns into the world's greatest Gothic party. But in light of the threatening climate catastrophe and the ongoing mass extinction it is hard for us to celebrate and enjoy. So this year, we want to put a sign: Together with you we will form a huge funeral march straight through the city of Leipzig.

Together we will mourn more than one hundred species which die out each day at the consequences of global warming and many more man-made changes to our environment. Symbolically, we will carry a coffin in front of us. The march begins at "Bayrischer Bahnhof" and ends at the main station. Along the route we will pass many WGT locations and stop by for short speeches.

After the march there will be a die-in performance in front of the main station. During a die-in everybody lies on the floor like he/she was dead in order to draw attention to the ongoing mass extinction which most people choose to ignore.

The event is hosted by WGT-Guide and Extinction Rebellion Leipzig. We are happy to announce that among others Mark Benecke, Oswald Henke (Goethes Erben), Anke (Mila Mar) and Feline & Strange will join us!

Dresscode: Black, ideally funeral outfit containing veils etc. The march has been approved by the regulatory office.

We would love to see many of you. Let us make our funeral march unforgettable and vast!