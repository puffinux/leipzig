---
id: "459266211611056"
title: Offenes Treffen // XR Leipzig Ost
start: 2019-11-04 19:00
end: 2019-11-04 21:00
locationName: Pöge-Haus
address: Hedwigstraße 20, 04315 Leipzig
link: https://www.facebook.com/events/459266211611056/
image: 75594496_521745045336402_8814600422833520640_n.jpg
teaser: Der Stadtrat hat den Klimanotstand in Leipzig beschlossen. Nur müssen den
  Worten auch Taten folgen! Gleichzeitig wissen wir, dass die Klimaneutralität
isCrawled: true
---
Der Stadtrat hat den Klimanotstand in Leipzig beschlossen. Nur müssen den Worten auch Taten folgen! Gleichzeitig wissen wir, dass die Klimaneutralität bis 2050 viel zu spät ist und nicht mit dem Pariser Klimaabkommen vereinbar. Es steht also fest: Es gibt noch einiges zu tun!

Ihr habt Lust für das Überleben auf diesem Planeten aktiv zu werden und euch mit uns gemeinsam gegen die Klimakatastrophe zu stemmen? Dann kommt doch einfach zu unserem offenen Treffen, jeden Montag um 19 Uhr im Pöge-Haus. Dies sollte nicht länger als zwei Stunden dauern, danach treffen wir uns noch in AGs oder trinken noch gemeinsam ein Getränk.

Wir freuen uns auf dich! Alle sind willkommen, so wie sie sind!

***

The city council has decided on a climate emergency in Leipzig. But words must be followed by deeds! At the same time, we know that climate neutrality by 2050 is far too late and not compatible with the Paris Climate Agreement. It is therefore clear that there is still a lot to be done.

You want to become active for survival on this planet and work together with us against the climate catastrophe? Then come to our open meeting, every Monday at 7 p.m. in the Pöge House. This should not last longer than two hours, afterwards we meet in AGs or have a drink together.

We are looking forward to you! Everyone is welcome as they are!