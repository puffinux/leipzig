---
id: "2501578173499079"
title: Aktionsplenum Leipzig West - 27.11.
start: 2019-11-27 19:00
end: 2019-11-27 21:00
address: Pizzalab Eckladen, Georg-Schwarz-Strasse 10
link: https://www.facebook.com/events/2501578173499079/
image: 74441247_541736193337287_1704927460451155968_n.jpg
teaser: "XR Leipzig geht in die nächste Runde. Bei diesem Plenum wird's konkret: Ende
  Gelände Aktion, Globaler Klimastreik, Brainstorming und zusammen ordentli"
isCrawled: true
---
XR Leipzig geht in die nächste Runde. Bei diesem Plenum wird's konkret: Ende Gelände Aktion, Globaler Klimastreik, Brainstorming und zusammen ordentlich wat wuppen! ✊🏼💚

Du bist herzlich eingeladen, am Mittwoch, dem 27.11. um 19.00 beginnt's. Komm' vorbei, lerne uns kennen und werde aktiv! Wir sind schon ab 17.00 Uhr da, heizen vor, sind kreativ und malen Banner und Flaggen. Jede Unterstützung ist willkommen :) Wir freuen uns auf dich!
⠀⠀
With Love & Rage,
XR Leipzig West
⠀⠀
PS. Das Pizza Lab bietet eine große Auswahl an leckeren veganen Pizzen!