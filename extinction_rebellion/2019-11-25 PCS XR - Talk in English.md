---
id: "831637980585791"
title: "PCS: XR - Talk in English"
start: 2019-11-25 11:15
end: 2019-11-25 12:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/831637980585791/
image: 77325085_440649386870544_7840571504470261760_n.jpg
teaser: "Titel: XR - Talk in English Referent*in: Diego Ort: NSG S205, Hauptcampus
  Zeit: 11.15-12.45 Uhr  (Teaser folgt)  Diese Veranstaltung ist Teil der Publ"
isCrawled: true
---
Titel: XR - Talk in English
Referent*in: Diego
Ort: NSG S205, Hauptcampus
Zeit: 11.15-12.45 Uhr

(Teaser folgt)

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findet Ihr in Kürze unter: https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 