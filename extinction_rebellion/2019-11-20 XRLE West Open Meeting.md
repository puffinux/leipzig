---
id: "586985068709950"
title: "XRLE West: Open Meeting"
start: 2019-11-20 19:00
end: 2019-11-20 23:00
address: Georg-Schwarz-Straße 10, 04177 Leipzig
link: https://www.facebook.com/events/586985068709950/
image: 73390811_520293655481541_5184773481941696512_n.jpg
teaser: "Open Plenary  Note: the meeting will be in English to encourage people whose
  mother tongue is not German to come. If everyone is comfortable with Germ"
isCrawled: true
---
Open Plenary

Note: the meeting will be in English to encourage people whose mother tongue is not German to come. If everyone is comfortable with German, that will be the language spoken!

You are warmly invited to XR West Leipzig's next Plenum! Come along, get to know us and get active! We will discuss news, plans for the future, working groups and have some time for emotions, fun and reflections. We look forward to seeing you there!

With love & rage,
XR West Leipzig

PS. PizzaLab still offers a wide selection of delicious vegan pizzas!

-- 
Offenes Plenum

Hinweis: Das Treffen findet auf Englisch statt, um Menschen, deren Muttersprache nicht Deutsch ist, zu ermutigen, zu kommen. Wenn alle mit Deutsch zufrieden sind, dann ist das die Sprache, die gesprochen wird!

Sei herzlich eingeladen zum nächsten Plenum von XR West Leipzig am Mittwoch, den 30. Oktober. Komm mit, lerne uns kennen und werde aktiv! Wir werden über Neuigkeiten, Zukunftspläne, Arbeitsgruppen austauschen und Zeit für Emotionen, Spaß und Reflexionen haben. Wir freuen uns darauf, dich dort zu sehen!

Love&Rage
XR West Leipzig
PS. PizzaLab bietet immer eine große Auswahl an leckeren veganen Pizzen an! 