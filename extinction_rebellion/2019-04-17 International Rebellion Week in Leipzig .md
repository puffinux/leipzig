---
id: '282093929393417'
title: 'International Rebellion Week in Leipzig: XR Leipzig'
start: '2019-04-17 17:30'
end: '2019-04-17 20:30'
locationName: null
address: Angerstraße 36
link: 'https://www.facebook.com/events/282093929393417/'
image: 57462869_387891402055101_3695372544170262528_n.png
teaser: Die International Rebellion Week beginnt! Ab 15. April finden weltweit friedliche Aktionen zivilen Ungehorsams statt. Wir unterstützen am Montag die A
recurring: null
isCrawled: true
---
Die International Rebellion Week beginnt! Ab 15. April finden weltweit friedliche Aktionen zivilen Ungehorsams statt. Wir unterstützen am Montag die Aktionen in Berlin und planen in Leipzig kleinere Aktionen. Zudem wollen wir einen Workshop über die Bewegung Extinction Rebellion konzipieren und umsetzen. Auf lokaler, nationaler und internationaler Ebene passiert viel und wir wollen gemeinsam darüber reden.

Um die Entwicklungen, Aktionen, Vernetzung und vieles weitere soll es bei unserem nächsten offenen Treffen gehen. Falls du dabei sein möchtest, dann komm einfach um 17:30 Uhr in die Angerstraße 36. Wir geben allen, die neu sind, vor dem Treffen gerne eine kurze Einführung zu der Bewegung Extinction Rebellion und der Gruppe in Leipzig. Um 18 Uhr geht dann das Haupttreffen los. :)

Die Hauptthemen:
- Feedback / Review: Die-In bei FridaysForFuture 12.04.2019
- International Rebellion Week Leipzig: Aktionen, Rezeption
- Vernetzung in der Klimabewegung: Workshop-Konzeption
- Social Media und Öffentlichkeitsarbeit

Wir freuen uns auf alle, die vorbeikommen wollen.
Bis bald!