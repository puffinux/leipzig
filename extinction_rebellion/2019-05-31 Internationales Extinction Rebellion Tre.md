---
id: '1967719056672959'
title: Internationales Extinction Rebellion Treffen
start: '2019-05-31 16:00'
end: '2019-06-02 18:00'
locationName: null
address: Lollar
link: 'https://www.facebook.com/events/1967719056672959/'
image: 60411384_609892466196316_5704121197956956160_n.jpg
teaser: Das nächste Treffen von XR International wird vom 31.5.-2.6. in Lollar stattfinden. Die Teilnehmer*innen kommen aus der ganzen Welt.
recurring: null
isCrawled: true
---
Das nächste Treffen von XR International wird vom 31.5.-2.6. in Lollar stattfinden. Die Teilnehmer*innen kommen aus der ganzen Welt.