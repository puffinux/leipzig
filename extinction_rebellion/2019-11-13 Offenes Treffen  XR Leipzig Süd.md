---
id: "531688500729151"
title: Offenes Treffen // XR Leipzig Süd
start: 2019-11-13 19:00
end: 2019-11-13 21:00
locationName: ADFC Leipzig e.V.
address: Peterssteinweg 18, 04107 Leipzig
link: https://www.facebook.com/events/531688500729151/
image: 74596453_530042044506702_7740841960130215936_n.jpg
teaser: Der Stadtrat hat den Klimanotstand in Leipzig beschlossen. Nun müssen den
  Worten auch Taten folgen! Gleichzeitig wissen wir, dass das Ziel der Klimane
isCrawled: true
---
Der Stadtrat hat den Klimanotstand in Leipzig beschlossen. Nun müssen den Worten auch Taten folgen! Gleichzeitig wissen wir, dass das Ziel der Klimaneutralität bis 2050 viel zu spät ist und nicht mit dem Pariser Klimaabkommen vereinbar. Es steht also fest: Es gibt noch einiges zu tun!

Ihr habt Lust für das Überleben auf diesem Planeten aktiv zu werden und euch mit uns gemeinsam gegen die Klimakatastrophe zu stemmen? Dann kommt doch einfach zu unserem offenen Treffen, am Mittwoch um 19 Uhr in den Räumen des ADFC Leipzig e.V.. Dies sollte nicht länger als zwei Stunden dauern, danach treffen wir uns vielleicht noch in Kleingruppen oder trinken noch gemeinsam ein Getränk.

Aktuell arbeiten wir bei XR Leipzig in Stadtteilgruppen, Arbeitsgruppen und Projektgruppen. Es gibt eine Gruppe im...
- Westen (Diese trifft sich meist Mittwochs um 19 Uhr im Pizza LAB)
- Osten (Diese trifft sich meist Montags um 19 Uhr im Pöge-Haus)
- Süden (Diese trifft sich am 13.11 um 19 Uhr im ADFC Leipzig e.V.)

Zudem gibt es viele Arbeits- und Projektgruppen. Wenn du aktiv werden möchtest, dann komme gerne auf eines der Treffen vorbei (egal ob west, ost, süd) Zur Einführung in unsere Arbeitsweise gibt es zusätzlich offene Kennenlern // Onboarding-Treffen, die regelmäßig angeboten werden. Gerne kannst du auch eine Mail an leipzig@extinctionrebellion.de schreiben, um weitere Fragen los zu werden oder unseren Newsletter zu bekommen.

Wir freuen uns auf dich! Alle sind willkommen, so wie sie sind!
XR Leipzig Süd

***

The city council has decided on a climate emergency in Leipzig. But words must be followed by deeds! At the same time, we know that climate neutrality by 2050 is far too late and not compatible with the Paris Climate Agreement. It is therefore clear that there is still a lot to be done.

You want to become active for survival on this planet and work together with us against the climate catastrophe? Then come to our open meeting, on Wednesday at 19 o'clock in the rooms of the ADFC Leipzig e.V.. This should not last longer than two hours, afterwards we meet in AGs or have a drink together.

Currently we are working at XR Leipzig in district groups, working groups and project groups. There is one group in the...
- West (They meet mostly on Wednesdays at 19 o'clock in the Pizza LAB)
- East (These usually meet on Mondays at 19 o'clock in the Pöge-Haus)
- South (This meets on 13.11 at 19 o'clock in the ADFC Leipzig e.V.)

In addition, there are many working and project groups. If you want to become active, then come to one of the meetings (no matter if west, east, south). For an introduction to our way of working there are additional open meetings // Onboarding meetings, which are offered regularly. You are also welcome to send an email to leipzig@extinctionrebellion.de to get rid of further questions or to receive our newsletter.

We are looking forward to you! Everyone is welcome as they are!