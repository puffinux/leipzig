---
id: '2347680478797164'
title: Offenes Treffen XR Leipzig
start: '2019-04-24 18:30'
end: '2019-04-24 21:00'
locationName: null
address: 'Georg-Schwarz-Straße 10, 04177 Leipzig'
link: 'https://www.facebook.com/events/2347680478797164/'
image: 57460094_391954544982120_3139423589304893440_n.jpg
teaser: In London herrscht aktuell der Ausnahmezustand! Extinction Rebellion legt seit fast 7 Tagen die zentralen Punkte der öffentlichen Infrastruktur lahm u
recurring: null
isCrawled: true
---
In London herrscht aktuell der Ausnahmezustand! Extinction Rebellion legt seit fast 7 Tagen die zentralen Punkte der öffentlichen Infrastruktur lahm und es ist ungewiss, wie lange das noch weitergeht. Denn die Polizei und das Justizsystem sind überfordert, sodass XR die Regierung massiv unter Druck setzt. In diesem Moment spricht Greta Thunberg auf einer Blockade.

In Deutschland und in Leipzig befindet sich die Bewegung noch im Aufbau. Doch auch wir haben schon einige Aktionen durchgeführt, waren bei der Brückenblockade in Berlin und planen weitere Aktionen. Zudem vernetzen wir uns in der Klimabewegung und intern als Extinction Rebellion immer mehr, um auch größere Aktionen zu planen.

Um Aktionen, Vernetzung und unsere Struktur soll es bei unserem nächsten offenen Treffen gehen. Falls du dabei sein möchtest, dann komm einfach um 18:30 Uhr in die Ladenfläche der Georg-Schwarz-Straße 10 (Pizza Lab). Wir geben allen, die neu sind, vor dem Treffen gerne eine kurze Einführung zu der Bewegung und der Ortsgruppe in Leipzig. Um 19:00 Uhr geht dann das Haupttreffen los. :)

Die Themen:
- Einführung / Transparenz in den Gruppenprozess / Verantwortlichkeiten
- Organisation von Skillsharings / Wer kann was?
- Anstehende Aktionen
- Vernetzung in der Klimabewegung (Klimatresen / Workshop / was passiert im Sommer)
- Social Media und Öffentlichkeitsarbeit

Wir freuen uns auf alle, die vorbeikommen wollen.
Bis bald!