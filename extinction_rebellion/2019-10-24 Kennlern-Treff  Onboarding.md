---
id: "457118871591750"
title: Kennlern-Treff // Onboarding
start: 2019-10-24 18:30
end: 2019-10-24 20:30
locationName: Pizza LAB
address: Georg-Schwarz-Straße 10, 04177 Leipzig
link: https://www.facebook.com/events/457118871591750/
image: 73228703_512788789565361_6782172927149211648_n.jpg
teaser: Lerne deine XR-Ortsgruppe kennen! Unser nächstes Onboarding für interessierte
  Rebell*innen findet am Donnerstag um 18:30 Uhr in der XR-Dezentrale (Geo
isCrawled: true
---
Lerne deine XR-Ortsgruppe kennen! Unser nächstes Onboarding für interessierte Rebell*innen findet am Donnerstag um 18:30 Uhr in der XR-Dezentrale (Georg-Schwarz-Straße 10, neben dem Pizza Lab) statt. Hier bekommst du einen Einblick, wie wir als Ortgruppe funktionieren und wie du dich am besten einbringen kannst! Wir freuen uns auf dich!

Get to know your local XR group! Our next Onboarding for interested rebels will take place this Thursday, 24th October, at 6:30 p.m. at the XR-Dezentrale (Georg-Schwarz-Straße 10, next to Pizza Lab). You will get some insight into how our local group works and how you can get involved! We’re looking forward to seeing you there!

Love & Rage
XRLE