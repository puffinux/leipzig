---
id: '308010266523932'
title: Extinction Rebellion Leipzig 1. Offenes Treffen
start: '2019-02-28 16:00'
end: '2019-02-28 18:30'
locationName: Laden auf Zeit
address: '51  Kohlgarten Straße, 04315 Leipzig'
link: 'https://www.facebook.com/events/308010266523932/'
image: 52840276_359676391543269_3656728855097376768_n.jpg
teaser: 'Nach dem Auftakts- Vortrag über XR am 21.02. sind erneut alle, die mit uns die Rebellion in Leipzig aufbauen wollen oder einfach mehr über die Bewegun'
recurring: null
isCrawled: true
---
Nach dem Auftakts- Vortrag über XR am 21.02. sind erneut alle, die mit uns die Rebellion in Leipzig aufbauen wollen oder einfach mehr über die Bewegung erfahren möchten, herzlich eingeladen, zum ersten offenen Treffen vorbeizukommen: zum Schnacken, Planen und Vernetzen. 