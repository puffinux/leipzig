---
id: '451461478931925'
title: Die In auf dem Willy Brandt Platz
start: '2019-03-30 14:00'
end: null
locationName: null
address: 'Willy-Brandt-Platz, 04109 Leipzig'
link: 'https://www.facebook.com/events/451461478931925/'
image: 56303433_378318689679039_7022831687184678912_n.jpg
teaser: 'Uns rennt die Zeit davon.  Wir befinden uns in einem Zeitalter des Massenaussterbens. Jeden Tag sind es etwa 200 Spezies – Pflanzen, Insekten, Säugeti'
recurring: null
isCrawled: true
---
Uns rennt die Zeit davon.

Wir befinden uns in einem Zeitalter des Massenaussterbens. Jeden Tag sind es etwa 200 Spezies – Pflanzen, Insekten, Säugetiere – die keine Lebensgrundlage mehr finden und für immer verschwinden. Für die Stabilität unseres Ökosystems ist das katastrophal. Dieser rapide Verlust der Artenvielfalt wird auch für die Verursacher, uns Menschen, schwerste Folgen haben.

Es steht doch längst fest, wie dringend die Situation ist. Der Ausstoß von Treibhausgasen steigt stetig an, Polkappen und Permafrost schmelzen, Wetterextreme nehmen zu, die Welt ist überzogen von sichtbaren und unsichtbaren Plastikteilen. Und dagegen gemacht wurde so gut wie nichts.

Die Menschheit schaufelt sich ihr eigenes Grab, ohne Pause, immer schneller und schneller.
Gegen diesen Wahnsinn müssen wir ankämpfen, mit all der Lebendigkeit die uns noch bleibt!

Wir fordern von der Politik, die Probleme nicht mehr auf die lange Bank zu schieben, sich endlich mit der Dringlichkeit der Klima- und Umweltkrise auseinanderzusetzen und dementsprechend zu handeln! 
Wir brauchen konkrete und umfassende Pläne mit Mut zu Veränderung statt vage Abmachungen, die dann doch nicht eingehalten werden.

Und dafür gehen wir auf die Straße.

Unsere erste Aktion als Extinction Rebellion Leipzig wird am Samstag, den 30. März, auf dem Willy-Brandt-Platz stattfinden.
Wir veranstalten einen Die-In, um darzustellen, was auf uns zukommt. Denn wenn nicht bald gehandelt wird, sieht es für uns alle sehr düster aus.

MITMACHEN:
Alle sind eingeladen sich unserer Performance anzuschließen. Wir treffen uns um 13:45 um sie nochmal durchzugehen. 

PERFORMANCE:
Wir zeigen, wie die Existenz von Menschen auf der ganzen Welt in der Zukunft durch den Klimawandel bedroht sein wird, wenn wir nicht handeln. In der Performance werden schreckliche Zukunftszenarion von der Moderation laut vorgetragen, die nach und nach dazu führen, dass wir "sterben" werden, also uns auf den Boden legen werden. Beispiel: "Im Jahre 2049 kommt es zu einer Hitzewelle über Leipzig, 450 Menschen sterben", daraufhin fallen einige hin. Je weiter wir in der Zukunft voranschreiten, desto mehr Panik bekommen die noch "Überlebenden". Am Ende liegen wir alle auf dem Boden. Macht einfach mit!

Hier das Event für Menschen ohne Facebook:
https://gettogether.community/events/1053/die-in-auf-dem-richard-wagner-platz/?fbclid=IwAR0sAM_RGHB8ZtXRNWAzVrU5brMnHDKIlIgeqRT18Bf_TRxoACuR6GVFjfA

