---
id: '2694270473979116'
title: 'Leipzig: Workshop mit Rechtsanwalt Jürgen Kasek'
start: '2019-06-04 19:00'
end: '2019-06-04 21:00'
locationName: Pöge-Haus
address: 'Hedwigstraße 20, 04315 Leipzig'
link: 'https://www.facebook.com/events/2694270473979116/'
image: 60786293_406277183549856_6760820647561003008_n.jpg
teaser: 'Workshop mit Rechtsanwalt Jürgen Kasek für Aktionen mit Extinction Rebellion:  Wir steuern auf eine umfassenden ökologischen und gesellschaftlichen Ko'
recurring: null
isCrawled: true
---
Workshop mit Rechtsanwalt Jürgen Kasek für Aktionen mit Extinction Rebellion:

Wir steuern auf eine umfassenden ökologischen und gesellschaftlichen Kollaps zu. Extinction Rebellion organisiert Aktionen, um Regierungen, Medien und die Gesellschaft dazu zu bringen, endlich das Ausmaß der Katastrophe anzuerkennen und den Tatsachen entsprechend zu handeln. Die Zeit läuft uns davon. Wir müssen jetzt den Kurs ändern und handeln!!

Auch in Leipzig wollen wir mit gewaltfreien Aktionen des zivilen Ungehorsams gemeinsam dafür sorgen, dass die Probleme nicht mehr ignoriert werden können, und den nötigen politischen Druck für grundlegende Änderungen in Wirtschaft und Gesellschaft aufbauen. Seid ihr dabei?

Damit wir einen besseren Überblick über die rechtlichen Voraussetzungen und Konsequenzen unserer Aktionen bekommen und dabei informiert und organisiert auftreten können, veranstalten wir verschieden Workshops. 
Am 04. Juni geht es um rechtliche Fragen, welche mit zivilem Ungehorsam einhergehen. Der Leipziger Rechtsanwalt Jürgen Kasek wird über rechtliche Konsequenzen bei Straßenblockaden und unangekündigten Versammlungen und dem Umgang mit der Polizei und Passant*innen sprechen. Gerne könnt ihr auch eure eigenen Fragen zu dem Thema mitbringen. 

Alle Menschen, ob aktiv bei XR oder einfach interessiert, sind zu dieser Veranstaltung eingeladen!