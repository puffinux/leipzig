---
id: "2210498829252612"
title: Kennlern-Treff // Onboarding
start: 2019-11-06 19:30
end: 2019-11-06 21:00
locationName: Mühlstrasse 14 e.V.
address: Mühlstraße 14, 04317 Leipzig
link: https://www.facebook.com/events/2210498829252612/
image: 74667905_524871281690445_1261429163235999744_n.jpg
teaser: Lerne deine XR-Ortsgruppe kennen!   Vielleicht hast du schon von uns über
  Bekannte einiges mitbekommen? Oder du hast einen Artikel gelesen? Vielleicht
isCrawled: true
---
Lerne deine XR-Ortsgruppe kennen! 

Vielleicht hast du schon von uns über Bekannte einiges mitbekommen? Oder du hast einen Artikel gelesen? Vielleicht auch eine Aktion gesehen? Oder vielleicht warst du sogar bei einer Veranstaltung von uns? Oder auch schon mal bei einem Offenen Treffen, aber etwas überfordert von den vielen Menschen und der komplizierten Struktur?

Am besten zu kommst einfach mal zum Kennlern-Treff vorbei. Hier bekommst du einen Einblick, wie wir als Ortgruppe funktionieren und wie du dich am besten einbringen kannst! Wir freuen uns auf dich!

//

Get to know your local XR group!  You will get some insight into how our local group works and how you can get involved! We’re looking forward to seeing you there!

Love & Rage