---
id: leipzig-liest-ernst-paul-doerfler-nestwaerme-was-wir-von-voegeln-lernen-koennen-20-uhr
title: Was wir von Vögeln lernen können – Klimaschutz nach Vogelart, 18 Uhr
start: 2020-02-06 18:00
address: Café im Haus der Demokratie, Bernhard-Göring-Str. 152, 04277 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/leipzig-liest-ernst-paul-doerfler-nestwaerme-was-wir-von-voegeln-lernen-koennen-20-uhr/
image: Spatzen_im_Wildpark__Jana_Burmeister___6_.jpg
isCrawled: true
---
Klimaschutz, Gewaltverzicht, soziales Miteinander Vögel machen manches besser und nachhaltiger als wir Menschen. Wir können jede Menge von ihnen lernen, um die ökologischen und sozialen Herausforderungen unserer Zeit zu meistern.
Der vielfach ausgezeichnete Naturschützer Ernst Paul Dörfler hat ein berührendes Buch über das geheime Leben der Vögel geschrieben, die oft friedvoller und achtsamer miteinander umgehen als wir Menschen. Uns erwartet ein spannendes Plädoyer für einen nachhaltigen Umgang mit der Natur – und eine augenzwinkernde Aufforderung, das eigene Leben hin und wieder aus einer neuen Perspektive zu betrachten.