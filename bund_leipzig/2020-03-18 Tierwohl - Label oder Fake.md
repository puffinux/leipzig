---
id: tierwohl-label-oder-fake
title: Tierwohl - Label oder Fake?
start: 2020-03-18 18:00
end: 2020-03-18 20:00
address: Café im Haus der Demokratie, Bernhard-Göring-Str. 152, 04277 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/tierwohl-label-oder-fake/
image: Schweinsschnauze__Jana_Burmeister__weboptimiert.jpg
isCrawled: true
---
Der Arbeitskreis Landwirtschaft und Ernährung lädt zu einem Vortrag mit anschließender Diskussionsrunde zum Thema Tierwohl, Erzeugung und Kontrolle ein.