---
id: gemeinschaftliche-weihnachtsbaeckerei
title: Schmetterlinge und Politik, 18 Uhr
start: 2020-01-24 18:00
end: 2020-01-24 20:00
address: wird noch bekannt gegeben
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/gemeinschaftliche-weihnachtsbaeckerei/
image: schwalbenschwanz__Guy_Peer__weboptimiertjpg.jpg
isCrawled: true
---
Guy Pe'er von der Ortsgruppe Ost hält einen Vortrag über die Verbindung zwischen Insektenrückgang und Agrarpolitik. Wie können Schmetterlinge einen Unterschied machen? Fängt hier die friedliche Revolution der Insekten an? Mit anschließender Diskussion.