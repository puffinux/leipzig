---
id: diy-baumwollsaeckchen-wattepads-naehen
title: DIY Baumwollsäckchen & Wattepads nähen
start: 2020-03-21 15:00
end: 2020-03-21 17:00
address: wird noch bekannt gegeben
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/diy-baumwollsaeckchen-wattepads-naehen/
image: DIY_Watte_webo.jpg
isCrawled: true
---
Aus alten Stoffen etwas Neues machen. Waschbare Abschminkpads oder Baumwollsäckchen ganz einfach und schnell selber nähen. Wenn du noch alte Handtücher oder andere Stoffe zu Hause hast, die bei 60° gewaschen werden können, dann bring sie am besten mit.   






	Mehr Informationen
	
		Um Anmeldung wird gebeten unter anmeldung(at)bund-leipzig.de