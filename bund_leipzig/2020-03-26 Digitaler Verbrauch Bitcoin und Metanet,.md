---
id: digitaler-verbrauch-bitcoin-und-metanet-19-uhr
title: "Digitaler Verbrauch: Bitcoin und Metanet, 19 Uhr"
start: 2020-03-26 19:00
end: 2020-03-26 21:00
address: Haus der Demokratie, Bernhard-Göring-Str. 152, 04277 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/digitaler-verbrauch-bitcoin-und-metanet-19-uhr/
image: bitcoins_webo.jpg
isCrawled: true
---
Bitcoin und Metanet - Was ist das? Wozu soll das gut sein?
In einem weiteren Vortrag der Reihe "Digitaler Verbrauch" wird auf gezeigt, wie Bitcoin und die zugrunde liegenden Technologien (wie Blockchain, Proof of Work) funktionieren. Wieso es einen stetig steigenden Energieverbrauch hat und wieso dieser nicht zwangsläufig schlecht für die Umwelt sein muss. Zudem wird ein kleiner Vergleich zwischen Proof of Work und den Alternativen der Block-Erschaffung diskutiert. Sowie wird es eine kleine Einführung ins das Metanet geben. Nach dem Vortrag soll noch Zeit für Diskussion mit dem Publikum sein.