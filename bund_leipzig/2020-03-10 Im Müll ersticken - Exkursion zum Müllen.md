---
id: treffen-zur-vorbereitung-der-europaeischen-woche-abfallvermeidung-20uhr-im-krimzkrams-georg-schwa
title: Im Müll ersticken? - Exkursion zum Müllentsorgungsanlage Cröbern, 13 Uhr
start: 2020-03-10 13:00
end: 2020-03-10 16:30
address: Parkplatz im Hof des Haus der Demokratie, Bernhard-Göring-Straße 152, 04277
  Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/treffen-zur-vorbereitung-der-europaeischen-woche-abfallvermeidung-20uhr-im-krimzkrams-georg-schwa/
image: 2018-10-26_Tour_de_Muell_MBA_Croebern_-_Foto_Sebastian_Gerstenhoefer__3__weboptimiert.jpg
isCrawled: true
---
Seit 1990 wächst das Müllaufkommen, beim Verpackungsmüll ist Deutschland trauriger Spitzenreiter. Andererseits gilt nicht erst seitdem wir uns des Klimawandels bewusst sind: der beste Müll ist der, der nicht entsteht. Warum gibt es also immer mehr Müll, was passiert damit und wie kann man selbst zur Müllvermeidung beitragen? Mit Führung und Vortrag.






	Mehr Informationen
	
		Anmeldung bis 02.03.2020 unter anmelden@bund-leipzig.de