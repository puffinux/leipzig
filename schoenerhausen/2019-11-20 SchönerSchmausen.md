---
id: "2423013551301151"
title: SchönerSchmausen
start: 2019-11-20 18:00
end: 2019-11-20 21:00
address: Eisenbahnstr. 182
link: https://www.facebook.com/events/2423013551301151/
image: 74915085_1836669879810174_2685664692529004544_n.jpg
teaser: Willkommen zur Küfa bei SchönerHausen, welche in dieser Jahreszeit eher selten
  stattfindet. Es wird rustikal gemütlich im beheiztem Hinterhaus mit kös
isCrawled: true
---
Willkommen zur Küfa bei SchönerHausen, welche in dieser Jahreszeit eher selten stattfindet. Es wird rustikal gemütlich im beheiztem Hinterhaus mit köstlichen Kreationen und leckeren Getränken. Kommt vorbei!

Wir laden herzlichst zum gemeinsamen Essen und Trinken in die Räumlichkeiten von Schönerhausen ein. Wir, das sind Mitglieder der MRT-Gruppe Leipzig (Erklärung, was MRT ist, findest du weiter unten).
Es wird ein veganes Gericht aus Kartoffeln und Kürbis mit gebratenem Gemüse geben.
Du kannst auch gerne später auf einen Drink vorbeikommen. Alkoholische und nichtalkoholische Getränke stehen bereit (inklusive Glühwein).

Bei Bedarf kannst du uns gerne Fragen zur Radikalen Therapie stellen oder erste Infos zu den Startplänen einer neuen Gruppe erhalten. Die Einnahmen fließen komplett an Schönerhausen, deren Räume wir für unsere Treffen nutzen dürfen.

* Die Radikale Therapie (RT) ist ein selbstorganisiertes Therapieverfahren, welches in der Regel in Frauen- (FORT) und Männergruppen (MRT) praktiziert wird. Im Übrigen existieren auch gemischtgeschlechtliche bzw. queere sowie geschlechtlich nicht oder in anderer Weise definierte RT-Gruppen. RT basiert auf einem Menschenbild der humanistischen Psychologie und bezieht sich insbesondere auf die Transaktionsanalyse und das Co-Counseling. RT-Gruppen bestehen zumeist aus acht bis zwölf TeilnehmerInnen und treffen sich wöchentlich, wobei die Sitzungen im Rotationsmodus von jeweils zwei TeilnehmerInnen der Gruppe vorbereitet und geleitet werden. Dadurch tragen alle Personen gleichermaßen Verantwortung für den Gruppenprozess. (www.radikale-therapie.de)

+++Gegen Spende+++