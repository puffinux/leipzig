---
id: "460117194599482"
title: SchönerSchmausen
start: 2019-10-23 18:00
end: 2019-10-23 21:00
address: Eisenbahnstr. 182
link: https://www.facebook.com/events/460117194599482/
image: 75233815_1810059139137915_5010146322209046528_n.jpg
teaser: Willkommen zu SchönerSchmausen bei SchönerHausen in der goldenen
  Herbst-Edition. Schmackhaftes Essen, erfrischende Getränke, nette Leute. Kommt
  vorbei
isCrawled: true
---
Willkommen zu SchönerSchmausen bei SchönerHausen in der goldenen Herbst-Edition. Schmackhaftes Essen, erfrischende Getränke, nette Leute. Kommt vorbei!

+++Gegen Spende+++