---
id: '376351272915992'
title: TauschRausch im Mai - Schenken+Tauschen von Dingen mit Picknick
start: '2019-05-25 13:00'
end: '2019-05-25 17:00'
locationName: null
address: 'Lene-Voigt-Park, 04317 Leipzig'
link: 'https://www.facebook.com/events/376351272915992/'
image: 51666617_2475699569139231_8542743578705657856_n.jpg
teaser: 'Gehst Du sowieso jeden Tag an Lenes Tauschbude vorbei, willst aber auch mal echte Menschen treffen, die Dir was schenken?  Wenn Du Lust auf Picknick h'
recurring: null
isCrawled: true
---
Gehst Du sowieso jeden Tag an Lenes Tauschbude vorbei, willst aber auch mal echte Menschen treffen, die Dir was schenken?

Wenn Du Lust auf Picknick hast und ein paar Dinge loswerden möchtest, dann komm zu einem gemütlichen Samstag Nachmittag in den Lene-Voigt-Park. Wir lassen uns auf dem Rasen vor besagter Tauschbude nieder. 

Von zu Hause Mitbringen?
Nimm Dir eine Decke mit, Getränke, Snacks und ein paar Objekte zum Tauschen oder verschenken. Bring nicht mehr als Du bequem tragen kannst, und nur Klamotten und Dinge, die noch jemand gebrauchen kann.

Nach Hause Mitnehmen?
Alles was Du Dir ertauscht oder geschenkt bekommen hast, ob es ein Stück Kuchen der anderen oder ne tolle Hose ist. Natürlich muss am Ende sowohl Deine leere Saftflasche als auch alles andere wieder aus dem Park verschwinden: Zumüllen ist uncool.

Wir sorgen für eine Umkleidemöglichkeit und einen Spiegel zum Anprobieren und die lässige Atmosphäre.


Wir freuen uns auf Euch
KonsumGlobal Leipzig