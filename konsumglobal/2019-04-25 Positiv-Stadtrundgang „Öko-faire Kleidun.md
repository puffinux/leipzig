---
id: '264054494493948'
title: 'Positiv-Stadtrundgang: „Öko-faire Kleidung in Leipzig“'
start: '2019-04-25 16:00'
end: '2019-04-25 18:00'
locationName: null
address: 'Richard-Wagner-Platz, 04109 Leipzig'
link: 'https://www.facebook.com/events/264054494493948/'
image: 51856643_2479358682106653_753278654594678784_n.jpg
teaser: 'Anlassich der diesjährigen Fashion Revolution Leipzig/Halle wollen wir uns sechs Jahre nach dem Einsturz der Textilfabrik „Rana Plaza" anschauen, was'
recurring: null
isCrawled: true
---
Anlassich der diesjährigen Fashion Revolution Leipzig/Halle wollen wir uns sechs Jahre nach dem Einsturz der Textilfabrik „Rana Plaza" anschauen, was seitdem passiert ist – auch in Leipzig. Los geht es um 16 Uhr. Treffpunkt ist der Richard-Wagner-Platz (Pusteblumenspringbrunnen).

Der Rundgang führt durch die Leipziger Innenstadt zu Läden und Geschäften, die besonders nachhaltige und fair gehandelte Kleidung führen. Auf dem Weg durch die City erklärt Marcel Pruß vom Projekt KonsumGlobal Leipzig sowohl verschiedene Wirtschaftsmodelle an Leipziger Beispielen als auch globale Zusammenhänge.

Eingeladen sind alle Interessierten, die ihre eigene Lebensweise und ihren täglichen Kleidungs-Konsum überdenken und Alternativen dazu in Leipzig finden wollen. Am Ende erhalten alle Teilnehmenden einen Einkaufsführer mit Konsumalternativen.

Der Stadtrundgang dauert ca. zwei Stunden und ist kostenfrei.

Der Positiv-Stadtrundgang ist Teil der Fashion Revolution Leipzig/Halle. Alle weiteren Veranstaltungen gibt es auch hier: 
https://www.leipzig-handelt-fair.de/fashionrevolution/