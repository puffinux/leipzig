---
id: '2527397250829528'
title: Alles Kaffee?!
start: '2019-09-28 15:00'
end: '2019-09-28 18:00'
locationName: null
address: 'Zschochersche Straße 16, 04229 Leipzig'
link: 'https://www.facebook.com/events/2527397250829528/'
image: 68406069_565454020652561_6340292788927594496_n.jpg
teaser: 'KonsumGlobal Leipzig, der Eine Welt e.V. Leipzig und Café Chavalo präsentieren einen Kaffeenachmittag mit Mehrwert.  In gemütlcher Atmosphäre können d'
recurring: null
isCrawled: true
---
KonsumGlobal Leipzig, der Eine Welt e.V. Leipzig und Café Chavalo präsentieren einen Kaffeenachmittag mit Mehrwert.

In gemütlcher Atmosphäre können diverse Kaffeespezialitäten aus Fairem Handel verkostet und dabei gefachsimpelt werden. 
Der Fotograf Thomas Puschmann gibt zudem mit einem kleinen Vortrag und mit vielen Fotos einen Einblick in Leben, Kultur und Kaffeeanbau der indigenen Kaffeebauern (Nasa), südlich von Popayan in Kolumbien. Café Chavalo ergänzt zu Nicaragua und der Frage, wie fairer Handel funktioniert.

Die Veransattlung ist kostenfrei und findet im Weltladen Plagwitz statt.