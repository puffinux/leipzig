---
id: '404422610292494'
title: 'Radwanderkino: Von Fairtrade-Town zu Fairtrade-Town'
start: '2019-08-23 21:00'
end: '2019-08-23 23:59'
locationName: null
address: Kantgymnasium (Scharnhorststraße/KarLi)
link: 'https://www.facebook.com/events/404422610292494/'
image: 51692441_2475687935807061_1116963097806897152_n.jpg
teaser: 'Konsum Global Leipzig und der ADFC Leipzig e.V. laden ein, von Fairtrade-Town zu Fairtrade-Town zu radeln: Vom Süden Leipzigs geht es gemütlich nach M'
recurring: null
isCrawled: true
---
Konsum Global Leipzig und der ADFC Leipzig e.V. laden ein, von Fairtrade-Town zu Fairtrade-Town zu radeln: Vom Süden Leipzigs geht es gemütlich nach Markkleeberg und wieder zurück. Dabei sehen wir uns an diversen Häuserwänden Filmchen von Sukuma arts e.V. an ... Lustiges, Informatives und Provokantes zu Nachhaltigkeit und Konsum!

Neben dem Sound- und Videorad gibt es auch ein Lastenrad voller Getränke und Snacks aus Fairem Handel zur Stärkung.

Treffpunkt ist vor dem Kantgymnasium Ecke Scharnhorststraße/Karl-Liebknecht-Straße) und 21:00 geht es los.