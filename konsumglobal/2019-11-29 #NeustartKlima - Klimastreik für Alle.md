---
id: "496310504297487"
title: "#NeustartKlima - Klimastreik für Alle"
start: 2019-11-29 15:00
end: 2019-11-29 18:00
address: Simsonplatz, 04107 Leipzig
link: https://www.facebook.com/events/496310504297487/
image: 74667389_902050053529133_574441597983260672_n.jpg
teaser: "Unter dem #NeustartKlima werden am 29.11. in ganz Deutschland wieder
  hunderttausende Menschen für eine bessere Klimapolitik auf die Straßen
  gehen.  So"
isCrawled: true
---
Unter dem #NeustartKlima werden am 29.11. in ganz Deutschland wieder hunderttausende Menschen für eine bessere Klimapolitik auf die Straßen gehen.

So auch in Leipzig. Ab 15:00 Uhr auf dem Simson-Platz! 

Weitere Informationen folgen! 