---
id: '1898293140268966'
title: E E X kursion! Besuch bei der Energiebörse
start: '2019-06-11 14:00'
end: '2019-06-11 16:00'
locationName: null
address: European Energy Exchange
link: 'https://www.facebook.com/events/1898293140268966/'
image: 51854046_2494764303899424_1600289670434914304_n.jpg
teaser: Am heutigen Tag besuchen wir die European Energy Exchange (eex) - eine absolute Besonderheit in Leipzig.  Es ist die wichtigste Strom- und Energiebörs
recurring: null
isCrawled: true
---
Am heutigen Tag besuchen wir die European Energy Exchange (eex) - eine absolute Besonderheit in Leipzig. 
Es ist die wichtigste Strom- und Energiebörse in Europa - gleichzeitig findet hier der europäische CO2-Emissionshandel statt. 

Wie das alles funktioniert versuchen wir ja regelmäßig beim Klima-Rundgang: CO2 in Leipzig und der Welt zu klären. Hier haben wir aber die Chance, die ExpertInnen direkt selbst zu hören!
Normalerweise sind diese Räume nur in Gruppen betretbar, die ein nicht unerhebliches Entgeld zahlen - wir konnten aber einen für Euch kostenfreien Besuch organisieren.

Zwei MitarbeiterInnen werden uns herumführen, die Arbeitsweise erklären und schließlich unsere Fragen beantworten.
Hinweis: Diese Veranstaltung gibt einen etwas tieferen Einblick in die Arbeitsweise der Börse und ist für Kinder leider nicht geeignet.

Diese Veranstaltung findet im Rahmen der Leipziger Umwelttage 2019 statt. Da es sich um eine öffentlich-rechtliche Institution handelt und die Teilnehmendenzahl begrenzt ist, bitten wir Euch um Anmeldung mit komplettem Vor- und Familiennamen. Am Tag selbst müsst Ihr Euch über Euren Personalausweis oder Reisepass identifizieren lassen.
Anmelden könnt Ihr Euch bis zum 02.06.2019 per Email unter info(at)konsumglobal-leipzig.de oder auch über "0163 eins 7364 sieben 3".

