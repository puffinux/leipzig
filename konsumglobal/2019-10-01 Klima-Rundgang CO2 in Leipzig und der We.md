---
id: '2002067893429709'
title: 'Klima-Rundgang: CO2 in Leipzig und der Welt'
start: '2019-10-01 16:00'
end: '2019-10-01 18:00'
locationName: Neues Rathaus Leipzig
address: 'Martin-Luther-Ring 4, 04109 Leipzig'
link: 'https://www.facebook.com/events/2002067893429709/'
image: 52276850_2501732993202555_8095148707323838464_n.jpg
teaser: Der zweistündiger KlimaRundgang führt durch die Leipziger Innenstadt und behandelt abwechslungsreich verschiedene klimarelevante Lebensbereiche. Wir
recurring: null
isCrawled: true
---
 Der zweistündiger KlimaRundgang führt durch die Leipziger Innenstadt und behandelt abwechslungsreich verschiedene klimarelevante Lebensbereiche. Wir sehen so was in unserer Stadt und Region zum Thema Klimawandel passiert.

Das Besondere ist dabei der Klimarucksack, welchen wir nach und nach mit "CO2" befüllen werden. Daran sehen wir die konkreten Auswirkungen verschiedener Lebensweisen und Handlungen und finden heraus wie viel CO2 welche Alltagsstrategie einspart - also wie viel wir am Ende wieder aus unserem Rucksack rauswerfen können.

Wer Fragen stellt, kann auch Antworten bekommen - zum Beispiel beim Rundgang:

Besser die Gurke aus Spanien oder die Bio-Gurke in Plastik?
Lieber eine Scheibe Wurst als drei Scheiben Käse?
Wo kann ich CO2-Zertifikate kaufen?
Wie weit komme ich mit einem Kilo CO2?
Wer kümmert sich hier eigentlich um den Klimawandel?

Die Veranstaltung ist kostenfrei und für alle offen.

Immer 16:00, immer Start am Neuen Rathaus, aber dieses Jahr immer wieder mit speziellem Fokus:

MI    22.05.      Tag der biologischen Vielfalt
SA   08.06.      Tag der Ozeane
DO  04.07.      Hitzestress
MI    18.09.      Faire Woche / Nachhaltigkeitswochen
DI    01.10.      Weltvegetariertag