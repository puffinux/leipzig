---
id: "1284073695066465"
title: Verschenke–Markt zum Kauf-Nix-Tag
start: 2019-11-30 14:00
end: 2019-11-30 17:00
locationName: Mühlstrasse 14 e.V.
address: Mühlstraße 14, 04317 Leipzig
link: https://www.facebook.com/events/1284073695066465/
image: 51730745_2475684002474121_1909046567172571136_n.jpg
teaser: Die erprobte Tausch-Picknick-Variante für den Winter findet auch dieses Jahr
  wieder im Mühlstrasse 14 e.V. statt.  ***************************   Nix K
isCrawled: true
---
Die erprobte Tausch-Picknick-Variante für den Winter findet auch dieses Jahr wieder im Mühlstrasse 14 e.V. statt.

***************************   Nix Kaufen - Verschenken! ***************************

Zum Kauf-Nix-Tag 2019 - dem konsumkritischen Pendant zum Black Friday - wollen wir uns überflüssige und nicht mehr genutzte Dinge in lockerer Runde gegenseitig schenken. Ihr könnt aber natürlich auch untereinader tauschen, falls das Loslassen etwas schwerer fällt.  #kaufnixtag #buynothingday

Hier die FAQs zur Veranstaltung:

Was Du von Zuhause mitbringen kannst?
Einfach ein paar Objekte zum Verschenken oder Tauschen. Bring nicht mehr als Du bequem tragen kannst, und nur Klamotten und Dinge, die noch jemand gebrauchen kann.

Was Du dann mit nach Hause nehmen kannst?
Alles was Du geschenkt bekommen hast - und bitte auch Deine Dinge, die hier noch keinen neuen Haushalt gefunden haben!

Wir sorgen für eine Umkleidemöglichkeit zum Ausprobieren. Dazu wird es noch einiges Faires geben: Kaffee und Tee, Saft und größten teils veganes Gebackenes.
Bringt gern auch selbst Kuchen, Snacks oder anderes mit - hier kann man sich auch reich beschenken ;)

Zum Auslegen und Präsentieren Deiner Schätze gibt es genügend Tische auf zwei Etagen.

Mit ein wenig Musik und guten Leuten werden wir im Haus der Mühlstrasse 14 e.V. einen feinen, warmen und lohnenden Nachmittag haben. 

Wir freuen uns auf Euch
KonsumGlobal Leipzig

PS: Noch Fragen? Dann fragt :)