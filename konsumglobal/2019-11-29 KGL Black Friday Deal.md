---
id: "991450421052206"
title: KGL Black Friday Deal
start: 2019-11-29 16:00
end: 2019-11-29 18:30
address: Wilhelm-Leuschner-Platz, 04107 Leipzig
link: https://www.facebook.com/events/991450421052206/
image: 52608803_2508363535872834_594576207098413056_n.jpg
teaser: "!!! ENTFÄLLT!!!  Der KGL Black Friday Deal entällt zugunsten des
  #NeustartKlima - Klimastreik für Alle. Wir sehen uns da.   Keine Lust auf
  Wühltisch-"
isCrawled: true
---
!!! ENTFÄLLT!!!

Der KGL Black Friday Deal entällt zugunsten des #NeustartKlima - Klimastreik für Alle. Wir sehen uns da.


Keine Lust auf Wühltisch- und Schnäppchenschlacht mit den anderen Kaufwütigen am Black Friday?

Dann schau hier beim konsumkritischen Stadtrundgang vorbei. Wir kommen an verschiedenen Orten des modern (Sklaven)handels vorbei, hören und sehen Probleme und lernen Alternativen des globalen Warenaustausches kennen. Dabei geht es um Elektronisches, Klamotten und auch ein bisschen Schokolade - fast wie so ein ganz normaler Black Friday Nachmittag...

Wir bieten hier auf mehrfache Nachfrage die einmalige Gelegenheit unser Jugendprogramm für Erwachsene aufbereitet zu erleben: öffentlich und - das ist der große Black Friday Hammer Deal: kostenfrei!

Wir treffen uns am Wilhelm-Leuschner-Platz (S-Bahn Eingang zur Innenstadt).