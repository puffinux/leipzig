---
id: '2282097465174115'
title: 'Positiv-Stadtrundgang: "Nachhaltig leben, fair einkaufen in Leipzig"'
start: '2019-06-15 16:00'
end: '2019-06-15 18:00'
locationName: null
address: Richard Wagner Platz Leipzig
link: 'https://www.facebook.com/events/2282097465174115/'
image: 51575799_2494774687231719_2395864604614852608_n.jpg
teaser: KonsumGlobal Leipzig startet in diesem Jahr nur mit  "Sonderöffnungszeiten" zu dem beliebten Positiv-Stadtrundgang. Los geht es jeweils 16 Uhr. Treffp
recurring: null
isCrawled: true
---
KonsumGlobal Leipzig startet in diesem Jahr nur mit  "Sonderöffnungszeiten" zu dem beliebten Positiv-Stadtrundgang. Los geht es jeweils 16 Uhr. Treffpunkt ist immer der Richard-Wagner-Platz (Pusteblumenspringbrunnen).

Der Rundgang führt durch die Leipziger Innenstadt zu Läden und Geschäften, die ein nachhaltigeres Leben ermöglichen und fair gehandelte Produkte führen. Auf dem Weg durch die City erklärt Marcel Pruß vom Projekt KonsumGlobal Leipzig sowohl verschiedene Wirtschaftsmodelle an Leipziger Beispielen als auch globale Zusammenhänge.

Eingeladen sind alle Interessierten, die ihre eigene Lebensweise und ihren täglichen Konsum überdenken und Alternativen dazu in Leipzig finden wollen. Am Ende erhalten alle Teilnehmenden einen Einkaufsführer mit Konsumalternativen.

Der Stadtrundgang dauert ca. zwei Stunden, ist kostenfrei und findet im Rahmen der Leipziger Umwelttage 2019 statt.

Weitere Termine 2019:
Samstag, 11.05. im Rahmen des WorldFairtradeDay
Samstag, 15.06. im Rahmen der Leipziger Umwelttage 2019
Samstag, 21.09. im Rahmen der Faire Woche
Sonntag, 29.09. im Rahmen der 43. Leipziger Markttage 2019