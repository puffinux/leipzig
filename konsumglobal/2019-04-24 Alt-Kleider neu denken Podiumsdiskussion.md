---
id: '345335139430859'
title: 'Alt-Kleider neu denken: Podiumsdiskussion'
start: '2019-04-24 19:30'
end: '2019-04-24 21:00'
locationName: UNIVERSITÄT LEIPZIG
address: 'Augustusplatz 10, 04109 Leipzig'
link: 'https://www.facebook.com/events/345335139430859/'
image: 51417518_2479383698770818_2497518315778342912_n.jpg
teaser: '"(Alt)kleider - neu denken! Wege von der Wegwerf-Mode hin zu einem nachhaltigen Kleiderschrank"  Podiumsdiskussion -----------------------------------'
recurring: null
isCrawled: true
---
"(Alt)kleider - neu denken! Wege von der Wegwerf-Mode hin zu einem nachhaltigen Kleiderschrank"

Podiumsdiskussion
---------------------------------------------------------------------------

Wir kaufen immer mehr Kleidung zu immer günstigeren Preisen und in immer schlechterer Qualität - um sie dann immer kürzer zu tragen und schließlich zum Großteil in Altkleidercontainern oder sogar dem Hausmüll zu „entsorgen“. 

Und dann?: Welche Wege nimmt unsere Altkleidung? Welche sozialen und wirtschaftlichen Folgen hat der Export unserer Altkleidung, zum Beispiel in Ghana? Welche ökologischen Folgen hat unser Kleiderkonsum? 

Wie weiter?: Welche Potenziale stecken in Altkleidung als Ressource? Wie können wir wegkommen von Mode als Wegwerfprodukt hin zu einem nachhaltigen Kleiderschrank? Was können und müssen Unternehmen, Regierungen und wir als BürgerInnen und VerbraucherInnen dafür tun?

Diese und weitere Fragen diskutieren:

- Verena Bax, Referentin für Umweltpolitik, NABU-Regionalverband Leipzig
- Kwabena Obiri Yeboah, MBA, Doktorant im International SEPT Program, Leipzig University und Gründer des Upcycling-Labels Koliko Wear in Ghana
- Anna Zeitler , Modedesignerin, Referentin für nachhaltigen (Kleidungs-)konsum Anna Zeitler - Nachhaltigkeit
Moderation: Anna Grasemann

Die Veranstaltung ist natürlich kostenfrei und die Diskussion ist am Ende offen für das Publikum, d.h. es dürfen und sollen Fragen gestellt und mitdiskutiert werden.

Die Veranstaltung findet statt:
Universität Leipzig (Campus Augustusplatz), Grimmaische Straße 12, Institutsgebäude der Wirtschaftswissenschaftlichen Fakultät, Seminarraum 1 (Eingang über Innenof oder Universitätsstraße)

Die Podiumsdiskussion ist Teil der Fashion Revolution Leipzig/Halle. Alle weiteren Veranstaltungen gibt es auch hier: 
https://www.leipzig-handelt-fair.de/fashionrevolution/