---
id: "349461599238801"
title: Der Faire Nikolaus kommt in die Fairtrade-Town Leipzig
start: 2019-12-06 14:00
end: 2019-12-06 17:00
address: Thomaskirchhof, 04109 Leipzig
link: https://www.facebook.com/events/349461599238801/
image: 53046558_2508287265880461_682000751739797504_n.jpg
teaser: "... Alle Jahre wieder ... kann es Konsum Global Leipzig nicht lassen und lädt
  den Fairen Nikolaus in die Fairtrade-Town Leipzig ein: Denn Leipzig hand"
isCrawled: true
---
... Alle Jahre wieder ... kann es Konsum Global Leipzig nicht lassen und lädt den Fairen Nikolaus in die Fairtrade-Town Leipzig ein: Denn Leipzig handelt fair.

Der überraschend wenig rüstige Edelmann bringt sein Glücksrad mit, an dem Groß und Klein drehen und Schokolade aus Fairem Handel gewinnen können. Nebenbei informiert sein Assistent über die globalen Arbeitsbedingungen im Kakaoanbau und warum der Nikolaus nun schon seit Jahren auf faire Schokolade setzt.

Der Nikolaus ist heute natürlich viel unterwegs, aber Ihr findet ihn von 14-17 Uhr auf dem Thomaskirchhof (Innenstadt) gegenüber des Weltladens.