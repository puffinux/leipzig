---
id: '463978344137105'
title: Open Fair - Ein Tässchen Kaffee beim Bach
start: '2019-05-11 09:00'
end: '2019-05-11 12:00'
locationName: null
address: 'Thomaskirchhof, 04109 Leipzig'
link: 'https://www.facebook.com/events/463978344137105/'
image: 51755796_2479372765438578_6846705691239907328_n.jpg
teaser: Konsum Global Leipzig und der Eine Welt e.V. Leipzig spannen zusammen und veranstalten am Weltladentag ein Faires Frühstück im Schatten von J.S.B.  Ko
recurring: null
isCrawled: true
---
Konsum Global Leipzig und der Eine Welt e.V. Leipzig spannen zusammen und veranstalten am Weltladentag ein Faires Frühstück im Schatten von J.S.B.

Kommt auf eine Tasse Kaffee und ein paar Happen aus Fairem Handel vorbei.

Konsum Global Leipzig informiert euch über den globalen Kaffeeanbau mit unserer Kaffeeausstellung. Dazu gibt es viel Spiel und Spass mit Kinder-Kaffeemahlen, Kaffeetassen-Schätzen, einem Kaffeequiz und Gewinnspiel. Als Preise winken leckere Kaffee-Schokolade-Kombinationen. 

Auf dem Frühstückstisch des Eine Welt e.V. Leipzig gibt es frischen Kaffee und allerlei weitere Leckereien aus Fairem Handel.

Fairer Handel und was das mit Euch, uns und allen anderen zu tun hat? Hier könnt Ihr es rausfinden.

Kommt vorbei!