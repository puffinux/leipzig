---
id: '236269670640305'
title: „Und nach dem Konsum…?“ Teil 3 Kompostwerk-Tour Liemehna
start: '2019-06-13 16:00'
end: '2019-06-13 17:30'
locationName: null
address: 'Zschettgauer Straße 3, 04838 Jesewitz OT Liemehna'
link: 'https://www.facebook.com/events/236269670640305/'
image: 51479871_2479273035448551_2004594471223689216_n.jpg
teaser: Nach den erfolgreichen Veranstaltungen "Und nach dem Konsum...?" in den vergangenen beiden Jahren beim Zweckverband Abfallwirtschaft Westsachsen und d
recurring: null
isCrawled: true
---
Nach den erfolgreichen Veranstaltungen "Und nach dem Konsum...?" in den vergangenen beiden Jahren beim Zweckverband Abfallwirtschaft Westsachsen und der Stadtreinigung Leipzig, setzen wir die Reihe 2019 fort.

Zusammen mit der AG Abfall des Ökolöwe - Umweltbund Leipzig e.V. lädt KGL zu einem Besuch in das Kompostwerk Liemehna. Bei der geführten Tour über das Betriebsgelände erfahren wir was mit unserem Abfall aus der Biotonne passiert. Außerdem gehen wir der Frage nach, was wirklich alles in die Biotonne geworfen werden darf und welche Probleme Plastik dabei verursachen.  

Die Veranstaltung ist kostenfrei, dauert 1,5 Stunden und findet im Rahmen der Leipziger Umwelttage 2019 statt.

Wir treffen uns um 16.00 Uhr vor Ort in der Zschettgauer Str. 3, 04838 Jesewitz OT Liehmena. (51.448588, 12.539535)

Individuelle Anreise 
(mit PKW z.B.) Parkmöglichkeiten vorhanden oder

Gemeinsame Anreise 
mit S-Bahn S4 und Fahrrad, TP: 15.15 Uhr S-Bahnhof Leuschnerplatz (auf dem Bahnsteig), Kosten für Ticket 2x 3,40 Euro (für Hin- und Rückfahrt: Bitte möglichst im Vorfeld besorgen)

Eine Voranmeldung ist aufgrund begrenzter Platzzahl unbedingt erforderlich. Dafür gibt es zwei Möglichkeiten:
info(at)KonsumGlobal-Leipzig.de oder 0172 3 72 45 76