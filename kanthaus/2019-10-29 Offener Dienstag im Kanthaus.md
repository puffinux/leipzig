---
id: "2380486148834080"
title: Offener Dienstag im Kanthaus
start: 2019-10-29 16:00
end: 2019-10-29 18:00
locationName: Kanthaus
address: Kantstrasse 20, 04808 Wurzen
link: https://www.facebook.com/events/2380486148834080/
image: 61585324_2852653108138734_1297731296268648448_n.jpg
teaser: Liebe Leute, Das Kanthaus ist aus der Sommerpause zurück und öffnet wieder
  Dienstags seine Türen für die Nachhaltigkeit. Herzliche Einladung zum Herum
isCrawled: true
---
Liebe Leute, Das Kanthaus ist aus der Sommerpause zurück und öffnet wieder Dienstags seine Türen für die Nachhaltigkeit. Herzliche Einladung zum Herumschlendern im Verschenkeladen, zum Retten von genießbaren Lebensmitteln im Fair-Teiler und Skill-Sharing im Repair-Cafe. 

Der Verschenkeladen..

..ist unser neuestes Angebot. Er funktioniert wie ein normaler Haushalts- und Klamottenladen. Der einzige Unterschied ist, dass in diesem Laden kein Geld benötigt wird. Gebrauchte Gegenstände in gutem Zustand können einfach mitgenommen werden. Wenn ihr selbst etwas abzugeben habt, sagt uns einfach vorher Bescheid, damit wir nochmal drüber gucken können.

Der Tüftel-Treff bzw das Repair-Cafe

.. ist an alle Menschen gerichtet, die lernen möchten ihre kaputten Geräte unter Anleitung selbst zu reparieren. Darüber hinaus sind alle willkommen, die einfach Spaß daran haben sich über technische Themen auszutauschen, Ideen für neue Projekte zu entwickeln und diese dann gemeinsam in der Gruppe umzusetzen. Uns steht ein 3D-Drucker und eine CNC-Fräse zur Verfügung und freuen uns, wenn ihr auch Lust habt, diese mal auszuprobieren. 

Zum Fair-Teiler..
.. könnt ihr die Lebensmittel bringen, die zwar noch genießbar sind, aber ihr selbst nicht mehr verwerten könnt! Es ist ganz schön verrückt, wie viele gute Lebensmittel jeden Tag von Supermärkten weggeschmissen werden. Deswegen versuchen wir so viele wie möglich zu retten! Kommt vorbei, wenn ihr mehr darüber erfahren wollt, teilt eure Lebensmittel und macht gerne mit dabei Lebensmittelverschwendung in Supermärkten und Privathaushalten einzudämmen! :-) 

Alle sind willkommen. Wenn ihr Lust habt, auch mal eine Schicht zu übernehmen, meldet euch bei hallo@kanthaus.online :-)  