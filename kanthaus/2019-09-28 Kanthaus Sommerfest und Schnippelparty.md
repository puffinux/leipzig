---
id: '389534968393143'
title: Kanthaus Sommerfest und Schnippelparty
start: '2019-09-28 16:00'
end: '2019-09-28 22:00'
locationName: Kanthaus
address: 'Kantstrasse 20, 04808 Wurzen'
link: 'https://www.facebook.com/events/389534968393143/'
image: 70719091_3164542570283118_4454443593523789824_n.jpg
teaser: Hallo liebe FreundInnen des Kanthauses!  Spätsommerlich kündigt sich der Wechsel der Jahreszeiten an. Wir zögern den Herbst noch etwas hinaus und lade
recurring: null
isCrawled: true
---
Hallo liebe FreundInnen des Kanthauses!

Spätsommerlich kündigt sich der Wechsel der Jahreszeiten an. Wir zögern den Herbst noch etwas hinaus und laden euch zu unserem Sommerfest am 28. September 2019 ein!

Ab 16 Uhr gibt es eine Schnippelparty - gemeinsames Schnippeln und Kochen mit Musik. Gleichzeitg wollen wir euch über Projekte im und um das Kanthaus informieren.
Zum Abend entfachen wir die Feuerschalen und können in gemütlicher Stimmung den Sommer ausklingen lassen.

Was? Sommerfest im Kanthaus mit Schnippelparty
Wann? 28. September 2019 von 16 - 22 Uhr
Wo? Innenhof, Eingang Kantstr. 20-22, Wurzen

Essen und Getränke stellen wir zur Verfügung.
Wir freuen uns sehr, wenn ihr dabei seid!

Die BewohnerInnen vom Kanthaus