---
id: '1110445565823541'
title: Offener Dienstag - Feierliche Eröffnung
start: '2019-04-23 16:00'
end: '2019-04-23 18:00'
locationName: Kanthaus
address: 'Kantstrasse 20, 04808 Wurzen'
link: 'https://www.facebook.com/events/1110445565823541/'
image: 57358879_2729537070450339_2298636406278324224_n.jpg
teaser: 'Liebe alle,  Am 23. April wird der Verschenkeladen in der Kantstraße 22 feierlich eröffnet. Um 16 Uhr laden wir - die Bewohnerinnen und Bewohner des K'
recurring: null
isCrawled: true
---
Liebe alle,

Am 23. April wird der Verschenkeladen in der Kantstraße 22 feierlich eröffnet. Um 16 Uhr laden wir - die Bewohnerinnen und Bewohner des Kanthauses, sowie die Ortsgruppe Wurzen des BUND - die Anwohnerinnen und Anwohner der Stadt Wurzen ein dies mit uns zu feiern. Neben dem Verschenkeladen werden auch von 16 bis 18 Uhr der Tüfteltreff und Fair-Teiler stattfinden.  Unser Anliegen ist es mit diesen Angeboten Nachhaltigkeit in Wurzen zu fördern. 

DIE ANGEBOTE:

Der Verschenkeladen funktioniert wie ein normaler Haushalts- und Klamottenladen. Der einzige Unterschied ist, dass in diesem Laden kein Geld benötigt wird. Gebrauchte Gegenstände in gutem Zustand können einfach mitgenommen werden. Wir freuen uns wenn Wurzenerinnen und Wurzener uns informieren, falls sie etwas abzugeben haben. Ab dem 23. April wird der Verschenkeladen zunächst jeden Dienstag von 16 bis 18 Uhr geöffnet sein. Falls Interesse besteht, würden wir ihn gerne bald noch regelmäßiger öffnen.

Der Tüftel-Treff ist an alle Menschen gerichtet, die lernen möchten ihre kaputten Geräte unter Anleitung selbst zu reparieren. Darüber hinaus sind alle willkommen, die einfach Spaß daran haben sich über technische Themen auszutauschen, Ideen für neue Projekte zu entwickeln und diese dann gemeinsam in der Gruppe umzusetzen. Uns steht ein 3D-Drucker und eine CNC-Fräse zur Verfügung und freuen uns, wenn Wurzenerinnen und Wurzener mit diesen arbeiten möchten. 

Zum Fair-Teiler können Lebensmittel, die genießbar sind, allerdings nicht mehr selbst verwertet werden können, von jeder und jedem gebracht und abgeholt werden. Dieses Angebot ist nicht nur an bedürftige Menschen gerichtet, sondern an Wurzenerinnen und Wurzener, die wie wir Interesse daran haben, Lebensmittelverschwendung in Privathaushalten einzudämmen. 

Diese drei Angebote werden als „offener Dienstag“ jede Woche ab dem 23.04. von 16 bis 18 Uhr in der Kantstraße 22 stattfinden. Wir laden alle Bürgerinnen und Bürger ein, sich an diesen Angeboten zu beteiligen! 