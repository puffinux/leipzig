---
id: '437293903681867'
title: Repair-Café
start: '2019-03-26 17:00'
end: '2019-03-26 20:00'
locationName: Kanthaus
address: 'Kantstrasse 20, 04808 Wurzen'
link: 'https://www.facebook.com/events/437293903681867/'
image: 53716271_2646812402056140_6952381987168452608_o.jpg
teaser: 'Kaputte Geräte? Lust zu basteln? Interesse an Erfahrungsaustausch?  Komm vorbei: Jeden Dienstag ab 17 Uhr  Mehr Infos hier: https://kanthaus.online/de'
recurring: null
isCrawled: true
---
Kaputte Geräte? Lust zu basteln? Interesse an Erfahrungsaustausch?

Komm vorbei: Jeden Dienstag ab 17 Uhr

Mehr Infos hier: https://kanthaus.online/de/projects/repaircafe