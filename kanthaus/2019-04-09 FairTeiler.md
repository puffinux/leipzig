---
id: '607542239693423'
title: FairTeiler
start: '2019-04-09 19:00'
end: '2019-04-09 20:00'
locationName: Kanthaus
address: 'Kantstrasse 20, 04808 Wurzen'
link: 'https://www.facebook.com/events/607542239693423/'
image: 50506389_2544161745654540_6142131789688209408_n.jpg
teaser: 'Wir öffnen unsere Tür, um zu teilen! Bring mit, wovon du zuviel hast und/oder nimm mit, was du brauchst!  Dies ist ein Angebot für alle – nicht bloß d'
recurring: null
isCrawled: true
---
Wir öffnen unsere Tür, um zu teilen!
Bring mit, wovon du zuviel hast und/oder nimm mit, was du brauchst!

Dies ist ein Angebot für alle – nicht bloß die Armen und Bedürftigen! Lasst uns zusammen kommen, unser Essen miteinander teilen und etwas gegen all die sinnlose Verschwendung tun!