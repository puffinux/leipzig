---
id: '2380486128834082'
title: Offener Dienstag im Kanthaus
start: '2019-09-10 16:00'
end: '2019-09-10 18:00'
locationName: Kanthaus
address: 'Kantstrasse 20, 04808 Wurzen'
link: 'https://www.facebook.com/events/2380486128834082/'
image: 61585324_2852653108138734_1297731296268648448_n.jpg
teaser: 'Das Kanthaus läd neuerdings wöchentlich zum offenen Dienstag ein! :) Alle sind herzlich eingeladen unsere drei Angebote in Anspruch zu nehmen und sich'
recurring: null
isCrawled: true
---
Das Kanthaus läd neuerdings wöchentlich zum offenen Dienstag ein! :) Alle sind herzlich eingeladen unsere drei Angebote in Anspruch zu nehmen und sich einzubringen.

DIE ANGEBOTE:

Der Verschenkeladen..

..ist unser neuestes Angebot. Er funktioniert wie ein normaler Haushalts- und Klamottenladen. Der einzige Unterschied ist, dass in diesem Laden kein Geld benötigt wird. Gebrauchte Gegenstände in gutem Zustand können einfach mitgenommen werden. Wir freuen uns wenn Wurzenerinnen und Wurzener uns informieren, falls sie etwas abzugeben haben. Ab dem 23. April wird der Verschenkeladen zunächst jeden Dienstag von 16 bis 18 Uhr geöffnet sein. Falls Interesse besteht, würden wir ihn gerne bald noch regelmäßiger öffnen.

Der Tüftel-Treff..

.. ist an alle Menschen gerichtet, die lernen möchten ihre kaputten Geräte unter Anleitung selbst zu reparieren. Darüber hinaus sind alle willkommen, die einfach Spaß daran haben sich über technische Themen auszutauschen, Ideen für neue Projekte zu entwickeln und diese dann gemeinsam in der Gruppe umzusetzen. Uns steht ein 3D-Drucker und eine CNC-Fräse zur Verfügung und freuen uns, wenn Wurzenerinnen und Wurzener mit diesen arbeiten möchten. 

Zum Fair-Teiler..
.. können Lebensmittel, die genießbar sind, allerdings nicht mehr selbst verwertet werden können, von jeder und jedem gebracht und abgeholt werden. Dieses Angebot ist nicht nur an bedürftige Menschen gerichtet, sondern an Wurzenerinnen und Wurzener, die wie wir Interesse daran haben, Lebensmittelverschwendung in Privathaushalten einzudämmen. 

Alle sind willkommen. Wir suchen auch noch Menschen, die Lust haben, regelmäßig oder einmal Schichten zu übernehmen. 