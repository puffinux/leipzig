---
id: '421918671885525'
title: Foodsharing Mitbring-Brunch
start: '2019-02-09 11:00'
end: '2019-02-10 13:00'
locationName: Kanthaus
address: 'Kantstrasse 20, 04808 Wurzen'
link: 'https://www.facebook.com/events/421918671885525'
image: null
teaser: null
recurring: null
isCrawled: true
---
