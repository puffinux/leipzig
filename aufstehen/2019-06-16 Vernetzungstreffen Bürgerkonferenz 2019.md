---
id: '370149263588950'
title: Vernetzungstreffen Bürgerkonferenz 2019
start: '2019-06-16 10:00'
end: '2019-06-16 16:00'
locationName: Rahn Education
address: 'Salomonstraße 10, 04103 Leipzig'
link: 'https://www.facebook.com/events/370149263588950/'
image: 56178496_541548912918992_780770250630103040_o.jpg
teaser: 'Aufruf zum Bürgerkonferenz Vernetzungstreffen  Anmeldung unter: https://buergerkonferenz.com/bk_anmelden.php  Engagierte Bürger für Frieden und soz'
recurring: null
isCrawled: true
---
Aufruf zum Bürgerkonferenz Vernetzungstreffen

Anmeldung unter: https://buergerkonferenz.com/bk_anmelden.php

Engagierte Bürger für Frieden und soziale Gerechtigkeit
Motivation

In diesem Jahr finden in den Bundesländern Brandenburg, Sachsen und Thüringen Landtagswahlen statt. Aber welche Partei sollen die Bürgerinnen und Bürger wählen, wenn sie eine sozial gerechte, auf Frieden und Demokratie gerichtete linke Politik für unsere Länder wollen? Welche Parteien bei den letzten Wahlen auch gewählt wurden, die Politik blieb, bis auf kleine, unwesentliche Ausnahmen, stets die Gleiche. Begründet wird dies mit angeblich zwingenden, äußeren Bedingungen. Es wird behauptet, diese neoliberale Politik sei alternativlos. Es ist an der Zeit, dass die Bürgerinnen und Bürger ihren Willen zu einer Neuorientierung der Politik in unseren Bundesländern, zum Wohle der gesamten Bevölkerung, deutlich zum Ausdruck bringen.

Es hat sich daher, initiiert von https://www.aufstehen-le.de, ein Arbeitskreis aus mehreren Städten und Bundesländern gegründet, der ein Bürgerkonferenz Vernetzungstreffen, am 16. Juni 2019 von 10:00 bis 16:00 Uhr in der Rahn Schule in Leipzig organisieren und veranstalten will. Die Vorbereitungen mit Teilnehmern aus den Städten Chemnitz, Leipzig, Dresden, Plauen, Torgau, Halle/Saale und Magdeburg laufen bereits.

Weitere Informationen findet ihr in Kürze auch auf unserer Webseite (https://www.Buergerkonferenz.com).

Wir rufen alle Bürgerinnen und Bürger aus Sachsen, Sachsen-Anhalt und Thüringen sowie sämtliche Bürgerinitiativen, Vereine, Interessengruppen, etc. der demokratisch engagierten Zivilgesellschaft sowie der außerparlamentarischen Opposition auf, sich an der Bürgerkonferenz inhaltlich zu beteiligen, um die drängendsten Probleme in unserer Gesellschaft zu benennen, zu analysieren und Antworten zu ihrer Lösung zu erarbeiten. Wir bitten alle Interessierten, Kontakt mit uns aufzunehmen und ihre Vorstellungen einzubringen. 
Über unsere Webseite oder per Mail an: post@Buergerkonferenz.com

Bitte schreibt uns Euren Namen, Wohnort, E-Mail-Adresse, welche Gruppe, Initiative oder welchen Verein ihr vertretet und am besten auch gleich noch Eure Telefonnummer, damit wir Euch in die vernetzte Arbeit an dem Kongress einbinden können.

Parallel dazu rufen wir alle Aufgestandenen in sämtlichen anderen Bundesländern dazu auf, ebenfalls im gleichen Zeitraum ähnliche Bürgerkonferenzen zu veranstalten, um den bundesweiten Bürgerwillen nach Frieden und sozialer Gerechtigkeit in und für unsere Gesellschaft klar zu artikulieren und gemeinsame konstruktive, demokratische Forderungen an Landes- und Bundespolitik zu formulieren.

Damit die Bürgerkonferenz ein voller Erfolg wird, und möglichst viele Menschen (auch mit geringem Einkommen) teilnehmen können, brauchen wir eure Hilfe!

Mit euren Spenden finanzieren wir folgende Kostenpunkte:

Die Saalmiete für den Felsenkeller Leipzig
Werbemittel (Flyer, Plakate, Website) für die Veranstaltung
Technik und Ausstattung
Unterstützung für Anreise und Unterkunft für finanziell schwächere Interessenten.

Spenden können an folgendes Konto gerichtet werden:

Kontoinhaber:
WIBP e.V.
Konto:
DE69 8505 0300 0221 0751 51
(BIC: OSDDDE81XXX, Ostsächsische Sparkasse Dresden)

Oder über Crowdfunding unter:
https://www.leetchi.com/c/buergerkonferenz-2019

Wir freuen uns auf eine politisch vielfältige Beteiligung und bedanken uns vorab für eure Unterstützung.

