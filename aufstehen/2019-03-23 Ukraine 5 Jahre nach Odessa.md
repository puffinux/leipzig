---
id: '1243461042472585'
title: 'Ukraine: 5 Jahre nach Odessa'
start: '2019-03-23 20:00'
end: '2019-03-23 23:00'
locationName: Monopol-Leipzig
address: 'Haferkornstraße 15, 04129 Leipzig'
link: 'https://www.facebook.com/events/1243461042472585/'
image: 54432714_533944063679477_4269254744138055680_n.jpg
teaser: Kurz nach den Protesten auf dem Maidan in Kiew und dem Putsch an den damaligen ukrainischen Präsidenten Jaukowitsch kam es in der Hafenstadt Odessa zu
recurring: null
isCrawled: true
---
Kurz nach den Protesten auf dem Maidan in Kiew und dem Putsch an den damaligen ukrainischen Präsidenten Jaukowitsch kam es in der Hafenstadt Odessa zu einer der verheerendsten Pogrome in der jüngeren europäischen Geschichte.

Am 2. Mai 2014 verbrannten und erstickten im Gewerkschaftshaus von Odessa mehrere Dutzend Menschen. Mehrere starben nach ihrem Sprung aus dem Fenster, mit dem sie dem Flammentod entgehen wollten. Ein ultranationalistischer Mob hatte das Gebäude mit Molotow-Cocktails in Brand gesteckt. Über einen Seiteneingang war der Mob zudem mit Knüppeln in das Gebäude gestürmt und hatte Jagd auf Regierungskritiker gemacht, die in Büros Schutz gesucht hatten. Insgesamt 42 Menschen starben in und vor dem Gewerkschaftshaus, 210 Menschen wurden verletzt.

Wir möchten nun auf diese Veranstaltung hinweisen:

Der Politikwissenschaftler und Überlebende des Gewerkschaftshausbrandes in Odessa 2014, Oleg Muzyka, stellt nun sein Buch „Ukraine: Fünf Jahre nach Odessa“ vor.


WANN?   23. März, 20 Uhr
WO?        Haferkornstr. 15, 04129 Leipzig


Monopol, Haus 4, Atelier Malerei Brückner

