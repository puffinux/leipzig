---
id: '457070148171750'
title: Vollversammlung von Aufstehen Leipzig
start: '2019-07-05 18:00'
end: '2019-07-05 21:00'
locationName: Felsenkeller Leipzig
address: 'Karl-Heine-Straße 32, 04229 Leipzig'
link: 'https://www.facebook.com/events/457070148171750/'
image: 65057775_587846118289271_7701470390064775168_n.jpg
teaser: Was ist bei unserer Sammlungsbewegung in letzter Zeit passiert? Wo stehen wir? Wo wollen wir genau hin? Und wie kann das gelingen?  Nach dem Rückzug v
recurring: null
isCrawled: true
---
Was ist bei unserer Sammlungsbewegung in letzter Zeit passiert? Wo stehen wir? Wo wollen wir genau hin? Und wie kann das gelingen?

Nach dem Rückzug von Sahra Wagenknecht aus dem Kernteam von Aufstehen gab und gibt es große Verunsicherung, wie es mit der Bewegung weitergehen kann. Die Diskussionen förderten zahlreiche organisatorische Fehler aus der Anfangsphase zu Tage. Und auch was die Ziele und und Mittel von Aufstehen anbelangt, wurden zu viele Fragen bisher offen gelassen.

Dass es in den vergangenen Monaten ruhiger um Aufstehen geworden ist, kann man als Gewinn sehen, denn damit war Zeit, über diese Fragen nachzudenken - „innere Sammlung“ könnte man sagen.

Das diese Zeit nicht unproduktiv war, zeigen die Arbeitsergebnisse zahlreicher Gruppen in Leipzig und ganz Sachsen. Über diese wollen wir uns austauschen und die nächsten Meilensteine abstecken. Es wird Zeit, unsere Positionen konkret zu formulieren und umzusetzen. 