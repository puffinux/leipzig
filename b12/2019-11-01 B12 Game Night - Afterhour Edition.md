---
id: "2535797516656932"
title: B12 Game Night - Afterhour Edition
start: 2019-11-01 19:00
end: 2019-11-02 01:00
locationName: Wohn- und Kulturprojekt B12
address: Braustraße 20, 04107 Leipzig
link: https://www.facebook.com/events/2535797516656932/
image: 75485182_10156489386922483_2827741463356375040_n.jpg
teaser: Nach der SPIEL heißt es erstmal „ausruhen“ und wieder neue Kräfte für die
  nächste Würfelschlacht sammeln, denn noch sind sie nicht alle gefallen. Glei
isCrawled: true
---
Nach der SPIEL heißt es erstmal „ausruhen“ und wieder neue Kräfte für die nächste Würfelschlacht sammeln, denn noch sind sie nicht alle gefallen. Gleichzeitig haltet ihr diesmal die Karten (und den ganzen Rest) in der Hand, euer Schicksal zu ändern – oder zumindest euren Punktestand. Gewinnen werden aber nur die anderen. Die wirklich tollen Messeneuheiten gibt es dann allerdings erst im Dezember.
Für Snacks ist wie immer gesorgt.