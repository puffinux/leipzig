---
id: "425153831465893"
title: Vegane Küfa
start: 2019-11-05 20:00
end: 2019-11-05 23:00
locationName: Wohn- und Kulturprojekt B12
address: Braustraße 20, 04107 Leipzig
link: https://www.facebook.com/events/425153831465893/
image: 72163746_10156428199072483_4443783149687668736_n.jpg
teaser: Leckeres Essen, selbstverständlich vegan.  Ab 20 Uhr jeden Dienstag die Küfa
  in der B12!  (Das Bild dient nur zu Anschauungszwecken und entspricht nic
isCrawled: true
---
Leckeres Essen, selbstverständlich vegan.

Ab 20 Uhr jeden Dienstag die Küfa in der B12!

(Das Bild dient nur zu Anschauungszwecken und entspricht nicht dem Essen, das wir servieren ;).)