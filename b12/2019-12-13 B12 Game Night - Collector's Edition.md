---
id: "1383080501863028"
title: B12 Game Night - Collector's Edition
start: 2019-12-13 19:00
end: 2019-12-14 01:00
locationName: Wohn- und Kulturprojekt B12
address: Braustraße 20, 04107 Leipzig
link: https://www.facebook.com/events/1383080501863028/
image: 79381319_10156580111122483_1375886880578469888_n.jpg
teaser: 2019 ist vorbei. Jetzt schon. Es ging viel zu schnell. Doch was niemals endet,
  das ist der Spieleabend. Runde um Runde um Runde geht es immer weiter,
isCrawled: true
---
2019 ist vorbei. Jetzt schon. Es ging viel zu schnell. Doch was niemals endet, das ist der Spieleabend. Runde um Runde um Runde geht es immer weiter, denn es erscheinen mehr Spiele als wir alle je spielen können. Zum Jahresabschluss werden die Knaller des Jahres (welches Jahres, verraten wir nicht) auf den Tisch gepackt. Jetzt gilt es - spielt um euer Leben... oder zumindest um die meisten Siegpunkte.

Für Snacks ist wie immer gesorgt.