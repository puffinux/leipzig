---
id: "1604768689648326"
title: Vegane Küfa
start: 2019-10-22 20:00
end: 2019-10-22 23:00
locationName: Wohn- und Kulturprojekt B12
address: Braustraße 20, 04107 Leipzig
link: https://www.facebook.com/events/1604768689648326/
image: 71900413_10156428195997483_4778063770050101248_n.jpg
teaser: Leckeres Essen, selbstverständlich vegan.  Ab 20 Uhr jeden Dienstag die Küfa
  in der B12!  (Das Bild dient nur zu Anschauungszwecken und entspricht nic
isCrawled: true
---
Leckeres Essen, selbstverständlich vegan.

Ab 20 Uhr jeden Dienstag die Küfa in der B12!

(Das Bild dient nur zu Anschauungszwecken und entspricht nicht dem Essen, das wir servieren ;).)