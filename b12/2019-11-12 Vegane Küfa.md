---
id: "2478605025751104"
title: Vegane Küfa
start: 2019-11-12 20:00
end: 2019-11-12 23:00
locationName: Wohn- und Kulturprojekt B12
address: Braustraße 20, 04107 Leipzig
link: https://www.facebook.com/events/2478605025751104/
image: 76693226_10156489381577483_7279911026315755520_n.jpg
teaser: Leckeres Essen, selbstverständlich vegan.  Ab 20 Uhr jeden Dienstag die Küfa
  in der B12!  (Das Bild dient nur zu Anschauungszwecken und entspricht nic
isCrawled: true
---
Leckeres Essen, selbstverständlich vegan.

Ab 20 Uhr jeden Dienstag die Küfa in der B12!

(Das Bild dient nur zu Anschauungszwecken und entspricht nicht dem Essen, das wir servieren ;).)