---
id: "488341975103883"
title: Vegane Küfa
start: 2019-11-26 20:00
end: 2019-11-26 23:00
locationName: Wohn- und Kulturprojekt B12
address: Braustraße 20, 04107 Leipzig
link: https://www.facebook.com/events/488341975103883/
image: 71257656_10156489383187483_3051740595006472192_n.jpg
teaser: Leckeres Essen, selbstverständlich vegan.  Ab 20 Uhr jeden Dienstag die Küfa
  in der B12!  (Das Bild dient nur zu Anschauungszwecken und entspricht nic
isCrawled: true
---
Leckeres Essen, selbstverständlich vegan.

Ab 20 Uhr jeden Dienstag die Küfa in der B12!

(Das Bild dient nur zu Anschauungszwecken und entspricht nicht dem Essen, das wir servieren ;).)