---
id: "732095207263156"
title: Vegane Küfa
start: 2019-10-29 19:00
end: 2019-10-29 22:00
locationName: Wohn- und Kulturprojekt B12
address: Braustraße 20, 04107 Leipzig
link: https://www.facebook.com/events/732095207263156/
image: 72291801_10156428196402483_8474514415436169216_n.jpg
teaser: Leckeres Essen, selbstverständlich vegan.  Ab 20 Uhr jeden Dienstag die Küfa
  in der B12!  (Das Bild dient nur zu Anschauungszwecken und entspricht nic
isCrawled: true
---
Leckeres Essen, selbstverständlich vegan.

Ab 20 Uhr jeden Dienstag die Küfa in der B12!

(Das Bild dient nur zu Anschauungszwecken und entspricht nicht dem Essen, das wir servieren ;).)