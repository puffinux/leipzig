---
id: "595907431155765"
title: Festliche Jahresendküfa
start: 2019-12-17 20:00
end: 2019-12-17 23:00
locationName: Wohn- und Kulturprojekt B12
address: Braustraße 20, 04107 Leipzig
link: https://www.facebook.com/events/595907431155765/
image: 78417710_10156580512272483_2370220712100102144_o.jpg
teaser: 'Wie wir alle feiern wir auch alljährlich das Jubiläum, was uns am meisten
  bewegt: Die Herausgabe der "Euro-Starterkits" im Jahre 2001. Und zu diesem A'
isCrawled: true
---
Wie wir alle feiern wir auch alljährlich das Jubiläum, was uns am meisten bewegt: Die Herausgabe der "Euro-Starterkits" im Jahre 2001. Und zu diesem Anlass werden wir euch alle ganz festlich verköstigen mit einem klassischen 3-Gänge-Menü, was vielleicht aber die ein oder andere Überraschung zu bieten hat.

Alles selbstverständlich vegan und das Bild hier ist nur ein Symbolbild.