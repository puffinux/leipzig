---
id: "2741279452560659"
title: Metal punks II
start: 2020-02-15 21:00
end: 2020-02-16 00:00
locationName: Wohn- und Kulturprojekt B12
address: Braustraße 20, 04107 Leipzig
link: https://www.facebook.com/events/2741279452560659/
image: 80600401_10156638819942483_5283782408078884864_n.jpg
teaser: Krach in der b12.  Freut euch auf  Dethroned, metallic HC aus Dresden
  Deaftrap, Sludge/Crust aus Erfurt  t.b.a.
isCrawled: true
---
Krach in der b12. 
Freut euch auf

Dethroned, metallic HC aus Dresden
Deaftrap, Sludge/Crust aus Erfurt 
t.b.a. 