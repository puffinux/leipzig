---
id: "511812682702174"
title: Vlada Ina // Peace of mind
start: 2019-12-12 18:00
end: 2019-12-12 21:30
locationName: Wohn- und Kulturprojekt B12
address: Braustraße 20, 04107 Leipzig
link: https://www.facebook.com/events/511812682702174/
image: 75614154_10156542831942483_7537615373382713344_n.jpg
teaser: "Vlada ina (https://vladaina.bandcamp.com/) Peace Of Mind
  (https://peaceofmindhardcore.bandcamp.com/)   // vegan burger // soli-mexi  //
  Doors: 18.oo S"
isCrawled: true
---
Vlada ina (https://vladaina.bandcamp.com/)
Peace Of Mind (https://peaceofmindhardcore.bandcamp.com/)

 // vegan burger // soli-mexi  //
Doors: 18.oo
Start: 19.oo