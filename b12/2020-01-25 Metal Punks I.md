---
id: "619913748764542"
title: Metal Punks I
start: 2020-01-25 21:00
end: 2020-01-26 03:00
locationName: Wohn- und Kulturprojekt B12
address: Braustraße 20, 04107 Leipzig
link: https://www.facebook.com/events/619913748764542/
image: 80012661_10156616751187483_8268222214176768000_n.jpg
teaser: Wir wollen uns selbst überzeugen von den metallic Hardcore Newcomern Blade of
  Rust.  Und damit das Fragen ein Ende nimmt, wann endlich wieder Punk in
isCrawled: true
---
Wir wollen uns selbst überzeugen von den metallic Hardcore Newcomern Blade of Rust. 
Und damit das Fragen ein Ende nimmt, wann endlich wieder Punk in der b12 läuft, laden wir gleich Frachter mit dazu ein. 

wer schonmal reinhören will, was Thüringen musikalisch zu bieten hat, schaut hier
https://frachterpunx.bandcamp.com/
https://blad3ofrust.bandcamp.com/