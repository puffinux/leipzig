---
id: '848689165501200'
title: Soziale Wanderung zum ehemaligen KZ Flößberg
start: '2019-06-29 11:00'
end: '2019-06-29 20:00'
locationName: null
address: Leipzig Hauptbahnhof
link: 'https://www.facebook.com/events/848689165501200/'
image: 62442083_2335505676487626_4917449580657770496_n.jpg
teaser: '*Achtung: Anmeldung bitte per E-Mail, siehe unten!*  Am 29.06.2019 treffen wir uns um 11:00 Uhr am HBF Leipzig, um mit dem Zug nach Bad Lausick zu fah'
recurring: null
isCrawled: true
---
*Achtung: Anmeldung bitte per E-Mail, siehe unten!*

Am 29.06.2019 treffen wir uns um 11:00 Uhr am HBF Leipzig, um mit dem Zug nach Bad Lausick zu fahren. Von dort aus starten wir in Richtung Flößberg. 
Im Laufe des Tages wollen wir uns gemeinsam mit dem Thema Rüstungsindustrie, Zwangsarbeit und Erinnerungskultur im Leipziger Raum und insbesondere in Flößberg beschäftigen.
Das 1944 gegründete Außenlager des Konzentrationslagers Buchenwald, errichtet im Auftrag des Leipziger Rüstungsproduzenten HASAG, diente mehrere Monate der Produktion von Panzerfäusten. Vor allem jüdische Männer aus Polen und Ungarn waren dort inhaftiert und leisteten dort schwerste Zwangsarbeit, viele von ihnen 
kamen dabei ums Leben.
Eine Initiative vor Ort hat sich vor einigen Jahren das Ziel gesetzt, die Lagergeschichte aufzuarbeiten und den Ort ins öffentliche Bewusstsein zu rücken. Der jüdische Friedhof und die Erinnerungsstätte, die in der Nachkriegszeit auf dem Gelände errichtet wurde, wurden erst kürzlich erneuert. Die Initiative „Flößberg erinnert“ wird uns eine Führung über das Gelände geben. Die Rückfahrt treten wir abends von Borna an.

Die Spendenempfehlung zur Teilnahme beträgt 15€, davon bezahlen wir die Tickets und der Rest wird an die Initiative Flössberg gedenkt gespendet. Alle die sich das nicht leisten können sind trotzdem herzlich eingeladen an unserer Wanderung teilzunehmen.

Wir werden vermutlich zwischen 19:00 Uhr und 20:00 Uhr wieder in Leipzig ankommen.
Zur Anmeldung schreibt bitte eine E-Mail an leipzig@naturfreundejugend.de mit dem Betreff "SOZIALE WANDERUNG". Ich bekommt dann eine Rückmeldung mit allen weiteren Infos.
