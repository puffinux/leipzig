---
name: Naturfreundejugend Leipzig
website: https://www.naturfreundejugend.de/
email: leipzig@naturfreundejugend.de
scrape:
  source: facebook
  options:
    page_id: 1709698449068355
---
Die NATURFREUNDEJUNGEND LEIPZIG ist eine Gruppe im Aufbau. Wir sind ein parteiunabhängiger, linker Jugendverband, in dem Jugendliche selbstorganisiert herrschaftskritische Politik machen, Veranstaltungen und Aktionen organisieren oder zusammen verreisen.