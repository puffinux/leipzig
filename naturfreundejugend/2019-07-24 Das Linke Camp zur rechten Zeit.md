---
id: '630105170805813'
title: Das Linke Camp zur rechten Zeit
start: '2019-07-24 10:00'
end: '2019-07-31 18:00'
locationName: null
address: In der Nähe von Leipzig
link: 'https://www.facebook.com/events/630105170805813/'
image: 62144306_1568555119941385_3300635380033781760_n.jpg
teaser: '++++ ENGLISH VERSION BELOW ++++   Das Sommercamp feiert dieses Jahr gleich zweimal Premiere: Zum ersten Mal wird es von der Naturfreundejugend Berlin'
recurring: null
isCrawled: true
---
++++ ENGLISH VERSION BELOW ++++ 

Das Sommercamp feiert dieses Jahr gleich zweimal Premiere: Zum ersten Mal wird es von der Naturfreundejugend Berlin und der Naturfreundejugend Leipzig organisiert und zum ersten Mal findet es in Sachsen statt.

*Aber warum eigentlich?
Sachsen ist wahrlich nicht der Ort in Deutschland, der bekannt ist für Herrschaftskritik oder viel Engagment für die befreite Gesellschaft. AFD, Pegida, ein verschärftes Polizeigesetz und der NSU-Komplex sind die Schlagzeilen, die man über Sachsen in der Zeitung liest. Hier im Land der besorgten Bürger*innen und Naziverharmloser, wo die Ceska Pistolen an den Bäumen wachsen und Nazikonzerte in Kleingartenanlagen stattfinden , wollen wir unser Sommercamp abhalten. 
 
Gerade vor der kommenden Landtags- und Kommunalwahl in Sachsen ist es wichtig, aufzuzeigen, dass es auch anders geht: Solidarisch, ökologisch und herrschaftskritisch! 
Besonders in den ländlichen, strukturschwachen Regionen gibt es weiterhin Menschen, die uns Hoffnung machen und die wir auf dem Sommercamp zusammenbringen möchten: 
Denn auch in Sachsen finden sich engagierte Leute, die an einer besseren Gesellschaft feilen - mit viel Herzblut und Durchhaltevermögen stellen sie sich der braunen AFD und ihren bürgerlichen Freund*innen in den Weg. 

*Also raus aus der Großstadt-Blase und rein ins ländliche Getümmel! 
Acht Tage lang, vom 24. bis 31.07.2019, werden wir diskutieren, baden, entspannt rumhängen und die heiße Julisonne genießen. In zahlreichen Workshops wollen wir die gesellschaftlichen Verhältnisse analysieren und herausfinden, welche Rolle herrschaftlicher Kackmist darin spielt. Danach, dazwischen und währenddessen werden wir das schöne Leben austesten, in Hängematten unter den Bäumen lesen, uns den Bauch in der Sonne verbrennen oder abends beim Film schauen entspannen. Wir wollen die Tage nutzen, um uns auszutauschen, auf gute Ideen zu kommen und utopische Pläne zu schmieden.

Das Sommercamp wird unter anderem von der Rosa-Luxemburg-Stiftung gefördert. 

Bitte meldet euch verbindlich an:
https://naturfreundejugend-berlin.de/termine/herrschaftskritisches-sommercamp-2019/

+++ SICHERHEITS-DISCLAIMER +++
Wenn ihr nicht wollt, dass Facebook und Friends wissen, dass ihr an unserem Sommercamp teilnehmt, klickt hier nicht auf den "Teilnahme-Button". Anmelden müsst ihr euch sowieso unabhängig davon, hier: https://naturfreundejugend-berlin.us15.list-manage.com/subscribe?u=643275c5166d1a7b530f2c915&id=def6666e2a&fbclid=IwAR2kmQC90wP_s1TJMKAL6fYEg4sM6uEFBPD8hE62UndnDcUH17zfy1OpBtc

+++ ENGLISH +++ 

News from Saxony have not been very pleasant recently: AfD, Pegida, and a law that expands police powers - just to name a few. But let's not forget about all the rest that is a part of Saxony, too: people, for example, who make a stand against the AfD and their supporters, people who, despite the odds, are committed to building a better society. come join us for our left summer camp in Saxony! For eight days (July 24-31, 2019) we invite you to discuss, to swim in rivers and lakes, and to hang out and enjoy the sun. During numerous workshops we want to analyze society with you and find out why we'd be better off without societal oppressive power relations. After, before and in between workshops, we'll try out the good life: chilling out in hammocks, reading underneath trees, getting sunburned at the river, and watching movies together. We want to spend the days exchanging and connecting, inspiring new ideas, and making utopian plans. So let's get out of the big-city bubbles and into the buzzing country! With the state and communal elections in Saxony right ahead, let's show that another society is possible: solidary, antifascist, ecological, queer-feminist and critical!