---
id: '414974919288332'
title: Offenes Treffen des AK Mythos 89 der NFJ
start: '2019-04-16 18:00'
end: '2019-04-16 21:00'
locationName: Pöge-Haus
address: 'Hedwigstraße 20, 04315 Leipzig'
link: 'https://www.facebook.com/events/414974919288332/'
image: 56567310_2235856806452514_2429646601162063872_n.png
teaser: 'Liebe Zeitzeug*innen und Interessierte,  zuerst einmal möchten wir uns vorstellen: Wir sind ein Arbeitskreis der Naturfreundejugend Leipzig und Teil e'
recurring: null
isCrawled: true
---
Liebe Zeitzeug*innen und Interessierte,

zuerst einmal möchten wir uns vorstellen: Wir sind ein Arbeitskreis der Naturfreundejugend Leipzig und Teil eines parteiunabhängigen, linken Jugendverbands, in dem Jugendliche und jung gebliebene Erwachsene selbstorganisiert Politik machen.

Wie ihr sicher wisst begeht die Stadt Leipzig dieses Jahr ein besonderes Jubiläum, nämlich 30 Jahre „Friedliche Revolution“. Wir beschäftigen uns seit einigen Monaten mit dem etablierten Gedenkdiskurs und haben uns dazu auch den „Rundgang 89'“ des Museums an der Runden Ecke angeschaut. Wir finden: Da fehlt Einiges! Uns stellt sich jetzt die Frage. Was ist dran am Mythos der friedlichen Revolution? Während die nationalistische Ausrichtung der Montagsdemonstrationen immer stärker wurde, interessieren wir uns für die Entwicklung progressiver Kräfte und marginalisierter Gruppen. So ist über die Situation von sogenannten “Vertragsarbeiter*innen”, Migrant*innen, sowie Frauen und queeren Menschen nur wenig bekannt.

Aus diesen Gründen planen wir eine öffentlichkeitswirksame Kampagne, um dem etablierten Gedenken alternative Perspektiven entgegenzusetzen. Dazu konzipieren wir im Rahmen eines Arbeitskreises der Naturfreundejugend Leipzig einen neuen Stadtrundgang der die Komplexität der Ereignisse im Jahr 1989 adäquater vermitteln soll. Uns geht es um die Frage warum bestimmte Entwicklungen wie z.B der neu erstarkendr Nationalismus während der Montagsdemonstrationen ausgeblendet werden und wie die da damaligen Entwicklungen auf die heutige Gesellschaft einwirken. Auch möchten wir Perspektiven einbringen, die im gesamtgesellschaftlichen Narrativ kaum beachtet werden: die von Migrant*innen, LGBT-Aktivist*innen und auch die von oppositionellen Antifaschist*innen. Genau dafür brauchen wir euch!

Gerne würden wir Zeitzeug*innen anhand von Leitfragen interviewen und dies auch aufzeichnen. Aus den Aufzeichungen soll dann ein Audioguide für einen Stadtrundgang entstehen. Außerdem planen wir eine Podcast-Reihe, auch dort sollen die Interviews dokumentiert werden. Wir hoffen damit einen nachhaltigen und kritischen Beitrag in die Debatte um das 30-jährige Gedenken an die “friedliche Revolution” zu schaffen.

Wir würden uns freuen, wenn ihr Lust habt euch in das Projekt einzubringen. Zum Einen suchen wir Interviewpartner*innen und zum Anderen suchen wir Menschen die Lust haben an dem Projekt mitzuwirken.

Solltet ihr Fragen haben, immer her damit. Natürlich werden wir absolut nichts ohne euer Einverständnis veröffentlichen.

Wenn ihr Interesse habt, freuen wir uns sehr, wenn ihr mit uns per Mail Kontakt aufnehmt: mythos89-leipzig@naturfreundejugend.de - oder am 16.4.2019 um 18:00 Uhr zu unserem offenen Treffen im Pöge-Haus (Hedwigstraße 20) kommt.

Der Arbeitskreis „Mythos 89“ der Naturfreundejugend Leipzig