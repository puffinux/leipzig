---
id: "2964935233541266"
title: Offene Filmreihe zu Kapitalismus und Natur
start: 2019-12-18 19:00
end: 2019-12-18 22:00
address: Pracht
link: https://www.facebook.com/events/2964935233541266/
image: 73333448_2671266582911532_7448372494207025152_n.jpg
teaser: Der Klimawandel und die ökologische Krise werden unvorhersehbare soziale
  Auswirkungen haben. Sicher ist nur, dass es wesentlich schlimmer wird als bis
isCrawled: true
---
Der Klimawandel und die ökologische Krise werden unvorhersehbare soziale Auswirkungen haben. Sicher ist nur, dass es wesentlich schlimmer wird als bisher, wenn nicht schnell gehandelt wird. Uns steht also sehr sicher eine Katastrophe mit Ansage bevor. Alle können es wissen wenn sie es wissen wollen. So ziemlich jede Gegenmaßnahme scheint sinnvoll, notwendig und doch zugleich nicht ausreichend. Allein das konsumkritische und symbolische Einsparen von Plastiktüten, Flügen oder Fleisch, das Kaufen von Bioavocados oder Bäume pflanzen wird nicht reichen. Auch ein ‚grüner‘ Kapitalismus wird propagiert, in dem neue Technologien den wachsenden Konsum von Ressourcenverbrauch und Umweltverschmutzung entkoppeln sollen. Das ist aber nur die Illusion vom Weiter-So, vom Münchhausen-Kapitalismus der sich an den eigenen Haaren aus dem Sumpf zieht, vom bequemen und rettenden Wandel ohne Ökonomie und Gesellschaft grundlegend verändern zu wollen. Die politischen Debatten scheinen festgefahren und gleichzeitig formieren sich neue soziale Bewegungen wie Fridays for Future oder Extinction Rebellion.
Wird es in Zukunft überhaupt noch eine Erde für eine bessere Gesellschaft geben? Wie konnte es eigentlich dazu kommen? Warum ist dieser Kapitalismus so unbeherrschbar, unvernünftig und wie können wir ihn abschaffen?
In einer offenen Filmreihe zeigen wir Dokus und Spielfilme die sich mit dem Thema Kapitalismus und Natur auseinandersetzen. Anschließend ist Zeit und Raum für eine offene Diskussion.

Am 18.12.2019 zeigen wir 1900 Uhr eine Doku zur Einführung in das Anthropozän und seine Entstehungsgeschichte, also in das Erdzeitalter das mit der Industrialisierung begann und in dem der Mensch der prägendste Faktor ist.

Bis danni
Der Zutritt ist nur für Vereinsmitglieder der Pracht