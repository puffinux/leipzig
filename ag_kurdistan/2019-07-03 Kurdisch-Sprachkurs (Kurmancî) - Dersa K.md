---
id: '2298223116927094'
title: Kurdisch-Sprachkurs (Kurmancî) - Dersa Kurdî für Anfänger_Innen
start: '2019-07-03 17:00'
end: '2019-07-03 18:30'
locationName: null
address: 'Campus, Uni Leipzig, Augustusplatz 10'
link: 'https://www.facebook.com/events/2298223116927094/'
image: 64582671_450150025804464_6930281940889108480_n.jpg
teaser: 'Kurdisch-Sprachkurs  Seit 2018 organisieren wir in Leipzig in den Räumen der Universität Kurdisch-Sprachkurse (Kurmancî).  Zum Mittwoch, 03.07. werden'
recurring: null
isCrawled: true
---
Kurdisch-Sprachkurs

Seit 2018 organisieren wir in Leipzig in den Räumen der Universität Kurdisch-Sprachkurse (Kurmancî).

Zum Mittwoch, 03.07. werden wir einen neuen Kurs für Anfänger_Innen und Teilnehmer_Innen mit Grundkenntnissen beginnen.

Wir treffen uns am 03.07., 17 Uhr am Campus der Uni Leipzig, Augustusplatz 10 im Innenhof an der Statur.

Fragen und Anmeldungen an: ag_kurdistan at riseup . net



Dersa Kurdî

Korsa zimanê kurdî ji Sala 2018 ve dest pê kirye li Zanîngeha Leipzigê.

Ji bo kesên ku dixwazin zimanê kurdî fêrbibin û kesên bingeha zimanê kurdî li gel wan heye.

Destpêka perwerdeyê: çarşem, 03.07. saet 5 êvarê, paşê heftane di roja çarşemê de

Cih: Campus, Uni Leipzig, Augustusplatz 10 (li navendê li cem peyker)