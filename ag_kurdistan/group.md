---
name: AG Kurdistan
website: https://agkurdistan.noblogs.org
email: ag_kurdistan@riseup.net
scrape:
  source: facebook
  options:
    page_id: 160489244770545
---
Die AG Kurdistan arbeitet an der Uni Leipzig zur kurdischen Sprache und zu Ereignissen und Prozessen in Kurdistan.