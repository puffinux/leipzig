---
id: 1497-1553529600-1553540400
title: HuUpA 2019 - CRASHKURS Holz
start: '2019-03-25 16:00'
end: '2019-03-25 19:00'
locationName: null
address: 'Georg-Schwarz-Str. 7, Leipzig, Deutschland'
link: 'http://kunzstoffe.de/event/huupa-2019-crashkurs-holz/'
image: null
teaser: "\n.radius5c8e9b13cdd3a{border-radius:5px;} \n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
recurring: null
isCrawled: true
---


.radius5c8e9b13cdd3a{border-radius:5px;} 

																		

																			

																				

																				

																			

																		



C R A S H K U R S          Holz

 

  .list-5c8e9b13cdf07 ul {margin: 0;} .list-5c8e9b13cdf07 li{list-style-type: none;padding-bottom: .8em;position: relative;padding-left: 2em;font-size:16px}

			.list-5c8e9b13cdf07 li i{text-align: center;width: 1.6em;height: 1.6em;line-height: 1.6em;position: absolute;top: 0;

				left: 0;background-color: #cc3300;color: #ffffff;} 

			.list-5c8e9b13cdf07-icon-list-circle li i {border-radius: 50%;} .list-5c8e9b13cdf07-icon-list-square li i {border-radius: 0;} 

 Holzverbindung selbst gemacht 

Hier kannst du unter Anleitung eine gezinkte Buchstütze oder Schlüsselablage herstellen -ohne Schrauben, ohne Nägel, nur mit Säge und Stemmeisen.

 TERMINE:   

Mo, 25. März von 16:00 bis 19:00 Uhr 

Mo, 09. September von 16:00 bis 19:00 Uhr 

 MIT: Michael Seeber + Conrad Schwer (SchwarzArbeit GbR) 

 ORT: krimZkrams, Georg-Schwarz-Str. 7

 ANMEDLUNG: erwünscht, aber nicht erforderlich 

E-Mail: anmeldung.huupa@kunzstoffe.de 

Telefon: 0163 – 4846916 





