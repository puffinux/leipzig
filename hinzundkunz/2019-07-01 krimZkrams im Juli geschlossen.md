---
id: 2302-1561939200-1564617599
title: krimZkrams im Juli geschlossen
start: '2019-07-01 00:00'
end: '2019-08-01 00:00'
locationName: null
address: 'krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland'
link: 'http://kunzstoffe.de/event/krimzkrams-im-juli-geschlossen/'
image: null
teaser: |-
  Wir machen im Juli eine kleine Pause: Das krimZkrams öffnet erst wieder am 1. August.
  Alle gebuchten
recurring: null
isCrawled: true
---
Wir machen im Juli eine kleine Pause: Das krimZkrams öffnet erst wieder am 1. August.

Alle gebuchten Workshops und Veranstaltungen finden auch während der Schließzeit statt.

Euch allen einen schönen Sommer! 

