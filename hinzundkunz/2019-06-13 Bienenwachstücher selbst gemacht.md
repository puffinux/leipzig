---
id: 2187-1560438000-1560452400
title: Bienenwachstücher selbst gemacht
start: '2019-06-13 15:00'
end: '2019-06-13 19:00'
locationName: null
address: 'krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland'
link: 'http://kunzstoffe.de/event/offenes-angebot-bienenwachstuecher-selbst-gemacht/'
image: null
teaser: 'Der beste Müll ist der, der gar nicht erst anfällt. Immer mehr Menschen schließen sich dem Zero Wast'
recurring: null
isCrawled: true
---
Der beste Müll ist der, der gar nicht erst anfällt. Immer mehr Menschen schließen sich dem Zero Waste Trend an. Wir zeigen euch eine Alternative zur Aluminium- bzw. Frischhaltefolie. Einfach, schnell, wiederverwendbar und nachhaltig. 

Termin: Donnerstag, 13.06.2019, 15 – 19 Uhr

Ort: KrimZkrams, Georg-Schwarz-Straße 7, 04177 Leipzig

Kosten:  Spende 

Für die Teilnahme ist keine Anmeldung erforderlich. Kommt spontan vorbei! 

