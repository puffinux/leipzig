---
id: 1364-1551798000-1551985200
title: Ausstellung zur Materialreise // Erzählspende
start: '2019-03-05 15:00'
end: '2019-03-07 19:00'
locationName: null
address: 'Georg-Schwarz-Str. 7, Leipzig, Deutschland'
link: 'http://kunzstoffe.de/event/ausstellung-zur-materialreise-erzaehlspende/'
image: null
teaser: Von Mai bis Dezember 2018 konntet ihr bei uns liebgewonnene oder überflüssige Gegenstände auf die Ma
recurring: null
isCrawled: true
---
Von Mai bis Dezember 2018 konntet ihr bei uns liebgewonnene oder überflüssige Gegenstände auf die Materialreise schicken und die dazugehörigen Erzählspenden abgeben. Die Leipziger Künstlerin Inka Perl hat aus den abgegebenen Spenden Kunstwerke geschaffen, die in der künstlerischen Umsetzung wieder einen Sinn erhalten. Ihre Geschichte geht nun auf neue Art weiter. 

Ausstellung Materialreise // Erzählspende

5. – 7. März 2019

Vernissage: 4. März 2019, 18 Uhr

KrimZkrams, Öffnungszeiten: Di 15-19 Uhr, Mi 10-14 Uhr, Do 15-19 Uhr 

