---
id: 2333-1568127600-1568142000
title: DIY Bienenwachstücher
start: '2019-09-10 15:00'
end: '2019-09-10 19:00'
locationName: null
address: 'krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Sachsen, Deutschland'
link: 'http://kunzstoffe.de/event/diy-bienenwachstuecher/'
image: null
teaser: |-
  Offener Upcycling-Workshop: 
  Der beste Müll ist der, der gar nicht erst anfällt. Immer mehr Menschen
recurring: null
isCrawled: true
---
Offener Upcycling-Workshop: 

Der beste Müll ist der, der gar nicht erst anfällt. Immer mehr Menschen schließen sich dem Zero Waste Trend an. Wir zeigen euch eine Alternative zur Aluminium- bzw. Frischhaltefolie. Einfach, schnell, wiederverwendbar und nachhaltig. Wir verwenden für die Bienenwachstücher Stoffreste aus unserer Materialsammlung und Bienenwachs der Schloßimkerei Audigast aus dem Leipziger Umland. Ihr könnt aber auch eigene, gewaschene Baumwollstoffe (ohne Stretchanteil) mitbringen. 

Termin: ///ACHTUNG/// verschoben auf den Dienstag, 10.09.2019, 15 – 19 Uhr

Ort: KrimZkrams, Georg-Schwarz-Straße 7, 04177 Leipzig

Kosten: Spende

Für die Teilnahme ist keine Anmeldung erforderlich. Kommt spontan vorbei! 

