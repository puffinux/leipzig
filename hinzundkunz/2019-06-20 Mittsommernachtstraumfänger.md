---
id: 2077-1561042800-1561042800
title: Mittsommernachtstraumfänger
start: '2019-06-20 15:00'
end: '2019-06-20 15:00'
locationName: null
address: 'krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland'
link: 'http://kunzstoffe.de/event/workshop-mittsommernachtstraumfaenger/'
image: null
teaser: 'Zur Sommersonnenwende knüpfen wir mit euch bunte, leichte, magische, elfenhaft flatternde Mittsommer'
recurring: null
isCrawled: true
---
Zur Sommersonnenwende knüpfen wir mit euch bunte, leichte, magische, elfenhaft flatternde Mittsommernachtstraumfänger. Kleine Schmuckstücke für den Sommer im Garten oder auf dem Balkon.

Wir verwenden dafür Ringe aus alten Verpackungen und Stoffreste.  

Kommt vorbei! 

