---
id: 2185-1559833200-1559847600
title: DIY Aufbewahrungsgläser
start: '2019-06-06 15:00'
end: '2019-06-06 19:00'
locationName: null
address: 'krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland'
link: 'http://kunzstoffe.de/event/offenes-angebot-diy-aufbewahrungsglaeser/'
image: null
teaser: Alte Gläser können super weiterverwendet werden. Wir sorgen für schmückende Ordnung im Regal und ges
recurring: null
isCrawled: true
---
Alte Gläser können super weiterverwendet werden. Wir sorgen für schmückende Ordnung im Regal und gestalten dekorative Aufbewahrungsgläser. In unserer Materialsammlung findest du zahlreiche Möglichkeiten für dein persönliches Lieblingsglas. 

Termin: Donnerstag, 06.06.2019, 15 – 19 Uhr

Ort: KrimZkrams, Georg-Schwarz-Straße 7, 04177 Leipzig

Kosten:  Spende 

