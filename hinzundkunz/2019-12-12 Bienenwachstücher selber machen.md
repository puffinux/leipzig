---
id: 2970-1576162800-1576177200
title: Bienenwachstücher selber machen
start: 2019-12-12 15:00
end: 2019-12-12 19:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: http://kunzstoffe.de/event/bienenwachstuecher-selber-machen-2/
teaser: "ohne Anmeldung || Workshopzeitraum 15-19 Uhr || späterer Einstieg möglich ||
  gerne könnt ihr eure "
isCrawled: true
---
ohne Anmeldung || 

Workshopzeitraum 15-19 Uhr || späterer Einstieg möglich || 

gerne könnt ihr eure eigenen gewaschenen Baumwollstoffe (kein Stretchanteil) mitbringen 

