---
id: 2725-1571929200-1571943600
title: MAGISCHE LICHTER zu Halloween // offener Upcycling-Workshop
start: 2019-10-24 15:00
end: 2019-10-24 19:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: http://kunzstoffe.de/event/magische-lichter-zu-halloween-offener-upcycling-workshop/
teaser: Zu Halloween wird es magisch und mystisch… Unsere Altglassammlung verwandeln
  wir in Kerzengläser mit
isCrawled: true
---
Zu Halloween wird es magisch und mystisch… Unsere Altglassammlung verwandeln wir in Kerzengläser mit märchenhaften Traummotiven. Im Licht der Kerze tanzen die zarten Schatten an der Wand und die Geister können kommen!

Für die Teilnahme ist keine Anmeldung erforderlich. Kommt spontan vorbei!

Kosten: Spende

 

