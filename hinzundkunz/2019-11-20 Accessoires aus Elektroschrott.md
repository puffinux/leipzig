---
id: 2846-1574262000-1574276400
title: Accessoires aus Elektroschrott
start: 2019-11-20 15:00
end: 2019-11-20 19:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: http://kunzstoffe.de/event/accessoires-aus-elektroschrott/
teaser: "Löte, schneide, binde, klebe, kombiniere alte Kabel, Draht, Platinen oder
  Widerstände und erschaffe "
isCrawled: true
---
Löte, schneide, binde, klebe, kombiniere alte Kabel, Draht, Platinen oder Widerstände und erschaffe fancy Accessoires aus Elektroschrott. 

