---
id: 2848-1574348400-1574362800
title: Haushaltsprodukte selbst machen
start: 2019-11-21 15:00
end: 2019-11-21 19:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: http://kunzstoffe.de/event/haushaltsprodukte-selbst-machen/
teaser: Aus den vier Bestandteilen Soda, Natron, Zitronensäure und Essig stellen wir
  nachhaltige Produkte fü
isCrawled: true
---
Aus den vier Bestandteilen Soda, Natron, Zitronensäure und Essig stellen wir nachhaltige Produkte für den Haushalt her. Damit wird nicht nur die chemische Belastung für das Grundwasser reduziert, sondern auch viel Plastikverpackung eingespart. 

