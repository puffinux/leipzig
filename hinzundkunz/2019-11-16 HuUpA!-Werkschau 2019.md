---
id: 2701-1573927200-1574442000
title: HuUpA!-Werkschau 2019
start: 2019-11-16 18:00
end: 2019-11-22 17:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: http://kunzstoffe.de/event/huupa-werkschau-2019/
teaser: Am 16. November 2019 gibt es wieder viel selbst Gebautes, Genähtes, Geklebtes
  und Geschraubtes zu be
isCrawled: true
---
Am 16. November 2019 gibt es wieder viel selbst Gebautes, Genähtes, Geklebtes und Geschraubtes zu bestaunen. Die Handwerk- und Upcycling-Akademie (kurz: HuUpA!) ging dieses Jahr in die dritte Runde. Alle entstandenen Stücke werden (mit Stolz und Brimborium) in der Werkschau im krimZkrams in der Georg-Schwarz-Straße 7 präsentiert! 

Ab 18 Uhr sind die kreativen und innovativen Werkstücke der Kurs-Teilnehmenden zu bewundern und teilweise sogar auszuprobieren. Die Werkschau bleibt bis 22. November 2019 im krimZkrams ausgestellt. 

Die Akademie ist für alle im Leipziger Westen dank Förderung durch den Europäischen Sozialfonds und die Stadt Leipzig kostenfrei, Vorkenntnisse sind nicht nötig. Die HuUpA möchte die Vorteile des Selbermachens mit den Vorteilen des Nutzens von Materialien verbinden, die sonst viel zu oft als Müll entsorgt werden – kurz gesagt: Es geht um Upcycling, soziale/kulturelle Wertschätzung und Nachhaltigkeit. 

Kommt vorbei! 

 

