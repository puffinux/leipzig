---
id: 2474-1563640200-1567184400
title: Ausstellung  B U N T E R   H U N D  A U F   R E I S E N
start: '2019-07-20 16:30'
end: '2019-08-30 17:00'
locationName: null
address: 'Inklusives Nachbarschaftszentrum, Lindenauer Markt 13, Leipzig, Sachsen, 04177, Deutschland'
link: 'http://kunzstoffe.de/event/ausstellung-b-u-n-t-e-r-h-u-n-d-a-u-f-r-e-i-s-e-n/'
image: null
teaser: |-

  Am 20. JULI um 16.30 eröffnet die Ausstellung BUNTER HUND AUF REISEN.
  Trefft die BUNTEN HUNDE und b
recurring: null
isCrawled: true
---


Am 20. JULI um 16.30 eröffnet die Ausstellung BUNTER HUND AUF REISEN.

Trefft die BUNTEN HUNDE und bestaunt was in unserer KinderKunstWoche entstanden ist.

Im Inklusiven Nachbarschaftszenrum Lindenauer Markt 13 / Odermann-Passage / 04177 LEIPZIG.

Die Ausstellung kann bis 30. AUGUST besichtigt werden. 

