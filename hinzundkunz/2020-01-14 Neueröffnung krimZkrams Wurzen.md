---
id: 3028-1579017600-1579032000
title: Eröffnung krimZkrams Wurzen
start: 2020-01-14 16:00
end: 2020-01-14 20:00
address: krimZkrams Wurzen, Wenceslaigasse 22, Wurzen, 04808
link: http://kunzstoffe.de/event/neueroeffnung-krimzkrams-wurzen/
teaser: Das krimZkrams Wurzen wird feierlich eröffnet!Kommt in die Wenceslaigasse 22
  zum Stöbern und Schaue
isCrawled: true
---
Das krimZkrams Wurzen wird feierlich eröffnet!

Kommt in die Wenceslaigasse 22 zum Stöbern und Schauen, Schwatzen und Schmausen.

Mehr über das krimZkrams Wurzen erfahrt ihr hier. 

Das krimZkrams Wurzen ist innerhalb von 10 Minuten Fußweg vom Bahnhof Wurzen erreichbar.

Von Leipzig aus fahren die S3 und die Regionalbahn nach Wurzen. 

