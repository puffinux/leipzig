---
id: 2854-1574191800-1574191800
title: Filmwelten und Alltagshelden | "Closing the Loop"
start: 2019-11-19 19:30
end: 2019-11-19 19:30
address: hinZundkunZ, Georg-Schwarz-Str. 7, Leipzig, 04177, Deutschland
link: http://kunzstoffe.de/event/filmwelten-und-alltagshelden-closing-the-loop/
teaser: Der Film zeigt, dass eine Kreislaufwirtschaft (Circle Economy) nicht nur
  dringend notwendig ist, son
isCrawled: true
---
Der Film zeigt, dass eine Kreislaufwirtschaft (Circle Economy) nicht nur dringend notwendig ist, sondern auch anhand mehrer Beispiele wie sie umgesetzt werden kann. 

von GRAHAM SHELDON & RIN EHLERS SHELDON | UK | 2018 | 89 min 

anschließend Diskussion 

