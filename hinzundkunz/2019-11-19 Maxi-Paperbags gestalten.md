---
id: 2844-1574181000-1574195400
title: Maxi-Paperbags gestalten
start: 2019-11-19 16:30
end: 2019-11-19 20:30
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: http://kunzstoffe.de/event/maxi-paperbags-gestalten/
teaser: "Papier fasziniert halt nicht nur in Buchform: es hat mit seinen vielen
  Talenten auch längst schon di"
isCrawled: true
---
Papier fasziniert halt nicht nur in Buchform: es hat mit seinen vielen Talenten auch längst schon die Wohnwelt erobert und zeigt sich überraschend vielseitig als Aufbewahrungsutensil mit individuellen Design. 

