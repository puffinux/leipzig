---
id: 2723-1570010400-1570024800
title: 90er Jahre Haargummis // offener Upcycling-Workshop
start: '2019-10-02 10:00'
end: '2019-10-02 14:00'
locationName: null
address: 'krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland'
link: 'http://kunzstoffe.de/event/90er-jahre-haargummis-upcycling-workshop/'
image: null
teaser: 'Die Haargummis der 90er Jahre sind zurück! Und wir zeigen dir, wie du sie selbst machen kannst.In un'
recurring: null
isCrawled: true
---
Die Haargummis der 90er Jahre sind zurück! Und wir zeigen dir, wie du sie selbst machen kannst.In unserer Materialsammlung findest du schöne Stoffe und das passende Gummi. Ruckizucki ist aus alten Stoffresten ein modisches Haarteil genäht.

Für die Teilnahme ist keine Anmeldung erforderlich. Kommt spontan vorbei!

Kosten: Spende 

