---
id: 2836-1573570800-1573585200
title: "Aktion: Herzen nähen"
start: 2019-11-12 15:00
end: 2019-11-12 19:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: http://kunzstoffe.de/event/aktion-herzen-naehen-4/
teaser: "Eine Herzensangelegenheit für uns: Wir unterstützen den Clowns&Clowns e.V.
  bei deren Zeig-Herz-Aktio"
isCrawled: true
---
Eine Herzensangelegenheit für uns: Wir unterstützen den Clowns&Clowns e.V. bei deren Zeig-Herz-Aktion. Im Zeitraum vom 05.11.2019 bis 14.11.2019 könnt ihr zu unseren Öffnungszeiten HumorHerzen nähen oder gar häkeln. Die Clowns besuchen in der Adventszeit Leipziger Kliniken und Pflegeheimen, verschenken die Herzen und zaubern so ein Lächeln auf die Lippen. Wir haben haben den Faden und ihr die Fähigkeit und freuen uns auf gaaaaaaaanz vielen Herzen!! 

