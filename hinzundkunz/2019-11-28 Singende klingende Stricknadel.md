---
id: 2827-1574964000-1574971200
title: Singende klingende Stricknadel
start: 2019-11-28 18:00
end: 2019-11-28 20:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: http://kunzstoffe.de/event/singende-klingende-stricknadel-3/
teaser: "Im November wird es gemütlich: Im krimZkrams gibt es Stricken und Singen mit
  unseren Mary‘s. Ihr sei"
isCrawled: true
---
Im November wird es gemütlich: Im krimZkrams gibt es Stricken und Singen mit unseren Mary‘s. Ihr seid herzlich eingeladen mit euren Strick-Projekten, Ideen oder Fragen vorbei zu kommen und in geselliger Runde sowie mit musikalischer Begleitung bei uns und mit uns zu sein. 

