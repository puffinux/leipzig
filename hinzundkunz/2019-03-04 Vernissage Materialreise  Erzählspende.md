---
id: 1367-1551722400-1551729600
title: 'Vernissage: Materialreise // Erzählspende'
start: '2019-03-04 18:00'
end: '2019-03-04 20:00'
locationName: null
address: 'Georg-Schwarz-Str. 7, Leipzig, Deutschland'
link: 'http://kunzstoffe.de/event/vernissage-materialreise-erzaehlspende/'
image: null
teaser: Von Mai bis Dezember 2018 konntet ihr bei uns liebgewonnene oder überflüssige Gegenstände auf die Ma
recurring: null
isCrawled: true
---
Von Mai bis Dezember 2018 konntet ihr bei uns liebgewonnene oder überflüssige Gegenstände auf die Materialreise schicken und die dazugehörigen Erzählspenden abgeben. Die Leipziger Künstlerin Inka Perl hat aus den abgegebenen Spenden Kunstwerke geschaffen. Die Gegenstände erhalten in der künstlerischen Umsetzung wieder einen Sinn und ihre Geschichte geht nun auf neue Art weiter. 

Ausstellung Materialreise // Erzählspende

5. – 7. März 2019

Vernissage: 4. März 2019, 18 Uhr

KrimZkrams, Öffnungszeiten: Di 15-19 Uhr, Mi 10-14 Uhr, Do 15-19 Uhr 

