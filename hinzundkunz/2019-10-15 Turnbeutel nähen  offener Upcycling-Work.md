---
id: 2721-1571151600-1571166000
title: Turnbeutel nähen // offener Upcycling-Workshop
start: 2019-10-15 15:00
end: 2019-10-15 19:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: http://kunzstoffe.de/event/turnbeutel-naehen-offener-diy-workshop/
teaser: "Was kann man immer gut gebrauchen? Richtig, einen Turnbeutel: Für den Sport,
  zum Ausgehen, für Ausfl"
isCrawled: true
---
Was kann man immer gut gebrauchen? Richtig, einen Turnbeutel: Für den Sport, zum Ausgehen, für Ausflüge, als Bäckerbeutel, als Handtaschenersatz, für den kleinen Einkauf, als Wickelbeutel, und und und… Wer noch keinen hat, sollte sich einen besorgen! Z.B. bei uns: In unserem Upcycling-Workshop nähen wir mit dir deinen ganz eigenen Turnbeutel aus Stoffresten. Hier findest du eine riesige Auswahl an Farben und Mustern sowie fachliche Unterstützung beim Schneiden und Nähen durch unsere Workshopleiterin. Mach‘ dir dein Turnbeutel-Unikat selbst! 

Für die Teilnahme ist keine Anmeldung erforderlich. Kommt spontan vorbei! 

Kosten: Wir freuen uns über eine Spende 

