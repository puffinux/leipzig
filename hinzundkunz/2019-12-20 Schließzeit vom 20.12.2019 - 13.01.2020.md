---
id: 2989-1576828800-1578934800
title: Schließzeit vom 20.12.2019 - 13.01.2020
start: 2019-12-20 08:00
end: 2020-01-13 17:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: http://kunzstoffe.de/event/schliesszeit-vom-20-12-2019-13-01-2020/
teaser: "Das krimZkrams bleibt in der Zeit vom 20.12.2019 bis zum 13.01.2020
  geschlossen. "
isCrawled: true
---
Das krimZkrams bleibt in der Zeit vom 20.12.2019 bis zum 13.01.2020 geschlossen. 

