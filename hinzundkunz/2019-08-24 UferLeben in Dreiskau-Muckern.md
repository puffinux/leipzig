---
id: 2362-1566640800-1566669600
title: UferLeben in Dreiskau-Muckern
start: '2019-08-24 10:00'
end: '2019-08-24 18:00'
locationName: null
address: 'Dreiskau-Muckern, Sachsen'
link: 'http://kunzstoffe.de/event/uferleben-in-dreiskau-muckern/'
image: null
teaser: 'Wir machen eine kleine Landpartie: Beim UferLeben in schönen Dreiskau-Muckern am Störmthaler See wol'
recurring: null
isCrawled: true
---
Wir machen eine kleine Landpartie: Beim UferLeben in schönen Dreiskau-Muckern am Störmthaler See wollen wir Landluft schnuppern, die Seele baumeln lassen und mit einem krimZkrams-Stand über die Themen Nachhaltigkeit, Ressourcenschutz und Upcycling informieren. Wir werden auch wieder handgemachte Upcycling-Produkte, wie beispielsweise unsere Bienenwachstücher, zum Kaufen dabei haben. Kommt mit! 

