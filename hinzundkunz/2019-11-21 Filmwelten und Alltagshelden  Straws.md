---
id: 2858-1574364600-1574364600
title: Filmwelten und Alltagshelden | "Straws"
start: 2019-11-21 19:30
end: 2019-11-21 19:30
address: hinZundkunZ, Georg-Schwarz-Str. 7, Leipzig, 04177, Deutschland
link: http://kunzstoffe.de/event/filmwelten-und-alltagshelden-straws/
teaser: Ein Dokumentarfilm über die immensen Auswirkungen die die kleinen
  Plastikstrohhalme auf das Ökosyste
isCrawled: true
---
Ein Dokumentarfilm über die immensen Auswirkungen die die kleinen Plastikstrohhalme auf das Ökosystem haben. 

von LINDA BOOKER | USA | 2017 | 32 min + Vorfilm 

anschließend Diskussion 

