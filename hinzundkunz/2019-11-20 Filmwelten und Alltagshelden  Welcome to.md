---
id: 2856-1574278200-1574278200
title: Filmwelten und Alltagshelden | "Welcome to Sodom"
start: 2019-11-20 19:30
end: 2019-11-20 19:30
address: hinZundkunZ, Georg-Schwarz-Str. 7, Leipzig, 04177, Deutschland
link: http://kunzstoffe.de/event/filmwelten-und-alltagshelden-welcome-to-sodom/
teaser: "Europas größte Müllhalde befindet sich mitten in Afrika: hier landen
  Elektroschrott, Tablets, Smartp"
isCrawled: true
---
Europas größte Müllhalde befindet sich mitten in Afrika: hier landen Elektroschrott, Tablets, Smartphones… . Der preisgekrönte Film portraitiert die Menschen in Agbogbloshe, die in und mit unserem Schrott leben. 

von FLORIAN WEIGENSAMER & CHRISTIAN KRÖNES | AUT | 2018 | 92 min 

anschließend Diskussion 

