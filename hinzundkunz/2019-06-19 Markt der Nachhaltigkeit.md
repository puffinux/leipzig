---
id: 2263-1560952800-1560963600
title: Markt der Nachhaltigkeit
start: '2019-06-19 14:00'
end: '2019-06-19 17:00'
locationName: null
address: 'HTWK-Parkplatz hinter dem Medienzentrum, Karl-Liebknecht-Str. 132, Leipzig, 04227'
link: 'http://kunzstoffe.de/event/markt-der-nachhaltigkeit/'
image: null
teaser: Die HTWK feiert ihr Hochschulsommerfest und wir sind auch wieder 2019 mit dabei. Unseren Infostand f
recurring: null
isCrawled: true
---
Die HTWK feiert ihr Hochschulsommerfest und wir sind auch wieder 2019 mit dabei. Unseren Infostand findet ihr auf dem Markt der Nachhaltigkeit. Wir freuen uns auf euch! 

