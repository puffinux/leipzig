---
id: 1824-112
title: Leipzig für Alle
start: 2019-11-13 19:00
end: 2019-11-13 21:00
link: http://leipzigfueralle.blogsport.eu/
teaser: '"Leipzig für alle: Aktionsbündnis Wohnen" trifft sich an wechselnden Orten in
  Leipzig. Frag am beste'
isCrawled: true
---
"Leipzig für alle: Aktionsbündnis Wohnen" trifft sich an wechselnden Orten in Leipzig. Frag am besten per Mail nach, wo das nächste Treffen stattfindet: l&#101;ipz&#105;gf&#117;era&#108;&#108;e&#64;po&#115;&#116;&#101;o&#46;&#100;e http://leipzigfueralle.blogsport.eu/