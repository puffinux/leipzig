---
id: 1792-104
title: 'Vollversammlung Wohnen #2'
start: '2019-03-06 18:00'
end: '2019-03-06 21:00'
locationName: null
address: distillery Kurt-Eisner-Straße 108a  Leipzig  04275
link: 'http://leipzigfueralle.blogsport.eu/vollversammlung-wohnen-wir-wollen-eine-stadt-in-der-alle-wohnen-koennen/'
image: null
teaser: 'Leipzig für Alle – Aktionsbündnis Wohnen: Nachdem am 07.02. zum ersten Bündnistreffen mehr als 70 Pe'
recurring: null
isCrawled: true
---
Leipzig für Alle – Aktionsbündnis Wohnen: Nachdem am 07.02. zum ersten Bündnistreffen mehr als 70 Personen kamen, wollen wir den Weg fortsetzen und am 06.03. die nächsten Schritte, auch in der Vorbereitung der bevorstehenden #Mietwahnsinn-Demo, diskutieren. Wenn ihr von Mietsteigerungen und Verdrängung betroffen seid, selbst gute Ideen habt oder euch solidarisch zeigen wollt und aktiv an der Entwicklung der Stadt mitarbeiten möchtet, seid ihr jederzeit willkommen. Für eine Stadt für Alle - für ein Leipzig für AlleEinlass ab 17:30 Uhr http://leipzigfueralle.blogsport.eu/vollversammlung-wohnen-wir-wollen-eine-stadt-in-der-alle-wohnen-koennen/