---
id: 1557-102
title: Leipzig für Alle
start: '2019-05-22 19:00'
end: '2019-05-22 21:00'
locationName: null
address: null
link: 'http://leipzigfueralle.blogsport.eu/'
image: null
teaser: '"Leipzig für alle: Aktionsbündnis Wohnen" trifft sich 14-tägig an wechselnden Orten in Leipzig. Frag'
recurring: null
isCrawled: true
---
"Leipzig für alle: Aktionsbündnis Wohnen" trifft sich 14-tägig an wechselnden Orten in Leipzig. Frag am besten per Mail nach, wo das nächste Treffen stattfindet: leipzigfueralle@posteo.de http://leipzigfueralle.blogsport.eu/