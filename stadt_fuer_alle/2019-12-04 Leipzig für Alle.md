---
id: 1825-116
title: Leipzig für Alle
start: 2019-12-04 19:00
end: 2019-12-04 21:00
link: http://leipzigfueralle.de/
teaser: '"Leipzig für alle: Aktionsbündnis Wohnen" trifft sich an wechselnden Orten in
  Leipzig. Frag am beste'
isCrawled: true
---
"Leipzig für alle: Aktionsbündnis Wohnen" trifft sich an wechselnden Orten in Leipzig. Frag am besten per Mail nach, wo das nächste Treffen stattfindet: l&#101;&#105;&#112;&#122;i&#103;fue&#114;a&#108;&#108;e&#64;&#112;&#111;ste&#111;&#46;&#100;e http://leipzigfueralle.de/