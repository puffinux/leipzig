---
id: 1809-110
title: Stirbt die Clubkultur?
start: '2019-05-03 07:00'
end: '2019-05-03 09:00'
locationName: null
address: Peter K. Ludwigstraße 81  Leipzig  04315
link: 'https://www.facebook.com/events/2228305034097486/'
image: null
teaser: 'Leipziger Clubkultur und Kneipenszene sind akut bedroht, weil Investoren Geld verdienen wollen. Nach'
recurring: null
isCrawled: true
---
Leipziger Clubkultur und Kneipenszene sind akut bedroht, weil Investoren Geld verdienen wollen. Nach dem Aus für das 4 Rooms, das So&amp;So und etliche kleinere Kneipen steht nun das Peter K. vor der Schließung. Mit ihm stehen auch andere Clubs, Kneipen und Kulturprojekte aufgrund von Immobilienspekulation, Investorenplänen aber auch bürokratischen Hürden auf der Kippe. Wir wollen diskutieren, wie diese Entwicklung gestoppt werden kann. Welche rechtlichen Möglichkeiten gibt es für die Betreiber, was kann die Politik tun und wie können wir uns in unseren Stadtvierteln aktiv zusammenschließen?Podiumdiskussion mit 	Norma Brecht, Leipzig – Stadt für alle 	René Hobusch, Vorstand Haus&amp;Grund Sachsen (Freibeuter-Fraktion im Stadtrat) 	Steffen Kache, Distillery, IG Livekomm 	Jürgen Kasek, Rechtsanwalt (Stadtratskandidat GRÜNE Wahlkreis 2) 	Tobias Peter, Pöge-Haus (Stadtratskandidat GRÜNE Wahlkreis 1) 	Prof. Dr. Dieter Rink, Stadtforscher UFZ Leipzig https://www.facebook.com/events/2228305034097486/