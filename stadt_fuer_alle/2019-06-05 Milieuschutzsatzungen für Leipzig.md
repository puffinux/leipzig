---
id: 1810-111
title: Milieuschutzsatzungen für Leipzig?
start: '2019-06-05 18:30'
end: '2019-06-05 21:00'
locationName: null
address: Galerie KUB Kantstraße 18   Leipzig  04275
link: 'http://www.leipzig-stadtfueralle.de/mein-kalender/'
image: null
teaser: 'Die Lage auf dem Leipziger Wohnungsmarkt wird quasi Tage für Tag angespannter, die Mieten steigen. W'
recurring: null
isCrawled: true
---
Die Lage auf dem Leipziger Wohnungsmarkt wird quasi Tage für Tag angespannter, die Mieten steigen. Was kann die Stadt dagegen tun? Ein Instrument, bei dem die Kommune nicht von der Bundes- und Landesebene abhängig ist, sind soziale Erhaltungssatzungen, auch als Milieuschutz bekannt. Sie ermöglichen es, teure Modernisierungen zu untersagen und räumen der Stadt ein Vorkaufsrecht an Wohnhäusern ein. In einige Berliner Bezirken wird dies mittlerweile regelmäßig angewendet. Auch für verschiedene Leipziger Stadtteile sind solche Satzungen derzeit in Arbeit. Doch ihre Aufstellung hat sich jüngst erneut verzögert.Wir diskutieren darüber mit: 	Jochen Biedermann, Stadtrat für Stadtentwicklung, Soziales und Bürgerdienste in Berlin-Neukölln 	Dr. Frank Amey, Leiter des Amtes für Wohnungsbau und Stadterneuerung der Stadt Leipzig 	Anke Matejka, Vorsitzende des Mietervereins Leipzig 	Moderation: Dr. Peter Bescherer, Stadtforscher an der Universität Jena 