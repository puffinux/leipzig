---
id: 1822-112
title: Leipzig für Alle
start: 2019-10-16 19:00
end: 2019-10-16 21:00
link: http://leipzigfueralle.blogsport.eu/
teaser: '"Leipzig für alle: Aktionsbündnis Wohnen" trifft sich an wechselnden Orten in
  Leipzig. Frag am beste'
isCrawled: true
---
"Leipzig für alle: Aktionsbündnis Wohnen" trifft sich an wechselnden Orten in Leipzig. Frag am besten per Mail nach, wo das nächste Treffen stattfindet: le&#105;p&#122;igf&#117;&#101;r&#97;l&#108;&#101;&#64;posteo&#46;&#100;&#101; http://leipzigfueralle.blogsport.eu/