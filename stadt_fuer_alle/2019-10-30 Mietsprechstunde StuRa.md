---
id: 1832-113
title: Mietsprechstunde StuRa
start: 2019-10-30 12:00
end: 2019-10-30 14:00
address: Universität Leipzig, Campus Augustusplatz 10  Leipzig  04109
link: https://stura.uni-leipzig.de/mietberatung
teaser: "Achtung: wegen des Feiertages verlegt auf Mittwoch.Immer am letzten
  Donnerstag eines Monats von 12:0"
isCrawled: true
---
Achtung: wegen des Feiertages verlegt auf Mittwoch.Immer am letzten Donnerstag eines Monats von 12:00 - 13:00 Uhr können Menschen sowohl in rechtlichen Fragen Hilfe bekommen, als auch Unterstützung erhalten von einer Person, die in Mietstreitigkeiten bereits persönlich Erfahrungen gesammelt hat.Dieses Angebot wird in Kooperation von Leipzig für Alle: Aktionsbündnis Wohnen, DGB-Hochschulgruppe, GEW-Hochschulgruppe und dem Student_innenRat der Universität Leipzig ermöglicht. https://stura.uni-leipzig.de/mietberatung