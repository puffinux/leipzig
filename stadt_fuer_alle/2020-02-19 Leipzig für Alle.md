---
id: 1831-112
title: Leipzig für Alle
start: 2020-02-19 19:00
end: 2020-02-19 21:00
link: http://leipzigfueralle.blogsport.eu/
teaser: '"Leipzig für alle: Aktionsbündnis Wohnen" trifft sich an wechselnden Orten in
  Leipzig. Frag am beste'
isCrawled: true
---
"Leipzig für alle: Aktionsbündnis Wohnen" trifft sich an wechselnden Orten in Leipzig. Frag am besten per Mail nach, wo das nächste Treffen stattfindet: leipzigfueralle@posteo.de http://leipzigfueralle.blogsport.eu/