---
id: 1836-114
title: Mietsprechstunde StuRa
start: 2019-11-28 12:00
end: 2019-11-28 13:00
address: Universität Leipzig, Campus Augustusplatz 10  Leipzig  04109
link: https://stura.uni-leipzig.de/mietberatung
teaser: Immer am letzten Donnerstag eines Monats von 12 bis 13 Uhr können Menschen
  sowohl in rechtlichen Fra
isCrawled: true
---
Immer am letzten Donnerstag eines Monats von 12 bis 13 Uhr können Menschen sowohl in rechtlichen Fragen Hilfe bekommen als auch Unterstützung erhalten von einer Person, die in Mietstreitigkeiten bereits persönlich Erfahrungen gesammelt hat.Dieses Angebot wird in Kooperation von Leipzig für Alle: Aktionsbündnis Wohnen, DGB-Hochschulgruppe, GEW-Hochschulgruppe und dem Student_innenRat der Universität Leipzig ermöglicht. https://stura.uni-leipzig.de/mietberatung