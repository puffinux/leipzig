---
id: 1799-107
title: Mietsprechstunde StuRa
start: '2019-04-25 12:00'
end: '2019-04-25 14:00'
locationName: null
address: 'Universität Leipzig, Campus Augustusplatz 10  Leipzig  04109'
link: 'https://www.facebook.com/events/375484533296743/'
image: null
teaser: 'Am 25. April von 12:00 bis 14:00 Uhr findet im Beratungsraum S 001 im Seminargebäude die erste Berat'
recurring: null
isCrawled: true
---
Am 25. April von 12:00 bis 14:00 Uhr findet im Beratungsraum S 001 im Seminargebäude die erste Beratung zu Wohnen und Mieten im StuRa der Uni Leipzig statt.Dieses Angebot wird in Kooperation von Leipzig für Alle: Aktionsbündnis Wohnen, DGB-Hochschulgruppe, GEW-Hochschulgruppe und dem Student_innenRat der Universität Leipzig ermöglicht. https://www.facebook.com/events/375484533296743/