---
id: 1793-105
title: 'Gemeinsam gegen Verdrängung und #Mietenwahnsinn!'
start: '2019-04-06 14:00'
end: '2019-04-06 17:00'
locationName: null
address: Bayerischer Bahnhof Bayrischer Platz  Leipzig  04103
link: 'http://leipzigfueralle.blogsport.eu/'
image: null
teaser: 'Leipzig für Alle: Aktionsbündnis Wohnen: »In der bundesweiten #Mietenwahnsinn-Aktionswoche wollen wi'
recurring: null
isCrawled: true
---
Leipzig für Alle: Aktionsbündnis Wohnen: »In der bundesweiten #Mietenwahnsinn-Aktionswoche wollen wir unserem Anliegen Ausdruck verleihen und Politik sowie Wohnungswirtschaft zum Handeln zwingen.«Die Demoroute wird vom Bayrischen Platz über den Wilhelm-Leuschner-Platz zum Markt ziehen. http://leipzigfueralle.blogsport.eu/