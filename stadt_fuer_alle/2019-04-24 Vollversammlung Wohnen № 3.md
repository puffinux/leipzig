---
id: 1794-106
title: Vollversammlung Wohnen № 3
start: '2019-04-24 18:00'
end: '2019-04-24 20:30'
locationName: null
address: distillery Kurt-Eisner-Straße 108a  Leipzig  04275
link: 'https://www.facebook.com/events/452968425444205/'
image: null
teaser: 'Nach großer Mietenwahnsinn-Demo: Vollversammlung am 24. AprilAm 6. April beteiligten sich in Leipzig'
recurring: null
isCrawled: true
---
Nach großer Mietenwahnsinn-Demo: Vollversammlung am 24. AprilAm 6. April beteiligten sich in Leipzig bis zu 3000 Menschen an einer bundesweiten Demonstration für bezahlbare Mieten unter dem Schlagwort #Mietenwahnsinn. Die große Beteiligung zeigt, wie wichtig das Thema der stetig steigenden Mieten, aber auch die Verdrängung in andere Stadtteile und das Verschwinden von Freiräumen und Clubkultur sind. Wir wollen die Menschen in Leipzig ermutigen, sich hierzu öffentlich zu engagieren und zur dritten Vollversammlung Wohnen zu kommen.Am 24.4.2019 lädt die Initiative „Leipzig für Alle: Aktionsbündnis Wohnen“ um 18 Uhr (Einlass ab 17:30 Uhr) dazu ein, in der Distillery weitere Aktionen zu ersinnen und zu planen, um das Thema Wohnen und Mieten auch hinsichtlich der anstehenden Wahlen in Bevölkerung und Politik noch stärker in den Vordergrund zu rücken. https://www.facebook.com/events/452968425444205/