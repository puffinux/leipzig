---
id: 1803-108
title: Mietsprechstunde
start: '2019-06-11 18:00'
end: '2019-06-11 19:30'
locationName: null
address: Pöge-Haus Hedwigstraße 20  Leipzig  04315
link: 'https://www.facebook.com/events/813723778983003/'
image: null
teaser: 'Neben dem Mieterverein, dem Miettreff Leipziger Osten, der Verbraucherzenzrale oder der Mietsprechst'
recurring: null
isCrawled: true
---
Neben dem Mieterverein, dem Miettreff Leipziger Osten, der Verbraucherzenzrale oder der Mietsprechstunde gibt es nun eine neue Möglichkeit sich in Sachen Wohnen und Mieten beraten zu lassen:Jeden zweiten Dienstag im Monat können Menschen nun sowohl in rechtlichen Fragen Hilfe bekommen als auch Unterstützung erhalten von einer Person, die in Mietstreitigkeiten bereits persönlich Erfahrungen gesammelt hat.Wenn die eigene Wohnung als der private Rückzugsraum bedroht wird, gilt es auch Ängste aufzufangen. Die Mietberatung soll daher neben der juristischen Erstberatung auch dabei helfen, mutig zu bleiben, indem beispielsweise Tipps zur Stärkung der Mietergemeinschaft im Haus oder der Kontakt zu solidarischen Gruppen vermittelt werden.Bei Interesse und Bedarf komm vorbei! https://www.facebook.com/events/813723778983003/