---
id: 1484-100
title: Mietsprechstunde (FdP)
start: '2019-11-19 19:00'
end: '2019-11-19 20:00'
locationName: null
address: linXXnet Bornaische Straße 3d  Leipzig
link: 'http://www.leipzig-stadtfueralle.de/mein-kalender/'
image: null
teaser: "Die Initiative „Für das Politische“\_bietet seit 2019 eine zweite monatliche Mietsprechstunde an. Die"
recurring: null
isCrawled: true
---
Die Initiative „Für das Politische“ bietet seit 2019 eine zweite monatliche Mietsprechstunde an. Die solidarisch organisierte Sprechstunde bietet konkrete Hilfe bei Mietstreitigkeiten und kann bei Einzelnen ein politisches Bewusstsein über den eigenen Mietvertrag hinaus entstehen lassen. Bei größeren Schwierigkeiten kann Unterstützung von Organisationsprozessen angeboten werden bis hin zur Übernahme von Öffentlichkeitsarbeit.Mail: fuerdaspolitische@riseup.net 