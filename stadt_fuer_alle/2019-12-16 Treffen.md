---
id: 721-98
title: Treffen
start: '2019-12-16 19:30'
end: '2019-12-16 22:00'
locationName: null
address: Haus Steinstraße Steinstraße 18  Leipzig  04275
link: 'http://www.leipzig-stadtfueralle.de/mein-kalender/'
image: null
teaser: Treffen des Netzwerks "Leipzig – Stadt für alle"Unsere regelmäßigen Treffen haben internen Charakter
recurring: null
isCrawled: true
---
Treffen des Netzwerks "Leipzig – Stadt für alle"Unsere regelmäßigen Treffen haben internen Charakter. Wenn ihr jedoch akute Probleme besprechen wollt, seid ihr dennoch dazu eingeladen.Für alle, die sich über das Recht auf Stadt informieren wollen, veranstalten wir regelmäßig Offene Treffen. Achte auf unseren Kalender oder halte dich über unseren Newsletter auf dem Laufenden! :)Normalerweise treffen wir uns im Leipziger Westen, immer mal auch im Süden. Bitte beachtet die Ortsangabe. 