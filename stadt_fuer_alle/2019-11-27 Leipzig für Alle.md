---
id: 1825-112
title: Leipzig für Alle
start: '2019-11-27 19:00'
end: '2019-11-27 21:00'
locationName: null
address: null
link: 'http://leipzigfueralle.blogsport.eu/'
image: null
teaser: '"Leipzig für alle: Aktionsbündnis Wohnen" trifft sich 14-tägig an wechselnden Orten in Leipzig. Frag'
recurring: null
isCrawled: true
---
"Leipzig für alle: Aktionsbündnis Wohnen" trifft sich 14-tägig an wechselnden Orten in Leipzig. Frag am besten per Mail nach, wo das nächste Treffen stattfindet: leip&#122;i&#103;&#102;&#117;e&#114;&#97;&#108;l&#101;&#64;&#112;&#111;s&#116;eo&#46;de http://leipzigfueralle.blogsport.eu/