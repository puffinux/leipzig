---
id: 1821-112
title: Leipzig für Alle
start: '2019-10-02 19:00'
end: '2019-10-02 21:00'
locationName: null
address: null
link: 'http://leipzigfueralle.blogsport.eu/'
image: null
teaser: '"Leipzig für alle: Aktionsbündnis Wohnen" trifft sich 14-tägig an wechselnden Orten in Leipzig. Frag'
recurring: null
isCrawled: true
---
"Leipzig für alle: Aktionsbündnis Wohnen" trifft sich 14-tägig an wechselnden Orten in Leipzig. Frag am besten per Mail nach, wo das nächste Treffen stattfindet: lei&#112;z&#105;gf&#117;&#101;r&#97;ll&#101;&#64;p&#111;&#115;t&#101;&#111;.&#100;e http://leipzigfueralle.blogsport.eu/