---
id: 1823-112
title: Leipzig für Alle
start: '2019-10-30 19:00'
end: '2019-10-30 21:00'
locationName: null
address: null
link: 'http://leipzigfueralle.blogsport.eu/'
image: null
teaser: '"Leipzig für alle: Aktionsbündnis Wohnen" trifft sich 14-tägig an wechselnden Orten in Leipzig. Frag'
recurring: null
isCrawled: true
---
"Leipzig für alle: Aktionsbündnis Wohnen" trifft sich 14-tägig an wechselnden Orten in Leipzig. Frag am besten per Mail nach, wo das nächste Treffen stattfindet: &#108;e&#105;p&#122;&#105;gf&#117;&#101;&#114;alle&#64;p&#111;s&#116;eo&#46;&#100;&#101; http://leipzigfueralle.blogsport.eu/