---
id: skillshare2020
title: Skillshare in Pödelwitz
start: '2020-01-02 15:00'
end: '2020-01-07 15:00'
locationName: AAA Pödelwitz
address: Pödelwitz 19 (Pfarrgarten vor der Kirche)
link: https://aaapoedi.noblogs.org/skillshare-2020/
image: banner.jpg
teaser: null
recurring: null
isCrawled: false
---
Ein Winter-Skillshare macht sich auf in die zweite Runde, diesmal als Camp. Mit einem dicken Paket an Workshops theoretischer und praktischer Art wird weder dem Körper noch dem Kopf zu kalt. Auch könnt ihr dazu beitragen das dass Programm noch abwechslungsreicher wird. Schreibt uns ne Mail; Kontaktdaten stehen unten.
All das findet nun auch auf neuem Boden statt. Der Pfarrgarten vor der Kirche hat sich zum Projektgarten gemausert und bietet mit Bauwägen, Jurte, Zeltstellfläche Platz für Programm und Unterkunft.
Um infrastrukturell fürs Skillshares gut aufgestellt zu sein brauchen wir eure Hilfe. Vorbeischauen könnt ihr gerne schon ab Dezember, wenn ihr interessiert seit gemeinsam Bauwägen auszubauen, Zelte zu errichten, Kompostklos zimmern und an anderen Dingen die es braucht um eine Campstruktur zusammenbringen. Unabhängig davon könnt ihr uns aber auch einfach so besuchen.

Schreibt uns gern, wenn ihr vorhabt zu kommen, dann können wir besser kalkulieren, wieviel Schlafplätze es braucht und mit Essen und so weider.. Mail steht unten, und unseren Schlüssel findet ihr auch auf der Website 🙂

##### Weiter Infos:

**Essen:** Wir kochen gemeinsam vegane Mahlzeiten.
**Kosten:** Du musst nichts bezahlen um am Skillshare teilzunehmen. Trotzdem freuen wir uns über Spenden, da wir einige Ausgaben haben.
**Mitbringen:** warmer Schlafsack, Zelt, Isomatte, lieber einen Pulli mehr, Musikinstrumente…

##### Liste von Dingen die wir brauchen damit was geht

Wenn du meinst, du könntest etwas davon spenden, dann lass es uns per Mail wissen!

* Plastikfässer/ IBC Tanks
* Decken (Feuerwehr und Deutsches Rotes Kreuz haben manchmal Altbestände abzugeben – top Dämmung)
* Stoff, Klamotten
* Jurte oder großes Zelt (SG-Zelt)
* Öfen + Ofenrohre
* ganz viel Verpackungscarton/Wellpappe (ist Super Dämmung)
* Veganes Essen (einschließlich Gewürze und Öl)
* Decken, Schlafsäcke, Matratzen, Kissen, Teppich (um es gemütlich zu machen)
* Flip Chart
* Holz, Werkzeuge, Nägel, Baumaterial, Stroh
* Besteck, Spülmittel, Schwämme
* Sonnenkollektor, Lampen
* Toilettenpapier und Händedesinfektionsmittel
* Umschläge und Briefmarken, Bücher, Stifte, Papier
* Material für Transpis

Suchen für logistische Angelegenheiten ein Auto, eventl. + Anhänger

##### Kontaktdaten

Schreib uns wenn du Lust hast ins Organisationsteam zu kommen

E-mail: aaa_poedelwitz(at)riseup(dot)net<br>
twitter: twitter.com/AAA_Poedelwitz<br>
Mastodon: https://anarchism.space/@apodelwitz