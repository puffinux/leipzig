---
name: AAA Pödelwitz
website: https://aaapoedi.noblogs.org
email: aaa_poedelwitz@riseup.net
# scrape:
#   source: facebook
#   options:
#     page_id: 682730832127474
# facebook page doesn't exist anymore
---
AAA-Pödelwitz ist ein Zusammenschluss von Menschen, die sich im mitteldeutschen Kohlerevier gegen den Abbau von Braunkohle und für eine klimagerechte Zukunft einsetzen. 