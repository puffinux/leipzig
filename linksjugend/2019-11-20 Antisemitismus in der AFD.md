---
id: "404633060164542"
title: Antisemitismus in der AFD
start: 2019-11-20 18:30
end: 2019-11-20 20:30
locationName: Nebenan
address: Hermann-Liebmann-Straße 89, 04315 Leipzig
link: https://www.facebook.com/events/404633060164542/
image: 75233394_400478637567011_225417063563264000_n.jpg
teaser: 'Vortrag mit Benjamin Männel Teil der Veranstaltungsreihe "Antisemitismus
  in..." Weitere Infos: http://www.ag-debate.tk/   "Als Hendrik M. Broder Anfan'
isCrawled: true
---
Vortrag mit Benjamin Männel
Teil der Veranstaltungsreihe "Antisemitismus in..."
Weitere Infos: http://www.ag-debate.tk/ 

"Als Hendrik M. Broder Anfang des Jahres vor der Bundestagsfraktion der AfD eine Rede hielt, war die Aufregung in der deutschen Linken groß. Wieso bietet eine völkische Partei einem der bekanntesten und schärfsten jüdischen Kritikern der deutschen Gegenwart und Vergangenheit eine Bühne? 
Die AfD präsentiert sich öffentlich als israelfreundliche Partei, die entschlossen gegen Antisemitismus vorgehen würde. Während die ""alte Rechte"" durch ihre Judenfeindschaft für die Mehrheit der wieder gutgewordenen Deutschen unwählbar war, zeichnet sich nunmehr der Trend ab, eine vermeintlich judeo-christliche Leitkultur für die eigene Argumentation heranzuziehen. Damit stellen sich die Heimatverliebten in eine Reihe mit weiteren rechten Parteien in der EU. Woher kommt die neuentdeckte Zuneigung zu jüdischem Leben und Kultur und wie fügt sie sich in das völkische Weltbild und ihren Geschichtsrevisionismus? 
Um diese Fragen zu beantworten, liefert der Vortrag einen kritischen Einblick in die Zusammenhänge rechten "Denkens" und einer falschen Freundschaft zum jüdischen Staat."
