---
id: "1354453628044205"
title: Antisemitismus in der Linken
start: 2019-12-04 19:00
end: 2019-12-04 22:00
address: Kulturbüro Sodann (Mariannenstraße 101)
link: https://www.facebook.com/events/1354453628044205/
image: 73065292_400483900899818_6158684610826862592_n.jpg
teaser: 'Vortrag mit Luise Henckel und Kolja Huth Teil der Veranstaltungsreihe
  "Antisemitismus in..." Weitere Infos: http://www.ag-debate.tk/  "Der Vortrag
  bes'
isCrawled: true
---
Vortrag mit Luise Henckel und Kolja Huth
Teil der Veranstaltungsreihe "Antisemitismus in..."
Weitere Infos: http://www.ag-debate.tk/

"Der Vortrag bestimmt die Genese des modernen Antisemitismus als fest verbunden mit dem Krisenmoment der kapitalistischen Moderne und dem Scheitern des Freiheitsversprechens der bürgerlichen Gesellschaft. Die Krisenerfahrung und das damit verbundene antisemitische Ressentiment bleibt dabei in den folgenden Jahrhunderten latent und zeigt sich immer wieder – mal offen, mal verschoben – in den unterschiedlichen Reaktionsweisen auf diese Erfahrung. 
Der Vortrag wendet dabei vor allem den Blick auf die durchgängige Verbundenheit „linker“ Krisenstrategien mit antisemitischen Narrativen. Bestimmend ist dabei die Annahme, dass eine aneinandergereihte Darstellung von Befunden des antisemitischen Ressentiments genauso wenig hinreichend dafür sein kann, den Antisemitismus auf den Begriff zu bringen, wie eine losgelöst von historischer Erfahrung formulierte Theorie, die schließlich zu herunterzubetenden Merksätzen verkommen muss."