---
id: "808152896324607"
title: Einführung zum Fetisch Begriff in der marxistischen Theorie
start: 2019-11-19 19:00
end: 2019-11-19 22:00
locationName: Interim
address: Demmeringstraße 32, 04177 Leipzig
link: https://www.facebook.com/events/808152896324607/
image: 75096917_10157027037374620_6264614086487572480_n.jpg
teaser: Einführung in die Wertkritik mit Martin Dornis Marx fordert in seinem Werk
  dazu auf, alle Verhältnisse umzustürzen, in denen der Mensch ein elendes, v
isCrawled: true
---
Einführung in die Wertkritik mit Martin Dornis
Marx fordert in seinem Werk dazu auf, alle Verhältnisse umzustürzen, in denen der Mensch ein elendes, verlassenes und geknechtetes Wesen ist. Davon ausgehend soll über grundlegende Probleme der Marxschen Gesellschaftskritik diskutiert werden. Wie verhalten sich die Ideologien zu der Gesellscaft, die sie hervorbringt? Entspringt das Bewußtsein aus dem Sein? Wie kann eine Gesellschaft, die aus sich selbst heraus ihre eigene Legitimation hervorbringt und einem Naturproßez gleich verläuft, überhaupt  kritisiert werden? Und was das alles mit den Krisen zu tun?
