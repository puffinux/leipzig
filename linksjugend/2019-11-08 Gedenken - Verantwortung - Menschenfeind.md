---
id: "2356635387923085"
title: Gedenken - Verantwortung - Menschenfeindlichkeit bekämpfen
start: 2019-11-08 16:30
end: 2019-11-08 18:00
address: Gneisenaustraße 7, 04105 Leipzig
link: https://www.facebook.com/events/2356635387923085/
image: 74586275_3144182422319952_79956013125468160_n.jpg
teaser: Das Aktionsnetzwerk „Leipzig nimmt Platz“ beteiligt sich am 8. November an der
  Stolpersteine-Putz-Aktion und ruft zum Mitmachen auf. Das Aktionsnetzwe
isCrawled: true
---
Das Aktionsnetzwerk „Leipzig nimmt Platz“ beteiligt sich am 8. November an der Stolpersteine-Putz-Aktion und ruft zum Mitmachen auf. Das Aktionsnetzwerk wird 16:30 Uhr in der Gneisenaustraße 7 die Stolpersteine von Daniel David, Hilde und Inge Katzmann sowie Rosa Rothschild putzen und gedenken. Alle Interessierten herzlich eingeladen.

Am 8. November organisiert der Erich- Zeigner Haus e.V., wie in jedem Jahr, das Putzen der mittlerweile über 480 Stolpersteine in Leipzig. Weil der 9. November in diesem Jahr auf den Sabbat fällt – im jüdischen Glauben ein Ruhetag, an dem keine Arbeit verrichtet werden soll – wird die Gedenkaktion „Mahnwache und Stolperstein Putzen“ auf den Freitag verlegt. Alle Informationen und noch offene Orte sind unter https://erich-zeigner-haus-ev.de/neunter-november/ zu finden.