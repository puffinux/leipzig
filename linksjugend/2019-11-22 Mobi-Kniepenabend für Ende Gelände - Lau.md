---
id: "589137335157508"
title: Mobi-Kniepenabend für Ende Gelände - Lausitz Revier
start: 2019-11-22 19:00
end: 2019-11-22 22:00
locationName: Frau Krause
address: Simildenstraße 8, 04277 Leipzig
link: https://www.facebook.com/events/589137335157508/
image: 74308568_10157038517524620_5567053587357368320_n.jpg
teaser: Am 29.11. - 01.12.2019 findet die Ende Gelände Aktion im Lausitz Revier statt.
  Die linksjugend, Jusos und Grüne Jugend Leipzig wollen bei einem gemütl
isCrawled: true
---
Am 29.11. - 01.12.2019 findet die Ende Gelände Aktion im Lausitz Revier statt. Die linksjugend, Jusos und Grüne Jugend Leipzig wollen bei einem gemütlichen Bier in der  Krause Infomaterial verteilen, die Aktion vorstellen und mit euch über Zweck und Nutzen diskutieren. Soweit möglich werden auch Leute von Ende Gelände Leipzig da sein und selbst noch einmal über das Projekt informieren. Kommt also gerne vorbei und genießt das ein oder andere Kaltgetränk mit uns!