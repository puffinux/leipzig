---
id: "571869640319995"
title: Mobi-Veranstaltung zu Protest gegen Naziaufmarsch in Dresden
start: 2020-01-08 18:00
end: 2020-01-08 20:00
address: WERK2-Kulturfabrik
link: https://www.facebook.com/events/571869640319995/
image: 81529172_3325421737529352_3738346616839471104_o.jpg
teaser: Wir wollen mit Vertreter*innen von Dresden Nazifrei über die Mobilisierungen
  der Rechten um den 13. Februar sprechen und darüber beraten, was zu tun i
isCrawled: true
---
Wir wollen mit Vertreter*innen von Dresden Nazifrei über die Mobilisierungen der Rechten um den 13. Februar sprechen und darüber beraten, was zu tun ist. Es wird eine gemeinsame Anreise zu den Protesten organisiert - aber zuerst: kommt vorbei, informiert euch, diskutiert und plant mit, wie wir den Naziaufmarsch im Februar in Dresden stoppen können.

🗓️Datum: 08.01.2020
⏲️ Zeit:  ab 18:00 Uhr 
🗺️ Ort: WERK2-Kulturfabrik.

-----------------------------------------------------------------------------------------

(Immer noch) Kein Ende in Sicht.

In Dresden hat das Gedenken an den 13. Februar und die Zerstörung der Stadt gegen Ende des Zweiten Weltkrieges Tradition. Während bei offiziellen städtischen oder zivilgesellschaftlichen Veranstaltungen nicht nur Kriegstoter aus Dresden, sondern Kriegstoten und Opfer des Faschismus im Allgemeinen gedacht wird, nutzen organisierte Neonazis dieses historische Datum spätestens seit den Neunzigern um Geschichtsrevisionismus und Relativierung der Millionen Toten von Faschismus und Krieg zu betreiben.

Das Bündnis „Nazifrei! – Dresden stellt sich quer“ gründete sich, als die jährliche Nazidemonstration zum größten faschistischen Aufmarsch in Europa geworden war. Durch verschiedene Protestformen und antifaschistische Intervention gelang es, die Aufmärsche einzudämmen. Stadt, Kirchen und Vereine organisieren jährlich eine Menschenkette um die Innenstadt, um symbolisch den Faschisten keinen Zutritt zu gewähren, Andere setzen sich den Nazis in den Weg und konnten so den Aufmarsch durch zivilen Ungehorsam stoppen oder verkürzen.

Auch wenn seit 2009/2010 weniger Nazis durch die Straßen ziehen, sie tun es noch immer. Auch 2020 wollen rechte Akteure in Dresden ihren Gedenkzirkus abhalten. Zu dem "traditionellen" NPD-/Kameradschafts-aufmarsch kommen nun noch Versammlungen von PEGIDA oder AfD.  Doch das wollen nicht alle Dresdner*innen hinnehmen - das Bündnis „Dresden Nazifrei“ will sich "Nazi-Aktivitäten entgegen (stellen), um sie zu verhindern" und dabei auch mit Mitteln des zivilen Ungehorsams agieren. Das wollen wir unterstützen und die Dresdner*innen nicht allein lassen, sondern mit Ihnen gemeinsam Widerspruch und Gegenwehr gegen die Faschisten organisieren.