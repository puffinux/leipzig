---
id: "816035302145975"
title: Antisemitismus in feministischen Kontexten
start: 2019-11-30 16:00
end: 2019-11-30 19:00
address: interim (Demmeringstraße 32)
link: https://www.facebook.com/events/816035302145975/
image: 72721868_400480714233470_3962533945366544384_n.jpg
teaser: 'Workshop mit Randi Becker Teil der Veranstaltungsreihe "Antisemitismus in..."
  Weitere Infos: http://www.ag-debate.tk/   „Feminist*innen bezeichnen sic'
isCrawled: true
---
Workshop mit Randi Becker
Teil der Veranstaltungsreihe "Antisemitismus in..."
Weitere Infos: http://www.ag-debate.tk/ 

„Feminist*innen bezeichnen sich heute oft als „intersectional feminist“. Das soll abbilden, dass Feminist*innen für sich beanspruchen, nicht nur gegen sexistische Diskriminierung zu kämpfen, sondern für einen inklusiven Feminismus stehen wollen, der auch die Kategorien race und class, also rassistische und klassistische Diskriminierungen in den Blick nimmt. So versuchen aktuelle feministische Strömungen Feminismus nicht nur als Bewegung weißer Mittelstandsfrauen zu definieren, sondern Feminismus als gemeinsamen Kampf unterschiedlichster Frauen* und LGBTIQ zu verstehen.
Trotz dieser vermeintliche Sensibilität gegenüber verschiedener Diskriminierungsformen nehmen die antisemitischen Tendenzen in aktuellen feministischen Strömungen zu!
Viele (queer-) feministische Gruppen unterstützen BDS (Boycott Divestment Sanctions), werfen Israel Homonationalismus und Pinkwashing vor und vertreten so antizionistische und antisemitische Positionen.
Im Workshop wollen wir uns mit unterschiedlichen feministischen Antisemitismen beschäftigen und diskutieren, wie unser Feminismus tatsächlich „intersectional“, im Sinne von alle Diskriminierungsformen reflektierend, sein kann.
Wir werfen dabei sowohl einen Blick auf historischen Antisemitismus in der (deutschen) Frauenbewegung, als auch auf aktuelle Phänomene von feministischem Antisemitismus.“
