---
id: "2528546397390807"
title: No Risk = No Fun? - Risikominimierung beim Partydrogengebrauch
start: 2020-01-28 18:00
end: 2020-01-28 20:00
locationName: Linksjugend Leipzig
address: Brandstraße 15, 04277 Leipzig-Connewitz, Sachsen
link: https://www.facebook.com/events/2528546397390807/
image: 79472411_10157137056349620_2092778086910132224_o.jpg
teaser: Upper, Downer, Halluzinogene – Was ist das? Mit einer Substanzkunde zu legalen
  und illegalisierten Drogen bieten die Drug Scouts einen Raum, um sich ü
isCrawled: true
---
Upper, Downer, Halluzinogene – Was ist das? Mit einer Substanzkunde zu legalen und illegalisierten Drogen bieten die Drug Scouts einen Raum, um sich über psychoaktiven Substanzen zu informieren. Außerdem wird ein Blick auf die verschieden Bestandteile eines Rauschs geworfen und die Frage beantwortet, wie man Risiken beim Konsumieren minimieren kann. 