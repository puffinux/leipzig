---
id: "2680031882048808"
title: Antisemitismus in deutschem Rap
start: 2019-11-16 16:00
end: 2019-11-16 19:00
address: linXXnet (Brandstraße 15)
link: https://www.facebook.com/events/2680031882048808/
image: 73088672_400476130900595_104224369349230592_n.jpg
teaser: 'Workshop mit Chucky Goldstein Teil der Veranstaltungsreihe "Antisemitismus
  in..." Weitere Infos: http://www.ag-debate.tk/   "Kaum jemand scheint heute'
isCrawled: true
---
Workshop mit Chucky Goldstein
Teil der Veranstaltungsreihe "Antisemitismus in..."
Weitere Infos: http://www.ag-debate.tk/ 

"Kaum jemand scheint heute noch die Existenz von Antisemitismus im Deutschrap leugnen zu wollen oder können. Auch scheinen sich innerhalb der Szene fast alle darüber einig zu sein, dass Antisemitismus nichts mit Deutschrap gemein haben dürfe. Im Rap-Selbstverständnis als „Zeitung des Ghettos“ (Freundeskreis) wird Rap umstandslos erstmal auf die Seite der Emanzipation und der Rebellion gestellt und die AntisemitInnen so nur als Ausbrecher begriffen. Die wirkliche Uneinigkeit besteht nur darin, wie Antisemitismus zu begreifen sei und wie viel Israelkritik noch legitim ist.
Auf der anderen Seite steht für die Rapszene immer die Mehrheitsgesellschaft, gegen die sich ihre Rebellion richtet. Unabhängig davon, dass im Anbetracht des aktuellen kommerziellen Erfolgs diese Gegenüberstellung in erster Linie eine (beidseitige) ideologische Abgrenzung ist, wird aus genau dieser Position eine Kritik am Antisemitismus im Deutschrap geübt. Diese Position abstrahiert notwendig von der gesellschaftlichen Basis, aus der sie selbst hervorgeht und Antisemitismus scheint ihr immer nur Widerspruch zur Idee der freien und gleichen Staatsbürger sein.
Gegen diese Kritik richtet sich der materialistische Begriff des Antisemitismus als notwendige Denkform der kapitalistischen Vergesellschaftung, ebenso wie er den Antisemitismus im Deutschrap nicht als bloßen Zufall behandeln kann. Der Workshop möchte ein Schlaglicht auf die Gesellschaft richten innerhalb der sich Deutschrap entwickeln konnten, gegen die er vermeintlich aufbegehrt und doch reproduziert. Es soll nachgezeichnet werden, warum bei einer Zeitung als warenförmiges Produkt ihr Inhalt so lange austauschbar bleibt, wie sie von ihrer eigenen Form abstrahiert. Antisemitismus wird als eine in dieser Abstraktion notwendig existierende Denkform begriffen."

