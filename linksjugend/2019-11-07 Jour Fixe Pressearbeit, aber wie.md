---
id: "2642783479076861"
title: "Jour Fixe: Pressearbeit, aber wie?"
start: 2019-11-07 19:00
end: 2019-11-07 22:00
locationName: Interim
address: Demmeringstraße 32, 04177 Leipzig
link: https://www.facebook.com/events/2642783479076861/
image: 74634728_10157010182214620_4924852609627455488_n.jpg
teaser: Statt des wöchentlichen Plenums wollen wir uns am Donnerstag den 7.11.,
  abseits von Protokollen und Formalia, treffen um mit einem Journalisten über P
isCrawled: true
---
Statt des wöchentlichen Plenums wollen wir uns am Donnerstag den 7.11., abseits von Protokollen und Formalia, treffen um mit einem Journalisten über Pressearbeit zu reden. Dabei Tipps und Tricks für Pressemitteilungen auszutauschen und einen gemütlichen Abend zusammen verbringen, damit auch Menschen die neu dabei sind uns mal mehr kennenlernen können und möglicherweise einen guten Einstieg in unsere Strukturen bekommen können. 