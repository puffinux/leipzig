---
id: '449174012492895'
title: Demonstration AM 8. MÄRZ | aus allen vier Himmelsrichtungen
start: '2019-03-08 15:00'
end: '2019-03-08 19:00'
locationName: null
address: Leipzig
link: 'https://www.facebook.com/events/449174012492895/'
image: 52559012_406632210102591_4032127258968719360_n.jpg
teaser: '---- english and spanish version, arabic short version below ----- Am 8. März ist Feministischen Kampftag, auch bekannt als Internationaler Frauentag.'
recurring: null
isCrawled: true
---
---- english and spanish version, arabic short version below -----
Am 8. März ist Feministischen Kampftag, auch bekannt als Internationaler Frauentag. Seit mittlerweile über 100 Jahren gehen wir an diesem Tag für eine gerechte und emanzipatorische Gesellschaft auf die Straße!
Dabei hat sich viel verändert, inzwischen gibt es das Wahlrecht für Frauen, die Elternzeit, die Ehe für Alle... Ist nicht schon alles erreicht? 
Schauen wir doch mal genauer hin..

Immer noch erledigen Frauen* 75-90% der Hausarbeit! Immer noch finden sich Frauen* und Queers** mehrheitlich in schlecht bezahlten Berufen wieder! Immer noch wird jeden dritten Tag ein Femizid (geschlechtsspezifische Tötung an Mädchen und Frauen*) in Deutschland verübt! Immer noch haben LGBTI* ein vier- bis sechsmal erhöhtes Suizid-Risiko! Geflüchtete und illegalisierte Frauen* und Queers erleben tagtäglich Repressionen und leben oft in Rechtlosigkeit und ständiger Angst vor Abschiebung!

Und die Aussichten:
Die Gewalt gegen Frauen* und Queers nimmt nicht ab. Sachsen bekommt ein Polizeiaufgabengesetz, das den  Rassismus dieser Gesellschaft fördern wird. Die AfD will  Vereinen , die sich für geschlechtliche und sexuelle Selbstbestimmung engangieren die staatliche Förderung streichen.

Angesichts dieser konservativen und rechten Kräfte wollen wir zusammenstehen und zeigen: Wir sind immer noch mehr! 
Wir lassen uns nicht zurück an den Herd schicken! Wir lassen uns nicht mehr vorschreiben, wann und wie wir Kinder bekommen wollen! Wir lassen uns nicht mehr abwerten und belästigen, nicht zu Hause, nicht im Job und auch nicht auf der Straße!

WIR HABEN GENUG!
------------------------------------------------------------------------------------
Lasst uns LAUT und BUNT und FORDERND sein! 
Kommt mit uns um 15 Uhr AUS ALLEN VIER HIMMELSRICHTUNGEN:

Osten: RABET –
Für eine solidarische Gesellschaft ohne Ausgrenzung und Rassismus!
Süden: SÜDPLATZ – 
Für sexuelle und körperliche Selbstbestimmung und Identität!
Westen: LINDENAUER MARKT – 
Für die Aufwertung und gerechte Verteilung von Care-Arbeit!
https://www.facebook.com/events/346452269300482/
Norden: HUYGENSPLATZ (Fahrraddemo) – 
Für eine gemeinsame und gerechte Organisierung der gesellschaftlich notwendigen Arbeit!
(Start an der Agentur für Arbeit)
    
 dann treffen wir zusammen zur Abschlusskundgebung
 um 17 Uhr am AUGUSTUSPLATZ!
-------------------------------------------------------------------------------------- 
Letztes Jahr nahmen weit über zwölf Millionen Frauen* und Queers in Argentinien, Chile, Mexico, USA, Canada, Schottland, Spanien und Polen an internationalen Streiks teil. Weitaus mehr Menschen protestierten und demonstrierten international. Lasst es uns ihnen gleich tun! In Deutschland wird in diesem Jahr in über 40 Städten gestreikt und protestiert - lasst uns dafür sorgen, dass in Leipzig am 8. März kein Alltag einkehrt!

Sagt all euren Freund*innen, Großeltern, Nachbar*innen, Kolleg*innen und Paketbot*innen Bescheid, je mehr kommen, desto schöner wird’s!
    
Gemeinsam gegen Sexismus, Rassismus und alle weiteren Formen von Diskriminierung! Gegen Verarmung und Entrechtung! Gegen rechte Hetze und Nationalismus! Machen wir unseren Protest, unsere Forderungen und unsere Vorstellungen von einer geschlechtergerechten und solidarischen Gesellschaft jenseits von Patriarchat und Kapitalismus sicht- und erfahrbar!
- ALLE Menschen sind auf den Demonstrationen willkommen! -

Schlechtes Wetter, harte Zeiten, für den Feminismus fighten!
 
*Das Sternchen soll das Spektrum vielfältigster Geschlechtsidentitäten, Körperlichkeiten und Ausdrucksweisen verdeutlichen. Wenn also von »Frauen*« die Rede ist, sind nicht nur cis-Frauen gemeint (Frauen, bei denen Geschlechtsidentität und bei der Geburt zugewiesenes Geschlecht übereinstimmen). Das Sternchen dient der Inklusion diverser Geschlechtsidentitäten, welche sich als »Frauen« identifizieren oder als solche behandelt werden.

**Queer: Queer kann als Überbegriff für alle sexuellen Orientierungen und Geschlechtsidentitäten gelten, die nicht der gesellschaftlichen Norm von Geschlecht und Sexualität entsprechen. Queer beschreibt aber auch eine Denkrichtung, die sich gegen Schubladendenken wehrt. 
Queer war zunächst Ausdruck eines politischen Aktivismus in den USA (Queer Politics) bzw. einer Denkrichtung (Queer Theory / Queer Studies). Queer bedeutet heute vor allem jegliche Abweichung von Heteronormativität und der Binarität der Geschlechter. Allgemein aber stellt sich der Begriff und die damit verbundene Theorie gegen eine exakte Definition.
----------------------------------------------------------------------------
DEMONSTRATION ON MARCH 8TH 
- Call for extraordinary conditions (in Leipzig) -
 
March 8 is the day of feminist struggles, also known as International Women's Day. For over 100 years now we have been taking to the streets for an equal and emancipatory society!  Much has changed – nowadays women are entitled to vote, we can take parental leave, marriage for all exists... Haven't we already achieved everything?
Let's take a closer look...

Women* still do 75-90% of the housework! Women* and queers** mostly still work in low paid jobs! And still every third day a femicide (gender-specific killing of girls and women*) is committed in Germany! For LGBTI* the risk of suicide is still four to six times higher! Illegalised women* and queers and those who have fled experience repressions on a daily basis. Often they live in lawlessness and constant fear of deportation!
And the prospects:
Violence against women* and queers is not decreasing. Saxony will get a police law that will promote the racism in this society. The AfD party wants to abolish state funds for associations that are committed to sexual and sexual self-determination.
In view of these conservative and right-wing forces we want to stand together and show them that we are still more! 
We will not be sent back to the stove! We will no longer allow that others dictate when and how we want to have children! We will no longer allow that others devalue and harass us, not at home, not at work or in the streets!

We have enough!
---------------------------------------------- 
Let’s be LOUD and COLORFUL and
Let’s DEMAND CHANGES! 
Join us at 3 p.m. FROM ALL FOUR CARDINAL DIRECTIONS:
(Exact locations tba)
East: Rabet - For a solidary society without exclusion and racism!
South: Südplatz - For sexual and physical self-determination and identity!
West: Lindenauer Markt - For a greater appreciation and fair distribution of care work!
North: Huygensplatz (Huygensstraße/Seelenbinderstraße) - For a fair and equal organization of work in society!
(Start at Agentur für Arbeit) -> by BIKE!
    
After that, we will come together
at 17 o'clock at the AUGUSTUSPLATZ
for the final rally!
---------------------------------------------------
Last year, more than twelve million women* and queers in Argentina, Chile, Mexico, USA, Canada, Scotland, Spain and Poland took part in international strikes. On an international level, even more people protested and demonstrated. Let's do the same!
This year, in Germany, there will be strikes and protests in more than 40 cities - let's make sure that March 8th will not be an ordinary day in Leipzig!

Tell all your friends, grandparents, neighbours, colleagues and parcel delivery staff that the more people come, the nicer it will be!
    
Together against sexism, racism and all other forms of discrimination! Against impoverishment and deprivation of rights! Against right-wing ideologies and nationalism! Let us make our protest, our demands and our ideas of a society based on gender justice and solidarity beyond patriarchy and capitalism visible and tangible!
- ALL people are welcome at the demonstrations! -

Fight for feminism! In good times and in bad.

*The asterisk is used to illustrate the diverse spectrum of gender identities, physical abilities and modes of expression. So when we talk about "women*", we do not only mean cis women (women whose gender identity matches the sex they are assigned at birth). The asterisk also includes various gender identities that identify themselves as "women" or are treated as such.

**Queer: Queer can be used as an umbrella term for all sexual orientations and gender identities that do not correspond to the social norm of gender and sexuality. Queer also describes a way of thinking that is contrary to thinking in rigid categories. Initially, queer was an expression of political activism in the USA (Queer Politics) or a line of thought (Queer Theory / Queer Studies). Today, queer means above all any deviation from heteronormativity and the binarity of the sexes. In general, however, the term and the theory associated with it oppose a closed definition.
----------------------------------------------------------------
MANIFESTACIÓN EL 8 DE MARZO 
- Llamamiento por una situación excepcional (en Leipzig) -
 
El 8 de marzo es el Día de la lucha feminista, también conocido como Día Internacional de la Mujer. Durante más de 100 años hemos salido a las calles por una sociedad más justa y emancipadora!  Mucho ha cambiado – hoy en día las mujeres tienen el derecho de voto, existe el permiso de paternidad y el matrimonio para todos.... ¿No se ha logrado ya todo?
Analicemos la situacion con más detalle....

Las mujeres* siguen realizando entre el 75 y el 90% de las tareas domésticas! Las mujeres* y las personas queer**, con mucha frecuencia, aún siguen teniendo trabajos mal pagados! Todavía cada tres días se comete un feminicidio (asesinato de niñas y mujeres* por el sólo hecho de su género) en Alemania! El riesgo de suicidio es cuatro a seis veces mayor en lxs LGBTI*! Las mujeres y personas queer ilegalizadas* y las que tenián que huir experimentan represiones a nivel diario.  Muy a menudo también están privadas de todo derecho y viven con el miedo constante a ser deportadas.

Y las perspectivas:
La violencia contra las mujeres* y personas queer** no disminuye. Sajonia va a implementar una nueva ley policial que promoverá el racismo en la sociedad. El partido AfD quiere anular los fondos estatales dirigidos a las asociaciones comprometidas con la autodeterminación de sexo y género.
En vista de estas fuerzas conservadoras y a la derecha, queremos mantenernos unidxs y demostrar que: Todavía somos más! 
¡No nos enviarán de vuelta a la estufa! Ya no permitimos que otros dicten cuándo y cómo queremos tener hijos! Ya no permitimos que nos desaprecien y acosen, ni en casa, ni en el trabajo, ni en la calle!

¡Ya basta!
El año pasado, más de doce millones de mujeres* y personas queer** en Argentina, Chile, México, Estados Unidos, Canadá, Escocia, España y Polonia participaron en huelgas internacionales. Aún más gente protestó y se manifestó internacionalmente. Sigamos su ejemplo!
En Alemania este año habrá huelgas y manifestaciones en más de 40 ciudades - ¡Aseguremos de que  el 8 de marzo no va a ser un día ordinario den Leipzig!
 
¡Seamos la mayoría diversa y ruidosa! ¡Demandemos cambios!
Ven con nosotros a las 15:00 DESDE TODOS LOS CUATRO PUNTOS CARDINALES:
(Los lugares exactos serán anunciados tan pronto como sea posible)
Este: Rabet - ¡Por una sociedad solidaria sin exclusión y sin racismo!
Sur: Südplatz - ¡Por la autodeterminación sexual y física e identidad!
West: Lindenauer Markt - ¡Por una mayor valoración y una distribución más justa del trabajo de cuidados!
Norte: Huygensplatz (Huygensstraße/Seelenbinderstraße) - ¡Por la igualdad de remuneración y por condiciones de trabajo justas!
    
 luego nos reunimos para la marcha final
 a las 17 en punto en el AUGUSTUSPLATZ!
    
Díselo a todxs tus amigxs*, abuelxs, vecinxs*, colegas* y lxs carterxs* que cuanta más gente venga, más bonito será!
    
Juntxs contra el sexismo, el racismo y todas las demás formas de discriminación! Contra el empobrecimiento y la privación de derechos! Contra la derecha y el nacionalismo! Hagamos visible y tangible nuestra protesta, nuestras demandas y nuestras ideas de una sociedad basada en la justicia de género y la solidaridad más allá del patriarcado y el capitalismo!
- TODA la gente es bienvenida en las manifestaciones! -

Luchar por el feminismo! En los buenos como enlos malos tiempos!
 
*El asterisco tiene por objeto ilustrar el espectro de las diversas identidades de género, fisicalidades y modos de expresión. Por lo tanto, cuando hablamos de "mujeres*", no nos referimos sólo a las mujeres cis (mujeres cuya identidad de género es igual al sexo que fueron asignadxs al nacer). El asterisco también sirve para incluir las varias identidades de género que se identifican como "mujeres" o son tratadas como tales.

**Queer: Queer puede ser usado como un término paraguas para todas las orientaciones sexuales e identidades de género que no corresponden a la norma social de género y sexualidad. Queer también describe una forma de pensar que es lo contratio de pensar en categorías fijas. Queer fue inicialmente una expresión del activismo político en los Estados Unidos (Queer Politics) o una línea de pensamiento (Queer Theory / Queer Studies). Hoy en día significa sobre todo cualquier desviación de la heteronormatividad y la binaridad de los sexos. En general, sin embargo, el término y la teoría asociada con él se oponen a una definición exacta.
------------------------------------------------------------------------
يصادف الثامن من آذار يوم الإضراب النسائي او ما يعرف باليوم العالمي للمرأة. منذ ما يزيد عن مئة عام نعتصم في هذا اليوم من
أجل مجتمع عادل و متحرر! نريد الوقوف معا في وجه القوى اليمينية لكي يرى المجتمع بأننا نشكل الغالبية العظمى. لا و لن نسمح
بإرسالنا مجددا الى المطبخ. لن يُفرض علينا متى و كيف ننجب أطفالنا. لن نصمح بالتقليل من قيمتنا و لا بالتحرش بنا لا في الشارع,
لا في العمل و لا حتى في المنزل.
هذا يكفي!
دعونا نكن مسموعين و متنوعين و مطالبين!
شاركونا في الساعة الثالثة بعد الظهر و تعالوا معنا من جميع الاتجاهات ) اماكن التجمع سوف تحدد قريبا (
ابتداءا من الساعة الثانية عشرة سوف تبدأ الفعاليات في الاماكن التالية وفقا للمواضيع التالية:
في منطقة شرق المدينة: Rabet  من أجل مجتمع متضامن خالي من التحييد والعنصرية.
في منطقة جنوب المدينة: Südplatz  من أجل تقرير المصير الجنسي و الجسدي و تحديد الهوية.
في منطقة غرب المدينة:  Lindenauer Mark  من أجل التوزيع المنصف لأعمال العناية و إعلاء شأنها.
في منطقة شمال المدينة:  في باحة الجوب سنتر من امام الباب الرئيسي  من أجل مساواة في الأجور و العدالة في ظروف العمل.
المسيرات الأربعة سوف تلتقي في الساعة الخامسة بعد الظهر في Augustusplatz للقيام بالمظاهرة الختامية.
معا ضد التمييزبكل أصنافه سواء على أساس جنسي أو عرقي أو أي شكل من أشكاله ! ضد الإفقار و التجريد من الحقوق! ضد
التحريض اليميني و القومي.
جميع الناس مرحب باشتراكهم معنا في المظاهرة.


