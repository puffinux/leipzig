---
id: '305274513747860'
title: 'Demo: Urteil umsetzen - Radfahren am Promenadenring ermöglichen'
start: '2019-06-03 17:00'
end: '2019-06-03 20:00'
locationName: null
address: 'Augustusplatz, 04109 Leipzig'
link: 'https://www.facebook.com/events/305274513747860/'
image: 61286522_2702533619761485_9108782249267953664_n.jpg
teaser: Seit über 20 Jahren kämpft der ADFC Leipzig für die Verbesserung der Radfahrbedingungen am Promenadenring. Ein wichtiger Schritt zur Verbesserung war
recurring: null
isCrawled: true
---
Seit über 20 Jahren kämpft der ADFC Leipzig für die Verbesserung der Radfahrbedingungen am Promenadenring. Ein wichtiger Schritt zur Verbesserung war der Sieg des ADFC vor dem Oberverwaltungsgericht in Bautzen. Dieses hat im September 2018 geurteilt, dass das Radfahrverbot weitestgehend rechtswidrig ist. Die Stadt hat daraufhin zugesagt, die Radfahrbedingungen am Promenadenring zu verbessern. Passiert ist seit September 2018 jedoch nichts. Wir wollen mit der Demo auf diesen Missstand hinweisen und Druck auf die Stadtverwaltung ausüben, denn es ist zu befürchten, dass die Stadtverwaltung das Urteil nicht umsetzt, sondern aussitzt. Das Aussitzen ist schon deshalb problematisch, weil ein Großteil der Schilder die den Radverkehr am Promenadenring betreffen, keine rechtliche Wirkung mehr entfalten. D.h. wer will, kann also seit Monaten in weiten Teilen auf dem Promenadenring mit dem Rad fahren.