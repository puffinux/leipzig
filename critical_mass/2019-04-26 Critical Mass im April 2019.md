---
id: '2080042035447492'
title: Critical Mass im April 2019
start: '2019-04-26 18:00'
end: '2019-04-26 21:00'
locationName: null
address: 'Augustusplatz, 04109 Leipzig'
link: 'https://www.facebook.com/events/2080042035447492/'
image: 56644928_701751536889611_3267028888735186944_n.jpg
teaser: An jedem letzten Freitag im Monat trifft sich die Critical Mass zur gemeinsamen Ausfahrt.
recurring: null
isCrawled: true
---
An jedem letzten Freitag im Monat trifft sich die Critical Mass zur gemeinsamen Ausfahrt.