---
id: '2719918918080554'
title: Critical Mass im Mai 2019
start: '2019-05-31 18:00'
end: '2019-05-31 21:00'
locationName: null
address: 'Augustusplatz, 04109 Leipzig'
link: 'https://www.facebook.com/events/2719918918080554/'
image: 59788889_720473805017384_7563598804537049088_n.jpg
teaser: Am letzten Freitag im Monat trifft sich die Critical Mass Leipzig um 18Uhr auf dem Augustusplatz zur gemeinsamen Ausfahrt.
recurring: null
isCrawled: true
---
Am letzten Freitag im Monat trifft sich die Critical Mass Leipzig um 18Uhr auf dem Augustusplatz zur gemeinsamen Ausfahrt.