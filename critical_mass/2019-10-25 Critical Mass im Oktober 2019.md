---
id: "814396602291707"
title: Critical Mass im Oktober 2019
start: 2019-10-25 18:00
end: 2019-10-25 21:00
address: Augustusplatz, 04109 Leipzig
link: https://www.facebook.com/events/814396602291707/
image: 72718416_820903424974421_2822447911869087744_n.jpg
teaser: Am letzten Freitag im Monat trifft sich die Critical Mass zur gemeinsamen
  Ausfahrt auf dem Augustusplatz (Gewandhausseite)
isCrawled: true
---
Am letzten Freitag im Monat trifft sich die Critical Mass zur gemeinsamen Ausfahrt auf dem Augustusplatz (Gewandhausseite)