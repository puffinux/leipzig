---
id: "2152586098377937"
title: Critical Mass im November 2019
start: 2019-11-29 18:00
end: 2019-11-29 21:00
address: Augustusplatz, 04109 Leipzig
link: https://www.facebook.com/events/2152586098377937/
image: 76905974_836971503367613_8275577036433522688_n.jpg
teaser: Am letzten Freitag im Monat trifft sich die Critical Mass zur gemeinsamen
  Ausfahrt
isCrawled: true
---
Am letzten Freitag im Monat trifft sich die Critical Mass zur gemeinsamen Ausfahrt