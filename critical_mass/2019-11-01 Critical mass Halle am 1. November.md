---
id: "380015149549056"
title: Critical Mass Halle - Trauerfahrt
start: 2019-11-01 18:00
end: 2019-11-01 19:30
locationName: critical mass Halle
address: August-Bebel-Platz, 06108 Halle, Saxony-Anhalt
link: https://www.facebook.com/events/380015149549056/
image: 76695083_2646780208676547_9136280798855430144_n.jpg
teaser: "UPDATE: Am 29.10.2019 wurde eine 20-jährige Radfahrerin an der Kreuzung
  Reilstraße/Paracelsusstraße von einem abbiegenden LKW erfasst und ist tödlich"
isCrawled: true
---
UPDATE: Am 29.10.2019 wurde eine 20-jährige Radfahrerin an der Kreuzung Reilstraße/Paracelsusstraße von einem abbiegenden LKW erfasst und ist tödlich verunglückt. Die Critical Mass möchte dafür zum Gedenken anhalten und bei der Fahrt am 1. November am Unfallort Blumen und Kerzen niederlegen und ein Ghostbike aufstellen.

Wir werden wie üblich 18 Uhr am August-Bebel-Platz starten und zeigen, dass wir trotz und gerade wegen der Ereignisse weiter unseren Raum im Straßenverkehr einfordern. Zunächst werden wir einen Silent Ride, also eine stille Critical Mass, zur Unfallstelle an der Kreuzung Reilstraße/Paracelsusstraße fahren. Dort werden wir ein Ghostbike aufstellen, Blumen und Kerzen niederlegen und eine Gedenkminute abhalten. Die Trauerveranstaltung wird durch die Polizei abgesichert. Anschließend werden wir die Critical Mass wie wir sie gewohnt sind fortsetzen: Die Gruppe fährt als Verband auf einer Spur auf der Fahrbahn und hält sich an die Verkehrsregeln. Alle Mitfahrenden bleiben möglichst kompakt zusammen, um dem motorisierten Verkehr nicht Gelegenheit zu geben, in Lücken hineinzufahren und die Masse in Teilgruppen zu zerreißen.
Wir bleiben friedlich und lassen uns nicht durch aggressive Autofahrende provozieren. Der Verkehr wird nicht absichtlich gestört. Es geht nicht um die Behinderung anderer, sondern darum, sich als unmotorisierte/r Verkehrsteilnehmer/in ein Stück öffentlichen Lebensraum, die Straße, zurückzuerobern.
Jede/r, der/die mitfährt, ist für sich selbst verantwortlich. Habt trotzdem gern ein Auge aufeinander und helft euch.

Bis Freitag
