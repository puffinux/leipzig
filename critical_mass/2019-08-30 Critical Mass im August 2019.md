---
id: '1302407743278244'
title: Critical Mass im August 2019
start: '2019-08-30 18:00'
end: '2019-08-30 21:00'
locationName: null
address: 'Augustusplatz, 04109 Leipzig'
link: 'https://www.facebook.com/events/1302407743278244/'
image: 68523287_779839765747454_8470408628564656128_n.jpg
teaser: Am letzten Freitag im Monat trifft sich die Critical Mass Leipzig zur gemeinsamen Ausfahrt.
recurring: null
isCrawled: true
---
Am letzten Freitag im Monat trifft sich die Critical Mass Leipzig zur gemeinsamen Ausfahrt.