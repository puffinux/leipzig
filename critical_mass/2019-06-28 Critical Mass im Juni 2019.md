---
id: '626057271228539'
title: Critical Mass im Juni 2019
start: '2019-06-28 18:00'
end: '2019-06-28 21:00'
locationName: null
address: 'Augustusplatz, 04109 Leipzig'
link: 'https://www.facebook.com/events/626057271228539/'
image: 64404251_743486892716075_3157732004095590400_n.jpg
teaser: Am letzten Freitag im Monat trifft sich die Critical Mass Leipzig zur gemeinsamen Ausfahrt.
recurring: null
isCrawled: true
---
Am letzten Freitag im Monat trifft sich die Critical Mass Leipzig zur gemeinsamen Ausfahrt.