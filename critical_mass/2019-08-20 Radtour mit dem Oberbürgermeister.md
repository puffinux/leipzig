---
id: '652995108554978'
title: Radtour mit dem Oberbürgermeister
start: '2019-08-20 17:00'
end: '2019-08-20 20:00'
locationName: null
address: 'Haltestelle "Sportforum Süd", Wochenmarkt'
link: 'https://www.facebook.com/events/652995108554978/'
image: 68243009_2837299146284931_7576207024631840768_n.jpg
teaser: 'Auch in diesem Jahr fahren wir mit OBM Jung durch die Stadt, diesmal durch den Leipziger Westen.'
recurring: null
isCrawled: true
---
Auch in diesem Jahr fahren wir mit OBM Jung durch die Stadt, diesmal durch den Leipziger Westen.