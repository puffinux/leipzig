---
id: '408765093022902'
title: Critical Mass im März 2019
start: '2019-03-29 18:00'
end: '2019-03-29 21:00'
locationName: null
address: 'Augustusplatz, 04109 Leipzig'
link: 'https://www.facebook.com/events/408765093022902/'
image: 53046594_684688868595878_7871232185074712576_n.jpg
teaser: Am letzten Freitag im Monat trifft sich die Critical Mass zur gemeinsamen Ausfahrt.
recurring: null
isCrawled: true
---
Am letzten Freitag im Monat trifft sich die Critical Mass zur gemeinsamen Ausfahrt.