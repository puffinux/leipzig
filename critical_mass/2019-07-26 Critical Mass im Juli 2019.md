---
id: '2449681518388036'
title: Critical Mass im Juli 2019
start: '2019-07-26 18:00'
end: '2019-07-26 21:00'
locationName: null
address: 'Augustusplatz, 04109 Leipzig'
link: 'https://www.facebook.com/events/2449681518388036/'
image: 67453232_766338337097597_3944179776289243136_n.jpg
teaser: Am letzten Freitag im Monat trifft sich die Critical Mass Leipzig zur gemeinsamen Ausfahrt.
recurring: null
isCrawled: true
---
Am letzten Freitag im Monat trifft sich die Critical Mass Leipzig zur gemeinsamen Ausfahrt.