---
id: '346073966230039'
title: 'Promenadenring: Vergangenheit und Zukunft - Neujahrstreffen'
start: '2019-02-01 19:00'
end: '2019-02-01 22:00'
locationName: Moritzbastei
address: 'Kurt-Masur-Platz 1, 04109 Leipzig'
link: 'https://www.facebook.com/events/346073966230039'
image: null
teaser: null
recurring: null
isCrawled: true
---
