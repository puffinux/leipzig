---
id: '715579875625877'
title: 'Demo: Klimaschutz ernst nehmen - autofreien Platz schaffen'
start: '2019-06-28 21:00'
end: '2019-06-30 21:00'
locationName: null
address: Leipzig Hauptbahnhof
link: 'https://www.facebook.com/events/715579875625877/'
image: 64787561_2744172212264292_1267429825253998592_n.jpg
teaser: '"Wir haben nichts zu verlier‘n, außer unsrer Angst. Es ist unsre Zukunft" Fridays For Future Leipzig und ADFC Leipzig lassen es vor den Sommerferien n'
recurring: null
isCrawled: true
---
"Wir haben nichts zu verlier‘n, außer unsrer Angst. Es ist unsre Zukunft"
Fridays For Future Leipzig und ADFC Leipzig lassen es vor den Sommerferien noch mal so richtig krachen – und wir wollen euch dabei haben. 

Da die Verkehrswende auf sich warten lässt und auch im Bereich Klimaschutz in Leipzig überschaubar wenig passiert, wollen wir mit euch den Druck auf Politik und Verwaltung erhöhen. Wir nehmen uns gemeinsam für 48 Stunden den Platz zwischen Hauptbahnhof und Straßenbahnhaltestelle. Vom Freitag, 28. Juni, ab 21 Uhr bis zum Sonntag, 30. Juni, 21Uhr laden wir zur Demonstration. Dieser Bereich wird dann in dieser Zeit (fast) autofrei :)
Und wer jetzt denkt: "Geht ja gar nicht!", können wir entgegnen: "Geht ja wohl!".

Wir haben bereits Programmpunkte zusammengetragen und würden uns freuen, wenn ihr auch mitmacht und euch zeigt. 

Teilt uns bitte eure Beiträge bis spätestens Dienstag, 25. Juni an info@adfc-leipzig.de mit.

Wenn ihr keinen Beitrag leisten könnt (oder wollt): Im Sinne der Klimaschutzbewegung wollen wir, dass viele Menschen ihren Fußabdruck mit Kreide auf die Fahrbahn malen. Kommt vorbei, werdet Teil der Bewegung und hinterlasst euren Fußabdruck als Zeichen der Solidarität :)
