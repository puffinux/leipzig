---
id: "2591865084378497"
title: Alle fürs Klima - Fahrraddemo
start: 2019-11-29 13:30
end: 2019-11-29 15:30
address: Johannisplatz, 04103 Leipzig
link: https://www.facebook.com/events/2591865084378497/
image: 76751415_3105769669437876_3075614437009260544_n.jpg
teaser: "Im Rahmen des Klimastreiks machen wir den Nordzubringer zur
  Großdemo:   13:30Uhr Johannisplatz 14Uhr, Schillergymnasium und 68. Schule
  (ehemals Coppis"
isCrawled: true
---
Im Rahmen des Klimastreiks machen wir den Nordzubringer zur Großdemo: 

13:30Uhr Johannisplatz
14Uhr, Schillergymnasium und 68. Schule (ehemals Coppischule)
14:15Uhr Huygensplatz
14:30Uhr Werner-Heisenberg-Gymnasium
14:45Uhr Leibnizgymnasium
15Uhr Simsonplatz Großdemo 