---
id: '763182314078066'
title: Fahrrad-Demo für einen Radweg an der B2
start: '2019-05-18 14:00'
end: '2019-05-18 17:00'
locationName: null
address: 'Messeteich, Leipziger Messe'
link: 'https://www.facebook.com/events/763182314078066/'
image: 59635447_2668269699854544_6559258953346187264_n.jpg
teaser: Eine gefährliche Lücke klafft noch immer im Radnetz zwischen Krostitz-Hohenossig und Leipzig entlang der B2. Als wichtige Achse für den Berufsverkehr
recurring: null
isCrawled: true
---
Eine gefährliche Lücke klafft noch immer im Radnetz zwischen Krostitz-Hohenossig und Leipzig entlang der B2. Als wichtige Achse für den Berufsverkehr zwischen Krostitz und Leipziger Norden darf ein Radweg nicht an der Grenze der Stadt/Kommune enden, sondern soll die B2 weiter begleiten zu den wichtigen Zielen (und prinzipiell weiter bis Bad Düben). Der ADFC Leipzig organisiert dazu zusammen mit Bürgern aus Krostitz am 18. Mai eine Fahrraddemonstration.
Schon im letzten Herbst, am 30.10., gab es mit ca. 50 Bürgern aus Leipzig und Krostitz eine Raddemo an der B2. Wir wollen besonders im Jahr der Kommunalwahlen und Landtagswahlen immer wieder auf die in Sachsen fehlenden Radwege hinweisen und zeigen, dass viele Bürger gerne auf das Autofahren verzichten und radfahren, wenn die Wege nur sicher genug sind!
Bitte kommt alle per Rad zum Teich an der Leipziger Messe, von dort aus fahren wir ab 16.00 Uhr Richtung Hohenossig, wo die Zwischenkundgebung stattfindet und wir auf die TeilnehmerInnen aus Krostitz treffen. Dann fahren wir gemeinsam die Strecke zur Abfahrt Regensburger Straße der B2 für eine 2. Zwischenkundgebung und beenden die Kundgebung in Hohenossig um etwa 18.00 Uhr.
Start: 16.00 Uhr am Teich der Neue Leipziger Messe
Ende: ca. 18.00 Uhr Hohenossig, am Kindergarten

Den Flyer gibt es unter https://www.adfc-leipzig.de/sites/adfc-leipzig.de/files/flyer_demo_krostitz_18.05.2019.pdf