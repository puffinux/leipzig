---
id: '2037737876328459'
title: CM Halle im Oktober
start: '2019-10-04 18:00'
end: '2019-10-04 19:00'
locationName: critical mass Halle
address: 'August-Bebel-Platz, 06108 Halle, Saxony-Anhalt'
link: 'https://www.facebook.com/events/2037737876328459/'
image: 70395578_2552937748060794_4749304638971314176_n.jpg
teaser: '#mitdemRadwärstduschonda #fürStauhabenwirkeinezeit    Kommt wieder zahlreich, bringt gern Winkobjekte und wer hat Polnudeln mit. Bunt geschmückte Räde'
recurring: null
isCrawled: true
---
#mitdemRadwärstduschonda #fürStauhabenwirkeinezeit   
Kommt wieder zahlreich, bringt gern Winkobjekte und wer hat Polnudeln mit. Bunt geschmückte Räder sind auch immer wieder gern gesehen.  
Traditionell am ersten Freitag im Monat treffen sich viele Radfahrende unter dem Motto "ihr seid der Stau - wir sind der Verkehr" auf dem August-Bebel-Platz zur gemeinsamen Ausfahrt um die Belange von RadfahrerInnen zu signalisieren.
Lade deine Freunde, Familie, Hausbewohner mit ein, teile die Veranstaltung, damit wir mehr und noch mehr werden. Je mehr Fahrräder, desto weniger Stau; je mehr Fahrradfahrer, desto weniger Lärm in der gesamten Stadt.
Wir behindern nicht den Verkehr, wir sind der Verkehr!
Für eine fahrrad-, lauf- und lebensfreundlichere Stadt Halle an der Saale.
Verhaltensregeln bei der Critical Mass:
Die Gruppe fährt als Verband auf einer Spur auf der Fahrbahn und hält sich an die Verkehrsregeln. Alle Mitfahrenden bleiben möglichst kompakt zusammen, um dem motorisierten Verkehr nicht Gelegenheit zu geben, in Lücken hineinzufahren und die Masse in Teilgruppen zu zerreißen.
Alles bleibt friedlich und lässt sich durch aggressive Autofahrer nicht provozieren. Der Verkehr wird nicht absichtlich gestört. Es geht nicht um die Behinderung anderer, sondern darum, sich als unmotorisierte/r Verkehrsteilnehmer/in ein Stück öffentlichen Lebensraum, die Straße, zurückzuerobern.
Jede/r, der/die mitfährt, ist für sich selbst verantwortlich. Trotzdem ist es schön, aufeinander ein Auge zu haben und sich gegenseitig zu helfen. Licht im Dunkeln anschalten, versteht sich ;)!!!
Glasflaschen aus Sicherheitsgründen zu Hause lassen. Für Sachen, die mit Handy gemacht werden bitte absteigen.
Danach gibt es eine AfterMass am Bebelplatz zum Austausch. See you on the road. 2019 sommerlichblumig am Bebel =)
Mehr Info @
http://de.wikipedia.org/wiki/Critical_Mass_(Protestform)
http://www.deutschlandfunkkultur.de/200-jahre-fahrrad-erobert-das-rad-die-staedte-zurueck.976.de.html?dram%3Aarticle_id=384464
https://twitter.com/criticalmasshal