---
id: '538376366687430'
title: Gemeinsam statt einsam - im Verband in Richtung Uni
start: '2019-05-14 06:40'
end: '2019-05-14 07:10'
locationName: null
address: 'Jorge-Gomondai-Platz, 01097 Dresden'
link: 'https://www.facebook.com/events/538376366687430/'
image: 57798893_290865545158551_8327589867777163264_n.jpg
teaser: 'Ihr wollt gern gemütlich, sicher, und gemeinsam mit euren Kommiliton*innen zur Uni kommen?  Dann schließt euch dem Verband zur Uni an! Durch § 27 (1)'
recurring: null
isCrawled: true
---
Ihr wollt gern gemütlich, sicher, und gemeinsam mit euren Kommiliton*innen zur Uni kommen? 
Dann schließt euch dem Verband zur Uni an! Durch § 27 (1) der StVO ist es möglich, ab 16 Radfahrenden die sehr gut ausgebaute Kfz-Spur zu nutzen und zu zweit nebeneinander zu fahren. Lasst uns das für den Weg zur Uni nutzen, so oft es nur geht!
Die Route führt vom Jorge-Gomondai-Platz über die Carolabrücke, Pirnaischer Platz und Hauptbahnhof zum Fritz-Förster-Platz.

Wenn ihr zusätzlich von woanders starten wollt, ist das natürlich mit der entsprechenden Menschananzahl möglich, wir erstellen euch dazu auch gern eine Extra-Veranstaltung, schreibt uns einfach an. :)