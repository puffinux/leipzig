---
id: '298224604417681'
title: Kommt die Verkehrswende? Fuß- und Radverkehr in Leipzig
start: '2019-05-16 17:00'
end: '2019-05-16 19:00'
locationName: Pöge-Haus
address: 'Hedwigstraße 20, 04315 Leipzig'
link: 'https://www.facebook.com/events/298224604417681/'
image: 59252921_10157557077859172_8458625222579322880_n.jpg
teaser: 'Der ADFC Leipzig und Radio Blau, das freie Radio in Leipzig, laden zu einer Podiumsdiskussion zum Thema "Fuß- und Radverkehr - kommt die Verkehrswende'
recurring: null
isCrawled: true
---
Der ADFC Leipzig und Radio Blau, das freie Radio in Leipzig, laden zu einer Podiumsdiskussion zum Thema "Fuß- und Radverkehr - kommt die Verkehrswende?" am Donnerstag, den 16.05.2019 um 19 Uhr ins Pöge-Haus e.V. ein. Die Veranstaltung wird in Hinblick auf die Kommunalwahl am 26.05.2019 Kandidat*innen für den Leipziger Stadtrat zu ihren Ideen und Visionen für den Fuß- und Radverkehr in Leipzig befragen und mit dem Publikum diskutieren lassen.

"Verkehrspolitik entscheidet mit über Lebensqualität, Umwelt, Stadtbild, Wirtschaft und Gesundheit. Die Rolle der Zu Fuß Gehenden und Radfahrenden ist dabei von größter Bedeutung, obwohl sie als Verkehrsteilnehmende oft in zweierlei Bedeutung übersehen werden. Das wollen wir ändern und am 16. Mai nur über sie reden." meint Rosalie Kreuijer, die stellvertretende Vorsitzende des ADFC Leipzig.

Gemeinsam mit ihrem Kollegen Alexander John vom ADFC und Anja Thümmler von Radio Blau wird sie darum die Veranstaltung moderieren und Fragen zum aktuellen Fahrradklima-Test, typischen Unfallschwerpunkten und schönen Flanier-Orten aufwerfen.

Zugesagt für das Podium haben bisher:
Sven Morlok (FDP)
Julia Kneisel (SPD)
Thomas Köhler (Piraten)
Volker Holzendorf (Grüne)
Franziska Riekewald (Linke)

Alle Einwohner*innen Leipzigs sind herzlich eingeladen, der Veranstaltung beizuwohnen. Das Pöge-Haus befindet sich in der Hedwigstraße 20 am Neustädter Markt im Leipziger Osten.

Die Sendung wird live auf Radio Blau übertragen. Der Sender ist zu empfangen auf den UKW-Frequenzen 99.2 MHz (West-Süd-Ost), 94,4 MHz (Nordwest) und 89.2 MHz (Nordost). Alternativ kann der Livestream verfolgt werden, der auf der Webseite www.radioblau.de/frequenzen-stream-url/ zu finden ist.

16.05.2019, 19 Uhr
Pöge-Haus e.V., Hedwigstraße 20