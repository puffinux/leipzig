---
id: "1637658816358981"
title: Critical Mass im Dezember 2019
start: 2019-12-27 18:00
end: 2019-12-27 21:00
address: Augustusplatz, 04109 Leipzig
link: https://www.facebook.com/events/1637658816358981/
image: 78429436_865356573862439_6145322044151037952_o.jpg
teaser: Am letzten Freitag im Monat trifft sich die Critical Mass Leipzig zur
  gemeinsamen Ausfahrt.
isCrawled: true
---
Am letzten Freitag im Monat trifft sich die Critical Mass Leipzig zur gemeinsamen Ausfahrt.