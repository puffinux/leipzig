---
id: "2549310875327508"
title: Critical Mass im Januar 2020
start: 2020-01-31 18:00
end: 2020-01-31 21:00
address: Augustusplatz, 04109 Leipzig
link: https://www.facebook.com/events/2549310875327508/
image: 80541187_888685934862836_3856986923658641408_o.jpg
teaser: Am letzten Freitag im Monat trifft sich die Critical Mass Leipzig zur
  gemeinsamen Ausfahrt.
isCrawled: true
---
Am letzten Freitag im Monat trifft sich die Critical Mass Leipzig zur gemeinsamen Ausfahrt.