---
id: '667505593689430'
title: 'CM JUNI mit #Fahrraddemo'
start: '2019-06-07 18:00'
end: '2019-06-07 19:00'
locationName: critical mass Halle
address: 'August-Bebel-Platz, 06108 Halle, Saxony-Anhalt'
link: 'https://www.facebook.com/events/667505593689430/'
image: 59897495_2335193326501905_3978233781705244672_o.jpg
teaser: '#mitdemRadwärstduschonda #fürStauhabenwirkeinezeit   #Hochstrasse #fürRad      Die Critical mass macht diesmal ihre besondere Ausfahrt im Juni über di'
recurring: null
isCrawled: true
---
#mitdemRadwärstduschonda #fürStauhabenwirkeinezeit  
#Hochstrasse #fürRad     
Die Critical mass macht diesmal ihre besondere Ausfahrt im Juni über die Hochstrasse in Halle.
Traditionell am ersten Freitag im Monat treffen sich viele Radfahrende unter dem Motto "ihr seid der Stau - wir sind der Verkehr" auf dem August-Bebel-Platz zur gemeinsamen Ausfahrt um die Belange von RadfahrerInnen zu signalisieren.
Lade deine Freunde mit ein, teile die Veranstaltung, damit wir mehr und mehr werden. Je mehr Fahrräder, desto weniger Stau; je mehr Fahrradfahrer, desto weniger Lärm in der gesamten Stadt.
Wir behindern nicht den Verkehr, wir sind der Verkehr.
Für eine fahrrad-, lauf- und lebensfreundlichere Stadt Halle an der Saale.
Verhaltensregeln bei der Critical Mass:
Die Gruppe fährt als Verband auf einer Spur auf der Fahrbahn und hält sich an die Verkehrsregeln. Alle Mitfahrenden bleiben möglichst kompakt zusammen, um dem motorisierten Verkehr nicht Gelegenheit zu geben, in Lücken hineinzufahren und die Masse in Teilgruppen zu zerreißen.
Alles bleibt friedlich und lässt sich durch aggressive Autofahrer nicht provozieren. Der Verkehr wird nicht absichtlich gestört. Es geht nicht um die Behinderung anderer, sondern darum, sich als unmotorisierte/r Verkehrsteilnehmer/in ein Stück öffentlichen Lebensraum, die Straße, zurückzuerobern.
Jede/r, der/die mitfährt, ist für sich selbst verantwortlich. Trotzdem ist es schön, aufeinander ein Auge zu haben und sich gegenseitig zu helfen. Licht im Dunkeln anschalten, versteht sich ;)!!!
Danach gibt es eine AfterMass am Bebelplatz zum Austausch. See you on the road. 2019 frühlingshaft am Bebel =)
Mehr Info @
http://de.wikipedia.org/wiki/Critical_Mass_(Protestform)
http://www.deutschlandfunkkultur.de/200-jahre-fahrrad-erobert-das-rad-die-staedte-zurueck.976.de.html?dram%3Aarticle_id=384464
https://twitter.com/criticalmasshal