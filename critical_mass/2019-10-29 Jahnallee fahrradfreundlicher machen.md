---
id: "456093258377333"
title: Jahnallee fahrradfreundlicher machen
start: 2019-10-29 16:00
end: 2019-10-29 19:00
address: Jahnallee/AOK
link: https://www.facebook.com/events/456093258377333/
image: 72631707_2993281920686652_6465297653163884544_n.jpg
teaser: SAVE THE DATE --- SAVE THE DATE --- SAVE THE DATE   Am Dienstag 29.10 ruft der
  ADFC Leipzig e.V., der Ökolöwe - Umweltbund Leipzig e.V. und der BUND L
isCrawled: true
---
SAVE THE DATE --- SAVE THE DATE --- SAVE THE DATE 

Am Dienstag 29.10 ruft der ADFC Leipzig e.V., der Ökolöwe - Umweltbund Leipzig e.V. und der BUND Leipzig zu einer Raddemo durch die Jahnallee und um den Ring und dem Motto „#Jahnallee fahrradfreundlicher machen/#fürmehrPlatzfürRad“ auf. 

Diese startet 17Uhr vor der AOK fährt durch die Jahnallee zum Ring, umkurvt diesen im Uhrzeigersinn und wird zurück durch die Jahnallee fahren um auf dem Waldplatz zu enden. 

Gleichzeitig wird in der Jahnallee ab 16Uhr beidseitig eine protected biklane gestellt.

SAVE THE DATE --- SAVE THE DATE --- SAVE THE DATE 

Kommt vorbei!