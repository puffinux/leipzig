---
id: '2194532940839005'
title: Freiheit braucht Vertrauen - Fahrrad-Demo
start: '2019-10-07 18:00'
end: '2019-10-07 21:00'
locationName: null
address: 'Augustusplatz, 04109 Leipzig'
link: 'https://www.facebook.com/events/2194532940839005/'
image: 72132641_2427716074117944_4475927509564653568_n.jpg
teaser: 'Am 7. Oktober 1989 feierte die DDR ihren 40. Geburtstag. Damit es während der Feierlichkeiten nicht zu Störungen kam, wurden Oppositionelle zugeführt'
recurring: null
isCrawled: true
---
Am 7. Oktober 1989 feierte die DDR ihren 40. Geburtstag. Damit es
während der Feierlichkeiten nicht zu Störungen kam, wurden
Oppositionelle zugeführt (eingesperrt) Dennoch kam es u.a. in Berlin, Leipzig, Dresden, Plauen, Jena, Magdeburg, Ilmenau, Arnstadt, Karl-Marx-Stadt und Potsdam zu Demonstrationen, die überall gewaltsam beendet wurden. Mehr als 1.000 Menschen wurden ohne Rechtsgrundlage verhaftet und ohne Anklage eingesperrt.

Auch am 7. Oktober 1989 wurde in Schwante bei Berlin die
Sozialdemokratische Partei in der DDR gegründet.

Mit dem neuen sächsischen Polizeigesetz, welches am 1. Januar 2020 in Kraft tritt, zeigt die sächsische Staatsregierung unter Beteiligung der SPD, dass auch sie 30 Jahre nach der *politischen Wende* wenig Vertrauen in ihre Bevölkerung hat. Erkämpfte Grundrechte wie das auf informationelle Selbstbestimmung, Versammlungsfreiheit und Freizügigkeit, werden mit dem neuen sächsischen Polizeigesetz empfindlich beschnitten. So wird die Videoüberwachung ausgeweitet, die Überwachung der Telekommunikation wird zukünftig bereits "präventiv"
zulässig, durch die Polizei können Kontaktverbote sowie
Aufenthaltsverbote ausgesprochen werden. Schliesslich sollen
Sondereinheiten der Polizei, die in Sachsen auch mal bei Demonstrationen zum Einsatz kommen, militärisch aufgerüstet werden. Das neue Polizeigesetz höhlt die rechtsstaatlich verbriefte Unschuldsvermutung aus und macht uns alle zu potentiellen Gefährder*innen. 
Wir wollen den 7. Oktober zum Anlass nehmen, um die, die am 9. Oktober den demokratischen Aufbruch der Wendezeit bejubeln und würdigen werden, an die Ursprünge der Aufbruchsbewegung zu erinnern. Es ging und geht um Freiheit und um die Zurückdrängung bzw. Verhinderung von Autoritarismus.


