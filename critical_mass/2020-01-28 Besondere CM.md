---
id: "461650991201770"
title: Besondere CM
start: 2020-01-28 16:00
end: 2020-01-28 18:00
address: August-Bebel-Platz, 06108 Halle
link: https://www.facebook.com/events/461650991201770/
image: 79886585_2770862192935014_2592441598440636416_o.jpg
teaser: Außerhalb des regelmäßigen Rhythmus wird es am 28. Januar eine ungewöhnliche
  CM geben. Weitersagen und Lauscher aufsperren!
isCrawled: true
---
Außerhalb des regelmäßigen Rhythmus wird es am 28. Januar eine ungewöhnliche CM geben. Weitersagen und Lauscher aufsperren!