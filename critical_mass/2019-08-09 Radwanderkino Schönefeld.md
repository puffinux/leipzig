---
id: '1254236641420410'
title: Radwanderkino Schönefeld
start: '2019-08-09 20:30'
end: '2019-08-09 23:30'
locationName: Schloss Schönefeld e.V.
address: 'Zeumerstraße 1, 04347 Leipzig'
link: 'https://www.facebook.com/events/1254236641420410/'
image: 67402487_2808815099133336_7353450061948256256_n.jpg
teaser: Gemütlich Rad fahren und gelegentlich einen Kurzfilm sehen.
recurring: null
isCrawled: true
---
Gemütlich Rad fahren und gelegentlich einen Kurzfilm sehen.