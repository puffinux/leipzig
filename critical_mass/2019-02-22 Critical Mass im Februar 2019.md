---
id: '607213503053028'
title: Critical Mass im Februar 2019
start: '2019-02-22 18:00'
end: '2019-02-22 21:00'
locationName: null
address: 'Augustusplatz, 04109 Leipzig'
link: 'https://www.facebook.com/events/607213503053028/'
image: 51439078_668598036871628_3842179031768236032_n.jpg
teaser: Am letzten Freitag im Monat trifft sich die Critical Mass (eine Art Fahrrad-Flashmob) zur gemeinsamen Ausfahrt.
recurring: null
isCrawled: true
---
Am letzten Freitag im Monat trifft sich die Critical Mass (eine Art Fahrrad-Flashmob) zur gemeinsamen Ausfahrt.