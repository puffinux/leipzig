---
id: 3489-1554548400-1554566400
title: Garteneinsatz
start: '2019-04-06 11:00'
end: '2019-04-06 16:00'
locationName: null
address: 'Neustädter Str. 20, 04315 Leipzig'
link: 'http://www.querbeet-leipzig.de/event/fruehjahrsputz/'
image: null
teaser: |-
  Auf in die neue Saison: Unser erster Gartenaufräumtag lockt ins Grüne.
  Wir freuen uns über viele anp
recurring: null
isCrawled: true
---
Auf in die neue Saison: Unser erster Gartenaufräumtag lockt ins Grüne.

Wir freuen uns über viele anpackende, räumende und buddelnde Hände von Großen und Kleinen. Und besonders über neue Menschen, die interessiert sind, den Garten und die Gemeinschaft kennenzulernen. 

