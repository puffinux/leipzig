---
id: 3494-1555146000-1555160400
title: Frühjahrsputz
start: '2019-04-13 09:00'
end: '2019-04-13 13:00'
locationName: null
address: null
link: 'http://www.querbeet-leipzig.de/event/fruehjahrsputz-2/'
image: null
teaser: " \nDas Querbeet rückt aus… Wie jedes Jahr beteiligen wir uns an der\_ lobenswerten Aktion des Bürgerve"
recurring: null
isCrawled: true
---
 

Das Querbeet rückt aus… Wie jedes Jahr beteiligen wir uns an der  lobenswerten Aktion des Bürgervereins Neustädter Markt e.V., das Viertel zu säubern. Wir freuen uns auf ein angenehmes work out bei muskelfreundlichen Temperaturen. Und über Eure Unterstützung 🙂 

