---
id: 3738-1569682800-1569708000
title: Herbstfest
start: '2019-09-28 15:00'
end: '2019-09-28 22:00'
locationName: null
address: 'Neustädter Str. 20, 04315 Leipzig'
link: 'http://www.querbeet-leipzig.de/event/herbstfest/'
image: null
teaser: Wir danken Mutter Erde und allen ihren Abkömmlingen für den reichen Erntesegen und Ihnen/Euch für di
recurring: null
isCrawled: true
---
Wir danken Mutter Erde und allen ihren Abkömmlingen für den reichen Erntesegen und Ihnen/Euch für die ihr angediehene Hilfe beim Gießen, Graben, Mulchen, Beobachten und Jäten. Als letztes Fest auf dieser Fläche wird es ein buntes sein! Dazu gehört natürlich (mindestens!) ein Workshop, Kinderquatsch, Gartenprodukte, viel Musik und Speisen. 

Kommet vorbey, seid herzlichst eingeladen! 

