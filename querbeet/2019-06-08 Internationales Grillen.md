---
id: 3607-1559995200-1559995200
title: Internationales Grillen
start: '2019-06-08 12:00'
end: '2019-06-08 12:00'
locationName: null
address: 'Neustädter Str. 20, 04315 Leipzig'
link: 'http://www.querbeet-leipzig.de/event/internationales-grillen/'
image: null
teaser: Was verbindet wie nichts anderes den Kopf mit dem Herzen? Der Bauch! Darum veranstalten wir in Koope
recurring: null
isCrawled: true
---
Was verbindet wie nichts anderes den Kopf mit dem Herzen? Der Bauch! Darum veranstalten wir in Kooperation mit der Mühlstraße 14 e.V. ein internationales Grillen. Gäste können auch eigenes veganes, vegetarisches, tierisches Grillgut mitbringen. Wir freuen uns auf Dich, Sie und Euch bei Gesprächen über den Tellerrand.  

