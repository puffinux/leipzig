---
id: 3548-1558796400-1558818000
title: Demo gegen Flächenfraß und Verdrängung
start: '2019-05-25 15:00'
end: '2019-05-25 21:00'
locationName: null
address: null
link: 'http://www.querbeet-leipzig.de/event/demo-gegen-flaechenfrass-und-verdraengung/'
image: null
teaser: 'Demo des Querbeet, unterstützt von den Gemeinschaftsgärten Leipzigs. Wir werden bereits zum zweiten '
recurring: null
isCrawled: true
---
Demo des Querbeet, unterstützt von den Gemeinschaftsgärten Leipzigs. Wir werden bereits zum zweiten Mal unsere Fläche verlieren und demonstrieren für urbane Freiräume im Leipziger Osten. 

Details folgen in Kürze! 

