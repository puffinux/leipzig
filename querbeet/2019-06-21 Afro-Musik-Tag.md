---
id: 3644-1561129200-1561149000
title: Afro-Musik-Tag
start: '2019-06-21 15:00'
end: '2019-06-21 20:30'
locationName: null
address: null
link: 'http://www.querbeet-leipzig.de/event/afro-musik-tag/'
image: null
teaser: "Ein\_Afro-Musik-Tag zwischen Zweigen und Beeren? Aber ja! Schwing‘ die Hüften und lass‘ die Haare fla"
recurring: null
isCrawled: true
---
Ein Afro-Musik-Tag zwischen Zweigen und Beeren? Aber ja! Schwing‘ die Hüften und lass‘ die Haare flattern – wir feiern die Lebensfreude mit Euch! Details folgen in Kürze… 

