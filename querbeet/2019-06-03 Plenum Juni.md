---
id: 3544-1559590200-1559597400
title: Plenum Juni
start: '2019-06-03 19:30'
end: '2019-06-03 21:30'
locationName: null
address: 'Neustädter Str. 20, 04315 Leipzig'
link: 'http://www.querbeet-leipzig.de/event/plenum-juni/'
image: null
teaser: 'Wir plenieren ordentlich und laden interessierte Mitmenschen dazu ein! Alle, die uns gern kennen ler'
recurring: null
isCrawled: true
---
Wir plenieren ordentlich und laden interessierte Mitmenschen dazu ein! Alle, die uns gern kennen lernen möchten, mitpflanzen wollen oder uns bei den kommenden Aktivitäten und Veranstaltungen unterstützen möchten, mögen sich bitte ganz herzlich angesprochen fühlen. Wir freuen uns drauf! 

