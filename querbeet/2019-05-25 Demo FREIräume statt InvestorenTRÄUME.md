---
id: 3548-1558796400-1558807200
title: Demo FREIräume statt InvestorenTRÄUME
start: '2019-05-25 15:00'
end: '2019-05-25 18:00'
locationName: null
address: null
link: 'http://www.querbeet-leipzig.de/event/demo-gegen-flaechenfrass-und-verdraengung/'
image: null
teaser: Urbane Gärten sind wichtige Bausteine in einer lebenswerten Stadt. Zu oft müssen sie unter dem Druck
recurring: null
isCrawled: true
---
Urbane Gärten sind wichtige Bausteine in einer lebenswerten Stadt. Zu oft müssen sie unter dem Druck des Baubooms weichen. Auch in Leipzig wurden viele Gärten Opfer dieser Entwicklung, wie zum Beispiel VagaBUND in Connewitz oder die Nachbarschaftsgärten in Lindenau. Bereits 2013 musste der Offene Garten Querbeet seine erste Fläche verlassen und in letzter Minute umsiedeln.







Aktuell steht Querbeet im Leipziger Osten ein zweites Mal vor dem Aus. Wir, das Netzwerk Leipziger Gemeinschaftsgärten, fordern die Leipziger Politik und Stadtplanung auf, die Bedeutung von urbanen Gärten anzuerkennen und ihre Position zu stärken.







Das heißt:



den Bewohner*innen Gestaltungsrecht einzuräumen und Gestaltungsfreiräume für zivilgesellschaftliches Engagement zu schaffen,

urbane Gärten im Bau- und Planungsrecht zu verankern und bei neuen Stadtentwicklungsprojekten zu integrieren,

bestehende urbane Gärten als Teil der grünen Infrastruktur zu schützen und geeignete Flächen zu bevorraten sowie

die Belange von Mensch und Stadtnatur zu berücksichtigen, indem Grün- und Freiflächen qualitätsvoll und partizipativ gestaltet werden!



Bitte schließt Euch an und demonstriert mit uns gegen Flächenfraß und dem damit verbundenen Verlust an Freiräumen. Für den Erhalt grüner Orte, für Freiräume, für Gemeinschaftsgärten, für ein solidarisches Miteinander! Nur gemeinsam können wir unsere Stadt gestalten.





Start: 15.30 Uhr Otto-Runki-Platz | Haltestelle Einertstraße



