---
id: 3514-1558191600-1558216800
title: Pflanzfest
start: '2019-05-18 15:00'
end: '2019-05-18 22:00'
locationName: null
address: 'Neustädter Str. 20, 04315 Leipzig'
link: 'http://www.querbeet-leipzig.de/event/pflanzfest-2/'
image: null
teaser: 'Unser Saisonauftakt in der letzten Gartensaison auf unserer Fläche Neustädter Straße: Wie all die Ja'
recurring: null
isCrawled: true
---
Unser Saisonauftakt in der letzten Gartensaison auf unserer Fläche Neustädter Straße: Wie all die Jahre könnt Ihr Jungpflanzen erwerben, Kinderaktivitäten erleben, Musik und Feuer genießen. Kommt, tanzt mit uns in der Mai-Sonne, lasst uns gemeinsam schwelgen in den Erinnerungen der vergangenen Jahre (und in den dargebotenen Speisen und Getränken) sowie neue Pläne schmieden. Bastelt mit uns Schilder und Plakate für die Demo für Freiräume. Wir freuen uns auf Eure Anwesenheit… 

  

