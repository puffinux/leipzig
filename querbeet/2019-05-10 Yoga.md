---
id: 3540-1557509400-1557513000
title: Yoga
start: '2019-05-10 17:30'
end: '2019-05-10 18:30'
locationName: null
address: 'Neustädter Str. 20, 04315 Leipzig'
link: 'http://www.querbeet-leipzig.de/event/yoga-2/'
image: null
teaser: |-
  Mit den Elementen verbunden…  
  Möchten Du, Sie und Ihr gerne Yoga im Grünen erleben? Dann kommt mit 
recurring: null
isCrawled: true
---
Mit den Elementen verbunden…  

Möchten Du, Sie und Ihr gerne Yoga im Grünen erleben? Dann kommt mit Eurer Matte bzw. Handtuch in unseren Garten. Die Stunde mit Gabriela ist für jedermensch geeignet – auch ohne Vorkenntnisse. 

Kurs auf Spendenbasis. 

  

