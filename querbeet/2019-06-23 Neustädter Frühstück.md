---
id: 3641-1561285800-1561298400
title: Neustädter Frühstück
start: '2019-06-23 10:30'
end: '2019-06-23 14:00'
locationName: null
address: null
link: 'http://www.querbeet-leipzig.de/event/neustaedter-fruehstueck-3/'
image: null
teaser: 'Bei einem kleinen Frühstück auf dem Neustädter Markt heißen wir Euch willkommen und freuen uns über '
recurring: null
isCrawled: true
---
Bei einem kleinen Frühstück auf dem Neustädter Markt heißen wir Euch willkommen und freuen uns über gärtnerischen Austausch, informieren über unseren Verein und die Möglichkeiten mitzumachen. 

