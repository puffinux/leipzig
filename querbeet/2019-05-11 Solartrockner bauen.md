---
id: 3510-1557561600-1557594000
title: Solartrockner bauen
start: '2019-05-11 08:00'
end: '2019-05-11 17:00'
locationName: null
address: null
link: 'http://www.querbeet-leipzig.de/event/solartrockner-bauen/'
image: null
teaser: |-
  Workshop! Trocknen mit der Sonne 
  Wir möchten die Kraft der Sonne nutzen, um unsere Ernte sowie eige
recurring: null
isCrawled: true
---
Workshop! Trocknen mit der Sonne 

Wir möchten die Kraft der Sonne nutzen, um unsere Ernte sowie eigenes Saatgut zu konservieren. Da der solare Tunneltrockner als Gemeingut behandelt wird, richtet sich der Workshop zuallererst an Mitglieder von Gemeinschaftsgärten. Weitere interessierte Mitmenschen können gerne teilnehmen, wenn Restplätze verfügbar sind. 

Bite anmelden unter: info@querbeet-leipzig. de. Auf diesen Sommer! 

