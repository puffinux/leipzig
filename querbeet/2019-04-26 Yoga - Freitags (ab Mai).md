---
id: 3506-1556299800-1556303400
title: Yoga - Freitags (ab Mai)
start: '2019-04-26 17:30'
end: '2019-04-26 18:30'
locationName: null
address: null
link: 'http://www.querbeet-leipzig.de/event/yoga-freitags-ab-mai/'
image: null
teaser: |-
  Mit den Elementen verbunden…  
  Möchten Du, Sie und Ihr gerne Yoga im Grünen erleben? Dann kommt mit 
recurring: null
isCrawled: true
---
Mit den Elementen verbunden…  

Möchten Du, Sie und Ihr gerne Yoga im Grünen erleben? Dann kommt mit Eurer Matte bzw. Handtuch in unseren Garten. Die Stunde mit Gabriela ist für jedermensch geeignet – auch ohne Vorkenntnisse. 

Kurs auf Spendenbasis. 

  

