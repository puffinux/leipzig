---
id: 3407-1543865400-1543872600
title: Plenum
start: '2018-12-03 19:30'
end: '2018-12-03 21:30'
locationName: null
address: 'Neustädter Str. 20, 04315 Leipzig'
link: 'http://www.querbeet-leipzig.de/event/plenum/'
image: null
teaser: 'Wir plenieren ordentlich und laden interessierte Mitmenschen dazu ein! Alle, die uns gern kennen ler'
recurring: null
isCrawled: true
---
Wir plenieren ordentlich und laden interessierte Mitmenschen dazu ein! Alle, die uns gern kennen lernen möchten, mitpflanzen wollen oder uns bei den kommenden Aktivitäten und Veranstaltungen unterstützen möchten, mögen sich bitte ganz herzlich angesprochen fühlen. Wir freuen uns drauf! 

