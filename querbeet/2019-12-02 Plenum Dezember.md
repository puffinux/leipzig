---
id: 3544-1575315000-1575322200
title: Plenum Dezember
start: 2019-12-02 19:30
end: 2019-12-02 21:30
address: Neustädter Str. 20, 04315 Leipzig
link: http://www.querbeet-leipzig.de/event/plenum-juni/
teaser: Wir plenieren ordentlich und laden interessierte Mitmenschen dazu ein! Alle,
  die uns gern kennen ler
isCrawled: true
---
Wir plenieren ordentlich und laden interessierte Mitmenschen dazu ein! Alle, die uns gern kennen lernen möchten, mitpflanzen wollen oder uns bei den kommenden Aktivitäten und Veranstaltungen unterstützen möchten, mögen sich bitte ganz herzlich angesprochen fühlen. Schreibt uns eine Anmelde-Mail, um den Ort zu erfahren – wir freuen uns drauf! 

