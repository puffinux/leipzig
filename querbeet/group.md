---
name: Initiative Querbeet
website: http://www.querbeet-leipzig.de/
email: info@querbeet-leipzig.de
scrape:
    source: ical
    options:
        url: http://www.querbeet-leipzig.de/events/?ical=1
---
Der offene Gemeinschaftsgarten hat sich aus einer jungen Initiative heraus entwickelt und steht in  in Leipzig für urban gardening. Seit 2012 bewirtschaftet die Gemeinschaft Brachflächen. Angefangen zu Gärtnern und soziokulturelle Angeboten zu schaffen, haben wir auf einer Fläche in der Hermann-Liebmann-Straße. Derzeit betreiben wird zwei Gärten im...