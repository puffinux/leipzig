---
id: 3609-1560610800-1560625200
title: Kleidertausch
start: '2019-06-15 15:00'
end: '2019-06-15 19:00'
locationName: null
address: 'Neustädter Str. 20, 04315 Leipzig'
link: 'http://www.querbeet-leipzig.de/event/kleidertausch/'
image: null
teaser: Ihr braucht eine frische Brise in eurer Sommergarderobe? Dann ladet euch herzlich gerne zu unserem K
recurring: null
isCrawled: true
---
Ihr braucht eine frische Brise in eurer Sommergarderobe? Dann ladet euch herzlich gerne zu unserem Kleidertausch ein! Bringt einfach mit, wessen ihr überdrüssig seid und findet neue Lieblingsteile. Dazu findet ihr mit aller Sicherheit Kaffee, Gartentee, (auch vegane) Kuchen und nette GesprächspartnerInnen! 

SONY DSC 

