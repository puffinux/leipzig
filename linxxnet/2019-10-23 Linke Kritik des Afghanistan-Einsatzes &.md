---
id: "472680226906606"
title: Linke Kritik des Afghanistan-Einsatzes & der linken Kritik daran
start: 2019-10-23 19:00
end: 2019-10-23 22:00
locationName: linXXnet
address: Brandstr. 15, 04277 Leipzig-Connewitz, Sachsen
link: https://www.facebook.com/events/472680226906606/
image: 69683499_10156828810284620_6641365465186697216_n.jpg
teaser: Seit 2001 befindet sich Deutschland im Krieg. Dennoch ist hierzulande wenig
  bekannt über den Konflikt und das Land, in dem er geführt wird. Stattdesse
isCrawled: true
---
Seit 2001 befindet sich Deutschland im Krieg. Dennoch ist hierzulande wenig bekannt über den Konflikt und das Land, in dem er geführt wird. Stattdessen folgt die öffentliche Debatte über Afghanistan eher den ideologischen oder geostrategischen Bedürfnissen der jeweiligen Kommentator*innen – sei das Antiimperialismus oder Bündnistreue. Selten geht es darum, wie ein menschengerechter Frieden in Afghanistan nicht etwa für die Deutschen, sondern für die Afghan*innen aussehen könnte. Das gilt leider auch, wenn von links der Abzug der Bundeswehr gefordert wird, ohne zu erklären, wie es danach weitergehen soll. Die Umsetzung dieser Forderung wird nun ausgerechnet von US-Präsident Trump am Verhandlungstisch mit den Taliban ausgehandelt, also keineswegs zwischen ausgemachten Menschenfreunden.

Dabei ist der Afghanistankrieg vor Allem ein afghanischer Bürgerkrieg. Statt Afghanistan und seine Einwohner*innen nur als Spielball geopolitischer Interessen zu betrachten, versucht der Vortrag daher, die Ursprünge des Konfliktes in Afghanistan, die afghanischen Konfliktparteien und ihre ideologischen Hintergründe zu beleuchten. Erst danach sollen die westliche Intervention und ihre Folgen behandelt werden. Der Vortrag reißt außerdem den (völker)rechtlichen Rahmen bewaffneter Konflikte im Allgemeinen und von Kriegseinsätzen der Bundeswehr im Speziellen an, sowie eine Einordnung des Afghanistaneinsatzes in andere Auslandseinsätze der Bundeswehr. Dabei soll auch die gängige linke Kritik auf ihre Substanz geprüft werden.

Es handelt sich um einen politischen, keinen wissenschaftlichen Vortrag. Als Referenten dürfen wir Michael Waßmann begrüßen.