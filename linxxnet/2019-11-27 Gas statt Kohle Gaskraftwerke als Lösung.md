---
id: "1774255972707831"
title: Gas statt Kohle? Gaskraftwerke als Lösung für die Wärmewende?
start: 2020-01-15 19:00
end: 2020-01-15 21:30
locationName: Bowling Regenbogen
address: Arno-Nitzsche-Straße 43, 04277 Leipzig
link: https://www.facebook.com/events/1774255972707831/
image: 74333480_1925094330927108_4468737880340037632_n.jpg
teaser: Die Stadt Leipzig plant den Ausstieg aus der Braunkohle. Der
  Versorgungsvertrag mit dem Kraftwerk Lippendorf, durch den die Stadt bisher
  mit Fernwärme
isCrawled: true
---
Die Stadt Leipzig plant den Ausstieg aus der Braunkohle. Der Versorgungsvertrag mit dem Kraftwerk Lippendorf, durch den die Stadt bisher mit Fernwärme versorgt wird, soll zum Jahr 2023 auslaufen. Dies besiegelte der Aufsichtsrat der Stadtwerke in seiner Sitzung im Juni 2019. 
Das größte Projekt für den Leipziger Braunkohle-Ausstieg bei Fernwärme ist ein Gasturbinen-Heizkraftwerk am SWL-Standort Bornaische Straße, das über eine Wärmeleistung von 150 Megawatt und eine Stromleistung von 120 MW verfügen soll. Außerdem sind mehrere kleinere Projekte für Biomasse und Erdgas geplant.  Diese Weichenstellungen sorgen für Verunsicherung und Ablehnung von Anwohner*innen. Auch aus ökologischer Perspektive stellen sich zahlreiche Fragen, sind doch durch diese alternativen Energiequellen ebenfalls Emissionsbelastungen zu erwarten. Nicht aus dem Blick fallen dürfen zudem etwaige Preiserhöhungen für die Energieversorgung. 

Wir wollen das Thema einer sozialen, ökologischen und von den Menschen akzeptierten Energie- und Wärmewende mit Vertreter*innen der Stadtwerke, mit Felix Pohl (UFZ  Helmholtz-Zentrum für Umweltforschung GmbH), Marco Böhme, MdL DIE LINKE und möglichst vielen Beteiligten und vor Ort Betroffenen aufgreifen und am 27. November öffentlich zur Diskussion stellen. 