---
id: "546886972532859"
title: Gutscheine, Isolation, Abschiebung - Merseburg, was los? pt2
start: 2019-12-16 19:00
end: 2019-12-16 21:00
locationName: linXXnet
address: Brandstr. 15, 04277 Leipzig-Connewitz, Sachsen
link: https://www.facebook.com/events/546886972532859/
image: 78576059_646352059104299_1472316580070162432_o.jpg
teaser: Gutscheine, Isolation, Abschiebungen - Merseburg, ein Ort aus dem die Menschen
  fliehen…  Eine Informationsveranstaltungen zum solidarischen Tausch, wa
isCrawled: true
---
Gutscheine, Isolation, Abschiebungen - Merseburg, ein Ort aus dem die Menschen fliehen…

Eine Informationsveranstaltungen zum solidarischen Tausch, warum dieser nötig ist und über das zugrundeliegende Problem – struktureller Rassismus, deutsche Bürokratie und deren Täter*innen.

Im südlichen Sachsen-Anhalt, im Saalekreis, liegt diese kleine Kreisstadt Merseburg, irgendwo zwischen Leipzig und (etwas näher an) Halle (Saale). Mit Dom, Hochschule und Asylzentrum. Die beiden erst genannten sollen Menschen in die Stadt locken. Letzteres ist dazu da, Menschen aus der Stadt zu treiben, abzuschieben oder bis dahin zu drangsalieren.

Bekannt wurde Merseburg in den letzten Jahren vor allem durch rassistische Übergriffe, ausgeübt von Faschisten und Neonazis auf Menschen die nicht ins deutsches Weltbild passen. Mitunter die gleichen Menschen, die auch das Asylzentrum unter Führung von Jan Rosenstein als das Problem im Saalekreis in Angriff nimmt. 

Dem Gros der hiesigen Gesellschaft stört beides nicht. Die meisten Bürger*innen schweigen, wenn Menschen angegriffen und verfolgt werden, ja sogar, wenn ihnen wie im Herbst 2015 in den Kopf geschossen wird. Aber auch dazu, dass hier seit Jahren, bundesweit, mit eine der restriktivsten Asylpolitiken in die Tat umgesetzt wird. 

Ob nun die schnell wieder abgeänderten sog. „Flüchtlingsintegrationsmasznahmen“, das mit Millionen an Steuergeldern finanzierte Lager im 13km entfernten Krumpa (in dem Menschen auf 5qm über Jahre leben müssen), oder der Kampf um die Spitze an Abschiebungen. Bei regulären Terminen im besagten Asylzentrum, in Zivil auflauernd oder ohne moralische Umstände werden auch Familien getrennt. Um einige wenige Punkte zu nennen, die sich permanent verschlimmernden Asylgesetze finden hier stets die menschenfeindlichste Umsetzung. 

Seit 2 Jahren versucht Jan Rosenstein sein Ziel, die Menschen in Duldung soweit es geht zu vertreiben/ abzuschieben, nun mittels Sanktionen und Masznahmen in der „Sozial“leistung zu erreichen. Lebensmittelgutscheine statt Bargeld sollen neben Arbeitsverboten und permanenter Androhung der Abschiebung und folgender Einreiseverbote den Menschen das Leben so schwer wie möglich machen. Damit sie gehen, oder „besser“ noch, an ihrer eigenen Abschiebung mitwirken – Stichwort: Mitwirkungspflicht!

Doch nicht alle Menschen schauen dieser rassistischen Kackscheisze tatenlos zu. Die betroffenen Menschen zeigen Stärke und Durchhaltevermögen, und brauchen desto mehr solidarische Unterstützung!

Eine Idee ist der Tausch der Gutscheine in Bargeld, darum soll es bei den Informationsveranstaltungen gehen!
Das Café Internationale und die Tauschgruppe stellen sich vor, bieten einen tieferen Einblick zum Support in der Provinz, und erläutern wie der Tausch, auch von Leipzig aus organisiert und umgesetzt wird. 

Seid dabei! Bringt Geld zum Tauschen mit! 
Solidarität statt Sanktionen!

Bei Fragen oder Hinweisen, gerne hier oder via Mail an cafeinternationale [ät] posteo [punkt] de

Für Übersetzung bitte rechtzeitig Bescheid geben, dann versuchen wir es einzurichten. 
