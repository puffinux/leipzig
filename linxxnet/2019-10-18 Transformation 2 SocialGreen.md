---
id: "405176640378754"
title: Transformation 2 SocialGreen
start: 2019-10-18 19:00
end: 2019-10-19 18:30
locationName: Interim
address: Demmeringstraße 32, 04177 Leipzig
link: https://www.facebook.com/events/405176640378754/
image: 71014851_2417153415037104_4389646899397787648_n.jpg
teaser: "Ökonomiekonferenz:  18.-19.10.2019: Transformation2SocialGreen:
  Herausforderungen, Pfade und Gestaltung sozialökologischer
  Transformationen  In der zw"
isCrawled: true
---
Ökonomiekonferenz:

18.-19.10.2019: Transformation2SocialGreen: Herausforderungen, Pfade und Gestaltung sozialökologischer Transformationen

In der zweitägigen Konferenz sollen Herausforderungen einer sozialökologischen Ökonomie diskutiert sowie praxisorientierte Lösungsansätze für widerstandsfähige, nachhaltige Transformationsstrategien erarbeitet werden. Die Konferenz wird am Freitagabend mit einer Podiumsdiskussion zum Thema: Zeit Umzudenken! – Die Plurale Ökonomie als Türöffner für die sozialökologische Transformation?,  eröffnet. Am Samstag finden vier parallel stattfindende ganztägige Workshops statt, welche in eine gemeinsame Abschlussdiskussion münden, in denen die in den Workshops erarbeiteten Strategien und Handlungsansätze vorgestellt werden. Es handelt sich um eine Veranstaltung des Roten Baum e.V. in Kooperation mit linXXnet und der Rosa-Luxemburg-Stiftung Sachsen. Die Teilnahme an der Konferenz ist kostenfrei. Eine Spende für die bereit gestellte Verpflegung ist gern willkommen. Um eine Anmeldung zur Teilnahme an mariusewert@googlemail.com wird gebeten.

Zeit: 18.10.2019, 19:00 - 20:30 Uhr (Podiumsdiskussion: Zeit Umzudenken! – Die Plurale Ökonomie als Türöffner für die sozialökologische Transformation?), 19.10.2019 10:00 - 18:30 Uhr (Workshops)

Ort: Projekte- und Abgeordnetenbüro INTERIM by linXXnet, Demmeringstraße 32, Leipzig

Ablauf:

Freitag, 18.10.2019, 19.00 Uhr		
Zeit umzudenken! – Die Plurale Ökonomie als Türöffner für die sozialökologische Transformation? Podiumsdiskussion mit Dr. Karin Schönpflug (Universität Wien), Luise Neuhaus-Wartenberg ( angefr.), Dr. Martin Quaas (Universität Leipzig, angefr.), Marius Ewert (Initiative Plurale Ökonomie Halle/kritische Ökonom*innen Siegen). 

Ort: Projekte- und Abgeordnetenbüro INTERIM by linXXnet, Demmeringstraße 32, Leipzig 

Samstag, 19.10.2019, 9:30 - 10:00 Uhr 	
Ankunft der Teilnehmenden, Registrierung, Begrüßungskaffee; Ort: Projekte- und Abgeordnetenbüro INTERIM by linXXnet 

10:15 – 10.30 Uhr 	
Begrüßung (Marius Ewert, linXXnet e.V.); Ort: Projekte- und Abgeordnetenbüro INTERIM by linXXnet 

10.45 – 11.30 Uhr 	
Notwendigkeit und politische Herausforderungen für sozialökologische Transformationen im Kontext des Neosozialismus (Dr. Klaus Dörre, Friedrich-Schiller-Universität Jena); Ort: Projekte- und Abgeordnetenbüro INTERIM by linXXnet 

11:45 - 13:45 Uhr 	
Workshopphase I: Herausforderungen, Pfade und Gestaltungsansätze in aktuellen Transformationsprozessen 
    • Feministische und postkoloniale Reflexion auf sozialökologische Transformationen (Dr. Karin Schönpflug, Universität Wien); Ort: Rosalinde Leipzig e.V. (Dr. Karin Schönpflug, Universität Wien); Ort: Rosalinde Leipzig e.V.
    • Postwachstum (Klaus Dörre, Friedrich-Schiller-Universität Jena); Ort: Projekte- und Abgeordnetenbüro INTERIM by linXXnet 
    • Euro, Wert und Kilojoule. Privateigentum als Öko-Killer (Dr. Alexander B. Voegele, Lehrbeauftragter an der HWR-Berlin); Ort: Safe Suchtzentrum gGmbH
    • Sozial-ökologischer Umbau - das Konzept der Arbeitsgruppe Alternative Wirtschaftspolitik (Dr. Axel Troost, Arbeitsgruppe Alternative Wirtschaftspolitik); Ort: Rosa-Luxemburg-Stiftung Sachsen

13:45 - 14:45 Uhr 	
Mittagspause

14:45 - 17:00 Uhr
Workshopphase II: Praktische Handlungsstrategieentwicklung 
    • Feministische und postkoloniale Reflexion auf bezahlte und unbezahlte Arbeit als Teil einer sozialökologischen Transformation (Dr. Karin Schönpflug, Universität Wien); Ort: Rosalinde Leipzig e.V.
    • Postwachstum (Klaus Dörre, Friedrich-Schiller-Universität-Jena); Ort: Projekte- und Abgeordnetenbüro INTERIM by linXXnet 
    • Euro, Wert und Kilojoule. Privateigentum als Öko-Killer (Dr. Alexander B. Voegele, Lehrbeauftragter an der HWR-Berlin); Ort: Safe Suchtzentrum gGmbH
    • Sozial-ökologischer Umbau - das Konzept der Arbeitsgruppe Alternative Wirtschaftspolitik (Dr. Axel Troost Arbeitsgruppe Alternative Wirtschaftspolitik); Ort: Rosa-Luxemburg-Stiftung Sachsen

17:00 – 17.30 Uhr 
Pause 

17.30 - 18.30 Uhr 	
Abschlussplenum mit allen Referent*innen und Teilnehmer*innen; 
Ort: Projekte- und Abgeordnetenbüro INTERIM by linXXnet 

Ab 18.30 Uhr		
Ende der Konferenz, Get together 