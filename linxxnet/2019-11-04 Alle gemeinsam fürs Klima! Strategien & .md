---
id: "430353357616834"
title: Alle gemeinsam fürs Klima! Strategien & Ziele der Klimabewegung
start: 2019-11-04 19:00
end: 2019-11-04 21:00
locationName: linXXnet
address: Brandstr. 15, 04277 Leipzig-Connewitz, Sachsen
link: https://www.facebook.com/events/430353357616834/
image: 72310698_2460074420745003_2296766192834052096_n.jpg
teaser: Seit einiger Zeit gibt es wachsende Proteste für wirksame Maßnahmen gegen den
  Klimawandel. Insbesondere die Jugendbewegung Fridays for future hat die
isCrawled: true
---
Seit einiger Zeit gibt es wachsende Proteste für wirksame Maßnahmen gegen den Klimawandel. Insbesondere die Jugendbewegung Fridays for future hat die Politik unter Druck gesetzt.
Das im September vorgelegte Klimapaket der Bundesregierung bleibt weiter hinten den Forderungen aus der Zivilgesellschaft zurück und wird nicht dazu beitragen die Klimaziele von Paris zu erfüllen.
Was sind Ziele und Mittel des Protestes gegen den Klimawandel? Muss es nicht um einen grundlegenden Wandel der  Produktionsverhältnisse anstelle von kosmetischen Maßnahmen gehen? Wie schaffen wir es Klimapolitik sozial gerecht zu gestalten und welche Mittel sind wirksam?

Das wollen wir mit Vertreter*innen von Fridays for future, Scientists for future, Extinction rebellion  und Marco Böhme, MdL DIE LINKE diskutieren.
Moderiert wird der Abend von Michael Neuhaus (Stadtrat Leipzig und Mitglied im Bundessprecher*innenrat der Linksjugend) 

Begleitend wird im linXXnet eine Ausstellung des Fotografen Tim Wagner zu Klimaprotesten im Hambacher Forst über »Ende Gelände« im Rheinland bis hin zu den Klimastreiks von Fridays for Future in Leipzig gezeigt.