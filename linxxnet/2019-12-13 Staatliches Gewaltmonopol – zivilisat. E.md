---
id: "2842993555712780"
title: Staatl. Gewaltmonopol – zivilisatorische Errungenschaft o Risiko
start: 2019-12-13 19:00
end: 2019-12-13 22:00
locationName: UT Connewitz
address: Wolfgang-Heinze-Str. 12a, 04277 Leipzig
link: https://www.facebook.com/events/2842993555712780/
image: 75388225_2538223059596805_8638798254537965568_o.jpg
teaser: Funktioniert eine Gesellschaft ohne Polizei?  Die Konstituierung des
  staatlichen Gewaltmonopols gilt als zivilisatorische Errungenschaft moderner
  Demo
isCrawled: true
---
Funktioniert eine Gesellschaft ohne Polizei?

Die Konstituierung des staatlichen Gewaltmonopols gilt als zivilisatorische Errungenschaft moderner Demokratien, mit dem Ziel Selbstjustiz und antidemokratische Machtausübung durch Monarchen einzuhegen und Gewaltausübung demokratischen Regeln zu unterwerfen.
In der Realität funktioniert dieses Ideal nicht, immer wieder löst(e) sich staatliches Handeln historisch und aktuell von verfassten Regeln ab und wurde und wird zur staatlichen Willkür.

Die kritischen Debatten um die Polizei beschränken sich in der Regel auf Forderungen nach deren Demokratisierung. Die Seite der fundamentaleren Kritik kommt meist nicht über brachiale Kampfansagen hinaus.
Für eine radikale Linke, die über das bestehende Gesellschaftssystem hinausdenkt, stellt die Konzentration der Gewalt in den Händen des bürgerlichen Staates grundsätzlich ein Problem dar. Schon auf dem Weg in eine befreite, basisdemokratisch orientierte Gesellschaft, die gleichsam die Überwindung des Bestehenden bedeutet, wird die Inhaber*in der bürgerlich-staatlichen Gewalt im Wege stehen und das Falsche bewahren wollen.
Und: was passiert danach? Wird eine freie, emanzipatorische Gesellschaft ohne staatliche Gewalt auskommen?

Ist eine Gesellschaft ohne Polizei möglich? Welche Alternativen zur Konzentration des Gewaltmonopols in den Händen einer staatlichen Institution gibt es? Wie kann das Zusammenleben alternativ und gewaltfrei unter Wahrung zivilisatorischer Errungenschaften organisiert werden? Darüber diskutieren Karl-Heinz Dellwo (Autor), Florian Krahmer (Politikwissenschaftler) und ein*e Vertreter*in des Internationalistischen Zentrum Dresden. 