---
id: "2119191475057297"
title: Linkes Forum zur OBM-Wahl
start: 2019-11-03 18:00
end: 2019-11-03 20:00
locationName: VILLA Leipzig
address: Lessingstrasse 7, 04109 Leipzig
link: https://www.facebook.com/events/2119191475057297/
image: 74477373_2501824249937296_5268432547150299136_n.jpg
teaser: Leipzig für Alle! LINKES Forum zur OBM-Wahl 2020 am SO 03.11. um 18 Uhr im
  Großen Saal der VILLA (Lessingstr. 7)  Am 02.02.2020 ist es wieder soweit!
isCrawled: true
---
Leipzig für Alle!
LINKES Forum zur OBM-Wahl 2020 am SO 03.11. um 18 Uhr im Großen Saal der VILLA (Lessingstr. 7)

Am 02.02.2020 ist es wieder soweit! Der Oberbürgermeisterstuhl steht in Leipzig zur Wahl. Während SPD, CDU, Grüne und AfD ihre Kandidat*innen bereits ausgeschachert haben, ruft DIE LINKE dazu auf, in direkten Wahlen auf der Gesamtmitgliederversammlung am 09.11. darüber zu entscheiden, wer für sie in den Wahlkampf ziehen soll: Die Stadträtin der LINKEN Franziska Riekewald oder der parteilose Sozialrechtsanwalt Dirk Feiertag. Wofür stehen die Kandidatin und der Kandidat? Welche Inhalte werden sie vertreten? Und welche progressiven Ideen haben sie für unsere Stadt?
Am Sonntag, den 03.11., um 18 Uhr im Großen Saal der VILLA (Lessingstr. 07) werden Franziska und Dirk über ihr Programm sprechen und sich Deinen Fragen stellen. Komm vorbei und misch Dich ein! Es ist Deine Wahl.

Der Eintritt ist frei.