---
id: "831700247247043"
title: Schulter an Schulter gegen Faschismus
start: 2019-11-23 10:00
end: 2019-11-23 13:00
address: Connewitzer Kreuz
link: https://www.facebook.com/events/831700247247043/
image: 77179429_2540578939361217_3553550993184522240_n.jpg
teaser: Am Samstag, 23. November soll es wieder eine faschistische Kundgebung in
  Leipzig-Connewitz geben. Mehrere Versuche der Rechten um André Poggenburg in
isCrawled: true
---
Am Samstag, 23. November soll es wieder eine faschistische Kundgebung in Leipzig-Connewitz geben. Mehrere Versuche der Rechten um André Poggenburg in diesem Jahr sind bereits gescheitert. Immer wieder geht es bei den Kundgebungen um die Stigmatisierung eines ganzen Stadtteils und deren Bewohner*innen. Getragen wird diese nicht nur von den rechten Akteur*innen selbst. Im Zuge der aktuellen Debatte um Angriffe auf Bauprojekte und eine Angestellte eines Immobilienunternehmens beteiligten sich auch AfD, CDU, Teile der Medien und der Polizei an der Stimmungsmache gegen einen ganzen Stadtteil. Kein Wunder, dass der gescheiterte Faschist Poggenburg wieder auf den Zug aufspringen will.

Wir stellen uns gemeinsam und entschlossen gegen diese Hetze. Wir haben die permanenten Schikanen gegen einen ganzen Stadtteil satt. Connewitz ist ein Ort des antifaschistischen Widerstandes, linker Utopien und einer solidarischen Nachbar*innenschaft. Ein Stadtteil, der seit mehreren Jahrzehnten rechten und staatlichen Angriffen ausgesetzt ist und sich nicht unterkriegen lässt. Wir werden daher auch Samstag wieder zusammen kommen müssen und deutlich machen, dass wir rechte Angriffe in Leipzig nicht einfach hinnehmen werden.

Kommt am Samstag um 10 Uhr zur Demonstration am Connewitzer Kreuz. Wir laufen zusammen eine kleine Runde zum geplanten Kundgebungsort der Faschisten in der Brandstraße (11:00 bis 12:30, Höhe Hausnummern 28/30). Das linXXnet wird offen sein, hier gibts Tee, Transpis und ein Plätzchen zum Aufwärmen.

Für ein antifaschistisches Viertel!

#le2311