---
id: "431844824125667"
title: "Workshop: Klima und Gesundheit"
start: 2019-10-23 19:00
end: 2019-10-23 21:00
address: Liebigstraße 27, 04103 Leipzig
link: https://www.facebook.com/events/431844824125667/
image: 71497845_2335738583356708_6707424411546288128_o.jpg
teaser: Der Klimawandel wirkt sich erheblich auf die Gesundheit der Menschen aus.
  Schon jetzt führen Luftverschmutzung, Naturkatastrophen und Hitze zu vermehr
isCrawled: true
---
Der Klimawandel wirkt sich erheblich auf die Gesundheit der Menschen aus. Schon jetzt führen Luftverschmutzung, Naturkatastrophen und Hitze zu vermehrten Infektionskrankheiten, Allergien und Existenzängsten. Die medizinische Versorgung scheint in Anbetracht dieser Bedrohungen machtlos. 

Wir werden gemeinsam erarbeiten, warum der Klimawandel als eine der wichtigsten Herausforderungen für die Gesundheit der Menschen erkannt werden muss. Die Veranstaltung richtet sich an Studierende aller Studienbereiche und andere Interessierte aus den Bereichen der Gesundheitsversorgung. 

Eine Woche später geht es weiter mit der Frage, wie das Gesundheitswesen zum Klimawandel beiträgt und wie ebendieses zur gesamtgesellschaftlichen Transformation hin zu Klimaneutralität teilnehmen kann. Wir freuen uns auf die Podiumsdiskussion "Green Hospital in Sachsen?!" am 30.10.2019 um 19 Uhr: 
https://m.facebook.com/events/759797111139204?acontext=%7B%22action_history%22%3A%22[%7B%5C%22surface%5C%22%3A%5C%22page%5C%22%2C%5C%22mechanism%5C%22%3A%5C%22main_list%5C%22%2C%5C%22extra_data%5C%22%3A[]%7D]%22%7D&aref=0&ref=m_notif&notif_t=page_insights_weekly_digest