---
name: Kritische Medizin Leipzig
website: https://www.leipzig.kritmed.de
email: kritmed-leipzig@riseup.net
scrape:
  source: facebook
  options:
    page_id: KritischeMedizinLeipzig
---
Wir sind eine Hochschulgruppe der Universität Leipzig und zusammen bilden wir die Arbeitsgruppe „Kritische Medizin Leipzig“, die Menschen aus dem Gesundheitswesen vereint.
Wir haben uns zur Aufgabe gemacht, sämtliche Prozesse, Abläufe und Strukturen in Bezug auf gesundheitspolitische Fragen kritisch zu beleuchten.