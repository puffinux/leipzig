---
id: "920142185053021"
title: Kittel fürs Klima - Global Climate Strike!
start: 2019-11-29 15:00
end: 2019-11-29 17:00
address: Simsonplatz, 04107 Leipzig
link: https://www.facebook.com/events/920142185053021/
image: 75279291_2380105612253338_6379336412470181888_n.jpg
teaser: ... und Kasacks und Patientenhemden auch!  Am 29.11.2019, dem Freitag vor der
  Weltklimakonferenz, gehen wieder weltweit Menschen für sofortige Klimage
isCrawled: true
---
... und Kasacks und Patientenhemden auch!

Am 29.11.2019, dem Freitag vor der Weltklimakonferenz, gehen wieder weltweit Menschen für sofortige Klimagerechtigkeit und Klimaschutz auf die Straße. Das Klimaschutzpaket der Bundesregierung ist eine Absage an das 1,5° Ziel. Das muss bestreikt werden!
Als Menschen des Gesundheitswesens wissen wir, dass der klimawandel die größte Gefahr für die Gesundheit der Menschen im 21. Jahrhundert ist. Deshalb werden auch wir am 29.11. in Leipzig auf die Straße gehen.

Als Teil des Bildungsblocks mit ScientistsForFuture werden HealthForFuture und die Kritische Medizin ein klares Zeichen für Klimaschutz zu setzen. Und dafür brauchen wir euch! Kommt am 29.11.2019 um 15 Uhr zum Simsonplatz und bringt Kasacks, Kittel, Patientenhemden und sonstige Accesoires mit. 

Denn: Gesundheit braucht Klimaschutz! 