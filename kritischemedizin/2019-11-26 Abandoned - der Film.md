---
id: "782585655534056"
title: Abandoned - der Film
start: 2019-11-27 20:00
end: 2019-11-27 22:00
address: Liebigstraße 27, 04103 Leipzig
link: https://www.facebook.com/events/782585655534056/
image: 76184065_729927597518335_2370865197712670720_n.jpg
teaser: im kleinen Hörsaal des CLIs   Vorführung des Dokumentarfilms ABANDONED von
  Patricia Josefine Marchart mit anschließender Diskussionsrunde.  Der Film z
isCrawled: true
---
im kleinen Hörsaal des CLIs 

Vorführung des Dokumentarfilms ABANDONED von Patricia Josefine Marchart mit anschließender Diskussionsrunde. 
Der Film zeigt die Geschichte von Frauen in Europa, denen ÄrztInnen einen legalen und medizinisch notwendigen Schwangerschaftsabbruch verweigerten.

Es werden keine expliziten Bilder gezeigt, dennoch sind die Themen emotional behaftet. Bei Fragen, schreibt uns gerne! 

-TRIGGER WARNUNG- 

https://abandoned.film/de/
