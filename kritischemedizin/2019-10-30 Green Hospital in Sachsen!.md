---
id: "759797111139204"
title: Green Hospital in Sachsen?!
start: 2019-10-30 19:00
end: 2019-10-30 21:00
address: Liebigstraße 27, 04103 Leipzig
link: https://www.facebook.com/events/759797111139204/
image: 71806248_2335740466689853_364225506012299264_o.jpg
teaser: 'Podiumsdiskussion: "Green Hospital in Sachsen?!" am 30.10.2019 um 19 Uhr im
  kleinen Hörsaal der Liebigstraße 27  Der anthropogene Klimawandel hat dire'
isCrawled: true
---
Podiumsdiskussion: "Green Hospital in Sachsen?!" am 30.10.2019 um 19 Uhr im kleinen Hörsaal der Liebigstraße 27

Der anthropogene Klimawandel hat direkte Auswirkungen auf die Gesundheit der Menschen. Paradoxerweise sind gerade Krankenhäuser oft Orte enormen Energieverbrauchs und riesiger Ressourcenverschwendung und stehen damit im Widerspruch zu Umweltfreundlichkeit. Dieses Phänomen lässt sich auf das gesamte Gesundheitswesen übertragen.
Wir werden in Form einer Podiumsdiskussion erarbeiten, was für Ansätze von Krankenhäusern und Politik bereits existieren und inwiefern diese in Sachsen und Deutschland schon umgesetzt werden. Wir möchten diese dann kritisch hinterfragen um zu verstehen, ob sie zielführend sind oder ob nach alternativen Lösungswegen gesucht werden muss.

Dafür konnten wir vier Podiumsteilnehmer*innen gewinnen:

Friedrich München, Krankenhausgesellschaft Sachsen e.V.
Dr. Nicola Klöß, Umweltbeauftragte Universität Leipzig und ehemals des Uniklinikums Leipzig,
Laura Jung, Health4Future Leipzig und KLUG
Rosa Emrich, AG Gesundes Klima der Kritischen Medizin Deutschland 

Diese Veranstaltung folgt auf die Einführungsveranstaltung am 23.10., bei der wie gemeinsam den Zusammenhang zwischen Klimawandel und Gesundheit erarbeiten werden: 
https://m.facebook.com/events/431844824125667?acontext=%7B%22ref%22%3A%223%22%2C%22action_history%22%3A%22null%22%7D&aref=3