---
id: 20190114-singenundswingen
title: Singen und Swingen im Frauenchor "Canta Animata"
start: '2019-01-14 19:00'
end: null
locationName: FraKu
address: 'Windscheidstr. 51, 04277 Leipzig'
link: 'https://www.frauenkultur-leipzig.de/Programm/Programm.html#14'
image: null
teaser: 'Leitung: REGINA KOLB'
recurring: null
isCrawled: true
---
Leitung: REGINA KOLB