---
id: 20181207-yogafrjedefrau
title: Yoga für jede Frau
start: '2018-12-07 15:00'
end: '2018-12-07 16:00'
locationName: FraKu
address: 'Windscheidstr. 51, 04277 Leipzig'
link: 'https://www.frauenkultur-leipzig.de/Programm/Programm.html#7'
image: null
teaser: |-
  Jeden Freitag: 15.00 - 16.00 Uhr, Kursleiterin: SEA www.wellyoga.org 
   Teilnahme ist kostenlos. 
   Ort: Kleine (neue) Halle am Rabet, Konradstraße 30, 
recurring: null
isCrawled: true
---
Jeden Freitag: 15.00 - 16.00 Uhr, Kursleiterin: SEA www.wellyoga.org 
 Teilnahme ist kostenlos. 
 Ort: Kleine (neue) Halle am Rabet, Konradstraße 30, 04315 Leipzig Ost/Volkmarsdorf. 
 Bitte bequeme Kleidung mitbringen.