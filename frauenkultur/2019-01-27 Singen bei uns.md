---
id: 20190127-singenbeiuns
title: Singen bei uns
start: '2019-01-27 15:00'
end: '2019-01-27 17:00'
locationName: FraKu
address: 'Windscheidstr. 51, 04277 Leipzig'
link: 'https://www.frauenkultur-leipzig.de/Programm/Programm.html#27'
image: null
teaser: |-
  Offenes CHOR-Projekt 

   Der Chor "Singen bei uns" lädt ein zum gemeinsamen Singen. 
  Chorleitung: STEFANIE PLESCHKA. 
   Das Chorprogramm spannt einen Bo
recurring: null
isCrawled: true
---
Offenes CHOR-Projekt 

 Der Chor "Singen bei uns" lädt ein zum gemeinsamen Singen. 
Chorleitung: STEFANIE PLESCHKA. 
 Das Chorprogramm spannt einen Bogen von berührenden Liedern bis zu mitreißenden Songs. Wir freuen uns über weitere interessierte Sängerinnen und Sänger, die Lust haben, mitzusingen. Eintritt: frei