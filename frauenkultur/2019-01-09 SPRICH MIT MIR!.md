---
id: 20190109-sprichmitmir
title: SPRICH MIT MIR!
start: '2019-01-09 15:00'
end: '2019-01-09 17:00'
locationName: FraKu
address: 'Windscheidstr. 51, 04277 Leipzig'
link: 'https://www.frauenkultur-leipzig.de/Programm/Programm.html#9'
image: null
teaser: |-
  Sprachtandem 
   
   Ein Sprachtandem-Projekt der Frauenkultur Leipzig und des Museums der Bildenden Künste Leipzig.
   
   Thema heute: Die Wüste 
   
   Interes
recurring: null
isCrawled: true
---
Sprachtandem 
 
 Ein Sprachtandem-Projekt der Frauenkultur Leipzig und des Museums der Bildenden Künste Leipzig.
 
 Thema heute: Die Wüste 
 
 Interessierte Frauen können an jedem 2. Mittwoch im Monat das Museum der bildenden Künste besuchen und im Sprachtandem dabei Sprache neu lernen. Eine Deutsch-Sprechende und eine Deutsch-Lernende Frau besuchen gemeinsam das Museum - und erzählen einander, was sie sehen. Sie entdecken zusammen Bilder - und lernen gemeinsam Deutsch oder auch eine andere Sprache. 
 
 Die Marion-Ermer-Preisträgerin 2018 FINE BIELER "schickt uns in die Wüste". Was für Bilder haben wir von diesem Ort? 
 Woher kommen diese Bilder? Und vielleicht ist die Realität ganz anders? 
 Für Frauen und Mädchen ab 14 Jahren. Die Teilnahme ist kostenlos. 
 Interessierte Frauen und Mädchen melden sich bitte in der Frauenkultur Leipzig | Tel. 0341 - 213 00 30
 oder im Museum der Bildenden Künste Leipzig I Tel. 0341 - 216 999 923, kirsten.huwig@leipzig.de