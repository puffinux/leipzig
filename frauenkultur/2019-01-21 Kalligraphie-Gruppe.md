---
id: 20190121-kalligraphiegrup
title: Kalligraphie-Gruppe
start: '2019-01-21 18:30'
end: '2019-01-21 20:00'
locationName: FraKu
address: 'Windscheidstr. 51, 04277 Leipzig'
link: 'https://www.frauenkultur-leipzig.de/Programm/Programm.html#21'
image: null
teaser: |-
  Relative feste Gruppe, die seit längerer Zeit zusammen 'kalligraphisch' arbeitet. Bitte bei Interesse Anfragen unter FraKu 0341 - 21 300 30 

  Leitung:
recurring: null
isCrawled: true
---
Relative feste Gruppe, die seit längerer Zeit zusammen 'kalligraphisch' arbeitet. Bitte bei Interesse Anfragen unter FraKu 0341 - 21 300 30 

Leitung: Renate Reitz-Schiweksche 
wöchentlich | Montag | 18:30 - 20:00 Uhr