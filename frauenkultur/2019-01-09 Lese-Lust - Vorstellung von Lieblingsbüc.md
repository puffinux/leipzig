---
id: 20190109-leselustvorstell
title: Lese-Lust - Vorstellung von "Lieblingsbüchern"
start: '2019-01-09 11:00'
end: '2019-01-09 13:00'
locationName: FraKu
address: 'Windscheidstr. 51, 04277 Leipzig'
link: 'https://www.frauenkultur-leipzig.de/Programm/Programm.html#9'
image: null
teaser: |-
  Herzlich eingeladen sind alle, die Freude am Lesen haben und gerne darüber debattieren möchten. 
   Jeden 1. Mittwoch im Monat von 11 - 13 Uhr.
   
   Leitu
recurring: null
isCrawled: true
---
Herzlich eingeladen sind alle, die Freude am Lesen haben und gerne darüber debattieren möchten. 
 Jeden 1. Mittwoch im Monat von 11 - 13 Uhr.
 
 Leitung: S. SOMMER