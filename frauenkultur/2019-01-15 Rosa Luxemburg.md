---
id: 20190115-rosaluxemburg
title: Rosa Luxemburg
start: '2019-01-15 11:00'
end: '2019-01-15 17:00'
locationName: FraKu
address: 'Windscheidstr. 51, 04277 Leipzig'
link: 'https://www.frauenkultur-leipzig.de/Programm/Programm.html#15'
image: null
teaser: |-
  FILM mit thematischer Einführung. 
   
   
   Anlässlich des 100. Jahrestages ihrer Ermordung zeigen wir den gleichnamigen Film von Margarethe von Trotta (1
recurring: null
isCrawled: true
---
FILM mit thematischer Einführung. 
 
 
 Anlässlich des 100. Jahrestages ihrer Ermordung zeigen wir den gleichnamigen Film von Margarethe von Trotta (117 min.), der auf berührende Weise das Schicksal der zutiefst humanistischen und visionären deutsch-polnischen Sozialistin erzählt.