---
id: 20190120-souvenirsdejeune
title: Souvenirs de jeunesse. Komponierende junge Frauen der Romantik
start: '2019-01-20 16:00'
end: null
locationName: FraKu
address: 'Windscheidstr. 51, 04277 Leipzig'
link: 'https://www.frauenkultur-leipzig.de/Programm/Programm.html#20'
image: null
teaser: |-
  KONZERT 
   
   
   Frühe Lieder von Clara Schumann geb. Wieck und Fanny Hensel geb. Mendelssohn. 
   Mit TEMI RAPHAELOVA, bulgarische Sopranistin mit langjäh
recurring: null
isCrawled: true
---
KONZERT 
 
 
 Frühe Lieder von Clara Schumann geb. Wieck und Fanny Hensel geb. Mendelssohn. 
 Mit TEMI RAPHAELOVA, bulgarische Sopranistin mit langjähriger Bühnenerfahrung, bis 2018 Ensemblemitglied des Dresdner Residenz-orchesters und CHRISTINA ENGELKE, Solo-Harfenistin u.a. an der Neuen Lausitzer Philharmonie am Theater Görlitz