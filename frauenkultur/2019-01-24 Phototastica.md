---
id: 20190124-phototastica
title: Phototastica
start: '2019-01-24 19:00'
end: null
locationName: FraKu
address: 'Windscheidstr. 51, 04277 Leipzig'
link: 'https://www.frauenkultur-leipzig.de/Programm/Programm.html#24'
image: null
teaser: |-
  AUSSTELLUNGSERÖFFNUNG 
  Bildwerke der Künstlerin GRET-SUSANN PANNASCH 
   Die Künstlerin möchte Geschichten erzählen... Geschichten von Stolz, Kampfgeist
recurring: null
isCrawled: true
---
AUSSTELLUNGSERÖFFNUNG 
Bildwerke der Künstlerin GRET-SUSANN PANNASCH 
 Die Künstlerin möchte Geschichten erzählen... Geschichten von Stolz, Kampfgeist, Schönheit, Anmut und Liebe. Während ihre Portraits in Momentaufnahmen Emotionen prägnant einfangen, bedient sie sich der Fotografie ebenso als künstlerische Vorzeichnung für die " technische Malerei" und kreiert fantastische Märchen. 
Musikalische Eröffnung: DJ Florian Fischer