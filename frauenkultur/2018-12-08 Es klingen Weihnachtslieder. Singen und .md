---
id: 20181208-esklingenweihnac
title: Es klingen Weihnachtslieder. Singen und Basteln für die ganze Familie
start: '2018-12-08 16:00'
end: null
locationName: FraKu
address: 'Windscheidstr. 51, 04277 Leipzig'
link: 'https://www.frauenkultur-leipzig.de/Programm/Programm.html#8'
image: null
teaser: |-
  WEIHNACHTS-SPECIAL 
   
   Vorbereitungen für große und kleine Geschenke machen Freude: kleine Weihnachtskärtchen oder gebastelte Geschenkkästchen machen 
recurring: null
isCrawled: true
---
WEIHNACHTS-SPECIAL 
 
 Vorbereitungen für große und kleine Geschenke machen Freude: kleine Weihnachtskärtchen oder gebastelte Geschenkkästchen machen das Schenken besonders schön. Bei unserem Weihnachtsspezial können alle, die Lust haben, kreativ werden. Und da zur Vorweihnachtszeit auch weihnachtliche Lieder gehören, lädt der Chor "Singen bei uns" ein zum gemeinsamen Singen. Dazu gibt es lustige Rätsel für die ganze Familie .und viel weihnachtliche Vorfreude. 
 Eintritt: frei