---
id: 20190107-singenundswingen
title: Singen und Swingen im Frauenchor "Canta Animata"
start: '2019-01-07 19:00'
end: null
locationName: FraKu
address: 'Windscheidstr. 51, 04277 Leipzig'
link: 'https://www.frauenkultur-leipzig.de/Programm/Programm.html#7'
image: null
teaser: 'Leitung: REGINA KOLB'
recurring: null
isCrawled: true
---
Leitung: REGINA KOLB