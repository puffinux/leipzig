---
id: 20181130-tausendundeinena
title: Tausendundeine Nacht
start: '2018-11-30 15:00'
end: '2018-11-30 17:00'
locationName: FraKu
address: 'Windscheidstr. 51, 04277 Leipzig'
link: 'https://www.frauenkultur-leipzig.de/Programm/Programm.html#30'
image: null
teaser: |-
  TEE & INTERKULTURELLES GESPRÄCH 
   
   
  Die Geschichten-Sammlung "1001 Nacht" ist weltbekannt. Nicht genau bekannt ist ihr kultureller Ursprung: Aus der 
recurring: null
isCrawled: true
---
TEE & INTERKULTURELLES GESPRÄCH 
 
 
Die Geschichten-Sammlung "1001 Nacht" ist weltbekannt. Nicht genau bekannt ist ihr kultureller Ursprung: Aus der Zeit um 250, indischer oder persischer Beheimatung, kommen die Kern-Erzählungen. Im 8. Jh. wur- den die Geschichten ins Arabische übersetzt und um Zitate des gerade entstandenen Islam erweitert. Im Laufe der Jahrhunderte kamen immer neue Geschichten wie auch Übersetzungen hinzu. 
 
Die Erzählerin Scheherazade aus "1000 und einer Nacht" wurde weltberühmt. Scheherazade erzählt dem König jede Nacht eine Geschichte, die sie am Ende der Nacht an der spannendsten Stelle abbricht. Der König, der unbedingt die Fortsetzung hören will, schiebt deshalb die geplante Hinrichtung Scheherazades immer wieder auf. Nach tausendundeiner Nacht sah der König sich selbst, die Frauen und die Welt anders.


Tee & Gespräch ist eine Veranstaltungsreihe im Interkulturellen Frauen-Informations-und Begegnungszentrum FiA - ein Projekt der Frauenkultur Leipzig. Diese Angebot im Leipziger Osten eröffnet einen sozial-kommunikativen Ort der Begegnung: Bei einer Tasse Tee kommen Frauen aus verschiedenen Ländern zu ausgewählten Themen miteinander ins Gespräch. Die Teilnahme ist kostenfrei. 

Veranstaltung in Kooperation mit der Volkshochschule der Stadt Leipzig im Bereich der Politischen Bildung 

Ort: FiA - Interkulturelles Frauen-Informations- und Begegnungszentrum, Konradstraße 62, 04315 Leipzig