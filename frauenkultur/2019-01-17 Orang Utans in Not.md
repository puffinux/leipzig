---
id: 20190117-orangutansinnot
title: Orang Utans in Not
start: '2019-01-17 19:00'
end: null
locationName: FraKu
address: 'Windscheidstr. 51, 04277 Leipzig'
link: 'https://www.frauenkultur-leipzig.de/Programm/Programm.html#17'
image: null
teaser: |-
  VORTRAG 
   
   mit Marlen Kücklich, Biologin und verantwortlich für die Umweltbildung des Vereins Orang Utans in Not e.V. 
   
   Unser Verein Orang Utans in
recurring: null
isCrawled: true
---
VORTRAG 
 
 mit Marlen Kücklich, Biologin und verantwortlich für die Umweltbildung des Vereins Orang Utans in Not e.V. 
 
 Unser Verein Orang Utans in Not wurde 2008 in Leipzig gegründet und widmet sich dem Schutz und Erhalt der letzten freilebenden Orang-Utans auf Sumatra und Borneo, deren Lebensraum für Palmölplantagen immer weiter zerstört wird. 
 Hier in Deutschland versuchen wir durch Umweltbildungsprojekte mit Kindern und Erwachsenen auf dieses Problem aufmerksam zu machen. Wir stellen die liebenswerten Waldmenschen und ihren Lebensraum, den Regenwald, vor und diskutieren über die Bedrohungen.