---
id: 20181207-weihnachtenamkre
title: Weihnachten am Kreuz
start: '2018-12-07 16:00'
end: null
locationName: FraKu
address: 'Windscheidstr. 51, 04277 Leipzig'
link: 'https://www.frauenkultur-leipzig.de/Programm/Programm.html#7'
image: null
teaser: |-
  Eröffnung des Weihnachtsmarkts der Kulturfabrik 
   
   .geöffnet bis zum 16.12.2018 und randvoll mit Köstlichem, Schönem, Einzigartigem und wundervoll Du
recurring: null
isCrawled: true
---
Eröffnung des Weihnachtsmarkts der Kulturfabrik 
 
 .geöffnet bis zum 16.12.2018 und randvoll mit Köstlichem, Schönem, Einzigartigem und wundervoll Duftendem in Halle A, Halle D und Halle 5 
 Wir freuen uns auf alle, die einfach Lust haben, auch bei unserem Spendenbasar vorbeizuschauen!