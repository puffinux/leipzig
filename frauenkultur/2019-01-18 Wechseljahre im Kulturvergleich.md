---
id: 20190118-wechseljahreimku
title: Wechseljahre im Kulturvergleich
start: '2019-01-18 15:00'
end: '2019-01-18 17:00'
locationName: FraKu
address: 'Windscheidstr. 51, 04277 Leipzig'
link: 'https://www.frauenkultur-leipzig.de/Programm/Programm.html#18'
image: null
teaser: |-
  TEE & INTERKULTURELLES GESPRÄCH
   
   
   Während der Wechseljahre lernen viele Frauen ihren Köper "anders kennen". Bis dahin unbekannte "hitzewallende ode
recurring: null
isCrawled: true
---
TEE & INTERKULTURELLES GESPRÄCH
 
 
 Während der Wechseljahre lernen viele Frauen ihren Köper "anders kennen". Bis dahin unbekannte "hitzewallende oder/und melancholische Momente" begleiten öfters diese Zeit, in der meist auch die Kinder aus dem Haus gehen und oft Eltern verstärkt der Unterstützung bedürfen. Der ganze Alltag verändert sich, es beginnt eine neue Lebensphase. Manchmal wird diese Neu-Beginn leichter, wenn man mit anderen Frauen darüber sprechen kann. Und auch über Mittel und Möglichkeiten, die den "Wechsel" erleichtern. 
 
 Diese Veranstaltung der Volkshochschule Leipzig im Bereich der Politischen Bildung ist entgeltfrei - und findet statt in Kooperation mit dem Soziokulturellem Zentrum Frauenkultur. 
 Anmeldung erwünscht. Ort: FiA, Konradstr. 62, 04315 Leipzig