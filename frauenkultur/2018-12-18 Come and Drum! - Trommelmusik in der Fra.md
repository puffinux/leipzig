---
id: 20181218-comeanddrumtromm
title: Come and Drum! - Trommelmusik in der Frauenkultur
start: '2018-12-18 20:00'
end: null
locationName: FraKu
address: 'Windscheidstr. 51, 04277 Leipzig'
link: 'https://www.frauenkultur-leipzig.de/Programm/Programm.html#18'
image: null
teaser: 'Westafrikanische und afrokaribische Trommelmusik zu erlernen und zu spielen ist eine der schönsten Möglichkeiten, Rhythmus zu erleben und den Tanz des'
recurring: null
isCrawled: true
---
Westafrikanische und afrokaribische Trommelmusik zu erlernen und zu spielen ist eine der schönsten Möglichkeiten, Rhythmus zu erleben und den Tanz des Lebens besser zu verstehen, seine Kraft zu erfahren und sich mit ihm wohl zu fühlen. 
 Anmeldung erforderlich! 
 Kursleitung: Ingeborg Freytag