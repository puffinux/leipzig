---
id: 20181209-lesbischeskaffee
title: Lesbisches Kaffee-Trinken gegen Rechts
start: '2018-12-09 16:00'
end: null
locationName: FraKu
address: 'Windscheidstr. 51, 04277 Leipzig'
link: 'https://www.frauenkultur-leipzig.de/Programm/Programm.html#9'
image: null
teaser: 'Zweites Netzwerktreffen der Leipziger Gruppe "Lesben gegen Rechts", um gemeinsam zu überlegen, wie dem steigenden Rechtsdruck entgegnet werden kann un'
recurring: null
isCrawled: true
---
Zweites Netzwerktreffen der Leipziger Gruppe "Lesben gegen Rechts", um gemeinsam zu überlegen, wie dem steigenden Rechtsdruck entgegnet werden kann und wie Lesben vor Ort einander stärken können - auch um aktiv zu werden, um alltäglichen rassistischen Begeg-nungen zu trotzen.