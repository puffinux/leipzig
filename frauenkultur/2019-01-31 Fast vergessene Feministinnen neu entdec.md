---
id: 20190131-fastvergessenefe
title: Fast vergessene Feministinnen neu entdeckt!
start: '2019-01-31 11:00'
end: '2019-01-31 17:00'
locationName: FraKu
address: 'Windscheidstr. 51, 04277 Leipzig'
link: 'https://www.frauenkultur-leipzig.de/Programm/Programm.html#31'
image: null
teaser: |-
  VORTRAG/FEMMAGE 
   mit SOPHIE KRÜGER, Leipzig 
   
   Verstaubte Bücher, welche nur noch antiquarisch zu bekommen sind, genauer anschauen und den Feminismu
recurring: null
isCrawled: true
---
VORTRAG/FEMMAGE 
 mit SOPHIE KRÜGER, Leipzig 
 
 Verstaubte Bücher, welche nur noch antiquarisch zu bekommen sind, genauer anschauen und den Feminismus der Zweiten Frauenbewegung in aktuellem Licht zu sehen. Das war und ist für mich eine sehr spannende Entdeckung, denn es gibt kaum noch Frauen, welche jemals z.B. von Monique Wittig gehört haben oder von Erika Wisselinck, welche u.a. die Bücher von Mary Daly ins Deutsche übersetzte. 
 Welche Gedanken und Gefühle hattest du als du "Zami" von Audre Lorde gelesen hast? Kennst du nicht?! So ist es also Zeit, eine kleine Femmage zu starten und ein paar dieser tollen Frauen, welche nicht vergessen werden sollten, kurz lebendig werden zu lassen.