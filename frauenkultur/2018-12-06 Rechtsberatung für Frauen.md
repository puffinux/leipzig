---
id: 20181206-rechtsberatungfr
title: Rechtsberatung für Frauen
start: '2018-12-06 18:00'
end: '2018-12-06 20:00'
locationName: FraKu
address: 'Windscheidstr. 51, 04277 Leipzig'
link: 'https://www.frauenkultur-leipzig.de/Programm/Programm.html#6'
image: null
teaser: |-
  mit der Rechtsanwältin Tanja Müller-Tegethoff 
   
   Die Rechtsanwältin Tanja Müller-Tegethoff bietet Beratung für Frauen zu rechtlichen Problemen an. Di
recurring: null
isCrawled: true
---
mit der Rechtsanwältin Tanja Müller-Tegethoff 
 
 Die Rechtsanwältin Tanja Müller-Tegethoff bietet Beratung für Frauen zu rechtlichen Problemen an. Die Beratung erfolgt vertraulich und kostenfrei.
 
 Wichtig:
 
 Vorherige Anmeldung unter 0341 - 2130030 ist notwendig.