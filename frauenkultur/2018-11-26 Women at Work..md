---
id: 20181126-womenatwork
title: Women at Work.
start: '2018-11-26 09:00'
end: '2018-11-26 00:00'
locationName: FraKu
address: 'Windscheidstr. 51, 04277 Leipzig'
link: 'https://www.frauenkultur-leipzig.de/Programm/Programm.html#26'
image: null
teaser: |-
  FACHTAG 
   Migrantinnen in Bildung, Arbeit und Gesellschaft 
   
  Die Arbeitsgruppe "Ausbildung und Arbeit" des Netzwerkes Integration Migrant/-innen in L
recurring: null
isCrawled: true
---
FACHTAG 
 Migrantinnen in Bildung, Arbeit und Gesellschaft 
 
Die Arbeitsgruppe "Ausbildung und Arbeit" des Netzwerkes Integration Migrant/-innen in Leipzig e.V. lädt zu diesem Fachtag ein. Ziel ist es, die aktuelle Arbeitsmarktsituation der Frauen ressourcenorientiert zu betrachten, Herausforderungen zu identifizieren und Lösungsansätze zu diskutieren.
 
Frau Prof. Dr. SCHAHRZAD FARROKHZAD wird mit ihrem Vortrag, welcher die bundesweite Situation in den Blick nimmt, thematisch ein-führen. Um danach den Fokus auf Leipzig zu richten, schließt sich ein Überblick des Jobcenters Leipzig an; sowie die Vorstellung Leipziger Projekte mit speziellen Angeboten für Migrantinnen. Zudem werden einige Frauen Eindrücke und Perspektiven durch persönliche Erfahrungsberichte einbringen. In Workshops sollen anschließend Herausforderungen, Bedarfe und Handlungsmöglichkeiten für eine erfolgreiche Arbeitsmarktintegration der Frauen in Leipzig diskutiert werden. Nur mit Voranmeldung möglich.
Mit freundlicher Unterstützung von: Stadt Leipzig, Referat für Migration und Integration, Frauenkultur Leipzig, DaMigra e.V.