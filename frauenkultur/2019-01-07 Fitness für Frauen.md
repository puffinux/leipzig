---
id: 20190107-fitnessfrfrauen
title: Fitness für Frauen
start: '2019-01-07 19:00'
end: null
locationName: FraKu
address: 'Windscheidstr. 51, 04277 Leipzig'
link: 'https://www.frauenkultur-leipzig.de/Programm/Programm.html#7'
image: null
teaser: |-
  Ort: Kleine (neue) Halle am Rabet, Konradstraße 30, 04315 Leipzig Ost/Volkmarsdorf 
   Bitte bequeme Kleidung mitbringen. Teilnahme ist kostenlos. In Ko
recurring: null
isCrawled: true
---
Ort: Kleine (neue) Halle am Rabet, Konradstraße 30, 04315 Leipzig Ost/Volkmarsdorf 
 Bitte bequeme Kleidung mitbringen. Teilnahme ist kostenlos. In Kooperation mit dem MUT-Projekt/Damigra. 
 Kleinkinder können mitgebracht werden, da eine Betreuung möglich ist. 
 Anmeldungen im FiA | Tel. 0341 - 581 54 515