---
id: 20190127-lesbischeskaffee
title: Lesbisches Kaffee-Trinken gegen Rechts
start: '2019-01-27 16:00'
end: null
locationName: FraKu
address: 'Windscheidstr. 51, 04277 Leipzig'
link: 'https://www.frauenkultur-leipzig.de/Programm/Programm.html#27'
image: null
teaser: |-
  Offenes Treffen 

   Zweites Netzwerktreffen der Leipziger Gruppe "Lesben gegen Rechts", um gemeinsam zu überlegen, wie dem steigenden Rechtsdruck entge
recurring: null
isCrawled: true
---
Offenes Treffen 

 Zweites Netzwerktreffen der Leipziger Gruppe "Lesben gegen Rechts", um gemeinsam zu überlegen, wie dem steigenden Rechtsdruck entgegnet werden kann und wie Lesben vor Ort einander stärken können - auch um aktiv zu werden, um alltäglichen rassistischen Begegnungen zu trotzen.