---
id: "1"
title: Gesprächsangebot
start: '2019-11-04 19:00'
end: '2019-11-04 20:30'
address: Eisenbahnstrasse 125
link: https://outofaction.blackblogs.org/?page_id=142
teaser: "Wir bieten emotionale erste Hilfe für betroffene Einzelpersonen und Gruppen an"
recurring:
  rules:
    - units: 1
      measure: dayOfWeek
    - units: 0
      measure: weekOfMonthByDay
---
_Out of Action_ ist eine Gruppe von Aktivist_innen, die über die psychischen Folgen von
Repression und Gewalt im Kontext von linkem politischen Widerstand
informiert. Wir bieten emotionale erste Hilfe für betroffene
Einzelpersonen und Gruppen an und unterstützen einen solidarischen
Umgang miteinander auch durch (Bezugs-)Gruppenworkshops und
Informationsveranstaltungen.

Leute aus Leipzig und Umgebung können sich per Mail wenden an:
outofaction-leipzig@nadir★org

Fingerprint: `F2BD 7D09 35D8 886B 66A2 DCB7 DB0F B7A6 34A5 55CD` (gerne mit dem auf der Webseite vergleichen!)

```
-----BEGIN PGP PUBLIC KEY BLOCK-----
mQINBFj/h3YBEAC7+7Zy+LBfBcctuIniQ6OcLmcakq/T59JbO7TAD+3rdi2NMap2
Mct0eHuIKvAQr98HIZlW2H5B405HObEuPzPwXvRCELncugNlPkqeMMNyeOMN2foF
X1axvFVwFmKZIzgbpJgBbu9A6ag1Q1jlyGm4VX557qEGobzyvf8Ru+UPCRuewwDp
KZooWH5xhMkvCejNEvYglRNbtY026SYf+Rh+NmP/A/i1ptNhWhZ+yglpJ/k7n9Jt
XJJ3feVtZMFHhh6PcgP4Wt59+pZ3GL+TTNb8Q6zPKw28qNhnah9g7rz0T1GOP7GG
cNoIfZqtMRy2BcT/TSFNL1QcZEO3f2tNX+hS9JvfTdl07p2njZtksvQnSxbqjGdG
u5Sw3NWqpFMXTSZrgBYRpfV/wDzpsJVW41NRw+96KTzMnk9X+dyvb5pqC/qE8loI
VB0pxuW2g/2rsSuADBoFtYCAgYqQVdjDi1uNA26GLG3iAnaxC24Z2GLTVO+FE9fH
WQYB2hP6Sbur6nNwLBVgW3zAyH4LwL2w2qw+tN6LAyOKqpMPKlmMnRfOxZrVnPbc
8SZL3LsQ7PBwnX9KHZg3x5nKXmah8A6NMjJTY4B+qVyiNzt/wqAfVqu7S4N0+pcU
t54KL9KbgezEJm+WhC4Ig/YSlHaapnxssqy6NebSI6LxNco13xfkTlLcTwARAQAB
tDNvdXRvZmFjdGlvbi1sZWlwemlnIDxvdXRvZmFjdGlvbi1sZWlwemlnQG5hZGly
Lm9yZz6JAj8EEwECACkFAlj/h3YCGyMFCQlmAYAHCwkIBwMCAQYVCAIJCgsEFgID
AQIeAQIXgAAKCRDbD7emNKVVzTi6EAChPYEcOSlmOhMaUOJiqILBu76VhCezejfd
Nuh4e09nW/UkLZRSMNzwITp+Ctlbl7nCvczKgLgTzido9w9HRP9SsgPDv2TvKbgo
Nk3NERHDHP9oCRIO0wIzwYHUHxQiA4A0JbEz1mqCX0rPQ3xRSA/AVFCxEV94c9Wg
pi1QwfZB5SY9zjkHOLe0+iy6+/4M7SukjoLgnTPSphqcVPAbLNn2IJ6+G+qJJDc5
r9Z09a5SqfU87UKbOZHzn3Z9gqW82gjhy6u5prxbCSCr43KgHZBXKMYrH5qsWmdF
K0FUeqRy70SUitwjQOJnlHr8mlv9rztaTu3Emp2H6cxRKmMGGD9vm4TLmbXPdgTa
vD0VsrmOw9NoADq9DMvcZPosUTkBwtLbhu5wasOZfCmKj1Nu6NJb8/sx5vO+TLIW
nE6YVNUBoMklSOxFCILXpbDkVZ36cnbhHE9FfJxTMOos9T74CTRxZd04vK5luJ+n
374zcOlSVHkjrRif3y90URI5q2Q0JXWWkw4MbC7at4cqneoYxz47ecAJM0tBjoLe
ZH87YrYdCG3VQXKPW5MJLGWCy+W7GVrqI0WviOX1uNNtbpUH4vWwqyfMWLkQIDWq
IMbvKBdx03/Iou+JXFXgLfdb22J0nVI8vYovPhf7srd8FLhmWb815j4Vhp4ISzIY
QWn9NIQ36LkCDQRY/4d2ARAAyBtT/AmXZg1BuAXdqZsqKkDGAmUYoAQBi3cFT8za
5WiTDUHAXBa40s1QZAJXlMXRtU1z6zpyxDBA1aUddL8rxTbQXKMg1T31FlOeAiiL
KJeqQFAGpV7GjImAj4TpY/EdL8lSeD274lBysk3EKc8cwgcujLUdZ6cAu6eNMFmr
+YV3TdOmIdskkzN+LXWOJXmRu9+I5xyiLx/pT1Q9uFKJAgZ9zmnK0gVqidj9W624
BPi5xkke8OnylwjWxXYxawxdYqyMPVomCU0Py77c247YlQwH7UgDlyUk30N2ntXn
Wa01HOQS3f9JP9xljbas22NAU342hCriwrAVPhv5c84VG0biePh9l0LqNJ6u3hpc
Dpt1zlt0Y+CDqae1t8zAzmno5AU9oAGKJHj5vT8pIFs16WNvXvnld2l7T781HdJz
Ql/rh8RXlmzgdBmKkfcHCoFlFJus7q5RDGZCWwQJ+5QGtaWMwINyzU2mkOvo9b8B
LkTNW5h2JN1G7UZqULIcmpSXYqhYFJCfE4R/ruDKXMqIsr78HM755SKgdxARKQLE
8KJaSUDUKYCxl+olO8mT+YTCNIsG+PCWAUwFuXaSz3I+54lMFq0TZqZBcMFJwHs7
iMvchFtNHD8CRvBvkxc/yTbXRcfK7JoXfx5/xz+tsR9Yzuq4uIpuQGE91ePXSdK4
mDMAEQEAAYkCJQQYAQIADwUCWP+HdgIbDAUJCWYBgAAKCRDbD7emNKVVzUsSEAC0
MmxwPapz47oAizVch9qye6gty65D5QRwSp099Bi4MfwAMPomi3aZouP4pR1q+9wR
FV6mGXbRL2bG53Dte34X+v8aLl0WS18BAfLtO234khP+iM1pDuxeAzq1v79qF8kb
PI+I038nt16CQ2dYRSnQBtTI2e4iCJA3FsXEdJ+cdFc0BLZYxQEIJDyT7C4N8XSG
g1qzKG+EN/dOfvlmTb9sKsLCYtRJP4FF+T92biTu0JRpPMuU+qlxI52ThGhyVbTm
seC3U2iCOtotjiYKpOY2g8cn2DBvVETCwrMQVfzfL0YmJrCwzF6Fu9FG5O//dS5K
bg9guKJg2xdohANYvmGn8p0ic8hH5Tqj+hYvTLKDGG933CiBRWTPFrt2unDLTDGN
f0KXj7eCvxaRV/ZRC7ZogtdJK6iJXawHg+bubyXhgyRmd1P2yBOtEMVua5RCH+rT
1zZI7zslm5lCAaemox1kKTsyxVUxo2zuoidCX7OS56fkIRcrK8hvXoqdPk7lBnHE
+0IWqb0+HaWPD0DffMeTE0Ks8/uOil7y3IynjYZkWM05oucOreweHlhdnTvEuAOJ
5BUSk4lIbAJfMjhISvM58r95NAeb81KuTf0xtilZOVc1NVVAYalEHgs3naGp4q69
CiL3UAvo3NJaQLuoJEs5Nay3BeSSTQdhtGYhEg+ZKQ==
=jTnO
-----END PGP PUBLIC KEY BLOCK-----
```