---
id: '508410816574923'
title: Leiharbeit und Befristung abschafften - Spaltungen überwinden
start: '2019-09-28 10:00'
end: '2019-09-28 11:00'
locationName: null
address: 'Petersstraße 26, 04109 Leipzig'
link: 'https://www.facebook.com/events/508410816574923/'
image: 70092197_2549629178426897_7198652461278560256_n.jpg
teaser: '****Kundgebung gegen Leiharbeit und Befristung am 28.9.2019 vor dem Sitz der Leiharbeitsfirma Adecco (Petersstraße 26)*****  Leiharbeit und Befristung'
recurring: null
isCrawled: true
---
****Kundgebung gegen Leiharbeit und Befristung am 28.9.2019 vor dem Sitz der Leiharbeitsfirma Adecco (Petersstraße 26)*****

Leiharbeit und Befristung werden bei Amazon weltweit als Kampfmittel gegen die Beschäftigten eingesetzt. Einerseits um die Lohnkosten zu drücken und die Profite zu steigern, andererseits um die Belegschaften in Arbeiter*innen erster und zweiter Klasse zu spalten. Hiedruch will Amazon die betriebliche Herrschaft gegenüber den Arbeiter*innen absichern und die Macht der Arbeiter*innen schwächen. Adecco vermittelt in Ländern wie Spanien und Polen Arbeiter*innen unter prekären Bedingungen an Amazon und unterstützt damit die Strategie des Unternehmens. Hier gegen setzen wir uns zur Wehr!

Wir , "Amazing Workers – United accross borders“ - der transnationale Zusammenschluss von Amazon-Arbeiter*innen aus ganz Europa und den USA - rufen im Rahmen unseres Treffens in Leipzig dazu auf, vor dem Sitz der Leiharbeitsfirma Adecco mit Kolleg*innen ein Zeichen der Solidarität gegen Leiharbeit und Befristung zu setzen! 

Wir schließen damit an unseren letzten Besuch bei Adecco im Rahmen des transnationalen Arbeiter*innen-Treffens in Poznan im März 2019 an: https://vimeo.com/326854438 

Mehr Infos zu "Amazing Workers – United accross borders“: https://amworkers.wordpress.com/