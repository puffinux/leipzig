---
id: '328399494690762'
title: Deutsch-arabische Lesung des Interkulturellen Gartens
start: '2019-03-22 19:00'
end: '2019-03-22 20:30'
locationName: Kontaktstelle Wohnen
address: 'Georg-Schwarz-Straße 19, 04177 Leipzig'
link: 'https://www.facebook.com/events/328399494690762/'
image: 54044990_2280811178617677_1084710350135230464_n.jpg
teaser: Zum zweiten Mal in Folge veranstaltet der Interkulturelle Garten der ANNALINDE anlässlich der Internationalen Wochen gegen Rassismus Leipzig 2019 sowi
recurring: null
isCrawled: true
---
Zum zweiten Mal in Folge veranstaltet der Interkulturelle Garten der ANNALINDE anlässlich der Internationalen Wochen gegen Rassismus Leipzig 2019 sowie der Leipziger Buchmesse eine mehrsprachige Lesung in der Georg-Schwarz-Straße 19. Es werden ausgewählte arabischsprachige Texte sowie deren deutsche Übersetzung vorgetragen. Dazu gibt es schöne Klänge und im Anschluss die Möglichkeit zu regem Austausch über die gelesenen Gedichte und arabische Lyrik im Allgemeinen. Der Eintritt ist frei!

———————————
Das Projekt „Interkultureller Garten“ wird von September 2016 bis August 2019 durch die Europäische Union mit Mitteln aus dem Europäischen Sozialfond finanziert.