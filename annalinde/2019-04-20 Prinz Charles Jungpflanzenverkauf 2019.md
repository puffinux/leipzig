---
id: '264928121075236'
title: Prinz Charles Jungpflanzenverkauf 2019
start: '2019-04-20 13:00'
end: '2019-04-20 19:00'
locationName: null
address: Annalinde Leipzig
link: 'https://www.facebook.com/events/264928121075236/'
image: 53327459_2272632422768886_5398626724017602560_n.jpg
teaser: 'Wie die vergangenen Jahre könnt ihr Euch im April und Mai mit allem eindecken, was an Pflanzen für Balkon, Garten oder Küche geeignet ist. Von Aubergi'
recurring: null
isCrawled: true
---
Wie die vergangenen Jahre könnt ihr Euch im April und Mai mit allem eindecken, was an Pflanzen für Balkon, Garten oder Küche geeignet ist. Von Aubergine bis Zucchini, über Zierpflanzen bis hin zu diversen Küchenkräutern bieten wir wieder ein breites und buntes Angebot.
Zu fairen Preisen und zu 100 % selbst gezogen aus sortenechtem Saatgut!

Eine detaillierte Sortenliste wird in Kürze veröffentlicht. 