---
id: '292874864944555'
title: 'Gemüsesprechstunde: Die Gärtnerei im April'
start: '2019-04-04 17:00'
end: '2019-04-04 18:00'
locationName: null
address: Annalinde Leipzig
link: 'https://www.facebook.com/events/292874864944555/'
image: 55496012_2301009216597873_4661287702375694336_n.jpg
teaser: 'Der Gemüsegarten im April: Wir wagen uns an die ersten Freiland-Aussaaten und im Gewächshaus wird alles für den Jungpflanzenverkauf vorbereitet. Unser'
recurring: null
isCrawled: true
---
Der Gemüsegarten im April: Wir wagen uns an die ersten Freiland-Aussaaten und im Gewächshaus wird alles für den Jungpflanzenverkauf vorbereitet. Unsere Gärtner*innen können nach den frostigen Monaten endlich wieder auf die Äcker, denn die müssen jetzt für die Saison vorbereitet werden. Und kann man jetzt eigentlich schon Salat ernten? Kommt vorbei, schaut uns über die Schulter und löchert uns mit euren Fragen.

Kostenfrei (Spenden willkommen)

Mit freundlicher Unterstützung der Sächsischen Landesstiftung Natur und Umwelt, Akademie.