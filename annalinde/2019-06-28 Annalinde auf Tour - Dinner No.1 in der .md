---
id: '2738110442870196'
title: Annalinde auf Tour - Dinner No.1 in der Villa Hasenholz
start: '2019-06-28 18:30'
end: '2019-06-28 23:00'
locationName: Villa Hasenholz
address: 'Gustav-Esche-Str. 1, 04179 Leipzig'
link: 'https://www.facebook.com/events/2738110442870196/'
image: 60603900_2375292649169529_1304934836892336128_n.jpg
teaser: 'Auch diese Saison werden wir wieder gemeinsam mit Pepe''s Kitchen drei Gartendinner in den Monaten Juni, Juli und August auf die Beine stellen  Erster'
recurring: null
isCrawled: true
---
Auch diese Saison werden wir wieder gemeinsam mit Pepe's Kitchen drei Gartendinner in den Monaten Juni, Juli und August auf die Beine stellen

Erster halt: Villa Hasenholz in Leipzig-Leutzsch.

Wir servieren Euch in Gartenathmosphäre mehrere Gänge der besten Zutaten frisch aus unseren Gärtnereien.

Menü:

Vorspeise

Linsen mit Pumpernickelmiso
Spitzkohl und Rapskernölemulsion - Estragon
Süße Zwiebelcreme und Mairübchen


Hauptgang
Käse aus dem Elstertal mit Haselnusscrumble
Honiggebackene Jungkarotten mit Salbei
Kartoffeln mit Radieschensenf und Salat

Dessert
Holundersorbet mit Himbeeren und Verbenejoghurt
Honigwaffel mit Lavendel

Beim diesjährigen Dinner in der Villa Hasenholz steht das an unseren Standorten produzierte Gemüse im absoluten Fokus. Daher werden wir diesmal mit Pepe's Kitchen ein komplett vegetarisches Menü kreieren. Ihr dürft gespannt sein.  

Preis 33,- EUR p.P.

Weitere Termine 2019:

26. Juli - tba
23. August - Hostel Eden

Anmeldungen bereits möglich:

Verbindliche Anmeldung mit (einen) Name, Anzahl Personen, Adresse und Tel.Nr. bitte an dinner@annalinde-leipzig.de

Aufgrund schlechter Reservierungsmoral das letzte Jahr: Stornierung bis Montag vor Veranstaltung kostenlos. Im Anschluss berechnen wir den vollen Preis.