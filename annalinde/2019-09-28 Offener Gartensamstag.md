---
id: '2416470568565258'
title: Offener Gartensamstag
start: '2019-09-28 15:00'
end: '2019-09-28 19:00'
locationName: null
address: 'Zschochersche Straße 12, 04229 Leipzig'
link: 'https://www.facebook.com/events/2416470568565258/'
image: 70346110_2584634321568693_8825919242206445568_n.jpg
teaser: 'Die Gartensamstage sind zurück! Wir laden Euch während der goldenen Jahreszeit ein, euren Samstagnachmittag mit uns im ANNALINDE Gemeinschaftsgarten z'
recurring: null
isCrawled: true
---
Die Gartensamstage sind zurück! Wir laden Euch während der goldenen Jahreszeit ein, euren Samstagnachmittag mit uns im ANNALINDE Gemeinschaftsgarten zu verbringen. 

Werkelt mit den Gemeinschaftsgärtner*innen nach der großen Sommerhitze was das Zeug hält, entspannt im Garten zwischen Tomaten, Auberginen und frisch ausgesäten Herbstkulturen oder genießt Feines aus dem Gartenkiosk.

Kommt einfach vorbei – wir freuen uns auf die gemeinsamen Nachmittage im Garten!

____________

Zeit: 15-19 Uhr

Ort: ANNALINDE Gemeinschaftsgarten, Zschochersche Str. 12