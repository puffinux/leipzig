---
id: '824389171272825'
title: Frühlingsfest des Interkulturellen Gartens & offene Jam-Session
start: '2019-05-08 13:00'
end: '2019-05-08 20:00'
locationName: null
address: Annalinde Leipzig
link: 'https://www.facebook.com/events/824389171272825/'
image: 56857733_2322518594446935_3721257221141561344_n.jpg
teaser: 'Lasst uns im Mai tanzen, Frühling und Blüten gebührend feiern – bei einem bunten Frühlingsfest mit Musik.  Wir freuen uns sehr, dieses Jahr die dritte'
recurring: null
isCrawled: true
---
Lasst uns im Mai tanzen, Frühling und Blüten gebührend feiern – bei einem bunten Frühlingsfest mit Musik.

Wir freuen uns sehr, dieses Jahr die dritte Saison des Interkulturellen Gartens mit einer offenen Jam-Session zu feiern. Packt eure Instrumente ein und lasst sie zwischen unseren Hochbeeten erklingen! Wie es sich für einen lauen Frühlingsabend gehört, entfachen wir am Abend natürlich ein Lagerfeuer – und wo lässt es sich besser jammen als zwischen duftenden und vom Feuerschein erleuchteten Kräutern, Ringelbumen und Radieschen…?

—————————————
Das Projekt „Interkultureller Garten“ wird von September 2016 bis August 2019 durch die Europäsiche Union mit Mitteln aus dem Europäischen Sozialfond finanziert.