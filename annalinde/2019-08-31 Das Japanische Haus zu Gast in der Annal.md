---
id: '340501710192220'
title: Das Japanische Haus zu Gast in der Annalinde!
start: '2019-08-31 15:00'
end: '2019-08-31 22:00'
locationName: null
address: 'Zschochersche Straße 12, 04229 Leipzig'
link: 'https://www.facebook.com/events/340501710192220/'
image: 67643471_2522370284461764_2787203466072686592_n.jpg
teaser: Konnichiwa – nach 6 Jahren begrüßen wir Japan zum zweiten Mal in den sommerlichen Gefilden unseres Gemeinschaftsgartens. Frisch geerntetes Gemüse aus
recurring: null
isCrawled: true
---
Konnichiwa – nach 6 Jahren begrüßen wir Japan zum zweiten Mal in den sommerlichen Gefilden unseres Gemeinschaftsgartens. Frisch geerntetes Gemüse aus der ANNALINDE Gärtnerei wird live vor Ort zu japanischen Röllchen verarbeitet, dazu gibt es Musik vom Feinsten.

Wir feiern hiermit auch den Abschluss unseres ESF-Projektes „Interkultureller Garten“, das uns von September 2016 bis August 2019 viele wunderschöne Momente beschert hat. Danke an alle, die mit uns ihre Hände in die Erde gesteckt und uns unterstützt haben und vielen Dank natürlich auch an den Europäischen Sozialfond!

Künstlerische Performance/Konzert ab 18 Uhr:

Titel: 31082019
Israel Adriani (Trompete)
Anggana Putra Ciremay (Gitarre) 
Molham Sam (Darbuka)

Das Konzert wird multikulturell und unplugged improvisiert.

_______________________________________________________________
Das Projekt „Interkultureller Garten“ wird von September 2016 bis August 2018 durch die Europäsiche Union mit Mitteln aus dem Europäischen Sozialfond finanziert.