---
id: '703164173474126'
title: Kräuterfest
start: '2019-08-25 10:00'
end: '2019-08-25 22:00'
locationName: null
address: 'Zschochersche Straße 12, 04229 Leipzig'
link: 'https://www.facebook.com/events/703164173474126/'
image: 66461112_2478581128840680_8963495097761005568_n.jpg
teaser: 'Im ANNALINDE Gemeinschaftsgarten dreht sich alles um Kräuter mit Workshops, Vorträge, wild krautige Schlemmereien, Geschichten und Musik am Lagerfeuer'
recurring: null
isCrawled: true
---
Im ANNALINDE Gemeinschaftsgarten dreht sich alles um Kräuter mit Workshops, Vorträge, wild krautige Schlemmereien, Geschichten und Musik am Lagerfeuer.

Wildkräuterexpert*innen, Gärtner*innen, Künstler*innen, Köch*innen und Co. teilen ihr Wissen und den Spaß an Kräutern.

–––––––––––––

Zeit: Samstag 10-22 Uhr; Sonntag 10-18 Uhr

Ort: ANNALINDE Gemeinschaftsgarten, Zschochersche Str. 12

Eintritt frei – einige Angebote sind kostenpflichtig und mit Voranmeldung.
 