---
id: '311895786426740'
title: 'Gemüsesprechstunde: Die Gärtnerei im Juni'
start: '2019-06-06 17:00'
end: '2019-06-06 18:00'
locationName: null
address: Annalinde Leipzig
link: 'https://www.facebook.com/events/311895786426740/'
image: 60496827_2382093758489418_4645844498922864640_n.jpg
teaser: Im Juni geht auch in der ANNALINDE die Sonne kaum unter und die Pflanzen wachsen was das Zeug hält – wenn es denn ausnahmsweise mal regnet.  Kein Acke
recurring: null
isCrawled: true
---
Im Juni geht auch in der ANNALINDE die Sonne kaum unter und die Pflanzen wachsen was das Zeug hält – wenn es denn ausnahmsweise mal regnet.

Kein Acker steht jetzt noch leer und die Gärtner*innen der ANNALINDE „erziehen“ die Tomaten- und Gurkenpflanzen, damit sie viele große Früchte produzieren.

In den Abendstunden stößt man gern mal mit einem Junikäfer auf Hochzeitsflug zusammen.

Wir zeigen euch die ANNALINDE-Gärtnerei und sind wieder gespannt auf eure gärtnerischen Fragen.

 _______________________

Zeit: 17-18 Uhr

Ort: ANNALINDE Gärtnerei West, Lützner Str. 108

Kostenfrei (Spenden willkommen)

 
___________________________________________
Mit freundlicher Unterstützung der Sächsischen Landesstiftung Natur und Umwelt, Akademie