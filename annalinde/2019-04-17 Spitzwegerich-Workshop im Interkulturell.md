---
id: '794141380961645'
title: Spitzwegerich-Workshop im Interkulturellen Garten
start: '2019-04-17 16:00'
end: '2019-04-17 19:00'
locationName: null
address: Annalinde Leipzig
link: 'https://www.facebook.com/events/794141380961645/'
image: 56917742_2328778057154322_5107847164516630528_n.jpg
teaser: '"Wir freuen uns sehr, dass wir unsere Kräuterpädagogin Martina Milisavljevic im Rahmen des Projektes "Interkultureller Garten der ANNALINDE" für einen'
recurring: null
isCrawled: true
---
"Wir freuen uns sehr, dass wir unsere Kräuterpädagogin Martina Milisavljevic im Rahmen des Projektes "Interkultureller Garten der ANNALINDE" für einen kleinen aber feinen Kräuterworkshop rund um eines ihrer Lieblingskräuter - den Spitzwegerich - begeistern konnten.

Wir bereiten zusammen mit ihr einen leckeren Salat zu, erproben den Ernstfall eines Insektenstiches sowie den damit verbundenen Einsatz des Krautes und brauen einen hustenlindernden Saft. Damit auch Nicht-Muttersprachler*innen alles verstehen können, gehen wir an diesem Mittwoch nicht ganz so sehr in die Tiefe und es bleibt natürlich viel Raum für Vokabelfragen und Austausch! Also kommt gerne einfach vorbei und lasst euch vom Spitzwegerich verzaubern.

Wer sich intensiver und tiefgreifender mit Wildkräutern & gärtnerischen Themen beschäftigen möchte, findet viele spannende Veranstaltungen auf unserer Website (annalinde-leipzig.de) oder hier auf unserer Facebook-Seite: Annalinde Leipzig!

Nützliche Pflanzentipps und interessante Facts über unsere essbare Umwelt bekommt ihr außerdem auf Martinas Facebook-Seite: Forschen und Entdecken.

Der Workshop ist kostenlos, um Spenden für die Zutaten wird jedoch gebeten! Bitte bringt außerdem leere und heiß ausgespülte Marmeladengläser für den Hustensaft mit.

—————————————
Das Projekt „Interkultureller Garten“ wird von September 2016 bis August 2019 durch die Europäsiche Union mit Mitteln aus dem Europäischen Sozialfond finanziert.