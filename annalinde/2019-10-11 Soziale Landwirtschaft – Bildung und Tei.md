---
id: '2782695241742000'
title: Soziale Landwirtschaft – Bildung und Teilhabe am Arbeitsleben
start: '2019-10-11 09:30'
end: '2019-10-11 17:00'
locationName: null
address: Annalinde Leipzig
link: 'https://www.facebook.com/events/2782695241742000/'
image: 70556202_2622008647831260_2569525014453813248_n.jpg
teaser: 'Mit einem gemeinsamen Seminar wollen ASG und der Thüringer Ökoherz e.V. sich diesem Thema nähern. Das Seminar informiert Förderschulen, Landwirte, Bil'
recurring: null
isCrawled: true
---
Mit einem gemeinsamen Seminar wollen ASG und der Thüringer Ökoherz e.V. sich diesem Thema nähern. Das Seminar informiert Förderschulen, Landwirte, Bildungsträger und Arbeitsvermittler über den Aufbau, die  Durchführbarkeit und die Finanzierung und von stunden- oder tageweisen Angeboten zur Teilhabe für Menschen mit Förderbedarf auf landwirtschaftlichen oder gärtnerischen Betrieben. Es wird anschaulich dargelegt, wie Menschen mit Unterstützungsbedarf ein Einblick in das Berufsfeld Landwirtschaft/Gartenbau gegeben werden kann und wie Bildungs- und Teilhabeangebote auf entsprechenden Betrieben aussehen können. Das Seminar gibt Aufschluss darüber, wie den Menschen anforderungsgerecht ein Einblick zu den täglichen anfallenden Arbeiten in Gärtnerei oder auf einem Bauernhof und zur Herkunft und Produktion von dort erzeugten Lebensmitteln gegeben werden kann. Außerdem soll gezeigt werden, welche Zusammenarbeit zwischen allen Beteiligten nötig ist, um diese Bildungs- und Teilhabeangebote effektiv umzusetzen und wie diese Maßnahmen finanziert und begleitet werden können.

Zielgruppen: Gärtner, Landwirte, Förderschulen, Berufsbildungsträger, Arbeitsagenturen

Gebühr EUR 40,00 inkl Verpflegung

Anmeldung:
Marika Krüger
Thüringer Ökoherz e.V.
Tel.: 034341-992084 oder 01522-6969633
E-Mail: m.krueger@oekoherz.de

Mehr Infos: https://www.asg-goe.de/pdf/ASG-Seminar-Leipzig-2019.pdf