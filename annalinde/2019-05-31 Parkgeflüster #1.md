---
id: '325986658085942'
title: 'Parkgeflüster #1'
start: '2019-05-31 16:00'
end: '2019-05-31 20:00'
locationName: null
address: 'Mariannenpark, 04347 Leipzig'
link: 'https://www.facebook.com/events/325986658085942/'
image: 60347684_2382027675162693_3901034854115966976_n.jpg
teaser: 'Liebe Anwohner*innen, liebe Akteure aus Schönefeld und rund um den Mariannenpark,  wir laden euch im Rahmen des Projektes KoopLab (www.kooplab.de) seh'
recurring: null
isCrawled: true
---
Liebe Anwohner*innen, liebe Akteure aus Schönefeld und rund um den Mariannenpark,

wir laden euch im Rahmen des Projektes KoopLab (www.kooplab.de) sehr herzlich zu unserer kommenden Veranstaltung im Mariannenpark ein:

Parkgeflüster #1
Der Mariannenpark gestern, heute und morgen

31. Mai 2019 von 16 bis 20 Uhr
 im Eingangsbereich des Mariannenparks (Rohrteichstraße, Ecke Schönefelder Allee)

Was passiert dort?

Wir wollen mit Euch über den Mariannenpark ins Gespräch kommen! Dabei soll es z.B. darum gehen, wie er einst errichtet wurde und wie wir ihn heute nutzen wollen. Der Mariannenpark hat eine spannende Geschichte, es gibt einiges zu erfahren und wir wollen den Park gemeinsam weiter entdecken!

Zu den Programmpunkten gehören unter anderem:
          16 Uhr: Kinder entdecken den Park
·         17 Uhr:  gemeinsamer Parkspaziergang
·         durchgehend: Infowand zur Geschichte des Mariannenparks, Pflanzenaktionen sowie Einladung zu Gesprächen und Austausch an verschiedenen Themenstationen

Wir sind gespannt auf Eure Geschichten, Erfahrungen und Ideen! Leitet diese Informationen sehr gern an Eure Freunde und Bekannten weiter und verbreitet sie in Euren Netzwerken.

Zum vormerken: Parkgeflüster #2 wird am 6. Juli 2019 ab 10 Uhr stattfinden. Dabei wird es wieder verschiedene Programmpunkte sowie ein kleines Mitbring-Buffet geben und Vereine und Einrichtungen aus der Nachbarschaft haben die Möglichkeit, über ihre Arbeit zu informieren. Weitere Informationen folgen!

Wenn Ihr Fragen oder Anregungen habt, wendet Euch gern an uns!

(Falls es regnet oder stürmt, muss die Veranstaltung leider ausfallen.)

das Leipziger KoopLab-Team
leipzig@kooplab.de

Parkgeflüster #1 ist eine gemeinsame Veranstaltung von ANNALINDE gGmbH und dem Helmholtz-Zentrum für Umweltforschung GmbH (UFZ) im Rahmen des Projekts "KoopLab: KoopLab: Teilhabe durch kooperative Freiraumentwicklung in Ankunftsquartieren"
Mehr Infos: www.kooplab.de
