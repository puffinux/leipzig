---
id: '333831504001309'
title: Bright Monday - ALLES MUSS RAUS!
start: '2019-05-20 15:00'
end: '2019-05-20 19:00'
locationName: null
address: Annalinde Leipzig
link: 'https://www.facebook.com/events/333831504001309/'
image: 60624189_2385896354775825_7700115375127527424_n.jpg
teaser: Am Montag öffnen wir zum wirklich letzten Mal die Pforten für den Jungpflanzenverkauf. 50 % auf Alle Jungpflanzen!  Don`t miss!
recurring: null
isCrawled: true
---
Am Montag öffnen wir zum wirklich letzten Mal die Pforten für den Jungpflanzenverkauf.
50 % auf Alle Jungpflanzen!

Don`t miss!