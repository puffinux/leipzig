---
id: '733980600378252'
title: Leipzig von morgen - gemeinsam nachhaltig gestalten
start: '2019-09-25 14:00'
end: '2019-09-29 22:00'
locationName: heiter bis wolkig
address: 'Röckener Straße 44, 04229 Leipzig'
link: 'https://www.facebook.com/events/733980600378252/'
image: 70523541_2457660264515829_6075284798434181120_n.jpg
teaser: '----------------------------------------------------------------------------------------       Willst du direkt zum Programm?: https://kulturjurte-lei'
recurring: null
isCrawled: true
---
----------------------------------------------------------------------------------------      
Willst du direkt zum Programm?: https://kulturjurte-leipzig.de/?p=579
◢ ◣◥ ◤◢ ◣◥ ◤◢ ◣◥ ◤◢ ◣◥ ◤◢ ◣◥ ◤◢ ◣◥ ◤◢ ◣◥ ◤◢ ◣◥ ◤

▨  WIE wollen wir leben? 
▨  WIE können wir Leipzig gemeinsam nachhaltig gestalten? 
▨  WAS kann ich als Einzelner dazu beitragen? 
▨  WO kann ich mich engagieren? 
▨  WAS fehlt noch in unserer Stadt? 
▨  WAS ist alles schon da?

Mit diesen und weiteren Fragen wollen wir uns GEMEINSAM auseinandersetzen und laden dich herzlich zu unserer Bildungs- Vernetzungs- & Aktionswoche ein!

» Wir wollen in dieser Woche einen Raum schaffen zum Lernen, Impulse geben und aufnehmen, sich austauschen und Kontakte knüpfen, inspirieren, aktiv werden, genießen und visionieren!

» Das alles rund um die zentralen Themen Nachhaltigkeit & soziales Miteinander.

◢ ◣◥ ◤◢ ◣◥ ◤◢ ◣◥ ◤◢ ◣◥ ◤◢ ◣◥ ◤◢ ◣◥ ◤◢ ◣◥ ◤◢ ◣◥ ◤

¡ Eintritt frei  //\\  Spende erwünscht !
﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍
BILDUNG & AKTION
——————————————————————————————
Euch erwarten Workshops, Vorträge, Diskussionen & Aktionen von und mit u. a. Konzeptwerk Neue Ökonomie Geräuschkulisse Hildegarten Leipzig Prozesswerkstatt Freiwilligen-Agentur Leipzig Cradle to Cradle e.V. von morgen Cleanup Leipzig,
[Hilo.cafe](https://hilo.cafe), Einfach Unverpackt ....und weiteren spannenden Referent*innen. 
﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍
VERNETZUNG
——————————————————————————————
Bringt Flyer von gemeinwohlorientierten Projekten oder eurer Initative mit, denn die leipziger Projektlandschaft wird in einer interaktiven Installation über die Tage hinweg - mit EURER HILFE - sichtbar gemacht werden.
Auf der Onlineplattform Karte von morgen werden diese digitalisiert und dann zu einer ausgedruckten Karte "Leipzig im Wandel" werden.

Am SAMSTAG 28.09. 10 - 22 Uhr gibt es ein ●●Leipzig von morgen - Vernetzungsfest●● mit Workshop, Markt der Möglichkeiten und
OpenSpace zum Synergien schaffen und Ideen weiter spinnen.

Du kannst dich auch jetzt schon mit deiner Initiative auf der Karte von morgen eintragen!
﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍
MUSIK
————————————————————————————
...gibt's abends von Tatjana Milia , Elisabeth Weems, Handpan von Nicolas, Klezmer von Klezwerk und Impromusik vom Alles Impro Kollektiv!
﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍﹍
OPEN SPACE
————————————————————————————
Du möchtest dich inhaltlich noch kurzfristig mit einbringen oder hast Kontakte und Ideen zu spezifischen Themen oder Initiativen?
dann melde dich bitte bei uns und wir Schauen wie wir deinen Impuls integrieren können! - schreib uns doch an: kulturjurteleipzig[at]posteo.de

»»»» HIER ««««««««««««««««««««««««««««««««««««««
findest du das gesamte Programm: 
https://kulturjurte-leipzig.de/?p=545

MACH MIT !
Es gibt viele Möglichkeiten sich zu beteiligen:

    ♦ Teile den Link vom Event mit Freunden und interessanten Gruppen
    ♦ Bring Infomaterial von interessanten Projekten mit
    ♦ Stelle eine Initiative auf dem Markt der Möglichkeiten vor
    ♦ schau ins Programm
    ♦ komm vorbei und bringe ein paar Freunde mit
    ♦ Trag deine Initative online auf der Karte von morgen ein!

// Beim  Café kannst du dich mit heißen & kalten Getränken sowie frisch gebackenen Kuchen eindecken. Außerdem gibt es jeden morgen zwischen 12 - 14 Uhr ein MIBRING-BRUNCH. Am Samstag und Sonntag jeweils von 11 - 13 Uhr.  

// Der benachbarte Bauspielplatz WILDER WESTEN lädt Kinder ab 6 Jahre ein, sich kreativ auszutoben (bis 6 Jahre nur in Begleitung, Mittwoch nur ohne Begleitung, Sonntags geschlossen).

// Es gibt eine barrierefreie Kompost-Toilette, sonst ist der Boden eher uneben. In der Jurte gibt es Sitzkissen, weniger Stühle. Sprich uns von der Kulturjurte Leipzig bei Fragen gerne an! 

Wir freuen uns auf DICH!
Deine Jurties ❤

--> Du möchtest weiterhin Infos über die Projekte der Kultujurte Leipzig erhalten? Dann trag dich einfach HIER auf unserem Newsletter ein: https://kulturjurte-leipzig.de/?page_id=27

-------------------------
Tram & Bus:
Tram 1 + 2 Antonien-/Gießerstraße
Bus 60 Siemensstraße
Tram 14 (oder auch S-Bahn) Plagwitzer Bahnhof
Tram 3 Adler