---
id: '2001522429903123'
title: Zeichenkurs mit Anas im Wintertreff des Interkulturellen Gartens
start: '2019-02-21 17:00'
end: '2019-02-21 19:30'
locationName: Annalinde Leipzig
address: 'Lützner Straße 108, 04177 Leipzig'
link: 'https://www.facebook.com/events/2001522429903123/'
image: 52401086_2251126381586157_938231869189652480_n.jpg
teaser: 'Heute ist es wieder so weit: Anas gibt seine Zeichenkünste an uns weiter! Nachdem wir uns im letzten Winter mit alten Tontöpfen aus der Gärtnerei dem'
recurring: null
isCrawled: true
---
Heute ist es wieder so weit: Anas gibt seine Zeichenkünste an uns weiter! Nachdem wir uns im letzten Winter mit alten Tontöpfen aus der Gärtnerei dem Thema „Schatten“ gewidmet hatten, kommen uns dieses mal Gartenkräuter unter die Bleistifte.

Alle sind Willkommen, Vorkenntnisse sind nicht notwendig und für Papier & Stift sorgen wir – bringt aber auch gerne euch eigenen Zeichenutensilien mit!

Uhrzeit: 17-19:30
Ort: Kontaktstelle Wohnen, Georg-Schwarz-Straße 19

———————————
Das Projekt „Interkultureller Garten“ wird von September 2016 bis August 2019 durch die Europäische Union mit Mitteln aus dem Europäischen Sozialfond finanziert.