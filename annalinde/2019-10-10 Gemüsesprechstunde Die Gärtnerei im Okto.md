---
id: '760681337674609'
title: 'Gemüsesprechstunde: Die Gärtnerei im Oktober'
start: '2019-10-10 17:00'
end: '2019-10-10 18:00'
locationName: null
address: Annalinde Leipzig
link: 'https://www.facebook.com/events/760681337674609/'
image: 67337160_2501806649851461_4734551666827722752_n.jpg
teaser: 'Im Oktober ist die Gartensaison noch lange nicht zu Ende. Für Aussaaten ist es zwar nun zu spät, aber jetzt werden die Lager für den Winter gefüllt:'
recurring: null
isCrawled: true
---
Im Oktober ist die Gartensaison noch lange nicht zu Ende. Für Aussaaten ist es zwar nun zu spät, aber jetzt werden die Lager für den Winter gefüllt:

Rote Bete, Spätmöhren, Chinakohl und Endiviensalate haben jetzt Saison. Einige Gemüsekulturen warten auf die ersten Nachtfröste: Grün- und Rosenkohl mögen es so richtig kalt. Und der Knoblauch wird schon für die nächste Saison gesteckt.

Zum letzten Mal in diesem Jahr führen wir euch durch die ANNALINDE-Gärtnerei und freuen uns wie immer auf eure gärtnerischen Fragen.

_____________ 

Zeit: 17-18 Uhr
Ort: ANNALINDE Gärtnerei West, Lützner Str. 108
Kostenfrei (Spenden willkommen)

_____________ 
Mit freundlicher Unterstützung der Sächsischen Landesstiftung Natur und Umwelt, Akademie