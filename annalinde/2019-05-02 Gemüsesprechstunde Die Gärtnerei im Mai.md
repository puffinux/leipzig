---
id: '295673378025394'
title: 'Gemüsesprechstunde: Die Gärtnerei im Mai'
start: '2019-05-02 17:00'
end: '2019-05-02 18:00'
locationName: null
address: Annalinde Leipzig
link: 'https://www.facebook.com/events/295673378025394/'
image: 59229755_2356501221048672_6788552761765003264_n.jpg
teaser: '... heute findet unsere 2. Gemüsesprechstunde im Rahmen des "Stadtlandwirtschaftlichen Kursus" statt - der Eintritt ist frei, Spenden sind Willkommen:'
recurring: null
isCrawled: true
---
... heute findet unsere 2. Gemüsesprechstunde im Rahmen des "Stadtlandwirtschaftlichen Kursus" statt - der Eintritt ist frei, Spenden sind Willkommen: 

Tomaten, Paprika, Kürbis – alles muss raus. Aber Vorsicht: Die Eisheiligen haben schon so manche ungeduldige Gärtner*innen bestraft. Was sonst noch so in unserer Gärtnerei im Mai passiert, erfahrt ihr auf unserem monatlichen Rundgang. Kommt vorbei und bringt eure Fragen und Anregungen mit.

Die Gemüsesprechstunde ist ein regelmäßig stattfindender Termin jeden ersten Donnerstag im Monat! 

Parallel dazu hat  unser Jungpflanzenverkauf Prince Charles im Gewächshaus der Gärtnerei geöffnet, bei dem ihr euch nach den jahreszeitlichen Gärtner-Tips gleich mit den passenden Jungpflanzen eindecken könnt!

____________________________________________
Mit freundlicher Unterstützung der Sächsischen Landesstiftung Natur und Umwelt, Akademie