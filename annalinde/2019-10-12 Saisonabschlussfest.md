---
id: '2071642623132060'
title: Saisonabschlussfest
start: '2019-10-12 14:00'
end: '2019-10-12 22:00'
locationName: null
address: 'Zschochersche Straße 12, 04229 Leipzig'
link: 'https://www.facebook.com/events/2071642623132060/'
image: 66461109_2478690008829792_1286545633766801408_n.jpg
teaser: 'Rückblickend auf eine wunderbare, gemeinsame Gartensaison, feiern wir mit euch:  „Auf die gute Ernte, die Leute und den tollen Ort.“  Unser Garten-Kio'
recurring: null
isCrawled: true
---
Rückblickend auf eine wunderbare, gemeinsame Gartensaison, feiern wir mit euch:

„Auf die gute Ernte, die Leute und den tollen Ort.“

Unser Garten-Kiosk ist geöffnet. Am Nachmittag habt ihr die Gelegenheit eure eigenen Pflanzenpostkarten zu drucken und in den Abend hinein wird getanzt. Unsere Bühne freut sich schon auf ihre musikalischen Gäste und wir freuen uns auf euch.

––––––––––––– 

Zeit: 14-22 Uhr

Ort: ANNALINDE Gemeinschaftsgarten, Zschochersche Str. 12 

Kostenfrei, kommt vorbei!