---
id: '523623444844936'
title: Annalinde auf Tour - Dinner No. 3 im Hostel Eden
start: '2019-08-23 19:00'
end: '2019-08-23 23:30'
locationName: null
address: Annalinde Leipzig
link: 'https://www.facebook.com/events/523623444844936/'
image: 66723001_2486640104701449_5463658584782929920_n.jpg
teaser: 'Dritter Halt unseres Gartendinners mit Pepe''s Kitchen: das in direkter Nachbarschaft gelegene Hostel Eden.  Wir servieren Euch in Gartenathmosphäre me'
recurring: null
isCrawled: true
---
Dritter Halt unseres Gartendinners mit Pepe's Kitchen: das in direkter Nachbarschaft gelegene Hostel Eden.

Wir servieren Euch in Gartenathmosphäre mehrere Gänge der besten Zutaten frisch aus unseren Gärtnereien.

Seit der Gründung von ANNALINDE 2011 an spielt Kulinarik eine zentrale Rolle. Das Ergebnis jahrelangen Ausprobierens, Neuentdeckens und Dazulernens lässt sich an diesem Abend auf dem Teller erleben.
Im Winter geplant, Am Vortag geerntet.

Das Team von ANNALINDE wünscht

Guten Appetit 

Menü:
tba


Einlass: 19:00 Uhr

Start:    19:30 Uhr

Preis 33,- EUR p.P.

Anmeldungen bereits möglich:

Verbindliche Anmeldung mit (einen) Name, Anzahl Personen, Adresse und Tel.Nr. bitte an dinner@annalinde-leipzig.de

Aufgrund schlechter Reservierungsmoral das letzte Jahr: Stornierung bis Montag vor Veranstaltung kostenlos. Im Anschluss berechnen wir den vollen Preis.