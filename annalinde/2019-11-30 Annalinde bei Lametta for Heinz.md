---
id: "789250491535568"
title: Annalinde bei "Lametta for Heinz"
start: 2019-11-30 11:00
end: 2019-11-30 20:00
locationName: Zum Wilden Heinz
address: Hähnelstraße 22, 04177 Leipzig
link: https://www.facebook.com/events/789250491535568/
image: 77066435_2758761650822625_7280155263926009856_n.jpg
teaser: 'Auch dieses Jahr werden wir für einen Tag beim Weihnachtsmarkt des "Wilden
  Heinz" anzutreffen sein.  Mit im Gepäck: Marmeladen, Honig, Kimchi, Chilipa'
isCrawled: true
---
Auch dieses Jahr werden wir für einen Tag beim Weihnachtsmarkt des "Wilden Heinz" anzutreffen sein.

Mit im Gepäck:
Marmeladen, Honig, Kimchi, Chilipasten, Kräuter de Lindenau, diverse Tees

Der konservierte Sommer - das perfekte Geschenk. 

Annalinde bei "Lametta for Heinz"
Samstag | 30.11.19 | 11:00 bis 20:00