---
id: '2116174225355757'
title: Annalinde Spezial in der Dankbar
start: '2019-10-12 19:00'
end: '2019-10-12 23:00'
locationName: Dankbar Kaffee Leipzig
address: 'Jahnallee 23, 04109 Leipzig'
link: 'https://www.facebook.com/events/2116174225355757/'
image: 70770265_2612082595490532_3770182486395453440_n.jpg
teaser: Wir laden ein zu einem ganz besonderen Event.  Seit der Gründung von ANNALINDE 2011 spielt Kulinarik im Gartenprojekt eine zentrale Rolle. Dies haben
recurring: null
isCrawled: true
---
Wir laden ein zu einem ganz besonderen Event.

Seit der Gründung von ANNALINDE 2011 spielt Kulinarik im Gartenprojekt eine zentrale Rolle. Dies haben wir auch dieses Jahr mit drei Gartendinnern unter Beweis stellen dürfen. Zum Abschluß der Saison serviert Felix Bielmeier ein konsequent regionales Menü mit Zutaten aus:

84% ANNALINDE 15% DIAKONIE PANITZSCH 1% HALLENSER SALINE

Fermentiertes als Amuse
-
Rote Bete Eistee mit Zitronenverbene 
-
Pastinake geschmort, Karotten, Birnen, Radieschen, gehobeltes Eigelb
-
Gefüllte Zwiebel mit AnnaBelle und Zierquitte 
-
Kaninchen mit Sellerie, Apfel, Knoblauchcreme und griechischem Bergtee
-
Glasierte Schupfnudeln mit Karotte 

Besonders ist diesmal, dass wir separat eine Naturweinbegleitung korrespondierend zu der puristischen Regionalküche von Felix Bielmeier anbieten.
Gastwinzer an diesem Abend ist Alexandre Dupont de Ligonnès. Der gebürtige Pariser hat in Dresden Wurzeln geschlagen und bringt naturbelassene Weine aus Dresden-Wachwitz und dem Radebeuler Niederlößnitz in die Flasche. Dabei verzichtet er auf Reinzuchthefen, Enzyme, Schönungsmittel und Filtration, Herbizide, Fungizide sowie Pestizide - also alle Tricks und Kniffe, die im konventionellen Weinbau alltäglich geworden sind. Ausgebaut werden die Weine im Holzfass, Tonei oder Edelstahltank - je nach Sorte. Organisiert wurde dies durch unseren gemeinsamen Freund Jens Hugel.

Die Teams von ANNALINDE, DANKBAR & dem WEINKOMBINAT HUGEL wünschen

Guten Appetit 

Einlass: 19:00 Uhr

Start:    19:30 Uhr

Preis für 5-Gang-Menü 44,- EUR p.P.

Preis für Weinbegleitung 22,- EUR p.P.

Anmeldungen bereits möglich:

Verbindliche Anmeldung mit (einen) Name, Anzahl Personen, Adresse und Tel.Nr. bitte an dinner@annalinde-leipzig.de


Stornierung bis Montag vor Veranstaltung kostenlos. Im Anschluss berechnen wir den vollen Preis.

Foto: Martin Neuhof