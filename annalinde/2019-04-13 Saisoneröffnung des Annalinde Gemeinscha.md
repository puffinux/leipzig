---
id: '1289779107826643'
title: Saisoneröffnung des Annalinde Gemeinschaftsgartens
start: '2019-04-13 15:00'
end: '2019-04-13 22:00'
locationName: null
address: Annalinde Leipzig
link: 'https://www.facebook.com/events/1289779107826643/'
image: 56451925_2319372424761552_1900399840228016128_n.jpg
teaser: 'Die ersten Pflanzen sitzen schon in den Startlöchern, die Beete stecken voller Energie...  Während am kommenden Samstag parallel unser Jungpflanzenver'
recurring: null
isCrawled: true
---
Die ersten Pflanzen sitzen schon in den Startlöchern, die Beete stecken voller Energie...

Während am kommenden Samstag parallel unser Jungpflanzenverkauf "Prince Charles" in der Gärtnerei in die nächste Runde startet, eröffnen wir im ANNALINDE Gemeinschaftsgarten zum neunten Mal feierlich die nächste Gartensaison! 

Wir laden euch zu Musik und Beisammensein zwischen unseren Hochbeeten ein. 