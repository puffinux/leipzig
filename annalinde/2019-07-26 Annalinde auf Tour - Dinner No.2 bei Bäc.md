---
id: '342213716683051'
title: Annalinde auf Tour - Dinner No.2 bei Bäckerei Backstein
start: '2019-07-26 19:00'
end: '2019-07-26 23:30'
locationName: Backstein - Bäckerei für zeitgenössisches Brot
address: 'Grassistr. 4, 04107 Leipzig'
link: 'https://www.facebook.com/events/342213716683051/'
image: 64757661_2446099578755502_2924151633631772672_n.jpg
teaser: 'Zweiter Halt unseres Gartendinners mit Pepe''s Kitchen: Der Garten des Backstein - Bäckerei für zeitgenössisches Brot direkt neben der Galerie für Zeit'
recurring: null
isCrawled: true
---
Zweiter Halt unseres Gartendinners mit Pepe's Kitchen: Der Garten des Backstein - Bäckerei für zeitgenössisches Brot direkt neben der Galerie für Zeitgenössische Kunst Leipzig

ANNALINDE ist ein urbanes Landwirtschaftsprojekt welches seit 2011 existiert. Begonnen hat alles mit einer Brachflächenzwischennutzung im Leipziger Westen. 
Mittlerweile sind wir zu einer gemeinnützigen GmbH gewachsen, betreiben 2 Gärtnereien, einen Gemeinschafts- und einen Obstgarten, ist in Forschungsprojekten aktiv, anerkannter Ausbildungsbetrieb, Ort für interkulturellen Austausch, Wissenstransfer und sozialer Arbeit. All dies vor dem Hintergrund ökologischer, nachhaltiger und sozialer Aspekte. 

Von Beginn an spielte Kulinarik eine zentrale Rolle. Das Ergebnis jahrelangen Ausprobierens, Neuentdeckens und Dazulernens lässt sich an diesem Abend auf Ihrem Teller erleben.
Im Winter geplant, gestern geerntet.

Das Team von ANNALINDE wünscht

Guten Appetit 

Wir servieren Euch in Gartenathmosphäre mehrere Gänge der besten Zutaten frisch aus unseren Gärtnereien.

Menü:
tba


Einlass: 19:00 Uhr

Start:    19:30 Uhr

Preis 33,- EUR p.P.

Weitere Termine 2019:

23. August - Hostel Eden

Anmeldungen bereits möglich:

Verbindliche Anmeldung mit (einen) Name, Anzahl Personen, Adresse und Tel.Nr. bitte an dinner@annalinde-leipzig.de

Aufgrund schlechter Reservierungsmoral das letzte Jahr: Stornierung bis Montag vor Veranstaltung kostenlos. Im Anschluss berechnen wir den vollen Preis.

Foto: Annabellesagt