---
id: '448980162553681'
title: Fête de la musique mit BrewsSebs
start: '2019-06-21 15:00'
end: '2019-06-21 22:00'
locationName: Georg-Maurer-Bibliothek
address: 'Zschochersche Straße 14, 04229 Leipzig'
link: 'https://www.facebook.com/events/448980162553681/'
image: 61966564_2874843142557744_5410020293001347072_n.jpg
teaser: '15 - 17 Uhr: Musik im Gemeinschaftsgarten ANNALINDE  17 - 19 Uhr: Auf der Terrasse der Bibliothek: Musik von BrewsSebs  17 - 20 Uhr: Pedro Pardalis [k'
recurring: null
isCrawled: true
---
15 - 17 Uhr: Musik im Gemeinschaftsgarten ANNALINDE 
17 - 19 Uhr: Auf der Terrasse der Bibliothek: Musik von BrewsSebs 
17 - 20 Uhr: Pedro Pardalis [kap tripiti - Leipzig] im Gemeinschaftsgarten ANNALINDE
20 - 22 Uhr: Musik von BrewsSebs im Gemeinschaftsgarten ANNALINDE 

Außerdem blühende Hochbeete mit (Stil)-Blüten und Gedichten Georg Maurers und  Kaffee, Kuchen und Herzhaftes 

Gemeinschaftsveranstaltung mit dem Institut Francais Leipzig 

Eintritt: frei 

©BrewSebs privat