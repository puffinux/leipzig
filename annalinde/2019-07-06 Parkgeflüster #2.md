---
id: '1963839767054796'
title: 'Parkgeflüster #2'
start: '2019-07-06 10:00'
end: '2019-07-06 13:00'
locationName: null
address: 'Mariannenpark, 04347 Leipzig'
link: 'https://www.facebook.com/events/1963839767054796/'
image: 61714219_2416260781739382_4311978231235346432_n.jpg
teaser: 'Liebe Anwohner*innen, liebe Akteure aus Schönefeld und rund um den Mariannenpark,  wir laden Euch im Rahmen des Projektes KoopLab (www.kooplab.de) seh'
recurring: null
isCrawled: true
---
Liebe Anwohner*innen, liebe Akteure aus Schönefeld und rund um den Mariannenpark,

wir laden Euch im Rahmen des Projektes KoopLab (www.kooplab.de) sehr herzlich zu unserer nächsten  Veranstaltung im Mariannenpark ein:

Parkgeflüster #2: Der Mariannenpark gestern, heute und morgen

6. Juli 2019 von 10 bis 13 Uhr
Gemeinsames Parkfrühstück am Baumrondell 
in der Nähe des Eingangsbereich des Mariannenparks (Rohrteichstraße, Ecke Schönefelder Allee)

Was wird diesmal passieren?

Beim  Parkgeflüster #1 am 31. Mai kamen wir bereits mit vielen Menschen ins Gespräch und lernten verschiedenste Perspektiven auf den Park kennen. Nun wollen wir Eure Ideen und die Zukunft des Mariannenparks bei einem gemütlichen Frühstück weiterdenken. Es wird wieder einige Infos zur Geschichte des Parks, Spiele und Mitmach-Stationen geben.
Kommt vorbei, redet mit und lasst Euch überraschen!
Leitet diese Einladung sehr gern an Freund*innen und Bekannt*innen weiter und verbreitet sie in Euren Netzwerken. Wenn Ihr Fragen oder Anregungen habt, wendet Euch gern an uns!

das Leipziger KoopLab-Team
leipzig@kooplab.de

Falls es regnet oder stürmt, muss die Veranstaltung leider ausfallen.

Parkgeflüster #2 ist eine gemeinsame Veranstaltung von ANNALINDE gGmbH und dem Helmholtz-Zentrum für Umweltforschung GmbH (UFZ) im Rahmen des Projekts "KoopLab: Teilhabe durch kooperative Freiraumentwicklung in Ankunftsquartieren"
Mehr Infos: www.kooplab.de
