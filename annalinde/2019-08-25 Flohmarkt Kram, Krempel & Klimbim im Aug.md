---
id: '898965027105780'
title: 'Flohmarkt Kram, Krempel & Klimbim im August'
start: '2019-08-25 11:00'
end: '2019-08-25 18:00'
locationName: heiter bis wolkig
address: 'Röckener Straße 44, 04229 Leipzig'
link: 'https://www.facebook.com/events/898965027105780/'
image: 64320009_1073342992858862_4405297231943434240_n.jpg
teaser: 'Hereinspaziert, Hereinspaziert! Nach unserer kleinen aber feinen Sommerpause im Juli melden wir uns munter & fröhlich zurück!   ** Die Standplätze für'
recurring: null
isCrawled: true
---
Hereinspaziert, Hereinspaziert! Nach unserer kleinen aber feinen Sommerpause im Juli melden wir uns munter & fröhlich zurück! 

** Die Standplätze für August sind leider schon alle vergeben! **

Seid wieder dabei, wenn sich der wunderschöne Garten des heiter bis wolkig neben der Obtwiese der Annalinde Leipzig in eine Fundgrube für alte und neue Schätze verwandelt. Beim bunten Markttreiben gibt es Kunst, Klamotten und allerhand Kram, Krempel & Klimbim zu entdecken. Hier nimmt garantiert jeder sein neues ♥ – Teil mit nach Hause!

Weil Schätze suchen hungrig macht, gibt es leckeres Essen, kalte Getränke an der Bar & frische Eiscreme zur Abkühlung. Es erwarten Euch außerdem wilde Spiele für Groß und Klein, Musik & jede Menge tolle Menschen aus der Nachbarschaft. ♥ 

Euer Krempel, Trödel, Selbstgemachtes suchen ein neues Zuhause? Dann meldet euch für einen Flowmarktstand unter  kramkrempelklimbim@gmail.com Ihr bekommt dann alle nötigen Infos zugeschickt.

Das Heiter bis Wolkig ist ein Ort zum Entspannen, frei und kreativ sein - wo man die Welt an sich vorbeiziehen lassen kann, zwischen Stadt und Wildnis & nebenbei noch tolle Flohmarktschätze finden kann. 

Wir freuen uns auf Euch!

Der Eintritt ist frei! Falls das Wetter nicht mitflowt, müssen wir den Flohmarkt leider absagen, da wir keine Schlechtwetter-Alternative vor Ort haben. Wir bitten daher um euer Verständnis! ♥ Der nächste Flowmarkt kommt bestimmt. ;) Der nächste Termin ist höchstwahrscheinlich am 22. September. Stay tuned! 