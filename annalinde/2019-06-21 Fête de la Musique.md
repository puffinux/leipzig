---
id: '369530103909601'
title: Fête de la Musique
start: '2019-06-21 15:00'
end: '2019-06-21 22:00'
locationName: null
address: 'Zschochersche Straße 12, 04229 Leipzig'
link: 'https://www.facebook.com/events/369530103909601/'
image: 60973909_2397988010233326_7887015150505426944_n.jpg
teaser: Auch dieses Jahr findet im Gemeinschaftsgarten der ANNALINDE ein abwechslungsreicher und bunter Beitrag zur Fête de la Musique statt.  Wie in den verg
recurring: null
isCrawled: true
---
Auch dieses Jahr findet im Gemeinschaftsgarten der ANNALINDE ein abwechslungsreicher und bunter Beitrag zur Fête de la Musique statt.

Wie in den vergangenen Jahren laden wir alle herzlich ein, zwischen unseren Beeten und unserer Bar gemütlich sitzend oder wild tanzend der Musik zu lauschen.

 
______________________

Zeit: 15-22 Uhr

Ort: ANNALINDE Gemeinschaftsgarten

Eintritt ist frei!