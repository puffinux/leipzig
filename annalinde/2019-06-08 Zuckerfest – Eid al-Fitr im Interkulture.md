---
id: '301348544075728'
title: Zuckerfest – Eid al-Fitr im Interkulturellen Garten
start: '2019-06-08 15:00'
end: '2019-06-08 22:00'
locationName: null
address: 'Zschochersche Straße 12, 04229 Leipzig'
link: 'https://www.facebook.com/events/301348544075728/'
image: 60518259_2382116771820450_8005363008706445312_n.jpg
teaser: Gemeinsam möchten wir mit euch das Ende des Fastenmonats Ramadan feiern.  Alle sind herzlich eingeladen bei Musik und Kulinarischem einen warmen Nachm
recurring: null
isCrawled: true
---
Gemeinsam möchten wir mit euch das Ende des Fastenmonats Ramadan feiern.

Alle sind herzlich eingeladen bei Musik und Kulinarischem einen warmen Nachmittag im Garten zu verbringen. 


Alle sind Willlkommen – wir freuen uns auf euch!
 
—————————————

15-18:00 Ickar Flyer & friends
18:30 Luise Rauer & Leandro Salvatierra
20:00 Gatti Randali

Ort: ANNALINDE Gemeinschaftsgarten, Zschochersche Str. 12

—————————————
Das Projekt „Interkultureller Garten“ wird von September 2016 bis August 2019 durch die Europäsiche Union mit Mitteln aus dem Europäischen Sozialfond finanziert.