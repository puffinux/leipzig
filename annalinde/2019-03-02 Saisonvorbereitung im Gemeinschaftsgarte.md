---
id: '386394041916289'
title: Saisonvorbereitung im Gemeinschaftsgarten
start: '2019-03-02 12:00'
end: '2019-03-02 18:00'
locationName: null
address: 'Zschochersche Straße 12, 04229 Leipzig'
link: 'https://www.facebook.com/events/386394041916289/'
image: 52803655_2259325514099577_4244563990765633536_n.jpg
teaser: 'Krokusse, Schneeglöckchen und die kleinen Märzenbecher sind schon da - Zeit, den Gemeinschaftsgarten aus dem Winterschlaf zu wecken! Seid herzlich ein'
recurring: null
isCrawled: true
---
Krokusse, Schneeglöckchen und die kleinen Märzenbecher sind schon da - Zeit, den Gemeinschaftsgarten aus dem Winterschlaf zu wecken! Seid herzlich eingeladen zu unserem ersten Garteneinsatz im neuen Jahr.

Gemeinsam wollen wir mit den Vorbereitungen der Saison 2019 beginnen. Wir freuen uns auf ein Wiedersehen mit alten und neuen Gesichtern!

Kommt einfach vorbei – keine Anmeldung erforderlich!