---
id: "2552607474962610"
title: Jolly Holiday get together for BIPoC only
start: 2019-11-30 15:30
end: 2019-11-30 22:00
locationName: Seniorenbüro Ost - Inge & Walter
address: Eisenbahnstr. 66, 04315 Leipzig
link: https://www.facebook.com/events/2552607474962610/
image: 74232818_1412061015622857_2789592524451217408_n.jpg
teaser: We want to start cooking from 3:30pm, join us if you like!  Afterwards we plan
  to eat, talk, listen to music - just hang out with each other. If you h
isCrawled: true
---
We want to start cooking from 3:30pm, join us if you like! 
Afterwards we plan to eat, talk, listen to music - just hang out with each other.
If you have any questions, needs or suggestions please contact us!


