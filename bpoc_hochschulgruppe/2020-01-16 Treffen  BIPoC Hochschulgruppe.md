---
id: "1585027581636193"
title: Treffen // BIPoC Hochschulgruppe
start: 2020-01-16 18:00
end: 2020-01-16 20:00
address: S 103, Seminargebäude, Universitätsstr. 1, 04109 Leipzig
link: https://www.facebook.com/events/1585027581636193/
image: 74791869_1393958840766408_5877746307459710976_n.jpg
teaser: "[Deutsch | English]  [Deutsch] Liebe Mit-Student_innen, die sich als Schwarz,
  Indigen oder Person of Color identifizieren, Wir laden euch zum wöchentl"
isCrawled: true
---
[Deutsch | English]

[Deutsch]
Liebe Mit-Student_innen, die sich als Schwarz, Indigen oder Person of Color identifizieren,
Wir laden euch zum wöchentlichen Treffen der Bpoc Hochschulgruppe Leipzig ein.

Unser Ziel ist es uns zu vernetzen, gegenseitig zu inspirieren, zu unterstützen und einen Raum von und für uns zu schaffen, in dem wir Wege finden unsere Ressourcen auszudrücken und zu teilen - damit die Realität an der Uni Leipzig die Stimmen ihrer BIPoC Student_innen inkludiert. Das Treffen ist offen für Externe.

Im Wintersemester 2019/20 treffen wir uns jeden Donnerstag ab 18 Uhr in Raum S 103 im Seminargebäude am Hauptcampus.

- Nur für BIPoC (Black, Indigenous, People of Color) -

Disclaimer:
Wir sind nicht in Lage einen rundum sicheren Raum zu schaffen bezüglich verbaler Äußerungen. Jede einzelne Person ist mitverantwortlich für den Raum - falls Themen unangenehm sind, achtet bitte auf euch selbst und respektiert andere.
Keine Toleranz für Diskriminierung und Gewalt.

Wir freuen uns, wenn du vorbeikommst!
Bei Fragen wende dich gerne per Nachricht an die Bpoc Hochschulgruppe Leipzig oder schicke eine Mail an antira@stura.uni-leipzig.de


[English]
Dear fellow students, who identify themselves as Black Indigenous or as Person of Color. 
We are inviting you to join our weekly meeting of the BIPoC campus group.

Our aim is to connect, inspire and support each other and create a space by ourselves for ourselves where we find ways to express and share our resources - for that the reality of the university Leipzig includes the voices of their BIPoC students (the meeting is also open for externals). 

FACTS: meeting every thursday at 6 pm on Campus - Uni Leipzig in Room 103 at Seminargebäude.

- BIPoC only -

Disclaimer:
We are not able to create a safe space regarding the possibility of being hurt through words. Every single person creates and shapes the room, if issues make you uncomfortable please protect your own health and respect the others.
No acceptance for discrimination and violence.

We are looking forward for you to join! 
If you have questions you can adress Bpoc Hochschulgruppe Leipzig on facebook or write an email to antira@stura.uni-leipzig.de