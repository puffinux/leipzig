---
id: "1585027564969528"
title: Treffen // BIPoC Hochschulgruppe
start: 2019-11-14 18:00
end: 2019-11-14 20:00
address: S 103, Seminargebäude, Universitätsstr. 1, 04109 Leipzig
link: https://www.facebook.com/events/1585027564969528/
image: 74791869_1393958840766408_5877746307459710976_n.jpg
teaser: Im Wintersemester 2019/20 trifft sich die BIPoC Hochschulgruppe jeden
  Donnerstag ab 18 Uhr in Raum S 103 im Seminargebäude am Hauptcampus.  - Nur
  für
isCrawled: true
---
Im Wintersemester 2019/20 trifft sich die BIPoC Hochschulgruppe jeden Donnerstag ab 18 Uhr in Raum S 103 im Seminargebäude am Hauptcampus.

- Nur für BIPoC (Black, Indigenous, People of Color) -