---
id: "388980288714752"
title: "BIPoC Vernetzung #2 - BIPOC Meeting"
start: 2019-10-22 13:00
end: 2019-10-22 15:00
address: S203 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/388980288714752/
image: 71565125_1031741663662699_8298619948293423104_o.jpg
teaser: "Di. 22.10.19 / 13.00  BIPoC Vernetzung #2 - BIPOC Meeting – Living in the
  Diaspora, get together & was bisher geschah  BPoC Hochschulgruppe / S203  De"
isCrawled: true
---
Di. 22.10.19 / 13.00

BIPoC Vernetzung #2 - BIPOC Meeting – Living in the Diaspora, get together & was bisher geschah

BPoC Hochschulgruppe / S203

Der Workshop soll einen Raum des Austausches bieten, deshalb öffnen wir diese Veranstaltung speziell nur für BIPoCs.Wir stehen zur Verfügung, Fragen zu beantworten, eigene Erfahrungen zu teilen und aktiv zuzuhören. Die Selbstbezeichnung BIPoC entspringt aus Erfahrungen der Gruppenmitglieder und reflektiert unsere Lebensrealitäten in der Diaspora. 