---
id: '635258496927265'
title: Bürotag des Solidarischen Netzwerks
start: '2019-07-15 12:00'
end: '2019-07-15 14:00'
locationName: Autodidaktische Initiative
address: 'Georg-Schwarzstr. 19, 04177 Leipzig'
link: 'https://www.facebook.com/events/635258496927265/'
image: 53761841_2864339403591231_4249797833617571840_o.jpg
teaser: 'Solidarisches  Netzwerk – gegenseitige Hilfe bei Stress mit Chefs, Behörden und Vermietern  Das Solidarische Netzwerk Leipzig ist ein Zusammenschluss'
recurring: null
isCrawled: true
---
Solidarisches  Netzwerk – gegenseitige Hilfe bei Stress mit Chefs, Behörden und Vermietern

Das Solidarische Netzwerk Leipzig ist ein Zusammenschluss von Menschen, die keine Lust mehr darauf haben ähnliche Probleme vereinzelt lösen zu müssen. Stress mit Vermieter*innen,Chef*innen und Sachbearbeiter*innen kennen wir alle. Unsere Probleme sind jedoch nicht individuell verschuldet, sie sind nicht das Ergebnis unseres Versagens. Vielmehr sind sie Ausdruck dieser Gesellschaft, die auf Konkurrenz und Ausbeutung beruht. 

Unser Netzwerk lebt aus den gemeinsamen Erfahrungen und Berichten der eigenen Betroffenheit. Im regelmäßig stattfindenden Anlaufpunkt tragen wir unser Wissen zusammen und diskutieren auf Augenhöhe kreative Möglichkeiten, unsere eigenen Interessen einzufordern und durchzusetzen. Das Ausfüllen von Anträgen, Schreiben von Widersprüchen und Formulieren von Forderungen sind lästige Angelegenheiten, die wir aus der Vereinzelung heraus holen wollen. Unser gemeinsamer Bürotag bietet dafür genug Möglichkeiten, gemütlich bei Kaffee und Tee den Papierberg abzuarbeiten. 

Lasst uns mit dem Solidarischen Netzwerk [Leipzig] eine handlungsfähige Struktur schaffen, die bei der eigenen Betroffenheit ansetzt und Schritte wagt hin zu einer frei(er)en Gesellschaft!

In gemütlicher und ruhiger Atmosphäre machen wir unseren Papierkram, recherchieren in Ratgebern oder basteln an Webseiten. WLAN, Drucker, Kopierer, Kaffee und Tee gibt es vor Ort.

Alle aktuellen Termine und Informationen findet ihr auf solidarisch.blogsport.eu