---
id: '2066434933430599'
title: Arabisch Kurs// Alle Niveaus
start: '2019-02-20 17:00'
end: '2019-02-20 19:00'
locationName: Autodidaktische Initiative
address: 'Georg-Schwarzstr. 19, 04177 Leipzig'
link: 'https://www.facebook.com/events/2066434933430599/'
image: 52417054_2803626252995880_1498038220082905088_n.jpg
teaser: Wöchentlicher Arabisch-Kurs mit Ahmed in der adi.  Immer mittwochs 17-19Uhr.  Einstieg jederzeit möglich.
recurring: null
isCrawled: true
---
Wöchentlicher Arabisch-Kurs mit Ahmed in der adi. 
Immer mittwochs 17-19Uhr.

Einstieg jederzeit möglich.