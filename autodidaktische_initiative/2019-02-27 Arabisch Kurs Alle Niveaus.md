---
id: '2282214545142630'
title: Arabisch Kurs// Alle Niveaus
start: '2019-02-27 17:00'
end: '2019-02-27 18:30'
locationName: Autodidaktische Initiative
address: 'Georg-Schwarzstr. 19, 04177 Leipzig'
link: 'https://www.facebook.com/events/2282214545142630/'
image: 51497673_2792435064114999_6437639953563254784_n.jpg
teaser: Wöchentlicher Arabisch-Kurs mit Ahmed in der adi.  Immer mittwochs 17-19Uhr.  Einstieg jederzeit möglich.
recurring: null
isCrawled: true
---
Wöchentlicher Arabisch-Kurs mit Ahmed in der adi. 
Immer mittwochs 17-19Uhr.

Einstieg jederzeit möglich.