---
id: "2315666845339068"
title: Stammtisch // informal group meeting
start: 2020-02-07 20:00
end: 2020-02-07 23:00
locationName: Autodidaktische Initiative
address: Georg-Schwarzstr. 19, 04177 Leipzig
link: https://www.facebook.com/events/2315666845339068/
image: 69859290_3330551023636731_4903243564009390080_n.jpg
teaser: Gemütliche Runde zum Kennenlernen und Austauschen in der adi. Kommt gerne
  vorbei! Diskutiert mit uns, hört zu, stellt Fragen, trinkt mit uns etwas.  J
isCrawled: true
---
Gemütliche Runde zum Kennenlernen und Austauschen in der adi. Kommt gerne vorbei! Diskutiert mit uns, hört zu, stellt Fragen, trinkt mit uns etwas.

Jeden ersten Donnerstag im Monat! 

+++

A Stammtisch (German for "regulars' table") is an informal group meeting held on a regular basis, and also the usually large, often round table around which the group meets. A Stammtisch is not a structured meeting, but rather a friendly get-together.

Traditionally, the meeting table is marked with a somewhat elaborate sign reserving it for regulars. Historically, a Stammtisch was an all-male affair that might involve socialising, card playing (such as skat), and often political or philosophical discussions. 

So, come arround and have a drink with us, discuss with us and so on.