---
id: '322305851966723'
title: Offenes Plenum
start: '2019-07-11 19:00'
end: '2019-07-11 21:00'
locationName: Autodidaktische Initiative
address: 'Georg-Schwarzstr. 19, 04177 Leipzig'
link: 'https://www.facebook.com/events/322305851966723/'
image: 53921549_2870118883013283_1365282868317650944_n.jpg
teaser: 'Zum offenen Plenum der adi sind alle Interessierten eigeladen!   Hier werden Raumanfragen, Kooperationsanfragen, ADI-Projekte und weitere organisatori'
recurring: null
isCrawled: true
---
Zum offenen Plenum der adi sind alle Interessierten eigeladen! 

Hier werden Raumanfragen, Kooperationsanfragen, ADI-Projekte und weitere organisatorische Themen besprochen.
Es findet jeden Donnerstag um 19 Uhr statt!

Du willst eine Lesekreis oder ähnliches gründen und suchst Räumlichkeiten in denen du deine Idee umsetzen kannst?

Dann komm vorbei, teil deine Idee mit uns und wir schauen gemeinsam, ob die adi der Raum für deine Idee ist.

Jeden 1. Donnerstag im Monat ist Stammtisch.  Da verbringen wir einfach Zeit miteinander, ohne dass Orga-Themen im Mittelpunkt stehen. Einfach zum Quatschen, Träumen, Philosophieren.

Beginnt auch erst um 20h.
