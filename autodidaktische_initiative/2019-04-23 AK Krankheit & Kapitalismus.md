---
id: '377037509512641'
title: AK Krankheit & Kapitalismus
start: '2019-04-23 17:00'
end: '2019-04-24 00:00'
locationName: Autodidaktische Initiative
address: 'Georg-Schwarzstr. 19, 04177 Leipzig'
link: 'https://www.facebook.com/events/377037509512641/'
image: 49022269_2701093729915800_3937097199124480000_n.jpg
teaser: 'BITTE MELDET EUCH VORHER AN, DA DER AK SICH NICHT IMMER DIENSTAG TRIFFT. E-Mail zum anmelden auf Anfrage.  Wissenschaftlicher Arbeitskreis zum Verhält'
recurring: null
isCrawled: true
---
BITTE MELDET EUCH VORHER AN, DA DER AK SICH NICHT IMMER DIENSTAG TRIFFT.
E-Mail zum anmelden auf Anfrage.

Wissenschaftlicher Arbeitskreis zum Verhältnis von Krankheit und Kapitalismus anhand der vom Sozialistischen Patientenkollektiv verfassten Agitationsschrift

„Aus der Krankheit eine Waffe machen“.

Aus dem Kapital VІ: Krankheit und Kapital:

„Krankheit ist Ausdruck der lebensvernichtenden Gewalt des Kapitals. Krankheit wird kollektiv produziert: d.h. indem der Arbeitende im Arbeitsprozeß das Kapital, das ihm als fremde Macht gegenübertritt, schafft, produziert er kollektiv seine Vereinzelung. Es ist deshalb nur konsequent, daß das kapitalistische Gesundheitswesen diese Vereinzelung perpetuiert, indem es die Symptome nicht als kollektiv produziert, sondern als individuelles Schicksal, als Schuld und Versagen behandelt. Allerdings produziert der Kapitalismus in Gestalt der Krankheit die gefährlichste Waffe gegen sich selbst. Deshalb muß er auch mit seinen schärfsten Waffen gegen das progressive Moment der Krankheit ankämpfen: mit Gesundheitswesen, Justiz, Polizei. Objektiv ist Krankheit als defekte (= nicht verwertbare) Arbeitskraft Totengräber des Kapitalismus. Krankheit = innere Schranke des Kapitalismus: Wenn alle akut krank (= arbeitsunfähig) sind, kann keiner mehr Mehrwert produzieren. Als kollektiv bewußter Prozeß ist Krankheit die revolutionäre Produktivkraft,in ihren Wirkungsgraden abgestuft nach: gehemmtem Protest, bewußtem Protest, kollektivem Bewußtsein, solidarischem Kampf.“

Das Buch gibt es online auf der Seite des SPK PF unter der Rubrik „Texte in Deutsch“. Ansonsten könnt es dort oder im Buchhandel auch erwerben.