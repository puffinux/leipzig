---
id: "567145160783680"
title: Lesekreis Ernährungssouveränität
start: 2019-12-16 19:00
end: 2019-12-16 21:00
locationName: Autodidaktische Initiative
address: Georg-Schwarzstr. 19, 04177 Leipzig
link: https://www.facebook.com/events/567145160783680/
image: 75653242_3582606598431171_8123575021386858496_o.jpg
teaser: Tagtäglich kommen wir mit Nahrungsmitteln in Kontakt – wir bereiten sie zu,
  essen,  laufen an ihnen in unglaublichen Mengen im Supermarkt vorbei. Den
isCrawled: true
---
Tagtäglich kommen wir mit Nahrungsmitteln in Kontakt – wir bereiten sie zu, essen,  laufen an ihnen in unglaublichen Mengen im Supermarkt vorbei. Den wenigsten sieht man hierbei an, woher sie kommen, wer sie produziert hat, unter welchen Bedingungen, auf welchem Boden sie gewachsen sind, und wie sie in das Regal vor uns gekommen sind. Dabei sind das die wichtigen Fragen – die, die bestimmen, welche Qualität unser Essen hat, ob Menschen für den Anbau fair entlohnt werden, und ob wir in 30 Jahren noch genau so weiter machen können. Deshalb möchten wir uns mithilfe des Buches „Genial lokal – So kommt die Ernährungswende in Bewegung“ mit dem Begriff der Ernährungssouveränität auseinandersetzen. Ernährung nicht als passiver Versorgungszustand, sondern als selbstbestimmte Teilnahme und aktive Mitgestaltung, die am Ende so viel mehr bedeutet als das, was am Ende auf unseren Tellern liegt.