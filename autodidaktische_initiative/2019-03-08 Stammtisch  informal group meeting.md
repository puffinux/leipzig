---
id: '2208316446073910'
title: Stammtisch // informal group meeting
start: '2019-03-08 20:00'
end: '2019-03-08 23:00'
locationName: Autodidaktische Initiative
address: 'Georg-Schwarzstr. 19, 04177 Leipzig'
link: 'https://www.facebook.com/events/2208316446073910/'
image: 49793797_2716424545049385_1443844614799753216_n.jpg
teaser: 'Gemütliche Runde zum Kennenlernen und Austauschen in der adi. Kommt gerne vorbei! Diskutiert mit uns, hört zu, stellt Fragen, trinkt mit uns etwas.  J'
recurring: null
isCrawled: true
---
Gemütliche Runde zum Kennenlernen und Austauschen in der adi. Kommt gerne vorbei! Diskutiert mit uns, hört zu, stellt Fragen, trinkt mit uns etwas.

Jeden ersten Donnerstag im Monat! 

+++

A Stammtisch (German for "regulars' table") is an informal group meeting held on a regular basis, and also the usually large, often round table around which the group meets. A Stammtisch is not a structured meeting, but rather a friendly get-together.

Traditionally, the meeting table is marked with a somewhat elaborate sign reserving it for regulars. Historically, a Stammtisch was an all-male affair that might involve socialising, card playing (such as skat), and often political or philosophical discussions. 

So, come arround and have a drink with us, discuss with us and so on.