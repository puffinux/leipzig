---
id: '244227546458877'
title: Deutsch als Fremdsprache // German as foreign language
start: '2019-06-13 17:00'
end: '2019-06-13 19:00'
locationName: Autodidaktische Initiative
address: 'Georg-Schwarzstr. 19, 04177 Leipzig'
link: 'https://www.facebook.com/events/244227546458877/'
image: 52914594_2818873371471168_6004179102293032960_n.jpg
teaser: — English version below —  Am 5. Oktober startet der neue Kurs für Deutsch als Fremdsprache! Wir werden Gesprächstraining und lustige Lernspiele mache
recurring: null
isCrawled: true
---
— English version below —

Am 5. Oktober startet der neue Kurs für Deutsch als Fremdsprache! Wir werden Gesprächstraining und lustige Lernspiele machen. Außerdem wird es ein paar Übungen geben:

Präpositionen (im, am, um)

Artikel (der, die, das)

Satzverbindungen („Ich lerne Deutsch, weil es Spaß macht!“)

und viele mehr!

Wenn du Interesse hast, komm einfach vorbei. Der Kurs wird donnerstags von 17-19 h in der adi (Georg-Schwarz-Str. 19) stattfinden. Es sind sechs Termine (bis zum 9.11.) vorgesehen. Der Eintritt ist frei!

—

The new course for German as a foreign language starts on October 5th! We will practice conversation and play funny learning games. We will also have some exercises:

im, am, um? prepositions

der, die, das? articles

„Ich lerne Deutsch, weil es Spaß macht!“ connecting sentences

and many more!

If you are interested, come over. The course will take place on Thursdays from 17-19 h in the adi (Georg-Schwarz-Str. 19). It is planned for six dates (until Nov. 9th). Entrance is free of charge!