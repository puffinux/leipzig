---
id: '252288682345866'
title: 'Anmeldeschluss: Austauschprojekt - Laboro ergo sum?'
start: '2019-03-24 00:00'
end: '2019-03-24 23:59'
locationName: Westwerk Leipzig
address: 'Karl-Heine-Strasse 85-93, 04229 Leipzig-Plagwitz, Sachsen'
link: 'https://www.facebook.com/events/252288682345866/'
image: 54225916_2614111911939225_4784880634768130048_o.jpg
teaser: Laboro ergo sum - Ich arbeite also bin ich?  15 Leute aus Terni (Italien) treffen 15 Leute aus Leipzig. Im Mittelpunkt steht der Austausch untereinand
recurring: null
isCrawled: true
---
Laboro ergo sum - Ich arbeite also bin ich?

15 Leute aus Terni (Italien) treffen 15 Leute aus Leipzig. Im Mittelpunkt steht der Austausch untereinander zum Thema „Arbeit“.

Begegnung vom 5.-18. April 2019 in Leipzig

In der ersten Woche nähern wir uns dem Thema und dem Arbeiten mit Fotografie und Film. In der zweiten Woche entstehen in Kleingruppen künstlerische/dokumentarische Projekte zum Thema. Keine Kamera / Vorkenntnisse nötig! Teilnahme gegen Spende. Kinderbetreuung möglich!

Mehr Infos: https://laboroergosum.eu/de/
Verbindliche Anmeldung bis zum 24. März!

Bitte auch weitersagen/teilen/Freunde einladen! :)