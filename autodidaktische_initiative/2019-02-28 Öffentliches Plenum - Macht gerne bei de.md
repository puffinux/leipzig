---
id: '187386175280046'
title: Öffentliches Plenum - Macht gerne bei der adi mit
start: '2019-02-28 19:00'
end: '2019-02-28 21:00'
locationName: Autodidaktische Initiative
address: 'Georg-Schwarzstr. 19, 04177 Leipzig'
link: 'https://www.facebook.com/events/187386175280046/'
image: 36358241_2327049110653599_2003056963126034432_n.jpg
teaser: Immer donnerstags um 19h findet unser öffentliches Plenum statt und alle Interessierten sind eingeladen vorbeizukommen. Vielleicht hast du ja Lust die
recurring: null
isCrawled: true
---
Immer donnerstags um 19h findet unser öffentliches Plenum statt und alle Interessierten sind eingeladen vorbeizukommen. Vielleicht hast du ja Lust die adi mal kennenzulernen und mitzumachen. 
Auf dem Plenum werden alle wichtigen Sachen besprochen. 

++ Jeden 1. Donnerstag im Monat ist Stammtisch, anstatt Plenum ++

Da geht es um gemeinsam Zeit verbringen und mal nicht ums Organisieren. : )