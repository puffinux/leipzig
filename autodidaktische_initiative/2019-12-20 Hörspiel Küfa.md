---
id: "439557906998674"
title: Hörspiel Küfa
start: 2019-12-20 19:00
end: 2019-12-20 22:00
locationName: Autodidaktische Initiative
address: Georg-Schwarzstr. 19, 04177 Leipzig
link: https://www.facebook.com/events/439557906998674/
image: 79272966_3633519830006514_2667304282864222208_o.jpg
teaser: "3-Klang zum (fast) dritten Advent:  Am Freitag, den 20.12.2019, laden wir
  euch ab 19 Uhr in die adi zur Küfa und zum gemeinsamen Hörspielhören ein. Na"
isCrawled: true
---
3-Klang zum (fast) dritten Advent:

Am Freitag, den 20.12.2019, laden wir euch ab 19 Uhr in die adi zur Küfa und zum gemeinsamen Hörspielhören ein. Nachdem wir uns alle ordentlich den Bauch vollgehauen haben wird der Ofen angeheizt und dann gibt es bei gemütlichem Feuergeknister diese drei Hörspiele für euch:

Der Betonflüsterer von Jan Bolender, 14 min

Mit ‚Kultur Konkret‘ und Wolfgang F. wandeln wir auf den zarten Spuren des einzigen Betonflüsterers Deutschlands. Während um ihm herum gebohrt, geflext und dreckige Witze gerissen werden, muss er sich auf etwas konzentrieren, für das viele Kollegen kein Verständnis aufbringen können: die emotionale Stabilität eines Gebäudes.


Hühner und überhaupt … oder- eigentlich ist es ja egal, was zuerst da war von Rando Trost, 22 min

Von der Emotionalen Stabilität eines Gebäudes kommen wir zu den Gefühlswelten von Hennen und Hähnen. Was macht seine Hühner zu den glücklichsten der Welt und was zählt eigentlich wirklich zwischen Mensch und Tier:“ikko balli bali nawero; bola, boola poppi-GOO GOGOGOGO“?

 
Wolpertingers von Cassandra Rink, 30 min, englischsprachiges Original

Als letztes Stück hören wir zwei Forscherinnen zu, die auf der Suche nach Übernatürlichem und Folkloristischen im bayrischen Wald auf Wolpertinger stoßen. Wesen mit gigantischen Hasenköpfen, goldenem Geweih, roten Augen, fuchsigem Körper und Entenfüßen. Aber ist es der beste Zeitpunkt, in einer nebeligen Vollmondnacht der Mutter aller Wolpertinger zu begegnen…?

 