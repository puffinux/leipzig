---
id: '2456662407806004'
title: Support Your Sisters Not Your Cisters mit FaulenzA
start: '2019-04-19 18:00'
end: '2019-04-19 21:00'
locationName: Autodidaktische Initiative
address: 'Georg-Schwarzstr. 19, 04177 Leipzig'
link: 'https://www.facebook.com/events/2456662407806004/'
image: 57191791_660342701071162_4992935937282408448_o.jpg
teaser: 'FaulenzA wird ihr Buch "Support Your Sisters Not Your Cisters" im Rahmen eines Workshops vorstellen:  Trans*misogynie- das ist die Gewalt, die ich erl'
recurring: null
isCrawled: true
---
FaulenzA wird ihr Buch "Support Your Sisters Not Your Cisters" im Rahmen eines Workshops vorstellen:

Trans*misogynie- das ist die Gewalt, die ich erlebe, seid ich denken
kann. Ich bin mit ihr aufgewachsen, sie hat mich geprägt und
geformt. Sie hat mich tief verwundet, und klein gemacht. Immer
wieder, jeden Tag, in unterschiedlichsten Formen. Mal durch offene
Aggressive körperliche –, mal durch alltägliche, unterschwellige
Gewalt. Ich meine die Art und Weise, wie Leute mit mir reden, mich
behandeln, über mich denken – auch in der (queer)feministischen
Szene. Nun versuche ich mich stark zu machen und gegen
Trans*misogynie zu kämpfen.

In dem Workshop stelle ich nach einer inhaltlichen Einführung
aktuelle Beispiele vor, die zeigen dass selbst feministische Räume
kein save-space für Trans*frauen sind. Diese können wir gemeinsam
diskutieren: Was ist daran trans*misogyn? Wie könnten Texte,
Veranstaltungsankündigungen, Sticker etc. anders gestaltet,
geschrieben sein? Was kann getan werden um feministische Räume
inklusiver für Trans*Frauen und Trans*Weiblichkeiten zu machen?
Wichtige Themen in diesem Workshop sind unter anderem:

-Trans*misogynie“ als ein Zusammenwirken von verschiedenen
Unterdrückungsformen, nämlich: Feminitätsfeindlichkeit
(Misogynie), Trans*phobie, Klassismus und Ableismus.
-Trans*misogynie in (queer)feministischer Szene,
-Frauen*Räume /FLT*I*Räume und Ausschlüsse von trans*Frauen
-Das „Sozialisationsargument“, mit dem Ausschlüsse von
Trans*Frauen begründet werden
-Trans*gender Day of Remembrance zum Gedenken der ermordeten
Trans*menschen (größtenteils trans*Frauen of Colour)
-die diskriminierende Weise, wie im Feminismus manchmal
Körperlichkeit thematisiert wird (zB Genitalien, Menstruation)

Wann? 18:00 Küche für alle
              19:00 Lesung und Workshop mit FaulenzA 
Wo?      Georg-Schwarz-Straße 19, 04177 Leipzig
Eintritt:  Gegen Spende