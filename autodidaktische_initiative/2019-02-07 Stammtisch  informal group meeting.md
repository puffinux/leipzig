---
id: '2208317376073817'
title: Stammtisch // informal group meeting
start: '2019-02-07 20:00'
end: '2019-02-08 23:00'
locationName: Autodidaktische Initiative
address: 'Georg-Schwarzstr. 19, 04177 Leipzig'
link: 'https://www.facebook.com/events/2208317376073817'
image: null
teaser: null
recurring: null
isCrawled: true
---
