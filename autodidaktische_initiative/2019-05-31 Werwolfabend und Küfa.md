---
id: '2401269556823455'
title: Werwolfabend und Küfa
start: '2019-05-31 19:00'
end: '2019-05-31 22:00'
locationName: Autodidaktische Initiative
address: 'Georg-Schwarzstr. 19, 04177 Leipzig'
link: 'https://www.facebook.com/events/2401269556823455/'
image: 61142353_3045690882122748_338760382921310208_n.jpg
teaser: 'This Friday we cook Mozambic food and                                      Wooouhuuoou, Spontaneous werewolf card game night. Come by and join, don’t'
recurring: null
isCrawled: true
---
This Friday we cook Mozambic food and                                      Wooouhuuoou, Spontaneous werewolf card game night.
Come by and join, don’t be scared, or be?
See you in the adi!

Diesen Freitag gibt es Mozambikanisch Essen und Woouhoou, spontaner Werwolf-Spiele-Abend nach der Küfa.
Kommt vorbei und habt keine Angst, odeeer doch?
Bis dann!