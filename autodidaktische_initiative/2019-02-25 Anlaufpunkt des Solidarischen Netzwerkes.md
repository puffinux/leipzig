---
id: '195498181165517'
title: Anlaufpunkt des Solidarischen Netzwerkes
start: '2019-02-25 19:00'
end: '2019-02-25 21:00'
locationName: Autodidaktische Initiative
address: 'Georg-Schwarzstr. 19, 04177 Leipzig'
link: 'https://www.facebook.com/events/195498181165517/'
image: 36437853_2327042317320945_148002880803045376_n.jpg
teaser: 'Das Solidarische Netzwerk Leipzig ist ein Zusammenschluss von Menschen, die keine Lust mehr darauf haben ähnliche Probleme vereinzelt lösen zu müssen.'
recurring: null
isCrawled: true
---
Das Solidarische Netzwerk Leipzig ist ein Zusammenschluss von Menschen, die keine Lust mehr darauf haben ähnliche Probleme vereinzelt lösen zu müssen. Stress mit Vermieter*innen,Chef*innen und Sachbearbeiter*innen kennen wir alle. Unsere Probleme sind jedoch nicht individuell verschuldet, sie sind nicht das Ergebnis unseres Versagens. Vielmehr sind sie Ausdruck dieser Gesellschaft, die auf Konkurrenz und Ausbeutung beruht. Unser Netzwerk lebt aus den gemeinsamen Erfahrungen und Berichten der eigenen Betroffenheit. Im regelmäßig stattfindenden Anlaufpunkt tragen wir unser Wissen zusammen und diskutieren auf Augenhöhe kreative Möglichkeiten, unsere eigenen Interessen einzufordern und durchzusetzen. Das Ausfüllen von Anträgen, Schreiben von Widersprüchen und Formulieren von Forderungen sind lästige Angelegenheiten, die wir aus der Vereinzelung heraus holen wollen. Unser gemeinsamer Bürotag bietet dafür genug Möglichkeiten, gemütlich bei Kaffee und Tee den Papierberg abzuarbeiten. Lasst uns mit dem Solidarischen Netzwerk [Leipzig] eine handlungsfähige Struktur schaffen, die bei der eigenen Betroffenheit ansetzt und Schritte wagt hin zu einer frei(er)en Gesellschaft!

Gegenseitige Unterstützung u.a. bei Problemen rund um Arbeit, Jobcenter und Wohnen.

Alle aktuellen Termine und Informationen findet ihr auf solidarisch.blogsport.eu