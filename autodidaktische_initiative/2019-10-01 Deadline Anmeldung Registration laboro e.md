---
id: '2444259275694270'
title: 'Deadline Anmeldung Registration laboro ergo sum #2'
start: '2019-10-01 09:00'
end: '2019-10-01 23:30'
locationName: Autodidaktische Initiative
address: 'Georg-Schwarzstr. 19, 04177 Leipzig'
link: 'https://www.facebook.com/events/2444259275694270/'
image: 70419072_2950666571617089_3663616840178860032_n.jpg
teaser: '+++++ Deutsch unten drunter +++++  What is »laboro ergo sum«? Hi! Laboro ergo sum #2 is an international exchange program by Erasmus+ of the associati'
recurring: null
isCrawled: true
---
+++++ Deutsch unten drunter +++++

What is »laboro ergo sum«?
Hi! Laboro ergo sum #2 is an international exchange program by Erasmus+ of the association Arciragazzi Gli Anni in Tasca – Blob.lgc lab in collaboration with Demetra -Centro di Palmetta and the German associations ADI and unofficial.pictures.

What is the plan?
For ten days we want to explore the topic of work through audio-visual (photography, cinema) and other creative medias. Which views and experiences do you connect with “work”? Which differences and similiarities are there between our cultural environments and worlds? In small groups we’ll observe local realities in Terni linked to the subject of work and develop ideas in a creative way. And above all we want to spend and share good moments of growth as a group.

How can you contribute, what you should bring?
You don’t have experience with photography or cinema? It doesn’t matter. It is more important that you are willing to get out of your comfort zone. Equipment (cameras, photo, video) will be provided, but if you have one, you can bring it with you and use it. It would be great if you could bring locks for bikes if you have some.

When and where?
Terni, Italy, 18 – 28 October 2019

How much does it cost?
Nothing! It’s financed by Erasmus+ the European Commission.

How to participate?
Submit your application until 1 October here: https://laboroergosum.eu/de/register/ Bring your ideas. There will be a preparatory meeting in the beginning of October.

+++++ Deutsche Version: +++++

Was ist »laboro ergo sum«?
Hallo! »laboro ergo sum« ist ein Erasmus+ Austauschprojekt zwischen der adi Leipzig in Kooperation mit unofficial.pictures und dem Kulturzentrum Centro di Palmetta aus Terni, Italien in Kooperation mit dem Videolabor blob.lgc.

Was haben wir vor?
Mit Hilfe von audio-visuellen und anderen kreativen Medien (Foto, Video) wollen wir uns 10 Tage lang dem Thema Arbeit widmen. Welche Ansichten und Erfahrungen verbindest du mit „Arbeit“? Welche Unterschiede und Gemeinsamkeiten gibt es in unseren Kulturkreisen und Lebenswelten? Bezogen auf das Thema Arbeit werden wir in Kleingruppen auf kreative Art und Weise Ideen entwickeln und Einblicke in verschiedene Lebensweisen in und um Terni erhalten. Darüber hinaus wollen wir schöne Momente der persönlichen Entwicklung innerhalb der Gruppe teilen. Auch das Mittag- und Abendessen werden wir in diesen Wochen gemeinsam kochen und verputzen.

Wie kann ich teilnehmen und was sollte ich mitbringen?
Du hast keine Erfahrung mit Fotografie und Videoproduktion – das macht nichts. Es ist viel wichtiger, dass du bereit bist deine Komfortzone zu verlassen. Equipment (wie Kameras, Foto, Video) wird euch zur Verfügung gestellt. Falls du aber bereits Equipment besitzt, kannst du es gern mitbringen und nutzen. Es wäre außerdem super, wenn ihr Fahrradschlösser mitbringen könntet, wenn ihr welche habt.

Wann und wo?
Terni, 18. – 28. Oktober 2019

Was kostet es?
Nichts! Es wird über die europäische Erasmus+ Kommission finanziert.

Wie kann ich teilnehmen?
Bitte fülle das Anmeldeformular aus: https://laboroergosum.eu/de/register/ Und bringe deine Ideen mit zum Austausch. Es wird ein Vorbereitsungstreffen Anfang Oktober geben. Wir werden euch den Treffpunkt per Mail mitteilen.