---
id: '786491648435946'
title: Blut muss fließen Film & Gespräch mit Peter Ohlendorf
start: '2019-10-07 18:00'
end: '2019-10-07 21:00'
locationName: null
address: 'HS 1, Campus Augustusplatz, Uni Leipzig'
link: 'https://www.facebook.com/events/786491648435946/'
image: 71022734_3399434406748392_9059530716823420928_n.jpg
teaser: Mo. 07.10.19 / 18.00  "Blut muss fließen"  adi (Autodidaktische Initiative e.V.) / HS1  Der Film wurde seit seiner Fertigstellung 2012 bereits 1500 Ma
recurring: null
isCrawled: true
---
Mo. 07.10.19 / 18.00

"Blut muss fließen"

adi (Autodidaktische Initiative e.V.) / HS1

Der Film wurde seit seiner Fertigstellung 2012 bereits 1500 Mal gezeigt, hat seitdem aber erschreckend wenig an Aktualität verloren. Über neun Jahre hinweg sammelte Thomas Kuban undercover unter Nazis mit versteckter Kamera einzigartiges Material. Es dokumentiert hautnah wie junge Leute mit Rechtsrock geködert und radikalisiert werden. Die Leitfrage des Films: Wie ist es möglich, dass auf der rechtsextremen Partymeile über alle Grenzen hinweg gefeiert werden kann?

Der Regisseur Peter Ohlendorf kommt mit dem Film im Rahmen der kritischen Einführungswoche nach Leipzig und wird im Anschluss Fragen beantworten als auch Hintergründe zur Entstehung des Films erklären

Weitere Infos https://www.filmfaktum.de/blut-muss-fliessen/