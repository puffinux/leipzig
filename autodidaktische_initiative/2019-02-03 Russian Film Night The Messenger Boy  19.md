---
id: '603095000138255'
title: 'Russian Film Night: The Messenger Boy // 19:00'
start: '2019-02-03 19:00'
end: '2019-02-03 22:00'
locationName: Autodidaktische Initiative
address: 'Georg-Schwarzstr. 19, 04177 Leipzig'
link: 'https://www.facebook.com/events/603095000138255'
image: null
teaser: null
recurring: null
isCrawled: true
---
