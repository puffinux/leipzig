---
id: "388040511786145"
title: LK – Das Kapital, Band 1
start: 2019-10-29 18:30
end: 2019-10-29 21:30
locationName: Autodidaktische Initiative
address: Georg-Schwarzstr. 19, 04177 Leipzig
link: https://www.facebook.com/events/388040511786145/
image: 69691869_3330843410274159_8063773022038786048_n.jpg
teaser: Da die kapitalistischen Verhältnisse impertinenter Weise auch gut 150 Jahre
  nach Erscheinen des marxschen Hauptwerkes weiterhin Bestand haben, haben w
isCrawled: true
---
Da die kapitalistischen Verhältnisse impertinenter Weise auch gut 150 Jahre nach Erscheinen des marxschen Hauptwerkes weiterhin Bestand haben, haben wir uns entschlossen einen neuen Kapital-Lesekreis ins Leben zu rufen um diesen zumindest mit den Waffen der Kritik etwas entgegen zusetzten.

Im Vordergrund soll selbstredend die  gemeinsame Lektüre des 1. Bandes  der „Kritik der politischen Ökonomie“ stehen, wobei bei Bedarf ergänzende Texte zur Klärung inhaltlicher Fragen und theoretischem Kontext zur Hilfe genommen werden können. Im Sinne der Autodidaktik soll sich der Lesekreis in der Gruppe selbst organisieren. 

Das Angebot richtet sich sowohl an Einsteiger*innen, als auch Interessierte mit Vorkenntnissen. Wir hoffen dabei auf einen beidseitig fruchtbaren Austausch, während wir eine Atmosphäre einseitiger Belehrung oder der Durchsetzung dogmatischer Deutungshoheiten vermeiden wollen.

Hierfür wurde am 14. Mai um 18:30 zu einem Vortreffen eingeladen. Seitdem trifft sich der LK regelmäßig.

Bei Interesse wendet euch bitte an das adi-Team.
Wir klären, ob ein Einstieg noch möglich/sinnvoll ist.