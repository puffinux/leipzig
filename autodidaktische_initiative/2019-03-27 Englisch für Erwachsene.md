---
id: '2080513898736259'
title: Englisch für Erwachsene
start: '2019-03-27 19:00'
end: '2019-03-27 20:00'
locationName: Autodidaktische Initiative
address: 'Georg-Schwarzstr. 19, 04177 Leipzig'
link: 'https://www.facebook.com/events/2080513898736259/'
image: 53001521_2837570496268122_3813760903912882176_n.jpg
teaser: 'Egal ob im täglichen Leben, in der Arbeit oder in den neuen Medien, es gibt kaum noch einen Bereich, in dem keine Englischkenntnisse benötigt werden.'
recurring: null
isCrawled: true
---
Egal ob im täglichen Leben, in der Arbeit oder in den neuen Medien, es gibt kaum noch einen Bereich, in dem keine Englischkenntnisse benötigt werden. Aus diesem Grund bieten wir ab dem 6.März 2019 einen Englischkurs für Erwachsene. 

Das Angebot richtet sich sowohl an Menschen ohne jede Vorkenntnisse als auch an solche mit unzureichendem oder eingerostetem Schulenglisch. 

Der Kurs ausschließlich spendenfinanziert und ohne Voranmeldung offen für alle.