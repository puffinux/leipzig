---
id: '303532323694804'
title: Küfa and movie - Waltz with Bashir
start: '2019-03-01 20:00'
end: '2019-03-01 22:00'
locationName: Autodidaktische Initiative
address: 'Georg-Schwarzstr. 19, 04177 Leipzig'
link: 'https://www.facebook.com/events/303532323694804/'
image: 53194170_2837426056282566_6918476140944293888_n.jpg
teaser: 'Liebe Küfa-Freund*innen,  diesen Freitag gibt es wie gewohnt ab 19Uhr lecker veganes Essen gegen Spende und danach erwartet euch der Film "Waltz with'
recurring: null
isCrawled: true
---
Liebe Küfa-Freund*innen,

diesen Freitag gibt es wie gewohnt ab 19Uhr lecker veganes Essen gegen Spende und danach erwartet euch der Film "Waltz with Bashir":

In einer Bar erzählt ein alter Freund dem Regisseur Ari eines Nachts von einem immer wiederkehrenden Albtraum, in dem er von 26 dämonischen Hunden gejagt wird. Jede Nacht, immer genau 26 Bestien. Die beiden Männer kommen zu dem Schluss, dass ein Zusammenhang zu ihrem Einsatz im ersten Libanonkrieg bestehen muss. Ari ist überrascht, denn er hat jegliche Erinnerung an diese Zeit verloren.

Erscheinungsdatum: 6. November 2008 (Deutschland)
Regisseur: Ari Folman

Filmstart gegen 20:00 und 20:30Uhr.