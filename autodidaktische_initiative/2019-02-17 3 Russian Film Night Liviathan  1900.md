---
id: '377031439760563'
title: '3 Russian Film Night: Liviathan // 19:00'
start: '2019-02-17 19:00'
end: '2019-02-17 22:00'
locationName: Autodidaktische Initiative
address: 'Georg-Schwarzstr. 19, 04177 Leipzig'
link: 'https://www.facebook.com/events/377031439760563/'
image: 51939083_2803646902993815_1689499083225432064_n.jpg
teaser: 'On Sunday, you are invited to watch the film „Leviathan“ directed by Andrey Zvyagintsev as a part of „Russian winter: movie festival“. It’s about to s'
recurring: null
isCrawled: true
---
On Sunday, you are invited to watch the film „Leviathan“ directed by Andrey Zvyagintsev as a part of „Russian winter: movie festival“. It’s about to show some films, which deal with more or less contemporary Russian reality and get a view of current status of different parts of everyday and social life and their conflicts.
Original Russian/English subtitles

Plot: In a Russian coastal town, Kolya is forced to fight the corrupt mayor when he is told that his house will be demolished. He recruits a lawyer friend to help, but the man’s arrival brings further misfortune for Kolya and his family.