---
id: "380795319529900"
title: Lesung mit PMS
start: 2019-10-18 19:00
end: 2019-10-18 22:00
locationName: Autodidaktische Initiative
address: Georg-Schwarzstr. 19, 04177 Leipzig
link: https://www.facebook.com/events/380795319529900/
image: 71171197_3399455580079608_8653863049897181184_o.jpg
teaser: Wir laden euch zu einer Lesung mit PMS ein, gemütliche Wohnzimmeratmosphäre
  und leckeres zu Futtern garantiert!  PMS ist die Post Migrantische Störung
isCrawled: true
---
Wir laden euch zu einer Lesung mit PMS ein, gemütliche Wohnzimmeratmosphäre und leckeres zu Futtern garantiert!

PMS ist die Post Migrantische Störung – Wir lesen eigene Texte, Kurzgeschichten und Gedichte, Dramen und Aufsätze über eigene und ausgedachte Situationen, Ideen und Erlebnisse aus unterschiedlichen Stimmungen, mit verschiedenen Gefühlen und von mehreren Stimmen – meistens auf deutsch, aber nicht nur.
