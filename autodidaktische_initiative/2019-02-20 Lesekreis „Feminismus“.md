---
id: '334959207282374'
title: Lesekreis „Feminismus“
start: '2019-02-20 19:00'
end: '2019-02-20 21:30'
locationName: null
address: Leipzig
link: 'https://www.facebook.com/events/334959207282374/'
image: 45304345_2586340248057816_6322844613136613376_n.jpg
teaser: '+++ ACHTUNG AKTUELL TRIFFT DER LK SICH AUS ORGANISATORISCHEN GRÜNDEN NICHT MEHR IN DER ADI +++  + BEI INTERESSE BITTE HIER MELDEN: jana@adi-leipzig.ne'
recurring: null
isCrawled: true
---
+++ ACHTUNG AKTUELL TRIFFT DER LK SICH AUS ORGANISATORISCHEN GRÜNDEN NICHT MEHR IN DER ADI +++

+ BEI INTERESSE BITTE HIER MELDEN: jana@adi-leipzig.net +

Anfangs in Zusammenarbeit mit der feministischen Bibliothek Leipzig „MONAliesA“ und der engagierten Hilke findet seit Ende Oktober 2017 der „Lesekreis Feminismus“ in der adi statt. Mittlerweile ist die Gruppe um den LK autonom und sucht sich selbstständig spannende Texte zum Thema. 

Wir möchten gerne eine Gruppe für Frauen* eröffnen, die sich für Feminismus interessieren, aber bisher noch nicht so recht Anschluss gefunden haben. Bei unseren Treffen wollen wir gemeinsam Texte lesen und ins Gespräch kommen. Dabei wollen wir die Theorie mit dem eigenen Erleben verbinden und in unserem Alltag aufspüren. Wenn ihr also Lust habt aktuelle feministische Texte zu lesen, euch eine Meinung dazu zu bilden, euch in einer Gruppe auszutauschen, das Gelesene mit euren eigenen Erfahrungen zu verknüpfen und die adi und gleichgesinnte Frauen* kennen zu lernen, seid ihr hier genau richtig! Es werden keine Vorkenntnisse erwartet.

Die Treffen finden jeden 1. und 3. Mittwoch 19:00 bis 20:30 Uhr in der adi oder andernorts statt. Für jedes Treffen ist ein Textauszug von ca. 20 Seiten zur Vorbereitung zu lesen. 

Lesekreissprache: Deutsch

=======================================

Bisher u.a. gelesene Textauswahl:

25.10.2017 Bin ich Feministin?
Text aus: Roxane Gay „Bad Feminist“

08.11.2017 Was ist ein „schöner“ Frauen*körper?
Text aus: Laurie Penny „Fleischmarkt“

22.11.2017 Wie selbstbestimmt ist meine Sexualität?
Text aus: Margarete Stokowski „Untenrum frei“

13.12.2017 Muss ich Mutter werden?
Text aus: Orna Donath „Regretting Motherhood“

10.01.2018 Wie gehen Feminismus und Marxismus zusammen?
Text: Frigga Haug „Marxismus-Feminismus – Ein Projekt“ (in „Feminismen Heute“)

24.01.2018 Ist Selfcare eine feministische Position?
Texte: vom Blog Mädchenmannschaft „Was sind queer-feministische caring* Communities?“
Zine „Self Care“