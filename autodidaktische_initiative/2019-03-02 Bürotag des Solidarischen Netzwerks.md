---
id: '280526495855800'
title: Bürotag des Solidarischen Netzwerks
start: '2019-03-02 12:00'
end: '2019-03-02 14:00'
locationName: Autodidaktische Initiative
address: 'Georg-Schwarzstr. 19, 04177 Leipzig'
link: 'https://www.facebook.com/events/280526495855800/'
image: 36380535_2327073560651154_1014132260661624832_n.jpg
teaser: 'Solidarisches  Netzwerk – gegenseitige Hilfe bei Stress mit Chefs, Behörden und Vermietern  Jede gerade Woche am Montag.  Das Solidarische Netzwerk Le'
recurring: null
isCrawled: true
---
Solidarisches  Netzwerk – gegenseitige Hilfe bei Stress mit Chefs, Behörden und Vermietern

Jede gerade Woche am Montag.

Das Solidarische Netzwerk Leipzig ist ein Zusammenschluss von Menschen, die keine Lust mehr darauf haben ähnliche Probleme vereinzelt lösen zu müssen. Stress mit Vermieter*innen,Chef*innen und Sachbearbeiter*innen kennen wir alle. Unsere Probleme sind jedoch nicht individuell verschuldet, sie sind nicht das Ergebnis unseres Versagens. Vielmehr sind sie Ausdruck dieser Gesellschaft, die auf Konkurrenz und Ausbeutung beruht. Unser Netzwerk lebt aus den gemeinsamen Erfahrungen und Berichten der eigenen Betroffenheit. Im regelmäßig stattfindenden Anlaufpunkt tragen wir unser Wissen zusammen und diskutieren auf Augenhöhe kreative Möglichkeiten, unsere eigenen Interessen einzufordern und durchzusetzen. Das Ausfüllen von Anträgen, Schreiben von Widersprüchen und Formulieren von Forderungen sind lästige Angelegenheiten, die wir aus der Vereinzelung heraus holen wollen. Unser gemeinsamer Bürotag bietet dafür genug Möglichkeiten, gemütlich bei Kaffee und Tee den Papierberg abzuarbeiten. Lasst uns mit dem Solidarischen Netzwerk [Leipzig] eine handlungsfähige Struktur schaffen, die bei der eigenen Betroffenheit ansetzt und Schritte wagt hin zu einer frei(er)en Gesellschaft!

In gemütlicher und ruhiger Atmosphäre machen wir unseren Papierkram, recherchieren in Ratgebern oder basteln an Webseiten. WLAN, Drucker, Kopierer, Kaffee und Tee gibt es vor Ort.

Alle aktuellen Termine und Informationen findet ihr auf solidarisch.blogsport.eu