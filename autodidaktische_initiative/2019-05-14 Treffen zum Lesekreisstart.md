---
id: '2284743045122803'
title: Treffen zum Lesekreisstart
start: '2019-05-14 16:30'
end: '2019-05-14 19:30'
locationName: Autodidaktische Initiative
address: 'Georg-Schwarzstr. 19, 04177 Leipzig'
link: 'https://www.facebook.com/events/2284743045122803/'
image: 59502799_2988435081181662_8000297567227215872_n.jpg
teaser: Lesekreis – Das Kapital I. Band  Da die kapitalistischen Verhältnisse impertinenter Weise auch gut 150 Jahre nach Erscheinen des marxschen Hauptwerkes
recurring: null
isCrawled: true
---
Lesekreis – Das Kapital I. Band

Da die kapitalistischen Verhältnisse impertinenter Weise auch gut 150 Jahre nach Erscheinen des marxschen Hauptwerkes weiterhin Bestand haben, haben wir uns entschlossen einen neuen Kapital-Lesekreis ins Leben zu rufen um diesen zumindest mit den Waffen der Kritik etwas entgegenzusetzten.