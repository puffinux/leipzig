---
id: '1964300850362075'
title: 5 Jahre adi- Geburtstagsparty!
start: '2019-04-12 16:00'
end: '2019-04-14 04:00'
locationName: Autodidaktische Initiative
address: 'Georg-Schwarzstr. 19, 04177 Leipzig'
link: 'https://www.facebook.com/events/1964300850362075/'
image: 55944971_2913292975362540_7357245236784922624_o.jpg
teaser: '//english below  Die adi wird am 12.4. fünf Jahre alt - Yeeeeeeah, Wohooo! Und das wollen wir mit euch feiern!  Am Freitag, 12.4. haben wir Folgendes'
recurring: null
isCrawled: true
---
//english below

Die adi wird am 12.4. fünf Jahre alt - Yeeeeeeah, Wohooo!
Und das wollen wir mit euch feiern!

Am Freitag, 12.4. haben wir Folgendes für euch:
- ab 16Uhr geöffnet - Ausstellung aus dem Archiv der adi, Adidoten, Tombola
- ab 18Uhr Küfa - wie gewohnt bestes Essen gekocht von Kevin
- ab 19Uhr open mic und Spiele! - wir stellen unsere Lieblingsbücher vor, bringt auch eure Texte, Lieder, Theatereinlagen mit (selbst geschrieben oder nicht), Karten- und Brettspiele gibts auch!

Am Samstag gibts feinsten Hip Hop auf die Ohren!
- Open Mic mit DJ My Hoodie Is My Castle an den Decks // Boom-Klatsch-Beatz// LE
- D-Vee (Rana Esculenta) // Rap // Leipzig
  https://www.youtube.com/watch?v=ALTRj9albJs

- Ket // Rap // Leipzig //
  https://www.youtube.com/watch?v=aS5orAAKkGA

- Secret Act // Rap // XY (UK) // SAMSTAG lösen wir das Geheimnis auf - WHOOP WHOOP!

- DJ Team Li-Vee (Mona Lina & D-Vee) // Hip Hop

- DJ Phadlip// glitchy bassmusic // LE // 
   https://soundcloud.com/phadlip

- DJ Duo Playback&Playback // 80's & 90's // LE

Einlass 21 Uhr, Beginn 22Uhr
Was zu Futtern und zu Trinken gibts selbstverständlich auch;)
///
Adi turs 5 on the 12th of April - Yeeeeah, Wohooooo!
And we want to celebrate with you!

On Friday we have for you...
- from 4 pm open - exhibition from our archive, Adidotes, tombola
- from 6 pm - like always excellent food, cooked by Kevin
- from 7 pm open-mic and games! - we present our favorite books, feel free to bring your texts, songs, theater (whether written by yourself or not), card and board games also will be availale!

On Saturday there will be finest Hip Hop!

- Open Mic with DJ My Hoodie Is My Castle an den Decks // Boom-Klatsch-Beatz// LE
- D-Vee (Rana Esculenta) // Rap // Leipzig
- Ket // Rap // Leipzig
- Secret Act // Rap // XY (UK) // SAMSTAG lösen wir das Geheimnis auf - WHOOP WHOOP!
- DJ Team Li-Vee (Mona Lina & D-Vee) // Hip Hop
- DJ Phadlip// glitchy bassmusic // LE
- DJ Duo // 80's & 90's // LE

doors 9 pm, start 10 pm
Of course, there will be something to eant and drink ;)
.................
Die adi ist kein Ort für rassistische, sexistische, homo- und transfeindliche Positionen und anderes diskriminierendes Verhalten! Entsprechendes Gerede und Gehabe wird nicht geduldet.
///
The adi is no place for racist, sexist, antisemitic, homo- and transphobic positions and other discriminatory behavior! Such will not ne tolerated.