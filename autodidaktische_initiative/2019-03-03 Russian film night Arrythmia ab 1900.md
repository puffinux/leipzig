---
id: '885679691602307'
title: 'Russian film night: Arrythmia ab 19:00'
start: '2019-03-03 19:00'
end: '2019-03-03 21:30'
locationName: Autodidaktische Initiative
address: 'Georg-Schwarzstr. 19, 04177 Leipzig'
link: 'https://www.facebook.com/events/885679691602307/'
image: 52397758_2827168313975007_4038321460803010560_n.jpg
teaser: 'On Sunday, 03.03 for the last time you are invited to watch the film "Arrhythmia" directed by Boris Khlebnikov as a part of "Russian winter: movie fes'
recurring: null
isCrawled: true
---
On Sunday, 03.03 for the last time you are invited to watch the film "Arrhythmia" directed by Boris Khlebnikov as a part of "Russian winter: movie festival". It's about to show some films, which deal with more or less contemporary Russian reality and get a view of current status of different parts of everyday and social life and their conflicts. 

Original Russian/English subtitles 
 
Plot: Oleg is a young gifted paramedic. His wife Katya, a doctor, works at the hospital emergency department. She loves Oleg, but is fed up with him caring more about patients than her. She tells him she wants a divorce. The new head of Oleg's EMA substation is a cold-hearted manager who's got new strict rules to implement. Oleg couldn't care less about the rules - he's got lives to save. His attitude gets him in trouble with the new boss. The crisis at work coincides with the personal life crisis. Caught between emergency calls, alcohol-fueled off-shifts, and search for a meaning in life, Oleg and Katya have to find the binding force that keeps them together.