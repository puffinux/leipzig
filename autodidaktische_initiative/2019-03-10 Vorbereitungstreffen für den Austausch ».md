---
id: '336254333671748'
title: Vorbereitungstreffen für den Austausch »laboro ergo sum«
start: '2019-03-10 14:00'
end: '2019-03-10 18:00'
locationName: null
address: Autodidaktische Initiative
link: 'https://www.facebook.com/events/336254333671748/'
image: 53121362_2606910239326059_3500438186356637696_o.jpg
teaser: »laboro ergo sum« ist ein Erasmus+ Austauschprojekt zwischen der Autodidaktische Initiative in Kooperation mit Unofficial Pictures und dem Videolabor
recurring: null
isCrawled: true
---
»laboro ergo sum« ist ein Erasmus+ Austauschprojekt zwischen der Autodidaktische Initiative in Kooperation mit Unofficial Pictures und dem Videolabor Blob.lgc Pratiche di Evasione aus Terni, Italien in Kooperation mit dem Kulturzentrum Centro Di Palmetta.

Zwei Wochen lang (vom 5. – 17. April) wollen uns mittels Fotografie und Film mit dem Thema „Arbeit“ beschäftigen, das kann gern in alle Richtungen des Begriffs gehen. Natürlich wollen wir auch eine schöne Zeit miteinander verbringen und als Gruppe zusammenwachsen. Es sind einige Ausflüge in und um Leipzig geplant. Auch das Mittag- und Abendessen werden wir in diesen Wochen gemeinsam kochen und verputzen.

Du hast keine Erfahrung mit Fotografie und Film? Das stört keinen großen Geist – was zählt ist, dass du Lust hast! Für Material wie Kameras oder Notizbücher ist gesorgt. Wenn du eine Kamera hast kannst du sie mitbringen und damit arbeiten. Wenn du genug Platz hast eine*n italienische*n Asutauschpartner*in aufzunehmen wäre das super, ist aber kein Muss. Eine Kinderbetreuung kann für Eltern* ermöglicht und finanziert werden! Generell funktioniert die Teilnahme gegen Spende, da wir eine Förderung von Erasmus+ bekommen haben. Ihr solltet nur (fast) über die ganze Zeit (5. – 17. April) Zeit haben und verbindlich zusagen.
Diesen Sonntag (10. März) wollen wir euch mehr Infos zum Austausch geben und ihr könnt euch kurz entschlossen aber verbindlich anmelden. Wenn ihr am Sonntag nicht könnt aber am Austausch teilnehmen wollt, schreibt uns schnell eine Mail an leipzig@laboroergosum.eu, da wir nur noch ein paar Plätze frei haben.

Folgendes Programm haben wir für den Vorbereitungsworkshop geplant:

14.00 Beginn, durch gegenseitiges Fragebögen ausfüllen kennenlernen
15.20 Infos zur adi, zur Partnerorga und vor allem zum Programm des Austauschs und logistischen Fragen
16.15 Interviews zum Thema Arbeit (finden während dem Austausch statt) vorbereiten
17.25 offene Fragen, Teilnahme verbindlich machen
17.45 Feedback
18.00 Schluss

Dazwischen gibts natürlich immer mal Pause. Mag vielleicht jemand Kuchen mitbringen?
Das Treffen findet in der adi statt (Autodidaktische Initiative e.V., Georg-Schwarz-Str. 19 in Lindenau, gleichnamige Haltestelle der Linie 7).
