---
id: "474873423066868"
title: Englisch für Erwachsene
start: 2019-11-20 19:00
end: 2019-11-20 20:30
locationName: Autodidaktische Initiative
address: Georg-Schwarzstr. 19, 04177 Leipzig
link: https://www.facebook.com/events/474873423066868/
image: 69604843_3330638390294661_3639602737429086208_n.jpg
teaser: Egal ob im täglichen Leben, in der Arbeit oder in den neuen Medien, es gibt
  kaum noch einen Bereich, in dem keine Englischkenntnisse benötigt werden.
isCrawled: true
---
Egal ob im täglichen Leben, in der Arbeit oder in den neuen Medien, es gibt kaum noch einen Bereich, in dem keine Englischkenntnisse benötigt werden. Aus diesem Grund bieten wir ab dem 6.März 2019 einen Englischkurs für Erwachsene. 

Das Angebot richtet sich sowohl an Menschen ohne jede Vorkenntnisse als auch an solche mit unzureichendem oder eingerostetem Schulenglisch. 

Der Kurs ausschließlich spendenfinanziert und ohne Voranmeldung offen für alle.