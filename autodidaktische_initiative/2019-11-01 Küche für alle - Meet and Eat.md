---
id: "977557425945166"
title: Küche für alle - Meet and Eat
start: 2019-11-01 19:00
end: 2019-11-01 22:00
locationName: Autodidaktische Initiative
address: Georg-Schwarzstr. 19, 04177 Leipzig
link: https://www.facebook.com/events/977557425945166/
image: 69805790_3330643443627489_3115686915672637440_n.jpg
teaser: Unsere nächste Küfa steht an. Wir laden euch immer freitags ab 19 Uhr herzlich
  zu veganen Köstlichkeiten ein! Kommt gerne vorbei und schmaust mit uns.
isCrawled: true
---
Unsere nächste Küfa steht an. Wir laden euch immer freitags ab 19 Uhr herzlich zu veganen Köstlichkeiten ein!
Kommt gerne vorbei und schmaust mit uns.
Getränke gibt es gegen Spende.
Im Anschluss laden wir zum geselligen Miteinander ein und wechseldem Programm.

+++

Our next Küfa will be this friday.
Every friday from 7pm we invite you to vegan,self-made food!
Come round and eat with us.
There are drinks for donation.
After the Küfa you are invited for sitting together and talk and programm what changes.