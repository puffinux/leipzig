---
id: '775041189547950'
title: 'Offener Workshop: Wie unterrichte ich Deutsch als Fremdsprache?'
start: '2019-02-23 14:00'
end: '2019-02-23 17:00'
locationName: Autodidaktische Initiative
address: 'Georg-Schwarzstr. 19, 04177 Leipzig'
link: 'https://www.facebook.com/events/775041189547950/'
image: 52595914_2818880054803833_3662765036494913536_n.jpg
teaser: 'In dem Workshop lernen wir gemeinsam mit professioneller Anleitung, wie man mit wenig Aufwand erfolgreichen DaF-Unterricht gestalten kann. Er bietet a'
recurring: null
isCrawled: true
---
In dem Workshop lernen wir gemeinsam mit professioneller Anleitung, wie man mit wenig Aufwand erfolgreichen DaF-Unterricht gestalten kann. Er bietet auch eine Gelegenheit, die adi kennen zu lernen.

Das DaF-Team des selbstorganisierten Lernangebots sucht Verstärkung beim ehrenamtlichen Unterrichten.

Alles basiert auf Freiwilligkeit und ist kostenfrei. Wenn Du dir also vorstellen könntest, ein paar Mal im Monat beim Unterricht mitzumachen, wäre das spitze! Du bist aber natürlich auch herzlich willkommen, wenn Du einfach nur mal reinschauen und etwas dazulernen möchtest.

Kontakt: <daf@adi-leipzig.net>