---
id: '2140508712711089'
title: Luft zum Atmen - Film & Gespräch über Widerstand im Betrieb
start: '2019-06-13 19:00'
end: '2019-06-13 21:30'
locationName: die naTo
address: 'Karl-Liebknecht-Straße 46, 04275 Leipzig'
link: 'https://www.facebook.com/events/2140508712711089/'
image: 56881375_2275983592657523_5174584977701994496_n.jpg
teaser: Filmvoführung in Anwesenheit von Protagonist*innen und der Regisseurin Johanna Schellhagen. Mit anschließendem Gespräch.  1972 gründeten ein paar Arbe
recurring: null
isCrawled: true
---
Filmvoführung in Anwesenheit von Protagonist*innen und der Regisseurin Johanna Schellhagen. Mit anschließendem Gespräch.

1972 gründeten ein paar Arbeiter und Revolutionäre bei Opel in Bochum die "Gruppe oppositioneller Gewerkschafter"(GoG). Die GoG existierte über 40 Jahre und hat mit ihrer radikalen Betriebsarbeit den Widerstandsgeist in der Bochumer Belegschaft befeuert.

Als Betriebsräte gaben sie geheime Informationen an die Belegschaft weiter, sie sorgten für achtstündige Betriebsversammlungen, kämpften gegen Krankenverfolgung, organisierten ihren eigenen Bildungsurlaub und versuchten sogar, auf eigenen Faust direkte internationale Solidarität zwischen den verschiedenen General Motors Belegschaften in Europa herzustellen, um sich gegen die Standorterpressungen in den '90er Jahren zur Wehr zu setzen.

Ihre radikalen Aktivitäten kulminierten schließlich im wichtigsten Wilden Streik der deutschen Nachkriegsgeschichte, als die Belegschaft im Oktober 2004 sechs Tage lang das Werk besetzte und die Produktion in ganz Europa lahmlegte.

Ein Portrait von Kollegen, die sich Gehör verschafften. 

Eine Veranstaltung von translib Leipzig, in Kooperation mit Cinémathèque Leipzig e.V. und der linken Wochenzeitung Jungle World.

Credits
Luft zum Atmen. D 2019, 70 min, ein Film von labournet.tv
Regie, Ton & Schnitt: Johanna Schellhagen, Kamera: Thilo Schmidt, Milica Denic, Zara Zandieh, Mischung: Birte Gerstenkorn & Joel Vogel, Grafik: Zoff Kollektiv, Musik: Tomi Simatupang & AK Kessel, Animation: Julien Bach, Verleih: Sabcat Media
Produktion: labournet.tv 

