---
id: '922928688041947'
title: Frauen in der Novemberrevolution - Lesung mit Dania Alasti
start: '2019-03-23 20:00'
end: '2019-03-23 23:00'
locationName: translib
address: 'Goetzstraße 7, 04177 Leipzig'
link: 'https://www.facebook.com/events/922928688041947/'
image: 53561717_2255714618017754_6552823533263650816_n.jpg
teaser: Buchvorstellung »Frauen der Novemberrevolution / Kontinuitäten des Vergessens« von Dania Alasti.  Frauen protestierten vor hundert Jahren in Massen ge
recurring: null
isCrawled: true
---
Buchvorstellung »Frauen der Novemberrevolution / Kontinuitäten des Vergessens« von Dania Alasti.

Frauen protestierten vor hundert Jahren in Massen gegen den Ersten Weltkrieg und das deutsche Kaiserreich. Ihre Streiks, Demonstrationen und Ausschreitungen leisteten einen wesentlichen Beitrag zur Vorbereitung der Novemberrevolution. Doch während der Formung und Kämpfe um die Richtung der Revolution tauchten Frauen als Massenerscheinung nicht mehr auf. 
Das Buch ist eine Suche nach den Spuren, die uns von den revoltierenden Frauen geblieben sind. In ihren Proteste zeigten sich Konflikte, die in der spezifischen Rolle von Frauen als Versorgerinnen angelegt sind. Ein Unverständnis gegenüber diesen Konflikten erschwerte die Bildung politischer Organe, in denen diese Frauen ihre Wünsche in Programmen hätten artikulieren können. Stattdessen wurden sie von Zeitgenossen verdrängt und von der Geschichtsschreibung vergessen.
Dieses Unverständnis ist bestehen geblieben. Nach wie vor artikulieren sich Konflikte in Protesten, wie gegenwärtig die weltweiten Frauen*streikbewegungen zeigen. Dabei ist es kein Zufall, dass sowohl die ökonomische Rolle der Versorgungsarbeiten, die Verfügung über den weiblichen Körper, als auch Gewalt gegen Frauen* Themen der Bewegungen sind. Sie beruhen auf der widersprüchlichen Einschreibung der Rolle von Frauen in die Gesellschaft, deren Tätigkeiten auf unsichtbare und unverstandene Weise zur Verfügung stehen sollen.
Dabei zeigen Beobachtungen schreibender Frauen zur Kriegsbegeisterung des Ersten Weltkrieges, dass eine falsche Einigkeit der Gesellschaft über bestehende Konflikte hinweg hergestellt wurde, indem die Wut aus den Konflikten auf ein vermeintliches Außen projiziert wurde. Für Frauen  zeigten sich die Konsequenzen dieser falschen Einigkeit nicht nur während des Ersten Weltkrieges, sondern auch in der konterrevolutionären Gewalt der Freikorps, die sich auch explizit gegen Frauen, die von der patriarchalen Ordnung abgewichen sind, richtete.
Das Buch soll nicht nur die weitestgehend unbekannte Geschichte der protestierenden Frauen sichtbar machen, sondern auch zu einem anderen historischen Verständnis führen, das Geschichte nicht in eine Linie zwingt, sondern Abspaltungen, sowie Brüche, Reaktionen und Wiederholungen wahrnimmt, und die Gründe dafür hinterfragt.