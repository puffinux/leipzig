---
id: '523624711712709'
title: Une situation excellente? Zur Gelbwestenbewegung in Frankreich
start: '2019-07-25 19:30'
end: '2019-07-25 22:00'
locationName: Falkenturm
address: 'Frauentormauer 3, 90402 Nuremberg'
link: 'https://www.facebook.com/events/523624711712709/'
image: 66455187_2698646353498043_6473643032318050304_n.jpg
teaser: Die translib aus Leipzig hat mit „Une situation excellente? Beiträge zu den Klassenauseinandersetzungen in Frankreich“ eine Broschüre zur Bewegung der
recurring: null
isCrawled: true
---
Die translib aus Leipzig hat mit „Une situation excellente? Beiträge zu den Klassenauseinandersetzungen in Frankreich“ eine Broschüre zur Bewegung der Gelbwesten in Frankreich herausgegeben. Neben den Ergebnissen ihrer eigenen Auseinandersetzung umfasst die Publikation auch Übersetzungen einiger bedeutender Beiträge aus der Debatte vor Ort sowie ausgewählter Texte aus der Bewegung selbst. 

Gemeinsam mit den Falken Nürnberg laden wir am 25. Juli in den Falkenturm, um die Thesen der Gelbwesten-Broschüre mit einem Genossen aus der translib zu diskutieren.

Aus der Release-Ankündigung der translib: 

„Als der französische Präsident Emmanuel Macron im Oktober 2018 eine Steuererhöhung auf Diesel und Benzin verkündete, rechnete wohl kaum jemand mit dem, was in den kommenden Monaten passieren sollte. Die Revolte der Gelbwesten traf nicht nur die Regierung völlig unerwartet, auch die traditionelle Linke wusste mit den Forderungen, Aktionsformen und Symboliken zunächst nichts anzufangen. Die Gilets Jaunes kamen buchstäblich aus dem Nichts: Sie nahmen ihren Ausgangspunkt an den verlassenen Kreisverkehren der französischen Peripherie, mobilisierten sich in den Untiefen der sozialen Netzwerke und verweigern sich weiterhin hartnäckig jeder Form der Repräsentation.

Was führte zu dieser unvorhergesehenen Explosion?

Um das herauszufinden, haben wir in den vergangenen Monaten eine Menge Texte zu den Gelbwesten gesichtet und gelesen. Einige haben wir ausgewählt und übersetzt und schließlich selbst einen längeren Aufsatz geschrieben. Parallel zur Arbeit an den Texten haben wir erste Resultate auf Veranstaltungen vorgetragen und natürlich während all dessen immer wieder über die Gelbwesten diskutiert.

Die Ergebnisse unserer Auseinandersetzung haben wir in einer Broschüre zusammengestellt: „Une situation excellente? Beiträge zu den Klassenauseinandersetzungen in Frankreich".
Nach einer kurzen Vorstellung der Broschüre, soll dargestellt werden, warum diese Bewegung so besonders ist und was die Linke möglicherweise daraus lernen kann.“