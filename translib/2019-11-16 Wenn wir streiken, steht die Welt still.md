---
id: "2557573477669105"
title: Wenn wir streiken, steht die Welt still
start: 2019-11-16 13:00
end: 2019-11-16 17:00
locationName: translib
address: Goetzstraße 7, 04177 Leipzig
link: https://www.facebook.com/events/2557573477669105/
image: 75231674_2422568584665689_7679891293253664768_n.jpg
teaser: Frauenstreik als beginnende proletarische Öffentlichkeit?   Workshop mit 3
  Genossinnen aus Hamburg  Die Teilnehmer_innenzahl für den Workshop ist begr
isCrawled: true
---
Frauenstreik als beginnende proletarische Öffentlichkeit? 

Workshop mit 3 Genossinnen aus Hamburg

Die Teilnehmer_innenzahl für den Workshop ist begrenzt. Anmelden könnt ihr euch per Mail an workshop_translib@gmx.net 

Nachdem bereits 2018 in verschiedenen Ländern der Welt riesige Frauenstreiks stattgefunden hatten, wurden dieses Jahr auch in Deutschland alle FLTI* zum politischen Streik aufgerufen. Am 08. März 2019 hoben die feministischen Streiks die Welt wieder ein Stück aus den Angeln: Weltweit streikten und demonstrierten Millionen Menschen gegen die Zurichtung der Geschlechter und die patriarchalen Verhältnisse im Job, in ihren Wohnungen und auf den Straßen. In Deutschland traten hunderttausend Menschen in den Ausstand und brachten die größten feministischen Demonstrationen seit mehr als über zehn Jahren auf die Straße; die Schweiz erlebte sogar die zweitgrößte politische Aktion ihrer Geschichte. Auch in Hamburg versammelten sich 10 000 trans*, inter und cis Frauen, um lautstark zu demonstrieren und zu singen, um AfD-Plakate zu entfernen, Straßen zu besetzen und mit ihren Freundinnen, Genossinnen und Nachbarinnen ins Gespräch zu kommen.

Die Aktionen am 08. März sollten das Patriachat auf verschiedenen Ebenen treffen, im Öffentlichen und im Privaten, und Druck aufbauen anstatt zu appellieren. In Deutschland wurde über Möglichkeiten des politischen Streiks diskutiert und dieser bis ins bürgerliche Lager hinein in Erwägung gezogen - tatsächlich gestreikt wurde aber so gut wie gar nicht. Dennoch sprach der Streikbegriff viele von uns an, sich schließlich auf unterschiedliche Art am 08. März zu beteiligen: Er ermöglichte, verschiedene feministische Themenfelder, die derzeit zwar wieder verstärkt sichtbar, aber dennoch oft als einzelne, isolierte Konflikte verhandelt werden, aufeinander zu beziehen. Das Soziale und Private wurden ganz selbstverständlich mitpolitisiert und, neben feminisierter Lohnarbeit, als Kernthemen der Streiks begriffen. Weiblichkeit wurde als konkrete Arbeitskategorie gefasst und eine kollektive Organisierung anhand dieser Arbeitsstrukturen und den damit einhergehenden Unterdrückungsverhältnissen versucht. Für viele von uns bedeutete das nicht nur eine Sichtbarmachung und Organisierung unserer Erfahrungen, sondern auch eine Form der Organisierung anhand unserer Lebensrealitäten, die uns ziemlich erstaunt und glücklich zurückließ und die über den Aktionstag 08. März hinausreicht.

Wir denken, dass die feministischen Streiks einen sozialrevolutionären Kern haben, der viel damit zu tun hat, wie unsere Erfahrungen in ihnen verhandelt werden, insbesondere in der Art, wie Erfahrung in der Öffentlichkeit organisiert oder auch nicht organisiert wird. Für den Workshop haben wir unsere Überlegungen zur feministischen Praxis um den 08. März in Hamburg systematisiert und noch einmal grundlegender über Aktivismus, feministische und linksradikale Praxis und Organisierung nachgedacht. Dazu haben wir einige interessante Ideen in linksradikalen Organisierungsdebatten und bei Oscar Negts und Alexander Kluges Theorie zu Öffentlichkeit und Erfahrung gefunden. Unsere Thesen möchten wir gerne mit euch diskutieren, um für kommende Auseinandersetzungen klüger zu werden.



