---
id: '323029018598658'
title: Die Gelbwesten und die Linke. Gespräch mit Pariser Genossen
start: '2019-06-21 19:00'
end: '2019-06-22 02:00'
locationName: null
address: Pracht
link: 'https://www.facebook.com/events/323029018598658/'
image: 62612699_2315110658744816_8424591025552490496_n.jpg
teaser: // Die Gelbwesten und die Linke // Gespräch mit einem Pariser Genossen über Versuche seiner Gruppe in der Gelbwesten-Bewegung zu wirken // Danach Bara
recurring: null
isCrawled: true
---
// Die Gelbwesten und die Linke
// Gespräch mit einem Pariser Genossen über Versuche seiner Gruppe in der Gelbwesten-Bewegung zu wirken
// Danach Barabend 
// In der Pracht

// Info

Als der französische Präsident Emmanuel Macron im Oktober 2018 eine Steuererhöhung auf Diesel und Benzin verkündete, rechnete wohl kaum jemand mit dem was in den kommenden Monaten passieren sollte.

Obwohl es sich ihrer sozialen Zusammensetzung nach um eine proletarische Bewegung handelt, fügen sich die Gelbwesten nicht ins eingeschliffene Muster gewerkschaftlicher und linker Proteste: Die notorischen Bewegungsverwalter wurden links liegen gelassen, wodurch niemand da war, der die Proteste in „geregelte Bahnen“ hätte lenken können.

Die Entzündung der Proteste an der sogenannten „Ökosteuer“ führte unter linken Beobachterinnen zur anfänglichen Einschätzung, es handele sich bei den Gelbwesten schlicht um eine Fortsetzung der rechten Mobilisierung gegen eine „links-grüne Ökodiktatur“.

Die ausbrechende Revolte der Gelbwesten traf also nicht nur die Regierung völlig unerwartet, auch die traditionelle Linke wusste mit den Forderungen, Aktionsformen und Symboliken zunächst nichts anzufangen. Es bedurfte erst der schwersten Krawalle seit Mai '68, bis Linke sich der Bewegung zuwandten und die Gelbwesten ernst nahmen.

In Paris beteiligten sich AktivistInnen der Gruppe "Platforme d‘Enquêtes Militantes" (Plattform militanter Untersuchungen) an den Versammlungen und Protesten der Gelbwesten. Mit einem der Beteiligten wollen wir über seine Einschätzung der Bewegung und daraus gezogene praktische Konsequenzen sprechen. Dabei sollen vor allem die verschiedenen Formen der Beteiligung an und der Intervention in die Bewegung diskutiert werden.

Im Anschluss an das Gespräch wird es Zeit für Fragen und Diskussion geben. Die Veranstaltung findet in deutscher Sprache statt.

Nach der Diskussion feiern wir das Erscheinen unserer Gelbwesten-Broschüre mit kalten Getränken und Auflegerei (New Wave). 

Unsere Broschüre "Une situation excellente? Beiträge zu den Klassenauseinandersetzungen in Frankreich" ist an dem Abend erhältlich.