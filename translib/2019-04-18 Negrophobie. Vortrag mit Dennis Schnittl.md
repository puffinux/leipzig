---
id: '603082460153643'
title: Negrophobie. Vortrag mit Dennis Schnittler
start: '2019-04-18 19:00'
end: '2019-04-18 23:00'
locationName: translib
address: 'Goetzstraße 7, 04177 Leipzig'
link: 'https://www.facebook.com/events/603082460153643/'
image: 51240512_2235127550076461_8243180866918940672_n.jpg
teaser: 'Negrophobie. Grundsätzliches zum Rassismus gegen schwarze Menschen  Vortrag und Diskussion mit Dennis Schnittler  Die stereotypen Ressentiments, die v'
recurring: null
isCrawled: true
---
Negrophobie.
Grundsätzliches zum Rassismus gegen schwarze Menschen

Vortrag und Diskussion mit Dennis Schnittler

Die stereotypen Ressentiments, die viele weiße Menschen gegen schwarze Menschen hegen und das Phänomen des anhaltenden, weit verbreiteten Rassismus überhaupt werden heutzutage primär soziologisch und psychologisch gedeutet. In den Vorstellungen, auch linker Antirassistinnen und Antirassisten, erscheint der Rassismus immer wieder als eine Art ‚toxischer Volksglaube’, als bloße Herrschaftsideologie, die mit engagierter Aufklärung und staatlichen Antidiskriminierungsmaßnahmen aus der Welt geschafft werden könnte. Dass dies so nicht funktionieren kann, zeigen die diversen, rassistisch motivierten Angriffe, z.B. gegen geflüchtete Menschen. Es reicht jedoch nicht aus, die traurige Don Quijoterie vieler bürgerlich-antirassistischer und linksradikaler Kampagnen zu bejammern und den mangelhaften Interpretationen des Phänomens zu widersprechen. Was zu bewerkstelligen wäre, ist zuvorderst eine materialistische und historische Untersuchung des negrophoben, bzw. rassistischen Syndroms, das über alle gesellschaftlichen Entwicklungen hinweg, in allen „zivilisierten“ Ländern des Westens, mindestens in den letzten 150 Jahren, in seinem Charakter weitgehend gleich geblieben ist.

Dabei sind zwei Fragen zentral:

1. Warum hat der Rassismus gegen Schwarze seine grundsätzlichen Wesenszüge beibehalten, obwohl sich die kapitalistische Gesellschaft immer wieder verändert hat und Schwarze inzwischen alle Rechte innehaben und nahezu alle Bastionen erobert haben, die zuvor häufig nur Weißen vorbehalten blieben?

2. Aus was besteht der gesellschaftliche Nährboden, aus dem sich die rassistisch-stereotypen Denkweisen speisen, die sich immer wieder (v.A. in Zeiten der persönlich erlebten, gesellschaftlichen Krise) zum mörderischen Hass aufpeitschen?

Dennis Schnittler ist Autor gesellschaftskritischer Texte und Vorträge. Er unterhält den Blog Marias First: https://mariasfirst.wordpress.com/.  Zuletzt beschäftigte er sich mit Verschwörungstheorien. Der Vortrag umfasst einen ökonomischen, einen psychoanalytischen und einen politischen Teil und dauert circa 120 Minuten. Zum Vortrag gehört ein ausführlicher Reader, den ihr auf unserem Blog bekommt: https://wordpress.com/post/translibleipzig.wordpress.com/2220  
Der Reader wird auch bei der Veranstaltung ausliegen. Nach dem Vortrag kann diskutiert werden.

Triggerwarnung/FSK: Der Vortrag enthält die Darstellung drastischer rassistischer und sexueller Gewalt und die Nennung diverser rassistischer Begriffe in Wort und Bild und ist deswegen nur für erwachsene Menschen ab dem 18. Lebensjahr geeignet. 

Gefördert vom Jugenbildungs-Initiativfonds der Rosa-Luxemburg-Stiftung 