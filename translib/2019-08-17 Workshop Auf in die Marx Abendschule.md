---
id: '942136979488064'
title: 'Workshop: Auf in die Marx Abendschule'
start: '2019-08-17 13:00'
end: '2019-08-17 19:00'
locationName: translib
address: 'Goetzstraße 7, 04177 Leipzig'
link: 'https://www.facebook.com/events/942136979488064/'
image: 68267395_382192552492940_1688752059563638784_o.jpg
teaser: 'Was machen gute Marxist*innen? Marx lesen, klar und deshalb machen wir auch nach dem ersten Band wieder weiter mit der Marx Abendschule und lesen zusa'
recurring: null
isCrawled: true
---
Was machen gute Marxist*innen? Marx lesen, klar und deshalb machen wir auch nach dem ersten Band wieder weiter mit der Marx Abendschule und lesen zusammen Band zwei des Kapitals. Wenn ihr schon immer mal wissen wolltet, wie es nach dem Cliffhanger-Ende in Band 1 weitergeht, seid ihr bei uns richtig. Wir haben zwar schon ein Stück von Band 2 gelesen, wollen es Neu- und Quereinsteiger*innen aber ermöglichen noch dazu zu stoßen. Deshalb machen wir am 17. August einen Workshop, wo wir uns noch einmal wichtige Punkte der bisherigen Kapitel anschauen, Begriffe aus dem ersten Band wiederholen und versuchen zu verstehen, wie der zweite Band im Kapital-Werk einzuordnen ist. 

Wir richten uns hiermit an Leute, die interessiert sind an unseren Montagssitzungen (dreimal im Monat, 19h in der translib) regelmäßig Teil zu nehmen und die Lust haben, den Text in den Sitzungen aufzudröseln und durch offenes Fragen und Diskutieren zusammen zu verstehen. Die Kurzvorträge, die wir am Anfang jeder Sitzung abwechselnd halten, sind dazu gedacht, dass auch Teilnehmer*innen, die aufgrund von Lohnarbeit, familiären oder sonstigen Verpflichtungen wenig Zeit fürs Lesen haben, mit dabei bleiben können. Durch Workshops wie diesen wollen wir zudem ein Überblicksverständnis der drei Bände fördern.

Eine Besonderheit der Marx Abendschule, die es seit Januar 2018 in Leipzig gibt, ist außerdem, dass wir die nach wie vor männlich dominierte Kapitallektüre aufbrechen wollen. Darum ist die Teilnehmer*innenzahl nach Geschlecht quotiert. Wir bemühen uns auch um eine profeministische Diskussionsatmosphäre. 

Sagt euch all das zu, dann schreibt uns gerne eine Mail und meldet euch für den Workshop an, es wird deliziöse Snacks geben und anregende Gespräche über den Evergreen «Das Kapital — Band 2»!

anmeldung@kapitalviernull.info 