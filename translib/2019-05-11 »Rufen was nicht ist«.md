---
id: '2152836044762571'
title: »Rufen was nicht ist«
start: '2019-05-11 17:00'
end: '2019-05-11 19:00'
locationName: null
address: 'Eisenbahnstraße 176, 04315 Leipzig'
link: 'https://www.facebook.com/events/2152836044762571/'
image: 55931147_2192886920799645_3087173508262789120_o.jpg
teaser: »Rufen was nicht ist« - Zur Utopie und ihrer Rolle für die Emanzipation - Vortrag von GenossInnen aus der translib  Emanzipatorische Bewegung zeichnet
recurring: null
isCrawled: true
---
»Rufen was nicht ist« - Zur Utopie und ihrer Rolle für die Emanzipation
- Vortrag von GenossInnen aus der translib

Emanzipatorische Bewegung zeichnet sich durch den Willen der Veränderung des herrschenden Zustandes aus. Die Kritik am Bestehenden, von der sie ausgeht, enthält immer schon ihre logische Gegenseite – so abstrakt sie auch sein mag – der Gedanke eines Anderen, Besseren. Die Diskussion darum, ob und – wenn ja – wie dieses bestimmt werden kann, ist daher in der Linken immer geführt worden: von der Kritik des »wissenschaftlichen Sozialismus« am »utopischen« Frühsozialismus zum sozialdemokratischen »Zukunftsstaat«, vom Rätekommunismus bis zu gegenwärtigen Ideen eines »Marktsozialismus« - die Bedeutung der Utopie als der Gestalt, in der das Bessere als Ganzes auftritt, für das Denken und die Praxis der Emanzipation spiegelt sich in der Bedeutung, die ihr in verschiedenen, mitunter verfeindeten linken Strömungen zugesprochen wird. Gemeinhin sind jedoch eine fundamentale Umwälzung der Produktionsverhältnisse beinhaltende Utopien heute weitestgehend diskreditiert.
 
Die Reflexion auf das bisherige Scheitern der revolutionären Linken zeigt aber auch, dass diese entweder alles in Schutt legen wollte, in der Behauptung des Besseren dem Schlechteren womöglich näher kam, oder dem Besseren unter Verweis auf die Unmöglichkeit es zu denken entsagte, um sich doch noch im Bestehenden einzurichten. Allen geht das Denken konkreter kommunistischer Forderungen ab. Dass es mit dem Verweis auf die Vergesellschaftung der Produktionsmittel nicht getan ist, weil keiner sagen kann, was sie mehr meint als die Negation des Privateigentums, hat Karl Korsch bereits vor mehr als hundert Jahren festgestellt.
 
Dies verweist uns heute auf die drängende Frage nach konkreter Utopie. Was ist sie und wie müsste sie gedacht werden, wenn sie nicht hinter berechtigter Kritik an abstraktem Utopismus zurückfallen soll? Wie kann sie vermeiden, bestehendes Elend gedanklich bloß zu verlängern? Wie kann sie konkretes Fordern ohne bloß Soziareformismus zu sein?
 
In unserem Vortrag wollen wir uns zunächst dem Begriff der Utopie unter Reflexion des Vorwurfs des Utopismus und des Bilderverbots annähern und ihn historisch verorten. Hier sollen auch die Debatten innerhalb der Arbeiterinnenbewegung und revolutionären Linken beleuchtet werden. Auf Basis dieser Reflexionen möchten wir moderne konkrete Utopien vorstellen, um dann mit euch ins Gespräch zu kommen.

-----------------------------------------------------------

Dieser Vortrag findet im Rahmen der Reihe "Can't Take My Eyes Off You. Ein Versuch utopisch zu denken" statt.

Einen Überblick über die weiteren Veranstaltungen findet ihr hier: https://www.facebook.com/events/2278745022177657/


____________________________
Gefördert durch:
Student_innenRat Universität Leipzig
Studentenwerk Leipzig 
Kulturamt der Stadt Leipzig
Gewerkschaft Erziehung und Wissenschaft GEW
Gerda-Weiler-Stiftung
Rosa-Luxemburg-Stiftung - Diese Steuermittel werden auf Grundlage des von den Abgeordneten des Sächsischen Landtags beschlossenen Haushaltes zur Verfügung gestellt.

Danke an:
Radio Corax