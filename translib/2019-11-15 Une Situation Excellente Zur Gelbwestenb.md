---
id: "2430673677193850"
title: Une Situation Excellente? Zur Gelbwestenbewegung in Frankreich
start: 2019-11-15 19:00
end: 2019-11-15 21:00
locationName: hauszwei
address: Friedrich-Engels-Straße 22, 14473 Potsdam
link: https://www.facebook.com/events/2430673677193850/
image: 75174684_578518962685419_2155792717500645376_n.jpg
teaser: "Une Situation Excellente?  Zur Gelbwestenbewegungen in Frankreich: Beiträge
  zu den Klassenauseinandersetzungen in Frankreich.  Die Translib Leipzig ha"
isCrawled: true
---
Une Situation Excellente? 
Zur Gelbwestenbewegungen in Frankreich: Beiträge zu den Klassenauseinandersetzungen in Frankreich.

Die Translib Leipzig hat eine Broschüre zur Bewegung der Gelbwesten in Frankreich herausgegeben. Wir haben die Gruppe zum einjährigen Jubiläum der Bewegung eingeladen, die Gelbwesten-Broschüre und ihre Thesen vorzustellen und sie mit uns zu diskutieren.

„Als der französische Präsident Emmanuel Macron im Oktober 2018 eine Steuererhöhung auf Diesel und Benzin verkündete, rechnete wohl kaum jemand mit dem, was in den kommenden Monaten passieren sollte. Die Revolte der Gelbwesten traf nicht nur die Regierung völlig unerwartet, auch die traditionelle Linke wusste mit den Forderungen, Aktionsformen und Symboliken zunächst nichts anzufangen. Die Gilets Jaunes kamen buchstäblich aus dem Nichts: Sie nahmen ihren Ausgangspunkt an den verlassenen Kreisverkehren der französischen Peripherie, mobilisierten sich in den Untiefen der sozialen Netzwerke und verweigern sich weiterhin hartnäckig jeder Form der Repräsentation.

Was führte zu dieser unvorhergesehenen Explosion? Um das 
herauszufinden, haben wir in den vergangenen Monaten eine 
Menge Texte zu den Gelbwesten gesichtet und gelesen. Einige 
haben wir ausgewählt und übersetzt und schließlich selbst einen 
längeren Aufsatz geschrieben. Parallel zur Arbeit an den Texten 
haben wir erste Resultate auf Veranstaltungen vorgetragen und 
natürlich während all dessen immer wieder über die Gelbwesten diskutiert. Die Ergebnisse unserer Auseinandersetzung haben wir in einer Broschüre zusammengestellt: „Une situation excellente? Beiträge zu den Klassenauseinandersetzungen in Frankreich“. Nach einer kurzen Vorstellung der Broschüre bringen wir einige Thesen zu zentralen Aspekten der Bewegung zu Gehör und umreißen die politischen Konsequenzen, die wir aus unserer Analyse ziehen. Diese Gedanken wollen wir mit euch diskutieren.“

https://translibleipzig.wordpress.com/2019/10/01/gelbwestenbroschuere-als-pdf-zum-download/

Freitag, 15. November 2019 | 19:00 Uhr | Café hausZwei
freiLand | Friedrich-Engels-Str. 22 | 14473 Potsdam
Campus Libertalia, Translib Leipzig und konsensnonsens