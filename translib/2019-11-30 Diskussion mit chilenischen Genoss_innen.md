---
id: "2447177832188192"
title: Wir werden nicht zur Normalität zurückkehren
start: 2019-11-30 19:30
end: 2019-11-30 23:30
locationName: translib
address: Goetzstraße 7, 04177 Leipzig
link: https://www.facebook.com/events/2447177832188192/
image: 77150163_2440020846253796_9110025620825309184_n.jpg
teaser: „Wir werden nicht zur Normalität zurückkehren, da die Normalität das Problem
  war“ Diskussionsabend mit chilenischen Genoss_innen zum Aufstand in Chile
isCrawled: true
---
„Wir werden nicht zur Normalität zurückkehren, da die Normalität das Problem war“
Diskussionsabend mit chilenischen Genoss_innen zum Aufstand in Chile

Seit Mitte Oktober kommt Chile nicht mehr zur Ruhe. Spontane Proteste von Schülerinnen und Studentinnen gegen eine Fahrpreiserhöhung vermochten es, mit der schrecklichen Normalität zu brechen, die viele Chileninnen seit Jahrzehnten verarmt und verschuldet in einer „Oase des Wachstums“ (Präsident Piñera) gefangen hält. Dieser Moment barg nun enorme Sprengkraft in sich. Den jungen Protesten gegen die Fahrpreiserhöhung schlossen sich sogleich Arbeiter_innen, ein urbanes Sub-Proletariat, die indigenen Mapuche, wie auch Teile der prekären Mittelschicht an. Aus einem limitierten Protest gegen Verteuerung wurde schnell eine soziale Mobilisierung gegen die gesamte politische Ordnung. Der Staat reagierte prompt und, wie in Chile üblich, mit brutaler Repression. So hat die Bewegung bereits zahlreiche Tote zu beklagen, ebenso wird über Folter, Vergewaltigungen und das Verschwindenlassen von Demonstranten berichtet. Die Regierung hat mittlerweile die Erhöhung zurückgezogen und weitere Zugeständnisse gemacht. In einem letzten Versuch, die Lage zu beruhigen, hat Piñera die Bildung einer von der Opposition geforderte verfassungsgebende Versammlung in Aussicht gestellt. Trotz der massiven staatlichen Gewalt und entgegen der Befriedungsversuche der Regierung halten die Proteste an und anstatt „zur Normalität zurückzukehren“ sind viele Leute weiterhin auf der Straße. Stellenweise haben sich lokale Versammlungen gebildet, die als Orte der Diskussion und der Selbstorganisation dienen.

Wir wollen gemeinsam mit chilenischen Genoss_innen aus Leipzig die Proteste in Chile in den Kontext der globalen Revolten einordnen, über die chilenische Normalität und die Proteste dagegen sprechen und uns insbesondere darüber austauschen, wie mit dieser Normalität gebrochen werden kann. Außerdem zeigen unsere Gäste ihren Kurzfilm „Chile in Flammen“, den sie anlässlich der Proteste produziert haben. 