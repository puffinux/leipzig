---
id: '452716525556129'
title: Une situation excellente? Release unserer Gelbwesten-Broschüre
start: '2019-06-20 19:00'
end: '2019-06-20 23:59'
locationName: translib
address: 'Goetzstraße 7, 04177 Leipzig'
link: 'https://www.facebook.com/events/452716525556129/'
image: 62043687_2314004788855403_2057516340985462784_n.jpg
teaser: 'Vorstellung der Broschüre „Une situation excellente?“, Diskussion und Barabend.  Als der französische Präsident Emmanuel Macron im Oktober 2018 eine S'
recurring: null
isCrawled: true
---
Vorstellung der Broschüre „Une situation excellente?“, Diskussion und Barabend.

Als der französische Präsident Emmanuel Macron im Oktober 2018 eine Steuererhöhung auf Diesel und Benzin verkündete, rechnete wohl kaum jemand mit dem, was in den kommenden Monaten passieren sollte. Die Revolte der Gelbwesten traf nicht nur die Regierung völlig unerwartet, auch die traditionelle Linke wusste mit den Forderungen, Aktionsformen und Symboliken zunächst nichts anzufangen.

Die Gilets Jaunes kamen buchstäblich aus dem Nichts: Sie nahmen ihren Ausgangspunkt an den verlassenen Kreisverkehren der französischen Peripherie, mobilisierten sich in den Untiefen der sozialen Netzwerke und verweigern sich weiterhin hartnäckig jeder Form der Repräsentation.

Was führte zu dieser unvorhergesehenen Explosion?

Um das herauszufinden, haben wir in den vergangenen Monaten eine Menge Texte zu den Gelbwesten gesichtet und gelesen. Einige haben wir ausgewählt und übersetzt und schließlich selbst einen längeren Aufsatz geschrieben. Parallel zur Arbeit an den Texten haben wir erste Resultate auf Veranstaltungen vorgetragen und natürlich während all dessen immer wieder über die Gelbwesten diskutiert. 

Die Ergebnisse unserer Auseinandersetzung haben wir in einer Broschüre zusammengestellt: „Une situation excellente? Beiträge zu den Klassenauseinandersetzungen in Frankreich“.

Heute abend wollen wir unsere frisch gedruckte Broschüre erstmals präsentieren und in Umlauf bringen. 

Nach einer kurzen Vorstellung der Texte bringen wir einige Thesen zu zentralen Aspekten der Bewegung zu Gehör und umreißen die politischen Konsequenzen, die wir aus unserer Analyse ziehen. 
Diese Gedanken wollen wir mit euch diskutieren.

Im Anschluss stoßen wir auf das Heft an. Santé!

***

Für den Folgetag planen wir eine weitere Veranstaltung zum Thema mit einem Gast aus Paris. Weitere Infos folgen.