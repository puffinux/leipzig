---
id: '513623929432770'
title: Aspekte der politischen Ökonomie Ostdeutschlands
start: '2019-07-10 19:00'
end: '2019-07-10 23:00'
locationName: translib
address: 'Goetzstraße 7, 04177 Leipzig'
link: 'https://www.facebook.com/events/513623929432770/'
image: 65501724_2327024384220110_8738199513052741632_n.jpg
teaser: 'Debatten über Ostdeutschland erreichen im Moment einen neuen Höhepunkt: AFD-Wahlerfolge, Forderungen nach einer Treuhand-Aufarbeitung und einer „Ossi-'
recurring: null
isCrawled: true
---
Debatten über Ostdeutschland erreichen im Moment einen neuen Höhepunkt: AFD-Wahlerfolge, Forderungen nach einer Treuhand-Aufarbeitung und einer „Ossi-Quote“ sowie das Sprechen über biographische Brüche der Nachwendezeit bestimmen teils die bundesweite Öffentlichkeit. Die Frage, allenthalben: Was ist eigentlich im Osten los?
Bei den Erklärungsversuchen nimmt eine polit-ökonomische Perspektive auf die ostdeutsche Teilgesellschaft eine eher randständige Stellung ein. Gerade hierbei zeigen sich jedoch handfeste Unterschiede zu Westdeutschland: Spezifische Eigentumsverhältnisse und die Abhängigkeit von einem innerstaatlichen Transferkreislauf kennzeichnen einen peripherisierten Wirtschaftsraum ohne lokale Bourgeoisie.
Im Vortrag wird diese spezifische politische Ökonomie näher beleuchtet, in ihrer historischen Gewordenheit nachvollzogen und die zukünftige Entwicklung antizipiert. Außerdem soll thesenhaft das Verhältnis jener spezifisch ostdeutschen politischen Ökonomie zu den verbreiteten
ideologischen Verarbeitungsformen diskutiert werden. 