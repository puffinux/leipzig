---
id: '2406046446351007'
title: Film und Vortrag „Unter der Haut liegen die Knochen“
start: '2019-09-13 19:00'
end: '2019-09-13 21:00'
locationName: MONAliesA
address: 'Bernhard-Göring-Straße 152, 04277 Leipzig-Connewitz, Sachsen'
link: 'https://www.facebook.com/events/2406046446351007/'
image: 67255879_2382274355375343_4213921160045264896_n.jpg
teaser: 'Freitag, 13.09.19, 19 Uhr: Film „Unter der Haut liegen die Knochen“ von Marlene Pardeller zum Thema Femizid, Vorstellung der Kampagne „KeineMehr“  Der'
recurring: null
isCrawled: true
---
Freitag, 13.09.19, 19 Uhr: Film „Unter der Haut liegen die Knochen“ von Marlene Pardeller zum Thema Femizid, Vorstellung der Kampagne „KeineMehr“

Der Film untersucht, wie das gesamtgesellschaftliche Problem der Frauen*morde in den politisch so unterschiedlichen Realitäten wie Italien und Mexiko sich dennoch so ähnlich gestalten kann. Dabei werden Frauenhauskoordinatorinnen, Trägerinnen von Traditionen wie Schuachplattlerinnen und Wrestlerinnen sowie Forscherinnen zu diesem Thema befragt, um die Kausalzusammenhänge zu erforschen.

Nach einer gemeinsamen Diskussion mit der Filmemacherin wird der Bezug zur deutschen Realität hergestellt. Jeden Tag wird in Deutschland versucht, eine Frau* umzubringen. Das ist keine Realität, die wir als Normalzustand akzeptieren können. Die Arbeit der KeineMehr-Kampagne wird von den Initiatorinnen vorgestellt. Ziel der Kampagne ist es, die Zustände in Deutschland zu benennen um sie nachhaltig zu verändern.

Einlass: 18.30 Uhr
Veranstaltungsbeginn: 19 Uhr
Reservierung von Sitzplätzen nicht möglich!
Eintritt: Spendenempfehlung 2-10 Euro