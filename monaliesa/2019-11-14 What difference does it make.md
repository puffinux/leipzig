---
id: "491263471731846"
title: What difference does it make?
start: 2019-11-14 19:00
end: 2019-11-14 21:00
locationName: MONAliesA
address: Bernhard-Göring-Straße 152, 04277 Leipzig-Connewitz, Sachsen
link: https://www.facebook.com/events/491263471731846/
image: 69252032_2408911942711584_2435200341768142848_n.jpg
teaser: "Do, 14.11.19, 19 Uhr: What difference does it make? – Zur Bedeutung der
  Geschlechterdifferenz in der männlichen Subjektwerdung.  Vortrag von Johanna
  N"
isCrawled: true
---
Do, 14.11.19, 19 Uhr: What difference does it make? – Zur Bedeutung der Geschlechterdifferenz in der männlichen Subjektwerdung.

Vortrag von Johanna Niendorf

Differenz und Differenzierung gehört notwendig zum Prozess der Individuation. Die Erkenntnis, dass Ich nicht Du bin, ist der erster Schritt, der auf dem Weg der Subjektwerdung vollzogen werden muss. Damit einher gehen Verwerfungen und Probleme, denn wenn Ich nicht Du bin, bedeutet das auch, dass Ich nicht alles sein kann. Subjektwerdung heißt Grenzen zu ziehen und eigene Begrenzungen zu akzeptieren.

Das männliche Subjekt der patriarchalen und kapitalistischen Gesellschaft befindet sich dabei in einem besonderen Konflikt zwischen Autonomiebestrebungen und der Abhängigkeit von Anderen. Gerade in der Beziehung zu Frauen ist dieser Konflikt zur Ursache von Abwertung,Aggression und Gewalt werden.
Im Vortrag soll das Spannungsfeld männlicher Subjektwerdung mit der psychoanalytischen Sozialpsychologie beleuchtet und Erklärungsansätze vorgestellt werden.
