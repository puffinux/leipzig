---
id: "490477025042934"
title: Feministisch, Lesbisch, Widerständig!
start: 2019-10-20 16:00
end: 2019-10-20 18:00
locationName: MONAliesA
address: Bernhard-Göring-Straße 152, 04277 Leipzig-Connewitz, Sachsen
link: https://www.facebook.com/events/490477025042934/
image: 68577446_2496901747036855_3735640130312994816_n.jpg
teaser: feministisch, lesbisch, widerständig!  Vorstellung der Initiative "Autonome
  feministische FrauenLesben aus Deutschland und Österreich", welche mit ein
isCrawled: true
---
feministisch, lesbisch, widerständig!

Vorstellung der Initiative "Autonome feministische FrauenLesben aus Deutschland und Österreich", welche mit einer Gedenkkugel im ehemaligen Frauenkonzentrationslager Ravensbrück dauerhaft an die Verfolgung und  Ermordung von Lesben im Nationalsozialismus erinnern möchte.

"In Gedenken aller lesbischen Frauen und Mädchen im Frauen-KZ Ravensbrück und Uckermark. Lesbische Frauen galten als 'entartet' und wurden als 'asozial', als widerständig und ver-rückt und aus anderen Gründen verfolgt und ermordet. Ihr seid nicht vergessen!" sind die Worte, welche die Gedenkkugel trägt.

In ihrem Vortrag "Lesben im Nationalsozialismus" wird Wiebke Hass die Lebensumstände von Frauen und Lesben im Nationalsozialismus aufgreifen und die Inschrift der Gedenkkugel untermauern. Susanne Kuntz wird über die Entstehung der Initiative für die dauerhafte Sichtbarkeit der Gedenkkugel auf dem Gelände der Mahn- und Gedenkstätte Ravensbrück erzählen und über den momentanen Stand der Dinge berichten.

Bei Interesse kann ein Video von der ersten Gedenkkugellegung 2015 gezeigt werden.

Vehemente Ablehnung  aber auch internationale Unterstützung für dieses Anliegen und vieles mehr regen bei Kaffee und Kuchen zum Diskutieren an.

Die Veranstaltung findet im Rahmen des LeLeTre´s statt
26. Leipziger Lesbentreffen
Zu der 26. Ausgabe des „Leipziger Festivals der lesbischen Lebenskunst“ werden wieder über tausend Besucher*innen im Soziokulturellen Zentrum Frauenkultur und den anderen Festival-Orten erwartet. Doch es geht um mehr als „lesbische Lebenskunst“… denn es existieren zahlreiche verschiedene Formen von Lebens-Gemeinschaften und Identitäten. 

Das Festival thematisiert verschiedene Erfahrungen … von der schwul-lesbischen Arbeiterinnenbewegung in der DDR bis zu Queer Refugees. Nachgefragt werden Lebenskonzepte und Möglichkeiten von Lesben im Alter; gesprochen wird über Visionen der queeren Community. Es gibt Filmnächte, Spaziergänge, Gottesdienst, Spieleabend, Party und Brunch.

LeLeTre bedeuten Austausch und Auseinandersetzung, Gemeinschaft und Lachen… und miteinander unterwegs sein – unabhängig von Herkunft, Religion, Identität oder Andersfähigkeit.

19.10. 2019 | 22 Uhr:   Queer-Party zum LeipzigerLesbenTreffen 

Internet: www.leletre.de/