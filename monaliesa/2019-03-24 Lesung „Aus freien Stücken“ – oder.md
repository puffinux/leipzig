---
id: '779311282436421'
title: 'Lesung: „Aus freien Stücken“ – oder?"'
start: '2019-03-24 15:00'
end: '2019-03-24 17:00'
locationName: MONAliesA
address: 'Bernhard-Göring-Straße 152, 04277 Leipzig-Connewitz, Sachsen'
link: 'https://www.facebook.com/events/779311282436421/'
image: 51308434_2263357027267077_5006108370051006464_n.jpg
teaser: 'Sonntag, 24. März, 15 Uhr: Lesung „Aus freien Stücken“ – oder? 15 Geschichten über Macht, Gewalt und Selbstbestimmung  Wie kann man sich bei einer Anh'
recurring: null
isCrawled: true
---
Sonntag, 24. März, 15 Uhr: Lesung „Aus freien Stücken“ – oder? 15 Geschichten über Macht, Gewalt und Selbstbestimmung

Wie kann man sich bei einer Anhörung im Asylverfahren einen Rest Selbstbestimmung bewahren, wenn man sogar aufs Klo nur in Begleitung darf? Kann man sich den Ekel vor »hässlichen« Körpern durch puren guten Willen abgewöhnen? Wem gehört meine Brust? Wie gefährlich kann ein Coming Out für syrische Ministerialbeamte werden? Und wie kann ich mir sicher sein, dass jemand wirklich Tee mit mir trinken will? Ein klarer weiblicher Blick auf komplizierte sexuelle- und Machtdynamiken ruft noch immer heftige Abwehrreaktionen hervor – doch eine neue Generation schüttelt jetzt die bleierne Decke auf. Herausgegeben von Redakteurinnen des Missy Magazine schreiben 15 namhafte Autor*innen über Selbstbestimmung und Sex in allen Lebensbereichen. Im Rahmen der Leipziger Buchmesse stellen die Herausgeberin Anna Mayrhauser sowie die beiden Autorinnen Sibel Schick und SchwarzRund das Buch vor und lesen aus ihren Texten. Dabei werden die absurden Erwartungsbilder von außen bzgl. der Körperbehaarung von Frauen und dem persönlichen Umgang damit thematisiert, sowie Erfahrungen und gedankliche Einordnungen als Person of Color im Unialltag versprachlicht. 

Eine Veranstaltung im Rahmen von "Leipzig liest"

Einlass: 14.30 Uhr
Veranstaltungsbeginn: 15 Uhr
Reservierung von Sitzplätzen nicht möglich!
Eintritt: Spendenempfehlung 2-10 Euro
