---
id: '2645791368794593'
title: 'Wirklich frei sind wir erst, wenn wir auch dick sein dürfen!'
start: '2019-04-11 19:00'
end: '2019-04-11 22:00'
locationName: MONAliesA
address: 'Bernhard-Göring-Straße 152, 04277 Leipzig-Connewitz, Sachsen'
link: 'https://www.facebook.com/events/2645791368794593/'
image: 51124310_2262466617356118_5354364861239263232_n.jpg
teaser: 'Vortrag und Diskussion über Fatacceptance und Feminismus von Judith Schreier:  Im Vortrag sollen die Bewegungen Fat Acceptance und Body Positivity in'
recurring: null
isCrawled: true
---
Vortrag und Diskussion über Fatacceptance und Feminismus von Judith Schreier:

Im Vortrag sollen die Bewegungen Fat Acceptance und Body Positivity in ihrem  Potential für eine Analyse struktureller Unterdrückungsmechansimen und einer Emanzipation von diesen kritisch hinterfragt werden und dabei immer wieder mit der Kategorie Geschlecht in Beziehung gesetzt werden:
Dick gilt als Tabu, als unter keinen Umständen erstrebenswert, darüber hinaus sind ganze Industrien darauf abgezielt zu verhindern, dass Menschen dick werden. Entstanden in den 1970er Jahren als
Untergrundbewegung in San Francisco, gewinnt die Fat Acceptance
Bewegung, die diese Einstellungen hinterfragt, nun langsam auch in
Deutschland immer mehr an Aufmerksamkeit in feministischen und
tendenziell auch in mehrheitsgesellschaftlichen Diskursen. Dicke
Menschen erfahren bis heute Diskriminierung und Benachteilungen in allen Lebensbereichen, die sogenannte Fettfeindlichkeit (engl.
Fatshaming/Fatphobia/-misia). Die Fat Acceptance Bewegung versucht der Entstehung von Stigmatisierung gegenüber dicken Menschen entgegenzuwirken und somit Diskriminierung abzubauen. Eine feministische Hauptüberzeugung der Bewegung ist dabei, dass Menschen – unabhängig wie dick – Akzeptanz, Wertschätzung und vollständige gesellschaftliche Teilhabe verdienen. Obwohl Menschen aller Gesellschaftsschichten dick sein können, sind besonders Menschen, die Mehrfachdiskriminierungen erfahren, betroffen. Denn der Diskurs über das Dicksein wird gesellschaftlich über mehrere Achsen verhandelt: Gesundheit, Schönheit und Verantwortung. Daraus resultierend sind Frauen und Queers durch die enge Verknüpfung dieser Achsen erhöhter Fettfeindlichkeit ausgesetzt.
Beispielsweise müssen Mütter nicht nur selbst dünn, in fettfeindlicher Rhetorik synonym mit gesund und schön, sein, sondern müssen auch konstant dafür sorgen ihre Kinder keinesfalls dick sind oder werden, damit diese möglichst nie zu erwachsenen Dicken werden. Zudem hat sich seit den 1990er Jahren aus der Fat Acceptance Bewegung heraus die Body Positivity Bewegung in den sozialen Medien herausbildet. Obwohl eine Öffnung der Thematiken wünschenswert sein könnte, ist heute der Diskurs zwischen Fat Acceptance und Body Positivity geprägt von Ausschlüssen und
Zugehörigkeit, kapitalistischer Verwertbarkeit und Ausnutzung der
Slogans.

Judith Schreier studiert im Master Amerikanistik an der Universität
Leipzig. Sie forscht über die Darstellung von dicken Körpern sowie
Gender und Sexualität in (amerikanischer) Literatur und Kultur und ist zudem Fat-Aktivistin. 

Einlass: 18.30 Uhr
Veranstaltungsbeginn: 19 Uhr
Reservierung von Sitzplätzen nicht möglich!
Eintritt: Spendenempfehlung 2-10 Euro

Der Vortrag ist Teil der neuen Veranstaltungsreihe "Geschlecht. Gesellschaft. Psyche" der monaliesa. In dieser Veranstaltungsreihe wollen wir uns mit der Frage beschäftigen, inwiefern die Kategorie Geschlecht vermittelt über gesellschaftliche Prozesse mit psychischer Gesundheit zusammenhängt bzw. auch andersherum, wie psychische Gesundheit durch gesellschaftliche Veränderungen insbesondere bezüglich Geschlechternormen befördert werden könnte.