---
id: '283069289038552'
title: 'Barbara Degen: Lesbendiskriminierung. Kein Recht auf Erinnerung?'
start: '2019-04-05 19:00'
end: '2019-04-05 22:00'
locationName: MONAliesA
address: 'Bernhard-Göring-Straße 152, 04277 Leipzig-Connewitz, Sachsen'
link: 'https://www.facebook.com/events/283069289038552/'
image: 51765547_2266729860263127_3509773615602073600_n.jpg
teaser: 'Vortrag von Barbara Degen: Lesbendiskriminierung. Kein Recht auf Erinnerung?  Dr. Barbara Degen wird über die Kontinuitäten der Lesbendiskriminierung'
recurring: null
isCrawled: true
---
Vortrag von Barbara Degen: Lesbendiskriminierung. Kein Recht auf Erinnerung?

Dr. Barbara Degen wird über die Kontinuitäten der Lesbendiskriminierung und –verfolgung sprechen und ihre rechtshistorische Entwicklung, also die historische Einordnung des § 175 StGB, erläutern. Anhand des Beispiels der Gedenkstätte Ravensbrück zeigt sie die Auseinadersetzung und den Streit ums Gedenken und die Anerkennung von Lesben als Opfergruppe.

Dr. Barbara Degen, Juristin, Frauengeschichtsforscherin, Mitbegründerin des „Hauses der Frauengeschichte“, Bonn und Autorin des Buches „Das Herz schlägt in Ravensbrück – Die Gedenkkultur der Frauen“ (2010)

Eine Veranstaltung im Rahmen der Fahrt in die Mahn- und Gedenkstätte Ravensbrück, in Zusammenarbeit mit der Gedenkstätte für Zwangsarbeit Leipzig.

Einlass: 18.30 Uhr
Veranstaltungsbeginn: 19 Uhr
Reservierung von Sitzplätzen nicht möglich!
Eintritt: Spendenempfehlung 2-10 Euro