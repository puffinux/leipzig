---
id: '352063015363071'
title: Erwerbs- und Sorgearbeit im Umbruch. Frauen organisieren sich
start: '2019-06-28 16:00'
end: '2019-06-28 17:30'
locationName: null
address: 'Universität Leipzig, Augustusplatz, genaue Angabe folgt'
link: 'https://www.facebook.com/events/352063015363071/'
image: 62178152_2348436318759147_185170338076688384_o.jpg
teaser: Der politische und ökonomische Umbruch 1989/90 stellte das Leben von Frauen auf den Kopf. Das in der DDR erprobte Babyjahr wurde nach 1990 vom dreijäh
recurring: null
isCrawled: true
---
Der politische und ökonomische Umbruch 1989/90 stellte das Leben von Frauen auf den Kopf. Das in der DDR erprobte Babyjahr wurde nach 1990 vom dreijährigen Erziehungsurlaub abgelöst, sichere Arbeitsverhältnisse aufgekündigt und Kinderbetreuung mehr und mehr zur Privatangelegenheit. Die veränderten Lebensumstände brachten viele Herausforderungen für Frauen mit sich. Angelika Fischer, Marion Ziegler und Raymonde Will berichten von dieser spannungsreichen Zeit und der ostdeutschen Frauenbewegung, die noch im Umbruch neue Netzwerke und Räume schuf.

Mit:
Angelika Fischer, Initiatorin des Netzwerkes Frauen und Beruf
Raymonde Will, Mitbegründerin des Mütterzentrums Leipzig
Marion Ziegler, vertrat die Fraueninitiative Leipzig am Runden Tisch der Stadt, von 1990 bis 1994 war sie Stadtverordnete des UFV in der Fraktion Bündnis 90/Grüne/UFV
Moderation:
Pia Marzell, Historikerin, MONAliesA Leipzig

Die Veranstaltung findet im Rahmen des vom BMFSFJ geförderten DDF-Projektes „Zentrale Kämpfe der ostdeutschen oppositionellen Frauenbewegung während der Umbruchsphase 89/90 – Auseinandersetzungen um Erwerbsarbeit und Kinderbetreuung.“ und der Sommeruni des Digitales Deutsches Frauenarchiv - DDF statt.

Zum Projekt: In der feministischen Bibliothek MONAliesA lagert ein Archiv an Grauer Literatur mit Dokumenten zu frauenbewegten und feministischen Themen der letzten 70 Jahre. Seit 2016 arbeiten wir diesen Bestand sukzessive wissenschaftlich auf. Im Rahmen des DDF-Projekts widmen wir uns 2019 zentralen Themen der oppositionellen Frauenprojekte der Umbruchphase 1989/90. Für die Erstellung digitaler Dossiers und die Durchführung von Interviews, die im DDF veröffentlicht werden sollen, beschäftigen wir uns mit Kämpfen der Frauen um Gleichberechtigung in der Erwerbsarbeit und der damit in Zusammenhang stehenden Problematik der Kinderbetreuung. Darüber hinaus wollen wir die Auseinandersetzungen von und mit Erzieherinnen bzw. Kindergärtnerinnen während der Umbruchsphase 89/90 aufarbeiten und in Kontext mit heutigen Diskussionen um Care-Arbeit und Kinderbetreuung stellen.

Zur Sommeruni: „Ohne Frauen keine Demokratie!“ heißt es in diesem Jahr zur Feministischen Sommeruni. Mit vielen Zeitzeug*innen sowie frauenpolitischen Vertreter*innen aus Politik, Wissenschaft, Kunst und Kultur begrüßen wir euch am 28. und 29. Juni 2019 in Leipzig.

Das Programm wird ständig aktualisiert. https://www.feministische-sommeruni.de/programm#freitag-28-juni-14-bis-18-uhr