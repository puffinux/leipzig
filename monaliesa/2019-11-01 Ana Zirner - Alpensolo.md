---
id: "754955588256740"
title: Ana Zirner - Alpensolo
start: 2019-11-01 19:00
end: 2019-11-01 20:30
locationName: MONAliesA
address: Bernhard-Göring-Straße 152, 04277 Leipzig-Connewitz, Sachsen
link: https://www.facebook.com/events/754955588256740/
image: 68425341_2393496457586466_8759356968629436416_n.jpg
teaser: Ana Zirner beschließt, allein von Ost nach West die Alpen zu überqueren. Nur
  mit einem 35-Liter-Rucksack bepackt, begibt sie sich auf ihre selbst gele
isCrawled: true
---
Ana Zirner beschließt, allein von Ost nach West die Alpen zu überqueren. Nur mit einem 35-Liter-Rucksack bepackt, begibt sie sich auf ihre selbst gelegte Route: knapp 2000 Kilometer vom slowenischen Ljubljana über Österreich, Italien und die Schweiz bis ins französische Grenoble. 
Außerdem hat sie die Pyrenäen überquert und zuletzt das große Projekt realisiert, allein den Colorado River herunterzupaddeln.
Was für Menschen trifft sie und welche Erfahrungen macht sie in einem leistungs- und männerdominierten Sportbereich? Welche Reaktionen bekommt sie von Männern und auch Frauen darauf, dass sie sich Wünsche überlegt und diese umsetzt und wie reagiert eine Sportmarke, die die angebotenen pinken Wanderstöcke zurückgeschickt bekommt aufgrund ihrer Farbe, jedoch keine Alternative anbieten kann?

Über ihre Erfahrungen am Berg und in der Natur wird Ana berichten; eine Frage- und Diskussionsrunde im Anschluss ist fest eingeplant.

Ana Zirner, Jahrgang 1983, ist freiberufliche Autorin, Kulturmanagerin, Film- und Theaterregisseurin und war u.a. in New York, Madrid und Berlin tätig. Aufgewachsen im Bayerischen Voralpenland, zieht es sie jedoch immer wieder in die Berge zurück, wo sie leidenschaftlich gern Mehrtages- und Gipfeltouren unternimmt, klettern geht sowie ausgedehnte Ausflüge mit dem Splitboard absolviert. Über ihre Alpenüberquerung von Ost nach West im Sommer 2017 bloggte sie auf ihrer Webseite. 

Einlass: 18.30 Uhr
Veranstaltungsbeginn: 19 Uhr
Reservierung von Sitzplätzen nicht möglich!
Eintritt: Spendenempfehlung 2-10 Euro