---
id: '1657678297668485'
title: Traumgeburt oder Geburtstrauma?
start: '2019-06-20 19:00'
end: '2019-06-20 21:00'
locationName: MONAliesA
address: 'Bernhard-Göring-Straße 152, 04277 Leipzig-Connewitz, Sachsen'
link: 'https://www.facebook.com/events/1657678297668485/'
image: 57503403_2310755415860571_5350441992729919488_n.jpg
teaser: '"Traumgeburt oder Geburtstrauma? - Zum neuen Unbehagen an der Geburt"  Vortrag und Diskussion mit Dr. Sabine Flick: Seit wenigen Jahren engagieren sic'
recurring: null
isCrawled: true
---
"Traumgeburt oder Geburtstrauma? -
Zum neuen Unbehagen an der Geburt"

Vortrag und Diskussion mit Dr. Sabine Flick:
Seit wenigen Jahren engagieren sich Aktivist*innen der so genannten „Roses Revolution“gegen geburtshilfliche Gewalt in Europa. Sie beziehen sich dabei auf Erfahrungen respektlosenUmgangs und ungerechtfertigter Behandlung bis hin zu körperlicher Misshandlung, die Frauen unter der Geburt erleben. Während die WHO dazu bereits ein Statement veröffentlicht hat, die Diskussionen um ‚Black BirthingJustice‘ in den USA aufrassistische Strukturen auch in der Geburtshilfehinweisenund in einigen lateinamerikanischen Ländern geburtshilflichen Gewalt sogar bereits als Rechtsgegenstand verhandelt wird, hat die Debatte in Europa gerade erst begonnen. Interessanterweise konzentriert sich die europäische, insbesondere deutschsprachige Diskussion stark auf die psychischen Aspekte der Erfahrungen mit geburtshilflicher Gewalt. Mütter, Doulas und Hebammen beschreiben traumatische Erlebnisse während der Geburt und leiten Folgeprobleme beim Stillen, Depressionen und Bindungsprobleme mit dem Säugling aus diesen Traumata ab. Experten aus Psychotherapie und Psychiatrie stehen Pate für diese Erklärungen. In der Folge bietet der Bereich der Eltern-Kind-Psychotherapie immer mehr Dienstleistungen zur Prävention und Bewältigung von Geburtstraumata an. 
Was ist passiert? Erleben wir tatsächlich ein #metoo im Kreißsaal? Ist dies alles also ein Hinweis darauf, dass sich die Situation der Frauen unter der Geburt in Europa verschlechtert hat oder sind womöglich noch andere Diskurse am Werk, die uns ein insgesamt stark verändertes Bild von dem geben, was heute unter einer „normalen“ Geburt verstanden wird?Und welche Rolle spielt die Ökonomisierung der Kliniken?Der Vortrag gibt Antworten auf diese Fragen und greift dabei auf laufende Forschungen zurück. 




Dr. Sabine Flick vertritt derzeit die Professur für Soziologie mit dem Schwerpunkt Familien- und Jugendsoziologie an der Goethe-Universität in Frankfurt am Main und forscht am dortigen Institut für Sozialforschung. Ihre Schwerpunkte sind Medizin- und Wissenssoziologie, qualitative Forschungsmethoden sowie die Soziologie der Geschlechterverhältnisse. Neben ihrer Tätigkeit in der Wissenschaft ist sie als Supervisorin in Feldern psychosozialer Arbeit unterwegs. 



Einlass: 18.30 Uhr
Veranstaltungsbeginn: 19 Uhr
Reservierung von Sitzplätzen nicht möglich!
Eintritt: Spendenempfehlung 2-10 Euro

Der Vortrag ist Teil der neuen Veranstaltungsreihe "Geschlecht. Gesellschaft. Psyche" der monaliesa. In dieser Veranstaltungsreihe wollen wir uns mit der Frage beschäftigen, inwiefern die Kategorie Geschlecht vermittelt über gesellschaftliche Prozesse mit psychischer Gesundheit zusammenhängt bzw. auch andersherum, wie psychische Gesundheit durch gesellschaftliche Veränderungen insbesondere bezüglich Geschlechternormen befördert werden könnte.
















