---
id: "543765986457827"
title: Ich habe da immer so ein Bild im Kopf
start: 2019-12-19 19:00
end: 2019-12-19 21:00
locationName: MONAliesA
address: Bernhard-Göring-Straße 152, 04277 Leipzig-Connewitz, Sachsen
link: https://www.facebook.com/events/543765986457827/
image: 74514834_2491292331140211_7700888061218914304_o.jpg
teaser: “Ich habe da immer so ein Bild im Kopf, das ein bisschen bunter ist als das
  Jetzt…”  Gespräch mit dem AK.Unbehagen  In der gegenwärtigen Gesellschaft
isCrawled: true
---
“Ich habe da immer so ein Bild im Kopf, das ein bisschen bunter ist als das Jetzt…”

Gespräch mit dem AK.Unbehagen

In der gegenwärtigen Gesellschaft erscheinen Fürsorge, die fundamentale Bezogenheit aufeinander und Abhängigkeiten – kurz alles, was mit Mütterlichkeit assoziiert ist – systematisch abgewertet. Dies spiegelt sich nicht nur in konkreten Erfahrungen von Müttern, sondern weiterhin in den prekären Arbeitsbedingungen im Care-Sektor oder den miserablen Zuständen in Pflegeeinrichtungen wieder. Auch die feministische Auseinandersetzung mit Mutterschaft verläuft notgedrungen ambivalent.

Wir möchten nach einer Kritik an gegenwärtigen Bedingungen von Mutterschaft suchen, die nicht bei der Aushandlung von Aufgaben oder beim Entwurf der “neuen Mutterideale” endet. Wir möchten danach fragen, was mit einer Gesellschaft und ihren Beziehungen passiert, wenn eine grundlegende Abhängigkeit nicht mehr geleugnet werden muss.

In einem fiktiven Gespräch diskutiert der AK Unbehagen die Wünsche und (An-)Forderungen an utopische Mutterschaft und Elternschaft mit dem Begehren, utopische Vorstellung von dem Umgang mit Care-Arbeit ins hier und jetzt zu holen.
