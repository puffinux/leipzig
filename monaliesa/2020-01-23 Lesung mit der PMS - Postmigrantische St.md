---
id: "446465399359375"
title: Lesung mit der PMS - Postmigrantische Störung
start: 2020-01-23 19:00
end: 2020-01-23 22:00
locationName: MONAliesA
address: Bernhard-Göring-Straße 152, 04277 Leipzig-Connewitz, Sachsen
link: https://www.facebook.com/events/446465399359375/
image: 80457201_2514773165458794_1559852308529938432_o.jpg
teaser: PMS ist die Post Migrantische Störung – Wir lesen eigene Texte,
  Kurzgeschichten und Gedichte, Dramen und Aufsätze über eigene und ausgedachte
  Situatio
isCrawled: true
---
PMS ist die Post Migrantische Störung – Wir lesen eigene Texte,
Kurzgeschichten und Gedichte, Dramen und Aufsätze über eigene und ausgedachte Situationen, Ideen und Erlebnisse aus unterschiedlichen Stimmungen, mit verschiedenen Gefühlen und von mehreren Stimmen - meistens auf Deutsch, aber nicht nur.