---
id: '283480452329144'
title: Fahrt in die Mahn- und Gedenkstätte Ravensbrück
start: '2019-05-17 15:00'
end: '2019-05-19 17:00'
locationName: MONAliesA
address: 'Bernhard-Göring-Straße 152, 04277 Leipzig-Connewitz, Sachsen'
link: 'https://www.facebook.com/events/283480452329144'
image: null
teaser: null
recurring: null
isCrawled: true
---
