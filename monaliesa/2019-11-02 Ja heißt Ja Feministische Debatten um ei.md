---
id: "2249524645142832"
title: Ja heißt Ja? Feministische Debatten um einvernehmlichen Sex
start: 2019-11-02 19:00
end: 2019-11-02 21:00
locationName: MONAliesA
address: Bernhard-Göring-Straße 152, 04277 Leipzig-Connewitz, Sachsen
link: https://www.facebook.com/events/2249524645142832/
image: 67403265_2386260778310034_3947536946066096128_o.jpg
teaser: "Samstag, 02.11.19, 19 Uhr: Ja heißt Ja? Feministische Debatten um
  einvernehmlichen Sex Lesung mit Rona Torenz  Weil es nicht immer allen möglich
  ist,"
isCrawled: true
---
Samstag, 02.11.19, 19 Uhr: Ja heißt Ja? Feministische Debatten um einvernehmlichen Sex
Lesung mit Rona Torenz

Weil es nicht immer allen möglich ist, «Nein» zu sagen, ist aus der feministischen Losung «Nein heißt Nein» die Forderung nach «Ja heißt Ja» entstanden. Demnach ist die ausdrückliche Zustimmung aller Beteiligten notwendig, damit Sex als einvernehmlich gilt. So wird die Verantwortung für sexuelle Gewalt verlagert: Weg von jenen, die nicht (klar genug) «Nein» sagen hin zu jenen, die nicht das nötige «Ja» bekommen. Aus der Perspektive sexueller Selbstbestimmung erscheint das zunächst plausibel. 

Die Autorin sieht das Konzept dennoch kritisch. Ihre wesentliche These: Die Fokussierung auf ausdrückliches Einverständnis als Hebel zur Verhinderung sexualisierter Gewalt unterschätzt die Verinnerlichung sexueller Machtverhältnisse. «Ja heißt Ja» kann somit heteronormative Strukturen stabilisieren und wenig zu einer Veränderung hegemonialer Sexualkultur beitragen. Was heißt das für unseren Kampf gegen sexuelle Gewalt?

Diese Fragen und mehr diskutiert Rona Torenz im Gespräch.

Einlass: 18.30 Uhr
Veranstaltungsbeginn: 19 Uhr
Eintritt: Spendenempfehlung 2-10 Euro