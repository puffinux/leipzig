---
id: '2001813029916473'
title: Die Aufseherinnen des KZ-Außenlagers HASAG Leipzig
start: '2019-04-25 19:00'
end: '2019-04-25 22:00'
locationName: MONAliesA
address: 'Bernhard-Göring-Straße 152, 04277 Leipzig-Connewitz, Sachsen'
link: 'https://www.facebook.com/events/2001813029916473/'
image: 51548872_2266716240264489_8459168793640304640_n.jpg
teaser: 'Vortrag der Gedenkstätte für Zwangsarbeit Leipzig: Die Aufseherinnen des KZ-Außenlagers “HASAG Leipzig”  Die Rolle von Frauen im NS war lange Zeit ums'
recurring: null
isCrawled: true
---
Vortrag der Gedenkstätte für Zwangsarbeit Leipzig: Die Aufseherinnen des KZ-Außenlagers “HASAG Leipzig”

Die Rolle von Frauen im NS war lange Zeit umstritten. Erst gegen Ende der 1980er Jahre richtete sich der Blick der Forschung von der einseitigen Opferperspektive hin zu einem Verständnis von Frauen als (Mit)Täterinnen und eigenständig handelnden Akteurinnen innerhalb des nationalsozialistischen Verfolgungsapparates. In Leipzig existierte seit 1944 das KZ-Frauenaußenlager „HASAG Leipzig“. Die ca. 5000 weiblichen
Häftlinge wurden rund um die Uhr von über 80 KZ-Aufseherinnen bewacht. Nach 1945 gab es keine weitergehende wissenschaftliche Auseinandersetzung mit diesen Täterinnen.

Die Referentinnen widmen sich seit 2017 in einem Forschungsprojekt der Aufarbeitung der Biografien dieser Aufseherinnen. Der Vortrag zielt darauf ab, am Beispiel des KZ-Außenlagers „HASAG Leipzig“ eine Einführung in das Thema KZ-Aufseherinnen zu geben. Dabei sollen anhand von Forschungsergebnissen die Wege der Frauen von ihrer Rekrutierung bis zu ihrem Dienst im Außenlager sowie mögliche Handlungsräume und der juristische Umgang mit ihren Verbrechen nach 1945 beleuchtet werden.

Eine Veranstaltung im Rahmen der Fahrt in die Mahn- und Gedenkstätte Ravensbrück in Kooperation mit der Gedenkstätte für Zwangsarbeit Leipzig.

Einlass: 18.30 Uhr
Veranstaltungsbeginn: 19 Uhr
Reservierung von Sitzplätzen nicht möglich!
Eintritt: Spendenempfehlung 2-10 Euro