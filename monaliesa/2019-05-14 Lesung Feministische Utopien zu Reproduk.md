---
id: '2288679401445894'
title: Lesung Feministische Utopien zu Reproduktion und Arbeit
start: '2019-05-14 17:00'
end: '2019-05-14 20:00'
locationName: MONAliesA
address: 'Bernhard-Göring-Straße 152, 04277 Leipzig-Connewitz, Sachsen'
link: 'https://www.facebook.com/events/2288679401445894/'
image: 56938300_660451437726955_5497058402255241216_o.jpg
teaser: “… dass die Wahrheit eine Sache der Vorstellungskraft ist.” Feministische Utopien zu Reproduktion und Arbeit. Lesung und Diskussion mit der AG Feminis
recurring: null
isCrawled: true
---
“… dass die Wahrheit eine Sache der Vorstellungskraft ist.”
Feministische Utopien zu Reproduktion und Arbeit.
Lesung und Diskussion mit der AG Feministische Kämpfe (FAU Dresden)

Wir leben nicht gerade in utopischen Verhältnissen. Die AFD und Pegida wollen den Menschen ein konservatives Familienbild aufzwingen, “Lebensschützer” und fundamentalistische Christen demonstrieren am 25.05.2019 in Annaberg-Buchholz erneut für ein komplettes Verbot von Abtreibung. Sexualität, Familienstrukturen, Kindererziehung sollen sich an starren Geschlechterrollen orientieren. Gegen diese Entwicklung müssen notwendige Abwehrkämpfe geführt werden.
Oft kommen wir gar nicht dazu, darüber nachzudenken, wie eigentlich eine Gesellschaft aussehen könnte, in der Geschlecht keine Rolle spielt, oder in der Geschlecht ganz andere Rollen spielt. 
Wer kümmert sich dann in welchem Maß um Arbeit, Haushalt und Kindererziehung? Müssen sich überhaupt Menschen darum kümmern? Wie werden solche Dinge entschieden? Wie leben und lieben wir? Wie sehen unsere Beziehungen zueinander aus? Welche Voraussetzungen müssen erfüllt sein, damit gleichberechtigtes Zusammenleben möglich ist?

Auch Science-Fiction-Autor_innen haben sich diese Fragen gestellt und die Zukunft als Folie für Gedankenexperimente genutzt. Anhand von utopischen Romanen, wie beispielsweise “Planet der Habnichtse” von Ursula K. Leguin wollen wir miteinander darüber ins Gespräch kommen, wie
dieser (noch) nicht vorhandene Ort (deutsche Übersetzung des Wortes Utopia) aussehen kann, was wir unter einem besseren Zusammenleben verstehen und wie wir dahin kommen, wenn uns die Herrschenden nun mal keinen Planeten schenken.