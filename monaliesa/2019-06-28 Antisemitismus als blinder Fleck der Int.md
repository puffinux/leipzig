---
id: '807051056312669'
title: Antisemitismus als blinder Fleck der Intersektionalität
start: '2019-06-28 19:00'
end: '2019-06-28 22:00'
locationName: MONAliesA
address: 'Bernhard-Göring-Straße 152, 04277 Leipzig-Connewitz, Sachsen'
link: 'https://www.facebook.com/events/807051056312669/'
image: 51620284_2266623660273747_1318182985639395328_o.jpg
teaser: 'Merle Stöver: Die Achillesferse der Intersektionalität – Überlegungen zu Antisemitismus als blinder Fleck der Intersektionalität   Wer sich einst hoff'
recurring: null
isCrawled: true
---
Merle Stöver: Die Achillesferse der Intersektionalität – Überlegungen zu Antisemitismus als blinder Fleck der Intersektionalität 

Wer sich einst hoffnungsvoll unter dem Schlagwort der Intersektionalität versammelte, musste mit jener Hoffnung vor der Unmöglichkeit, Antisemitismus adäquat zu erfassen und einzubeziehen, kapitulieren. Im Vortrag wird nicht nur jene Unmöglichkeit thematisiert, sondern auch zugleich der Blick auf aktuelle Entwicklungen geworfen, die den antisemitischen Geist der Intersektionalitätsdebatten der vergangenen Jahre offenbaren. Doch die Kapitulation vor dem Antisemitismus, der als Ressentiment, politische Bewegung und Ideologie zugleich für sich beansprucht, die Welt erklären zu können, soll nicht das Ende sein: Im Anschluss soll im Vortrag diskutiert werden, inwiefern Bedingungen geschaffen werden können, um Antisemitismus zu erfassen und im Zusammenspiel mit Rassismus, Sexismus oder Klassismus zu analysieren.

Merle Stöver ist Masterstudentin am Zentrum für Antisemitismusforschung der TU Berlin. Sie forscht, schreibt und spricht zu Antisemitismus und Geschlecht.

Eine Veranstaltung im Rahmen der Jüdische Woche in Leipzig.
