---
id: "1182210158654893"
title: MONAliesA hat an sich gedacht!
start: 2019-12-16 19:00
end: 2019-12-16 21:00
locationName: MONAliesA
address: Bernhard-Göring-Straße 152, 04277 Leipzig-Connewitz, Sachsen
link: https://www.facebook.com/events/1182210158654893/
image: 77408486_2498441807091930_4425016998757400576_n.jpg
teaser: Unser Projekt zur Aufarbeitung der MONAliesA-Geschichte von der Gründung bis
  zum Jahr 2000 neigt sich dem Ende zu. Unser Ziel war es, ein weiteres Stü
isCrawled: true
---
Unser Projekt zur Aufarbeitung der MONAliesA-Geschichte von der Gründung bis zum Jahr 2000 neigt sich dem Ende zu. Unser Ziel war es, ein weiteres Stück Frauengeschichte sichtbar zu machen. Es konnten wieder zahlreiche Bewegungsfrauen für die Rechtefreigabe der Originaldokumente zur MONAliesA-Geschichte gewonnen werden. Mittlerweile stehen über 200 Digitalisate der MONAliesA im WebOpac der deutschsprachigen Frauen- und Lesbenarchive der Öffentlichkeit digital zur Verfügung. Einige dieser Zeugnisse sowie unsere Rechercheergebnisse zur MONAliesA-Geschichte werden in einer Veranstaltung am 16.12. 19 Uhr in der MONAliesA präsentiert. 

Um die Auseinandersetzung mit unserer eigenen Geschichte fortzuführen, soll an diesem Abend kritisch auf die esoterischen Tendenzen der Frauenbibliothek und der Frauenbewegung zu dieser Zeit eingegangen und über deren Auswirkungen debattiert werden.

https://www.meta-katalog.eu/Search/Results?type=AllFields&filter%5B0%5D=institution%3A%22MONAliesA%22&filter%5B1%5D=iiifTitle%3A%2A&fbclid=IwAR1iWQSJfUiTE04RwBqIHFe0EC4Fw3jgiWSsmguXka10gx2Qj5yDmK5Ns9g