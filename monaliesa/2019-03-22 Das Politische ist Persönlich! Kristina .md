---
id: '1970472256587451'
title: Das Politische ist Persönlich! Kristina Hänel liest
start: '2019-03-22 19:00'
end: '2019-03-22 22:00'
locationName: GWZ- Das Geisteswissenschaftliche Zentrum
address: 'Beethovenstr. 15, 04107 Leipzig'
link: 'https://www.facebook.com/events/1970472256587451/'
image: 48398111_2232277700375010_6927918334486773760_n.jpg
teaser: '>>ACHTUNG RAUMÄNDERUNG! Die Veranstaltung findet im GWZ der Universität Leipzig, Hörsaal Erdgeschoss, Beethovenstr. 15 statt!!<<  Das Politische ist P'
recurring: null
isCrawled: true
---
>>ACHTUNG RAUMÄNDERUNG! Die Veranstaltung findet im GWZ der Universität Leipzig, Hörsaal Erdgeschoss, Beethovenstr. 15 statt!!<<

Das Politische ist Persönlich! Tagebuch der „Abtreibungsärztin“, die sich nicht mundtot machen lässt 

Von einer, die auszog, einen Unrechtsparagrafen zu kippen: die Gießener Ärztin Kristina Hänel und ihr Kampf für das Recht auf Information zum Schwangerschaftsabbruch.

Als Kristina Hänel am 3. August 2017 nach Hause kommt, erwartet sie ein Brief vom Amtsgericht. »Strafverfahren gegen Sie wegen Werbens für den Abbruch einer Schwangerschaft …« Laut §219a StGB gilt die Sachinformation auf ihrer Homepage als Werbung und ist verboten, ein Umstand, den Abtreibungsgegner nutzen, um Mediziner*innen bundesweit anzuzeigen. Bis dato unbemerkt von einer breiteren Öffentlichkeit. Dies ändert sich mit der »Causa Hänel«, als die Ärztin ihren Fall mit einer Petition öffentlich macht und mit Haut und Haar für die Aufklärung über §219a und seine Abschaffung eintritt.

Als Galionsfigur der Kampagne für das Recht auf Information zum Schwangerschaftsabbruch wird Kristina Hänel große mediale Aufmerksamkeit zuteil, doch die Reduzierung auf den Begriff »Abtreibungsärztin« akzeptiert sie nicht. In ihrem persönlichen Tagebuch hält sie mit ungefilterter Offenheit fest, welche inneren und äußeren Kämpfe sie vor, während und nach dem Prozess begleiten und wie sie zu der öffentlichen Person wurde, die sie heute ist. Dabei ordnet sie ihr Engagement gegen den §219a ebenso in die Geschichte des Kampfes für Frauengesundheit ein wie in den Kontext ihres Wirkens als Ärztin und Mensch. Erkennbar wird eine beherzte Frau, die stets versucht, anderen zu helfen und das Richtige zu tun.

Einlass: 18.30 Uhr
Veranstaltungsbeginn: 19 Uhr
Reservierung von Sitzplätzen nicht möglich!
Eintritt: Spendenempfehlung 2-10 Euro

Dies ist eine Veranstaltung im Rahmen von "Leipzig liest"