---
id: '501757177254973'
title: 'Outside the box #7 - Lesung und Diskussion zum Thema "Erfahrung"'
start: '2019-09-08 16:00'
end: '2019-09-08 18:00'
locationName: MONAliesA
address: 'Bernhard-Göring-Straße 152, 04277 Leipzig-Connewitz, Sachsen'
link: 'https://www.facebook.com/events/501757177254973/'
image: 68521633_2391369484465830_8582435119539683328_n.jpg
teaser: In diesen Zeiten eine Ausgabe zu Erfahrung? Redet die „outside the box“ jetzt nur noch über das Besondere? Oder wird die "outside" jetzt ganz im Gegen
recurring: null
isCrawled: true
---
In diesen Zeiten eine Ausgabe zu Erfahrung? Redet die „outside the box“ jetzt nur noch über das Besondere? Oder wird die "outside" jetzt ganz im Gegenteil völlig abgehoben, befasst sich nicht mehr mit der Veränderung der Gesellschaft, sondern nur noch mit Begriffsgeschichte? Kein StreikHeft? Kein Magazin gegen die Wahlen in Sachsen? Gegen § 218 und 219a? Gerade jetzt eine Ausgabe zu einer ganz abstrakten Frage? 

Im Juli erschien die siebte Ausgabe der outside the box zum Thema Erfahrung. Wir werden einzelne Passagen aus dem Heft lesen, erzählen, wie es zu dieser Ausgabe kam und laden euch ein, mit uns die Inhalte zu diskutieren. Denn wie ihr vielleicht schon ahnt, geht es um beides: Es geht um die Erfahrungen als individuellen, subjektiven Moment – das Besondere, das nicht im Allgemeinen aufgeht. Es geht aber auch um die Beschäftigung mit einer theoretischen Reflexion über Erfahrung, denn die Wiedergabe von Einzelmomenten, das bloße Nebeneinanderstellen von individuellen Erfahrungen reicht nicht aus, wenn das Anliegen ist, eine feministische Gesellschaftstheorie zu formulieren.  

Weitere Informationen zur outside the box findet ihr unter outside-mag.de

Einlass: 15:30 Uhr
Veranstaltungsbeginn: 16 Uhr
Eintritt: Spendenempfehlung 2-10 Euro
