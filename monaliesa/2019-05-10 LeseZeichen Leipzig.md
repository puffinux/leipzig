---
id: '410054419757851'
title: LeseZeichen Leipzig
start: '2019-05-10 12:00'
end: '2019-05-10 18:00'
locationName: null
address: 'Petersstraße 43, 04109 Leipzig'
link: 'https://www.facebook.com/events/410054419757851/'
image: 54729207_400418580691167_5876741590055124992_n.jpg
teaser: 'Mit einer öffentlichen Lesung möchten wir im Initiativkreis 9. November an die Bücherverbrennungen erinnern und damit an die als jüdisch, marxistisch,'
recurring: null
isCrawled: true
---
Mit einer öffentlichen Lesung möchten wir im Initiativkreis 9. November an die Bücherverbrennungen erinnern und damit an die als jüdisch, marxistisch, pazifistisch, liberal oder auf andere Weise oppositionell verunglimpften und verfolgten Autor*innen im NS-Staat. Wir möchten daran erinnern, dass diese Ereignisse das Ende der Demokratie und der Gedankenfreiheit einläuteten und in eine Diktatur führten, in der schließlich Heinrich Heines vielzitierter Satz auf unfassbar schreckliche Weise Realität werden sollte: „Das war ein Vorspiel nur, dort wo man Bücher verbrennt, verbrennt man auch am Ende Menschen.“

Wir laden zum gemeinsamen Lesen und Zuhören ein, zum Wiederentdecken der Ideen und Texte der Verfemten für unsere Gegenwart. Und wir sprechen uns mit dieser Lesung für gesellschaftliche Vielfalt aus und gegen die Ausgrenzung von Menschen und ihre Verfolgung, denn: „Freiheit darf kein Privilegium sein“ (Rosa Luxemburg).

Zuhören ist einfach, Mitlesen auch!

Jede/r Vorlesende entscheidet sich für ein Werk und eine Textstelle. Wir freuen uns über Beiträge bis max. zehn Minuten pro Teilnehmer*in. Eine Titelliste findet ihr auf unserer Facebook-Seite und bitten um Anmeldung bis 1. Mai via E-Mail an lesezeichen-leipzig@riseup.net. 