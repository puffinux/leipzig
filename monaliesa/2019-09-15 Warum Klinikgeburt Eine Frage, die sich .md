---
id: '353661791970792'
title: 'Warum Klinikgeburt? Eine Frage, die sich in den 1950ern stellte'
start: '2019-09-15 16:00'
end: '2019-09-15 19:00'
locationName: MONAliesA
address: 'Bernhard-Göring-Straße 152, 04277 Leipzig-Connewitz, Sachsen'
link: 'https://www.facebook.com/events/353661791970792/'
image: 67000914_2382268162042629_4946279625941581824_o.jpg
teaser: 'Sonntag, 15.09.2019, 16 Uhr: Warum Klinikgeburt? Eine Frage, die sich in den 1950er Jahren stellte  Referentin: Dr. Christine Loytved  Im Osten früher'
recurring: null
isCrawled: true
---
Sonntag, 15.09.2019, 16 Uhr: Warum Klinikgeburt? Eine Frage, die sich in den 1950er Jahren stellte

Referentin: Dr. Christine Loytved

Im Osten früher als im Westen wurde die Geburt im Krankenhaus zur Routine. Christine Loytved geht der Frage nach, wodurch die Frauen veranlasst wurden, die eigene Wohnung als Geburtsort für ihr Kind aufzugeben. Möglicherweise helfen die Antworten, die heutige Situation zu verstehen, in der die Selbstbestimmung der Frau selbstverständlich sein sollte, die Frage nach den Wahlmöglichkeiten für den Ort der Geburt aber emotional aufgeladen ist.

Dr. Christine Loytved, Hebamme und Medizinhistorikerin, arbeitet als Dozentin am Institut für Hebammen der Zürcher Hochschule für Angewandte Wissenschaften und unterrichtet an weiteren Hebammenstudiengängen in Österreich, Deutschland und der Schweiz.

Moderation: Marie Müller-Zetzsche

Im Rahmen der Veranstaltungsreihe „Gebären und Geburtshilfe – wie steht es um die Selbstbestimmung von Frauen?"

Einlass: 15.30 Uhr
Veranstaltungsbeginn: 16 Uhr
Reservierung von Sitzplätzen nicht möglich!
Eintritt: Spendenempfehlung 2-10 Euro

