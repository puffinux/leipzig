---
id: "2349017498681278"
title: "Barbara Umrath: Geschlecht, Familie, Sexualität"
start: 2019-10-24 19:00
end: 2019-10-24 22:00
address: Institut fuer Zukunft
link: https://www.facebook.com/events/2349017498681278/
image: 71813067_2430397190563059_2288955313769938944_n.jpg
teaser: Kritische Theorie und Geschlecht – Ansätze einer
  materialistisch-feministischen Gesellschaftsanalyse  Vortrag von Barbara
  Umrath  Seit einigen Jahren
isCrawled: true
---
Kritische Theorie und Geschlecht –
Ansätze einer materialistisch-feministischen Gesellschaftsanalyse

Vortrag von Barbara Umrath

Seit einigen Jahren wird wieder vermehrt diskutiert, wie sich materialistisches und feministisches Denken verbinden lassen. Allerdings hat der ‚new materialism’, der in der akademischen Geschlechterforschung aktuell stark rezipiert wird, wenig mit einer Kritik der politischen Ökonomie in Marxscher Tradition zu tun. Marxistisch-feministische Ansätze wiederum stellen in erster Linie auf geschlechtliche Arbeitsteilung als ein entscheidendes Moment kapitalistischer (Re-)Produktionsverhältnisse ab. Welche Zugänge die Kritische Theorie eröffnet, die ab den 1930er-Jahren von Max Horkheimer, Erich Fromm, Theodor W. Adorno, Herbert Marcuse und anderen entwickelt wurde, soll im Vortrag dargestellt und diskutiert werden. Deutlich wird dabei werden, dass deren historisch-materialistische Perspektive über Kapitalismuskritik im engeren Sinne hinausgeht, insofern sie als kritische Theorie bürgerlicher Gesellschaft und Subjektivität immer auch Kritik herrschaftsförmiger Geschlechter- und sexueller Verhältnisse bedeutet.

Barbara Umrath ist wissenschaftliche Mitarbeiterin am Institut für Geschlechterstudien der Fakultät für Angewandte Sozialwissenschaften der TH Köln. Ihre Dissertation „Geschlecht, Familie, Sexualität. Die Entwicklung der Kritischen Theorie aus der Perspektive sozialwissenschaftlicher Geschlechterforschung“ erschien im März. 