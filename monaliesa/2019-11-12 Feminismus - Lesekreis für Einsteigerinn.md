---
id: "1228172357366337"
title: Feminismus - Lesekreis für Einsteiger*innen
start: 2019-11-12 19:15
end: 2019-11-12 21:45
locationName: MONAliesA
address: Bernhard-Göring-Straße 152, 04277 Leipzig-Connewitz, Sachsen
link: https://www.facebook.com/events/1228172357366337/
image: 71680141_2440797932856318_8917817729726021632_n.jpg
teaser: Auch diesen Herbst bieten wir wieder einen feministischen Lesekreis für
  Einsteiger*innen an. Zusammen wollen wir fünf aktuelle feministische Texte les
isCrawled: true
---
Auch diesen Herbst bieten wir wieder einen feministischen Lesekreis für Einsteiger*innen an. Zusammen wollen wir fünf aktuelle feministische Texte lesen und diskutieren und die darin vermittlete Theorie im eigenen Alltag aufspüren. Wenn ihr Lust habt, aktuelle feministische Texte zu lesen, euch eine Meinung dazu zu bilden, euch in einer Gruppe auszutauschen und Gleichgesinnte kennen zu lernen, seid ihr hier genau richtig! Es werden keine Vorkenntnisse erwartet. 
Die Treffen finden jeweils dienstags 19:15 bis 20:45 Uhr in der MONAliesA statt. Für jedes Treffen ist ein Textauszug von ca. 20 Seiten zur Vorbereitung zu lesen. 
Der Lesekreis richtet sich an FLINT* Personen. (Frauen, Lesben, Inter, Nonbinary, Trans*)
Meldet euch bitte verbindlich für die gesamte Reihe unter bibliothek@monaliesa.de an (max. 10 Teilnehmer*innen)