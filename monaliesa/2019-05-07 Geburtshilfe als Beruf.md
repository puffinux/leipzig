---
id: '473924760017925'
title: Geburtshilfe als Beruf
start: '2019-05-07 17:00'
end: '2019-05-07 20:00'
locationName: MONAliesA
address: 'Bernhard-Göring-Straße 152, 04277 Leipzig-Connewitz, Sachsen'
link: 'https://www.facebook.com/events/473924760017925/'
image: 57652686_2315794335356679_883230616781324288_o.jpg
teaser: 'Gespräch mit den Hebammen Annett Heitmann-Mbise und Guisette Quander über Klinik- und Geburtshausalltag, Risiken der Selbstständigkeit und das Handwer'
recurring: null
isCrawled: true
---
Gespräch mit den Hebammen Annett Heitmann-Mbise und Guisette Quander über Klinik- und Geburtshausalltag, Risiken der Selbstständigkeit und das Handwerk der Geburtshilfe

Der Hebammenberuf als einer der ältesten Frauenberufe ist anhand des gesellschaftlichen Umgangs mit Gebären stetig im Wandel. Über 97 Prozent aller Geburten in Deutschland finden in einer Klinik statt, nur wenige im Geburtshaus oder zu Hause. Die meisten Hebammen arbeiten folglich als Angestellte im Krankenhaus oder in der Vor- und Nachsorge, während die selbstständige Arbeit in der Geburtshilfe mit hohen Versicherungssummen belegt ist und immer weniger Krankenhäuser selbstständige Beleghebammen akzeptieren. Das Recht auf freie Hebammenwahl steht somit in Frage.
In den Kreißsälen hat sich seit der Hospitalisierung der Geburt viel getan, in Ostdeutschland vor allem seit 1990: die „aktive“ Geburtshilfe mit vielen Interventionen ist – zumindest vom Anspruch her – einer „sanfteren“ Geburtshilfe gewichen. Es bleiben ökonomische Zwänge und Hierarchien, die eine 1:1-Betreuung von Gebärenden kaum zulassen.
In den kommenden Jahren wird die Hebammenausbildung akademisiert: ein Versuch, den Hebammenberuf mit mehr gesellschaftlicher Anerkennung und besserer Bezahlung neu aufzustellen?

Annett Heitmann-Mbise ist seit 2008 Hebamme und Gründerin des Geburtshauses „Aus dem Bauch heraus“ in Leipzig
Guisette Quander ist seit 1988 Hebamme und arbeitet im Sana-Klinikum Borna
Moderation: Marie Müller-Zetzsche

Die Veranstaltung ist Teil unserer Reihe "Gebären und Geburtshilfe"

Einlass: 18.30 Uhr
Veranstaltungsbeginn: 19 Uhr
Eintritt: Spendenempfehlung 2-10 Euro


