---
id: "1510867505737557"
title: Putsch in Bolivien
start: 2020-01-22 18:00
end: 2020-01-22 20:00
address: Schaubühne Lindenfels
link: https://www.facebook.com/events/1510867505737557/
image: 81370314_3692730240738923_3490995464533180416_n.jpg
teaser: "Thema: Putsch in Bolivien Film „Before the Coup“ (Bolivien 2020, Diego
  Gonzales, spanisch mit engl UT, 25min) und Vortrag von Rumi Muruchi Poma über
  d"
isCrawled: true
---
Thema: Putsch in Bolivien
Film „Before the Coup“ (Bolivien 2020, Diego Gonzales, spanisch mit engl UT, 25min) und Vortrag von Rumi Muruchi Poma über die
Hintergründe des Putsches und die aktuelle Situation.

Am 12. November 2019 ersetzte in Bolivien eine reaktionäre Putschregierung eine demokratisch gewählte Regierung. Die Regierung von Evo Morales und Alvaro Linera verkündete am 10. November 2019 ihren erzwungenen Rücktritt, nachdem das Haus von Evos Schwester und die Häuser einiger seiner Minister und Gouverneure aus der Partei MAS (Bewegung zum Sozialismus) niedergebrannt wurden. Die internationale und nationale Presse, vertuscht den Charakter dieses Putsches mit dem Argument des Wahlbetrugs. 

Nichts rechtfertigt einen Staatsstreich und die Jagd auf die Ureinwohner, die mit der De-facto-Regierung von Jeannine Áñez begann. Zahlreiche Opfer sind seither zu beklagen. Das einzige „Verbrechen“ von Evo Morales ist es, ein Indio zu sein, der die Indios sehr schlecht verteidigte und ihre Henker finanziell gemästet hat…

MittwochsATTACke am 22.01. 18 Uhr
Schaubühne Lindenfels, K.-Heine-Str. 50
Film: „Before the coup“
(Diego Gonzales, Bolivien 2020, Erstausstrahlung)
Referent: Rumi Muruchi Poma  

Der Eintritt ist frei.