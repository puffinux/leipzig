---
id: '589504454877702'
title: 'GlobaLE Kino: Berlin - Prenzlauer Berg'
start: '2019-08-08 20:00'
end: '2019-08-08 23:00'
locationName: Richard-Wagner-Hain
address: 'Jahnallee / Lützner Straße, 04109 Leipzig'
link: 'https://www.facebook.com/events/589504454877702/'
image: 64369149_3091519560859997_7526311586395324416_o.jpg
teaser: 'Wann: Do 08 August 2019 20:00 - 22:30  Berlin - Prenzlauer Berg - Begegnungen zwischen dem 1.Mai und dem 1.Juli 1990.  DDR / 1990 / Petra Tschörtner /'
recurring: null
isCrawled: true
---
Wann: Do 08 August 2019 20:00 - 22:30

Berlin - Prenzlauer Berg - Begegnungen zwischen dem 1.Mai und dem 1.Juli 1990.

DDR / 1990 / Petra Tschörtner / 75 min / dt. / Im Anschluss Diskussion mit Dr. Matthias Bernt (Leibnitz Institute for Research on Society and Space). Eintritt frei.

Bilder aus dem Berliner Stadtbezirk Prenzlauer Berg in der Zeit vor der Währungsunion. "We need revolution" singt "Herbst in Peking" aus dem Prenzlauer Berg in den Trümmern der Mauer am Rande ihres Stadtbezirks. Dabei ist im Mai ’90 schon fast alles gelaufen, die Annexion sicher und das Ende der Demokratischen Republik besiegelt. Im „Prater“ schwooft Knatter-Karl mit seiner Freundin. Frieda und Gerda im "Hackepeter" sind erschüttert; denn gleich nach dem Fall der Mauer wurde im Tierpark ein Papagei gestohlen. Die Polizei jagt bewaffnete Männer, während Näherinnen erklären, warum die Vietnamesen zuerst entlassen werden. Ein einsamer Gast aus dem "Wiener Cafe" singt zum Abschied das Lied von der Heimat, während die rumänische Combo zum Balkan-Express zurückeilt. Die Hausbesetzer träumen von Anarchie und Frau Ziervogel, Inhaberin von Berlins berühmtester Würstchenbude "Konnopke", segnet das erste Westgeld. Der Tag der Währungsunion ist da. Filipp Moritz besetzt den Prenzlauer Berg. Der Staatsvertrag zwischen den beiden deutschen Staaten und der Beginn der Währungsunion am 1.7.1990 durchzieht den Dokumentarfilm und zeigt die hohen Erwartungen, die manche daran hegen, aber auch die befürchteten negativen Auswirkungen auf die Menschen und die Gesellschaft.

Wo: Richard-Wagner-Hain am Elsterbecken