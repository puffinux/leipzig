---
id: "536236607148734"
title: "GlobaLE Kino: Über Bananen und Republiken"
start: 2019-10-16 20:00
end: 2019-10-16 22:30
locationName: Neues Schauspiel Leipzig
address: Lützner Str. 29, 04177 Leipzig
link: https://www.facebook.com/events/536236607148734/
image: 64995948_3092231580788795_200879348465532928_o.jpg
teaser: "Wann: Mi 16 Oktober 2019 20:00 - 22:30  Über Bananen und Republiken  BRD,
  Frankreich / 2017 / 52 min / Mathilde Damoisel / dt. / Im Anschluss Diskussi"
isCrawled: true
---
Wann: Mi 16 Oktober 2019 20:00 - 22:30

Über Bananen und Republiken

BRD, Frankreich / 2017 / 52 min / Mathilde Damoisel / dt. / Im Anschluss Diskussion. Eintritt frei.

Die Dokumentation schildert, wie die Banane zum weltweiten Exportprodukt und die United Fruit Company - heute als „Chiquita“ bekannt - zu einem multinationalen Imperium wurde. Ein Imperium das mittelamerikanische Nationen zu "Bananenrepubliken" degradierte und mit Monokulturen hektarweise Landstriche zerstörte. Außerdem wird gezeigt, wie sich amerikanische Abenteurer in diktatorische "Bananenunternehmer" verwandelten, die mithilfe eines gerissenen Werbefachmanns den Grundstein des globalisierten Kapitalismus legten.

Wo: Neues Schauspiel, (Lützner Straße 29)