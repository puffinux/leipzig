---
id: '445654225999129'
title: 'GlobaLE Kino: Das Land der Erleuchteten'
start: '2019-08-02 20:00'
end: '2019-08-02 23:00'
locationName: null
address: Wagenplatz Toter Arm
link: 'https://www.facebook.com/events/445654225999129/'
image: 62613482_3091517290860224_7997097168051634176_o.jpg
teaser: 'Wann: Fr 02 August 2019 20:00 - 23:00 Das Land der Erleuchteten  Afghanistan, BRD, Niederlande, Irland / 2015 / Pieter-Jan De Pue / original mit dt. U'
recurring: null
isCrawled: true
---
Wann: Fr 02 August 2019 20:00 - 23:00
Das Land der Erleuchteten

Afghanistan, BRD, Niederlande, Irland / 2015 / Pieter-Jan De Pue / original mit dt. UT / Im Anschluss Diskussion mit Bahadur Rajabi. Eintritt frei.

„Immer, wenn ein Afghane einen Bruder tötet, wird ein neuer Stern geboren. Bald wird die Nacht so hell sein wie der Tag... Eines Tages werden wir zusammen in die Nacht reisen und dem Tag sein Licht zurückbringen.“

Der raue, abgelegene Nordosten Afghanistans ist eine Hochgebirgslandschaft mit Gipfeln bis zu 7.500 Metern. Es ist das Grenzgebiet zu Tadschikistan, Pakistan und China. In dieser Region verdienen sich nomadisch lebende Kinderbanden etwas Geld mit den Überbleibseln der vielen Kriege, unter denen ihr Land seit langer Zeit zu leiden hatte. Sie sammeln alle Arten von Metallresten, um sie zu verkaufen. Sie graben Landminen aus, um deren explosiven Inhalt an Kinder weiterzugeben, die in den Lapis Lazuli Minen arbeiten. Andere Kinderbanden leben ihr eigenes, nomadisches Leben und verdienen ihren Lebensunterhalt damit, Karawanen und Schmuggler auf ihrem Weg zur Grenze zu überfallen - oder sie lassen sich dafür bezahlen, ihnen Begleitschutz zu gewähren. Die Währung ist Opium.
Immer wieder hängen die Kinder ihren Träumen nach, in denen die amerikanischen Besatzer ein für allemal das Land verlassen. Der Film entstand über einen Zeitraum von 7 Jahren. Nahtlos verschmilzt in dem Film die dokumentarische Beobachtung mit der fiktionalen Erzählung - die harsche Realität, die den Alltag der Kinder prägt, mit ihren Wünschen, Träumen und Hoffnungen. Der Regisseur erschafft eine hybride Erzählform, in der er immer wieder die Grenzen des dokumentarischen Genres auslotet und erschafft einen bildgewaltigen Film von magischer Intensität.

Wo: Wagenplatz Toter Arm, Lauerscher Weg 70a, Nahe Cospudener See