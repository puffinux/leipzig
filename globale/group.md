---
name: GlobaLE Leipzig
website: http://www.globale-leipzig.de/
email: info@globale-leipzig.de
scrape:
  source: facebook
  options:
    page_id: 471789386166374
# TODO: write website crawler
---
Das Projekt globaLE ist ein politisches Filmfestival auf Initiative von attac Leipzig, welches Film als Medium nutzt, um die weltweiten Zusammenhänge und Auswirkungen kapitalistischer Ökonomie zu dokumentieren, aber auch den Widerstand gegen Ausbeutung und Ausgrenzung zu zeigen und wie Menschen ihren Mut, ihre Würde und ihre Hoffnung nicht verlieren.