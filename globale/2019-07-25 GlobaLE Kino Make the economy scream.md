---
id: '568501917010296'
title: 'GlobaLE Kino: Make the economy scream'
start: '2019-07-25 20:00'
end: '2019-07-25 23:00'
locationName: null
address: 'Caracan im Auenwald, Neue Linie 20'
link: 'https://www.facebook.com/events/568501917010296/'
image: 64252126_3091511237527496_3105267213343391744_o.jpg
teaser: 'Wann: Do 25 Juli 2019 20:00 - 23:00 Make the economy scream, Griechenland / 2019 / 80 min / Aris Chatzistefanou / original mit dt. UT / Anschließend D'
recurring: null
isCrawled: true
---
Wann: Do 25 Juli 2019 20:00 - 23:00
Make the economy scream, Griechenland / 2019 / 80 min / Aris Chatzistefanou / original mit dt. UT / Anschließend Diskussion mit dem Aktivisten und Buchautoren Harri Grünberg. Eintritt frei.

Manche Leute sehen, dass die Supermarktregale völlig leer sind. Andere sehen sie völlig voll. Einige geben dem Sozialismus die Schuld. Andere dem Fehlen davon. Für einige gibt es politische Gefangene in Venezuela. Für andere gibt es sie auch in Katalonien. Venezuela ist Opfer eines autoritären Regimes. Aber ist dieses Regime in Caracas oder in Washington angesiedelt? Der Film will helfen das herauszufinden. Mit Hilfe von bekannten Personen aus Wirtschaft, Politik und Medien beschreibt der Film die aktuelle Situation, geht Hintergründen, Ursachen und Interessen auf den Grund und hinterfragt interessengeleitete Medienberichterstattung.

Wo: Caracan im Auenwald, (Neue Linie 20)