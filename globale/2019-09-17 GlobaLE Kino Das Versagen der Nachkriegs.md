---
id: '325484788389710'
title: 'GlobaLE Kino: Das Versagen der Nachkriegsjustiz'
start: '2019-09-17 20:00'
end: '2019-09-17 23:00'
locationName: null
address: 'Villa Davignon, Friedrich-Ebert-Straße 77'
link: 'https://www.facebook.com/events/325484788389710/'
image: 64388747_3091849087493711_2580917947782397952_o.jpg
teaser: 'Wann: Di 17 September 2019 20:00 - 22:30  Das Versagen der Nachkriegsjustiz  BRD / 2014 / 44 min / Christoph Weber / dt. /  Im Anschluss Buchvorstellu'
recurring: null
isCrawled: true
---
Wann: Di 17 September 2019 20:00 - 22:30

Das Versagen der Nachkriegsjustiz

BRD / 2014 / 44 min / Christoph Weber / dt. / 
Im Anschluss Buchvorstellung "Im Namen des Volkes" und Diskussion mit den Autoren Dieter Skiba und Reiner Stenzel. Gemeinsame Veranstaltung mit der Zeitschrift Rotfuchs. Eintritt frei.

Als die BRD entstand, war fast ihre gesamte Bevölkerung noch die alte NS-Volksgemeinschaft. Was Wunder, dass Prozesse gegen Nazi-Verbrecher und Schreibtischtäter praktisch nicht mehr stattfanden, nachdem die Alliierten die Aufgabe übertragen hatten. In der Bundesrepublik wurde faschistischen Täterinnen und Tätern durch eine ganze Reihe von Amnestiegesetzen der Weg zur Straflosigkeit geebnet. Dafür sorgten hochrangige Beamtinnen und Beamte im Bundesjustizministerium, viele von ihnen selbst belastet. So weit, so bekannt; nicht ganz so bekannt ist, dass die Bundesrepublik noch bis in dieses Jahrhundert hinein vieles unternahm, um solche Prozesse gar nicht erst stattfinden zu lassen. Aus Gründen des internationalen Ansehens. Der Dokumentarfilm erzählt diese Geschichte mit einer Fülle von Aufnahmen.
In der Veranstaltung wollen wir den Umgang in Ost- und Westdeutschland mit dem Thema diskutieren sowie auf die Relevanz für heute eingehen.

Wo: Villa Davignon, (Friedrich-Ebert-Straße 77)