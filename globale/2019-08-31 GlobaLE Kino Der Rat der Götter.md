---
id: '2523556094538889'
title: 'GlobaLE Kino: Der Rat der Götter'
start: '2019-08-31 20:00'
end: '2019-08-31 23:00'
locationName: null
address: 'ehemaliges Kino der Jugend, Eisenbahnstraße 162'
link: 'https://www.facebook.com/events/2523556094538889/'
image: 62556468_3091533047525315_4469584418394079232_o.jpg
teaser: 'Wann: Sa 31 August 2019 20:00 - 22:30  Der Rat der Götter  DDR / Spielfilm / 1950 / 111 min / Kurt Maetzig / dt. / Im Anschluss Diskussion. Vorab vora'
recurring: null
isCrawled: true
---
Wann: Sa 31 August 2019 20:00 - 22:30

Der Rat der Götter

DDR / Spielfilm / 1950 / 111 min / Kurt Maetzig / dt. / Im Anschluss Diskussion. Vorab voraussichtlich um 17 Uhr auf dem Marktplatz, Kundgebung und Verleihung des diesjährigen Leipziger Friedenspreises anlässlich des Weltfriedenstages am Folgetag. Aktuelle Infos vorab unter: www.Leipzig-gegen-krieg.de.
Gemeinsame Veranstaltung mit der IG Fortuna am Vorabend des Weltfriedenstages. Abendveranstaltung im Rahmen des Bülowstraßenmusikfestivals.

Der Film thematisiert die Verstrickung der IG Farben in Rüstungsproduktion und Giftgasherstellung für die Konzentrationslager. Dem Vorstandsvorsitzenden Geheimrat Mauch geht es um Expansion und Gewinn für die Firma um jeden Preis. Der Chemiker Dr. Scholz ist ein Mitläufer, der sich aus Angst um Stellung und Familie der Wahrheit verschließt. Als 1948 eine verheerende Explosionskatastrophe in Ludwigshafen beweist, dass der Konzern trotz Verbotes der Alliierten wieder Sprengstoff produziert, bricht Scholz sein Schweigen.

Bündnis "Leipzig gegen Krieg": http://www.leipzig-gegen-krieg.de.
Aufruf "Abrüsten statt Aufrüsten": https://abruesten.jetzt/.

Wo: ehemaliges Kino der Jugend (Eisenbahnstraße 162)