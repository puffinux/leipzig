---
id: '1638791629593133'
title: 'GlobaLE Kino: Waltz with Bashir'
start: '2019-09-18 20:00'
end: '2019-09-18 23:00'
locationName: UT Connewitz
address: 'Wolfgang-Heinze-Str. 12a, 04277 Leipzig'
link: 'https://www.facebook.com/events/1638791629593133/'
image: null
teaser: 'Wann: Mi 18 September 2019 20:00 - 22:30 Waltz with Bashir  Israel, Frankreich, BRD / 2008 / 90 min / Ari Folman / dt. / Im Anschluss Diskussion mit J'
recurring: null
isCrawled: true
---
Wann: Mi 18 September 2019 20:00 - 22:30
Waltz with Bashir

Israel, Frankreich, BRD / 2008 / 90 min / Ari Folman / dt. / Im Anschluss Diskussion mit Jacqueline Andres (Informationsstelle Militarisierung, IMI). Eintritt frei.

Eines Nachts in einer Bar erzählt ein alter Freund dem Regisseur Ari von einem immer wiederkehrenden Alptraum, in dem er von 26 dämonischen Hunden gejagt wird. Jede Nacht, immer genau 26 Bestien. Die beiden Männer kommen zu dem Schluss, dass ein Zusammenhang zu ihrem Einsatz im ersten Libanon Krieg bestehen muss. Ari ist überrascht, denn er hat jegliche Erinnerung an diese Zeit verloren. Verstört macht er sich auf, Freunde und Kameraden von damals zu besuchen und zu befragen. Er muss die Wahrheit über jene Zeit und über sich selbst herausfinden. Je tiefer Ari in seine Vergangenheit eindringt, desto klarer werden seine Gedanken und die verdrängten Erlebnisse erscheinen in surrealen Bildern.
Hintergrund: Vor 37 Jahren, - zwischen dem 16. und 18. September 1982 – mitten im libanesischen Bürgerkrieg – wurden die Flüchtlingslager Sabra und Schatila von phalangistischen Milizen gestürmt. Dabei wurden sie vom israelischen Militär, durch Abriegelung der Lager und Abfeuern von Leuchtraketen unterstützt. Nach filmisch belegten Aussagen beteiligter Milizionäre richtete sich die Aktion in erster Linie gegen Zivilisten, bewaffneter Widerstand soll kaum vorhanden gewesen sein. Die Milizionäre verstümmelten, folterten, vergewaltigten und töteten überwiegend Zivilisten, unter ihnen viele Frauen, Kinder und Alte. Das Massaker fand unter Aufsicht des israelischen Militärs statt und wurde damals weltweit verurteilt.

Wo: UT Connewitz, Wolfgang-Heinze-Straße 12a