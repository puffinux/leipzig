---
id: "1328985580586769"
title: "GlobaLE Kino: Guerrero"
start: 2019-11-06 20:00
end: 2019-11-06 23:00
address: Neues Schauspiel Leipzig
link: https://www.facebook.com/events/1328985580586769/
image: 64362898_3092244574120829_673604928194740224_o.jpg
teaser: "Wann: Mi 06 November 2019 20:00 - 22:30  Guerrero  Mexiko / 2017 / 114 min /
  Ludovic Bonleux / spanisch mit dt. UT / Im Anschluss Diskussion mit Gäste"
isCrawled: true
---
Wann: Mi 06 November 2019 20:00 - 22:30

Guerrero

Mexiko / 2017 / 114 min / Ludovic Bonleux / spanisch mit dt. UT / Im Anschluss Diskussion mit Gästen. Eintritt frei.

Im mexikanischen Bundesstaat Guerrero unterstellt die Bevölkerung der Regierung, mit den Drogenkartellen zusammenzuarbeiten. Deshalb organisiert sich die Menschen, um selbst die Probleme und Gewalt, die durch Drogenhandel und Korruption entstanden sind, anzugehen. Der Filmemacher Ludovic Bonleux hat bereits zahlreiche Filme über die Lebensumstände im mexikanischen Bundesstaat Guerrero gedreht. In seiner neuesten Dokumentation begleitet er drei Menschen, die sich entschlossen haben, sich zu organisieren und gegen den Zustand von Straflosigkeit und Gewalt Widerstand zu leisten, der sie beständig umgibt: Coni, eine Frau in der eigenorganisierten Gemeindepolizei, die gegen die schlechte Regierung sowie am Drogenhandel beteiligte Gangs kämpft. Mario, einen jungen Mann auf der Suche nach seinem verschwundenen Bruder, und Juan, einen Lehrer in einer ländlichen Region. Sie sprechen über ihre Überzeugungen, ihre Ängste und Zweifel. Ihre Geschichten werden in den Kontext des Chaos gesetzt, dass sich in Folge des Verschwindens der 43 Studenten der Escuela de Maestros in Ayotzinapa ergeben hat.

Wo: Neues Schauspiel, (Lützner Straße 29) 