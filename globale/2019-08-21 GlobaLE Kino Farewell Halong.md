---
id: '2217032251942432'
title: 'GlobaLE Kino: Farewell Halong'
start: '2019-08-21 20:00'
end: '2019-08-21 22:30'
locationName: Eutritzscher Markt
address: 'Delitzscher Str., 04129 Leipzig'
link: 'https://www.facebook.com/events/2217032251942432/'
image: 64270483_3091526177526002_4597363389121953792_o.jpg
teaser: 'Wann: Mi 21 August 2019 20:00 - 22:30  Farewell Halong  Vietnam / 2017 / 98 min / Duc Ngo Ngoc / original mit dt. UT / Anschließend Diskussion mit dem'
recurring: null
isCrawled: true
---
Wann: Mi 21 August 2019 20:00 - 22:30

Farewell Halong

Vietnam / 2017 / 98 min / Duc Ngo Ngoc / original mit dt. UT / Anschließend Diskussion mit dem Filmemacher Duc Ngo Ngoc.
Eintritt frei.

In der Bucht von Ha Long im Norden Vietnams lebt der 46-jährige Nguyen Van Cuong mit seiner Familie in seiner selbstgebauten Hütte auf einem Floss. Für ihn ist das Leben auf dem Wasser normal - hier schläft, kocht und arbeitet die Familie schon seit Generationen. Doch der Schein der Idylle trügt.

Goung muss seine außergewöhnliche Umgebung verlassen, da die Regierung die Bewohner des schwimmenden Dorfes auf das Land umsiedeln will. Goung ist unschlüssig und schiebt die Entscheidung vor sich her. Als Kapitän steuert er auf eine äußerst ungewisse Zukunft zu - ob er sie auf dem Land oder auf dem Wasser verbringen wird, bleibt ungewiss.

Wo: Eutritzscher Markt