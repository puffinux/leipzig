---
id: '865462593821392'
title: 'GlobaLE Kino: Der marktgerechte Patient'
start: '2019-08-14 20:00'
end: '2019-08-14 23:00'
locationName: null
address: 'Schönauer Park, Leipzig Grünau'
link: 'https://www.facebook.com/events/865462593821392/'
image: 65013337_3091523037526316_7005927460411801600_o.jpg
teaser: 'Wann: Mi 14 August 2019 20:00 - 23:00  Der marktgerechte Patient  BRD / 2018 / 72 min / Leslie Franke und Herdolor Lorenz / dt. / Anschließend Diskuss'
recurring: null
isCrawled: true
---
Wann: Mi 14 August 2019 20:00 - 23:00

Der marktgerechte Patient

BRD / 2018 / 72 min / Leslie Franke und Herdolor Lorenz / dt. / Anschließend Diskussion mit Gästen. Die Veranstaltung findet im Rahmen des Schönauer Parkfests in Kooperation mit dem Kommhaus e.V. statt und ist Teil des Grünauer Kultursommers. Eintritt frei.

Es gibt zwar zahllose Berichte über skandalöse Zustände in den deutsche Krankenhäusern, erstaunlicherweise fehlt dabei aber fast immer der Bezug auf die wesentliche Ursache dieser Zustände: Die seit 2003 verbindliche Vergütung der Krankenhäuser durch sog. Fallpauschalen (jede diagnostizierbare Krankheit hat einen fixen Preis – wer mit möglichst geringen Personal-, Sach- und Organisationskosten den Patienten optimal schnell abfertigt, macht Gewinn – wer sich auf die Patienten einlässt und Tarife zahlt, macht Verlust). Die Einführung der sog. DRGs (Diagnosis Related Groups) war der radikale Schritt zur kompromisslosen Kommerzialisierung eines Bereichs, der bis dahin vom Gedanken der Empathie und Fürsorge getragen wurde. Seither wird der Mensch dort, wo er am Verletzlichsten ist, nämlich als hilfsbedürftiger Patient, den gnadenlosen Prinzipien von Gewinn und Verlust untergeordnet. "Wir sind nicht an der Zurschaustellung von Skandalen interessiert. Uns kommt es bei der Aufdeckung von Folgen vor allem auf die Ursachen der unhaltbaren Zustände in den deutschen Krankenhäusern an. Nur so sind sie zu verändern!" sagen die beiden Filmemacher Leslie Franke und Herdolor Lorenz.
Die Flucht aus den Krankenhäusern ist für Pflegerinnen und Pfleger bereits real, weil sie trotz unsäglichem Stress nicht mehr zur wirklichen Pflege der Patienten kommen. Aber auch die meisten Ärzte sind es leid, gezwungen zu sein, in erster Linie auf die Profitabilität ihrer Abteilung zu achten.

Mehr zum Projekt: http://der-marktgerechte-patient.org/index.php/de/.

Wo: Schönauer Park, Grünau, Haltestelle Parkallee (im Rahmen des Grünauer Parkfests)