---
id: '632031120617625'
title: 'GlobaLE Kino: Die Gentrifizierung bin Ich'
start: '2019-08-22 20:00'
end: '2019-08-22 23:00'
locationName: RSL Sportanlage Teichstraße
address: 'Teichstraße 12, 04277 Leipzig'
link: 'https://www.facebook.com/events/632031120617625/'
image: 64398763_3091527920859161_3176773784552079360_o.jpg
teaser: 'Wann: Do 22 August 2019 20:00 - 23:00  Die Gentrifizierung bin ich - Beichte eines Finsterlings  Schweiz / 2018 / 98 min / Thomas Haemmerli / original'
recurring: null
isCrawled: true
---
Wann: Do 22 August 2019 20:00 - 23:00

Die Gentrifizierung bin ich - Beichte eines Finsterlings

Schweiz / 2018 / 98 min / Thomas Haemmerli / original mit dt. UT / Im Anschluss Diskussion mit Aktivist/innen. Gemeinsame Veranstaltung mit dem Roten Stern Leipzig. Eintritt frei.

(Vorab nachmittags Volleyballturnier auf dem Gelände.)

Thomas Haemmerli hielt sich für einen linken Freigeist. Bis er feststellen musste: Sein Verhalten sorgt für die Umwandlung ganzer Stadtviertel. In dem ihm eigenen persönlichen, bisweilen sarkastischen und ausgesprochen witzigen Stil verarbeitete er diese Erkenntnis zu einer klugen und unterhaltsamen Betrachtung zu den Themen: Wie sollen und wollen wir wohnen? Wie sollen unsere Landschaften und Städte in Zukunft aussehen? Stimmt es, dass die Reichen immer die weniger Zahlungskräftigen verdrängen und dadurch „Ghettos“ entstehen? Und was hat das alles mit São Paulo zu tun?
„Die Gentrifizierung bin ich“ ist ein kluger, lustiger und umfassender Dokumentar-Essay, der Raumgebrauch, Wohnen, Stadtentwicklung, Dichte, Fremdenfeindlichkeit und Gentrifizierung thematisiert. Dabei verschreibt sich das Projekt einem autobiografischen Zugriff: Der große Bogen sind diverse Wohnsituationen des Autors, begonnen mit der Kindheit im Reichenghetto, über besetzte Häuser, WGs und Yuppie-Wohnungen, bis hin zu Behausungen in Großstädten wie Tiflis, São Paulo und Mexiko-Stadt.

Wo: Roter Stern Leipzig ‘99 e.V., (Teichstraße 12)