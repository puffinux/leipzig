---
id: '2413116352079713'
title: 'GlobaLE Kino: Liebe und Revolution'
start: '2019-09-05 20:00'
end: '2019-09-05 23:00'
locationName: Heizhaus Leipzig
address: 'Alte Salzstraße 63, 04209 Leipzig'
link: 'https://www.facebook.com/events/2413116352079713/'
image: 64322276_3091535634191723_817136760927551488_o.jpg
teaser: 'Wann: Do 05 September 2019 20:00 - 22:30  Liebe und Revolution  Griechenland / 2018 / 86 min / Yannis Youlountas / original mit dt. UT / Anschließend'
recurring: null
isCrawled: true
---
Wann: Do 05 September 2019 20:00 - 22:30

Liebe und Revolution

Griechenland / 2018 / 86 min / Yannis Youlountas / original mit dt. UT / Anschließend Diskussion mit Dr. Nadja Rackowitz (Verein demokratischer Ärztinnen und Ärzte). Die Veranstaltung findet im Rahmen des Grünauer Kultursommers statt. Eintritt frei.

Fast zehn Jahre EU-Austeritätspolitik gegen Griechenland. Liberalisierung, Privatisierung, Sozialkürzungen, - Schleifung eines ganzen Landes, seiner Ökonomie und seiner Bevölkerung. Die Auswirkungen der brutalen, vor allem durch die deutsche Bundesregierung in der EU vorangetriebenen Politik sind fatal und heute vielfältig sichtbar.
Der große Teil der europäischen Medien behauptet, dass die Politik der Sparmaßnahmen in Griechenland ein Erfolg war und Ruhe eingekehrt sei. Der Film beweist das Gegenteil. Eine musikalische Reise vom Norden in den Süden Griechenlands mit Menschen, die von Liebe und Revolution träumen.

Wo: Skatehalle Heizhaus Grünau, (Alte Salzstraße 68). Bei schönem Wetter draußen vor der Halle im Skatepark.