---
id: '309660749939777'
title: 'GlobaLE Kino: Luft zum atmen'
start: '2019-08-09 20:00'
end: '2019-08-09 23:00'
locationName: null
address: Wagenplatz Toter Arm
link: 'https://www.facebook.com/events/309660749939777/'
image: 64383043_3091521884193098_1310113450759815168_o.jpg
teaser: 'Wann: Fr 09 August 2019 20:00 - 22:30  Luft zum atmen - 40 Jahre Opposition bei Opel in Bochum  BRD / 2019 / 70 min / Johanna Schellhagen / dt. / Im A'
recurring: null
isCrawled: true
---
Wann: Fr 09 August 2019 20:00 - 22:30

Luft zum atmen - 40 Jahre Opposition bei Opel in Bochum

BRD / 2019 / 70 min / Johanna Schellhagen / dt. / Im Anschluss Diskussion u.a. mit Bärbel Schönafinger (Labournet TV). Eintritt frei.

1972 gründeten ein paar Arbeiter und Revolutionäre bei Opel in Bochum die "Gruppe oppositioneller Gewerkschafter"(GoG). Die GoG existierte über 40 Jahre und hat mit ihrer radikalen Betriebsarbeit den Widerstandsgeist in der Bochumer Belegschaft befeuert. Als Betriebsräte gaben sie geheime Informationen an die Belegschaft weiter, sie sorgten für achtstündige Betriebsversammlungen, kämpften gegen Krankenverfolgung, organisierten ihren eigenen Bildungsurlaub und versuchten sogar, auf eigenen Faust direkte internationale Solidarität zwischen den verschiedenen General Motors Belegschaften in Europa herzustellen, um sich gegen die Standorterpressungen in den 90er Jahren zur Wehr zu setzen. Ihre radikalen Aktivitäten kulminierten schließlich in einem der wichtigsten Wilden Streiks deutschen Nachkriegsgeschichte, als die Belegschaft im Oktober 2004 sechs Tage lang das Werk besetzte und die Produktion in ganz Europa lahmlegte. Ein Portrait von Kollegen, die sich Gehör verschafften.

PS: Der Film lief in einer Rohfassung bereits im Herbst vergangenen Jahres im Rahmen der globaLE. Damals war der Arbeitstitel noch 'Gegenwehr ohne Grenzen' und mit Uwe Lübke und Wolfgang Schaumberg hatten wir zwei der Protagonisten zu Gast. Dieses Jahr zeigen wir den fertigen Film.

Wo: Wagenplatz Toter Arm, Lauerscher Weg 70a, Nahe Cospudener See