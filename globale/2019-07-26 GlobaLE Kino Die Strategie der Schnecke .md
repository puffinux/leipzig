---
id: '2495229713843484'
title: 'GlobaLE Kino: Die Strategie der Schnecke + Party'
start: '2019-07-26 20:00'
end: '2019-07-26 23:00'
locationName: null
address: 'Caracan im Auenwald, Neue Linie 20'
link: 'https://www.facebook.com/events/2495229713843484/'
image: 65954042_3149714828373803_8292897681760583680_o.jpg
teaser: 'Wann: Fr 26 Juli 2019 20:00 - 23:00  Die Strategie der Schnecke,  Kolumbien / Spielfilm / 1993 / 107 min / Sergio Cabrera / spanisch mit dt. UT / Eint'
recurring: null
isCrawled: true
---
Wann: Fr 26 Juli 2019 20:00 - 23:00

Die Strategie der Schnecke, 
Kolumbien / Spielfilm / 1993 / 107 min / Sergio Cabrera / spanisch mit dt. UT / Eintritt frei.

Danach Party "15 Jahre globaLE" mit Tanzflächenaufleger (Latino/Afro/Electronica). :)

Ein altes Mietshaus in einem Vorort der kolumbianischen Hauptstadt Bogotá. Der Besitzer, ein aufgeblasener Emporkömmling, will das Haus «entmieten», die Bewohner auf die Straße setzen. Doch hier leben schon seit Jahren Menschen zusammen, die sich trotz der drangvollen Enge in diesem Haus heimisch fühlen: Don Jacinto, der alte Anarchist; Romero, der Anwalt, der keine Zulassung hat, doch die Gesetzestricks kennt; ein junger Revolutionär, der sich gern auf die Massen stürzt; ein Pater, der im Diesseits Befriedigung sucht; Gabriel, der sich als Gabriela verkauft; eine Alte, die mit einem Scheintoten lebt und der ein Wunder geschieht. Sie alle haben keine Chance, aber sie nutzen sie. Denn der Anarchist Jacinto entwickelt einen schlitzohrig-genialen Plan, wie man die noch verbleibende Zeit bis zum angedrohten Rauswurf nutzen kann: Die Strategie der Schnecke.

Wo: Caracan im Auenwald, (Neue Linie 20)