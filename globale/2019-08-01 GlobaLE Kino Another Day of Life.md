---
id: '1327654600718690'
title: 'GlobaLE Kino: Another Day of Life'
start: '2019-08-01 20:00'
end: '2019-08-01 23:00'
locationName: null
address: 'Clara-Zetkin-Park, 04178 Leipzig'
link: 'https://www.facebook.com/events/1327654600718690/'
image: 64657924_3091516307526989_5302484439442391040_o.jpg
teaser: 'Wann: Do 01 August 2019 20:00 - 22:30  Another Day of Life  Polen, Spanien, Belgien, BRD, Ungarn / 2018 / 86 min / Raúl de la Fuente, Damian Nenow / d'
recurring: null
isCrawled: true
---
Wann: Do 01 August 2019 20:00 - 22:30

Another Day of Life

Polen, Spanien, Belgien, BRD, Ungarn / 2018 / 86 min / Raúl de la Fuente, Damian Nenow / deutsch und original mit dt. UT / Im Anschluss Diskussion. Eintritt frei.

Der Film schildert das Drama des Angolanischen Bürgerkriegs in 1975 aus Sicht des legendären Kriegsreporters Ryszard Kapuscinski. In einer Mischung aus Animationssequenzen und aktuellen dokumentarischen Interviews mit Kapuscinskis Weggefährten von damals vermittelt der Film die Schrecken und Absurditäten des Krieges.
Zugleich erlebt der Zuschauer den Übergang des Protagonisten vom objektiven Berichterstatter zum Schriftsteller, der versucht mit den Mitteln der Literatur der Wahrheit des Krieges näher zu kommen. Der Film basiert lose auf Motiven des Romans "Another Day of Life" (Wieder ein Tag Leben), in dem Kapuscinski mit seismografischer Sensibilität und detailgenauem Blick seine Erfahrungen während des Bürgerkriegs verarbeitet hat.

Wo: Clara-Zetkin-Park (auf der Wiese zwischen Glashaus und Sachsenbrücke)