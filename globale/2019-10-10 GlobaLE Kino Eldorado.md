---
id: '445101002713043'
title: 'GlobaLE Kino: Eldorado'
start: '2019-10-10 20:00'
end: '2019-10-10 23:00'
locationName: Werkcafé
address: 'Eisenacher Str. 72, 04155 Leipzig'
link: 'https://www.facebook.com/events/445101002713043/'
image: 63381722_3092177037460916_4744628192680083456_o.jpg
teaser: 'Wann: Do 10 Oktober 2019 20:00 - 22:30  Eldorado  BRD, Schweiz / 2018 / 92 min / Markus Imhoof / original mit dt. UT / Im Anschluss Diskussion. Eintri'
recurring: null
isCrawled: true
---
Wann: Do 10 Oktober 2019 20:00 - 22:30

Eldorado

BRD, Schweiz / 2018 / 92 min / Markus Imhoof / original mit dt. UT / Im Anschluss Diskussion. Eintritt frei.

Der Film verbindet Kindheitserfahrungen des Regisseurs mit gegenwärtigen Beobachtungen aus der "Festung Europa". Eldorado begleitet Geflüchtete entlang der globalen Migrationsroute nach Norden, übers Meer, durch Lager, Tomatenplantagen und graue Amtsstuben.

"Das Einzige, was uns am Ende bleibt, sind Erinnerungen, die auf Liebe basieren."

Es ist eine solche Erinnerung, die Markus Imhoof sein Leben lang begleitet hat: Es ist Winter, die Schweiz ist das neutrale Land inmitten des Zweiten Weltkriegs und seine Mutter wählt am Güterbahnhof ein italienisches Flüchtlingskind aus, um es aufzupäppeln. Das Mädchen heißt Giovanna – und verändert den Blick, mit dem der kleine Markus die Welt sieht.
70 Jahre später kommen wieder Fremde nach Europa. Markus Imhoof hat Giovanna nie vergessen, hat ihre Spuren verfolgt und in ihrem Land gelebt. Nun geht er an Bord eines Schiffes der italienischen Marine, es ist die Operation "Mare Nostrum", in deren Verlauf mehr als 100.000 Menschen aus dem Mittelmeer gezogen werden. Mit den Augen des Kindes, das er damals war, spürt er den Fragen nach, die ihn seit jeher umtreiben.
Der Dokumentarfilm erzählt eine sehr persönliche Geschichte, um ein globales Phänomen erfahrbar zu machen. Seine Fragen nach Menschlichkeit und gesellschaftlicher Verantwortung in der heutigen Welt führen den Filmemacher zurück zu den Erlebnissen seiner Kindheit und seiner ersten Liebe.

Wo: Werkcafé Leipzig, im Kulturhof Gohlis (Eisenacher Straße 72)