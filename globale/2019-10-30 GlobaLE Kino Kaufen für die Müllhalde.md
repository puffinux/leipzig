---
id: "622612774888635"
title: "GlobaLE Kino: Kaufen für die Müllhalde"
start: 2019-10-30 18:00
end: 2019-10-30 20:30
address: Schaubühne Lindenfels
link: https://www.facebook.com/events/622612774888635/
image: 64540874_3092242934120993_921690738951979008_o.jpg
teaser: "Wann: Mi 30 Oktober 2019 18:00 - 20:30  Kaufen für die Müllhalde  Frankreich,
  Spanien / 2010 / 90 min / Cosima Dannoritzer / dt. und original mit dt."
isCrawled: true
---
Wann: Mi 30 Oktober 2019 18:00 - 20:30

Kaufen für die Müllhalde

Frankreich, Spanien / 2010 / 90 min / Cosima Dannoritzer / dt. und original mit dt. UT / Im Rahmen der MittwochsATTACken von Attac Leipzig. Anschließend Diskussion u.a. mit Gästen von Das Café kaputt, dem Unverpackt-Laden LOCKER & LOSE und anderen. Achtung: Beginn bereits um 18 Uhr! (auf dem Flyer ist die falsche Uhrzeit gedruckt.)

Glühbirnen, Nylonstrümpfe, Drucker, Mobiltelefone - bei den meisten dieser Produkte ist das Abnutzungsdatum in der kapitalistischen Warenproduktion bereits geplant. Die Verbraucher sollen veranlasst werden, lieber einen neuen Artikel zu kaufen, als den defekten reparieren zu lassen. Die bewusste Verkürzung der Lebensdauer eines Industrieerzeugnisses, zum Zweck die Wirtschaft in Schwung zu halten, nennt man "geplante Obsoleszenz". Bereits 1928 schrieb eine Werbezeitschrift unumwunden: "Ein Artikel, der sich nicht abnutzt, ist eine Tragödie fürs Geschäft".

Gestützt auf mehr als drei Jahre andauernde Recherchen erzählt der Dokumentarfilm die Geschichte der geplanten Obsoleszenz. Sie beginnt in den 20er Jahren mit der Schaffung eines Kartells, das die Lebensdauer von Glühbirnen begrenzt, und gewinnt in den 50er Jahren mit der Entstehung der Konsumgesellschaft weiter an Boden.

Heute wollen sich viele Verbraucher nicht mehr mit diesem System abfinden. Als Beispiel für dessen verheerende Umweltfolgen zeigt der Dokumentarfilm die riesigen Elektroschrottdeponien im Umkreis der ghanaischen Hauptstadt Accra. Neben diesem schonungslosen Blick auf die Wegwerfgesellschaft stellt Filmemacherin Cosima Dannoritzer auch die Lösungsansätze von Unternehmern vor, die alternative Produktionsweisen entwickeln. Und Intellektuelle mahnen an, die Technik möge sich auf ihre ursprüngliche Aufgabe zurückbesinnen, auf die dauerhafte Erleichterung des Alltags ohne gleichzeitige Verwüstung des Planeten.

Wo: Schaubühne Lindenfels, Grüner Salon, (Karl-Heine-Str. 50) 