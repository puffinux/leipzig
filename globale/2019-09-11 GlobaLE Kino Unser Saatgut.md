---
id: '430924867754062'
title: 'GlobaLE Kino: Unser Saatgut'
start: '2019-09-11 20:00'
end: '2019-09-11 23:00'
locationName: null
address: Schaubühne Lindenfels
link: 'https://www.facebook.com/events/430924867754062/'
image: 64837633_3091840947494525_8803288114631016448_o.jpg
teaser: 'Wann: Mi 11 September 2019 20:00 - 22:30  Unser Saatgut - Wir ernten, was wir säen  USA / 2016 / 94 min / Jon Betz, Taggart Siegel / original mit dt.'
recurring: null
isCrawled: true
---
Wann: Mi 11 September 2019 20:00 - 22:30

Unser Saatgut - Wir ernten, was wir säen

USA / 2016 / 94 min / Jon Betz, Taggart Siegel / original mit dt. UT / Anschließend Diskussion mit Dr. Sarah Ruth Sippel (Uni Leipzig) . Eintritt frei.

Wenige Dinge auf unserer Erde sind so kostbar und lebensnotwendig wie Saatgut. Verehrt und geschätzt seit Beginn der Menschheit, sind die Samen unserer Kulturpflanzen die Quelle fast allen Lebens. Sie ernähren und heilen uns und liefern Rohstoffe für unseren Alltag. Doch diese wertvollste aller Ressourcen ist bedroht: Mehr als 90 Prozent aller Saatgutsorten sind bereits verschwunden. Biotech-Konzerne wie Syngenta und Bayer/Monsanto kontrollieren mit gentechnisch veränderten Pflanzen längst den globalen Saatgutmarkt. Daher kämpfen immer mehr Menschen aus Landwirtschaft, Wissenschaft und Justiz gemeinsam mit indigenen Saatgutbesitzenden wie David gegen Goliath um die Zukunft der Sortenvielfalt. Der Film ist ein Appell an uns alle: Schützt die ursprüngliche Saatgutvielfalt, sonst ist das reiche Angebot unserer Nahrung bald nur noch schöne Erinnerung!

Wo: Schaubühne Lindenfels, Grüner Salon (Karl-Heine-Straße 50)