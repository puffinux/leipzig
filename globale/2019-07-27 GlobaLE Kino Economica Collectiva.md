---
id: '1015611445298988'
title: 'GlobaLE Kino: Economica Collectiva'
start: '2019-07-27 20:00'
end: '2019-07-27 23:00'
locationName: null
address: Leipzig
link: 'https://www.facebook.com/events/1015611445298988/'
image: 64270034_3091513434193943_6562251007953207296_n.jpg
teaser: 'Wann: Sa 27 Juli 2019 20:00 - 23:00  Economica Collectiva - Europas letzte Revolution  Spanien / 2014 / 66 min / Eulàlia Comas / original mit dt. UT /'
recurring: null
isCrawled: true
---
Wann: Sa 27 Juli 2019 20:00 - 23:00

Economica Collectiva - Europas letzte Revolution

Spanien / 2014 / 66 min / Eulàlia Comas / original mit dt. UT / Anschließend Diskussion mit Vertreter/innen verschiedener selbstorganisierter Projekte und Initiativen. Danach Livemusik (Ska/Punk) und anschließend DJ. Eintritt frei. 
Ort: Siehe Webseite

Der Dokumentarfilm gibt einen tiefen Einblick in ein weitgehend vergessenes, aber nach wie vor inspirierendes Ereignis der jüngeren Geschichte: Die selbstbestimmte Kollektivierung von 80 Prozent der katalanischen Wirtschaft zwischen 1936 und 1939.

Auf den faschistischen Putsch General Francos im Juli 1936 antworteten die Arbeiterinnen und Arbeiter Kataloniens mit einer sozialen Revolution. Die Besitzlosen und Ausgebeuteten, zum Großteil Anarchosyndikalisten, organisierten die Wirtschaft neu, selbstorganisiert und erschufen ein bis dato nie gesehenes soziales Sicherungssystem. Eine der radikalsten sozial-ökonomischen Umbrüche im 20. Jahrhundert, - Europas letzte Revolution.