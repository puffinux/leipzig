---
id: '2210162572408580'
title: 'GlobaLE-Kino: Concerning Violence'
start: '2019-05-16 18:00'
end: '2019-05-16 21:00'
locationName: UNIVERSITÄT LEIPZIG
address: 'Augustusplatz 10, 04109 Leipzig'
link: 'https://www.facebook.com/events/2210162572408580/'
image: 59979989_2988357397842881_2623474803033505792_o.jpg
teaser: 'OpenAir-Kino: "Concerning Violence – Nine Scenes from the Anti-Imperialistic Self-Defence", (Schweden / 2014 / 89 min / Göran Olsson / original mit UT'
recurring: null
isCrawled: true
---
OpenAir-Kino: "Concerning Violence – Nine Scenes from the Anti-Imperialistic Self-Defence", (Schweden / 2014 / 89 min / Göran Olsson / original mit UT.)
Anschließend Diskussion mit Dr. Sarah Ruth Sippel (Institut für Ethnologie Uni Leipzig). 

Ort: Universität Leipzig, Innenhof Campus Augustusplatz 
(bei Regen im Hörsaalgebäude)

Auf der Grundlage von Frantz Fanons berühmtem Buch “Die Verdammten dieser Erde“ erzählt der Film von den Aufständen, die zur Entkolonialisierung Afrikas führen sollten. Der Filmemacher konzentriert sich dabei auf Archivmaterial, das schwedische Dokumentarfilmer und Fernsehjournalisten zwischen 1966 und 1984 in Afrika aufgenommen haben. Aufnahmen von der Befreiungsbewegung in Angola, der Frelimo in Mozambique und dem Unabhängigkeitskampf in Guinea-Bissau werden dokumentarische Bilder von schwedischen Missionaren in Tansania und einem Streik in einer schwedischen Mine in Liberia gegenübergestellt.

Die Musikerin Lauryn Hill erweckt die polarisierenden Texte Fanons zum Leben, die das Bildmaterial strukturieren und kommentieren. Ein Blick auf heutige Konflikte, die entlang der alten Kolonialgrenzen schwelen, zeigt, dass Afrika auch fast 60 Jahre nach Fanons Tod die Folgen der jahrhundertelangen europäischen Raubzüge und Interventionen noch lange nicht überwunden hat.