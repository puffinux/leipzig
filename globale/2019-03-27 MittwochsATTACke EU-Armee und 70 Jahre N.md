---
id: '561653441007909'
title: 'MittwochsATTACke: EU-Armee und 70 Jahre NATO'
start: '2019-03-27 18:00'
end: '2019-03-27 20:00'
locationName: Schaubühne Lindenfels
address: 'Karl-Heine-Str. 50, 04229 Leipzig'
link: 'https://www.facebook.com/events/561653441007909/'
image: 53218977_1590188834417088_6479981699672309760_n.jpg
teaser: 'MittwochsATTACke:  "Europa-Armee und 70 Jahre NATO" - Militärische Aufrüstung in der EU - Referent: Tobias Pflüger  Seit Jahren bemühen sich politisch'
recurring: null
isCrawled: true
---
MittwochsATTACke: 
"Europa-Armee und 70 Jahre NATO"
- Militärische Aufrüstung in der EU -
Referent: Tobias Pflüger

Seit Jahren bemühen sich politische, wirtschaftliche und militärische Eliten Westeuropas darum, die Europäische Union zu einer Großmacht zu entwickeln.
Auf Augenhöhe mit den USA oder China sollen die Geschicke der Welt bestimmt und vor allem die eigenen Interessen durchgesetzt werden. Zu diesem Zweck verfolgt die EU schon lange eine
Geostrategie, die primär auf die Ausweitung des Einflussgebiets und den Aufbau umfassender militär. Fähigkeiten setzt. Lange kam vor allem die Aufrüstung der EU nur schleppend voran, doch gerade
in jüngster Zeit nimmt die 'Militärmacht EUropa' unter französischer und insbesondere deutscher Führung in beängstigendem Tempo Gestalt an. Während die Vision einer europäischen Armee immer
wieder debattiert wird, entsteht sie bereits. Die Militärkooperationen
in der EU waren nie so ausgeprägt wie heute. Mehr als 200 Mrd. Euro geben die EU-Staaten zusammen jährlich fürs Militär aus.
Card, Pesco und der EU Verteidigungsfonds sind beschlossene Sache. Sie sollen das europäische Militärbündnis als Teil der NATO weiter stärken.

MittwochsATTACke am 27.03. 18 Uhr
Schaubühne Lindenfels, Karl-Heine-Str. 50
Referent: Tobias Pflüger (MdB und Attac Mitgliedsorganisation Informationsstelle Militarisierung (IMI) e.V.)
Eine gemeinsame Veranstaltung von attac Leipzig, der AG Frieden von „aufstehen“ und dem Bündnis Leipzig gegen Krieg.

Der Eintritt ist frei.