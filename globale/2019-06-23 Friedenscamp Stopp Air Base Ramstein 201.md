---
id: '2248702745195067'
title: Friedenscamp Stopp Air Base Ramstein 2019
start: '2019-06-23 12:00'
end: '2019-06-30 12:00'
locationName: null
address: 'StoppRamstein Camp, 66879 Steinwenden'
link: 'https://www.facebook.com/events/2248702745195067/'
image: 56666883_2001652956627805_8699562829071515648_n.jpg
teaser: 'Wir freuen uns schon riesig auf das diesjährige Camp! Und wie immer gilt, wer sich noch einbringen möchte, darf sich liebend gern bei uns melden. Denn'
recurring: null
isCrawled: true
---
Wir freuen uns schon riesig auf das diesjährige Camp! Und wie immer gilt, wer sich noch einbringen möchte, darf sich liebend gern bei uns melden. Denn das Friedenscamp entsteht und wächst nur mit und durch all jene Menschen die sich mit einbringen!

Auch hier der Hinweis, dass die Worte “Ticket“ und „Verkauf“ uns nicht so richtig gefallen. Daher möchten wir ausdrücklich darauf hinweisen, dass dies keine kommerzielle Veranstaltung ist! Die Tickets die jetzt erworben werden, helfen uns die Kosten die im Vorfeld für den Aufbau eines solchen Camps anfallen, stemmen zu können. Daher ein riesen Dankeschön an euch alle die ihr Ticket jetzt schon sichern und uns dadurch überhaupt die Möglichkeit geben das Camp aufzubauen. Und ein besonderes Dankeschön an diejenigen die das auch schon die letzten Jahre getan haben.

Ein detailliertes Programm wird in den nächsten Wochen auf der Website veröffentlicht!

Wir freuen uns schon auf euch :)

Eure AG Friedenscamp
#StoppRamstein 2019