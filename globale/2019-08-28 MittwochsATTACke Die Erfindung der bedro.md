---
id: '624478208038925'
title: 'MittwochsATTACke: Die Erfindung der bedrohten Republik'
start: '2019-08-28 18:00'
end: '2019-08-28 20:00'
locationName: null
address: Schaubühne Lindenfels
link: 'https://www.facebook.com/events/624478208038925/'
image: 64509049_3091530800858873_6286459763328811008_o.jpg
teaser: 'Wann: Mi 28 August 2019 18:00 - 20:00  Die Erfindung der bedrohten Republik  Buchvorstellung im Rahmen der MittwochsATTACken / Vortrag und Diskussion'
recurring: null
isCrawled: true
---
Wann: Mi 28 August 2019 18:00 - 20:00

Die Erfindung der bedrohten Republik

Buchvorstellung im Rahmen der MittwochsATTACken / Vortrag und Diskussion mit David Goeßmann. Eintritt frei.

Das Buch „Die Erfindung der bedrohten Republik. Wie Flüchtlinge und Demokratie entsorgt werden“ des Journalisten David Goeßmann, mit einem Vorwort von Konstantin Wecker, steht auf der Bestseller-Liste (zu Flucht, Migration, Rechtsextremismus) der Zeitschrift MIGAZIN. Deutschland und Europa sind in den letzten Jahren politisch nach rechts gerückt. Schuld daran sind nicht Geflüchtete und besorgte Bürger, sondern Politik und Medien, die Schutzsuchende zur Mega-Bedrohung gemacht haben. Das Buch zeigt, wie mit manipulativen Methoden Krisenstimmung erzeugt und Flüchtlingsabwehr im Schnellverfahren als alternativlos durchgewinkt wurde. Ein gefährliches Spiel, das Demokratie aushöhlt und die Gesellschaft für autoritäre Lösungen empfänglich macht.

Wo: Schaubühne Lindenfels, Grüner Salon (Karl-Heine-Straße 50)