---
id: '402019830404785'
title: 'GlobaLE Kino: Letztes Jahr Titanic'
start: '2019-10-03 20:00'
end: '2019-10-03 22:30'
locationName: null
address: die naTo
link: 'https://www.facebook.com/events/402019830404785/'
image: 70840215_3342443189100965_1104302397506715648_o.jpg
teaser: 'Wann: Do 03 Oktober 2019 20:00 - 22:30  Letztes Jahr Titanic  BRD 1991 / 97 min / Andreas Voigt und Sebastian Richter / dt. / Im Anschluss Diskussion'
recurring: null
isCrawled: true
---
Wann: Do 03 Oktober 2019 20:00 - 22:30

Letztes Jahr Titanic

BRD 1991 / 97 min / Andreas Voigt und Sebastian Richter / dt. / Im Anschluss Diskussion u.a. mit Prof. Dr. Cornelius Weiss. Eintritt frei.

Leipzig. Die Umbruchszeit in der DDR zwischen Dezember 1989 und Dezember 1990. Lebengeschichten und Schicksale, Alltagsgeschichten, Menschen in Leipzig. Wie erleben sie dieses Jahr? Wahlkämpfe und Wahlen, die Einführung der D-Mark, die Freiheit des Reisens, die zunehmende wirtschaftliche Unsicherheit – schließlich die Auflösung ihres Landes, die Annexion der DDR und der Ausverkauf einer ganzen Volkswirtschaft.
Wolfgang, der Eisengießer, war zweimal wegen „versuchter Republikflucht“ im Gefängnis. Er will so schnell wie möglich die Westmark, die Wiedervereinigung und selbst in den Westen gehen. Sylvia macht ihre Kneipe zu. Ihr Mann hat schon Arbeit in Bayern. Nach der Währungsunion geht auch sie.
Renate, eine ehemalige Journalistin, spricht über ihre Kontakte zur Staatssicherheit, über Verantwortung und Schuld, gleich zu Beginn des Jahres 1990, zu einer Zeit als das noch kaum jemand tat. Isabell ist vierzehn, Schülerin und „Grufti“. Am Tag des neuen Geldes kommen ihr die Tränen. Für John, den Red-Skin und Hausbesetzer, sind Faschos keine Menschen und Gewalt gegen sie der einzige Weg. Lebensgeschichten und Schicksale in Leipzig – gedreht über ein Jahr hinweg – im letzten Jahr der Deutschen Demokratischen Republik.

Wo: Cinémathèque Leipzig in der naTo, Karl-Liebknecht-Straße 46