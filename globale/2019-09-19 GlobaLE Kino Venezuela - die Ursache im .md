---
id: '476320796474884'
title: 'GlobaLE Kino: Venezuela - die Ursache im Dunkeln'
start: '2019-09-19 20:00'
end: '2019-09-19 23:00'
locationName: null
address: Institut für Kunstpädagogik Leipzig – Studienart
link: 'https://www.facebook.com/events/476320796474884/'
image: 64325822_3091864224158864_5794379987222003712_n.jpg
teaser: 'Wann: Do 19 September 2019 20:00 - 22:30  Venezuela - Die Ursache im Dunkeln  Frankreich, Kolumbien, Venezuela / 2017 / 39 min / Hernando Calvo Ospina'
recurring: null
isCrawled: true
---
Wann: Do 19 September 2019 20:00 - 22:30

Venezuela - Die Ursache im Dunkeln

Frankreich, Kolumbien, Venezuela / 2017 / 39 min / Hernando Calvo Ospina / Original mit dt. UT / Anschließend Diskussion mit Dr. Natalie Benelli (ALBA Suiza) und anderen. Die Veranstaltung findet in Kooperation mit dem Netzwerk „Kritische und Weltoffene Universität" statt. Eintritt frei.

Der Dokumentarfilm von 2017 ist keine vergängliche Arbeit. Er wird aktuell sein, solange die Vereinigten Staaten daran festhalten, die in Venezuela entstehende Bolivarische Revolution beenden zu wollen, um sich das Erdöl und die anderen Bodenschätze des Landes anzueignen.

Diese Dokumentation stützt sich auf Interviews mit venezolanischen Fachleuten, die in einer verständlichen und didaktischen Sprache eine Geschichte erzählen, die in den großen Massenmedien versteckt oder verzerrt wird dargestellt wird..

Wo: Geschwister-Scholl-Haus, HS 301, (Ritterstraße 8-10)