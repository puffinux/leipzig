---
id: '1025378884334527'
title: 'GlobaLE Kino: Push - Für das Grundrecht auf Wohnen'
start: '2019-09-04 20:00'
end: '2019-09-04 23:00'
locationName: Peterskirche Leipzig
address: 'Schletterstraße 5, 04107 Leipzig'
link: 'https://www.facebook.com/events/1025378884334527/'
image: 64415048_3091838680828085_4836158193480499200_o.jpg
teaser: 'Wann: Mi 04 September 2019 20:00 - 23:00  Push - Für das Grundrecht auf Wohnen  Schweden / 2019 / 92 min / Fredrik Gertten / original mit dt. UT / Ans'
recurring: null
isCrawled: true
---
Wann: Mi 04 September 2019 20:00 - 23:00

Push - Für das Grundrecht auf Wohnen

Schweden / 2019 / 92 min / Fredrik Gertten / original mit dt. UT / Anschließend Diskussion mit Aktivist/innen aus Berlin und Leipzig. Eintritt frei.

Überall auf der Welt schnellen die Mietpreise in den Städten in die Höhe. Die Einkommen tun das nicht. Langzeitmieter werden aus ihren Wohnungen herausgedrängt. Selbst Krankenpflegende, Polizisten und Feuerwehrleute können es sich nicht mehr leisten in den Städten zu leben, für deren Grundversorgung sie notwendig sind. Der Film wirft ein Licht auf eine neue Art des anonymen Hausbesitzers, auf unsere immer weniger bewohnbaren Städte und eine eskalierende Krise, die uns alle betrifft. Das ist keine Gentrifizierung mehr: Wohnungen sind Kapital und Orte, um Geld anzulegen.
Der Film folgt Leilani Farha, der UN-Sonderberichterstatterin für das Menschenrecht auf Wohnen, wie sie die Welt bereist, um herauszufinden, wer aus der Stadt gepusht wird und warum. „Ich glaube es gibt einen riesen Unterschied zwischen Wohnen als Handelsware und Gold als Handelsware. Gold ist kein Menschenrecht, Wohnen schon“, sagt Leilani.

Wo: Peterskirche (Schletterstraße 5)