---
id: '2222782351162930'
title: 'GlobaLE-Kino: Earthlings'
start: '2019-08-07 20:00'
end: '2019-08-07 23:00'
locationName: Richard-Wagner-Hain
address: 'Jahnallee / Lützner Straße, 04109 Leipzig'
link: 'https://www.facebook.com/events/2222782351162930/'
image: 64576783_3091518427526777_1965426718831280128_o.jpg
teaser: 'Wann: Mi 07 August 2019 20:00 - 22:30 Earthlings  USA / 2005 / 95 min / Shaun Monson / original mit dt. UT / Eintritt frei.  Der Film thematisiert die'
recurring: null
isCrawled: true
---
Wann: Mi 07 August 2019 20:00 - 22:30
Earthlings

USA / 2005 / 95 min / Shaun Monson / original mit dt. UT / Eintritt frei.

Der Film thematisiert die Abhängigkeit der Menschheit von Tieren und deren Ausbeutung. Er wirft einen kritischen Blick auf den menschliche Konsum von Fleisch und die Nutzhaltung von Tieren. Die Tiere dienen unter anderem als Rohstofflieferanten für Kleidung, der Unterhaltung und als Testobjekt im Rahmen von Tierversuchen.

Mit einer Betrachtung bezüglich Tierzucht, Tierheimen sowie Massentierhaltung, des Leder- und Pelzhandels, der Sport- und Unterhaltungsindustrie sowie der medizinischen und wissenschaftlichen Nutzung zeigt der Film unter Verwendung von versteckten Kameras und heimlich gemachten Bildaufnahmen das tägliche Elend der Tiere.

Wo: Richard-Wagner-Hain am Elsterbecken