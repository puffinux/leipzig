---
id: "902703706736272"
title: "GlobaLE Kino: Der marktgerechte Mensch"
start: 2019-11-08 19:00
end: 2019-11-08 22:00
address: UNIVERSITÄT LEIPZIG
link: https://www.facebook.com/events/902703706736272/
image: 70186672_3333493699995914_7803066222248984576_o.jpg
teaser: "Wann: Fr 08 November 2019 19:00 - 22:00  Vor-Premiere: Der marktgerechte
  Mensch  BRD / 2019 / 90 min / Leslie Franke und Herdolor Lorenz / dt. / Im An"
isCrawled: true
---
Wann: Fr 08 November 2019 19:00 - 22:00

Vor-Premiere: Der marktgerechte Mensch

BRD / 2019 / 90 min / Leslie Franke und Herdolor Lorenz / dt. / Im Anschluss Diskussion mit Gästen. / Eine gemeinsame Veranstaltung der globaLE in Kooperation mit dem studium universale der Universität Leipzig. Eintritt frei.
(Achtung: Beginn um 19 Uhr (auf dem Flyer steht die falsche Startzeit gedruckt!)

Im Kapitalismus wird alles zur Ware, - auch der Mensch selbst. Wenn Effizienz als einziger anzustrebender Wert auf dem freien Markt übrig bleibt, verändert sich die Arbeitswelt in eine pausenlose Konkurrenzmaschinerie. Was geschieht mit Menschen, die zunehmend in allen Bereichen dem Wettbewerbsdiktat unterworfen sind?
Europa ist im Umbruch. Seit dem neuen Jahrtausend und zuletzt nach der „Finanzkrise“ wurden neue Weichen gestellt. Die sogenannte „soziale Marktwirtschaft“, gesellschaftliche Solidarsysteme, über Jahrzehnte erstritten, werden infrage gestellt. Besonders der Arbeitsmarkt und mit ihm die Menschen verändern sich rasant. Noch vor 20 Jahren waren in der Bundesrepublik knapp zwei Drittel der Beschäftigten in einem Vollzeitjob mit Sozialversicherungspflicht. Heute sind es noch 38%. Aktuell arbeitet bereits knapp die Hälfte der Beschäftigten in Unsicherheit. Sie befinden sich in Praktika, wiederholt befristeter Arbeit, in Werkverträgen und Leiharbeit. Sogar die vollkommen ungesicherten Jobs der „Crowdworker“ (Internet-Arbeiter) und der „Gig-Economy“ (Auftragsarbeit per App) breiten sich gerade bei jungen Leuten schnell aus. Diese Jobs funktionieren auf Honorarbasis und unterlaufen den Mindestlohn. Sozialversichern müssen sich alle selbst wie Kleinstunternehmer. Welche Folgen hat das für die Gesellschaft und die Solidargemeinschaft? Sozialwissenschaftler prognostizieren z.B. eine enorme gesellschaftliche Belastung durch hohe Altersarmut.

Welche Folgen hat die Arbeitsmarktderegulierung für die Menschen? Wer auf dem Arbeitsmarkt und in der Gesellschaft mitspielen will, muss sich von klein auf auf Flexibilität und Wettbewerb einstellen. Wie verändert diese Unbeständigkeit und Konkurrenz uns Menschen selbst und unsere sozialen Beziehungen zu anderen? Wie gehen junge Erwachsene mit den veränderten Bedingungen um? Schafft das vielleicht sogar neue Freiheiten? Der Film fragt nach, ob der Mensch von Natur aus auf Egoismus und Konkurrenz gepolt ist, oder ob nicht eher die Fähigkeit zur Zusammenarbeit seine Entwicklung gefördert hat. Eine junge Protagonisten-Familie führt uns durch die verschiedenen Ebenen von befristeter Beschäftigung, Leih- und Werkverträgen und nicht zuletzt zu den Familienproblemen, die aus dieser Situation erwachsen.

Wie konnte es zu dieser Entwicklung kommen? Welche politischen Entscheidungen waren bestimmend, als seit den 1980er Jahren zuerst in den USA und dann auch in Großbritannien der schlanke Staat, die Beseitigung aller Schranken des Marktes und der entgrenzte globale Wettbewerb propagiert wurden? In der Bundesrepublik etablierte sich diese Politik erstmals mit der Rot-Grünen Regierung Schröder/Fischer. Mit einer Senkung der Unternehmenssteuern und der Deregulierung des Arbeitsmarktes schaffte sie es, deutschen Konzernen nachhaltig Kostenvorteile zu verschaffen. Die Realeinkommen sanken allerdings infolge dessen zwischen den Jahren 2000 und 2010 im Mittel um 4,2 Prozent, im unteren Lohnbereich sogar um bis zu 23,1 Prozent.

„Wir haben geliefert“ sagen die verantwortlichen Politiker in Griechenland, Italien Spanien und Portugal. Auch sie haben nach der Finanzkrise gezwungenermaßen den Arbeitsmarkt dereguliert. Die Arbeitslosigkeit ist dadurch nirgendwo gesunken. In der von der Bundesrepublik und Frankreich dominierten EU herrscht das Kapital und die Macht der Banken und Konzerne. Fast alle Menschen in Europa verlieren an sozialer Sicherheit und werden in einen Konkurrenzkampf geschickt, der zunehmend alle Lebensbereiche umfasst und viele ins Abseits drängt. Der Film fragt auch nach den gesellschaftlichen Kosten für Behandlung und Reintegration derjenigen, die es nicht schaffen und angeblich „selbst schuld“ sind. Der Film „Der marktgerechte Mensch“ diskutiert schließlich verschiedene Versuche, dieser Entwicklung entgegenzutreten, sie sozial abzufedern und ihr auch individuell zu entgehen. Es ist ein Film, der Verständnis schaffen will und Mut macht, sich einzumischen.

Mehr Infos: http://www.marketable-people.org/index.php/de/

Wo: Universität Leipzig, Audimax, (Augustusplatz 10)