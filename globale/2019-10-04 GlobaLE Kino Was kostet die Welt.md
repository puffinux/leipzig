---
id: '764961150629174'
title: 'GlobaLE Kino: Was kostet die Welt'
start: '2019-10-04 20:00'
end: '2019-10-04 23:00'
locationName: Moritzbastei
address: 'Kurt-Masur-Platz 1, 04109 Leipzig'
link: 'https://www.facebook.com/events/764961150629174/'
image: 70489156_3334367766575174_6981701181982638080_o.jpg
teaser: 'Wann: Fr 04 Oktober 2019 20:00 - 23:00 Sonderveranstaltung: "Was kostet die Welt?" (BRD / 2018 / 90 min / Bettina Borgfeld / dt. und original mit dt.'
recurring: null
isCrawled: true
---
Wann: Fr 04 Oktober 2019 20:00 - 23:00
Sonderveranstaltung: "Was kostet die Welt?" (BRD / 2018 / 90 min / Bettina Borgfeld / dt. und original mit dt. UT.) Anschließend Diskussion u.a. mit Bettina Borgfeld. Die Veranstaltung steht nicht im gedruckten Programm. 
Eine gemeinsame Veranstaltung der Moritzbastei und der globaLE. Eintritt ist frei.

Die kleine Insel Sark war bis zum Jahr 2008 der letzte feudalistisch regierte Staat Europas. Nur 600 Menschen leben dort, inmitten von grünen Auen, windschiefen Bäumen und sehr vielen Schafen - aber mit einem eigenem Parlament und eigenen Gesetzen. Direkt der Krone unterstellt, regierten Fischer, Gärtner und Milchbauern die Insel über die Jahrhunderte nach ihren Vorstellungen von Grundbesitz, Steuern und Recht. 
Bis zwei britische Milliardäre beginnen, gegen die Gesetze der Insel juristisch vorzugehen und Stück für Stück der Insel aufzukaufen. Das gute Recht der einen wird für Andere zum Albtraum. Und die idyllische Insel wird zum Schauplatz eines absurden Konfliktes um Demokratie, Meinungsfreiheit und gesellschaftliche Verantwortung.
In Zeiten von Fake News, Panama Papers und Turbokapitalismus blickt der Film auf eine kleine Gemeinschaft, die sich gegen einen milliardenschweren Investor stellt und einen hohen Preis dafür zu zahlen hat.  

Ort: Moritzbastei (Ratstonne), Kurt-Masur-Platz 1 (bzw. Universitätsstr. 9)
