---
id: '397633244177873'
title: 'GlobaLE Kino: Everything must fall'
start: '2019-10-02 20:00'
end: '2019-10-02 23:00'
locationName: Ost-Passage Theater
address: 'Konradstr. 27, 04315 Leipzig'
link: 'https://www.facebook.com/events/397633244177873/'
image: 64725317_3092172997461320_2956033678381154304_o.jpg
teaser: 'Wann: Mi 02 Oktober 2019 20:00 - 22:30  Everything must fall  Südafrika / 2018 / 85 min / Rehad Desai / original mit UT / Im Anschluss Diskussion. Ein'
recurring: null
isCrawled: true
---
Wann: Mi 02 Oktober 2019 20:00 - 22:30

Everything must fall

Südafrika / 2018 / 85 min / Rehad Desai / original mit UT / Im Anschluss Diskussion. Eintritt frei.

Der Film wirft einen Blick auf die #FeesMustFall Studierendenbewegung, die 2015 als Protest gegen die Erhöhung der Studiengebühren in die politische Landschaft Südafrikas eindrang und zu den militantesten nationalen Aufständen seit den ersten demokratischen Wahlen 1994 führte.

Im Zentrum des Films steht ein Generationenkonflikt, der uns mit einem wichtigen zeitgenössischen Diskurs über die Konzeptualisierung der Hochschulbildung als öffentliches Gut verbindet. Im Laufe der Proteste gab es drei Todesfälle und über 800 Festnahmen. Indem dramatische Entfaltungshandlungen mit einer Erzählung mehrerer Protagonistinnen und Protagonisten kombiniert werden, liegt ein Großteil des Dramas in den internen Kämpfen, die die Aktivistinnen und Aktivisten um Einfluss in der Führung der Bewegung führen. Durch den Film zieht sich ein Impuls der darauf hindeutet, dass die jungen Menschen an einem Wendepunkt angelangt sind und erst dann zurückkehren, wenn sie die Art sozialer Transformation erreichen, auf die frühere Generationen lange verzichtet hatten.

Wo: Ost-Passage Theater, (Konradstr. 27) 