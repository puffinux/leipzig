---
id: '345484806141525'
title: 'GlobaLE Kino: Gundermann'
start: '2019-10-01 18:30'
end: '2019-10-01 21:00'
locationName: Jugend- und Altenhilfeverein e.V.
address: 'Goldsternstraße 9, 04329 Leipzig'
link: 'https://www.facebook.com/events/345484806141525/'
image: 64533542_3092172310794722_8098329122016591872_o.jpg
teaser: 'Wann: Di 01 Oktober 2019 18:30 - 21:00  Gundermann  BRD / Spielfilm / 2018 / 127 min / Andreas Dresen / dt. / Im Anschluss Diskussion. Achtung: Der Fi'
recurring: null
isCrawled: true
---
Wann: Di 01 Oktober 2019 18:30 - 21:00

Gundermann

BRD / Spielfilm / 2018 / 127 min / Andreas Dresen / dt. / Im Anschluss Diskussion. Achtung: Der Film beginnt bereits 18:30 Uhr und nicht wie üblich 20 Uhr. Eintritt frei.

Der Film wirft einen Blick auf das Leben von Gerhard Gundermann, einem der prägendsten Künstler im Osten. Er zeigt ausgewählte Episoden aus dem Leben des Liedermachers und Baggerfahrers.
Gundermanns Leben und seine Umwelt sind geprägt von Widersprüchen: Seine Arbeit reißt die Erde auf, gleichzeitig beschreibt und besingt er die Schönheiten der Natur. Er gewinnt Inspirationen für seine Lieder und Songs, während er auf dem Bagger sitzt und Braunkohle abbaut. Als überzeugter Kommunist stößt er mit seiner Direktheit und Eigenwilligkeit an Grenzen. Der Film blickt noch einmal auf ein verschwundenes Land und die Hoffnungen die ‘89 kurz aufflammten und in der Konterrevolution erstickten.

Wo: Jugend- und Altenhilfeverein Paunsdorf (Goldsternstraße 9) 