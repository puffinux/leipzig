---
id: '363374724318521'
title: 'GlobaLE Kino: #Female Pleasure'
start: '2019-07-24 20:00'
end: '2019-07-24 23:00'
locationName: Grassimuseum
address: Leipzig
link: 'https://www.facebook.com/events/363374724318521/'
image: 64323586_3091509014194385_6798765614174306304_o.jpg
teaser: 'Wann: Mi 24 Juli 2019 20:00 - 22:30 Ort: Open Air Kino auf der Wiese im Grassimuseum (Johannisplatz 5-11).  #Female Pleasure,  BRD, Schweiz / 2018 / 9'
recurring: null
isCrawled: true
---
Wann: Mi 24 Juli 2019 20:00 - 22:30
Ort: Open Air Kino auf der Wiese im Grassimuseum (Johannisplatz 5-11). 
#Female Pleasure, 
BRD, Schweiz / 2018 / 97 min / Barbara Miller / original mit dt. UT / Aufführung im Rahmen der Ausstellung Women to Go, welche noch bis zum 11.08. im Grassimuseum zu sehen ist. Anschließend Gespräch u.a. mit Hannah Maneck (Femstreikbündnis Leipzig) und Dr. Anna Artwinska (Zentrum für Frauen- und Geschlechterforschung an der Universität Leipzig) Der Eintritt ist frei.

Fünf mutige, kluge und selbstbestimmte Frauen stehen im Zentrum von Barbara Millers Dokumentarfilm „#Female Pleasure“ . Sie brechen das Tabu des Schweigens und der Scham, das ihnen die Gesellschaft oder ihre religiösen Gemeinschaften mit ihren archaisch-patriarchalen Strukturen auferlegen. Mit einer unfassbaren positiven Energie und aller Kraft setzen sich Deborah Feldman, Leyla Hussein, Rokudenashiko, Doris Wagner und Vithika Yadav für sexuelle Aufklärung und Selbstbestimmung aller Frauen ein, hinweg über jedwede gesellschaftliche sowie religiöse Normen und Schranken. Dafür zahlen sie einen hohen Preis – sie werden öffentlich diffamiert, verfolgt und bedroht, von ihrem ehemaligen Umfeld werden sie verstoßen und von Religionsführern und fanatischen Gläubigen sogar mit dem Tod bedroht.

„#Female Pleasure“ ist ein Film, der schildert, wie universell und alle kulturellen und religiösen Grenzen überschreitend die Mechanismen sind, die die Situation der Frau – egal in welcher Gesellschaftsform – bis heute bestimmen. Gleichzeitig zeigen die fünf Protagonistinnen, wie man mit Mut, Kraft und Lebensfreude jede Struktur verändern kann. Der Film ist ein Plädoyer für das Recht auf Selbstbestimmung und gegen die Dämonisierung der weiblichen Lust durch Religion und gesellschaftliche Restriktionen.