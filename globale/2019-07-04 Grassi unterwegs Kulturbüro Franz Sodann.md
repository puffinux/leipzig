---
id: '2222777741367517'
title: 'Grassi unterwegs: Kulturbüro Franz Sodann'
start: '2019-07-04 19:00'
end: '2019-07-04 21:00'
locationName: Grassi Museum für Völkerkunde zu Leipzig
address: 'Johannisplatz 5-11, 04103 Leipzig'
link: 'https://www.facebook.com/events/2222777741367517/'
image: 65275625_709522236150861_3113447721587965952_n.jpg
teaser: 'Gespräch und Film: Rückgabe von geraubten Gebeinen aus Sachsen nach Hawaii  Im Herbst 2017 fand die erste Rückgabe von menschlichen Gebeinen aus Sachs'
recurring: null
isCrawled: true
---
Gespräch und Film: Rückgabe von geraubten Gebeinen aus Sachsen nach Hawaii

Im Herbst 2017 fand die erste Rückgabe von menschlichen Gebeinen aus Sachsen statt. Birgit Scheps-Bretschneider arbeitet in der Provenienzforschung der Völkerkundemuseen in Sachsen und war maßgeblich an dieser Restitution beteiligt. Sie erzählt über den langen Prozess der Rückführung nach Hawaii.

Ein dabei entstandener Kurzfilm wird gezeigt. Gemeinsam mit Leipzig Postkolonial und GlobaLE Leipzig.

Treff: Treffpunkt: Kulturbüro Franz Sodann, Mariannenstraße 101, 04315 Leipzig 