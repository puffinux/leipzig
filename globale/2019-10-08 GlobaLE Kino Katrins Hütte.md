---
id: '901734033500327'
title: 'GlobaLE Kino: Katrins Hütte'
start: '2019-10-08 20:00'
end: '2019-10-08 22:30'
locationName: Neues Schauspiel Leipzig
address: 'Lützner Str. 29, 04177 Leipzig'
link: 'https://www.facebook.com/events/901734033500327/'
image: 64345047_3092174814127805_1144580068232658944_o.jpg
teaser: 'Wann: Di 08 Oktober 2019 20:00 - 22:30  Katrins Hütte  BRD / 1991 / 91 min / Joachim Tschirner / dt. / Im Anschluss Diskussion.  Am Beispiel der Abwic'
recurring: null
isCrawled: true
---
Wann: Di 08 Oktober 2019 20:00 - 22:30

Katrins Hütte

BRD / 1991 / 91 min / Joachim Tschirner / dt. / Im Anschluss Diskussion.

Am Beispiel der Abwicklung der thüringischen Maxhütte begleitet der Film die Blockwalzerin und Volkskammerabgeordnete Katrin Hensel in den Jahren des Endes der DDR. Die Langzeitdokumentation, welche im März 1986 begann, begleitete Katrin bis in das Jahr 1991 hinein, so werden Sorgen, Ängste, Erfolge, Hoffnungen und Enttäuschungen bei ihrer beruflichen und privaten Entwicklung erkennbar. Besonders problematisch war für sie und ihren Mann Faiko die Zeit um die politische „Wende“ in der DDR und ihrer Folgen für die Vollbeschäftigung. Zunehmend sichtbar wird der Wandel einer dynamischen Frau im ewigen Kampf zwischen eigenen Ansprüchen, Aus- und Weiterbildung, Privatleben, Volkskammertätigkeit, politischem Engagement, und dem ernüchternden Erkennen der Tatsachen beim Kampf um den Erhalt des Arbeitsplatzes, dem offenem Mobbing der Kollegen und dem Versagen beim Politikwechsel nach der Annexion der DDR.

Wo: Neues Schauspiel Leipzig, Lützner Straße 29