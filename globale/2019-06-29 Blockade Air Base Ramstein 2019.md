---
id: '783118912082354'
title: Blockade Air Base Ramstein 2019
start: '2019-06-29 11:00'
end: '2019-06-29 18:00'
locationName: null
address: 'Airbase Ramstein, 66877 Ramstein-Miesenbach'
link: 'https://www.facebook.com/events/783118912082354/'
image: 61489912_2079844425475324_288092791169351680_n.jpg
teaser: 'Wir blockieren die Air Base Ramstein!  Aufruf zu Aktionen des zivilen Ungehorsams:  Ramstein ist die logistische europäische Drehscheibe für Kriegsein'
recurring: null
isCrawled: true
---
Wir blockieren die Air Base Ramstein!

Aufruf zu Aktionen des zivilen Ungehorsams: 
Ramstein ist die logistische europäische Drehscheibe für Kriegseinsätze. Die Air Base Ramstein ist das Auswertungszentrum und die Relaisstation für weltweite  Drohneneinsätze, z.B. in Afghanistan, oder im Jemen. Satellitendaten der Kampfdrohnen werden in Ramstein empfangen und über Glasfaserleitungen an die steuernden Drohnenpiloten in den USA übertragen.  Von dort werden dann gezielte Tötungen durch Lenkraketen der Drohnen per Joystick ausgelöst. Das bedeutet:

- Von deutschem Boden aus wird der völkerrechtswidrige Drohnenkrieg koordiniert und werden Drohnen ins Ziel gelenkt.

- Drohnen töten aus dem Hinterhalt, ohne Kriegserklärung und ohne Gerichtsurteil – auch Zivilpersonen wie Frauen und Kinder. Das ist ethisch verwerflich und untergräbt das Völkerrecht.

- Wir wehren uns nicht nur gegen die US-Drohnen, sondern auch gegen den Beschluss der Bundesregierung vom Juni 2018, die Kampfdrohne „HeronTP“ für die Bundeswehr zu beschaffen. Mit der geplanten „Eurodrohne“ ab 2025 sollen sogenannte „Fähigkeitslücken“ geschlossen werden.  Damit wird die EU-Militarisierung  im Rahmen von PESCO, der ständigen Zusammenarbeit der EU-Staaten, vorangetrieben.

- Für die Bevölkerung in den betroffenen Regionen bedeutet die Relaisstation, in ständiger Bedrohung und mit Fluglärmbelastungen zu leben sowie mit großen Umweltbelastungen am Boden und im Grundwasser konfrontiert zu werden.

- Wir sagen Nein!

 Wir lehnen die zerstörerische Gewalt des Krieges nicht nur ab, wir widersetzen uns ihr! 
Wir machen das Unrecht des Drohnenkriegs, das von der Air Base Ramstein ausgeht, öffentlich. Mehr noch: 
Wir widersetzen uns konkret. Wir verweigern der Politik und dem kriegerischen System unseren Gehorsam. Wir stellen uns dem Drohnenkrieg und der Kriegsvorbereitung, wie sie in der Air Base Ramstein betrieben werden, in den Weg. Wir nehmen uns das Recht auf Widerstand und praktizieren zivilen Ungehorsam, um friedliche Wege in die Zukunft zu ermöglichen. Wir stehen persönlich, couragiert und solidarisch für die Alternative:

Frieden heißt: Abrüstung, sozial-ökologische Konversion der Region und zivile Konfliktbearbeitung!

Frieden ist für uns nicht nur fernes Ziel und Utopie. Wir stehen für Völkerverständigung und leben den Gedanken einer solidarischen Gesellschaft, in der alle Menschen dazugehören und niemand auf Grund seines Geschlechts, seiner Herkunft, seiner Nationalität, seiner Religion oder seiner sexuellen Identität ausgegrenzt wird.

Wir widersetzen uns – gewaltfrei, machtvoll und konsequent!

Wo Recht zu Unrecht wird, wird Widerstand zur Pflicht. Wir nehmen die Friedensgebote der UN-Charta und des Grundgesetzes ernst. Wir praktizieren mit unserer Blockade zivilen Widerstand und bauen Gegenmacht auf – gegen den Wahnsinn des täglichen Drohnenkriegs und das System der Drohnenkriegführung. Macht mit und nehmt an der Blockadevorbereitung am 26. und 27. Juni teil!

Wir blockieren die Air Base Ramstein am 29.06.2019.