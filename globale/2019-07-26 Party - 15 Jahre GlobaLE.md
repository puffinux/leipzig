---
id: '675026922945087'
title: Party - 15 Jahre GlobaLE
start: '2019-07-26 23:00'
end: '2019-07-27 04:00'
locationName: null
address: 'Caracan im Auenwald, Neue Linie 20'
link: 'https://www.facebook.com/events/675026922945087/'
image: 64783947_3086545594690727_5469775984012034048_n.jpg
teaser: '15 Jahre gibt es die globaLE mittlerweile in Leipzig. Wir finden das ist ein guter Grund für ne Feier! Deswegen gibt''s am Freitag, 26.7. nach dem kolu'
recurring: null
isCrawled: true
---
15 Jahre gibt es die globaLE mittlerweile in Leipzig. Wir finden das ist ein guter Grund für ne Feier!
Deswegen gibt's am Freitag, 26.7. nach dem kolumbianischen Spielfilm "Die Strategie der 🐌" ne Party. :) 

Vor dem Film (ab 20 Uhr) spielt die Leipziger Band "Hans Hadert".

Im Anschluss an den Film gibt's Electronica, Tropical, Latin, Afro Rhythm vom Tanzflächenaufleger.

Es gibt ne Bar mit leckeren Getränken vor Ort.

Eintritt ist frei. Also schwingt euch her. :)
