---
id: "2165613583561583"
title: "GlobaLE Kino: Eldorado - The struggle for Skouries"
start: 2019-10-24 20:00
end: 2019-10-24 22:30
address: Ritterstr 14 Leipzig
link: https://www.facebook.com/events/2165613583561583/
image: 75291052_3458366497508633_8240000443636252672_n.jpg
teaser: "Wann: Do 24 Oktober 2019 20:00 - 22:30  Eldorado - The struggle for
  Skouries  Griechenland / 2018 / 56 min / Wasil Schauseil, Leo Helbich /
  original m"
isCrawled: true
---
Wann: Do 24 Oktober 2019 20:00 - 22:30

Eldorado - The struggle for Skouries

Griechenland / 2018 / 56 min / Wasil Schauseil, Leo Helbich / original mit dt. UT / Im Anschluss Diskussion u.a. mit Wasil Schauseil. Die Veranstaltung findet in Kooperation mit dem Netzwerk „Kritische und Weltoffene Universität" statt. Eintritt frei.

In den Bergen von Chalkidiki im Norden Griechenlands tobt seit mehr als dreißig Jahren ein harter Kampf gegen die Bergbauindustrie und deren zerstörerische Folgen für die Natur. Mitten im gut 317 Quadratkilometer großen Bergbaukomplex befinden sich drei Abbauorte: die Gold- und Kupfermine Skouries, die ältere Silber-, Blei- und Zinkmine Stratoni, und Olimpiada, eine Gold-, Blei- und Zinkmine mit einem angeschlossenen Weiterverarbeitungswerk. Der Film dokumentiert den Kampf der Bevölkerung gegen Umweltzerstörung und Menschenrechtsverletzungen bei Europas größtem Bergbauprojekt.

Wo: Geschwister-Scholl-Haus, HS 301, (Ritterstraße 8-10)