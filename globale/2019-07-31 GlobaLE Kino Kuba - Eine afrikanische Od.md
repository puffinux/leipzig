---
id: '636066386870888'
title: 'GlobaLE Kino: Kuba - Eine afrikanische Odyssee'
start: '2019-07-31 20:00'
end: '2019-07-31 23:00'
locationName: null
address: 'Clara-Zetkin-Park, 04178 Leipzig'
link: 'https://www.facebook.com/events/636066386870888/'
image: 64280033_3091514730860480_4773074594305146880_o.jpg
teaser: 'Wann: Mi 31 Juli 2019 20:00 - 22:30  Kuba - eine afrikanische Odyssee  Frankreich / 2006 / 120 min / El-Tahri, Jihan / dt. und original mit dt. UT /'
recurring: null
isCrawled: true
---
Wann: Mi 31 Juli 2019 20:00 - 22:30

Kuba - eine afrikanische Odyssee

Frankreich / 2006 / 120 min / El-Tahri, Jihan / dt. und original mit dt. UT / 

Im Anschluss Diskussion mit dem Journalisten und Buchautoren Volker Hermsdorf. Veranstaltung in Kooperation mit Cuba Sí. Eintritt frei.

Während es den USA und der Sowjetunion im Kalten Krieg auch in Afrika vor allem um politische Kontrolle und den Zugriff auf strategisch wichtige Rohstoffe ging, stellte Fidel Castro den antikolonialen Bewegungen Afrikas kubanische Soldaten, Ärzte, Techniker und Ausbilder zur Seite, ohne dafür Gegenleistungen zu verlangen: „Uns ging es nicht um Diamanten, sondern um internationale Solidarität.“
Die Dokumentation vermittelt mit bislang kaum bekannten Archivaufnahmen einen faszinierenden Einblick in die Geschichte des afrikanischen Unabhängigkeitskampfes (von 1961 bis 1989). Der Filmemacherin ist es darüber hinaus gelungen, Zeitzeugen aus aller Welt vor die Kamera zu holen, die den Kalten Krieg in Afrika kommandiert und ausgefochten haben. Dazu gehören Agenten der US-amerikanischen und sowjetischen Geheimdienste ebenso wie südafrikanische Militärs und Vertreter afrikanischer Befreiungsbewegungen. Und neben kubanischen Veteranen, die als Ärzte und Brigadisten ihr Leben riskierten, begründet auch Fidel Castro noch einmal persönlich, warum sich seine revolutionäre Regierung in Afrika stärker engagiert hat, als irgendwo anders in der Welt.

Wo: Clara-Zetkin-Park (Wiese zwischen Glashaus und Sachsenbrücke)