---
id: '2239200883058183'
title: 'GlobaLE Kino: Die Strategie der krummen Gurken'
start: '2019-08-23 20:00'
end: '2019-08-23 22:30'
locationName: null
address: 'SoLaWi Allerlei, Floraweg'
link: 'https://www.facebook.com/events/2239200883058183/'
image: 64376543_3091528960859057_1009731581335568384_o.jpg
teaser: 'Wann: Fr 23 August 2019 20:00 - 22:30  Die Strategie der krummen Gurken  BRD / 2013 / 64 min / Sylvain Darou und Luciano Ibarra / dt. / Anschließend D'
recurring: null
isCrawled: true
---
Wann: Fr 23 August 2019 20:00 - 22:30

Die Strategie der krummen Gurken

BRD / 2013 / 64 min / Sylvain Darou und Luciano Ibarra / dt. / Anschließend Diskussion mit Aktiven vom Projekt. Eintritt frei.

Die GartenCoop Freiburg setzt ein erfolgreiches Modell solidarischer Landwirtschaft um. Rund 260 Mitglieder teilen sich die Verantwortung für einen landwirtschaftlichen Betrieb in Stadtnähe und tragen gemeinsam die Kosten und Risiken der Landwirtschaft. Die gesamte Ernte – ob gut oder schlecht, krumm oder gerade – wird auf alle Mitglieder verteilt. Ein konsequenter ökologischer Anbau, Saisonalität, 100% samenfeste Sorten, kurze Wege, solidarische Ökonomie, kollektives Eigentum, Bildung, sowie mit anpacken in der Landwirtschaft sind nur einige der vielen Merkmale des Projekts.
Der Dokumentarfilm gibt Einblick in die Motivationen und das Innenleben der Kooperative. Er zeigt Menschen, die in Zeiten ökonomischer und ökologischer Krise der Macht der Agrarindustrie etwas entgegensetzen: Die Strategie der krummen Gurken.

Wo: SoLaWi Allerlei Leipzig (Floraweg, Dölitz-Dösen)