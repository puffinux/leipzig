---
id: "388332711788688"
title: "GlobaLE Kino: Jakarta Disorder"
start: 2019-10-23 20:00
end: 2019-10-23 23:00
address: Ost-Passage Theater
link: https://www.facebook.com/events/388332711788688/
image: 64231209_3092240404121246_2792527881194962944_o.jpg
teaser: "Wann: Mi 23 Oktober 2019 20:00 - 22:30  Jakarta Disorder  BRD, Indonesien /
  2013 / 88 min / Ascan Breuer / original mit dt. UT / Im Anschluss Diskussi"
isCrawled: true
---
Wann: Mi 23 Oktober 2019 20:00 - 22:30

Jakarta Disorder

BRD, Indonesien / 2013 / 88 min / Ascan Breuer / original mit dt. UT / Im Anschluss Diskussion. Eintritt frei.

Oma Dela lebt im Slum. Wardah ist dagegen eine Intellektuelle und politische Hochaktivistin. Zusammen versuchen die beiden die rechtlosen Armen Jakartas gegen die vielen Zwangsräumungen zu mobilisieren. Viele Bewohnerinnen und Bewohner der Metropole leben in wilden Siedlungen, so genannten „Kampungs“, die in den vergangenen Jahrzehnten auf nicht-gewidmeten Flächen entstanden sind. Diese Siedlungen sollen nun den Großprojekten weichen, die wie Pilze aus dem Boden schießen.

Es ist Wahlkampf: In Indonesien wird der Präsident gewählt. Oma Dela und Wardah wollen sich aber mit bloßen Wahlen nicht zufrieden geben. Statt sich wie üblich von den elitären Bewerbern mit Almosen abspeisen zu lassen, wollen sie lieber echte Zugeständnisse an das Wahlvolk sehen: Ihr Forderungskatalog an die Kandidaten umfasst fünf Punkte, die ebenso einfach wie grundsätzlich sind: Arbeit, Wohnen und Bildung für alle, soziale Krankenversicherung sowie formale Anerkennung der informellen Wirtschaft. Sie wollen anderthalb Millionen Menschen dafür gewinnen, einen Vertrag zu unterschreiben. Damit versprechen sie, jenen Kandidaten zu wählen, der die fünf Forderungen wirklich umsetzen mag. Für Oma Dela und Wardah beginnt damit ein Kampf von Haustür zu Haustür. Mit voller Energie stürzen sie sich in dieses rasante Abenteuer mit ungewissem Ausgang: Kann die Macht der Vielen tatsächlich die Macht einer kleinen Elite brechen oder bleibt „echte Demokratie“ ein Ideal?

Wo: Ost-Passage Theater, (Konradstr. 27)