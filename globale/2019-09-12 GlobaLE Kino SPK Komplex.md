---
id: '451228825691893'
title: 'GlobaLE Kino: SPK Komplex'
start: '2019-09-12 20:00'
end: '2019-09-12 23:00'
locationName: null
address: Schaubühne Lindenfels
link: 'https://www.facebook.com/events/451228825691893/'
image: 64558855_3091835937495026_6176498816615710720_o.jpg
teaser: 'Wann: Do 12 September 2019 20:00 - 22:30  SPK Komplex  BRD / 2018 / 111 min / Gerd Kroske / dt. / Im Anschluss Diskussion. Eintritt frei.  1970 begrün'
recurring: null
isCrawled: true
---
Wann: Do 12 September 2019 20:00 - 22:30

SPK Komplex

BRD / 2018 / 111 min / Gerd Kroske / dt. / Im Anschluss Diskussion. Eintritt frei.

1970 begründete der Arzt Wolfgang Huber in Heidelberg gemeinsam mit Patienten das antipsychiatrische »Sozialistische Patientenkollektiv« (SPK). Umstrittene Therapiemethoden, politische Forderungen und der massive Zulauf von Patienten, die der üblichen »Verwahr–Psychiatrie« tief misstrauten, führten zum Konflikt mit der Universität Heidelberg und der Landesregierung, der sich bald zuspitzte und in die Radikalisierung des SPK mündete. Das gruppentherapeutische Experiment endet schließlich mit Verhaftungen, Gefängnis und der Aberkennung von Hubers Approbation.

Die SPK–Gerichtsprozesse wirken heute wie eine Vorwegnahme der Stammheim-Prozesse – mit Mitteln zum Ausschluss der Rechtsanwälte, Totalverweigerung der Angeklagten und empfindlichen Strafen für das Ehepaar Huber. Dabei stand die Härte der Strafverfolgung in kaum einem Verhältnis zu den eigentlichen Taten. Der Ruf, die RAF unterstützt zu haben und letztendlich in deren Terror aufgegangen zu sein, haftet dem SPK seither an. Er überlagert, worum es damals eigentlich ging: um die Rechte von Psychiatriepatientinnen und -patienten, Widerstand und um Selbstermächtigung. Um Fragen also, die noch immer Aktualität besitzen. Der Film widmet sich der unerzählten Geschichte des „Deutschen Vorherbstes“ und seinen Folgen bis ins Heute. Eine Geschichte vom Irresein und Irrewerden, ihrer öffentlichen Wahrnehmung und (nicht) zwangsläufigen Gewalt.

Wo: Schaubühne Lindenfels, Grüner Salon (Karl-Heine-Straße 50)