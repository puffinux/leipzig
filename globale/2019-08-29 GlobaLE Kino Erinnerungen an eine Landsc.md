---
id: '676208562807611'
title: 'GlobaLE Kino: Erinnerungen an eine Landschaft'
start: '2019-08-29 20:00'
end: '2019-08-29 23:00'
locationName: Kulkwitzer See
address: 'Seestraße 1, 04207 Leipzig'
link: 'https://www.facebook.com/events/676208562807611/'
image: 64419195_3091531804192106_8165620569371312128_n.jpg
teaser: 'Wann: Do 29 August 2019 20:00 - 22:30  Erinnerungen an eine Landschaft - für Manuela  DDR / 1983 / 80 min / Kurt Tetzlaff / dt. / Anschließend Diskuss'
recurring: null
isCrawled: true
---
Wann: Do 29 August 2019 20:00 - 22:30

Erinnerungen an eine Landschaft - für Manuela

DDR / 1983 / 80 min / Kurt Tetzlaff / dt. / Anschließend Diskussion mit Gästen. Die Veranstaltung findet in Kooperation mit dem Kommhaus e.V. statt und ist Teil des Grünauer Kultursommers. Eintritt frei.

Im Süden Leipzigs veränderte sich eine ganze Landschaft. Flüsse und Bahnlinien wurden verlegt, ganze Dörfer verschwanden - wichen neuen Kohletagebauten.
Die DDR erzielte 1989 die bis heute weltweit unerreichte jährliche Förderquote von 300 Mio. Tonnen Kohle! Das hatte enorme Auswirkungen auch auf die Kulturlandschaft im Süden von Leipzig. Waren bis Mitte der 1970er Jahre „nur“ kleinere Ortschaften dem Bergbau zum Opfer gefallen, schlug 1977-1981 für Magdeborn (Tagebau Espenhain) und für Eythra/Bösdorf 1984-1986 (Tagebau Zwenkau) mit ihren jeweils rund 3.000 Einwohnern, das letzte Stündlein. Im Zuge dieser Entwicklung entstand ein, gemessen an den damaligen Umständen, bemerkenswerter Dokumentarfilm. Der Regisseur Kurt Tetzlaff begleitete über die Jahre hinweg den Untergang der beiden Orte. Er zeigt den schmerzlichen Abschied der Menschen, von den ihnen vertrauten Orten, die aufhören werden zu existieren. Der Film ist ein Dokument gesellschaftlicher Veränderung, das dem letztgeborenen Kind in Magdeborn, Manuela, als Erinnerung an die Landschaft ihrer Vorfahren gewidmet ist.

Wo: Kulkwitzer See, Seebühne (nahe Tauchschule Delphin)