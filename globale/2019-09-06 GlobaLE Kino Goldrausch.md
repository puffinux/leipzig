---
id: '366193187588069'
title: 'GlobaLE Kino: Goldrausch'
start: '2019-09-06 18:30'
end: '2019-09-06 21:00'
locationName: null
address: 'Völkerfreundschaft Grünau, Stuttgarter Allee 9'
link: 'https://www.facebook.com/events/366193187588069/'
image: 64418293_3091839984161288_5759119950694318080_o.jpg
teaser: 'Wann: Fr 06 September 2019 18:30 - 21:00  Goldrausch - Die Geschichte der Treuhand  BRD / 2012 / 94 min / Dirk Laabs / dt. / Anschließend Diskussion m'
recurring: null
isCrawled: true
---
Wann: Fr 06 September 2019 18:30 - 21:00

Goldrausch - Die Geschichte der Treuhand

BRD / 2012 / 94 min / Dirk Laabs / dt. / Anschließend Diskussion mit Gästen. Die Veranstaltung findet im Rahmen des Grünauer Kultursommers statt. Achtung: Die Veranstaltung beginnt bereits 18:30 Uhr und nicht wie sonst üblich 20 Uhr. Eintritt frei.

Ende der 80er Jahre war das Bruttoinlandsprodukt der DDR höher als in zahlreichen europäischen Marktwirtschaften, die Pro-Kopf-Verschuldung der Menschen in der DDR lag deutlich unter der in der Alt-BRD. Dennoch wird auch 30 Jahre nach „der Wende“ noch immer das Märchen vom wirtschaftlichen Bankrott der DDR bedient, - gerade auch in diesem Jahr sind viele Medien voll von Desinformation. Der Film erzählt die Geschichte der Treuhandanstalt, die nach der Annexion der DDR für die Privatisierung ostdeutschen Volkseigentums zuständig war. Ein dunkles Kapitel und der größte wirtschaftspolitische Skandal im Nachkriegsdeutschland: der Ausverkauf der DDR. In ihrer Schaffenszeit wurden in vier Jahren ungefähr 4000 DDR-Betriebe geschlossen, woraufhin zweieinhalb Millionen Arbeitsplätze verloren gingen. Wie verwandelte die Treuhand ein produktives Volksvermögen von mehr als 600 Milliarden Mark innerhalb weniger Jahre in einen Schuldenberg von 264 Milliarden Mark? Ein Großteil des Geldes konnte durch Tricks und Kniffe in fremde Taschen fließen. Bis heute wurde der Skandal nie vollständig aufgeklärt. In Interviews kommen ehemalige Vorstandsvorsitzende sowie Bürgerrechtler zu Wort.

Wo: Völkerfreundschaft Grünau (Stuttgarter Allee 9)