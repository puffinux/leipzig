---
id: "541106419931137"
title: Offenes Treffen
start: 2020-01-07 19:00
locationName: Libelle
address: 19 Kolonnaden Straße, 04109 Leipzig
link: https://www.facebook.com/events/541106419931137/
teaser: Planung, Vernetzung, Austausch
isCrawled: true
---
Planung, Vernetzung, Austausch 