---
id: "528021858063675"
title: "GlobaLE Kino: Welcome to Sodom"
start: 2019-10-17 18:30
end: 2019-10-17 21:30
address: Bürgerverein Messemagistrale, Str. d. 18. Oktober 10a
link: https://www.facebook.com/events/528021858063675/
image: 72490136_3434835603195056_2223635170504736768_o.jpg
teaser: "Wann: Do 17 Oktober 2019 18:30 - 21:30  Welcome to Sodom - Dein Smartphone
  ist schon hier  Ghana, Österreich / 2018 / 92 min / Florian Weigensamer, Ch"
isCrawled: true
---
Wann: Do 17 Oktober 2019 18:30 - 21:30

Welcome to Sodom - Dein Smartphone ist schon hier

Ghana, Österreich / 2018 / 92 min / Florian Weigensamer, Christian Krönes / Original mit dt. UT / Im Anschluss Diskussion. Eintritt frei.

Der Dokumentarfilm lässt hinter die Kulissen von Europas größter Müllhalde mitten in Afrika blicken und portraitiert die Verliererinnen und Verlierer der digitalen Revolution.
Dabei stehen nicht die Mechanismen des illegalen Elektroschrotthandels im Vordergrund, sondern die Lebensumstände und Schicksale von Menschen, die am untersten Ende der globalen Wertschöpfungskette stehen. Die Müllhalde von Agbogbloshie wird höchstwahrscheinlich auch letzte Destination für die Tablets, Smartphones und Computer sein, die wir morgen kaufen!

Wo: Bürgerverein Messemagistrale, (Straße des 18. Oktober 10a)