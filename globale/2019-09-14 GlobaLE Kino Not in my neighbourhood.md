---
id: '2399554513424494'
title: 'GlobaLE Kino: Not in my neighbourhood'
start: '2019-09-14 20:00'
end: '2019-09-14 23:00'
locationName: null
address: Pöge-Haus
link: 'https://www.facebook.com/events/2399554513424494/'
image: 64302951_3091842894160997_7035636122860388352_o.jpg
teaser: 'Wann: Sa 14 September 2019 20:00 - 22:30  Not in my neighbourhood - From Colonization to Gentrification  Brasilien, Südafrika, USA / 2017 / 76 min / K'
recurring: null
isCrawled: true
---
Wann: Sa 14 September 2019 20:00 - 22:30

Not in my neighbourhood - From Colonization to Gentrification

Brasilien, Südafrika, USA / 2017 / 76 min / Kurt Orderson / original mit UT / Anschließend Diskussion mit Gästen. Gemeinsame Veranstaltung mit dem Polyloid-Festival. Eintritt frei.

Der Film erzählt die generationenübergreifenden Geschichten darüber, wie Menschen auf die Politik, den Prozess und die Institutionen reagieren, die die gegenwärtigen Formen räumlicher Gewalt und Gentrifizierung in Kapstadt, New York und São Paulo vorantreiben. Der Film zielt darauf ab, die Solidarität zwischen aktiven Bewohnerinnen und -bewohnern zu stärken, indem er die Instrumente und Ansätze beleuchtet, mit denen städtische Aktive, ihre von Kolonialisierung, architektonischer Apartheid und Gentrifizierung betroffenen Städte gestalten und navigieren. „Not in My Neigbourhood“ untersucht die Auswirkungen verschiedener Formen räumlicher Gewalt auf den Geist und die soziale Psyche der betroffenen Menschen. Im Film verfolgen wir die täglichen Kämpfe, Prüfungen und Siegesmomente der aktiven Menschen, die für das Recht auf ihre Städte kämpfen.

Wo: Pöge-Haus, (Hedwigstraße 20) 