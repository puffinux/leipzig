---
id: '420914081847361'
title: Café im Garten
start: '2019-06-30 15:00'
end: '2019-06-30 18:00'
locationName: Stadtgarten H17
address: 'Hähnelstraße 17, 04177 Leipzig'
link: 'https://www.facebook.com/events/420914081847361/'
image: 65222283_2456558241239963_5534825260869746688_n.jpg
teaser: 'Es gibt fruchtige Kuchen, Kaffee und ein Plantschbecken!  #sichsgutgehenlassenimgarten  Spendenbasis, klar.'
recurring: null
isCrawled: true
---
Es gibt fruchtige Kuchen, Kaffee und ein Plantschbecken! 
#sichsgutgehenlassenimgarten

Spendenbasis, klar.