---
id: '420515652071913'
title: VeKü - Abendessen im Garten
start: '2019-05-01 18:00'
end: '2019-05-01 20:00'
locationName: Stadtgarten H17
address: 'Hähnelstraße 17, 04177 Leipzig'
link: 'https://www.facebook.com/events/420515652071913/'
image: 58570593_2410373315858456_3491204981627813888_n.jpg
teaser: 'Wie immer, wie lecker - Essen im Garten gegen Spende. Hast du Lust mitzukochen? Prima, dann schreib an vekue@freiraumsyndikat.de!'
recurring: null
isCrawled: true
---
Wie immer, wie lecker - Essen im Garten gegen Spende.
Hast du Lust mitzukochen? Prima, dann schreib an vekue@freiraumsyndikat.de!