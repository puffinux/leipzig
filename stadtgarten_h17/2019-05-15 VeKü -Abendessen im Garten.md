---
id: '463426431066385'
title: VeKü -Abendessen im Garten
start: '2019-05-15 16:00'
end: '2019-05-15 18:00'
locationName: Stadtgarten H17
address: 'Hähnelstraße 17, 04177 Leipzig'
link: 'https://www.facebook.com/events/463426431066385/'
image: 59928730_2418748945020893_8409011255024549888_n.jpg
teaser: 'Kommt ihr Leut und ratet, was im Topfe bratet!  Leckres Essen gegen Spende, helfende Hände bei der Vorbereitung werden  freudig begrüßt.'
recurring: null
isCrawled: true
---
Kommt ihr Leut und ratet, was im Topfe bratet! 
Leckres Essen gegen Spende, helfende Hände bei der Vorbereitung werden  freudig begrüßt. 