---
id: '366925657352206'
title: Sommerfest des Stadtgarten H17 am 25. August
start: '2019-08-25 15:00'
end: '2019-08-25 21:00'
locationName: Stadtgarten H17
address: 'Hähnelstraße 17, 04177 Leipzig'
link: 'https://www.facebook.com/events/366925657352206/'
image: 67709776_2471748489720938_8694959929800261632_n.jpg
teaser: Stadtgartgen H17 e.V. wird 5 Jahre – Lasst uns zusammen Geburtstag feiern!  Herzlichen Einladung zum Gartensommerfest! am 25. August 15.00 – 21.30 Uhr
recurring: null
isCrawled: true
---
Stadtgartgen H17 e.V. wird 5 Jahre –
Lasst uns zusammen Geburtstag feiern!

Herzlichen Einladung zum Gartensommerfest!
am 25. August
15.00 – 21.30 Uhr

Es gibt:
Kaffee und (veganen) Kuchen
(selbstgemachte) Limos

Konzert der BrazzBanditen um 16 Uhr
Eine Kostprobe der Banditen findet ihr hier: https://www.youtube.com/watch?v=DA-EFMqnMNQ

Kinderschminken und -spielecke
Eröffnung der Ausstellung zu Tomatenvielfalt der Gartenwerkstadt Marburg
Infos zur Ausstellung findet ihr hier: https://www.gartenwerkstadt.de/index.php/2017-tomatenvielfalt/

leckeres Abendessen ab 18 Uhr
Lagerfeuer mit Livemusik von Amir Kalhor gegen 20.30 Uhr
Einen Eindruck von Amirs Musik bekommt ihr hier: https://amirakalhor.wixsite.com/amir-kalhor

Wo? Hähnelstraße 17 in Lindenau


