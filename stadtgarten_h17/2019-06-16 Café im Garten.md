---
id: '443302602885088'
title: Café im Garten
start: '2019-06-16 15:00'
end: '2019-06-16 18:00'
locationName: Stadtgarten H17
address: 'Hähnelstraße 17, 04177 Leipzig'
link: 'https://www.facebook.com/events/443302602885088/'
image: 62468622_2446158235613297_1430365225013477376_n.jpg
teaser: 'Wir halten wieder Kaffee, Kuchen (auch vegan) und Limo für euch bereit, für Katerfrühstück, Kaffeekränzchen oder was ihr draus macht! Seid willkommen!'
recurring: null
isCrawled: true
---
Wir halten wieder Kaffee, Kuchen (auch vegan) und Limo für euch bereit, für Katerfrühstück, Kaffeekränzchen oder was ihr draus macht! Seid willkommen!