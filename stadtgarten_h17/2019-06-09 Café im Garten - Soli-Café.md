---
id: '2312209382349838'
title: Café im Garten - Soli-Café
start: '2019-06-09 15:00'
end: '2019-06-09 18:00'
locationName: Stadtgarten H17
address: 'Hähnelstraße 17, 04177 Leipzig'
link: 'https://www.facebook.com/events/2312209382349838/'
image: 62042761_2441231192772668_6757033332514488320_n.jpg
teaser: 'Katerfrühstück und Kaffeekränzchen im Garten. Die Einnahmen gehen diesmal an #unteilbar https://www.unteilbar.org/ denn auch wir sagen: „Es reicht, wi'
recurring: null
isCrawled: true
---
Katerfrühstück und Kaffeekränzchen im Garten. Die Einnahmen gehen diesmal an #unteilbar https://www.unteilbar.org/ denn auch wir sagen: „Es reicht, wir wollen eine andere Gesellschaft! . . .

. . . Wir stehen #unteilbar für Gleichheit und soziale Rechte. Eine Politik, die auf grenzenloses Wachstum und maximale Gewinne setzt, erzeugt massive soziale Ungleichheit und zerstört die Natur. Sie bereitet den Weg für autoritäre Lösungen und das Erstarken völkischer Parteien. Die Wahlen im Jahr 2019 sind eine Nagelprobe für die Demokratie. Machen wir gemeinsam eine andere, eine offene und solidarische Gesellschaft sichtbar! Ergreifen wir die Initiative!“
