---
id: '419522011961277'
title: Flohmarkt im Stadtgarten
start: '2019-06-02 15:00'
end: '2019-06-02 18:00'
locationName: Stadtgarten H17
address: 'Hähnelstraße 17, 04177 Leipzig'
link: 'https://www.facebook.com/events/419522011961277/'
image: 60055872_2422988454596942_7276077965572571136_n.jpg
teaser: 'Es soll Flohmarkt sein! Komm trödeln oder mache einen Stand! Kaffee und Kuchen gibts natürlich auch :)  Willst du einen Stand machen, dann schreib ein'
recurring: null
isCrawled: true
---
Es soll Flohmarkt sein! Komm trödeln oder mache einen Stand!
Kaffee und Kuchen gibts natürlich auch :)

Willst du einen Stand machen, dann schreib eine Mail an flohmarkt@freiraumsyndikat.de