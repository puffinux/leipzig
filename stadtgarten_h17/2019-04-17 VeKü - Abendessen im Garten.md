---
id: '630320377414927'
title: VeKü - Abendessen im Garten
start: '2019-04-17 18:00'
end: '2019-04-17 20:00'
locationName: Stadtgarten H17
address: 'Hähnelstraße 17, 04177 Leipzig'
link: 'https://www.facebook.com/events/630320377414927/'
image: 56140559_2394960034066451_1266446565506023424_n.jpg
teaser: Der kulinarische Wahnsinn geht weiter! Leckeres aus dem Garten und vom Markt landet in Topf und Pfanne und schlussendlich in unser aller Mägen. Wie im
recurring: null
isCrawled: true
---
Der kulinarische Wahnsinn geht weiter! Leckeres aus dem Garten und vom Markt landet in Topf und Pfanne und schlussendlich in unser aller Mägen. Wie immer vegan und lecker und Lagerfeuer.

Gegen Spende.