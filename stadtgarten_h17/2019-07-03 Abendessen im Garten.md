---
id: '414597045800815'
title: Abendessen im Garten
start: '2019-07-03 18:00'
end: '2019-07-03 20:00'
locationName: Stadtgarten H17
address: 'Hähnelstraße 17, 04177 Leipzig'
link: 'https://www.facebook.com/events/414597045800815/'
image: 65012919_2453552824873838_6497204677443059712_n.jpg
teaser: 'Wem läuft da nicht das Wasser im Mund zusammen: selbstgebackenes Brot, darauf Hummus, Baba Ganoush, Kräuterquark und anderes, ein großer grüner Garten'
recurring: null
isCrawled: true
---
Wem läuft da nicht das Wasser im Mund zusammen: selbstgebackenes Brot, darauf Hummus, Baba Ganoush, Kräuterquark und anderes, ein großer grüner Gartensalat und zur Krönung Schokopudding. Aufgrund der Hitze warten wir dieses Mal mit einem kalten Buffet auf, natürlich wie immer gegen Spende. Alles frisch, selbstgemacht, vegan/ vegetarisch. 