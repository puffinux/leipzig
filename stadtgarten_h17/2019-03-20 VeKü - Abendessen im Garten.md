---
id: '256704388540814'
title: VeKü - Abendessen im Garten
start: '2019-03-20 18:00'
end: '2019-03-20 20:00'
locationName: Stadtgarten H17
address: 'Hähnelstraße 17, 04177 Leipzig'
link: 'https://www.facebook.com/events/256704388540814/'
image: 53681255_2382237515338703_4484573437645291520_n.jpg
teaser: Der Frühling ist da- und mit ihm kommt unsere Lust zum Kochen und Gärtnern zurück! Du liebst auch alles was grünt und hast abends ordentlich Kohldampf
recurring: null
isCrawled: true
---
Der Frühling ist da- und mit ihm kommt unsere Lust zum Kochen und Gärtnern zurück! Du liebst auch alles was grünt und hast abends ordentlich Kohldampf? Dann komm zu uns und schmause lecker Gemüse-Ingwer-Eintopf mit Brot und Pesto. Flackerndes Feuer inklusive.

Achtung: Essen gibts dieses Mal schon 18 Uhr!

Alles gegen Spende.
VeKü = Vereinsküche