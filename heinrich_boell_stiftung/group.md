---
name: weiterdenken - Heinrich-Böll-Stiftung
website: http://www.weiterdenken.de/de
scrape:
  source: facebook
  options:
    page_id: 307074760494
  filter:
    address: Leipzig
---
Weiterdenken ist eine Einrichtung der politischen Bildung für Erwachsene in Sachsen. Mit Seminaren, Workshops, Vorträgen, Ausstellungen, Veröffentlichungen, Lesungen und künstlerische Annäherungen an politische Themen unterstützen wir in Sachsen, Ideen, Orientierung, Engagement und konkrete Konzepte für die sozialen und ökologischen Lebensgrundlag...