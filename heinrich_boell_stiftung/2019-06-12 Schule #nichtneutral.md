---
id: '326652907994865'
title: 'Schule #nichtneutral'
start: '2019-06-12 13:00'
end: '2019-06-12 18:00'
locationName: Mediencampus Villa Ida
address: 'Poetenweg 28, 04155 Leipzig'
link: 'https://www.facebook.com/events/326652907994865/'
image: 57485900_10161942023520495_5382684234911580160_n.jpg
teaser: '#nichtneutral  Schule unter Druck. Wertebildung am Pranger? Eine Tagung für Lehrer_innen, Schulsozialarbeiter_innen und diejenigen, die sie unterstütz'
recurring: null
isCrawled: true
---
#nichtneutral 
Schule unter Druck. Wertebildung am Pranger?
Eine Tagung für Lehrer_innen, Schulsozialarbeiter_innen und diejenigen, die sie unterstützen. 

Nicht nur Rechtspopulist*innen fordern „Neutralität“ von Schulen, unterstellen Lehrkräften Indoktrination und schüren Misstrauen gegen Aufklärung und Haltung. Es gibt kein Neutralitätsgebots, obwohl das immer wieder behauptet wird.
Der Beutelsbacher Konsens kennt ein Kontroversitätsgebot und ein Überwältigungsverbot. Das Sächsische Schulgesetz formuliert einen humanistischen und keinen „neutralen“ Bildungsauftrag. 
Aber was heißt das ganz konkret: Was darf ich  sagen? Darf ich mich positionieren und wo muss ich es sogar? Wie gelingt konstruktive Positionierung?
Zu diesen Fragen, die sich Lehrende stellen, kommen aktuell politische und fachliche Angriffe auf die schulische Praxis unter anderem durch „Online-Pranger“ der Partei AfD. Das verunsichert Lehrerinnen und Lehrer oft und bringt sie in eine unnötige Verteidigungssituation gegenüber Leitungen, Aufsichtsbehörden und Eltern. Hier braucht es Klarheit und Unterstützung.
Diese Tagung ist ein Angebot für all jene, für die Schweigen keine pädagogische Alternative und Neutralität keine Haltung ist.