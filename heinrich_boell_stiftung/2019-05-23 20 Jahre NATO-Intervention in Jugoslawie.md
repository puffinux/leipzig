---
id: '2309446869098964'
title: 20 Jahre NATO-Intervention in Jugoslawien
start: '2019-05-23 18:00'
end: '2019-05-23 20:00'
locationName: null
address: 'GWZO, Specks Hof, Reichsstraße 4-6, 04109 Leipzig'
link: 'https://www.facebook.com/events/2309446869098964/'
image: 60083119_10157088603208329_8027404484516773888_n.jpg
teaser: 'Verortungen, Erfahrungen, Kontroversen  Wir wollen zum 20igsten Jahrestag mit Expert/innen ins Gespräch kommen und das historische Vermächtnis diskuti'
recurring: null
isCrawled: true
---
Verortungen, Erfahrungen, Kontroversen

Wir wollen zum 20igsten Jahrestag mit Expert/innen ins Gespräch kommen und das historische Vermächtnis diskutieren. Gibt es ein lessons learned in Hinblick auf aktuelle Konflikte?

Einführung: 
Katarina Ristic & Elisa Satjukow (Universität Leipzig)

Als Podiumsgäste sind eingeladen:
Nora Ahmetaj, Gründerin des Centre for Research, Documentation
and Publication, Pristina

Stefan Troebst, Leibniz-Institut für Geschichte und Kultur des östlichen Europa

Sofija Todorovic, BIRN-Balkan Investigative Regional Network

Winfried Nachtwei, ehem. MdB Bündnis 90/Die Grünen

Moderation:
Simon Ilse, Heinrich Böll Stiftung e.V., Büro Belgrad

Ort:
GWZO, Specks Hof, Reichsstraße 4-6, 04109 Leipzig

Die Podiumsdiskussion wird dt./engl und engl./dt. gedolmetscht.

Kontakt: 
Petra Zimmermann, Projektkoordination Ost- und Südosteuropa
E-Mail zimmermann@boell.de

Mehr Infos: 
https://calendar.boell.de/de/event/20-Jahre-NATO-Intervention-in-Jugoslawien