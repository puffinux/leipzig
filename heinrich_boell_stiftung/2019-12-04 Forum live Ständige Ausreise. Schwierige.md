---
id: "539690749910345"
title: "Forum live: Ständige Ausreise. Schwierige Wege aus der DDR"
start: 2019-12-04 16:30
end: 2019-12-04 18:00
locationName: Zeitgeschichtliches Forum Leipzig
address: Grimmaische Str. 6, 04109 Leipzig
link: https://www.facebook.com/events/539690749910345/
image: 75439303_563370270903161_1278356015896592384_n.jpg
teaser: Lesung und Diskussion im Forum live  Mit den Herausgebern Jana Göbel und
  Matthias Meisner und der Journalistin Heike Kleffner In Kooperation mit dem C
isCrawled: true
---
Lesung und Diskussion im Forum live 
Mit den Herausgebern Jana Göbel und Matthias Meisner und der Journalistin Heike Kleffner
In Kooperation mit dem Ch.Links Verlag und Weiterdenken - Heinrich-Böll-Stiftung Sachsen e.V.
Eintritt frei

Nachdem die DDR 1975 die KSZE-Schlussakte von Helsinki unterzeichnet hatte, forderten immer mehr Bürger ihre Freiheitsrechte ein. Bis 1989 verließen fast 400.000 Menschen die DDR, indem sie ihre »ständige Ausreise« beantragten – ohne bei einem Fluchtversuch ihr Leben zu riskieren. Doch wer einen Ausreiseantrag gestellt und die „Entlassung aus der Staatsbürgerschaft der DDR“ beantragt hatte, musste sich auf eine harte Zeit einstellen und wurde oft wie ein Verräter behandelt – nicht nur von Behörden, oft auch von Vorgesetzten, Kollegen und sogar von Nachbarn oder Familienmitgliedern. Was waren die Motive für das Verlassen der DDR? Wie lebte man in der Zeit zwischen Antraganstellung und Ausreise, wenn man sich besser nicht verlieben sollte, wenn man monate- oder jahrelang auf gepackten Koffern saß?

Journalistinnen und Journalisten aus ganz Deutschland erzählen 24 Geschichten von Menschen, die per Ausreiseantrag die DDR verlassen haben, von Repressionen durch die Staatssicherheit, von Familienzusammenführungen und trickreichen Scheinehen.
