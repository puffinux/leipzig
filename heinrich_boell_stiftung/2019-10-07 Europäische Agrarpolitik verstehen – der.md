---
id: '707647253066311'
title: Europäische Agrarpolitik verstehen – der Agraratlas
start: '2019-10-07 19:00'
end: '2019-10-07 20:30'
locationName: null
address: 'Kino Cineding, Karl-Heine-Str. 83, 04229 Leipzig'
link: 'https://www.facebook.com/events/707647253066311/'
image: 69991207_1263165473865028_6473707851964481536_n.jpg
teaser: 'Der Agrar-Atlas zeigt, dass kaum etwas von den fast 60 Milliarden Euro, die die EU jährlich für die europäische Landwirtschaft ausgibt, für gesunde Le'
recurring: null
isCrawled: true
---
Der Agrar-Atlas zeigt, dass kaum etwas von den fast 60 Milliarden Euro, die die EU jährlich für die europäische Landwirtschaft ausgibt, für gesunde Lebensmittel, den Schutz von Umwelt, Klima und Biodiversität oder den Erhalt von kleinen und mittleren Betrieben verwendet wird.
Im Gegenteil, von 80 Prozent der Gelder profitieren nur 20 Prozent der Betriebe. Aber der Atlas beweist auch, dass es falsch wäre, die Förderung der Agrarpolitik einfach abzuschaffen. Denn der Umbau zu einer nachhaltigen und global gerechten Landwirtschaft ist nicht umsonst.
Referentin: Dipl. Agr. Ing. Urte Grauwinkel für BUND Sachsen

Der Vortrag ist kostenfrei.

Eine Kooperation der Weiterdenken - Heinrich-Böll-Stiftung Sachsen e.V und dem BUND Sachsen e.V
