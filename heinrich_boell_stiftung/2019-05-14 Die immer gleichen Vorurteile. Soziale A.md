---
id: '270656527178916'
title: Die immer gleichen Vorurteile. Soziale Arbeit von und mit Roma
start: '2019-05-14 07:00'
end: '2019-05-14 14:30'
locationName: null
address: 'Romano Sumnal e. V./Roma Infobüro, Leipzig'
link: 'https://www.facebook.com/events/270656527178916/'
image: 57083600_2076851732370062_4705274180440948736_n.jpg
teaser: 'Das Seminar vermittelt Wissen über die vielfältigen Hintergründe, Realitäten und (Diskriminierungs-)Erfahrungen von v.a. jugendlichen Roma in Sachsen.'
recurring: null
isCrawled: true
---
Das Seminar vermittelt Wissen über die vielfältigen Hintergründe, Realitäten und (Diskriminierungs-)Erfahrungen von v.a. jugendlichen Roma in Sachsen. Es schafft einen Raum, in dem Irritationen oder Konflikte im Arbeitsalltag von Fachkräften gemeinsam besprochen, aufgelöst oder aus einer anderen Perspektive betrachtet werden können. Es soll zur Sensibilisierung und Reflexion der eigenen Profession angeregt werden, die häufig paternalistisch auf Roma blickt. Dabei wird es die Möglichkeit geben sich mit bestehenden good-practice-Projekten auszutauschen.