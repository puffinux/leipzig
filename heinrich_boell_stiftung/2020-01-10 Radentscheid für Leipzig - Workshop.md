---
id: "2477257875923634"
title: Radentscheid für Leipzig? - Workshop
start: 2020-01-10 14:00
end: 2020-01-10 20:00
locationName: Pöge-Haus
address: Hedwigstraße 20, 04315 Leipzig
link: https://www.facebook.com/events/2477257875923634/
image: 80450395_10163101363630495_3164976802175123456_n.jpg
teaser: "Insbesondere der Radverkehr wird in Leipzig oft stiefmütterlich behandelt:
  Lücken im Radwegenetz, plötzlich endende Radspuren und zugeparkte Radwege s"
isCrawled: true
---
Insbesondere der Radverkehr wird in Leipzig oft stiefmütterlich behandelt: Lücken im Radwegenetz, plötzlich endende Radspuren und zugeparkte Radwege sind der Alltag. Zu oft ist die Verkehrsplanung noch an männlichen Erwachsenen ausgerichtet. Eine aktive Community kümmert sich bereits durch Petitionen, die Critical Mass oder institutionell über die AG Rad. Aber reicht das? Brauchen wir weitere Unterstützung durch die Bevölkerung? Wie können wir die Verkehrsplaner weiblicher und kindgerechter machen zum Nutzen der gesamten Bevölkerung? Ist dazu ein Radentscheid in Leipzig das geeignete Mittel?

Darüber wollen wir einen Tag diskutieren mit:
Heinrich Stößenreuther, Radentscheid Berlin, clevere Städte
und Fachleuten aus Leipzig.

Wenn Ihr dabei sein wollt, meldet Euch bitte bis zum 6. Januar per Mail (info@weiterdenken.de) an.
Die Teilnahme ist kostenfrei.

Foto: Thomas Puschmann, Frühbeetgrafik. All rights reserved. 