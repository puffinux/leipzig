---
id: '1590100707786563'
title: 'Vortrag: Strukturelle Gründe für Rechtspopulismus'
start: '2019-08-20 19:00'
end: '2019-08-20 21:00'
locationName: Erich- Zeigner Haus e.V.
address: 'Zschochersche Straße 21, 04229 Leipzig'
link: 'https://www.facebook.com/events/1590100707786563/'
image: 67427256_1298994503609282_6312601373833691136_n.jpg
teaser: '- Strukturelle Gründe für Rechtspopulismus: Kultur, Ökonomie oder  Facebook? -  Eine Verantaltung des AK Plurale Ökonomik Leipzig In Zusammenarbeit m'
recurring: null
isCrawled: true
---

- Strukturelle Gründe für Rechtspopulismus: Kultur, Ökonomie oder 
Facebook? -

Eine Verantaltung des AK Plurale Ökonomik Leipzig
In Zusammenarbeit mit Weiterdenken - Heinrich-Böll-Stiftung Sachsen und der Jungen GEW Sachsen
20. August
19.00 Uhr im Erich-Zeigner-Haus 
Eintritt frei


„Menschen, welche die AfD wählen, zählen zu den wirtschaftlich „Abgehängten“. Wir müssen etwas gegen Ungleichheit und prekäre Beschäftigungsverhältnisse tun, um dem entgegen zu wirken!“
„Stimmt nicht! Rechtspopulistische Wähler*innen sind einfach fremdenfeindlich und rassistisch. Sie kommen mit dem gesellschaftlichen Wandel nicht klar, mit Wirtschaft hat das nichts zu tun!“
„Stimmt auch nicht! Rechte Parteien beherrschen soziale Netzwerke und framen die mediale Berichterstattung. Filterblasen und Facebook sind das Problem!“

Es existiert eine Vielzahl an Erklärungen für den Erfolg rechter Parteien in Deutschland, Europa und der Welt. Doch wer hat am Ende recht? Und was sagt die wissenschaftliche Forschung dazu?
Dieser Vortrag versucht das Dickicht zu lichten und die zahlreichen gängigen Erklärungsansätzen für rechte Wahlerfolge zusammenzubringen, einzuordnen und kritisch auszuwerten.
Der Vortrag ist in einfacher Sprache gehalten, basiert auf wissenschaftlich fundierter und fächerübergreifender Forschung und kann als Grundlage für eine hoffentlich spannende Diskussion dienen!