---
id: '2357467834534002'
title: 'Hannibal, Todeslisten, Tag X'
start: '2019-06-20 19:00'
end: '2019-06-20 21:00'
locationName: UT Connewitz
address: 'Wolfgang-Heinze-Str. 12a, 04277 Leipzig'
link: 'https://www.facebook.com/events/2357467834534002/'
image: 61242738_10162107052995495_6577492399482732544_n.jpg
teaser: 'Am Donnerstag, 20. Juni 2019 um 19 Uhr  Gespräch mit der taz-Journalistin Christina Schmidt: Hannibal, Todeslisten, Tag X - Rechtes Schatten-Netzwerk'
recurring: null
isCrawled: true
---
Am Donnerstag, 20. Juni 2019 um 19 Uhr 
Gespräch mit der taz-Journalistin Christina Schmidt: Hannibal, Todeslisten, Tag X - Rechtes Schatten-Netzwerk in der Bundeswehr?

Gibt es ein rechtes Schatten-Netzwerk in Deutschland, das in den bewaffneten Kampf gegen Andersdenkende treten will? Seit über einem Jahr recherchiert die Journalistin Christina Schmidt mit einem Team der tageszeitung dazu. Eine geheime Gruppe aus Soldaten, Polizisten und Behördenmitarbeitern. "Prepper", die sich auf die große Katastrophe vorbereiten, auf den sogenannten Tag X, an dem das System zusammenbricht. Extreme Rechte, die mutmaßlich Todeslisten geführt haben, darauf linke Politiker/innen, die sie an Tag X abholen und hinrichten wollen. Franco A., ein Soldat, der sich als Flüchtling ausgab und Anschläge geplant haben soll, um Stimmung gegen Geflüchtete zu verbreiten. Im Zentrum dieses Netzes: „Hannibal“ – André S. Er war bis 2018 Teil des Kommandos Spezialkräfte der Bundeswehr und ist bis heute Soldat. Er war der Kopf des Vereins Uniter. Der Verein netzwerkt international, bildet Sicherheitspersonal in autoritären Staaten aus und baut offenbar eine "Kampfeinheit" auf. So berichtet es die taz.

Doch der große öffentliche Aufschrei bleibt aus. Behörden und Geheimdienste ermitteln und informieren nur langsam. Doch ein rechtes Schatten-Netzwerk mit Zugang zu Waffen ist eine Bedrohung für eine demokratische Gesellschaft. Wer ist Hannibal und was hat sein Netzwerk vor? Was hat der Verein Uniter damit zu tun? Und warum bleibt die öffentliche Aufregung so gering? Darüber sprechen wir mit Christina Schmidt.

Christina Schmidt ist Reporterin der tageszeitung in Berlin und weltweit. Sie recherchiert zu Geheimdiensten, schreibt über militante Rechte und porträtiert das politische Leben.

Moderation: 
Hannah Eitel, Weiterdenken – Heinrich-Böll-Stiftung Sachsen und 
Michael Nattke, Kulturbüro Sachsen e.V.

Der Eintritt ist frei.
Für alle, die nicht live dabei sein können. Es wird einen Livestream geben.