---
id: '331634647541056'
title: ROADS Publikumspremiere in Leipzig mit Sebastian Schipper
start: '2019-05-28 20:00'
end: '2019-05-28 22:30'
locationName: Passage Kinos Leipzig
address: 'Hainstraße 19a, 04109 Leipzig'
link: 'https://www.facebook.com/events/331634647541056/'
image: 58543531_10158878811479896_117863244156108800_n.jpg
teaser: Drei Jahre nach dem sensationellen Erfolg von VICTORIA schickt Regisseur Sebastian Schipper in ROADS zwei Achtzehnjährige auf einen ebenso aufregenden
recurring: null
isCrawled: true
---
Drei Jahre nach dem sensationellen Erfolg von VICTORIA schickt Regisseur Sebastian Schipper in ROADS zwei Achtzehnjährige auf einen ebenso aufregenden wie bewegenden Trip durch Europa. ROADS erzählt von einer bedingungslosen Freundschaft zweier Jugendlicher – angesiedelt in einer Welt, die sich im radikalen Umbruch befindet. 

Noch vor dem offiziellen Kinostart feiern wir die Publikumspremiere von ROADS mit unseren Partnern in Anwesenheit des Regisseurs Sebastian Schipper im Kino in der Passage. Karten sind im VVK und an der Abendkasse erhältlich.

28.5.2019 | Filmbeginn 20.00 Uhr | Passage Kino
Publikumspremiere ROADS
mit anschließendem Filmgespräch in Kooperation mit Weiterdenken – Heinrich-Böll-Stiftung Sachsen

Podium:
- Sebastian Schipper, Regisseur ROADS
- Mathias Anderson, Geschäftsführer arche noVa

Moderation: Petra Cagalj Sejdi, migrationspolitische Sprecherin der Stadtratsfraktion von Bündnis90/Die Grünen Leipzig

www.roads-film.de | unterstützt von AKTION DEUTSCHLAND HILFT, Start with a Friend e.V., Amnesty International Deutschland, No Hate Speech Movement Deutschland
