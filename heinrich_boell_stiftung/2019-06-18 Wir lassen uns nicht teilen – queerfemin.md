---
id: '460961868041920'
title: Wir lassen uns nicht teilen – queerfeministische Perspektiven
start: '2019-06-18 19:00'
end: null
locationName: Galerie KUB
address: 'Kantstr. 18, 04275 Leipzig-Connewitz, Sachsen'
link: 'https://www.facebook.com/events/460961868041920/'
image: 61894639_2144496119012760_4576910424145920000_n.jpg
teaser: 'SCROLL DOWN FOR SPANISH, FRENCH AND ENGLISH TRANSLATION  Die Landtagswahlen stehen vor der Tür und ein gesellschaftlicher und politischer Rechtsruck i'
recurring: null
isCrawled: true
---
SCROLL DOWN FOR SPANISH, FRENCH AND ENGLISH TRANSLATION

Die Landtagswahlen stehen vor der Tür und ein gesellschaftlicher und politischer Rechtsruck in Sachsen ist deutlich spürbar.  Was heißt das für den Lebensalltag und die Projekte von Frauen und Queers? Wie können wir einen solidarischen Zusammenhalt untereinander organisieren?

Wir laden zur Vollversammlung ein, um diese dringenden Fragen mit euch zu diskutieren. Im ersten Teil der Veranstaltung wird es ein einführendes Gespräch über Antifeminismus von Rechts und die lokalen Auswirkungen des Rechtsrucks auf queerfeministische Projekte geben. Im Anschluss folgt eine Austausch- und Vernetzungsphase, in der wir über die Zukunft unserer Projekte, queerfeministischen Zusammenhalt und mögliche Strategien des Widerstands diskutieren wollen. 

Die Vollversammlung findet im Rahmen von #unteilbar Sachsen statt. #unteilbar kämpft gegen jegliche Art von gruppenbezogener Menschenfeindlichkeit und Antifeminismus und steht für eine offene und solidarische Gesellschaft ein. Mehr Infos zu #unteilbar hier: www.unteilbar.org

Referentinnen:
Hannah Eitel (weiterdenken e.V.) und Manuela Tillmanns (Rosalinde e.V., Queer durch Sachsen) 

Moderation:
Sarah Ulrich (Freie Journalistin)

Bei Bedarf nach Kinderbetreuung oder Verdolmetschung bitte anmelden unter: 
feminismus-unteilbar-le@web.de

//////////

The state elections are just around the corner and a social and political shift to the right is clearly noticeable in Saxony.  Which effect does this have on the everyday lives of women and queers as well as on projects organized by/for them? How can we organise solidarity among each other?

We invite you to a Plenary Assembly to discuss these urgent questions with us. The first part of the event will be an introductory discussion about anti-feminism from the right-wing as well as local effects of the shift to the right on queer feminist projects. Afterwards we want to enter an exchange and networking phase in which we want to discuss the future of our projects, aspects of a queer feminist cohesion and possible strategies of resistance.

The plenary assembly takes place within the framework of #Unteilbar Saxony. #unteilbar fights against any kind of group-related misanthropy and anti-feminism and stands for an open and solidary society. More info about #unteilbar: www.unteilbar.org

Speakers:

Hannah Eitel (weiterdenken e.V.) and Manuela Tillmanns (Rosalinde e.V., Queer through Saxony)

Moderation:

Sarah Ulrich (freelance journalist)

If you need childcare or interpreting, please register under:

feminismus-unteilbar-le@web.de

//////////

Nous ne nous laisserons pas nous diviser !
Point de vue queer-feministe sur les élections au parlement des Länder

Discours et discussion

Les élections du parlement des Länder sont proches et le virage à droite pris par la société et la politique en Saxe est clairement visible. Qu'est-ce que cela signifie pour la vie quotidienne et les projets des femmes et des queers? Comment pouvons-nous organiser la solidarité les un*e*s avec les autres?

Nous vous invitons à une Assemblée générale pour discuter de ces questions pressentes avec vous. La première partie de l'événement comportera une présentation introductive sur l'anti-féminisme de droite et l'impact local de la droitisation sur des projets queer-féministes. Cela sera suivi d’une phase d’échange et de mise en réseau au cours de laquelle nous discuterons de l’avenir de nos projets, de la cohésion queer-féministe et des stratégies de résistance possibles.

L'assemblée générale se déroule dans le cadre de #unteilbar Sachsen.  #unteilbar lutte contre toutes sortes d'inimitié adressée à l'encontre de certains groupes de personnes et contre l'anti-féminisme. #unteilbar défend une société ouverte à tou.tes et solidaire. Plus d'informations sur #unteilbar ici: www.unteilbar.org

Conferencières :
Hannah Eitel (weiterdenken e.V.) et Manuela Tillmanns (Rosalinde e.V., Queer durch Sachsen)

Modération :
Sarah Ulrich (Journaliste indépendante)

Pour une garde d'enfants ou pour une traduction, veuillez vous inscrire sous:
feminismus-unteilbar-le@web.de

//////////

Las elecciones regionales están a la vuelta de la esquina y se
nota un cambio social y político hacia la derecha en Sajonia. ¿Qué significa esto para la vida cotidiana y los proyectos organizados por y para mujeres y personas queer? ¿Cómo podemos organizar una solidaridad entre nosotrxs?
Les invitamos a la asamblea plenaria a discutir estas cuestiones tan
urgentes. En la primera parte del evento habrá una discusión
introductoria sobre el antifeminismo de la derecha y los efectos locales a
proyectos queerfeministas causadas por el giro hacia la derecha. Después
habrá una fase de intercambio y networking en la cual discutimos el
futuro de nuestros proyectos, la cohesión queerfeminista y las
estrategias de resistencia posibles.

La asamblea plenaria tendrá lugar en el marco de #unteilbar Sajonia.
#unteilbar lucha contra todos los tipos de misantropía (relacionada con
el grupo) y antifeminismo y defiende una sociedad abierta y solidaria.
Más información sobre #unteilbar aquí: www.unteilbar.org

Ponentes:
Hannah Eitel (weiterdenken e.V.) y Manuela Tillmanns (RosaLinde e.V.,Queer durch Sachsen)

Moderación:
Sarah Ulrich (periodista autónoma)

Si necesita cuidado de niños o interpretación, por favor, regístrese en:
feminismus-unteilbar-le@web.de