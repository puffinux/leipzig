---
id: '537274133747103'
title: »LOST in Transformation«
start: '2019-10-04 18:00'
end: '2019-10-05 20:00'
locationName: Galerie KUB
address: 'Kantstr. 18, 04275 Leipzig-Connewitz, Sachsen'
link: 'https://www.facebook.com/events/537274133747103/'
image: 70461152_127445958591925_8069512872167735296_n.jpg
teaser: 'Was ist eigentlich im Osten los? Seit 2014/15 hat die Dynamik der Ereignisse - Wahlergebnisse für die AfD, gewalttätige Auseinandersetzungen, verbale'
recurring: null
isCrawled: true
---
Was ist eigentlich im Osten los? Seit 2014/15 hat die Dynamik der Ereignisse - Wahlergebnisse für die AfD, gewalttätige Auseinandersetzungen, verbale Entgrenzungen - insbesondere in Ostdeutschland ständig zugenommen. Es scheint eine neue Phase der Nachwendegeschichte begonnen zu haben, in der das bisherige, stille Einverständnis mit den Regierenden von einem beträchtlichen Teil der Bevölkerung aufgekündigt wird.

Mit den gegenwärtigen Debatten um die Anerkennung der „ostdeutschen Lebensleistung“, einen „Gerechtigkeitsfonds“, eine verbindliche Einstellungsquote für Ostdeutsche, aber auch das Sprechen über biographische Brüche rückt die Zeit der „friedlichen Revolution“ erneut in den Fokus der Debatten. Deutungen über den Charakter der ostdeutschen Teilgesellschaft 30 Jahre nach dem Mauerfall haben wieder Hochkonjunktur.

Die Konferenz „Lost in Transformation“ möchte geläufige und weniger geläufige Analysen über die Eigenarten Ostdeutschlands versammeln und mit einer interessierten Öffentlichkeit debattieren. Neben aktuell diskutierten Fragen zum ostdeutschen Wohnungsmarkt, zum Rechtsruck und zur spezifischen ostdeutschen Wirtschaftsweise werden auch die ideologischen Hinterlassenschaften der DDR-Gesellschaft und die Widersprüchlichkeiten der Debatten zu Migration damals und heute beleuchtet.

Die Konferenz versteht sich dabei als Debattenforum, richtet sich explizit auch an ein nicht-wissenschaftliches Publikum und möchte zivilgesellschaftliche Initiativen zum Austausch einladen.