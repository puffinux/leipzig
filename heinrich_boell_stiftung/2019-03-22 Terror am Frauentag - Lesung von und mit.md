---
id: '2239743796293950'
title: Terror am Frauentag - Lesung von und mit Christine Lehmann
start: '2019-03-22 19:00'
end: '2019-03-22 21:00'
locationName: Pöge-Haus
address: 'Hedwigstraße 20, 04315 Leipzig'
link: 'https://www.facebook.com/events/2239743796293950/'
image: 52634581_10161724876550495_7761315004078358528_n.jpg
teaser: Im Rahmen der Leipziger Buchmesse findet im Leipziger Pöge-Haus am 22. März eine Lesung mit Christine Lehmann aus ihrem Buch "Die zweite Welt" statt.
recurring: null
isCrawled: true
---
Im Rahmen der Leipziger Buchmesse findet im Leipziger Pöge-Haus am 22. März eine Lesung mit Christine Lehmann aus ihrem Buch "Die zweite Welt" statt.

Worum geht es in dem Buch?
s ist der 8. März 2019 in Stuttgart. Ein kalter Tag, noch winterlich. Aus dem Radio kommt schon in den Morgenstunden allerlei Wissenswertes zum Thema Feminismus. Was die Sender nicht ausstrahlen, sind die eingehenden Drohanrufe.

»Der Ehrenmann handelt. Schluss mit dem Feminazen-Terror. Ich will heute keine von euch Schlampen auf der Straße sehen« – das hat Sally mitgeschrieben, Lisa Nerz’ alte Freundin, die im SWR-Studio am Frauentag Telefondienst macht. Die Wortwahl gemahnt an die Propaganda der PGM, der ›Partei des gesunden Menschenverstandes‹, die sich über Einwanderung, Genderfragen, die Bedrohung der Familie und die Verkrüppelung der Muttersprache ereifert. Mit Unbehagen erinnert sich Lisa, von den ›Unfren‹ gehört zu haben, ›unfreiwillig enthaltsame‹ Männer, die im Internet ihren Frust zelebrieren und sich in Frauenhass hineinsteigern. Steckt so einer hinter den Drohungen?

Gemeinsam mit der technisch hoch versierten Schülerin Tuana klappert Lisa Nerz ihre Quellen ab, um Licht in die Sache zu bringen, ehe die Demo losgeht. Wobei Tuana, ganz selbstverständlich in Hidschab und Abaya gekleidet, ihr einen ungewohnten Blick auf Straßenkommunikation verschafft. Können die beiden rechtzeitig herausfinden, wer wirklich hinter den Drohungen steckt?

Das Buch ist im Ariadne-Verlag erschienen.

mehr zur Leipziger Buchmesse: https://www.facebook.com/events/690900671356377/ 