---
id: '620066448410631'
title: 'Dui Rroma - zwei Generationen, eine Geschichte'
start: '2019-03-20 18:00'
end: '2019-03-20 20:00'
locationName: Alte Nikolaischule
address: 'Nikolaikirchhof 2, 04109 Leipzig'
link: 'https://www.facebook.com/events/620066448410631/'
image: 51258199_2264597410424961_8450758569069903872_n.jpg
teaser: 'Dui Rroma - zwei Generationen, eine Geschichte Der Dokumentarfilm erzählt die Geschichte Hugo Höllenreiners, ein Sinto, der mehrere Konzentrationslage'
recurring: null
isCrawled: true
---
Dui Rroma - zwei Generationen, eine Geschichte
Der Dokumentarfilm erzählt die Geschichte Hugo Höllenreiners, ein Sinto, der mehrere Konzentrationslager überlebte und Opfer des KZ-Arztes Josef Mengele wurde. In einer Zugfahrt nach Auschwitz erzählt der er dem Komponisten und Rom Adrian Gaspar, von seinem erlittenen, unvorstellbaren Leid. Der junge, in Rumänien geborene Musiker Adrian Gaspar, verarbeitet diese Begegenung in seiner Komposition „Bari Duk - Großes Leid“. 
Der Dokumentarfilm wurde 2014 mit dem Fernsehpreis der Erwachsenenbildung ausgezeichnet.
Filmvorführung mit anschließendem Klavier-Konzert und Gespräch mit der Regisseurin Iovanka Gaspar und Komponisten Adrian Gaspar