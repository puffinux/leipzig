---
id: "2501920026553070"
title: Kundgebung gegen Zirkus Aeros
start: 2019-12-19 16:00
end: 2019-12-19 17:30
address: Kleinmesseplatz, 04177 Leipzig
link: https://www.facebook.com/events/2501920026553070/
image: 78686197_1183888128482144_6174040076687769600_o.png
teaser: (English version below!)  Zirkus Aeros ist leider wieder, wie jedes Jahr, mit
  seinem Weihnachtsprogramm in Leipzig und beutet dabei noch immer nichtme
isCrawled: true
---
(English version below!)

Zirkus Aeros ist leider wieder, wie jedes Jahr, mit seinem Weihnachtsprogramm in Leipzig und beutet dabei noch immer nichtmenschliche Tiere zu Unterhaltungszwecken aus. Dagegen protestieren wir zur Premiere am Donnerstag, den 19.12.2019 von 16:00 bis 17:30 am Kleinmesseplatz am Cottaweg.
Über eure Unterstützung würden wir uns sehr freuen! :)

Bringt auch gerne Demomaterial (z.B. Schilder und Transpis) mit. Wenn ihr allerdings in anderen Tierbefreiungs-, Tierrechts- oder generell anderen politischen Gruppen aktiv seid, würden wir euch bitten, keine Logos o.ä. dieser Gruppen zu zeigen.

Wie bei jeder unserer Veranstaltungen gilt: Antiemanzipatorische und diskriminierende Verhaltensweisen werden nicht geduldet. Damit sind alle ausgeladen, die rassistisches, antisemitisches, sexistisches, klassistisches, ableistisches, homo-, trans*- oder interfeindliches Gedankengut hegen.

----------------------------

Circus Aeros is back in Leipzig for yet another year of their christmas show... and of course, they are still exploiting nonhuman animals for entertainment. We'll protest against this on Thursday, the 19th of December, from 4pm 'til 5:30pm at the "Kleinmesse" / Cottaweg.
We'd appreciate your support! :)

Feel free to bring demonstration material (like signs and banners). If you are part of another animal liberation, animal rights oder other political groups, we ask you not to show any logos of these groups.

As for all our events: Antiemancipatory and discriminating behaviour will not be tolerated. This means that you are not welcome if you're endorsing or practicing racist, antisemitic, sexist, classist, ableist, homo-, trans*- or interhostile ideologies.

(German version above!)