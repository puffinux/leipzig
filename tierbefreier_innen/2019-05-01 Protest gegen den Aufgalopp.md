---
id: '2118700198421608'
title: Protest gegen den Aufgalopp
start: '2019-05-01 13:00'
end: '2019-05-01 17:00'
locationName: Galopprennbahn Scheibenholz
address: 'Rennbahnweg 2A, 04107 Leipzig, 04107 Leipzig Leipzig'
link: 'https://www.facebook.com/events/2118700198421608/'
image: 56870042_1018049075066051_8756411995453915136_n.jpg
teaser: 'Die Tierbefreier_innen Leipzig und Leipzig/Halle Animal Save rufen auch in diesem Jahr zum Protest am 1. Mai gegen den Aufgalopp, auf der Rennbahn Sch'
recurring: null
isCrawled: true
---
Die Tierbefreier_innen Leipzig und Leipzig/Halle Animal Save rufen auch in diesem Jahr zum Protest am 1. Mai gegen den Aufgalopp, auf der Rennbahn Scheibenholz, auf!

Erst vor sieben Monaten starb der Wallach Wilamendhoo, in Folge seiner, auf der Rennbahn in Leipzig erlittenen, Verletzung. 
Dies ist bei weitem kein trauriger Einzelfall; im August 2016 stürzte Hengst Lenno. Das Pferd sprang vor Schmerzen auf und sein gebrochenes rechtes Vorderbein schlackerte nur noch hin und her. Lenno wurde noch auf der Rennbahn getötet. Im Jahr 2012 wurde der Hengst Proust in Scheibenholz eingeschläfert, er musste trotz einer offenen Fesselgelenkfraktur noch über die Zielgerade laufen. Wilamendhoo, Lenno und Proust wurden nur 3, 4 und 5 Jahre alt. 
Weitere Pferde sind sowohl in Leipzig als auch in zahlreichen anderen Städten dem Rennsport zum Opfer gefallen. Wie viele mehr während des Trainings und als ausgediente Rennpferde ihr Leben lassen mussten, ist nicht zu beziffern.

Es ist nicht unüblich dass die Pferde mit gerade mal zwei Jahren ihr erstes Rennen laufen müssen, dafür werden sie mit 1,5 Jahren, fast noch im Fohlenalter, eingeritten. Genauso schnell werden sie wieder entsorgt; Während Pferde in Freiheit leicht ein Alter von 30 Jahren erreichen können, werden sie im Rennsport häufig noch unter 8 Jahren aussortiert. 
Ein wild lebendes Pferd bewegt sich die meiste Zeit des Tages mit seinen Artgenossen auf einer Weide. Dort traben Pferde, wenn sie miteinander im Spaß herumtoben. Pferde galoppieren nur, wenn sie vor Gefahr flüchten. Daher zählen Pferde zu den sogenannten Fluchttieren. Bei Anzeichen von Gefahr ziehen sie immer die Flucht der Auseinandersetzung vor. Sie sind Langstreckenläufer und keine Sprinter!
Das Rennen auf der Galopprennbahn ist daher unnatürlich und nicht artgerecht! Für den Sport werden die Pferde gezwungen regelmäßig zu galoppieren. Sie müssen längere Strecken im Sprint zurücklegen, weil sie so häufig zum “Training” gedrillt werden.
Vor den Geschwindigkeiten (im Vergleich Trab 13,5 km/h vs Galopp 60 bis 90 km/h), die Pferde beim Rennsport erreichen müssen, haben die Pferde selber Angst. Zynischerweise treibt sie das nochmal extra an, schneller zu laufen. Der Stress, der für die Pferde dabei entsteht, führt zu heftigen psychischen Störungen. Das zeigt sich bei Pferden, die Jahre später keine Rennen mehr laufen müssen. Wenn Sie das Glück haben, überhaupt ins Rentenalter zu kommen. Selbst wenn sie dann ein Leben ganz ohne Rennsport haben, und es ihnen verhältnismäßig gut geht, dauert es mitunter Jahre, bis sie ihre Angst verlieren und die psychischen Störungen langsam verschwinden. Außerdem leidet ein hoher Prozentsatz der Rennpferde unter stressbedingten Magengeschwüren.

Pferde sind keine Sportgeräte! Sie sind ebenso fühlende sensible Lebewesen mit einer zerbrechlichen Psyche.
Wir können Sie dem Druck auf der Rennbahn nicht aussetzen, ohne ihnen erheblichen Schaden zuzufügen. Übrigens leiden Pferde stumm. D.h. selbst die stärkste Qual ist ihnen kaum anzumerken.

Der 1. Mai ist traditionell der Auftakt der "Rennsaison" und damit der Startschuss für eine Schau an Ignoranz gegenüber den Bedürfnissen und dem Leid tierlicher Individuen. Wir stellen uns gegen diese Tradition der Pferderennen und gegen Tierausbeutung jeglicher Art!

Kommt mit uns vor die Rennbahn und zeigt Präsenz! Transpis und Schilder können wir zusammen in einer Kunststunde malen. Diese findet am Dienstag, dem 16. April, im Plaque statt.

Wie bei jeder unserer Veranstaltungen gilt: Antiemanzipatorische und diskriminierende Verhaltensweisen werden nicht geduldet. Damit sind alle ausgeladen, die sich rassistisch, antisemitisch, sexistisch, klassistisch, ableistisch, homofeindlich oder trans*feindlich äußern.
