---
id: '266676397353850'
title: Offenes Treffen der tierbefreier*innen Leipzig
start: '2018-12-11 18:00'
end: '2018-12-11 21:00'
locationName: Albertina
address: 'Beethovenstr. 6, 04107 Leipzig'
link: 'https://www.facebook.com/events/266676397353850'
image: null
teaser: null
recurring: null
isCrawled: true
---
