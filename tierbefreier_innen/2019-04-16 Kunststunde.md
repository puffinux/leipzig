---
id: '1207004519465747'
title: Kunststunde
start: '2019-04-16 15:00'
end: '2019-04-16 21:00'
locationName: null
address: Plaque
link: 'https://www.facebook.com/events/1207004519465747/'
image: 56874489_1016730315197927_9087111802797424640_n.jpg
teaser: Am 01. Mai wollen wir  wieder gemeinsam gegen den Aufgalopp an der Pferderennbahn Scheibenholz in Leipzig demonstrieren.   Dafür wollen wir im Vorfeld
recurring: null
isCrawled: true
---
Am 01. Mai wollen wir  wieder gemeinsam gegen den Aufgalopp an der Pferderennbahn Scheibenholz in Leipzig demonstrieren. 

Dafür wollen wir im Vorfeld unsere Forderungen auf Stoff malen! 
Kommt gern am 16.04. ab 15 Uhr im Plaque vorbei.

Für das nötige Zubehör ist gesorgt und ein paar kleine Verköstigungen wird es auch geben. 

Wir freuen uns auf euch!
 