---
id: '2218409205090833'
title: 'Vortrag: Alles, alles muß befreit werden'
start: '2019-03-23 19:00'
end: '2019-03-23 22:00'
locationName: Plaque
address: 'Industriestraße 101, 04229 Leipzig-Plagwitz, Sachsen'
link: 'https://www.facebook.com/events/2218409205090833/'
image: 53884387_999520110252281_196396923026931712_n.jpg
teaser: „Befreiung hört nicht beim Menschen auf“ – Einer der Slogans der Tierbefreiungsbewegung.   Befreiung – auf allen Ebenen – das wollen auch Anarchist*in
recurring: null
isCrawled: true
---
„Befreiung hört nicht beim Menschen auf“ – Einer der Slogans der Tierbefreiungsbewegung. 

Befreiung – auf allen Ebenen – das wollen auch Anarchist*innen. Was liegt also näher, als sich die Überschneidungen beider Bewegungen anzusehen?

Passend zum Titelthema der TIERBEFREIUNG 102 widmet sich der Vortrag unterschiedlichen Aspekten dieser Gemeinsamkeiten. Zwei historische, biografische Beiträge werden von neueren Diskussionssträngen gefolgt. Um in einem weiteren Schritt einen anarcha-veganen Organisationsansatz für die bio-vegane Landwirtschaft zu erkunden. Abschließend werden in einem Ausblick (über)regionale Projekt- bzw. Organisationsvorschläge gemacht.

Es wird vegane Snacks gegen Spende & Getränke an der Bar geben.

