---
id: "3316229771750817"
title: Vortrag in Leipzig "Tierbefreiung unterm Regenbogen"
start: 2019-11-17 14:30
end: 2019-11-17 17:30
address: Familiengarten Schenkendorfstraße 10 04275 Leipzig
link: https://www.facebook.com/events/3316229771750817/
image: 74664672_723004358201858_859968038819069952_n.jpg
teaser: The Vegan Rainbow Project setzt sich für mehr Sichtbarkeit für vegan lebende
  unterdrückte und marginalisierte Gruppen und den gemeinsamen Kampf für di
isCrawled: true
---
The Vegan Rainbow Project setzt sich für mehr Sichtbarkeit für vegan lebende unterdrückte und marginalisierte Gruppen und den gemeinsamen Kampf für die Befreiung aller Lebewesen ein.

Mit ihrem Vortrag wollen sie darauf aufmerksam machen, wie sich Unterdrückungsmechanismen in ihrer Art und Wirkungsweise ähneln. Welche Zusammenhänge bestehen zwischen Sexismus und Speziesismus? Wie legitimiert dies die Objektifizierung von und Gewalt gegen bestimmte Gruppen? Und: welche Auswirkungen hat dies in Zeiten der Klimakrise auf das Leben und Überleben auf unserem Planeten? 

Diese Thematiken werden insbesondere von einer queer-feministischen Perspektive beleuchtet.

Der Veranstaltungsort ist für Menschen im Rollstuhl über den Garteneingang zugänglich.  