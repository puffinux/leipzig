---
id: "1213691765507760"
title: The Plague Dogs - Filmvorführung / Movie Screening & Küfa
start: 2019-10-24 18:00
end: 2019-10-25 03:00
locationName: Plaque
address: Industriestraße 101, 04229 Leipzig-Plagwitz, Sachsen
link: https://www.facebook.com/events/1213691765507760/
image: 72872838_1145346899002934_2813467916937199616_n.jpg
teaser: /// english version below ///  Am Donnerstag, den 24.10., zeigen wir im Plaque
  den Animationsfilm “The Plague Dogs”.  In diesem Film, der auf einem Ro
isCrawled: true
---
/// english version below ///

Am Donnerstag, den 24.10., zeigen wir im Plaque den Animationsfilm “The Plague Dogs”.

In diesem Film, der auf einem Roman von Richard Adams beruht, brechen die zwei Hunde Rowf und Snitter aus einem Tierversuchslabor aus, um dann auf eigene Faust ein besseres Leben zu suchen. Wie zu erwarten, gestaltet sich das allerdings alles andere als einfach. Die Hunde leiden unter den Nachwirkungen der an ihnen durchgeführten Experimente und sehen sich fortwährend mit der Ablehnung und Verfolgung durch Menschen konfrontiert.

Wir möchten mit dieser Filmvorführung weiter auf das derzeit wieder sehr präsente Thema der Tierversuchsindustrie (siehe LPT-Enthüllungen) aufmerksam machen, für die Problematik sensibilisieren und für ein von Grund auf anderes Verhältnis zwischen Menschen und nichtmenschlichen Tieren plädieren.

Im Anschluss kochen wir im Rahmen der Donnerstags-Küfa des Plaque noch für euch, wenn ihr mögt – selbstverständlich vegan.

Weitere Infos: Es handelt sich zwar um einen Zeichentrickfilm, dennoch wird die Gewalt, die im Rahmen von Tierversuchen statt findet, nicht beschönigt.
- Folglich enthält der Film Szenen, die nicht angenehm anzuschauen sind! -
Der Film wird auf Englisch mit deutschen Untertiteln gezeigt werden.

Beginn der Filmvorführung: 18:00
(Küfa & Bar im Anschluss, ab 20:00)

Wie bei jeder unserer Veranstaltungen gilt: Antiemanzipatorische und diskriminierende Verhaltensweisen werden nicht geduldet. Damit sind alle ausgeladen, die rassistisches, antisemitisches, sexistisches, klassistisches, ableistisches, homo-, trans*- oder interfeindliches Gedankengut hegen. 

//////////////////////////////////////////////////////

On thursday, the 24th of October, we will show the animation movie “The Plague Dogs” in the Plaque.

In this movie, which is based on a novel by Richard Adams, two dogs named Rowf and Snitter escape from an animal testing facility, to go look for a better life on their own. As is to be expected, this is far from an easy task. The dogs suffer from the after-effects of the experiments they had to endure, as they constantly face the rejection and pursuit by humans.

With this screening we want to bring further attention to the currently discussed topic of the vivisection industry (see the exposure of LPT). We also want to sensitize for this complex of problems and we strive for a fundamentally different relationship between humans and nonhuman animals.

After the screening, we’ll be cooking for you (Thursday’s Küfa of the Plaque), if you like – everything will be vegan, of course.

Additional information: While this movie is an animated cartoon, the violence that typically occurs in animal testing still is depicted graphically, without any sugarcoating.
- In consequence the movie contains scenes that aren’t pleasant to watch! -
The movie will be shown in English with German subtitles.

Start of the Screening: 18:00
(Küfa & Bar afterwards, starting at 20:00)

As for all our events: Antiemancipatory and discriminating behaviour will not be tolerated. This means that you are not welcome if you're endorsing or practicing racist, antisemitic, sexist, classist, ableist, homo-, trans*- or interhostile ideologies.

/// deutsche Version oben ///