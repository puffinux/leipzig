---
id: '375494449899920'
title: Aufstand oder Aussterben? Die neue Bewegung Extinction Rebellion
start: '2019-02-21 18:00'
end: '2019-02-21 22:00'
locationName: Laden auf Zeit
address: '51  Kohlgarten Straße, 04315 Leipzig'
link: 'https://www.facebook.com/events/375494449899920/'
image: 51405654_980656722138620_7195304010285318144_n.jpg
teaser: 'Vortrag und Diskussion mit Friederike Schmitz und Thomas Frenzel  Aufstand oder Aussterben? Klimakatastrophe, ökologische Krise und die neue Bewegung'
recurring: null
isCrawled: true
---
Vortrag und Diskussion mit Friederike Schmitz und Thomas Frenzel

Aufstand oder Aussterben? Klimakatastrophe, ökologische Krise und die neue Bewegung Extinction Rebellion

Wenn sich Politik und Wirtschaft nicht in kürzester Zeit drastisch verändern, steuert die Erde auf eine katastrophale Erwärmung von mehr als drei Grad Celsius zu. Das bedeutet nicht nur Tod, Armut und Elend für hunderte Millionen Menschen und Tiere, sondern bedroht unsere ganze Zivilisation. Das sechste große Artensterben ist bereits im vollem Gange. Heutige Kinder und Jugendliche könnten in ihrer Lebenszeit die Vernichtung fast aller Ökosysteme erleben.

Um das zu verhindern, reichen kleine Konsumveränderungen nicht aus: Wir müssen Grundprinzipien unserer Gesellschaft hinterfragen. Die Regierungen sind weit davon entfernt, die nötigen Schritte zu unternehmen.

Es ist daher Zeit für eine massenhafte, gewaltfreie Rebellion. Die Bewegung „Extinction Rebellion“ hat
im Herbst 2018 mit beeindruckenden Aktionen in Großbritannien angefangen. Jetzt bilden sich auch Gruppen überall in Deutschland. Wir sind am Zug!

Nach dem Vortrag wird es Zeit geben bei Getränken und Snacks erste Eindrücke zu teilen und gemeinsam über den Vortrag zu reden.