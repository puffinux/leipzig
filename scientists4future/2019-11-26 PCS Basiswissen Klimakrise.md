---
id: "1412422665548274"
title: "PCS: Basiswissen Klimakrise"
start: 2019-11-26 17:15
end: 2019-11-26 18:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/1412422665548274/
image: 76769625_444856006449882_428000186900414464_n.jpg
teaser: "Titel: Basiswissen Klimakrise Referent*in: Scientists 4 Future Leipzig Ort:
  NSG SR 202, Hauptcampus Zeit: 17.15-18.45 Uhr  Das 1x1 der Klimakrise - er"
isCrawled: true
---
Titel: Basiswissen Klimakrise
Referent*in: Scientists 4 Future Leipzig
Ort: NSG SR 202, Hauptcampus
Zeit: 17.15-18.45 Uhr

Das 1x1 der Klimakrise - erklärt von Scientist for Future

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findet Ihr hier: https://studentsforfuture.info/ortsgruppe/leipzig/#calendar 