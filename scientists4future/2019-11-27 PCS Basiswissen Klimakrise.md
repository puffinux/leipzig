---
id: "744079026071735"
title: "PCS: Basiswissen Klimakrise"
start: 2019-11-27 17:15
end: 2019-11-27 18:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/744079026071735/
image: 76651665_444850906450392_1730567959941742592_n.jpg
teaser: "Titel: Basiswissen Klimakrise Referent*in: Scientists 4 Future Leipzig Ort:
  HS 4 / Sunita Narain (Indien) Zeit: 17.15-18.45 Uhr  Das 1x1 der Klimakris"
isCrawled: true
---
Titel: Basiswissen Klimakrise
Referent*in: Scientists 4 Future Leipzig
Ort: HS 4 / Sunita Narain (Indien)
Zeit: 17.15-18.45 Uhr

Das 1x1 der Klimakrise - erklärt von Scientist for Future

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findet Ihr hier: https://studentsforfuture.info/ortsgruppe/leipzig/#calendar 