---
id: "2811515792213421"
title: "PCS: Basiswissen Klimakrise"
start: 2019-11-28 17:15
end: 2019-11-28 18:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/2811515792213421/
image: 78600039_444853299783486_2781991355755266048_n.jpg
teaser: "Titel: Basiswissen Klimakrise Referent*in: Scientists 4 Future Leipzig Ort:
  HS 20 / Ken Saro-Wiwa (Nigeria) Zeit: 17.15-18.45 Uhr  Das 1x1 der Klimakr"
isCrawled: true
---
Titel: Basiswissen Klimakrise
Referent*in: Scientists 4 Future Leipzig
Ort: HS 20 / Ken Saro-Wiwa (Nigeria)
Zeit: 17.15-18.45 Uhr

Das 1x1 der Klimakrise - erklärt von Scientist for Future

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findet Ihr hier: https://studentsforfuture.info/ortsgruppe/leipzig/#calendar 