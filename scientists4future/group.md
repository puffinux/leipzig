---
name: Scientists 4 Future Leipzig
website: https://www.scientists4future.org/
email: s4f-leipzig@gmx.de
scrape:
  source: facebook
  options:
    page_id: scientists4futureLeipzig
---
Wir sind eine Untergruppe der Scientists 4 Future Bewegung bestehend aus Wissenschaftslern aus Leipzig und Umgebung. Wir unterstützen die Anliegen der Fridays for Future Bewegung.