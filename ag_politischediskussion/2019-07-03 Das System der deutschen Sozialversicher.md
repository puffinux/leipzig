---
id: '2227559754006711'
title: Das System der deutschen Sozialversicherungen
start: '2019-07-03 18:00'
end: '2019-07-03 21:00'
locationName: UNIVERSITÄT LEIPZIG
address: 'Augustusplatz 10, 04109 Leipzig'
link: 'https://www.facebook.com/events/2227559754006711/'
image: 65309840_888413381511856_959374764827213824_n.jpg
teaser: 'Sozialversicherungspflichtig beschäftigt? Herzlichen Glückwunsch!  Denn wenn man davon absieht,  - wie viel Geld man für wie viel und was für eine Sor'
recurring: null
isCrawled: true
---
Sozialversicherungspflichtig beschäftigt?
Herzlichen Glückwunsch!

Denn wenn man davon absieht, 
- wie viel Geld man für wie viel und was für eine Sorte Arbeit ‚in seinem Job‘ überhaupt verdient,
- dass man die Hälfte des verdienten Geldes wieder abdrücken kann, bevor man sie überhaupt aufs Konto bekommen hat,
- dass die Leistungen, auf die man sich ein Recht erwirbt, so begrenzt sind, dass einem ‚private Vorsorge‘ schon lange nicht mehr erspart bleibt;
und wenn man es gleichzeitig für ganz normal hält,
- dass zur Erwerbsarbeit solche ‚Wechselfälle‘ wie Entlassung, Krankheit und Alter einfach irgendwie dazugehören, die alle immer dasselbe, nämlich Einkommensverlust bedeuten,
- dass man in den ‚guten Zeiten‘ nie so viel Geld beiseitelegen kann, damit man zwischendurch oder hinterher davon leben könnte,
- dass es also nicht geht, privat ‚von seiner Hände Arbeit‘ über die Runden zu kommen,
dann kann man es tatsächlich für ein Glück halten, dass man selbst zu denen gehört, um die sich der Staat mit seinen Versicherungen kümmert.
Das erspart einem auch jeden Gedanken daran, warum er das und wie er das tut und was das alles über das wunderbare freiheitlich-marktwirtschaftliche System des Arbeitens und Arbeiten-lassens verrät, das ohne eine gehörige Portion staatlich organisierter Zwangssolidarität offensichtlich nicht auskommt.
Deswegen soll es auf unserer Veranstaltung genau darum gehen:

Das System der deutschen Sozialversicherungen
Von den notwendigen und wenig bekömmlichen Leistungen des Sozialstaates für die abhängig Beschäftigten
