---
id: "593177424553848"
title: Mietpreisexplosion & Wohnungsnot
start: 2019-12-03 18:00
end: 2019-12-03 21:00
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/593177424553848/
image: 76182912_1000297023656824_443134539254988800_n.jpg
teaser: Mietpreisexplosion & Wohnungsnot - Die Wohnungsfrage im Kapitalismus  Es
  herrscht akute Wohnungsnot. Wie immer mangelt es nicht an guten Vorschlägen,
isCrawled: true
---
Mietpreisexplosion & Wohnungsnot - Die Wohnungsfrage im Kapitalismus

Es herrscht akute Wohnungsnot. Wie immer mangelt es nicht an guten Vorschlägen, wie diesem Problem zu begegnen wäre: Aktivisten wettern gegen Auswüchse der Spekulation und Preistreiberei, die man politisch allemal verbieten oder bremsen könnte; progressive Parteien fordern einen Mietendeckel und sogar Enteignungen. Dann wären die Mieten vielleicht wieder bezahlbar. Eigentümergesellschaften und ihre freidemokratischen Interessenvertreter können vor so etwas nur warnen: Wenn man den Eigentümern Vorschriften macht und Fesseln beim Mietpreis anlegt, dann lohnen sich Investitionen in neue Wohnungen nicht mehr und unterbleiben deswegen; dann ist der Wohnraum knapp und dann – da kennen sie sich aus – steigen die Mieten doch nur noch weiter. Das Gegenteil – Abräumen der Schranken für ihr Geschäft – würde helfen, dann klappt’s vielleicht auch wieder mit dem Wohnen.
Man kann gar nicht sagen, in diesem Disput hätte eine Seite recht und die andere nicht. Recht haben sie beide in dem Sinne, dass genau so die politische Betreuung der Wohnungsfrage im Kapitalismus geht: Ermächtigung und Beschränkung als Hebel der Politik. Unrecht haben sie insofern, als das ausgerufene Problem so garantiert nicht ‚gelöst‘ wird. Denn wo Grund und Boden durch die Macht des Staates zu privatem Eigentum gemacht sind und als stattliche Einkommensquelle lizenziert werden, sind die Ansprüche des Grundeigentums so unhintergehbar wie unvereinbar mit den Wohnansprüchen eines in der Erwerbsarbeit eingehausten Volkes und den Erträgen aus dieser seiner Einkommensquelle.
Die ‚Wohnungsfrage‘ ist deswegen so alt wie der Kapitalismus selbst und ist als solche überhaupt nicht ‚zu lösen‘.
Allen erregten Gemütern, die das – mindestens für ihren Kiez – unmöglich glauben können, und allen, die ansonsten an einer Kritik der politischen Ökonomie des Grundeigentums interessiert sind, können wir das beweisen.

Vortrag und Diskussion, Dienstag 3.12., 18 Uhr, Hörsaal 8, Uni Leipzig 