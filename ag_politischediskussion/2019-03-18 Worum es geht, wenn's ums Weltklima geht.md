---
id: '426333444777706'
title: 'Worum es geht, wenn''s ums Weltklima geht'
start: '2019-03-18 16:00'
end: '2019-03-18 18:00'
locationName: UNIVERSITÄT LEIPZIG
address: 'Augustusplatz 10, 04109 Leipzig'
link: 'https://www.facebook.com/events/426333444777706/'
image: 53766582_825747081111820_1732720344147427328_n.jpg
teaser: 'An Greta Thunberg und ihre jugendlichen Follower  Worum es geht, wenn''s ums Weltklima geht: Staatenkonkurrenz um Energie  Ihr streikt und protestiert'
recurring: null
isCrawled: true
---
An Greta Thunberg und ihre jugendlichen Follower

Worum es geht, wenn's ums Weltklima geht:
Staatenkonkurrenz um Energie

Ihr streikt und protestiert gegen den Klimawandel. Ihr werft der Politik Inkonsequenz, Untätigkeit, Heuchelei bei der Lösung des Menschheitsproblems Nr.1 vor; und „den Erwachsenen“ überhaupt Ignoranz gegenüber den düsteren Aussichten für die „nachfolgenden Generationen“.

Nur: Stimmen denn diese Einwände?

Wer ist eigentlich dieses eigenartig kollektive Subjekt „Menschheit“, von dem man immer nur hört, wenn es „bedroht“ sein soll?
Dass Inselgruppen absaufen, weil Polkappen abschmelzen: Macht das aus den Bewohnern der Fidschiinseln und Reedern, die sich neue Seewege erschließen, wirklich gleichermaßen Betroffene einer einzigen großen Gemeinschaft?
Und wenn es schon um die Bedrohung der Menschheitszukunft gehen soll: Wer bedroht sie? Womit eigentlich und warum? Auch wieder „der Mensch“, „wir alle“ und „jeder und jede Einzelne“? Oder doch mehr „die Politik“ oder „die Erwachsenen“? Oder ist das alles ein und dasselbe?
Ist es nicht bemerkenswert, dass die Höchstwerte Menschheit und Klima, in deren Namen ihr gegen die Politik antretet, deren eigene Werte sind? Und gibt es nicht zu denken, dass die Politiker eurem Ruf nach Schutz von Menschheit und Klima regelmäßig Recht geben – um sich dann im Namen ihrer Verantwortung für Menschheit und Klima genauso regelmäßig jede wirkliche Einmischung in ihre Politik zu verbitten?

Was die politisch Zuständigen dann im Namen der allseits geteilten Sorge um das Weltklima unter dem Titel Klimapolitik betreiben, das ist ihre nationale Energiepolitik, die gerade für die wichtigsten und mächtigsten unter ihnen immer eine Frage weltweiter kapitalistischer Geschäftsmöglichkeiten und zugleich strategischer Sicherheit und Überlegenheit gegenüber anderen Nationen ist.

Von wegen also „Untätigkeit“!

Sie konkurrieren um den Zugriff auf alte und neue Energiequellen, mischen sich dafür in die Energiepolitik ihrer Konkurrenten ein und versuchen umgekehrt, jede Einmischung anderer Mächte in die eigene Energiebewirtschaftung abzuwehren. Wenn dafür „Weltklima“ nicht die absolut passende Überschrift ist!

Darüber wollen wir mit euch und allen anderen Interessierten diskutieren.

Ort: Uni Leipzig, Seminargebäude Raum 102
Datum: 18.03
Uhrzeit: 16:00