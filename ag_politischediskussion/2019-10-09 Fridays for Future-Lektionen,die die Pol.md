---
id: '1639205232880634'
title: 'Fridays for Future-Lektionen,die die Politik dem Protest erteilt'
start: '2019-10-09 18:00'
end: '2019-10-09 21:00'
locationName: UNIVERSITÄT LEIPZIG
address: 'Augustusplatz 10, 04109 Leipzig'
link: 'https://www.facebook.com/events/1639205232880634/'
image: 71265386_956996361320224_7471881869580042240_n.jpg
teaser: 'FRIDAYS FOR FUTURE Lektionen, die die Politik dem Protest erteilt  • Greta Thunberg droht: „Wir werden euch nicht davon kommen lassen!“ und macht die'
recurring: null
isCrawled: true
---
FRIDAYS FOR FUTURE
Lektionen, die die Politik dem Protest erteilt

• Greta Thunberg droht: „Wir werden euch nicht davon kommen lassen!“ und macht die weltweite Politiker-Riege am UNO-Hauptsitz verantwortlich für den Klimawandel. Aber ausgerechnet diese Politiker loben weiterhin die „Fridays For Future“-Bewegung für ihr Engagement. Was ist da los?
• Angela Merkel begrüßt es sehr, „dass junge Menschen demonstrieren und uns sozusagen ermahnen, schnell etwas für den Klimaschutz zu tun.“ Gleichzeitig beharrt die Kanzlerin darauf, dass SIE die Klimapolitik macht und zwar nur „das, was möglich ist.“ Damit erklärt Merkel die Forderungen der Protestierenden für praktisch irrelevant - aber lobt den Protest weiterhin. Das ist schon frech.
• Als Jugend“ erfährt die „Fridays For Future“-Bewegung ganz viel Respekt. Aber eben auch nur so lange, wie sie sich als Jugend benimmt. Die Politik besteht darauf, dass nicht sie sondern die Schüler ihre Lehren aus dem Protest ziehen sollen; der Protest ist „in Wahrheit das wichtigste Fach und eine wunderschöne Initiative“ (ital. Bildungsminister). Und was soll die Bewegung lernen? Die Politik lässt sich auf Nichts verpflichten.
• Bei jeder Zurückweisung der Proteste deutet die Politik an, für welche Gruppen, Interessen und Gesichtspunkte der Nation sie Verantwortung übernimmt, damit alle eine „Zukunft“ haben: „Arbeitsplätze“, „internationaler Wettbewerb“,„Sozialverträglichkeit“… So drückt die Politik schließlich aus, dass die Welt etwas ganz anderes ist als ein Stück vernachlässigte Natur. Nämlich ein globaler Markt, auf dem es in mehrfachem Sinn ums Geld geht, eingerichtet und aufrechterhalten durch Staaten, die mit ihrer Gewalt - in Konkurrenz gegeneinander, deswegen manche mit überhaupt nicht umwelt- und klimafreundlichen Atomwaffen - für die dazu passende Ordnung Sorge tragen. 

Die freche  Vereinahmung wie Zurückweisung des Klimaprotestes werfen ein ziemlich zweifelhaftes Licht auf die hohen Werte wie Klima, Zukunft, Planet, Erde; und genauso ein sehr schlechtes Licht auf den demokratischen Stellenwert menschlicher Interessen nach gesunden Lebensbedingungen. Darüber möchten wir diskutieren.

Universität Leipzig, Seminargebäude 111
Mittwoch 9.10., 18:00