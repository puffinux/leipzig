---
id: '1493200920815023'
title: Der demokratische Dialog in den Sozialen Medien
start: '2019-04-25 18:00'
end: '2019-04-25 21:00'
locationName: UNIVERSITÄT LEIPZIG
address: 'Augustusplatz 10, 04109 Leipzig'
link: 'https://www.facebook.com/events/1493200920815023/'
image: 57358066_845765139110014_4794091793429823488_n.jpg
teaser: 'Der demokratische Dialog in den Sozialen Medien: Politiker gehen online, Bürger erteilen Noten.  Nicht nur Individuen & Firmen, auch Politiker nutzen'
recurring: null
isCrawled: true
---
Der demokratische Dialog in den Sozialen Medien:
Politiker gehen online, Bürger erteilen Noten.

Nicht nur Individuen & Firmen, auch Politiker nutzen Social Media zur Werbung in eigener Sache: Auf Twitter, Facebook, Instagram sammeln sie Millionen Follower, Freunde, Abonnenten; und für viele Bürger ist das Netz der neue Stammtisch, wo sie den Mächtigen kräftig die Meinung sagen. Das ergibt einen bemerkenswert absurden Dialog. Die einen setzen qua Amt in die Welt, was man ‚Fakten‘ nennt, die anderen haben sich im wirklichen Leben danach zu richten – als User aber hat jeder ‚etwas zu sagen‘. Die Führer der Nation verkünden, was für tolle Sachen sie heute wieder gemacht haben, der Bürger gibt Noten, ob sie ihre Sache gut gemacht haben – Herrschaft und Volk begegnen sich wie von Du und Du. Diese Community ist für aktive Nutzer und moderne Medienexperten der Beginn eines echten Zwiegesprächs ‚auf Augenhöhe‘; für andere, v.a. Politiker und seriöse Presseorgane in Europa, ist Social Media ein Medium von ‚Populisten‘, das sich aufs Niveau des ‚Volksmunds‘ begibt und zur Verrohung der politischen Streitkultur führt. - Worüber und entlang welcher Maßstäbe man sich da eigentlich verständigt: Darüber wollen wir diskutieren.
1.) Am Fall des US-Präsidenten @realDonaldTrump, der sein Programm ‚America first‘ täglich in 140 Zeichen verpackt. 2.) Am Fall des Innenministers Seehofer, der für seinen Merksatz von der ‚Migration als Mutter aller Probleme‘ einen Shitstorm erntet. 3.) Am Fall des Grünen-Chefs Habeck, dessen öffentliches ‚Bye bye, Twitter & Facebook‘ eine Fluch-oder-Segen-Debatte auslöst.  

Trump, Seehofer, Habeck: Fallstudien zum interaktiven Stammtisch von Herrschaft & Volk

Ort: Universität Leipzig, Hörsaal 11
Wann: 25.04., 19 Uhr