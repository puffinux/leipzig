---
id: '2279444568974603'
title: YOGA for everyone
start: '2019-06-10 19:00'
end: '2019-06-10 20:00'
locationName: Das Japanische Haus e. V.
address: 'Eisenbahnstr.113b, 04315 Leipzig'
link: 'https://www.facebook.com/events/2279444568974603/'
image: 60137788_2250271091695141_228382815790563328_n.jpg
teaser: 'PEOPLE!! Important information!  Classes for the next 2 weeks are CANCELED, next week for family reasons and the second because of the 1st of May holi'
recurring: null
isCrawled: true
---
PEOPLE!! Important information!

Classes for the next 2 weeks are CANCELED, next week for family reasons and the second because of the 1st of May holiday.
In addition, as I have already mentioned, starting in May classes will change from Wednesday to Monday, at the same time (7:00 p.m.), so our next meet up will be on Monday the 6th.

Wishing you all a happy Easter!!