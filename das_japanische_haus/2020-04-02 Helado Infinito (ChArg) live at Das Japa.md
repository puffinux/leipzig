---
id: "485347872098415"
title: Helado Infinito (Ch/Arg) live at Das Japanische Haus!
start: 2020-04-02 20:00
end: 2020-04-02 23:00
locationName: Das Japanische Haus e. V.
address: Eisenbahnstr.113b, 04315 Leipzig
link: https://www.facebook.com/events/485347872098415/
image: 79752793_2526915634253687_6522686014035591168_o.jpg
teaser: From Chile and Argentina, Helado Infinito for the first time in
  Leipzig!   Helado Infinito is an alternative pop duo formed by Loreta Neira
  Ocampo (Ch
isCrawled: true
---
From Chile and Argentina, Helado Infinito for the first time in Leipzig! 

Helado Infinito is an alternative pop duo formed by Loreta Neira Ocampo (Chile) and Victor Borgert (Argentina) in March of 2016. Their music mix pop with folk, hip-hop and electronic music in songs that talk about existentialism, love, travel, and relationships.
The project started in La Plata (Argentina) and grew on a backpacking trip around Mexico and Europe, journey that developed their itinerant and independent way of playing and record their music.
Their two records "Canciones Dispersas" (2017) and “El Movimiento del Error” (2019) were fully composed, recorded, mixed, and mastered by the band members with multiple collaborations of their friends around the world, making this way of work a signature on their music.
Helado Infinito has performed in more than 20 countries and they are currently touring around Europe. 

"Canciones Dispersas" (First album, 2017): https://www.youtube.com/watch?v=6Z6A-n9Iw2A 

"El Movimiento del Error" (Second album, 2019): https://www.youtube.com/watch?v=jD55zDZG5bY 