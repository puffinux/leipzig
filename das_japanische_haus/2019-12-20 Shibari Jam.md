---
id: "523440771569869"
title: Shibari Jam
start: 2019-12-20 20:00
end: 2019-12-20 22:30
locationName: Das Japanische Haus e. V.
address: Eisenbahnstr.113b, 04315 Leipzig
link: https://www.facebook.com/events/523440771569869/
image: 74389093_142498023784581_1411194994970066944_o.jpg
teaser: English below...  Wir werden kreativ mit Seilen und Knoten. Ergänzung zum
  Shibari-Workshop. Freier Übungsabend für alle, die sich für Knotenkunst und
isCrawled: true
---
English below...

Wir werden kreativ mit Seilen und Knoten. Ergänzung zum Shibari-Workshop. Freier Übungsabend für alle, die sich für Knotenkunst und Seilfesselei interessieren. Seile, warme Decke zum Draufsetzen, bequeme Klamotten und Übungspartner gern mitbringen (aber kein Muss). Das ist eine lockere, geschützte Runde zum Ausprobieren, Fehler machen, sich gegenseitig helfen.

Auf Spendenbasis, Empfehlung 5-10 Euro.

Fragen & Antworten

Muss man Shibari schon können?
=> Nein, Ihr könnt auch kommen, wenn Ihr noch nie ein Seil in der Hand hattet, aber neugierig seid.

Braucht man eine Partnerin, einen Partner?
=> Idealerweise schon, vielleicht findet die oder der sich am Abend ja. Garantieren können wir das nicht, aber oft klappt das ganz gut.

Soll man was mitbringen?
=> Wenn ihr habt: gern eigene Seile, Yogamatte, Decke, dicke Socken, Partner. Leih-Seile auch vorhanden.

Let's practise some knots you might have learned in the Shibari workshop. This is an open practise for everyone interested in the art of rope tying and Shibari. Welcome to bring your ropes, warm blanket to sit on, comfy clothes and a partner - but not necessary. It's a safe and friendly gathering for trying out, making mistakes and helping one another.

Donation based, recommendation 5-10 euro.

Q&A

Do you have to know how to tie?
=> No. You can attend even if you've never held a rope in your hand, but you are curious about it.

Do you need a partner?
=> Ideally yes. You might find someone to practise with, but we cannot guarantee it.

Should you bring something?
=> If you have: ropes, yoga mat, blanket, warm socks, partner. We have ropes to lend you.