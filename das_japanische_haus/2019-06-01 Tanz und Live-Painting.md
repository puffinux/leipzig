---
id: '2267209609999555'
title: Tanz und Live-Painting
start: '2019-06-01 19:00'
end: '2019-06-01 19:30'
locationName: Das Japanische Haus e. V.
address: 'Eisenbahnstr.113b, 04315 Leipzig'
link: 'https://www.facebook.com/events/2267209609999555/'
image: 61180039_2274070059315244_2473508353377042432_n.jpg
teaser: 'Das Japanische Haus freut sich sehr darüber, euch am Samstag, dem 1. Juni, zu einem ganz besonderen Event einzuladen.  Es wird eine künstlerische Perf'
recurring: null
isCrawled: true
---
Das Japanische Haus freut sich sehr darüber, euch am Samstag, dem 1. Juni, zu einem ganz besonderen Event einzuladen. 
Es wird eine künstlerische Performance zu sehen sein, in welcher zwei Künstlerinnen durch Tanz und Live-Painting moderne (japanische) Weiblichkeit in bewegender Art und Weise zu Ausdruck bringen werden. (Dauer: 30 Minuten).

Profile der Künstlerinnen:

silsil (シルシル):
silsils Arbeiten sind tief geprägt durch ihre Auseinandersetzung mit Emotionalität und Weiblichkeit.
Ihre Improvisations-Kunst hat die japanische Künstlerin und Malerin schon an verschiedenen Orten auf der ganzen Welt aufgeführt, darunter auf Kunstmessen in Italien, Shanghai, New York, Taiwan, Korea und Japan. Die an den jeweiligen Ort empfundenen Eindrücke bringt sie live vor Ort in berauschende Formen und Farben.
<Facebook>　https://www.facebook.com/silsil.ism
<instagram>　https://www.instagram.com/silsilism/ 

Hapico dainagon ( 大納言はぴ子):
Die in Australien ausgebildete Burlesque-Tänzerin führt ihre Kunst derzeit vorallem in Japan aber auch im Ausland auf, bspw. 2015 in „der Heimat des Burlesque“ Hamburg. Ihre Show, die sie „Comic Burlesque“ nennt, basiert vor allem auf der Komödie, welche als der Ursprung des Burlesque bezeichnet werden kann. Hierdurch hebt sie sich klar von der sonst in diesem Genre üblichen Orientierung am „Striptease“ ab.
<Facebook>　https://www.facebook.com/happydainagon/ 
<instagram>　https://www.instagram.com/dainagonhapico/


ダンスとライブアートによりその場で作品が制作される、
心を揺さぶる（日本の）女性性の表現を体感ください。
観客と共に作り上げるアートパフォーマンス作品です。
（30分）

silsil（シルシル）プロフィール：
女性をモチーフとしたエモーショナルな作品。
また、即興絵画制作を得意とし、世界各国で招待作家として描く日本のペインターで現代アーティスト。 日本はもとより、イタリア・上海・NY・台湾・韓国などのアートフェアに出展。 受け取った感覚を色彩や造形に置き換えその場で作り上げる作品は、国を問わず観客から大きな支持を得る。

大納言はぴ子（Hapico dainagon）プロフィール：
オーストラリアでバーレスクを学び現在は日本、海外で活動中。 2015年にはドイツ・ハンブルクにあるバーレスクの老舗The home of burlesqueに出演するなど世界的に活躍するバーレスクダンサー。 「コミックバーレスク」と自ら称する演目は混同される「ストリップ」とは一線を画しバーレスクの原点である喜劇を含んだショーとして凝った演出がなされる。