---
id: '2247172162011060'
title: Japanisch-Deutsches Tandem-Treffen
start: '2019-05-17 16:00'
end: '2019-05-17 18:00'
locationName: Das Japanische Haus e. V.
address: 'Eisenbahnstr.113b, 04315 Leipzig'
link: 'https://www.facebook.com/events/2247172162011060/'
image: 57104242_2210488712340046_1482443090751913984_n.jpg
teaser: 'Japanisch-Deutsches Tandem-Treffen  Für alle, die Japanisch oder Deutsch lernen machen wir am Freitag im das Japanische Haus ein Tandem-Treffen. Beim'
recurring: null
isCrawled: true
---
Japanisch-Deutsches Tandem-Treffen

Für alle, die Japanisch oder Deutsch lernen machen wir am Freitag im das Japanische Haus ein Tandem-Treffen. Beim gemütlichen Beisammensitzen kann alltägliche Konversation geübt werden. Es ist auch eine gute Gelegenheit, um Fragen zu stellen, Unklarheiten im Bezug auf z.b. Grammatik zu klären, oder einfach um umgangssprachliche Redewendungen jenseits des Lehrbuchs kennen zu lernen. Das Ganze soll ca. 2 Stunden gehen, eine Stunde auf Deutsch und eine Stunde auf Japanisch.

Wir freuen uns auf euch!

Jeden Freitag　18:00 - 20:00 Uhr

日独タンデム会

ドイツ語又は日本語を勉強してる皆さん！
毎週金曜日、ドイツ語と日本語の学習会を開催します。
お茶を飲みながら日常の会話の練習が出来ます。
または、文法の分からないところについて質問したり、教科書に書かれていない口語を学ぶ機会です。
一時間ドイツ語で、一時間日本語で話しましょう！

お気軽にご参加ください！

毎週金曜日　18時～20時