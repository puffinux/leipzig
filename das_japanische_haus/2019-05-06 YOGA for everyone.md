---
id: '287621558826477'
title: YOGA for everyone
start: '2019-05-06 19:00'
end: '2019-05-06 20:00'
locationName: Das Japanische Haus e. V.
address: 'Eisenbahnstr.113b, 04315 Leipzig'
link: 'https://www.facebook.com/events/287621558826477/'
image: 58377098_2212822265440024_8354564470009233408_n.jpg
teaser: 'PEOPLE!! Important information!  Classes for the next 2 weeks are CANCELED, next week for family reasons and the second because of the 1st of May holi'
recurring: null
isCrawled: true
---
PEOPLE!! Important information!

Classes for the next 2 weeks are CANCELED, next week for family reasons and the second because of the 1st of May holiday.
In addition, as I have already mentioned, starting in May classes will change from Wednesday to Monday, at the same time (7:00 p.m.), so our next meet up will be on Monday the 6th.

Wishing you all a happy Easter!!