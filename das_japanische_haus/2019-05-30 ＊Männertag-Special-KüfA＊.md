---
id: '2429640017070932'
title: ＊"Männertag"-Special-KüfA＊
start: '2019-05-30 13:00'
end: '2019-05-30 20:00'
locationName: Das Japanische Haus e. V.
address: 'Eisenbahnstr.113b, 04315 Leipzig'
link: 'https://www.facebook.com/events/2429640017070932/'
image: 61420361_2275658052489778_7996359825246126080_n.jpg
teaser: 'Wer einen etwas anderen "Männertag" (nicht nur mit Männern zusammen) genießen möchte, ist herzlich eingeladen zu unserer Spezial-KüfA ins Japanische H'
recurring: null
isCrawled: true
---
Wer einen etwas anderen "Männertag" (nicht nur mit Männern zusammen) genießen möchte, ist herzlich eingeladen zu unserer Spezial-KüfA ins Japanische Haus zu kommen. 
Die KüfA wird es diesmal schon früher (ab 13:00 Uhr) und in etwas anderer Gestalt geben! 
Kommt vorbei und lasst euch überraschen. Wir freuen uns auf euch! 