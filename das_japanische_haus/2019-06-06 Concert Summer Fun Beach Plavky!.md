---
id: '629023340908349'
title: Concert Summer Fun Beach Plavky!
start: '2019-06-06 20:00'
end: '2019-06-06 23:00'
locationName: Das Japanische Haus e. V.
address: 'Eisenbahnstr.113b, 04315 Leipzig'
link: 'https://www.facebook.com/events/629023340908349/'
image: 61925444_438830016966664_3001317103425814528_n.jpg
teaser: oh awesome! there is a new positivity on the way and it called PLAVKY!  someone said it sounds like Mac DeMarco crushing too many beers in a czech pup
recurring: null
isCrawled: true
---
oh awesome! there is a new positivity on the way and it called PLAVKY!

someone said it sounds like Mac DeMarco crushing too many beers in a czech pup. sounds fine for us!

but maybe check yo'self:
https://www.youtube.com/watch?v=abyHm0-Ur1U&frags=pl%2Cwn

keep enjoy the weather, jump in the water and into our music with us xxx 


