---
id: "675538589604722"
title: EXTRA-KüfA am Mittwoch - Curry mit Gemüse der AnnaLinde
start: 2019-11-06 19:00
end: 2019-11-06 23:00
locationName: Das Japanische Haus e. V.
address: Eisenbahnstr.113b, 04315 Leipzig
link: https://www.facebook.com/events/675538589604722/
image: 75398086_2568086803246900_7691631375903883264_n.jpg
teaser: "Studenten der Uni Kassel werden für euch mit Gemüse von der AnnaLinde ein
  leckeres Curry kochen!  Wir freuen uns, wenn viele kommen! : )"
isCrawled: true
---
Studenten der Uni Kassel werden für euch mit Gemüse von der AnnaLinde ein leckeres Curry kochen! 
Wir freuen uns, wenn viele kommen! : )