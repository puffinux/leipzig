---
id: "1481795285310500"
title: Küche für Alle / ごはんの会 / Kitchen for all
start: 2019-12-26 18:30
end: 2019-12-26 22:00
locationName: Das Japanische Haus e. V.
address: Eisenbahnstr.113b, 04315 Leipzig
link: https://www.facebook.com/events/1481795285310500/
image: 78607981_2654577977931115_8009487238167527424_n.jpg
teaser: Jeder in Leipzig ist willkommen in unserem Haus! Genießt mal bitte unseren
  Raum und Speisen.  Ab 18 Uhr beginnen wir zu kochen(Vegan Gericht), ab 20 U
isCrawled: true
---
Jeder in Leipzig ist willkommen in unserem Haus! Genießt mal bitte unseren Raum und Speisen.

Ab 18 Uhr beginnen wir zu kochen(Vegan Gericht), ab 20 Uhr steht das Essen auf dem Tisch. Komm einfach mal vorbei.

– – –

Everyone in leipzig is welcome to our place! Please enjoy our space and foods. We’ll start cooking(Vegan food) from 6 p.m. and serving from 8 p.m. If you are interested in, just come and join us.

– – –

どなたでも参加できる「ごはんの会」です。18時からみんなで料理を作ります。20時から食事が始まります。投げ銭制です。気軽に遊びに来てください！

– – –

누구라도 참가할 수있는 「밥의 모임 '입니다. 18 시부 터 모두 요리를 만듭니다. 20 시부 터 식사가 시작됩니다. 부담없이 놀러 오세요!
– – –

这是任何人都可以参加的“大米派对”。 每个人都会从18:00开始做饭。 餐点从20:00开始。 请随时访问！

– – –

أهلاً بكم في البيت الياباني نرحب بالجميع هنا في مكانناو نرجو أن تستمتعو بالمكان والطعام, نبدأ الطبخ الساعة 6 مساءً, ويبدأ تقديم الطعام الساعة 8 مساءً. نرجو أن تشاركونا!

– – –

هر کس در لایپزیگ به جای ما خوش آمدید! لطفا فضای ما، موسیقی و غذاهای لذت ببرید. ما شروع به پخت و پز از 18:00 و خدمت به از 20:00 اگر شما علاقه مند هستند، فقط می آیند و به ما بپیوندید

– – –

შაბათს გეპატიჟებათ ყველას Das Japanische Haus ვინც ლაიფციგში ცხოვრობს. დატკბით ჩვენი ადგილით და კერძებით. 18:00 საათიდან ვიწყებთ ერთად საჭმლის კეთებას და 20:00 საათიდან ერთად დავაგემოვნებთ მზა კერძს. შემოგვიარე Das Japanische Haus-ში. Eisenbahnstrasse 113b 