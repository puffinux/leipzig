---
id: '534273883767654'
title: Exhibition
start: '2019-04-27 19:30'
end: '2019-05-01 17:00'
locationName: Das Japanische Haus e. V.
address: 'Eisenbahnstr.113b, 04315 Leipzig'
link: 'https://www.facebook.com/events/534273883767654/'
image: 58380450_2221025594619691_3980381007655206912_n.jpg
teaser: 'No Title  Exhibition : Mayuko Yamamoto. Kenji Kagenishi. Hyukjun Kwon   27.04.2019(Sam) -  01.05.2019(Mit)  15:00-17:00  Opening Day : 27.04.2019    1'
recurring: null
isCrawled: true
---
No Title

Exhibition : Mayuko Yamamoto. Kenji Kagenishi. Hyukjun Kwon

 27.04.2019(Sam) -  01.05.2019(Mit)  15:00-17:00

Opening Day : 27.04.2019    19:30-
Talk : TRITVS
Performance : Hiroko Iwai

Zeit ist etwas, das bei allen Veränderungen und unveränderten Dingen der Welt aufrechterhalten wird, und Zeit ist auch ein Berührungspunkt zwischen Mensch und Außenwelt. Zum Beispiel Gegenwart sehe, höre und fühle ich die Außenwelt. Sie ist mit Vergangenheit und auch Zukunft verknüpft. Es ist auch möglich, die Zeit als die Zeit zu definieren, zu der drei Arten von Aspekten wie Gegenwart, Vergangenheit und Zukunft als Berührungspunkt zwischen einer Person und der Welt angezeigt werden. Die Frage, wo man sich auf die letzten drei, die Gegenwart, die Vergangenheit und die Zukunft konzentrieren soll, ist eines der wichtigsten Themen der Zeit. In den buddhistischen Schriften Indiens werden diese drei Ordnungen fast immer als Vergangenheit, Zukunft und Gegenwart bezeichnet, was darauf hinweist, dass die Gegenwart als Echtzeit erkannt wird. Man nimmt an, dass sowohl die Zukunft als auch die Vergangenheit sekundär für die Gegenwart betrachtet werden.