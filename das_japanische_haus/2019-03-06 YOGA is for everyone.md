---
id: '628534697575523'
title: YOGA is for everyone
start: '2019-03-06 19:00'
end: '2019-03-06 20:00'
locationName: Das Japanische Haus e. V.
address: 'Eisenbahnstr.113b, 04315 Leipzig'
link: 'https://www.facebook.com/events/628534697575523/'
image: 53417198_2146595802062671_1219857165460701184_n.jpg
teaser: 'Neuer Yogakurs für neues Jahr :) Englisch / Spanisch Gegen Spende  •••••••••••••••••••••••••••••••••••••••••••• Let''s start the year with fresh and re'
recurring: null
isCrawled: true
---
Neuer Yogakurs für neues Jahr :) Englisch / Spanisch
Gegen Spende 
••••••••••••••••••••••••••••••••••••••••••••
Let's start the year with fresh and renewed energy! Step out of the house and let's move our bodies! "Yoga is for Everybody" is an event hosted by Das Japanishe Haus and will be conducted by Loreto Olivos, Hatha Yoga instructor. These voluntary-contribuition-based classes are open to the public and will be hosted in English* every Wednesday at 19:00 (may last longer than 1 hour).
These classes are meant for all levels but especially for those who are beginners or those who are looking for a soft and relaxing stretching session. So please, don't hesitate to try it!

If you have any questions please contact below;
FB Lore DeLoreto : @loreolivos.a
Email : loreolivos.a@hotmail.com

And don't forget to bring:
• Yoga Mat
• Blanket (for shavasana and adjustments) 
• A small towel 
• Water

*Las clases también podrán ser impartidas/aclaradas en español.