---
id: "449976918910883"
title: Ramentag im Japanischen Haus "TanTanMen"
start: 2019-12-14 18:00
end: 2019-12-14 22:00
locationName: Das Japanische Haus e. V.
address: Eisenbahnstr.113b, 04315 Leipzig
link: https://www.facebook.com/events/449976918910883/
image: 79024132_2659673717421541_8363528891389706240_o.jpg
teaser: 'Der letzte Ramen-Tag des Jahres im Japanischen Haus!  Um euch ein wenig
  aufzuwärmen gibt es (angenehm) scharfes, "TanTanMen"-Ramen（担々麺）: ).  Es gibt
  e'
isCrawled: true
---
Der letzte Ramen-Tag des Jahres im Japanischen Haus!

Um euch ein wenig aufzuwärmen gibt es (angenehm) scharfes, "TanTanMen"-Ramen（担々麺）: ). 
Es gibt eine vegane Version und eine mit Fleisch.

Um 18:00 geht's schon los, so lange der Vorrat reicht.
Freuen uns!