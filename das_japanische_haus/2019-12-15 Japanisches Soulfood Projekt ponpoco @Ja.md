---
id: "734304653757793"
title: Japanisches Soulfood Projekt "ponpoco" @Japanisches Haus
start: 2019-12-15 12:00
end: 2019-12-15 21:00
locationName: Das Japanische Haus e. V.
address: Eisenbahnstr.113b, 04315 Leipzig
link: https://www.facebook.com/events/734304653757793/
image: 77124863_168061137905247_2135837771162451968_n.jpg
teaser: Es gibt ein besonderes Essens-Event im Japanischen Haus!  Die Mädels vom
  japanischem Soulfood-Projekt "PONPOCO" kochen für uns "DONBURI"!  DONBURI bed
isCrawled: true
---
Es gibt ein besonderes Essens-Event im Japanischen Haus! 
Die Mädels vom japanischem Soulfood-Projekt "PONPOCO" kochen für uns "DONBURI"!

DONBURI bedeutet, dass Reis in eine Schüssel gefüllt wird und das ganze mit weiteren besonderen Zutaten getoppt wird! 🍚🇯🇵
Diesmal gibt es:
🐂 Gyūdon (mit Rindfleisch)
🍤 Tendon(mit Tenpura)
🍆 Nasumisodon (mit Aubergine)

Es geht bereits mittags los uns geht bis abends so lange der Vorrat reicht! Wir freuen uns!

ついに！2回目の出張ポンポコやります٩( ᐛ ) ( ᐖ ) ᐖ )۶
今回はどんぶり祭りです。おいしいよ〜(  ᷇࿀ ᷆ و)و
🐂牛丼
🍤天ぷら丼
🍆なす味噌丼
