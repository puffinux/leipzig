---
id: '2306946456249554'
title: Innensicht Frankreich
start: '2019-02-16 19:00'
end: '2019-02-16 22:00'
locationName: Das Japanische Haus e. V.
address: 'Eisenbahnstr.113b, 04315 Leipzig'
link: 'https://www.facebook.com/events/2306946456249554/'
image: 51887622_2118150041573914_299141530113802240_n.jpg
teaser: 'In Frankreich verursachen die "gelben Westen" einige Unruhe. Hierzulande ist man besorgt: sind das faschistische Kräfte? Wir haben drei junge Leute zu'
recurring: null
isCrawled: true
---
In Frankreich verursachen die "gelben Westen" einige Unruhe. Hierzulande ist man besorgt: sind das faschistische Kräfte?
Wir haben drei junge Leute zu Besuch, die Euch aus Frankreich berichten werden, und die Ihr nach Herzenslust ausfragen dürft..

Wir benötigen für den Abend noch paar Dolmetscher, es fehlt noch französisch- deutsch, französisch-arabisch, und französisch-japanisch. Französisch-Deutsch und französisch-Englisch gibt es Dolmetscher, wir sind dankbar über jeden, der irgendwie übersetzen hilft!
