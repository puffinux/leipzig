---
id: '445024779636914'
title: Ramentag in das Japanische Haus
start: '2019-05-04 20:00'
end: '2019-05-04 23:30'
locationName: Das Japanische Haus e. V.
address: 'Eisenbahnstr.113b, 04315 Leipzig'
link: 'https://www.facebook.com/events/445024779636914/'
image: 58933230_2228960597159524_3323207503348498432_n.jpg
teaser: wir machen noch mal Ramentag am dieses Samstag. Ihr bekommt handgemachte japanische Nudeln!  es gibt Vegan und mit Fleisch Ramen.
recurring: null
isCrawled: true
---
wir machen noch mal Ramentag am dieses Samstag.
Ihr bekommt handgemachte japanische Nudeln!

es gibt Vegan und mit Fleisch Ramen.