---
id: "2712768195410486"
title: Knowing The Present in Fukushima
start: 2019-11-26 17:00
end: 2019-11-26 19:00
locationName: Das Japanische Haus e. V.
address: Eisenbahnstr.113b, 04315 Leipzig
link: https://www.facebook.com/events/2712768195410486/
image: 78672874_2609257179129862_5205191129401655296_n.jpg
teaser: ・ Eight years and six months have passed since the Great East Japan Earthquake
  on 11th March in 2011. ・We, the students of Fukushima University, have
isCrawled: true
---

・ Eight years and six months have passed since the Great East Japan Earthquake on 11th March in 2011.
・We, the students of Fukushima University, have been involved in a variety of activities for reconstruction at Iitate Village in Fukushima Prefecture.
・Iitate Village in Fukushima Prefecture is located within 50 kilometers from the Fukushima Nuclear Power Plants. The entire village had been designated as an evacuation area.  However, ,  the evacuation order was lifted in 
March 2017 expect for some regions. 
・The population of Iitate Village before the earthquake was about 6,000 , but as of July 2019, the number of returnees is about 1,300 , which is only about 20% of that of the village.
・At the event, we would like to talk about "Activities we have done in Iitate Village, Fukushima Prefecture 
・Why don’t you learn about the current situation in Fukushima Prefecture with our activity report?


・2011年3月11日の震災から8年と半年が経過しました
・私たち福島大学大黒ゼミの学生は、福島県の飯舘村という場所で、震災以降、復興のために様々な活動をしてきました。
・福島県飯舘村は福島第一原子力発電所から50キロ圏内に位置し、その全域が避難地域に指定されることとなりましたが、2017年3月に一部地域を除いて避難指示が解除されました。震災前の人口は約6000人でしたが、2019年7月時点での帰還住民数は約1300人と、2割ほどに留まっています。
・イベントでは、「私たちが福島県飯舘村で行ってきた活動」についてのお話をさせていただきたいと思っております。
私たちの活動報告とともに、震災後9年目を迎える福島県の現状について知ってみませんか？ 
