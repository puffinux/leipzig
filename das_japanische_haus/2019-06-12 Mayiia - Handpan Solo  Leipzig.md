---
id: '300455544174071'
title: Mayiia - Handpan Solo | Leipzig
start: '2019-06-12 20:00'
end: '2019-06-12 22:00'
locationName: Das Japanische Haus e. V.
address: 'Eisenbahnstr.113b, 04315 Leipzig'
link: 'https://www.facebook.com/events/300455544174071/'
image: 61219461_2275502262505357_8284604731272200192_n.jpg
teaser: 'Weite Landschaften und ziehende Flächen, die sich ineinander verweben und sich wieder auflösen. Mayiia’s Kompositionen sind durchdrungen von atmosphär'
recurring: null
isCrawled: true
---
Weite Landschaften und ziehende Flächen, die sich ineinander verweben und sich wieder auflösen. Mayiia’s Kompositionen sind durchdrungen von atmosphärischen sounds und kraftvollen Rhythmen, die sie teils mit dem Synthesizer und teils mit den Klängen der Handpan untermalt. Ob gesprochener Text, improvisierter Klang oder gesungene Songtexte, Mayiia spielt
mit den Möglichkeiten, die ihre Stimme als Instrument hergibt. Eine Künstlerin die Geschichten erzählt, von kurzen Momenten und langen Reisen, wobei ihr Gefühl im Vordergrund steht.
„Wenn ich singe, dann fühle ich mich frei. Dann vergesse ich alles um mich herum, dann ist Musik nur noch ein Mittel zur Übertragung, dann werde ich eins mit der Musik und die Musik
wird eins mit mir.“
Musik ist für sie nicht begrenzt auf herkömmliche Instrumente. Diese Freiheit prägt ihre Musik und macht sie zu einem einzigartigen Klangerlebnis. Stetig auf der Suche nach den Grenzen der Stimme in Improvisation und Klang, weiß sie ihre Stimme teils mit Texten, teils mit erfundener Sprache stilsicher einzusetzen.
Mit der Handpan im Gepäck reist sie durch Europa und war u.a. auf dem Griasdi – Worldmusic Festival in Österreich und dem Copenhagen – Songwriters Festival zusehen. Grad aus China
zurück und schon in Deutschland unterwegs, präsentiert sie uns ihre neue Musik.
Ihre single „Song for Hibesca“ wurde am 17. Oktober 2018 veröffentlicht. Diesen Sommer folgt ihre neue EP „Brilliant silence“.
🎧  https://www.youtube.com/watch?v=OFZMVFpjWTI

doors: 7:30 pm
music: 8:00 pm

entry: free