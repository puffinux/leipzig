---
id: '322729485077030'
title: International Tea-time with Pianomusic (Live)
start: '2019-05-19 11:00'
end: '2019-05-19 17:00'
locationName: Das Japanische Haus e. V.
address: 'Eisenbahnstr.113b, 04315 Leipzig'
link: 'https://www.facebook.com/events/322729485077030/'
image: 58895125_2232136530175264_8509568388022927360_n.jpg
teaser: International Tea-time                       Internationale Teezeit with Pianomusic (Live)                    mit Klaviermusik (Live)  im Japanischen
recurring: null
isCrawled: true
---
International Tea-time                       Internationale Teezeit
with Pianomusic (Live)                    mit Klaviermusik (Live)

im Japanischen Haus e.V.
Eisenbahnstraße 113B
04315 Leipzig

Wir laden am Sonntag, den 19. 05.2019, zum Tee. 

Genießt bei verschiedenen Tees und internationalen Köstlichkeiten das Können unseres japanischen Pianisten. 

Es ist für jeden was dabei. Vollkost, Vegan, Glutenfrei! Kaffee gibt es natürlich auch.



We invite for tea on Sunday, May 19th, 2019.

Enjoy the abilities of our Japanese Pianist, whilst savouring different teas and international delicacies.

There's something for everyone. Conventional, Vegan, Gluten-free! There's coffee as well of course.


Am: 19.05. 2019
Beginn ab 13 Uhr
Ende gegen 19 Uhr

Date: May 19th, 2019
Beginning: 1 pm
Ending around 7 pm



Die nächste Veranstaltung  findet am 23.06.2019 statt.

The next event will be June 23rd, 2019



Ihr möchtet euch nicht hinsetzen, sondern nur etwas mitnehmen?
Gerne! Nur bringt am besten etwas zum transportieren mit.
 
*Die gezeigten Produkte dienen nur als Referenz



You don't want to sit down and only have some take away?
Best bring something for transportation.

*The shown products are only for reference.