---
id: "698142187357765"
title: "Concert: alexander fredericks"
start: 2019-11-14 21:00
end: 2019-11-14 22:00
locationName: Das Japanische Haus e. V.
address: Eisenbahnstr.113b, 04315 Leipzig
link: https://www.facebook.com/events/698142187357765/
image: 75362173_2594166020638978_8890197795772825600_o.jpg
teaser: alexander fredericks  alexanderfredericks.bandcamp.com lo-fi appalachian
  music  21:00
isCrawled: true
---
alexander fredericks 
alexanderfredericks.bandcamp.com
lo-fi appalachian music 
21:00 