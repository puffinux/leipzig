---
id: '342348950032758'
title: Herje Mine @Japanisches Haus
start: '2019-07-25 21:00'
end: '2019-07-26 00:00'
locationName: Das Japanische Haus e. V.
address: 'Eisenbahnstr.113b, 04315 Leipzig'
link: 'https://www.facebook.com/events/342348950032758/'
image: 64857569_2479386385456876_2475220829852401664_n.jpg
teaser: 'balkan goes japan!  herje mine erstes mal in dem japanischen haus!  https://soundcloud.com/herje_mine/the-very-best-of'
recurring: null
isCrawled: true
---
balkan goes japan!

herje mine erstes mal in dem japanischen haus!

https://soundcloud.com/herje_mine/the-very-best-of