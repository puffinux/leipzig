---
id: '2279444558974604'
title: YOGA for everyone
start: '2019-07-29 19:00'
end: '2019-07-29 20:00'
locationName: Das Japanische Haus e. V.
address: 'Eisenbahnstr.113b, 04315 Leipzig'
link: 'https://www.facebook.com/events/2279444558974604/'
image: 64984167_2319908944731355_8041702280321302528_o.jpg
teaser: 'Let''s give ourselves some time to disconnect, to meditate through movement and also have fun while estreching our bodies and working on our breathing,'
recurring: null
isCrawled: true
---
Let's give ourselves some time to disconnect, to meditate through movement and also have fun while estreching our bodies and working on our breathing, come to our Yoga sessions every Monday at 19 pm.
This events a meant to bring people of all levels of practice into the mat, so do not worry about experience, you only need to bring your mat and energy.
Classes are given on voluntary/conscious contribution.

For more information you can contact,
FB: @yogaisforeveryoneleipzig

See you on the mat, 
Lore