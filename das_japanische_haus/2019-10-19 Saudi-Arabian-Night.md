---
id: "1365527420277613"
title: Saudi-Arabian-Night
start: 2019-10-19 19:00
end: 2019-10-19 23:30
link: https://www.facebook.com/events/1365527420277613/
image: 71569002_2510954455626802_16846708570849280_n.jpg
teaser: Hallo Leute, am Samstag den 19. 10. läd das japanische Haus zur
  Saudi-Arabian-Night.   Fast jeder hat schon einmal mit jemandem zu tun gehabt
  der aus
isCrawled: true
---
Hallo Leute,
am Samstag den 19. 10. läd das japanische Haus zur Saudi-Arabian-Night. 

Fast jeder hat schon einmal mit jemandem zu tun gehabt der aus einem arabischen Land stammt. Doch selten ist es Saudi-Arabien. Man hört Gerüchte, liest schockierende Zeitungsmeldungen und schüttelt den Kopf über das Frauenbild in Saudi. Aber ist das schon alles? Oder gibt es mehr?

Freut euch auf Saudisches Essen und eine kleine Fotoausstellung mit ein paar Hintergrund-infos über dieses recht junge Land. 



Hi everyone,
on Saturday 19th October the Japanese House invites you for a Saudi-Arabian-Night.

Nearly everyone has had something to do with someone from an Arabic country. But it is not very often Saudi-Arabia. One hears rumours, reads shocking headliners or shakes their head at the image of women in Saudi. But is that everything? Or is there more?

Look forward to Saudian food and a small photo-exhibition with a bit of background information about this, relatively young country.