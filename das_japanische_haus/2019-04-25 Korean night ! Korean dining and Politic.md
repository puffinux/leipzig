---
id: '607587286381462'
title: Korean night ! Korean dining and Political conference
start: '2019-04-25 20:00'
end: '2019-04-25 23:00'
locationName: Das Japanische Haus e. V.
address: 'Eisenbahnstr.113b, 04315 Leipzig'
link: 'https://www.facebook.com/events/607587286381462/'
image: 57321591_2211205588935025_6913990227992248320_n.jpg
teaser: '25th Apr, 8PM  We invite you to our cultural collaboration "Korean night"   Let''s experience Korean food & arts In addition, we also prepare political'
recurring: null
isCrawled: true
---
25th Apr, 8PM

We invite you to our cultural collaboration "Korean night" 

Let's experience Korean food & arts
In addition, we also prepare political mini-conference about democratization movement between Leipzig and Gwangju. 
It will be a very exciting and meaningful gathering for us. 
*Gwangju is the city of democracy in Korea and the friendship city of Leipzig. 

Come and mingle with new people in different culture and perspectives! 