---
id: "706784213175745"
title: Filmvorführung und Diskussion zur Situation in Hong Kong
start: 2019-11-16 15:00
end: 2019-11-16 17:30
locationName: Das Japanische Haus e. V.
address: Eisenbahnstr.113b, 04315 Leipzig
link: https://www.facebook.com/events/706784213175745/
image: 75336383_2585072721548308_3721224287332335616_n.jpg
teaser: "Screening of the film: Boundaries (Gaaihaan/Naambak 界限／南北) 2019.5 30 mins
  Color & BW Cantonese with English subtitles Directed by Lee Chun Fung //////"
isCrawled: true
---
Screening of the film: Boundaries (Gaaihaan/Naambak 界限／南北)
2019.5
30 mins
Color & BW
Cantonese with English subtitles
Directed by Lee Chun Fung
////////////////////////////////////////////////
* The screening will be followed by a discussion with the director.

With a series of protests and escalations during the past several months, Hong Kong has again become a battlefield of two different ideologies. To understand the current situation, one must not limit the analysis to the protesters’ demand for “freedom and democracy,” but also pay attention to the identity politics behind it. The film provides an alternative perspective by looking at the origins of identity production, the geo-politics of the region, and the socio-political dimensions within the context of global capitalism.
The artist/director, Lee Chun Fung will join the discussion after the film-screening via Skype and share his viewpoint and analysis regarding the protests in Hong Kong now.
Thorben Pelzer (Chinese Studies, University of Leipzig) will chair the discussion.

////////////////////////////////////////////////

About the video:
The story is set in a parallel universe that diverged when the British government declared their proposals during the negotiations of Hong Kong’s future in the early 1980s…
When the British and Mainland Chinese governments started to negotiate the future of Hong Kong, the Chinese government displayed persistency and insisted on reclaiming sovereignty over the whole of Hong Kong. Originally, according to the Treaty of Nanjing and the Treaty of Beijing, Great Britain was the sovereign over Hong Kong Island and the Kowloon Peninsula, while the New Territories were on lease for 99 years and had to be returned to China in 1997. Considering the long-term interests the British had invested in Hong Kong, London strongly advocated the validity of these treaties. Therefore, the British government proposed to build the “Hong Kong Wall” at the boundary between the New Territories and Kowloon, in the vein of the Berlin Wall in Germany. Under the terms of this proposal, Hong Kong would be divided into a northern and a southern part, as assurance that Great Britain could retain their last “Far Eastern colony” after 1997.
The film depicts the possible development of Hong Kong’s society if such a wall had been built. Within the allegory, the wall serves not only as a physical division, but contributes towards constructing or re-articulating the identity of the Hong Kong people, which is influenced by various ideological, political, and economic agendas.

////////////////////////////////////////////////

About the artist:
Lee Chun Fung [李俊峰]
LEE Chun-Fung (b. 1984, Hong Kong) is an artist, community activist, and curator based in Hong Kong. He is the co-founder of Woofer Ten (2009–2015), an experimental community art space in Hong Kong. His artistic interest lies in the interconnection between the sense of community and activism. His practices cover different media and disciplines, ranging from art action to installation, video, photography, zine publication, workshop, writing, curating, and other forms of art.

www.leechunfung.blogspot.com