---
id: "540465243193563"
title: Vortrag über die Wendezeit "Pressefrühling und Profit"
start: 2019-11-23 21:00
end: 2019-11-23 22:00
locationName: Das Japanische Haus e. V.
address: Eisenbahnstr.113b, 04315 Leipzig
link: https://www.facebook.com/events/540465243193563/
image: 75291710_2569730399749207_5361775277552697344_n.jpg
teaser: "Pressefrühling und Profit. \vDie verpasste Chance der Wendezeit
  1989/1990  Mandy Tröger, Kommunikations- und Geschichtswissenschaftlerin,
  enthüllt, wie"
isCrawled: true
---
Pressefrühling und Profit. Die verpasste Chance der Wendezeit 1989/1990

Mandy Tröger, Kommunikations- und Geschichtswissenschaftlerin, enthüllt, wie nach dem Mauerfall westdeutsche Wirtschaftsinteressen und das Eigeninteresse der Bundesregierung eine basisdemokratische Wende in der Presselandschaft der ehemaligen DDR verhinderten. Basierend auf umfangreicher Archivarbeit und Zeitzeugen-Interviews dokumentiert sie, wie westdeutsche Großverlage bereits Ende 1989 aktiv Lobbyarbeit betrieben, um Marktvorteile im Osten zu sichern. Über die Reform des DDR-Pressevertriebes strebten Springer, Gruner + Jahr, Bauer und Burda nach Monopolstellungen in Ostdeutschland. Mit einem eigenen Vertrieb, Dumpingpreis-Produkten und frühen Joint-Venture-Abkommen mit den großen ehemaligen SED-Bezirkszeitungen bauten die Verlage ihre Marktdominanz aus. Dem wirtschaftlichen Druck fielen vor allem neugegründete Lokal- und Bürgerrechtszeitungen zum Opfer. Die Bundesregierung griff nicht im Sinne der Pressevielfalt ein, sondern setzte auf den freien Markt und schützte bestehende Pressestrukturen der BRD. Die Doktorarbeit von Mandy ist 2019 unter dem Titel "Pressefrühling und Profit" 2019 beim Halem-Verlag erschienen. 

Die Veranstaltung findet im Rahmen der 3. Jahrestagung des Netzwerks Kritische Kommunikationswissenschaft statt.

Davor gibt es wie immer Küche für Alle!