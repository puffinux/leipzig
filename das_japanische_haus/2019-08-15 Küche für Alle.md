---
id: '392369608129459'
title: Küche für Alle
start: '2019-08-15 18:00'
end: '2019-08-15 23:30'
locationName: Das Japanische Haus e. V.
address: 'Eisenbahnstr.113b, 04315 Leipzig'
link: 'https://www.facebook.com/events/392369608129459/'
image: 67741561_2393919977330251_8012248098979971072_n.jpg
teaser: 'Jeder in Leipzig ist willkommen in unserem Haus! Genießt mal bitte unseren Raum und Speisen.  Ab 17 Uhr beginnen wir zu kochen, ab 20 Uhr steht das Es'
recurring: null
isCrawled: true
---
Jeder in Leipzig ist willkommen in unserem Haus! Genießt mal bitte unseren Raum und Speisen.

Ab 17 Uhr beginnen wir zu kochen, ab 20 Uhr steht das Essen auf dem Tisch. Komm einfach mal vorbei.

– – –

Everyone in leipzig is welcome to our place! Please enjoy our space and foods. We’ll start cooking from 5 p.m. and serving from 8 p.m. If you are interested in, just come and join us.

– – –

أهلاً بكم في البيت الياباني نرحب بالجميع هنا في مكانناو نرجو أن تستمتعو بالمكان والطعام, نبدأ الطبخ الساعة 5 مساءً, ويبدأ تقديم الطعام الساعة 8 مساءً. نرجو أن تشاركونا!

– – –

هر کس در لایپزیگ به جای ما خوش آمدید! لطفا فضای ما، موسیقی و غذاهای لذت ببرید. ما شروع به پخت و پز از 17:00 و خدمت به از 20:00 اگر شما علاقه مند هستند، فقط می آیند و به ما بپیوندید

– – –

შაბათს გეპატიჟებათ ყველას Das Japanische Haus ვინც ლაიფციგში ცხოვრობს. დატკბით ჩვენი ადგილით და კერძებით. 17:00 საათიდან ვიწყებთ ერთად საჭმლის კეთებას და 20:00 საათიდან ერთად დავაგემოვნებთ მზა კერძს. შემოგვიარე Das Japanische Haus-ში. Eisenbahnstrasse 113b