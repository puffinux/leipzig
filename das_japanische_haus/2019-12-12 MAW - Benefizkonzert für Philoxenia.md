---
id: "880243042369745"
title: MAW - Benefizkonzert für Philoxenia
start: 2019-12-12 20:00
end: 2019-12-12 23:00
locationName: Das Japanische Haus e. V.
address: Eisenbahnstr.113b, 04315 Leipzig
link: https://www.facebook.com/events/880243042369745/
image: 76615085_1227741847409624_5027427879038746624_n.jpg
teaser: "8pm: KüFA 9.30pm: Concert with MAW Entry: Donation for Philoxenia - kitchen
  for refugees in Thessaloniki  MAW are an Italian-German trio based in Leip"
isCrawled: true
---
8pm: KüFA
9.30pm: Concert with MAW
Entry: Donation for Philoxenia - kitchen for refugees in Thessaloniki

MAW are an Italian-German trio based in Leipzig, drawing inspiration from avant-pop acts such as Bjork, Radiohead and PJ Harvey. 
Listen to MAW here: https://www.youtube.com/watch?v=IGkd4b8nddA

After our drummer volunteered at the Philoxenia kitchen this year, we would like to collect financial and clothes donations at this event, to be sent to the project during the cold season.

So, please use this perfect opportunity to sort out your wardrobe, and bring us your old, unwanted - but of course still wearable! - clothes, and finally, enjoy this evening with food and music. No better way to keep warm this winter, right? <3

More information about the project:
https://philoxeniakitchen.wixsite.com/philoxenia
https://www.gofundme.com/f/homeless-kitchen-in-thessaloniki