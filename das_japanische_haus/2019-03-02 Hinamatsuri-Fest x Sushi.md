---
id: '1248532388634420'
title: Hinamatsuri"-Fest x Sushi
start: '2019-03-02 18:00'
end: '2019-03-03 00:00'
locationName: Das Japanische Haus e. V.
address: 'Eisenbahnstr.113b, 04315 Leipzig'
link: 'https://www.facebook.com/events/1248532388634420/'
image: 52602388_2134497309939187_931665903576154112_n.jpg
teaser: 'In diesem Monat möchten wir als besonderes Event am 2. März das japanische „Hinamatsuri“-Fest feiern.  Am „Hinamatsuri“-Fest, auch Puppenfest oder Mäd'
recurring: null
isCrawled: true
---
In diesem Monat möchten wir als besonderes Event am 2. März das japanische „Hinamatsuri“-Fest feiern.

Am „Hinamatsuri“-Fest, auch Puppenfest oder Mädchenfest genannt, betet man für die gesunde Entwicklung und für das Glück der Kinder.

Zur Feier des Tages möchten wir Sushi zubereiten.

Wir freuen uns auf euch!

今月のスペシャルtagはひなまつり×お寿司です。
ひなまつりとは、子供の成長と健康と厄除けを祈ったお祭りです。3月2日は日本の家でその日にちなんでお寿司を作ります。
みんな来てくださいね～！