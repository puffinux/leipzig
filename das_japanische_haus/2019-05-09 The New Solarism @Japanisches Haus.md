---
id: '625946931164421'
title: The New Solarism @Japanisches Haus
start: '2019-05-09 21:00'
end: '2019-05-09 23:59'
locationName: Das Japanische Haus e. V.
address: 'Eisenbahnstr.113b, 04315 Leipzig'
link: 'https://www.facebook.com/events/625946931164421/'
image: 58384615_2843348955889665_1709194265646596096_o.jpg
teaser: 'melt away listening to the dreamy music of the new solarism - a solo project of violinist izabela kałduńska (gdańsk, pl)   https://soundcloud.com/izab'
recurring: null
isCrawled: true
---
melt away listening to the dreamy music of the new solarism - a solo project of violinist izabela kałduńska (gdańsk, pl)


https://soundcloud.com/izabela-ka-du-ska/waiting-for-an-earthquake 
https://www.facebook.com/thenewsolarism/
https://www.youtube.com/channel/UC4Tsx7ogx-a6DKuNXRrqRlQ
https://thenewsolarism.bandcamp.com/  

