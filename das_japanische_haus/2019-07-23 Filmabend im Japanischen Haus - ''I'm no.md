---
id: '2076617019115123'
title: Filmabend im Japanischen Haus - ''I'm not your negro''
start: '2019-07-23 19:00'
end: '2019-07-23 23:00'
locationName: Das Japanische Haus e. V.
address: 'Eisenbahnstr.113b, 04315 Leipzig'
link: 'https://www.facebook.com/events/2076617019115123/'
image: 67218131_418883022048969_5160072695525670912_n.jpg
teaser: 'Liebe Leute, Am Dienstag geht der Filmabend mit der Lokalgruppe Leipzig von Studieren Ohne Grenzen in die 2. Runde! Los geht es um 20:00Uhr mit Küfa ('
recurring: null
isCrawled: true
---
Liebe Leute,
Am Dienstag geht der Filmabend mit der Lokalgruppe Leipzig von Studieren Ohne Grenzen in die 2. Runde! Los geht es um 20:00Uhr mit Küfa ( Küche für alle). Im Anschluss also etwa 21:30 starten wir dann mit unserem Film :) 

Was wird gezeigt?  ''I am not your negro'' - ein Dokumentarfilm von Raoul Peck.

Worum gehts? 
''Ein Textmanuskript des amerikanischen Schriftstellers James Baldwin ist die Grundlage des Dokumentarfilms "I am not your Negro" von Regisseur Raoul Peck. Der unveränderte Text wird um Archivaufnahmen von Reden des Schriftstellers und verschiedene Ausschnitte aus TV-Sendungen, Filmen und Nachrichtenbeiträgen ergänzt. Aus Baldwins Sicht schildert der Film die Ermordung der afroamerikanischen Bürgerrechtler Malcolm X, Martin Luther King und Medgar Evers, mit denen Baldwin befreundet war. Der Film befasst sich darüber hinaus mit grundsätzlichen Fragen afroamerikanischer Identität. Eindrücklich zeichnet er einen wichtigen Teil in der Geschichte der US-amerikanischen Bürgerrechtsbewegung nach und thematisiert über den Text Baldwins die unterschiedliche öffentliche Wahrnehmung ihrer prägendsten Figuren. '' (http://www.bpb.de/mediathek/283417/i-am-not-your-negro)
Wir freuen uns auf euch,
eure Studieren Ohne Grenzen Lokalgruppe Leipzig
___________________________________________________
WAS IST STUDIEREN OHNE GRENZEN?
___________________________________________________

Etudes Sans Frontières - Studieren Ohne Grenzen e.V. ist eine gemeinnützige Organisation von Studenten für Studenten: Wir unterstützen Studiernde in den Regionen Afghanistan, der Demokratischen Republik Kongo und Tschetschenien durch Zugang zu Hochschulbildung. Unser Ziel ist es, junge Menschen aus diesen Krisengebieten dafür zu qualifizieren, selbstständig zum Wiederaufbau ihrer Region beizutragen.
Hier hast Du die Chance, Dich zu engagieren und kleine und große Projekte mit zu gestalten! Meld Dich einfach bei uns!
