---
id: "920801374973682"
title: The Mechanical Tales Konzert
start: 2019-10-23 21:00
end: 2019-10-23 23:00
locationName: Das Japanische Haus e. V.
address: Eisenbahnstr.113b, 04315 Leipzig
link: https://www.facebook.com/events/920801374973682/
image: 72836502_2550082811713966_5320843895624433664_n.jpg
teaser: The Mechanical Tales are Juliette, Lux and Lloyd Sinatra. The trio is based in
  Udine (IT), the so-called capital of the war, and plays cinematic music
isCrawled: true
---
The Mechanical Tales are Juliette, Lux and Lloyd Sinatra.
The trio is based in Udine (IT), the so-called capital of the war, and plays cinematic music with post-atomic vibes.

Being involved for many years in the italian experimental music scene with other bands, they decide to create a new musical project in 2012.

As the soundtrack on the cinematographic film does, they work closely with the video medium through the collaboration with several visual artists.

In 2014 they meet Eleonora Sovrani, a videoartist whose focus lies in the relationship between visual language, social dynamics and artistic practice. This close collaboration gives birth to many performative, intermedial, audiovisual live pieces, installations, some touring across Europe, and two LPs: Iceberg (October 2015, self-released) and About Fallout (February 2017, self-released).

They produce and release their albums, promote their works and organize their tours across Europe indipendently, with a DIY approach.

From October 2015 to August 2017 they curated the musical review VISI(ON)AIR in the Visionario cinema in Udine.