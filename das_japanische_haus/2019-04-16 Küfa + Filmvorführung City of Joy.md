---
id: '2127588900695822'
title: Küfa + Filmvorführung "City of Joy"
start: '2019-04-16 19:00'
end: '2019-04-16 22:00'
locationName: Das Japanische Haus e. V.
address: 'Eisenbahnstr.113b, 04315 Leipzig'
link: 'https://www.facebook.com/events/2127588900695822/'
image: null
teaser: Nach einem gemeinsamen Essen (gegen Spende) wollen wir mit euch gemeinsam den Film "City of Joy" schauen um anschließend ins Gespräch zu kommen.   Den
recurring: null
isCrawled: true
---
Nach einem gemeinsamen Essen (gegen Spende) wollen wir mit euch gemeinsam den Film "City of Joy" schauen um anschließend ins Gespräch zu kommen. 

Den Trailer zum Film findet ihr hier :https://www.youtube.com/watch?v=MNy0MG_iy0Y

Wenn ihr mehr über uns und unsere Arbeit erfahren wollt kommt zu unserem Infoabend in der folgenden Woche: 
https://www.facebook.com/events/1874126752693275/

Wer sind wir?
Studieren Ohne Grenzen engagiert sich für Hochschulbildung in Konfliktgebieten. Wir vergeben Stipendien an bedürftige Studentinnen und Studenten, tragen zur Verbesserung der Bildungsinfrastruktur bei und sensibilisieren die deutsche Öffentlichkeit für die Lage in den Zielregionen – derzeit sind das Afghanistan, die DR Kongo, Sri Lanka, Tschetschenien, Burundi und Guatemala.
Mit unserer Arbeit wollen wir junge Menschen dabei unterstützen, selbständig zum Wiederaufbau und zur Versöhnung in ihrer Heimat beizutragen. Gemeinsam mit den Stipendiaten und Stipendiatinnen möchten wir einen Beitrag zur friedlichen und nachhaltigen Entwicklung in Staaten und Regionen leisten, die unter Krieg oder seinen Folgen leiden.

Mehr Infos unter www.studieren-ohne-grenzen.org

Wir freuen uns auf euch!