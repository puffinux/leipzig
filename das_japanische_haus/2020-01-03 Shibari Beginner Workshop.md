---
id: "2500868146900436"
title: Shibari Beginner Workshop
start: 2020-01-03 20:00
end: 2020-01-03 22:30
locationName: Das Japanische Haus e. V.
address: Eisenbahnstr.113b, 04315 Leipzig
link: https://www.facebook.com/events/2500868146900436/
image: 79121918_159348272099556_4927765894534266880_o.jpg
teaser: English description below  SHIBARI?? Shibari stammt aus Japan und ist
  Fesselkunst mit dem Seil. Es hat einen martialischen Ursprung, heute wird es
  v.a
isCrawled: true
---
English description below

SHIBARI??
Shibari stammt aus Japan und ist Fesselkunst mit dem Seil. Es hat einen martialischen Ursprung, heute wird es v.a als Kunst, Performance, Meditation oder erotische Spielart betrieben.

In 2,5 Stunden üben wir grundlegende Techniken des modernen Shibari und besprechen wichtige Aspekte zu Sicherheit und Geschichte. Wir wollen euch einen schnellen und praktischen Einstieg ermöglichen!

Offen für alle: Gender, einzeln oder zu zweit, mit und ohne Vorkenntnissen, mit und ohne Seilen. Bitte verbindlich über den Anmeldeknopf anmelden.

Freiwillige Spende, Empfehlung 10-20 Euro pro Person.

Shibari is the japanese art of tying with rope. Despite it's martial origin, today it is practised mostly as art, performance, meditation or erotic play.

Within a two hour workshop we practise basic techniques of modern shibari and discuss important aspects of safety and history.

Open to all genders. No experience needed, also no need to bring your own rope or a partner (but welcome to do so, of course). Please sign up as the number of participants is limited.

Voluntary donation, 10-20 euro recommended.