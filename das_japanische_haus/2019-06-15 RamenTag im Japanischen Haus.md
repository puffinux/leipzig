---
id: '2338936276362634'
title: RamenTag im Japanischen Haus
start: '2019-06-15 18:00'
end: '2019-06-15 23:30'
locationName: Das Japanische Haus e. V.
address: 'Eisenbahnstr.113b, 04315 Leipzig'
link: 'https://www.facebook.com/events/2338936276362634/'
image: 62261260_2289527627769487_657131435102044160_n.jpg
teaser: wir machen noch mal Ramentag am 15.06.2019. Ihr bekommt handgemachte japanische Nudeln!  es gibt Vegan und mit Fleisch Ramen.
recurring: null
isCrawled: true
---
wir machen noch mal Ramentag am 15.06.2019.
Ihr bekommt handgemachte japanische Nudeln!

es gibt Vegan und mit Fleisch Ramen.
