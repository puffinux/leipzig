---
id: 1000-1567440000-1567445400
title: 'Leipziger Westen: offene Beratung für Hausprojekte'
start: '2019-09-02 16:00'
end: '2019-09-02 17:30'
locationName: null
address: 'HWR-Laden, Georg-Schwarz-Str. 19, Leipzig'
link: 'https://www.hwr-leipzig.org/event/leipziger-westen-offene-beratung-fuer-hausprojekte-8/'
image: null
teaser: 'Wenn ihr euch in eurem Freundeskreis schon lange mit der Idee tragt, ein Hausprojekt zu gründen, wen'
recurring: null
isCrawled: true
---
Wenn ihr euch in eurem Freundeskreis schon lange mit der Idee tragt, ein Hausprojekt zu gründen, wenn du Mitstreiter für deine eigene Initiative suchst; oder wenn ihr als Gruppe überlegt, welches der nächste Schritt ist, dann ist die offene Projektberatung die richtige Anlaufstelle. 

Die Veranstaltung bietet in einer Einstiegsberatung einen ersten Überblick über Herangehensweisen, Rechts- und Organisationsformen… 

Die offene Beratung beginnt am Montag, dem 2. September, 16 Uhr in der Georg-Schwarz-Str. 19. Die Runde beginnt pünktlich! 

Du kannst an diesem Termin nicht? 

Die offene Beratung findet in der Regel jeden ersten Montag im Monat statt. Da es in Ausnahmefällen zu terminlichen Verschiebungen kommen kann, informiere dich bitte immer vorher noch mal über unsere Website. Wenn du den Termin dort findest, dann findet er statt. 

Veranstaltung des Haus- und WagenRat e.V. 

