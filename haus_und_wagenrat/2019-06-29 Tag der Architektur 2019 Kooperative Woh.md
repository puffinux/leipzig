---
id: 991-1561824000-1561829400
title: 'Tag der Architektur 2019: Kooperative Wohnprojekte in Leipzig'
start: '2019-06-29 16:00'
end: '2019-06-29 17:30'
locationName: null
address: 'Lindenauer Markt 22 (Laden), 04177 Leipzig'
link: 'https://hwr-leipzig.org/event/tag-der-architektur-2019-kooperative-wohnprojekte-in-leipzig/'
image: null
teaser: Unterschiedliche Akteure des kooperativen Bauens und Wohnens in Leipzig stellen sich im Rahmen einer
recurring: null
isCrawled: true
---
Unterschiedliche Akteure des kooperativen Bauens und Wohnens in Leipzig stellen sich im Rahmen einer gemeinsamen Veranstaltung mit Ihren Konzepten und Projekten vor. Dabei werden Einblicke in bestehende und aktuelle Bestands- und Neubauprojekte gegeben. Im Rahmen der Veranstaltung besteht die Möglichkeit zum näheren Kennenlernen einzelner Projekte. 

