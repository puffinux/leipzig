---
id: 957-1551888000-1551893400
title: 'Grassimuseum: : offene Beratung für Hausprojekte'
start: '2019-03-06 16:00'
end: '2019-03-06 17:30'
locationName: null
address: 'Grassimuseum, Johannisplatz 5-11, Leipzig, 04103, Deutschland'
link: 'https://hwr-leipzig.org/event/grassimuseum-offene-beratung-fuer-hausprojekte/'
image: null
teaser: 'Wenn ihr euch in eurem Freundeskreis schon lange mit der Idee tragt, ein Hausprojekt zu gründen, wen'
recurring: null
isCrawled: true
---
Wenn ihr euch in eurem Freundeskreis schon lange mit der Idee tragt, ein Hausprojekt zu gründen, wenn du Mitstreiter für deine eigene Initiative suchst; oder wenn ihr als Gruppe überlegt, welches der nächste Schritt ist, dann ist die offene Projektberatung die richtige Anlaufstelle. 

Die Veranstaltung bietet in einer Einstiegsberatung einen ersten Überblick über Herangehensweisen, Rechts- und Organisationsformen… 

Die offene Beratung beginnt am Mittwoch, den 6. März 2019, 16 Uhr. Die Runde beginnt pünktlich! 

Die Veranstaltung ist Teil des Rahmenprogramms der Ausstellung „Together! Die neue Architektur der Gemeinschaft“, welche noch bis zum 17.03.2019 im Grassimuseum besucht werden kann. 

Eine Übersicht über das gesamte Rahmenprogramm der Ausstellung findet sich hier. 

Die Ausstellung wird des weiteren durch eine Filmreihe ergänzt. 

