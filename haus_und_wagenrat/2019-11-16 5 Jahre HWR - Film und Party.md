---
id: 1046-1573934400-1573963200
title: 5 Jahre HWR - Film und Party
start: 2019-11-16 20:00
end: 2019-11-17 04:00
address: huk, Georg-Schwarz-Str. 9, Leipzig
link: https://www.hwr-leipzig.org/event/5-jahre-hwr-film-und-party/
teaser: Unser 5 jähriges Jubiläum feiern wir gemeinsam mit einem der mittlerweile
  ältesten alternativen Vera
isCrawled: true
---
Unser 5 jähriges Jubiläum feiern wir gemeinsam mit einem der mittlerweile ältesten alternativen Veranstaltungsorte in Lindenau: dem hinZundkunZ… 

Mit dabei ist auch die SoWo eG mit ihrem 2 jährigem Jubiläum. 

16. November 2019 

Georg-Schwarz-Str. 9, Leipzig 

20.00 Uhr: Film „Gegen Spekulanten“ (Doku, BRD 1976 – https://dffb-archiv.de/dffb/gegen-spekulanten) über den Kampf gegen den Abriss einer Bergarbeitersiedlung in Duisburg mit kurzem Einführungsvortrag „Mieter*innenkämpfe und Genossenschaftsinitiativen von den 1970er-Jahren bis heute“ 

ab 22.00 Uhr DJs Shakin Casi & Alex de Salvo – Soul & R’n’B https://www.facebook.com/events/430130157561034/ 

Kommt vorbei – wir freuen uns! 

