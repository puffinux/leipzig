---
id: 1038-1574881200-1574892000
title: Hausprojekte scheitern! ? -5 Jahre Haus- und WagenRat -
start: 2019-11-27 19:00
end: 2019-11-27 22:00
address: HWR-Laden, Georg-Schwarz-Str. 19, Leipzig
link: https://www.hwr-leipzig.org/event/hausprojekte-scheitern-5-jahre-haus-und-wagenrat-ein-selbst-kritischer-rueckblick-auf-die-entwicklung-der-leipziger-projekteszene/
teaser: Eine theatralische Debatte/Show/Podiumsdiffusion für Hippies,
  Immobilienkaufleute und Theoriefetisch
isCrawled: true
---
Eine theatralische Debatte/Show/Podiumsdiffusion für Hippies, Immobilienkaufleute und Theoriefetischisten. [Oder: ein überbordender Debattenabend mit Idealismus, Realismus und Fundamentalkritik. Sowie Schnaps.] 

Wir werden (selbst-) kritisch auf die Entwicklung der Leipziger Projekteszene zurückblicken.Seit 5 Jahren begleiten wir als Haus- und WagenRat e.V. selbstorganisierte Projektinitiativen bei der Entstehung. Am 27. November wollen wir mal nicht auf das Positive an selbstorganisierten Wohnprojekten schauen. Sondern auf das, was danach passiert. 

Denn Hausprojekte scheitern. An sich selbst, an den Umständen,an Anderen. Und wenn sie nicht scheitern, dann etablieren sie sich. Auch eine Form des Scheiterns? 

Findet hier überhaupt eine Vergesellschaftung von Wohnraum statt?  Lasst euch herausfordern von Argumentationen, die euch nicht gefallen… 

Darüber wollen wir mit euch diskutieren. Und haben uns ein paar nette Gimmicks ausgedacht. 

19 Uhr Mitbringgeburtstagsbüfett und Getränke 

20 Uhr Beginn 

Barrierefreier Zugang: Der Veranstaltungsraum sowie sein Sanitärbereich sind ebenerdig erreichbar und barrierearm zugänglich. 

