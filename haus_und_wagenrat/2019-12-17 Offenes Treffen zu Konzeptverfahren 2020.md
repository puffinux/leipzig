---
id: 1057-1576609200-1576616400
title: Offenes Treffen zu Konzeptverfahren 2020
start: 2019-12-17 19:00
end: 2019-12-17 21:00
address: Zo11e, Zollschuppenstraße 11, Leipzig, 04229, Deutschland
link: https://www.hwr-leipzig.org/event/1057/
teaser: "Liebe Hausprojektinteressierte, ihr wollt ein Hausprojekt gründen –
  selbstverwaltet, kollektiv und "
isCrawled: true
---
Liebe Hausprojektinteressierte, 

ihr wollt ein Hausprojekt gründen – selbstverwaltet, kollektiv und eigentumsneutral? 

Hier kommt eure Chance: die Stadt Leipzig vergibt im nächsten Jahr wieder Grundstücke im Konzeptververfahren für kooperative und bezahlbare Wohnprojekte! 

Um Infos zu teilen, Ideen zu bündeln und sich gegenseitig auszutauschen laden wir interessierte Gruppen und Einzelpersonen zum Offenen Treffen Konzeptverfahren 2020 ein. 

— 

Hintergrundinfos: Aktuelle Vorlage der Verwaltung und von der grünen Stadtratsfraktion für die Ratsversammlung am 11.12.19 zum Thema. 

