---
id: 1030-1572886800-1572894000
title: Offene Wohnprojektberatung
start: 2019-11-04 17:00
end: 2019-11-04 19:00
address: Neuen Rathaus, Martin-Luther-Ring 4-6, Zimmer 259, Leipzig
link: https://www.hwr-leipzig.org/event/offene-wohnprojektberatung/
teaser: " Alle, die selbst ein Wohnprojekt als (Bau-)Gemeinschaft aufziehen möchten,
  sind zur 2. Offenen W"
isCrawled: true
---




 

Alle, die selbst ein Wohnprojekt als (Bau-)Gemeinschaft aufziehen möchten, sind zur 2. Offenen Wohnprojektberatung des Netzwerk Leipziger Freiheit im Neuen Rathaus, Martin-Luther-Ring 4-6, Zimmer 259 herzlich willkommen. Ein rollstuhlgerechter Zugang ist über den Seiteneingang möglich. Eine Anmeldung ist nicht erforderlich. Angesprochen sind auch MieterInnen, die zusammen ihr Haus kaufen und selbstverwalten möchten. Wer als EigentümerIn mit Wohnprojekten zusammenarbeiten möchte, findet ebenfalls hier die richtigen AnsprechpartnerInnen. 

SpezialistInnen aller Wohnprojekttypen geben an eigenen Beratungstischen wichtige Praxistipps. Wer unsicher ist, ob die GbR/WEG, die Ein-Haus-Genossenschaft, der Verein oder das Mietshäuser Syndikat die passende Rechtsform ist, kann sich einen Überblick über die Vor- und Nachteile all dieser Wohnprojektwege verschaffen. 

Da Beratende des Haus- und WagenRat bei dieser Netzwerkveranstaltung mit vor Ort dabei sein werden, entfällt die monatliche Offene Beratung seitens des Haus- und WagenRat diesen November. 

Die Veranstaltung ist kostenfrei. 





