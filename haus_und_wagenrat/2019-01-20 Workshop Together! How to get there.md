---
id: 965-1547982000-1548000000
title: Workshop Together! How to get there?
start: '2019-01-20 11:00'
end: '2019-01-20 16:00'
locationName: null
address: 'Grassimuseum, Johannisplatz 5-11, Leipzig, 04103, Deutschland'
link: 'http://hwr-leipzig.org/event/workshop-together-how-to-get-there/'
image: null
teaser: 'Zusammenwohnen gemeinsam entwerfen. Mit Dipl.-Ing. Juri Kuther (M.A.), AG Architekten Ingenieure, Le'
recurring: null
isCrawled: true
---
Zusammenwohnen gemeinsam entwerfen. Mit Dipl.-Ing. Juri Kuther (M.A.), AG Architekten Ingenieure, Leipzig. Anmeldung: email hidden; JavaScript is required

/*  */

 

Die Veranstaltung ist Teil des Rahmenprogramms der Ausstellung „Together! Die neue Architektur der Gemeinschaft“, welche noch bis zum 17.03.2019 im Grassimuseum besucht werden kann. 

Eine Übersicht über das gesamte Rahmenprogramm der Ausstellung findet sich hier. 

Die Ausstellung wird des weiteren durch eine Filmreihe ergänzt. 

