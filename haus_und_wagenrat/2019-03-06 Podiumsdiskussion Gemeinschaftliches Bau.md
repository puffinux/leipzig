---
id: 959-1551898800-1551906000
title: 'Podiumsdiskussion: Gemeinschaftliches Bauen und Wohnen: Was bringt''s der Stadt?'
start: '2019-03-06 19:00'
end: '2019-03-06 21:00'
locationName: null
address: 'Grassimuseum, Johannisplatz 5-11, Leipzig, 04103, Deutschland'
link: 'https://hwr-leipzig.org/event/podiumsdiskussion-gemeinschaftliches-bauen-und-wohnen-was-bringts-der-stadt/'
image: null
teaser: Die 150 x Wohnprojekte in Leipzig = 150 x in Gemeinschaft bauen und wohnen = ca. 1.800 x dauerhaft b
recurring: null
isCrawled: true
---
Die 150 x Wohnprojekte in Leipzig = 150 x in Gemeinschaft bauen und wohnen = ca. 1.800 x dauerhaft bezahlbare Wohnungen nach eigenen Vorstellungen. Zurück geht die Leipziger Wohnprojektkultur auf die 2000er Jahren: jenseits von Millionenbeträgen ließen sich für Gruppen sanierungsbedürftige Häuser erwerben. 

Mit dem Wachstum der Stadt hat sich das deutlich geändert. Bei den heutigen, stark steigenden Immobilienpreisen schaff en es Gruppen nur noch in Ausnahmefällen Altbauten oder Baufl ächen zu erwerben. War es das nun mit der Wohnprojektkultur in Leipzig? Nein! Das Wohnungspolitische Konzept (kurz: WoPoKo) der Stadt bietet

konkrete Unterstützungsangebote für die kooperative Wohnidee. Dazu zählen u.a. das Beratungsnetzwerk LEIPZIGER FREIHEIT und die Bereitstellung von Erbbaurechten auf städtischen Grundstücken an Baugemeinschaften im Konzeptverfahren (= das beste Konzept erhält den Zuschlag, der Preis ist fix.). 

Über diese Grundausstattung darf sich Leipzig freuen. Aber, reicht das aus? Gibt es die Akzeptanz, mehr als Stadt in die Wohnprojektkultur zu investieren? Welche Impulse gehen für die Stadt von Baugemeinschaften und jungen Genossenschaften im Gegenzug aus? 

Dazu diskutieren wir mit Ihnen und Dorothee Dubrau , Bürgermeisterin und

Beigeordnete für Stadtentwicklung und Bau der Stadt Leipzig | Andreas Hofer, Intendant der IBA 27 Stuttgart, Mitbegründer der Bau- und Wohnungsgenossenschaft Kraftwerk 1, Zürich | Veronika Hilbermann , stellv. Leiterin der Projektgruppe Planung Mitte Altona, Amt für Landesplanung und Stadtentwicklung Hamburg | Yvonne Außmann , Vorstand der jungen Genossenschaft WOGENO München eG. 

Jan Schaaf und Jens Gerhardt von der Koordinierungsstelle

NETZWERK LEIPZIGER FREIHEIT moderieren die Podiumsdiskussion. 

Ausführlichere Porträts der Podiumsgäste finden sie im Veranstaltungsflyer 

Die Veranstaltung ist Teil des Rahmenprogramms der Ausstellung „Together! Die neue Architektur der Gemeinschaft“, welche noch bis zum 17.03.2019 im Grassimuseum besucht werden kann. 

Eine Übersicht über das gesamte Rahmenprogramm der Ausstellung findet sich hier. 

Die Ausstellung wird des weiteren durch eine Filmreihe ergänzt. 

