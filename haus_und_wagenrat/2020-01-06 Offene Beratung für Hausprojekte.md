---
id: 1066-1578326400-1578331800
title: Offene Beratung für Hausprojekte
start: 2020-01-06 16:00
end: 2020-01-06 17:30
address: HWR-Laden, Georg-Schwarz-Str. 19, Leipzig
link: https://www.hwr-leipzig.org/event/offene-beratung-fuer-hausprojekte-2/
teaser: Wenn ihr euch in eurem Freundeskreis schon lange mit der Idee tragt, ein
  Hausprojekt zu gründen, wen
isCrawled: true
---
Wenn ihr euch in eurem Freundeskreis schon lange mit der Idee tragt, ein Hausprojekt zu gründen, wenn du Mitstreiter für deine eigene Initiative suchst; oder wenn ihr als Gruppe überlegt, welches der nächste Schritt ist, dann ist die offene Projektberatung die richtige Anlaufstelle. 

Die Veranstaltung bietet in einer kostenfreien Einstiegsberatung einen ersten Überblick über Herangehensweisen, Rechts- und Organisationsformen… 

Die offene Beratung beginnt am Montag, dem 6. Januar, 16 Uhr in der Georg-Schwarz-Str. 19 im ebenerdigen, nördlichen Laden. Die Runde beginnt pünktlich! 

Du kannst an diesem Termin nicht? 

Die offene Beratung findet in der Regel jeden ersten Montag im Monat statt. Da es in Ausnahmefällen zu terminlichen Verschiebungen kommen kann, informiere dich bitte immer vorher noch mal über unsere Website. Wenn du den Termin dort findest, dann findet er statt. 

Veranstaltung des Haus- und WagenRat e.V 

