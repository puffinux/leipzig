---
id: 988-1559577600-1559583000
title: 'Leipziger Westen: offene Beratung für Hausprojekte'
start: '2019-06-03 16:00'
end: '2019-06-03 17:30'
locationName: null
address: 'HWR-Laden, Georg-Schwarz-Str. 19, Leipzig'
link: 'https://hwr-leipzig.org/event/leipziger-westen-offene-beratung-fuer-hausprojekte-7/'
image: null
teaser: 'Wenn ihr euch in eurem Freundeskreis schon lange mit der Idee tragt, ein Hausprojekt zu gründen, wen'
recurring: null
isCrawled: true
---
Wenn ihr euch in eurem Freundeskreis schon lange mit der Idee tragt, ein Hausprojekt zu gründen, wenn du Mitstreiter für deine eigene Initiative suchst; oder wenn ihr als Gruppe überlegt, welches der nächste Schritt ist, dann ist die offene Projektberatung die richtige Anlaufstelle. 

Die Veranstaltung bietet in einer Einstiegsberatung einen ersten Überblick über Herangehensweisen, Rechts- und Organisationsformen… 

Die offene Beratung beginnt am Montag, dem 3. Juni, 16 Uhr in der Georg-Schwarz-Str. 19. Die Runde beginnt pünktlich! 

Veranstaltung des Haus- und WagenRat e.V. 

