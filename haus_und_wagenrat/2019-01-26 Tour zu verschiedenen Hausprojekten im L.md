---
id: 962-1548500400-1548505800
title: Tour zu verschiedenen Hausprojekten im Leipziger Westen
start: '2019-01-26 11:00'
end: '2019-01-26 12:30'
locationName: null
address: null
link: 'http://hwr-leipzig.org/event/tour-zu-verschiedenen-hausprojekten-im-leipziger-westen/'
image: null
teaser: "Mit Roman Grabolle (Haus- und WagenRat) \nTreffpunkt: Georg-Schwarz-Straße Ecke Holteistraße\_ in 0417"
recurring: null
isCrawled: true
---
Mit Roman Grabolle (Haus- und WagenRat) 

Treffpunkt: Georg-Schwarz-Straße Ecke Holteistraße  in 04177 Leipzig 

Die Veranstaltung ist Teil des Rahmenprogramms der Ausstellung „Together! Die neue Architektur der Gemeinschaft“, welche noch bis zum 17.03.2019 im Grassimuseum besucht werden kann. 

Eine Übersicht über das gesamte Rahmenprogramm der Ausstellung findet sich hier. 

Die Ausstellung wird des weiteren durch eine Filmreihe ergänzt. 

