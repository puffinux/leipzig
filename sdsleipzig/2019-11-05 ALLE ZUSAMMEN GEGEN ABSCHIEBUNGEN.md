---
id: "530196994433504"
title: ALLE ZUSAMMEN GEGEN ABSCHIEBUNGEN
start: 2019-11-05 18:00
end: 2019-11-05 20:00
locationName: Wilhelm Leuschner Platz
address: Wilhelm Leuschner  Platz, 04109 Leipzig
link: https://www.facebook.com/events/530196994433504/
image: 72631464_935934606781202_1378185963687116800_n.jpg
teaser: ----- 👇English version below 👇 ----  Am 6. November 2019 soll erneut ein
  Abschiebeflieger von einem deutschen Flughafen nach Afghanistan starten. Es
isCrawled: true
---
----- 👇English version below 👇 ----

Am 6. November 2019 soll erneut ein Abschiebeflieger von einem deutschen Flughafen nach Afghanistan starten. Es wäre der zehnte Abschiebeflug nach Kabul in diesem Jahr. 280 Menschen wurden 2019 bereits Opfer dieser menschenverachtenden Abschiebepolitik. Wir stellen uns dem entgegen und werden am Vorabend, dem 5. November 2019, um 18 Uhr mit einer Demonstration vom Wilhelm-Leuschner-Platz zum Kleinen Willy-Brandt-Platz ziehen.

Wie immer ist ungewiss, von welchem Ort der Flug starten wird. Aber ob Frankfurt, München, Düsseldorf oder Leipzig: Wir stellen uns gegen eine Abschiebepolitik, die den Tod und das Leid von Menschen billigend in Kauf nimmt! Wir stellen uns dagegen, dass Menschen ihrem Wohnort und Lebensmittelpunkt entrissen und an einen fremden, an einen gefährlichen Ort verschleppt werden. Wir stehen solidarisch an der Seite derjenigen, die hier Zuflucht suchen. Wir fordern ein sofortiges Ende der rassistischen Abschiebepolitik Deutschlands und Europas.

Der politische und gesellschaftliche Rechtsruck betrifft nicht nur Sachsen, nicht nur Deutschland. Er betrifft ganz Europa, das sich seit Jahren mehr und mehr abschottet. Seit Jahren werden die Forderungen Rechter und Rechtsradikaler umgesetzt. Innerhalb der Gesellschaft wird Geflüchteten die Teilhabe erschwert oder ganz verwehrt, sie sind Rassismus und rassistischer Gewalt ausgesetzt und müssen sich mit behördlichen Hürden plagen. Derweil werden in der Politik menschenverachtende Forderungen zu Handlungsempfehlungen, Regeln und Gesetzen. Während die Europäische Union sich weiterhin Humanität und Menschenrechte auf die Fahnen schreibt, sterben Menschen an ihren Außengrenzen, werden in Abschiebeknästen gefangen gehalten oder in Kriegsgebiete wie Afghanistan abgeschoben.

Geflüchtete erwarten in Europa statt Menschenrechte Repressionen und gewaltvolle Abschiebungen sind in Erstaufnahmeeinrichtungen, Gemeinschaftunterkünften und Belegwohnungen allgegenwärtig. Im Oktober wurde eine geflüchtete Person aus dem Landkreis Leipzig nach Afghanistan abgeschoben. Und auch im Juli wurde ein junger Mann aus seinem Leben in Grimma gerissen und unter Zwang nach Kabul geflogen.

Nichts könnte so eine Abschiebung "besser" machen. Doch dass seine Freunde berichten, dass die Polizei dabei gewaltsam vorging und auf Menschen einschlug - das macht die Lage noch schlimmer. Auch der Protest gegen Abschiebungen ist immer wieder von polizeilicher Gewalt betroffen. Polizeiliche Repression schüchtert Menschen ein, die von Abschiebung bedroht sind. Sie erschwert es außerdem der Zivilgesellschaft, den legitimen und notwendigen Protest und zivilen Ungehorsam gegen Abschiebungen auf die Straße zu tragen.

All dem können wir nur begegnen, wenn wir zusammen stehen und uns dagegen wehren: Gegen menschenverachtende Abschiebepolitik, gegen rechte Hetze und Gewalttaten, gegen Repression.

Am 5. November werden wir gemeinsam und lautstark mit einer Demonstration vom Wilhelm-Leuschner-Platz zum Kleinen Willy-Brandt-Platz ziehen. Wir hoffen auf viele Leipziger*innen, die ein entschlossenes Zeichen des Protests setzen und sich solidarisch miteinander vernetzen wollen. 


Quelle: https://www.fluechtlingsrat-bayern.de/informationen.html

------------------------------------------------------

United against deportations!

On the 6th of November, another deportation flight from Germany to Afghanistan will take off. It will be the 10th to Kabul in 2019 - 280 people already have become victims of this inhuman practice. We are taking a stand against it by marching from Wilhelm-Leuschner-Platz to Kleiner Willy-Brandt-Platz on the eve, the 5th of November. 

It is still unknown where the flight will start from, but no matter if it is Frankfurt, München, Düsseldorf or Leipzig: We condemn abducting and displacing people. We stand in solidarity with those who seek a safe place to live. For year, the right and the ultra-right were courted at the expense of refugeees whose living conditions are getting harsher and harsher. This has to stop finally! We demand an instant end to the racist deportations carried out by Germany in accordance with the European Union! 

Expecting to have human rights in Europe, refugees have to face repressions and violent deportationsinstead. In October, a person from Leipzig got deported to Afghanistan. And in July, a man was forcefully brought to Kabul. There is nothing that would make a deportation as such acceptable. What makes it even worse is that the police has beaten the person concerned, according to friends of his. Protests aginst deportations are also often acompanied by police violence. These repressions make it even harder for the civil society as well to keep up legal and necessary protest and civil disobedience.

The only way to face all of this is to stand together and resist: against all kinds of deportation, against right-winged agitation and acts of violence, against repression. This is why on November 5th we hope for many Leipzigers to join us.