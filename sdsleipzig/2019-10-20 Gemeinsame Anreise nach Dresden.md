---
id: "2413903685393497"
title: Gemeinsame Anreise nach Dresden
start: 2019-10-20 11:45
end: 2019-10-20 20:00
locationName: Leipzig Hauptbahnhof
address: Leipzig
link: https://www.facebook.com/events/2413903685393497/
image: 72351051_3076079525796909_607366495151849472_n.jpg
teaser: Am 20.10.2019 will Pegida in Dresden den fünften Geburtstag begehen. Seit fünf
  Jahren vergiftet die rassistische und fremdenfeindliche Bewegung das ge
isCrawled: true
---
Am 20.10.2019 will Pegida in Dresden den fünften Geburtstag begehen. Seit fünf Jahren vergiftet die rassistische und fremdenfeindliche Bewegung das gesellschaftliche Klima, auch über Dresden und Sachsen hinaus. Zogen sie anfangs noch mit tausenden Menschen durch die Straßen Dresdens, so sind es heute nur noch wenige Hundert, die ihren Hass und ihre Parolen durch die Straßen tragen. In anderen Städten konnte sich Pegida niemals dauerhaft etablieren. In Dresden erfolgt seit Monaten hingegen offen der Schulterschluss zwischen Pegida und der AfD. Am 07.10.19 redete z. B. der AfD-Bundestagsabgeordnete Heiko Heßenkemper auf der Kundgebung und zog dann gemeinsam mit Andre Wendt, dem neuen Vize-Landtagspräsident der AfD in Sachsen, sowie Jörg Urban, dem Vorsitzenden der AfD, mit Pegida durch Dresden. An jenem Tag äußerte Lutz Bachmann auch seine Massenmord-Phantasien darüber, wie er die "gesellschaftlichen Gräben" füllen wolle - mit seinen politischen Gegnern, sie hätten "sowieso keinen Nutzen für die Gesellschaft". 

Zwei Tage später sollte ein mit der rechtsradikalen Szene verbundene Mann in Halle einen Anschlag auf eine Synagoge und einen Döner-Imbiss in Halle begehen. Es sind Bewegungen wie Pegida und Parteien wie die AfD, welche mit ihrer Hetze und ihrem Hass den Boden für solche Gewalttaten ebnen. Umso wichtiger ist es also, uns am 20.10.19 gemeinsam Pegida in den Weg zu stellen!

Wer schweigt, stimmt zu!

Wir organisieren zusammen mit dem Bündnis Chemnitz Nazifrei eine Demo vom Hauptbahnhof zur Kundgebung von HOPE - fight racism, Herz statt Hetze, Fridays for Future Dresden, Zentrum Interkultureller Verständigung Dresden-ZIVD e.V., Dresden Nazifrei, Tolerave auf dem Neumarkt  in Hör- und Sichtweite zum Hassauflauf. 

Gemeinsame Anreise aus Leipzig 🚆🚆🚆
→ Treff  11:45 Uhr, Gleis 21 Querbahnsteig Hauptbahnhof Leipzig

