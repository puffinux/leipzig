---
id: "758930047865881"
title: "PCS: A socialist Green New Deal to connect several struggles"
start: 2019-11-26 17:15
end: 2019-11-26 18:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/758930047865881/
image: 78125430_441572290111587_6636446207214354432_n.jpg
teaser: 'Titel: "Paul Murphy, A socialist Green New Deal to connect several,
  struggles?" Referent*in: SDS Leipzig  Ort: HS 7 / Chico Mendes, (Brasilien)
  Zeit:'
isCrawled: true
---
Titel: "Paul Murphy, A socialist Green New Deal to connect several, struggles?"
Referent*in: SDS Leipzig 
Ort: HS 7 / Chico Mendes, (Brasilien)
Zeit: 17.15 Uhr -18.45 Uhr

Vortrag kontextualisiert die Vorschläge eines Green (New) Deals im internationalen und insbesondere im europäischen wirtschaftlichen Umfeld, zeigt die Hintergründe der Geldschöpfung sowie mögliche Stoßrichtungen auf, die Idee des Deals mit gerechtigkeitsorientierten Inhalten zu füllen.

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findet ihr bei studentsforfuture.info
Wir freuen uns auf euch!
