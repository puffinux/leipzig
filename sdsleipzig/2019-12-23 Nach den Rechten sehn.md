---
id: "1502344786587931"
title: Nach den Rechten sehn
start: 2019-12-23 14:00
end: 2019-12-23 19:30
address: Selneckerstraße, 04277 Leipzig
link: https://www.facebook.com/events/1502344786587931/
image: 79467640_3290933677644825_3496586820462837760_o.jpg
teaser: "#platznehmen fährt ins braune Hinterland nach Nöbeditz  🕑 Montag, den
  23.12.2019, 14 Uhr 🚗 Abfahrt im Konvoi ab Leipzig-Connewitz,
  Paul-Gerhardt-Kir"
isCrawled: true
---
#platznehmen fährt ins braune Hinterland nach Nöbeditz

🕑 Montag, den 23.12.2019, 14 Uhr
🚗 Abfahrt im Konvoi ab Leipzig-Connewitz, Paul-Gerhardt-Kirche / Selnecker Straße
#noebeditz2312

Am kommenden Montag organisiert die Initiative „AfA Connewitz“ eine Protestveranstaltung in Nöbeditz (Sachsen-Anhalt, 06667 Stößen), dem Wohnort von André Poggenburg. Diese Veranstaltung ist für alle Antifaschist*innen, die ihren Protest gegen die rechtspopulistische, rassistische, menschenverachtende und islamfeindliche Hetze zum Ausdruck bringen wollen, mit der sich Poggenburg seit Jahren zu Wort meldet.

Poggenburg hat sich durch seine unflätige völkisch-nationalistische Hetze ins politische Abseits manövriert, was ihn im März 2018 nach zwei Abmahnungen durch den AfD-Bundesvorstand und dem Vertrauens-Entzug seiner Fraktion u. a. zum Rücktritt als Vorsitzender der AfD Sachsen-Anhalt zwang. Nachdem Anfang 2019 der AfD-Bundesverband plante, Poggenburg für die Dauer von zwei Jahren von sämtlichen Parteiämtern auszuschließen, trat er aus der AfD aus und gründete unmittelbar danach eine neue Partei, den „Aufbruch deutscher Patrioten – Mitteldeutschland“ (ADPM).
Aber auch dieses politische Manöver Poggenburgs blieb erfolglos. Im August 2019 wurde bekannt, dass er aus der von ihm selbst gegründeten Partei ADPM ausgetreten war, nachdem ein Antrag zur Unterstützung der AfD und zur Selbstauflösung „seiner“ Partei auf dem ADPM-Parteitag abgelehnt worden war.

Umso erstaunlicher ist es, dass der nun parteilose #Poggenburg als Mitglied des Landtages von Sachsen-Anhalt selbst im politischen Sinkflug mit viel Tatendrang mehrere Demonstrationen im weitgehend linksgerichteten Leipziger Stadtteil #Connewitz organisiert, um sich so mit einer Handvoll rechtspopulistischer Unterstützer*innen Gehör für seine völkisch-nationalistische, rassistische, menschenverachtende und islamfeindliche Hetze zu verschaffen. Darüber hinaus gingen seine Hasstiraden auch gegen die sächsische Landtagsabgeordnete Juliane Nagel (Die Linke), die für ihre politische Arbeit für Weltoffenheit, Toleranz und ein friedliches Miteinander bekannt ist.

Das Aktionsnetzwerk „Leipzig nimmt Platz“ unterstützt die Antwort auf dieses „Engagement“ direkt in Nöbeditz, um zu zeigen, dass Poggenburgs politischer Irrgang nicht toleriert wird.

Feiern wir also gemeinsam ein antifaschistisches Weihnachten in Nöbeditz als Geste für Weltoffenheit, ein friedliches Miteinander und Toleranz!