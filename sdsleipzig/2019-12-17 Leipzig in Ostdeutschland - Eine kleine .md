---
id: "1278629582333836"
title: Leipzig in Ostdeutschland - Eine kleine politische Ökonomie
start: 2019-12-17 18:00
end: 2019-12-17 20:00
address: Hörsaal 5 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/1278629582333836/
image: 79131468_1094658017371063_7683249180290056192_n.jpg
teaser: "+++ KEW presents: WEIHNACHTSVORLESUNG +++  17.12.19 // HS 5 // 18.00  Leipzig
  in Ostdeutschland - Eine kleine politische Ökonomie  Ostdeutschland ist"
isCrawled: true
---
+++ KEW presents: WEIHNACHTSVORLESUNG +++

17.12.19 // HS 5 // 18.00

Leipzig in Ostdeutschland - Eine kleine politische Ökonomie

Ostdeutschland ist in aller Munde. Die Gründe dafür sind jedoch denkbar unschön: Mit dem Aufkommen der PEGIDA-Demonstrationen, exponierten AFD-Wahlergebnissen und breit geteilten, menschenfeindlichen Einstellungsmustern, ist Ostdeutschland erneut als Krisenfall in die öffentliche Wahrnehmung zurückgekehrt.
Um die aktuelle gesellschaftliche Dynamik in Ostdeutschland zu erklären, wird im Vortrag eine polit-ökonomische Analyse vorgestellt. Ausgangspunkt sind die politischen Entscheidungen der Wendezeit, die sich bis heute als wirkmächtig erweisen, denn Eigentumsverhältnisse – sowohl Wohneigentum als auch Produktionsmittel – und auch Arbeits- und Lohnverhältnisse sind bis heute davon bestimmt. Die Konstellation aus fehlender lokaler Bourgeoisie, kollektiv erlebter Eigentumslosigkeit und Abhängigkeit von Transferleistungen prägt das ostdeutsche Alltagsbewusstsein entscheidend mit.

In einem zweiten Teil sollen diese Befunde am Beispiel der Stadt Leipzig vertieft werden. Leipzig erscheint als Gegenpol innerhalb ostdeutscher Verhältnisse: weltoffen, experimentell und geprägt durch eine kaum einzugrenzende ‚alternative‘ Szene. Thesenhaft debattiert wird, wie sich vor dem Hintergrund der spezifischen politischen Ökonomie Ostdeutschlands dieses gesellschaftliche Ausnahme-setting herausbilden konnte.
