---
id: '301878127065006'
title: Wortspiel Live
start: '2019-10-05 20:00'
end: '2019-10-05 23:00'
locationName: Netzwerk für Demokratische Kultur
address: 'Domplatz 5, 04808 Wurzen'
link: 'https://www.facebook.com/events/301878127065006/'
image: 51718700_2033256180128838_4998929555554041856_n.jpg
teaser: 'https://www.ndk-wurzen.de/'
recurring: null
isCrawled: true
---
https://www.ndk-wurzen.de/