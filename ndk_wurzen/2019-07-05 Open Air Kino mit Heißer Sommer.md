---
id: '882859342064471'
title: Open Air Kino mit "Heißer Sommer"
start: '2019-07-05 20:00'
end: '2019-07-05 23:30'
locationName: D5 - Kultur- und Bürger_innenzentrum
address: 'Domplatz 5, 04808 Wurzen'
link: 'https://www.facebook.com/events/882859342064471/'
image: 58373501_2263763663899837_2528615661412810752_o.jpg
teaser: '„Heißer Sommer“ gehört zu den Unverwüstlichen aus der langen DEFA-Geschichte. Um den Kultfilm mit Frank Schöbel und Chris Doerk zu sehen, strömt das P'
recurring: null
isCrawled: true
---
„Heißer Sommer“ gehört zu den Unverwüstlichen aus der langen DEFA-Geschichte. Um den Kultfilm mit Frank Schöbel und Chris Doerk zu sehen, strömt das Publikum noch heute in die Kinos. Wer ihn bis heute noch nicht kennt, dem kann man nur raten: Nachholen. Denn es gibt Filme, die muss man einfach mal gesehen haben. Zum „Heißen Sommer“ (Beginn ca. 21.45 Uhr) servieren wir ab 20 Uhr kühle Cocktails.
Eintritt: 5,00 / erm. 3,00 Euro.