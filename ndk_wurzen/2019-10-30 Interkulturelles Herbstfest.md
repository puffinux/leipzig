---
id: "500355690728062"
title: Interkulturelles Herbstfest
start: 2019-10-30 16:30
end: 2019-10-30 18:30
locationName: D5 - Kultur- und Bürger_innenzentrum
address: Domplatz 5, 04808 Wurzen
link: https://www.facebook.com/events/500355690728062/
image: 67198499_2328020947474108_5303070968766267392_o.jpg
teaser: Das Unterstützer_innennetzwerk für Geflüchtete bietet zum Begegnungsfest tolle
  Herbstbasteleien für Kinder und Erwachsene an, und es gibt natürlich Le
isCrawled: true
---
Das Unterstützer_innennetzwerk für Geflüchtete bietet zum Begegnungsfest tolle Herbstbasteleien für Kinder und Erwachsene an, und es gibt natürlich Leckereien aus aller Welt. Wir zeigen zudem den 22-minütigen Dokumentarfilm „Nach Parchim - Vom Ankommen und Bleiben in der Fremde“. Wir freuen uns auf spannende Begegnungen und laden dazu herzlich ein.
Eintritt frei. Gerne können Speisen mitgebracht werden.