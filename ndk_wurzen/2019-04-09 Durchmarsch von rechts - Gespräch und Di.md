---
id: '231858651057446'
title: Durchmarsch von rechts - Gespräch und Diskurs
start: '2019-04-09 19:00'
end: '2019-04-09 21:00'
locationName: D5 - Kultur- und Bürger_innenzentrum
address: 'Domplatz 5, 04808 Wurzen'
link: 'https://www.facebook.com/events/231858651057446/'
image: 50110584_2200919140184290_5895607882442866688_n.jpg
teaser: 'Friedrich Burschel (Autor und Referent der Rosa-Luxemburg-Stiftung) und Kerstin Köditz (MdL Sachsen, Die LINKE.) werden die Verschiebung der gesellsch'
recurring: null
isCrawled: true
---
Friedrich Burschel (Autor und Referent der Rosa-Luxemburg-Stiftung) und Kerstin Köditz (MdL Sachsen, Die LINKE.) werden die Verschiebung der gesellschaftlichen Diskurse nach rechts; die Auflösung von Sagbarkeitsgrenzen und Tabus; Verschwörungstheorien, Fake News und die Zunahme von Organisationsgrad, Militanz und Gewaltbereitschaft (wie z.B. der Identitären Bewegung)  im gemeinsamen Gespräch analysieren.

Eintritt ist frei. Wir freuen uns über Spenden. 
Für Getränke ist gesorgt.