---
id: '1108868269311517'
title: Hamburger Gitter - Filmdoku 2018
start: '2019-10-02 18:30'
end: '2019-10-02 21:30'
locationName: D5 - Kultur- und Bürger_innenzentrum
address: 'Domplatz 5, 04808 Wurzen'
link: 'https://www.facebook.com/events/1108868269311517/'
image: 67060115_2328008677475335_2839646511405465600_n.jpg
teaser: 'Erzählt werden die Ereignisse rund um die Proteste gegen den G20-Gipfel im Juni 2017 an Hand von Grundrechten wie Demonstrationsfreiheit, Unschuldsver'
recurring: null
isCrawled: true
---
Erzählt werden die Ereignisse rund um die Proteste gegen den G20-Gipfel im Juni 2017 an Hand von Grundrechten wie Demonstrationsfreiheit, Unschuldsvermutung und Pressefreiheit. Dabei taucht der Film immer tiefer ein in ein Geflecht von systematischen Grundrechtsverstößen. Experten aus Wissenschaft, Politik, Medien und Polizei sowie geschädigte Demonstrationsteilnehmende beleuchten den Komplex dabei von verschiedenen Seiten. Im Anschluss sprechen wir mit dem Berliner Protestforscher Dr. Dr. Peter Ullrich von der TU Berlin. 
Eintritt: 5,00 / erm. 3,00 Euro