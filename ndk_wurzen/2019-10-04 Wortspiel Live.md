---
id: '2037103753254937'
title: Wortspiel Live
start: '2019-10-04 18:00'
end: '2019-10-04 19:30'
locationName: Projekttheater Dresden
address: 'Louisenstr. 47, 01099 Dresden'
link: 'https://www.facebook.com/events/2037103753254937/'
image: 52333390_2033259250128531_5089503084317507584_n.jpg
teaser: 'https://www.projekttheater.de/'
recurring: null
isCrawled: true
---
https://www.projekttheater.de/