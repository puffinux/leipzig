---
id: '2308227642754561'
title: Freiheit kontra Hitlerjugend - Vortrag mit Dr. Sascha Lange
start: '2019-04-08 19:00'
end: '2019-04-08 21:00'
locationName: Netzwerk für Demokratische Kultur
address: 'Domplatz 5, 04808 Wurzen'
link: 'https://www.facebook.com/events/2308227642754561/'
image: 54799691_2243796299229907_5650492209074536448_n.jpg
teaser: Im Rahmen der Ausstellung "Freiheit kontra Hitlerjugend" wird der Leipziger Historiker Sascha Lange in einem Multimediavortrag über Inhalte und Hinter
recurring: null
isCrawled: true
---
Im Rahmen der Ausstellung "Freiheit kontra Hitlerjugend" wird der Leipziger Historiker Sascha Lange in einem Multimediavortrag über Inhalte und Hintergründe der Ausstellung referieren. Er widmet sich der Geschichte der Leipziger Meuten und anderer jugendlicher Oppositionsgruppen in Sachsen und beleuchtet u.a. die Rollen der damaligen SPD und der Roten Falken, der Gewerkschaften und der Sportvereine für das linke Milieu in Leipzig während der Weimarer Republik. Auszüge aus Ermittlungsakten, Anklageschriften und Urteilen zeigen, wie irritiert der NS-Staat auf diese Jugend-Opposition reagierte und wie brutal er diejenigen verfolgte, derer er habhaft wurde.

Eintritt frei. 
Wir freuen uns über Spenden.