---
id: '557899414725955'
title: Die Leipziger Meuten - Multimediavortrag
start: '2019-03-25 19:00'
end: '2019-03-25 21:00'
locationName: D5 - Kultur- und Bürger_innenzentrum
address: 'Domplatz 5, 04808 Wurzen'
link: 'https://www.facebook.com/events/557899414725955/'
image: 50250694_2200917310184473_5530232927218040832_n.jpg
teaser: Im Rahmen der Ausstellungseröffnung wird der Leipziger Historiker Sascha Lange in einem Multimediavortrag über Inhalte und Hintergründe der Ausstellun
recurring: null
isCrawled: true
---
Im Rahmen der Ausstellungseröffnung wird der Leipziger Historiker Sascha Lange in einem Multimediavortrag über Inhalte und Hintergründe der Ausstellung referieren.

Er widmet sich der Geschichte der Leipziger Meuten und anderer jugendlicher Oppositionsgruppen in Sachsen und beleuchtet u.a. die Rollen der damaligen SPD und der Roten Falken, der Gewerkschaften und der Sportvereine für das linke Milieu in Leipzig während der Weimarer Republik. Auszüge aus Ermittlungsakten, Anklageschriften und Urteilen zeigen, wie irritiert der NS-Staat auf diese Jugend-Opposition reagierte und wie brutal er diejenigen verfolgte, derer er habhaft wurde. 

Eintritt frei. 
Wir freuen uns über Spenden.