---
id: "398392550860959"
title: Ghetto Justice vs Torch It vs  Gone Society vs False Trip
start: 2019-11-16 19:30
end: 2019-11-17 01:00
locationName: D5 - Kultur- und Bürger_innenzentrum
address: Domplatz 5, 04808 Wurzen
link: https://www.facebook.com/events/398392550860959/
image: 76702478_2419978618278340_5612108348343189504_o.jpg
teaser: Ghetto Justice (feinster Hardcore Punk aus Berlin, seit 2014, vulgär, anmaßend
  und antifaschistisch, aber immer gut gelaunt.)  Broke und Eat Me Fresh
isCrawled: true
---
Ghetto Justice (feinster Hardcore Punk aus Berlin, seit 2014, vulgär, anmaßend und antifaschistisch, aber immer gut gelaunt.)

Broke und Eat Me Fresh mussten leider absagen, aber geiler Ersatz ist da und eine Band mehr. Rockt.

Eintritt 5 - 10 Ois.

