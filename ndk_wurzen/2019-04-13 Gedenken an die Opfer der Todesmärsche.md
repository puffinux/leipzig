---
id: '647607762321904'
title: Gedenken an die Opfer der Todesmärsche
start: '2019-04-13 10:30'
end: '2019-04-13 11:30'
locationName: D5 - Kultur- und Bürger_innenzentrum
address: 'Domplatz 5, 04808 Wurzen'
link: 'https://www.facebook.com/events/647607762321904/'
image: 50299110_2200941453515392_5190434837248344064_n.jpg
teaser: Gemeinsam mit der Stadtverwaltung und der Radsportgruppe Roter Stern Leipzig gedenken wir der Opfer der Todesmärsche 1945 am Gedenkstein an den Mulden
recurring: null
isCrawled: true
---
Gemeinsam mit der Stadtverwaltung und der Radsportgruppe Roter Stern Leipzig gedenken wir der Opfer der Todesmärsche 1945 am Gedenkstein an den Muldenwiesen (Dreibrückenbad) in Wurzen.

Mit ihrer Fahrraddemonstration „Verantwortung erfahren“ touren die Radfahrer am Wochenende von Leipzig über Wurzen, Grimma und Colditz nach Chemnitz und weiter über Burgstädt und Borna zurück nach Leipzig.