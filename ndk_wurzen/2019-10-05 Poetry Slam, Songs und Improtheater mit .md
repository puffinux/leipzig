---
id: '487266251843901'
title: 'Poetry Slam, Songs und Improtheater mit Wortspiel im D5'
start: '2019-10-05 20:00'
end: '2019-10-05 22:30'
locationName: D5 - Kultur- und Bürger_innenzentrum
address: 'Domplatz 5, 04808 Wurzen'
link: 'https://www.facebook.com/events/487266251843901/'
image: 67414154_2328011237475079_1240757979561590784_n.jpg
teaser: 'In der Gruppe Wortspiel aus Oldenburg treffen am Samstag, 5. Oktober 2019 um 20 Uhr im Kultur- und Bürger_innenzentrum D5 in Wurzen drei unterschiedli'
recurring: null
isCrawled: true
---
In der Gruppe Wortspiel aus Oldenburg treffen am Samstag, 5. Oktober 2019 um 20 Uhr im Kultur- und Bürger_innenzentrum D5 in Wurzen drei unterschiedliche Kleinkunst-Genres aufeinander, die doch so einiges gemeinsam haben: Eine Bühne. Einen Abend. Spaß an der Sache. Die Offenheit für Ungeahntes, Verqueres, Anderes. Tragikomödie, Herzbruch.

Mitch (Autor/Poetry), Fabian (Singer/Songwriter), Jan und Jürgen (Impro-Theater 12 Meter Hase) kommen in diesem außergewöhnlichen Konzept zusammen und ergänzen sich auf diese ganz besondere Weise. Verbunden durch die Liebe zum Detail zeigen sie die unterschiedlichen Facetten der deutschen Sprache. In Worten gesponnen, auf einer Gitarre fabriziert, mit Händen und Füßen erklärt. Slam- und Wettkampferfahren treffen hier die Besten vom Besten zusammen. Alle drei Meister auf ihrem Gebiet und an diesem besonderen Abend alle symbiotisch miteinander verwoben. Joachim Ringelnatz hätte seine wahre Freunde daran. Eintritt: 7,00 / erm. 5,00 Euro