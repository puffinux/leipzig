---
id: '523239698164606'
title: Interkulturelles Frühlingsfest
start: '2019-04-10 16:30'
end: '2019-04-10 18:30'
locationName: D5 - Kultur- und Bürger_innenzentrum
address: 'Domplatz 5, 04808 Wurzen'
link: 'https://www.facebook.com/events/523239698164606/'
image: 50075726_2200937276849143_6080722798261567488_n.jpg
teaser: 'Wir laden herzlich ein, den Frühling zu begrüßen.  Das Knalltheater aus Leipzig wird mit unglaublich komischen improvisierten Szenen einen Riesenspaß'
recurring: null
isCrawled: true
---
Wir laden herzlich ein, den Frühling zu begrüßen. 
Das Knalltheater aus Leipzig wird mit unglaublich komischen improvisierten Szenen einen Riesenspaß auf die Bühne bringen. Es gibt Musik, Tanz und leckeres Essen. Gerne kann jeder/ jede auch etwas für das Buffet mitbringen.