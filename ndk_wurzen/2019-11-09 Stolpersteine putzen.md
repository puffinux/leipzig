---
id: "2453622101526614"
title: Stolpersteine putzen
start: 2019-11-09 18:00
end: 2019-11-09 19:00
locationName: D5 - Kultur- und Bürger_innenzentrum
address: Domplatz 5, 04808 Wurzen
link: https://www.facebook.com/events/2453622101526614/
image: 67203288_2328016374141232_1085663128500305920_n.jpg
teaser: Mit der Aktion des Stolpersteineputzens wollen wir gemeinsam mit den
  Bürgerinnen und Bürgern, wie in jedem Jahr, der ehemaligen jüdischen
  Wurzener_inn
isCrawled: true
---
Mit der Aktion des Stolpersteineputzens wollen wir gemeinsam mit den Bürgerinnen und Bürgern, wie in jedem Jahr, der ehemaligen jüdischen Wurzener_innen gedenken, welche aus unserer Stadt ins Exil vertrieben oder in den Vernichtungslagern der Nazis umgebracht wurden.  Unsere Gedanken sind bei ihnen und den Nachfahren der Familien Luchtenstein, Goldschmidt, Helft und Seligmann.

Beginn ist 18 Uhr in der Jacobsgasse. Bereits gegen 17.45 Uhr gibt es kleine Gedenken in der Dr.-Rudolf-Friedrichs-Straße und der Heinrich-Heine -Straße. 

19 Uhr findet in der Wenceslaikirche ein Friedensgebet statt, zu dem die Evangelisch-lutherische Kirchgemeinde zu Wurzen herzlich eingeladen wird.