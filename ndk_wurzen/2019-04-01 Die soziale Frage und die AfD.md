---
id: '524735161380276'
title: Die soziale Frage und die AfD
start: '2019-04-01 19:00'
end: '2019-04-01 21:00'
locationName: D5 - Kultur- und Bürger_innenzentrum
address: 'Domplatz 5, 04808 Wurzen'
link: 'https://www.facebook.com/events/524735161380276/'
image: 50170662_2200921330184071_4260506209288716288_n.jpg
teaser: 'Nachdem sich 2018 bereits Andreas Kemper dem Aufstieg der AfD und deren Strömungen widmete, wird der Hamburger Journalist Sebastian Friedrich seinen F'
recurring: null
isCrawled: true
---
Nachdem sich 2018 bereits Andreas Kemper dem Aufstieg der AfD und deren Strömungen widmete, wird der Hamburger Journalist Sebastian Friedrich seinen Fokus auf die vermeintlich soziale Politik der Partei legen.

Er liefert zudem eine kompakte Darstellung von Geschichte, Personal und Programmatik und ordnet die  Erfolge der Rechten in gesellschaftliche Entwicklung der vergangenen Jahrzehnte ein.

Eintritt ist frei. Wir freuen uns über Spenden. 
Für Getränke ist gesorgt.