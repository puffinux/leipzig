---
id: '406087976616558'
title: Freiheit kontra Hitlerjugend - Ausstellung
start: '2019-03-25 18:00'
end: '2019-03-25 19:00'
locationName: D5 - Kultur- und Bürger_innenzentrum
address: 'Domplatz 5, 04808 Wurzen'
link: 'https://www.facebook.com/events/406087976616558/'
image: 50221000_2200914966851374_4786658334616846336_n.jpg
teaser: Im Rahmen der "Internationalen Wochen gegen Rassismus" zeigen wir vom 25.3. bis 12.4.2019 die Ausstellung „Freiheit kontra Hitlerjugend – Jugendgruppe
recurring: null
isCrawled: true
---
Im Rahmen der "Internationalen Wochen gegen Rassismus" zeigen wir vom 25.3. bis 12.4.2019 die Ausstellung „Freiheit kontra Hitlerjugend – Jugendgruppen in Sachsen 1933-1945“ des Schulmuseums Leipzig.

Die Ausstellung zeigt bislang wenig bekannte oder völlig neu entdeckte Beispiele sächsischer Jugendlicher, die sich zwischen 1933 bis 1945 der Vereinnahmung durch die Nationalsozialisten entzogen und ihre Freiheit zu bewahren versuchten. 13 Tafeln machen deutlich, wie vielfältig  Nichtanpassung, Opposition und Widerstand waren. Der war keine Sache übernatürlicher Helden, sondern fand durchaus im Kleinen, in der Nachbarschaft, an Orten, an denen man es bislang nicht vermutete, statt. 
Eintritt frei.

Eröffnung am 25. März, 18 Uhr. Für Getränke und Schnittchen ist gesorgt.

Besuch von Mo-Fr, 10 – 16 Uhr oder nach Absprache.