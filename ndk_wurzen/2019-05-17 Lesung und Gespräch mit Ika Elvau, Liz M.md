---
id: '594172874377593'
title: 'Lesung und Gespräch mit Ika Elvau, Liz Micz & Sasha'
start: '2019-05-17 17:30'
end: '2019-05-17 19:30'
locationName: D5 - Kultur- und Bürger_innenzentrum
address: 'Domplatz 5, 04808 Wurzen'
link: 'https://www.facebook.com/events/594172874377593/'
image: 57398632_2146920175403312_8556346483161432064_n.jpg
teaser: 'Im Rahmen des Internationalen Tag gegen Homo-, Bi-, Trans*- und Inter*feindlichkeit (IDAHIT*) schaffen wir mit einer Aktionswoche Sichtbarkeit von Ges'
recurring: null
isCrawled: true
---
Im Rahmen des Internationalen Tag gegen Homo-, Bi-, Trans*- und Inter*feindlichkeit (IDAHIT*) schaffen wir mit einer Aktionswoche Sichtbarkeit von Geschlechtervielfalt.

17.05. Wurzen

17.00 – 19.00 Uhr // Markt
Infostand mit Redebeiträgen: https://www.facebook.com/events/386875778728134/

19.30 Uhr // Netzwerk für Demokratische Kultur e.V., Domplatz 5
Lesung und Diskussion mit Ika Elvau, Liz Micz & Sasha
Texte über die Suche einer Identität, die sich gut anfühlen kann, vom Queersein und davon, was Deutsch-sein und Nicht-Deutsch-sein bedeuten kann.

+ + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + 

Jährlich bietet der Tag gegen Homo-, Bi-, Trans*- und Inter*feindlichkeit (IDAHIT*) einen Anlass zur Erinnerung daran, dass Homosexualität erst am 17.05.1990 aus dem Krankheitskatalog der Weltgesundheitsorganisation gestrichen wurde.

Wir wollen gemeinsam mit Euch für gleiche Rechte, gesellschaftlichen Zusammenhalt und gegenseitigen Respekt von Lesben, Schwulen, Bisexuellen, trans*-, inter*geschlechtlichen und queeren Menschen sowie Asexuellen/Aromantischen (LSBTIQA*) streiten und jeder Ideologie der Ungleichwertigkeit entgegentreten.

Trans*- und Inter*geschlechtlichkeit gelten auch 2019 noch als Geschlechtsdifferenzierungs- bzw. Geschlechtsidentitätsstörungen. In Bezug auf gleichgeschlechtliche Lebensweisen ist ebenfalls noch keine volle Gleichstellung erreicht. Nicht zuletzt der aktuelle rechtskonservative Rollback und sog. „besorgte Eltern“ erfordern ein aktives Zeichen gegen Homo-, Bi-, Trans*- und Inter*feindlichkeit in Wurzen, Sachsen und überall.
Selbstbestimmt, offen und diskriminierungsfrei – eine demokratische Gesellschaft muss es allen Menschen ermöglichen, jederzeit und an jedem Ort ohne Angst verschieden sein zu können. Gewalt und Vorurteile gegenüber Lesben, Schwulen, Bisexuellen, trans*-, inter*geschlechtlichen und queeren Menschen sowie Asexuellen/Aromantischen haben keinen Platz in Sachsen.

LSBTIQA*-Rechte sind Menschenrechte!

+ + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + 

In Kooperation mit: RosaLinde Leipzig e.V., Queer durch Sachsen, Netzwerk für Demokratische Kultur

Diese Veranstaltung wird gefördert vom BMFSFJ im Rahmen des Modellprojekts "Demokratie Leben!" sowie dem Freistaat Sachsen und der Partnerschaft für Demokratie des Landkreises Leipzig.

+ + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + 

Die Aktionswoche:
13.05. Bautzen
14.05. Oschatz
15.05. Döbeln
16.05. Hoyerswerda
17.05. Wurzen
18.05. Görlitz
19.05. Zwickau
