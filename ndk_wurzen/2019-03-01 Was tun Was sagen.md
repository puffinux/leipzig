---
id: '2182042271838717'
title: Was tun? Was sagen?
start: '2019-03-01 14:00'
end: '2019-03-01 18:30'
locationName: Netzwerk für Demokratische Kultur
address: 'Domplatz 5, 04808 Wurzen'
link: 'https://www.facebook.com/events/2182042271838717/'
image: 50517224_2098561923544965_5931952819119587328_n.jpg
teaser: 'Regionaltreffen Bündnis gegen Rassismus – für ein gerechtes und menschenwürdiges Sachsen  Was können wir sagen, wenn Kolleginnen und Kollegen sich abw'
recurring: null
isCrawled: true
---
Regionaltreffen Bündnis gegen Rassismus
– für ein gerechtes und menschenwürdiges Sachsen

Was können wir sagen, wenn Kolleginnen und Kollegen sich abwertend gegenüber anderen äußern? Was sagen uns unsere eigenen Bilder im Kopf, die andere in Schubladen stecken? Und was wollen und können wir als Organisation oder Initiative tun, für mehr (gesellschaftlichen) Zusammenhalt? Was für mehr Vertrauen im eigenen Team, was, um politische Debatten, die auf unsere Arbeit abfärben, auszuhalten - oder aber im Zweifel, wenn rote Linien überschritten werden, diese klar zu benennen?
Das „Bündnis gegen Rassismus – für ein gerechtes und menschenwürdiges Sachsen“ will Lobby sein für Betroffene von Anfeindungen, bündelt Bildungsangebote und bezieht Position in der Öffentlichkeit. Es stellt sich auch immer selbst in Frage, z.B. wer unter uns profitiert von welchen Privilegien? Es umfasst über 40 sächsische Dachverbände unterschiedlichster thematischer Zielsetzungen und migrantische Selbstorganisationen. Mit unserem Regionaltreffen kommen wir zu Ihnen und Euch, wollen uns gegenseitig sensibilisieren für Formen von Diskriminierung und Rassismus, uns kennenlernen und austauschen und zusammen überlegen: Was können wir tun? Was können wir sagen? Und was denkt wer warum?

Das Seminar ist offen für alle Interessierte sowie Mitglieder der Organisationen im Bündnis gegen Rassismus!

Ansprechpartnerin: Susanne Gärtner

Anmeldungen und Nachfragen bitte an: kontakt@buendnisgegenrassismus.de



Programm
14.00 -  14.45: Wer ist da warum?
Kennenlernen und Vorstellen Vorstellung des „Bündnisses gegen Rassismus“

14.45 - 16.00: Was denkt wer warum?
1. „The Danger of the Single Story“ von Chimamanda Ngozi Adichie.
Input mit Austausch zu unseren „Bildern im Kopf“ sowie Stereotypen
2. Wie funktionieren unsere Bilder im Kopf?
Stille Diskussion: Wie aus Stereotypen mehr werden kann.
3. Wo finden diese Prozesse in meinem Tätigkeitsfeld statt?

16.00 - 16.30: Pause

16.30 -  17.15: Vertiefung: Wer sagt was wann?
Oder: Gibt es Rassismus und Diskriminierung in meiner Organisation? 

17.15 - 18.30: Sind wir viele?
Was können wir tun? Was haben wir bereits getan?
Bedarfssammlung & Lösungsansätze: Methoden, Kontaktstellen, bestehende Netzwerke, Planungsstand verschiedener Bündnisse zum Wahljahr 2019

Kontakt:
Ansprechpartnerinnen: Susanne Gärtner & Susanne Neupert
Mail: kontakt@buendnisgegenrassismus.de

