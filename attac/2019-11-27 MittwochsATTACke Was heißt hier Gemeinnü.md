---
id: "985120855186342"
title: "MittwochsATTACke: Was heißt hier Gemeinnützigkeit?"
start: 2019-11-27 18:30
end: 2019-11-27 20:30
address: Schaubühne Lindenfels
link: https://www.facebook.com/events/985120855186342/
image: 74271278_1955511244551510_1600073238442934272_o.jpg
teaser: 'MittwochsATTACke:  "Was heißt hier eigentlich gemeinnützig?" -
  Gemeinnützigkeit nach dem Urteil des Bundesfinanzhofs zu Attac", Vortrag +
  Diskussion'
isCrawled: true
---
MittwochsATTACke: 
"Was heißt hier eigentlich gemeinnützig?"
- Gemeinnützigkeit nach dem Urteil des Bundesfinanzhofs zu Attac", Vortrag + Diskussion

Welche Organisationen und Vereine können sich nach dem Urteil des Bundesfinanzhofes zu Attac noch sicher als gemeinnützig betrachten?
Wie wird Gemeinnützigkeit grundsätzlich definiert und was genau besagt das Urteil dazu?
Wie kann sich eine Aberkennung von Gemeinnützigkeit auswirken, positiv wie negativ?

Diese und weitere Fragen rund um das Thema Gemeinnützigkeit sollen bei der MittwochAttacke im November besprochen werden.
Wir freuen uns auf eine rege Diskussion. 

MittwochsATTACke am 27.11.2019, 18 Uhr 
Ort: Schaubühne Lindenfels, K.-Heine-Str. 50

Referent: Johannes Döring
(Mitglied im bundesweiten Attac-Koordinierungskreises und Mitglied der Attacinternen Arbeitsgruppe, die sich mit dem Urteil
und seinen Folgen auseinandersetzt.)

Der Eintritt ist frei.