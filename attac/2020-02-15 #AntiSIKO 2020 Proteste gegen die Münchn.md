---
id: "1216524545202896"
title: "#AntiSIKO 2020 Proteste gegen die Münchner Sicherheitskonferenz"
start: 2020-02-15 13:00
end: 2020-02-15 16:30
address: Stachus, 80335 München
link: https://www.facebook.com/events/1216524545202896/
image: 79451545_1291276427740430_782281366153723904_o.jpg
teaser: "#AntiSIKO Proteste 2020 ☮   Vom 14. bis zum 16. Februar findet die „Münchner
  Sicherheitskonferenz“ (Siko) statt.  Dort treffen sich Staats- und Regier"
isCrawled: true
---
#AntiSIKO Proteste 2020 ☮ 

Vom 14. bis zum 16. Februar findet die „Münchner Sicherheitskonferenz“ (Siko) statt. 
Dort treffen sich Staats- und Regierungschefs mit Vertretern von Großkonzernen und der Rüstungsindustrie, mit Militärs, Geheimdiensten und Politikern. 
Wenn sie von Sicherheit reden, geht es nicht – wie Konferenzleiter Wolfgang Ischinger behauptet – um die „friedliche Lösung von Konflikten“, nicht um die Sicherheit der Menschen hier und nicht um die Sicherheit der Menschen anderswo auf der Welt, sondern um die Vormachtstellung des Westens mit seinem kapitalistischen Wirtschaftssystem, das auf der Ausbeutung von Mensch und Natur basiert. 
(Auszug aus dem Aufruf)

https://www.antisiko.de/aufruf-2020/

Kommt zur Demonstration 
am Samstag, den 15. Februar 2020, um 13 Uhr 
in München am Stachus!