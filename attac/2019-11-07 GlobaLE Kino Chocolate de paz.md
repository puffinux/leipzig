---
id: "2222744557833453"
title: "GlobaLE Kino: Chocolate de paz"
start: 2019-11-07 20:00
end: 2019-11-07 22:30
address: die naTo
link: https://www.facebook.com/events/2222744557833453/
image: 64217005_3092245554120731_5380888040933687296_o.jpg
teaser: "Wann: Do 07 November 2019 20:00 - 22:30  Chocolate de paz  Kolumbien / 2017 /
  60 min / Gwen Burnyeat und Pablo Mejía Trujillo / spanisch mit dt. UT /"
isCrawled: true
---
Wann: Do 07 November 2019 20:00 - 22:30

Chocolate de paz

Kolumbien / 2017 / 60 min / Gwen Burnyeat und Pablo Mejía Trujillo / spanisch mit dt. UT / Anschließend Diskussion u.a. mit Aktivist/innen von Peace Brigades International. Eintritt frei.

Die Friedensgemeinde San José de Apartadó in Kolumbien weigert sich seit ihrer Gründung 1997 Partei für eine der im bewaffneten Konflikt beteiligten Gruppen zu ergreifen – Paramilitärs, Guerillagruppen oder die Armee – und kämpft mit gewaltfreien Mitteln für ein Leben in Frieden. Mehr als 250 Mitglieder der Friedensgemeinde und Zivilisten, die in der Gegend wohnten, sind seitdem getötet worden oder gewaltsam verschwunden.
Chocolate de Paz beschreibt die Erfahrungen der Friedensgemeinde. Er durchläuft dafür den Prozess von Anbau und Produktion von Bio-Schokolade mit Beginn der Aussaat der Kakaopflanze bis zur Verarbeitung. Kakao ist der Erzählfaden, der uns Geschichten von Gewalt und Widerstandskraft in einer Gemeinde näher bringt, die versucht, neutral gegenüber allen Gewaltakteuren des Konfliktes zu bleiben. Die Gemeinde erhielt im September 2007 den Aachener Friedenspreis.

Wo: Cinémathèque in der naTo, Karl-Liebknecht-Straße 46