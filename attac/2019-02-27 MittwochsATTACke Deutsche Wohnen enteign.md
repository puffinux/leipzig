---
id: '366197910845057'
title: 'MittwochsATTACke: Deutsche Wohnen enteignen'
start: '2019-02-27 18:00'
end: '2019-02-27 20:30'
locationName: Schaubühne Lindenfels
address: 'Karl-Heine-Str. 50, 04229 Leipzig'
link: 'https://www.facebook.com/events/366197910845057/'
image: 50464664_1532749593494346_1022550267712765952_o.jpg
teaser: 'Spekulation bekämpfen! Über das Volksbegehren «Deutsche Wohnen und Co enteignen»  Die Proteste gegen steigende Mieten, Verdrängung und Privatisierunge'
recurring: null
isCrawled: true
---
Spekulation bekämpfen!
Über das Volksbegehren «Deutsche Wohnen und Co enteignen»

Die Proteste gegen steigende Mieten, Verdrängung und Privatisierungen radikalisieren sich, denn seit Jahren treiben Wohnungskonzerne und Investoren die Kosten für Wohnraum in die
Höhe. Das Bündnis «Spekulation bekämpfen – Deutsche Wohnen und Co enteignen!» hat die Enteignung des größten Berliner Immobilienkonzerns Deutsche Wohnen (DW) und weiterer
Spekulanten zum Ziel. Hierfür legte es im vergangenen Jahr einen Kampagnenplan vor, dessen zentrales Element einen Volksentscheid zur Enteignung darstellt. Ein Ansatz, der vielleicht
auch Vorbildcharakter für uns in Leipzig haben kann?

"[..] Grund und Boden, Naturschätze und Produktionsmittel
können zum Zwecke der Vergesellschaftung [..] in Gemeineigentum oder in andere Formen der Gemeinwirtschaft überführt werden. [..]" Grundgesetz der BRD, Art 15.

MittwochsATTACke am 27.02.2019, 18 Uhr
Schaubühne Lindenfels, Karl-Heine-Str. 50

Referent: Rouzbeh Taheri (Sprecher der Initiative Mietenvolksentscheid Berlin und aktiv im Berliner Bündnis «Spekulation bekämpfen – Deutsche Wohnen & Co enteignen»)

Der Eintritt ist frei.