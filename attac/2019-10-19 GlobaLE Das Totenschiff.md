---
id: "564631214364141"
title: "GlobaLE: Das Totenschiff"
start: 2019-10-19 20:00
end: 2019-10-19 22:00
address: Haus Steinstraße e.V.
link: https://www.facebook.com/events/564631214364141/
image: 64782342_3092239557454664_310875827443073024_o.jpg
teaser: "Wann: Sa 19 Oktober 2019 20:00 - 22:00  Das Totenschiff  Lesung aus dem Werk
  B. Travens. Kein Film. Eintritt frei.  „Ich war nicht geboren, hatte kein"
isCrawled: true
---
Wann: Sa 19 Oktober 2019 20:00 - 22:00

Das Totenschiff

Lesung aus dem Werk B. Travens. Kein Film. Eintritt frei.

„Ich war nicht geboren, hatte keine Seemannskarte, konnte nie im Leben einen Pass bekommen, und jeder konnte mit mir machen, was er wollte, denn ich war ja niemand, war offiziell gar nicht auf der Welt, konnte infolgedessen auch nicht vermisst werden.“

Unter dem Pseudonym B. Traven sind in der Zeit von 1926 bis 1950 insgesamt 16 Bücher erschienen. Spannende Abenteuerromane, die sich zugleich kritisch mit Herrschaft, Ungerechtigkeit und sozialer Ungleichheit auseinandersetzen. Genau das macht das Besondere an den Werken Travens aus und begründet ihre bis heute anhaltende Aktualität.

Der Erzähler verpasst nach einem Landurlaub in Antwerpen sein Schiff. Da sein einziges Identitätsdokument, nämlich seine Seemannskarte, an Bord geblieben ist, macht er eine neue Erfahrung, nämlich ohne Papiere durch alle Maschen anerkannter gesellschaftlicher Zugehörigkeit zu fallen. Als Staatenloser geltend, wird er über Landesgrenzen abgeschoben und macht eine Irrfahrt durch Westeuropa von Belgien über die Niederlande nach Frankreich, Spanien und schließlich Portugal…

„Gegen Sturm und Wellen konnte er kämpfen, mit Farbe und mit harten Fäusten; gegen Paragraphen, Bleistifte und Papier nicht.“

In der Erzählung laufen zwei große Themenstränge zusammen und ergänzen sich zum Totenschiff, als Bild vom untergehenden Spätkapitalismus: die Verdinglichung des Menschen zum Schatten seiner Pässe und Berechtigungsnachweise. Der zweite Themenstrang zeigt die bis heute nicht stark veränderten aktuellen Profitpraktiken der kapitalistischen Seefahrt. Auch wenn das Buch vor nahezu einhundert Jahren geschrieben wurde, so findet man viele Stellen an denen man aktuellste Parallelen zur heutigen Gesellschaft ziehen kann.

Wo: Haus Steinstraße, Dachtheater (Steinstraße 18)