---
id: '2261335814081899'
title: 'Aktionsakademie 2019: Kreativen Protest lernen'
start: '2019-05-29 16:00'
end: '2019-06-02 15:00'
locationName: null
address: Augsburg
link: 'https://www.facebook.com/events/2261335814081899/'
image: 50748164_1245645202240327_8521287227764899840_o.jpg
teaser: 'DIE Veranstaltung rund um kreativen Protest: Aktionsakademie  29.5. bis 2.6. in Augsburg  Die Aktionsakademie 2019 – DIE Veranstaltung für die Aus- un'
recurring: null
isCrawled: true
---
DIE Veranstaltung rund um kreativen Protest: Aktionsakademie

29.5. bis 2.6. in Augsburg

Die Aktionsakademie 2019 – DIE Veranstaltung für die Aus- und Weiterbildung der sozial-ökologischen Bewegung in Sachen "Kreativer Protest"

* Mitveranstalter: Augsburger Öko-Sozial-Projekt, Bündnis "Aufstehen gegen Rassismus"
* Ort: Wir gastieren in der Waldorfschule. Vielen Dank ans Kollegium und die Elternschaft.
* Anmeldung: http://aktionsakademie.de

Das Angebot – organisiert vom globalisierungskritischen Netzwerk Attac und ehemaligen Teilnehmer*innen – richtet sich an Menschen aus allen Teilen der bunten "Mosaik-Linken". Egal ob Ihr aktiv seid oder es erst werden wollt, wir freuen uns über alle Menschen, die Spaß an Vielfalt, Vernetzung und Engagement haben; vom Attacie, über Aktive aus FfF, IL, BUND, Ende Gelände, Recht auf Stadt, Gewerkschaften, Menschen- und Tierrechtsbündnissen, Stadtteilinitiativen bis hin zu Antifas und was es sonst so gibt.

Ein paar Beispiele für Workshops (das genaue Programm demnächst auf http://aktionsakademie.de):
    * Politisches Straßentheater
    * Sambatrommeln
    * Clowning
    * Aktionsklettern
    * Adbusting
    * Aktionsstrategieplanung
    * Aktionstraining

Über Wichtigste über Philosophie und Ablauf erfahrt Ihr auf
ebenfalls auf der Aktionsakademie-Webseite unter "Der Ablauf".