---
id: "600610770673267"
title: "GlobaLE Kino: Ich war neunzehn"
start: 2020-05-06 19:00
end: 2020-05-06 22:00
link: https://www.facebook.com/events/600610770673267/
image: 81024334_3699650346713579_7908075127504371712_o.jpg
teaser: Sonderveranstaltung außerhalb des offiziellen globaLE Programms. Anlässlich 75
  Jahre Befreiung vom Faschismus, gemeinsame Veranstaltung des Rotfuchs L
isCrawled: true
---
Sonderveranstaltung außerhalb des offiziellen globaLE Programms. Anlässlich 75 Jahre Befreiung vom Faschismus, gemeinsame Veranstaltung des Rotfuchs Leipzig und der globaLE: 
„Ich war neunzehn“ (DDR, 1968), Film + Diskussion, Der Film erzählt die Geschichte des jungen Deutschen Gregor Hecker, der mit seinen Eltern vor den Nationalsozialisten nach Moskau geflüchtet war und nun, im Frühjahr 1945, als Leutnant der Roten Armee nach Deutschland zurückkehrt. 

Ort: Saal, Villa Davignon, Friedrich-Ebert-Str. 77. Eintritt frei.