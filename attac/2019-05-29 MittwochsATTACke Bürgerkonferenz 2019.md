---
id: '298863271035757'
title: 'MittwochsATTACke: Bürgerkonferenz 2019'
start: '2019-05-29 18:00'
end: '2019-05-29 20:00'
locationName: null
address: Schaubühne Lindenfels
link: 'https://www.facebook.com/events/298863271035757/'
image: 60322249_1682472588522045_7841647612821241856_o.jpg
teaser: 'MittwochsATTACke am 29.05.2019, 18 Uhr  In diesem Jahr finden in den Bundesländern Brandenburg, Sachsen, und Thüringen Landtagswahlen statt. Aber welc'
recurring: null
isCrawled: true
---
MittwochsATTACke am 29.05.2019, 18 Uhr

In diesem Jahr finden in den Bundesländern Brandenburg, Sachsen, und Thüringen Landtagswahlen statt. Aber welche Partei sollen die Bürgerinnen und Bürger wählen, wenn sie eine sozial gerechte, auf Frieden und Demokratie gerichtete linke Politik für unsere Länder wollen? Oft hört man die Behauptung, die derzeitige neoliberale Politik sei "alternativlos". 
Es ist an der Zeit, dass die Bürgerinnen und Bürger ihren Willen zu einer Neuorientierung der Politik in unseren Bundesländern , zum Wohle der gesamten Bevölkerung, deutlich zum Ausdruck bringen. 

Es hat sich daher, initiiert von Aufstehen-Leipzig, ein Arbeitskreis gegründet, der eine Bürgerkonferenz für Politikwandel in Leipzig organisieren und veranstalten will. 
Zur MittwochsATTACke stellt der Arbeitskreis das Konzept der Veranstaltung und den Stand der Vorbereitungen dar und möchte mit euch ins Gespräch kommen. 

Termin: 29.05.2019, 18 Uhr
Ort: Schaubühne Lindenfels (Karl-Heine-Straße 50)

Der Eintritt ist frei.