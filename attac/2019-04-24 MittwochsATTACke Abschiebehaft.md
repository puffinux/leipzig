---
id: '258534225066574'
title: 'MittwochsATTACke: Abschiebehaft'
start: '2019-04-24 18:00'
end: '2019-04-24 20:00'
locationName: null
address: Schaubühne Lindenfels
link: 'https://www.facebook.com/events/258534225066574/'
image: 53347631_1590194664416505_1061230038805905408_o.jpg
teaser: 'MittwochsATTACke: "Abschiebehaft" - 100 Jahre Repression und Beschneidung der Menschenrechte - Referent: Mark Gärtner  Nachdem verschiedentlich ein –'
recurring: null
isCrawled: true
---
MittwochsATTACke: "Abschiebehaft"
- 100 Jahre Repression und Beschneidung der Menschenrechte -
Referent: Mark Gärtner

Nachdem verschiedentlich ein – vermeintliches – Vollzugsdefizit bei Abschiebungen behauptet wurde und Bundeskanzlerin Merkel im Januar 2017 eine »nationale Kraftanstrengung« bei der Vollziehung von Abschiebungen verlangte, kündigte Bundesinnenminister Horst Seehofer im Laufe des Jahres 2018 an, das Instrumentarium der Abschiebungshaft ausbauen und extensiver nutzen zu wollen, um mehr Abschiebungen schneller durchzusetzen. In ganz Deutschland werden immer mehr Abschiebeknäste errichtet.

Wer glaubt, Abschiebehaft sei ein eher jüngerer Trend irrt – sie hat eine lange Geschichte in Deutschland. Bereits 1919 wurde die Abschiebehaft in das staatliche Repressionsinstrumentarium
gegen nicht erwünschte Menschen aufgenommen.
Damals betraf das vor allem Jüdinnen und Juden. War die Institution der Abschiebehaft schon damals nicht unumstritten, so hat sie auch heute als äußerst fragwürdig zu gelten. Als reiner Verwaltungsakt stellt sie eine Haftform ohne vorausgegangene Straftat dar. Aus
rechtsstaatlicher Perspektive ist das ein Skandal.

MittwochsATTACke am 24.04.2019, 18 Uhr
Schaubühne Lindenfels, Karl-Heine-Str. 50
Referent: Mark Gärtner (Sächsischer Flüchtlingsrat)

Der Eintritt ist frei.