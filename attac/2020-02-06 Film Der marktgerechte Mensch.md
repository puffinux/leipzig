---
id: "787191495127354"
title: "Film: Der marktgerechte Mensch"
start: 2020-02-06 18:00
end: 2020-02-06 21:00
address: Volkshaus Leipzig (Gewerkschaftssaal 5.Etage)
link: https://www.facebook.com/events/787191495127354/
image: 81280898_3692646567413957_6593933899364040704_o.jpg
teaser: "Do. 06.02.2020, 18 Uhr, Film: „Der marktgerechte Mensch“ (BRD 2020, Herdolor
  Lorenz & Leslie Franke, dt. und engl./frz. mit dt. UT) und Diskussion."
isCrawled: true
---
Do. 06.02.2020, 18 Uhr, Film: „Der marktgerechte Mensch“ (BRD 2020, Herdolor Lorenz & Leslie Franke, dt. und engl./frz. mit dt. UT) und Diskussion. 

Die Veranstaltung soll dazu dienen Erwerbslose und prekär beschäftigte Gewerkschaftsmitglieder zusammenzubringen und einen Ausgangspunkt für zukünftige Aktivitäten zu bilden. 

Organisiert vom ver.di Erwerbslosenausschuss in Kooperation mit attac Leipzig und der globaLE. 

Zum Film: 

Europa ist im Umbruch. Seit dem neuen Jahrtausend und zuletzt nach der Finanzkrise wurden neue Weichen gestellt. Die soziale Marktwirtschaft, gesellschaftliche Solidarsysteme, über Jahrzehnte erstritten, stehen zur Disposition. Besonders der Arbeitsmarkt und mit ihm die Menschen verändern sich rasant. Hier setzt der Film „Der marktgerechte Mensch“ an.

Noch vor 20 Jahren waren in Deutschland knapp zwei Drittel der Beschäftigten in einem Vollzeitjob mit Sozialversicherungspflicht. 38% sind es nur noch heute. Aktuell arbeitet bereits die Hälfte der Beschäftigten in Unsicherheit! Der Film zeigt Fahrer*nnen für Essenslieferanten, die von einem Algorithmus gesteuert werden, Beschäftigte des Einzelhandels, die auf Abruf arbeiten, Crowdworker, die auf Internet-Plattformen mit der ganzen Welt konkurrieren. Auch Menschen in bisher sicher geglaubten Arbeitsstrukturen an Universitäten erleben wir in befristeten Arbeitsverhältnissen. Hier gibt es Planungshorizonte von nur wenigen Monaten bis zu einem Jahr.
All diesen modernen Arbeitsverhältnissen ist eines gemein: Der Arbeitgeber zieht sich aus sozialen Verpflichtungen zurück und lädt das wirtschaftliche und soziale Risiko auf den Rücken der Beschäftigten. 
Fatal ist, dass all diese gezeigten Arbeits- und Lebensformen sehr oft mit sozialer Isolierung und Einsamkeit verbunden sind Symptome eines zerbrechenden Bindegewebes dieser Gesellschaft.

In einer Welt, die von Konkurrenz, Ausbeutung und uneingeschränkter Freiheit der Investoren getrieben ist, gibt es ein wesentliches Prinzip: „Race to the buttom“, der Wettbewerb um immer schlechtere Arbeitsbedingungen und niedrigere Löhne bei missachteter Menschenwürde. Der Film zeigt unter welchen Bedingungen Fahrer von osteuropäischen Subunternehmern in der LKW-Transportbranche arbeiten und Nobelketten der Textil- und Bekleidungsindustrie. Diese lassen ihre Produkte in Osteuropa von Arbeiterinnen fertigen, die 12 Stunden, 7 Tage die Woche Produkte im Akkord zusammennähen, für einen Lohn der fünffach unter dem Existenzminimum liegt. Und dennoch zieht die Karawane weiter dorthin, wo die Bedingungen für Investoren noch günstiger sind.Im Focus steht Äthiopien: Die ArbeiterInnen verdienen dort 27 Dollar im Monat, das ist die Hälfte des Lohnniveaus von Bangladesh! - Dieser Wahnsinn ist nicht alternativlos. Der Film stellt Betriebe vor, die nach dem Prinzip des Gemeinwohls wirtschaften, Beschäftigte von Lieferdiensten, die einen Betriebsrat gründen und die Kraft der Solidarität von jungen Menschen, die für einen Systemwandel eintreten. „Der marktgerechte Mensch“ ist ein Film, der Mut macht, sich einzumischen und zusammenzuschließen. Denn ein anderes Leben ist möglich.

Ort: Volkshaus, Gewerkschaftssaal 5. Etage, Karl-Liebknecht-Straße 30-32. 

Eintritt ist frei.