---
id: '433281314104845'
title: Attac-Sommerakademie 2019
start: '2019-07-31 19:30'
end: '2019-08-04 15:00'
locationName: null
address: 'Erich-Kästner-Gesamtschule, Markstraße 189, Bochum'
link: 'https://www.facebook.com/events/433281314104845/'
image: 57410693_10157490467120934_8485129530187448320_n.jpg
teaser: 'https://www.attac.de/soak  Wir leben in einer Zeit des Umbruchs – die neoliberale Dominanz in der Ökonomie und anderen Lebensbereichen trifft sich mit'
recurring: null
isCrawled: true
---
https://www.attac.de/soak

Wir leben in einer Zeit des Umbruchs – die neoliberale Dominanz in der Ökonomie und anderen Lebensbereichen trifft sich mit einem überall spürbaren gesellschaftlichen und kulturellen Rechtsruck. Linke und emanzipatorische Ideen und Kräfte dringen in dieser Situation noch schwerer durch, dabei haben globalisierungskritische Forderungen angesichts der globalen Krisen mehr denn je ihre Berechtigung.
Doch die herrschenden Eliten sind weiterhin sehr erfolgreich darin, ihre Agenda durchzusetzen – und zwar schneller, raffinierter und durchschlagskräftiger denn je.

Die kommende Attac-Sommerakademie steht unter dem Motto "Vom Leben auf Kosten anderer zum guten Leben für alle!"

Wem Fragen wie diese auf den Nägeln brennen, der findet bei der Attac-Sommerakademie Anfang August in Bochum viele Gelegenheiten zu lernen und zu diskutieren.

In fast 100 Seminaren, Workshops und Foren setzen sich die Teilnehmer*innen mit der neoliberalen Globalisierung auseinander, entwickeln Alternativen weiter und diskutieren neue Positionen. Im Fokus der Debatte stehen dabei Ansätze für eine sozialökologische Transformation sowie die Themenkomplexe Migration, Rechtsruck, autoritäre Herrschaftsformen und Demokratie. Dazu kommen Workshops, die politisches Handwerkszeug vermitteln, denn für Veränderung braucht es Wissen, Aktion und Strategie gleichermaßen!

Willst du Alternativen kennenlernen, Aktionen für eine solidarische Welt entwerfen und Lösungsstrategien entwickeln? Die Sommerakademie bietet eine entspannte Atmosphäre mit vielen gleichgesinnten, interessanten Menschen, spannenden Vorträgen, Aktionen und Workshops draußen und drinnen. Und auch das gemeinsame Feiern kommt nicht zu kurz – ein vielseitiges kulturelles Programm mit Konzerten, Kleinkunst und Ausstellungen bietet dafür einen schönen Rahmen.

Die Sommerakademie ist zentraler Treffpunkt für Interessierte, Jung und Alt, Attacies und andere Aktivist*innen gleichermaßen – wir freuen uns auf dich!