---
id: "1960004897479305"
title: Aktionsakademie 2020 in Darmstadt
start: 2020-05-20 15:00
end: 2020-05-24 12:00
locationName: Jugendhof Bessunger Forst e.V.
address: Aschaffenburger Str. 183-187, 64380 Roßdorf, Hessen
link: https://www.facebook.com/events/1960004897479305/
image: 77097322_1495146153956896_4947807483247722496_o.jpg
teaser: Die Aktionsakademie 2020 - organisiert von attac für die ganze Bewegung -
  findet in der Nähe von Darmstadt im wundervollen Jugendhof Bessunger Forst s
isCrawled: true
---
Die Aktionsakademie 2020 - organisiert von attac für die ganze Bewegung - findet in der Nähe von Darmstadt im wundervollen Jugendhof Bessunger Forst statt.

Über Himmelfahrt: 20.-24. Mai 2020

Was ist die Aktionsakademie?
DAS Weiterbildungsevent in Sachen kreative Protestformen für die sozial-ökologische und global solidarische Bewegung in ihrer ganzen Breite und Vielfalt. Unser Ziel ist die bessere Welt, unsere Themen sind so vielfältig wie unsere Herangehensweise und unsere Protestformen sollten kreativ, bunt, entschlossen, herausfordern und wirksam sein.

Und wir wollen Spaß dabei haben, darum lernen wir:

* Samba-Trommeln
* Aktionsklettern
* gewaltfreie und ungehorsame Aktion
* Straßentheater
* Adbusting/Streetart
* Fotografie und Filmen
* Basteln und Konstruieren
* Aktionen planen 
* Strategie
* undundund

Alle Infos gibt es unter www.aktionsakademie.de
Und auch hier kommt noch mehr.