---
id: "2382038252049867"
title: Hundelebensberatung – Tom Heithoff
start: 2019-11-28 20:00
end: 2019-11-28 23:30
address: Hostel & Garten Eden
link: https://www.facebook.com/events/2382038252049867/
image: 73395484_2411380345801320_987034972688744448_n.jpg
teaser: GERÄUSCHKULISSE – Gemeinsam Lauschen –  präsentiert  HUNDELEBENSBERATUNG
  Hörspiel von & mit Tom Heithoff  Diesseits und jenseits des Schreibtisches si
isCrawled: true
---
GERÄUSCHKULISSE
– Gemeinsam Lauschen –

präsentiert

HUNDELEBENSBERATUNG
Hörspiel von & mit Tom Heithoff

Diesseits und jenseits des Schreibtisches sitzen sie, die armen Hunde. Ein Hartz-4-Empfänger (der arme Hund) bekommt als Eingliederungsmaßnahme einen Köter (auch das ein armer Hund) aufgedrückt. Leider hasst er die Viecher.

Der Beamte vom Arbeitsamt (das ist jetzt schon der dritte arme Hund) muss die absurde Maßnahme rechtfertigen. "Der Hund gibt Ihrem Leben Struktur zurück!" Ein Hundepsychologe und eine Tierheimberaterin geben flankierende Auskünfte zum Thema. Und es wird klar: Unser Hundeleben wird erst durch die richtige Beratung richtig schlecht.

Skript, Musik und Regie: Tom Heithoff  

Mitwirkende: 
Sachbearbeiter – Dominik Stein 
Arbeitsloser – Lorenz Eberle 
Tierheimberaterin – Christine Winkelvoss 
Psychologischer Experte – Helmut Winkelvoss

Autorenproduktion 2009

http://www.geraeuschkulisse.org/hoerspielprogramm-naechste-veranstaltungen-in-leipzig/
