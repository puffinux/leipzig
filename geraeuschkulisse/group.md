---
name: Geräuschkulisse
website: https://www.geraeuschkulisse.org
email: kontakt@geraeuschkulisse.org
scrape:
  source: facebook
  options:
    page_id: geraeuschkulisse.leipzig
---
GERÄUSCHKULISSE liebt das Hören. Klangwelten & Geschichten, Fiktion & Wirklichkeit.