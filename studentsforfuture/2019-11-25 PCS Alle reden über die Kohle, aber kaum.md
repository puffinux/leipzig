---
id: "525622758292517"
title: "PCS: Alle reden über die Kohle, aber kaum über Kreislaufwirtsch."
start: 2019-11-25 17:00
end: 2019-11-25 18:30
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/525622758292517/
image: 78669471_442431966692286_154169354328473600_n.jpg
teaser: "Titel: Alle reden über die Kohle, aber kaum jemand über Kreislaufwirtschaft
  Referent*in: Arbeit&Leben Sachsen e.V. Ort: HS 17 / Kavous Seyed-Emami (Ir"
isCrawled: true
---
Titel: Alle reden über die Kohle, aber kaum jemand über Kreislaufwirtschaft
Referent*in: Arbeit&Leben Sachsen e.V.
Ort: HS 17 / Kavous Seyed-Emami (Iran)
Zeit: 17-18.30 Uhr

Schon eine Erhöhung der Recyclingquote deutscher Industrieunternehmen von derzeit knapp 15 Prozent auf 30 Prozent wäre mit einer jährlichen CO2-Einsparung von 60 Millionen Tonnen verbunden.

Im Plastikbereich ist diese Quote sogar rückläufig. Plastikabfälle werden immer häufiger verbrannt. Zwar haben einige amtierende Entscheidungsträger/-innen die Bedeutung der Abfallwirtschaft für den Klimaschutz in Deutschland erkannt, doch liegen in diesem Bereich Anspruch und Wirklichkeit weit auseinander.

Das angebotene Seminar zeigt den aktuellen Stand des Abfallmanagements in Deutschland auf, setzt diesen in den Kontext der globalen Herausforderung der Plastikverschmutzung und diskutiert anschließend denkbare Beiträge deutscher Industrieunternehmen für einen erfolgreichen Klimaschutz.  


 Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findest du auf studentsforfuture.info/leipzig
  
 Wir freuen uns auf euch!