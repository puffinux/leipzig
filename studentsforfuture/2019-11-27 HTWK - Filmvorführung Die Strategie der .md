---
id: "714821325593792"
title: "HTWK - Filmvorführung: Die Strategie der krummen Gurken"
start: 2019-11-25 20:00
end: 2019-11-25 22:00
locationName: HTWK Leipzig
address: Karl-Liebknecht-Str. 132, 04277 Leipzig
link: https://www.facebook.com/events/714821325593792/
image: 75603917_438893723712777_5639379840189595648_n.jpg
teaser: 'Filmvorführung: "Die Strategie der krummen Gurken"'
isCrawled: true
---
Filmvorführung: "Die Strategie der krummen Gurken"
