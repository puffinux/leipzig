---
id: "959944467717621"
title: "Klimaplan von Unten: Write-In Workshop // Public Climate School"
start: 2019-11-21 11:00
end: 2019-11-21 15:00
address: Ritterstraße 26, 04109 Leipzig, Hörsaal 17
link: https://www.facebook.com/events/959944467717621/
image: 78238997_533309293884594_1139245353778806784_o.png
teaser: Nach dem Kohlekonsens Anfang des Jahres ist das Klimapaket der Bundesregierung
  die nächste Beleidigung für alle Menschen, die sich für eine lebenswert
isCrawled: true
---
Nach dem Kohlekonsens Anfang des Jahres ist das Klimapaket der Bundesregierung die nächste Beleidigung für alle Menschen, die sich für eine lebenswerte Zukunft einsetzen. Es fördert genau die Ökonomie, die unseren Planeten, das Klima zerstört und die Menschen ausbeutet und die Menschheit in diese Krise gebracht hat. Es zeigt uns, dass die Regierung unsere Zukunft nicht retten wird. Es zeigt auch, dass nur wenn wir selbst anfangen, Antworten auf die Klimakrise zu formulieren, Maßnahmen entstehen können, die den Schutz von Mensch und Natur höher werten als die Profite von Konzernen. Doch die letzten Monate haben nicht nur gezeigt, dass die Bundesregierung nicht in der Lage ist die notwendigen Maßnahmen zu formulieren und umzusetzen. Sie haben auch gezeigt, dass Millionen von Menschen sich Gehör verschaffen und gemeinsam bereit sind zu streiken. Wir müssen also unsere Zukunft selbst in die Hand nehmen. Das wird ein langer und beschwerlicher Weg mit vielen Diskussionen und Widersprüchen, die ausgehalten und überbrückt werden müssen. Ein Weg hin zu einer Gesellschaft, in der wir Entscheidungen gemeinsam treffen, in der die Stimmen der Betroffenen besonders berücksichtigt werden, in der die Grenzen der Natur gewahrt werden, in der Menschen nicht mehr ausgebeutet werden, einer Welt des guten Lebens für alle Menschen. 

Einer der vielen Schritte auf diesem Weg ist konkrete nächste Schritte zu formulieren. Wir brauchen Maßnahmen, die die Treibhausgasemissionen massiv reduzieren und die soziale und globale Gerechtigkeit massiv erhöhen. Doch welche sind das? Welche gibt es schon? Welche müssen wir anpassen, damit sie unsere Werte widerspiegeln? 

Diese Diskussion wollen wir mit euch bei dem Write-In führen. Wir laden euch deshalb ein, gemeinsam und im Angesicht des gescheiterten Klimakabinetts der Bundesregierung an sozial gerechten Maßnahmen zu arbeiten, die notwendig sind, um unter 1,5 Grad globaler Erwärmung zu bleiben. In einem offenen und basisdemokratischen Prozess werden wir Maßnahmen für die Bereiche Mobilität, Energie, Produktion und Reproduktion, globale Gerechtigkeit, Wohn- und Raumplanung und Landwirtschaft erarbeiten.Im ersten Schritt wird dabei eine große Sammlung sinnvoller Maßnahmen entstehen. Diese können wir weiterentwickeln und daraus als Soziale Bewegungen eine gemeinsame Vision erarbeiten, einfordern und umsetzen.

Bringt ggF. eure Laptops mit