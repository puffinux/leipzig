---
id: "2197339037228547"
title: HTWK - Die Gelegenheit (Kairos) der Klimabewegung
start: 2019-11-25 16:30
end: 2019-11-25 18:00
locationName: HTWK Leipzig
address: Karl-Liebknecht-Str. 132, 04277 Leipzig
link: https://www.facebook.com/events/2197339037228547/
image: 78238443_438757453726404_5951344325878087680_n.jpg
teaser: 'Vortrag zum Thema: "Die Gelegenheit (Kairos) der Klimabewegung und der System
  Change"  Referent: Dr. Alexander Neupert-Doppler  Wo? G335'
isCrawled: true
---
Vortrag zum Thema: "Die Gelegenheit (Kairos) der Klimabewegung und der System Change"

Referent: Dr. Alexander Neupert-Doppler

Wo? G335