---
id: "487528371857696"
title: "PCS: Sozialpsychologie der Umweltkrise: Vom Wissen zum Handeln"
start: 2019-11-25 11:00
end: 2019-11-25 12:30
locationName: Städtisches Kaufhaus
address: Neumarkt 9, 04109 Leipzig
link: https://www.facebook.com/events/487528371857696/
image: 75392691_440661263536023_6559885520355196928_n.jpg
teaser: "Titel: Die Sozialpsychologie der Umweltkrise: Vom Wissen zum
  Handeln  Referent*in: Prof. Immo Fritsche Ort: Hörsaal, Psychologie im
  Städtischen Kaufha"
isCrawled: true
---
Titel: Die Sozialpsychologie der Umweltkrise: Vom Wissen zum Handeln 
Referent*in: Prof. Immo Fritsche
Ort: Hörsaal, Psychologie im Städtischen Kaufhaus
Zeit: 11.00-12.30 Uhr

*Geöffnete Veranstaltung

Umweltfreundliches Alltagshandeln Einzelner und deren Akzeptanz von Umweltschutzmaßnahmen ist wesentlich für das Gelingen einer solchen Transformation. Doch wovon hängt ökologisch motiviertes Alltagshandeln ab und wie kann es gefördert werden? Ausgehend von klassischen Erklärungen umweltgerechten Handelns, wie Einstellungen, interpersonellen Normen und persönlichen Wirksamkeitserwartungen, stellen wir darüber hinaus gehende neuere Forschungsarbeiten zur Rolle sozialer Identität vor. 

Diese Veranstaltung ist teil der Public Climate School. 
Das vollständige Programm findet ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 