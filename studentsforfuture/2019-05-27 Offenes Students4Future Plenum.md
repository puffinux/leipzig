---
id: '282964579251785'
title: Offenes Students4Future Plenum
start: '2019-05-27 17:00'
end: '2019-05-27 20:00'
locationName: UNIVERSITÄT LEIPZIG
address: 'Augustusplatz 10, 04109 Leipzig'
link: 'https://www.facebook.com/events/282964579251785/'
image: 59756764_303032643965553_5594491796949303296_n.png
teaser: 'Letztes Plenum vor dem Klimastreik und der Vollversammlung - kommt alle, bringt euch ein, den der Klimawandel wartet nicht bis euer Bachelor fertig is'
recurring: null
isCrawled: true
---
Letztes Plenum vor dem Klimastreik und der Vollversammlung - kommt alle, bringt euch ein, den der Klimawandel wartet nicht bis euer Bachelor fertig ist! Neue Leute sind mehr als willkommen..