---
id: '501637183726216'
title: Offenes Plenum Students4Future
start: '2019-10-07 17:00'
end: null
locationName: null
address: UNIVERSITÄT LEIPZIG
link: 'https://www.facebook.com/events/501637183726216/'
image: 71489981_403922487209901_45305316492967936_n.png
teaser: 'Seit Anfang April 2019 treffen sich engagierte Studierende der Hochschulen Leipzigs als „Students For Future“, um für eine klimagerechte Stadt und Uni'
recurring: null
isCrawled: true
---
Seit Anfang April 2019 treffen sich engagierte Studierende der Hochschulen Leipzigs als „Students For Future“, um für eine klimagerechte Stadt und Uni sowie einen nachhaltigen Umgang mit unserer Welt einzustehen. 
Dabei haben wir bereits viele Erfolge erreicht, dazu zählen eine studentische Vollversammlung mit 1.300 Teilnehmer*innen, die Mobilisierung zum globalen Klimastreik am 24. Mai mit insgesamt 4.500 Protestierenden sowie viele weitere Projekte. 
Doch damit ist nicht Schluss! Wir haben viele weitere Aktionen geplant, um die Gesellschaft und die Politik auf unsere Forderungen aufmerksam zu machen, so wie die Klimaaktionswoche Anfang Dezember. 
Wir laden euch herzlich ein, uns und unsere Arbeit beim offenen Plenum am 14. Oktober kennenzulernen.
Kommt vorbei und bringt eure Freunde und Bekannten mit!