---
id: "984857965187018"
title: "PCS: Locals United Leipzig stellt sich vor"
start: 2019-11-26 13:15
end: 2019-11-26 14:15
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/984857965187018/
image: 70404170_444921813109968_9181948541868703744_n.jpg
teaser: "Titel: Locals United Leipzig stellt sich vor Referent*in: Locals United
  Leipzig Ort: NSG S420, Hauptcampus Zeit: 13.15-14.15 Uhr  Locals United is a
  p"
isCrawled: true
---
Titel: Locals United Leipzig stellt sich vor
Referent*in: Locals United Leipzig
Ort: NSG S420, Hauptcampus
Zeit: 13.15-14.15 Uhr

Locals United is a project sponsored by Aktion Mensch. LU aims to bring locals together to create just and diverse spaces in our cities to face climate and environmental (in)justice using the networks of BUNDjugend and Young Friends of the Earth. Come by and get to know us better! (Deutsch/ English/ عربي)#

Diese Veranstaltung ist teil der Public Climate School. 
Das vollständige Programm findet ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 