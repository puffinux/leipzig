---
id: "1439369159552615"
title: "PCS: Dystopien in der Gegenwartsliteratur - Auszug aus Eva Horn"
start: 2019-11-26 09:00
end: 2019-11-26 11:00
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/1439369159552615/
image: 73498042_439062800362536_25679785556443136_n.jpg
teaser: "Titel: Dystopien in der Gegenwartsliteratur - Auszug aus Eva Horn: Zukunft
  als Katastrophe  Referent*in: Stephanie Bremerich  Raum: NSG 302  Uhrzeit:"
isCrawled: true
---
Titel: Dystopien in der Gegenwartsliteratur - Auszug aus Eva Horn: Zukunft als Katastrophe 
Referent*in: Stephanie Bremerich 
Raum: NSG 302 
Uhrzeit: 9 - 11 Uhr 

*Geöffnete Veranstaltung

Vorbei die Zeit, als die Zukunft ein Versprechen war. Heute ist sie  nur noch Katastrophe." 
So heißt es in Eva Horns Sachbuch "Zukunft als  Katastrophe" (2014). Am 26.11. werden 
wir uns im ersten Teil des  Seminars mit Horns Text beschäftigen und über die Bedeutung, die  
zukünftige Katastrophenszenarien in Film und Literatur für unsere  Gegenwart haben, sprechen. 
Der zweite Teil der Sitzung bietet der Public Climate School ein Forum, um über drängende Fragen des  
Klimwandels zu diskutieren.

Diese Veranstaltung ist Teil der Public Climate School.

Das vollständige Programm findet ihr unter studentsforfuture.info 

Wir freuen uns auf euch! :) 