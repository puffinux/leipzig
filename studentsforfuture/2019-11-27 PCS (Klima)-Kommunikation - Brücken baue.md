---
id: "2486473194798492"
title: "PCS: (Klima)-Kommunikation - Brücken bauen statt Gräben ziehen"
start: 2019-11-27 11:15
end: 2019-11-27 12:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/2486473194798492/
image: 76915246_439492000319616_8096674481570840576_n.jpg
teaser: "Titel: (Klima)-Kommunikation - Brücken bauen statt Gräben ziehen
  Referent*innen: Fabian Kursawe (mohio e.V. Halle) Raum: Seminarraum 1411,
  Wirtschafts"
isCrawled: true
---
Titel: (Klima)-Kommunikation - Brücken bauen statt Gräben ziehen
Referent*innen: Fabian Kursawe (mohio e.V. Halle)
Raum: Seminarraum 1411, Wirtschaftsfakultät (Grimmaische Str. 12) 
Uhrzeit: 11:15 Uhr - 12:45 Uhr 

Damit Menschen sich auf Änderungen einlassen können, müssen sie sich in ihrer „Sprache“, also mit ihren Werten angesprochen fühlen. Hierzu müssen wir unsere Werte und die, die für eine zukunftsfähige Welt notwendig sind kennen und verstehen. Außerdem setzen wir uns mit Deutungsrahmen (Framing) und anderen psychologischen Effekten außereinander und schauen uns Gelingensvoraussetzungen für eine erfolgreiche (Klima)Kommunikation an.

– Chancen und Fallen in der (Klima-) Kommunikation – Vom Framing und anderen psychologischen Effekten
– Die eigenen Ziele reflektieren und entsprechende Werte kommunizieren 

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findet Ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar

Wir freuen uns auf Euch !