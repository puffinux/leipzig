---
id: "414783265877116"
title: Nachhaltige Fahrradtour
start: 2019-10-16 15:00
end: 2019-10-16 17:00
address: "Treffpunkt: Moritzbastei"
link: https://www.facebook.com/events/414783265877116/
image: 71337993_1027078724128993_5395052520953348096_o.jpg
teaser: Mi. 16.10.19 / 15.00  Nachhaltige Fahrradtour  Students for Future /
  Treffpunkt an der Moritzbastei  Fahrradfahren ist das wohl praktischste und
  nachh
isCrawled: true
---
Mi. 16.10.19 / 15.00

Nachhaltige Fahrradtour

Students for Future / Treffpunkt an der Moritzbastei

Fahrradfahren ist das wohl praktischste und nachhaltigste Fortbewegungsmittel. Deswegen möchten wir zusammen mit euch eine Radtour durch das wunderschöne Leipzig unternehmen und euch die besten Spots für einen nachhaltigen Lebensstil zeigen.