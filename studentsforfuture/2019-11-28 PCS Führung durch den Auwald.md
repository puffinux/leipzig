---
id: "1360372940814419"
title: "PCS: Führung durch den Auwald"
start: 2019-11-28 10:00
end: 2019-11-28 12:00
address: Forstamt Leipzig Connewitz, Teichstr. 20, 04277 Leipzig
link: https://www.facebook.com/events/1360372940814419/
image: 76183111_439689716966511_4306454125903609856_n.jpg
teaser: "Führung durch den Auwald  Referent*in: Forstoberinspektor Thomas Knorr Ort:
  Forstamt in Leipzig Connewitz, Teichstraße 20, 04277 Leipzig Uhrzeit: 10.0"
isCrawled: true
---
Führung durch den Auwald

Referent*in: Forstoberinspektor Thomas Knorr
Ort: Forstamt in Leipzig Connewitz, Teichstraße 20, 04277 Leipzig
Uhrzeit: 10.00 - 12.00 Uhr

Die Führung beschäftigt sich mit der Entwicklung und Bewirtschaftungen des Leipziger Auenwaldes und berührt die Felder Naherholung, Bewirtschaftung, Verkehrssicherung, Jagd und aktuelle Probleme die sich aus den letzten beiden trockenen Sommern ergeben.

Diese Veranstaltung ist Teil der Public Climate School.
Das komplette Programm findet ihr auf studentsforfuture.info
Wir freuen uns auf euch!