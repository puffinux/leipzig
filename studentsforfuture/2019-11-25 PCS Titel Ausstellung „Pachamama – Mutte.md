---
id: "430670104522426"
title: "PCS: Titel: Ausstellung „Pachamama – Mutter Erde“ + Vernissage"
start: 2019-11-25 11:00
end: 2019-11-25 21:00
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/430670104522426/
image: 76605027_443224403279709_2210731336400896_n.jpg
teaser: "Titel: Ausstellung „Pachamama – Mutter Erde“ + Vernissage Referent*innen:
  Students for Future Leipzig Zeit: Mo, tagsüber + Vernissage 18:00  Ort: Hörs"
isCrawled: true
---
Titel: Ausstellung „Pachamama – Mutter Erde“ + Vernissage
Referent*innen: Students for Future Leipzig
Zeit: Mo, tagsüber + Vernissage 18:00 
Ort: Hörsaalgebäude

Die Künstlerin Catalina Ariza ist in Kolumbien geboren und arbeitet in Leipzig an verschiedenen Projekten für Geflüchtete, Migrant*innen und Herkunftsdeutsche in Bildungsinitiativen mit den Themen Vielfalt, Migration, Umwelt, Gleichberechtigung der Geschlechter und Diskriminierung. Unter diesen Rahmen fällt ebenfalls die Ausstellung „Mutter Erde“. Anhand dieser Metapher wird die Fortpflanzungsfähigkeit der Erde und der Frauen in Verbindung gebracht. Die Ausstellung schildert die Gemeinsamkeiten der, unter anderen feministischen, Kämpfe zur Verteidigung von Territorien und gegen die Ausbeutung und den Missbrauch von Ressourcen. Ariza ist tätig für und in der Initiative Latinxs Leipzig, eine Initiative von Migrant*innen aus Lateinamerika für die Entwicklung von kulturellen, soziopolitischen und künstlerischen Projekten und Aktivitäten.

Diese Veranstaltung ist teil der Public Climate School. 
Das vollständige Programm findet ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 
