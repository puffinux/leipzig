---
id: '2409346905947011'
title: Theoriesonntag - Mobilität in Leipzig
start: '2019-10-13 12:00'
end: '2019-10-13 18:00'
locationName: Pöge-Haus
address: 'Hedwigstraße 20, 04315 Leipzig'
link: 'https://www.facebook.com/events/2409346905947011/'
image: 70595276_1026267507543448_1407845882551861248_o.jpg
teaser: So. 13.10.19 / 12.00-18.00  Theoriesonntag - Mobilität in Leipzig  Students for Future / Pögehause  Die Klimakrise macht dir Angst und überfordert dic
recurring: null
isCrawled: true
---
So. 13.10.19 / 12.00-18.00

Theoriesonntag - Mobilität in Leipzig

Students for Future / Pögehause

Die Klimakrise macht dir Angst und überfordert dich als globale Riesenherausforderung? Doch wir können direkt vor unserer Haustür aktivistisch loslegen! Einen Sonntag lang schauen wir uns in gemütlicher Atmosphäre zunächst die Verkehrssituation in Leipzig an und überlegen zusammen mit Expert*innen, wie der Nahverkehr hier verbessert werden könnte und wie wir diese Verbesserungen gemeinsam erkämpfen können.