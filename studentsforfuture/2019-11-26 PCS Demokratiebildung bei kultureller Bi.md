---
id: "579722739464794"
title: "PCS: Demokratiebildung bei kultureller Bildung im Sachunterricht"
start: 2019-11-26 11:15
end: 2019-11-26 12:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/579722739464794/
image: 75462245_440341460234670_6640269621460992000_n.jpg
teaser: "Titel: Demokratiebildung im Kontext kultureller Bildung im Sachunterricht
  Referent*in: Prof. Rahut Ort: Marschnerstraße 31, Haus 3, Raum 25 Zeit: 11:1"
isCrawled: true
---
Titel: Demokratiebildung im Kontext kultureller Bildung im Sachunterricht
Referent*in: Prof. Rahut
Ort: Marschnerstraße 31, Haus 3, Raum 25
Zeit: 11:15 - 12:45
(wöchentlich Dienstags um 11:15 in der  Marschnerstraße 31, Haus 3, Raum 25)     

*Geöffnete Veranstaltung

Wie bilde ich mir eine Meinung? Wie viel Individualität ist in einer Masse möglich? Wo bestimmst Du mit? Welche Wirkungen gehen von Zahlen aus, wenn es um gesellschaftliche Prozesse geht? Ist es die Dauer von Protesten, die Zahl der Demonstranten, die mediale Präsenz oder die Sprache die eine Demokratie ins Wanken bringt? Haben Mehrheiten ein gemeinsames Korrektiv? Wie personenbezogen ist Macht in der Demokratie? Wie „echt“ ist unsere Demokratie?

Diese Veranstaltung ist Teil der Public Climate School.
Das vollständige Programm gibt es auf studentsforfuture.info
Wir freuen uns auf euch!