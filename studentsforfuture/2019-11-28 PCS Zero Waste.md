---
id: "2511604759122138"
title: "PCS: Zero Waste"
start: 2019-11-28 09:15
end: 2019-11-28 10:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/2511604759122138/
image: 75464222_439623686973114_352317820853288960_n.jpg
teaser: 'Titel: Public Clima Scool - Zero Waste Referent*in: Veronica "Locker&Lose"
  (Unverpackt Laden) Raum: NSG S213, Hauptcampus Uhrzeit: 09.15 -10.45 Uhr  U'
isCrawled: true
---
Titel: Public Clima Scool - Zero Waste
Referent*in: Veronica "Locker&Lose" (Unverpackt Laden)
Raum: NSG S213, Hauptcampus
Uhrzeit: 09.15 -10.45 Uhr

Unverpackt - ohne Verpackung - ohne Müll? 
Wie sieht es hinter den Kulissen im Unverpacktladen 
aus? Woher und wie werden Produkte bezogen? 
Und welche anderen Ansichten als nur 
Müllvermeidung stecken dahinter?

Die Veranstaltung ist Teil der Public Climate School.
Das vollständige Programm findet ihr auf studentsforfuture.info 
Wir freuen uns auf euch!