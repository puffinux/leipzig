---
id: '1053057308229985'
title: Offenes Plenum S4F Leipzig
start: '2019-08-20 18:00'
end: '2019-08-20 20:00'
locationName: null
address: Leipzig
link: 'https://www.facebook.com/events/1053057308229985/'
image: 67282891_353251075610376_2233037957722275840_n.jpg
teaser: 'Hallo ihr lieben Klimaaktivisten:  Auch in den Semesterferien findet unser Plenum weiter statt.  Wir treffen uns zusammen mit Fridays For Future im Ra'
recurring: null
isCrawled: true
---
Hallo ihr lieben Klimaaktivisten:

Auch in den Semesterferien findet unser Plenum weiter statt. 
Wir treffen uns zusammen mit Fridays For Future im Raum Raum Z001 des Psychologie-Instituts (Treffpunkt vor dem Konsum in der Universitätsstraße). Das Plenum beginnt immer um 18 Uhr (am 30.07. treffen wir uns ausnahmsweise bereits schon um 17 Uhr).

Jede*r ist willkommen, wir freuen uns immer über neue Gesichter! Auch Studis von anderen Hochschulen sind gerne gesehen.