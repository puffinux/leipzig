---
id: "801082493663245"
title: Offenes Plenum Students for Future Leipzig HTWK
start: 2019-11-27 17:00
end: 2019-11-27 19:00
locationName: HTWK Leipzig
address: Karl-Liebknecht-Str. 132, 04277 Leipzig
link: https://www.facebook.com/events/801082493663245/
image: 75540129_433832387552244_2974721093970952192_n.jpg
teaser: Liebe Klimaaktivist*innnen,  jeden Mittwoch findet nun auch an der HTWK ein
  wöchentliche Plenum statt. In dem Semester liegt der Fokus besonders auf d
isCrawled: true
---
Liebe Klimaaktivist*innnen, 
jeden Mittwoch findet nun auch an der HTWK ein wöchentliche Plenum statt. In dem Semester liegt der Fokus besonders auf der geplanten StudentsForFuture Public Climate School (25.11. – 29.11.2019).

Wo? LI 113
Wann? 17 Uhr jeden Mittwoch

Kommt alle vorbei!
Ladet alle eure Freund*innen ein und teilt die Veranstaltung. 
Our house is on fire!!!