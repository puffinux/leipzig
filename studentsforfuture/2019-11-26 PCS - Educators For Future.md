---
id: "3244142338960138"
title: PCS - Educators For Future
start: 2019-11-26 17:15
end: 2019-11-26 18:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/3244142338960138/
image: 75580134_439481610320655_1763278413688209408_n.jpg
teaser: 'Vortrag über "Educators For Future"  Referent*in: Sonja Liebermann Raum: HS 5
  - Chut Wutty (Kambodscha) Uhrzeit: 17:15 - 18:45   Wie Kinder und Jugend'
isCrawled: true
---
Vortrag über "Educators For Future"

Referent*in: Sonja Liebermann
Raum: HS 5 - Chut Wutty (Kambodscha)
Uhrzeit: 17:15 - 18:45


Wie Kinder und Jugendliche wirksam auf die Zukunft
vorbereiten? --> In Bildungsinstitutionen/ Netzwerken
etc. Raum für Zukunftsthemen schaffen 

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findest du auf studentsforfuture.info/leipzig
 
Wir freuen uns auf euch!
