---
id: "470969270213030"
title: "PCS: The Hilo System - Next actions towards sustainability"
start: 2019-11-27 17:15
end: 2019-11-27 18:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/470969270213030/
image: 70398358_439521583649991_8775099419633647616_n.jpg
teaser: "Titel, title : Next actions towards sustainability through alternative
  systems: The Hilo System  Referent*in: Martin Rojas  Raum, room: NSG S126,
  Haup"
isCrawled: true
---
Titel, title : Next actions towards sustainability through alternative systems: The Hilo System

Referent*in: Martin Rojas

Raum, room: NSG S126, Hauptcampus

Uhrzeit, time : 17.15 Uhr - 18.45 Uhr 

The way we grow and drink coffee is broken. We are all part of a vicious cycle of exploitation and global markets. Hilo changes this by combining business, science and social initiatives to kick-start sustainability processes. We’re bypassing the conventional global trade and providing families with a regular, livable income and sustaining educational projects that heal communities so we can escape that vicious cycle. We move from trade to a system of co-benefits for growing and drinking coffee.

This event is part of the Public Climate School. You can find the program for every single day  here: 
ttps://studentsforfuture.info/ortsgruppe/leipzig/#calendar

We're looking forward seeing you there ! 
