---
id: "1399087790253507"
title: HTWK - Madagaskar und nachhaltige Auslandsprojekte
start: 2019-11-25 09:30
end: 2019-11-25 13:00
locationName: HTWK Leipzig
address: Karl-Liebknecht-Str. 132, 04277 Leipzig
link: https://www.facebook.com/events/1399087790253507/
image: 74589769_438751400393676_691996184314642432_n.jpg
teaser: 'Vortrag zum Thema: "Madagaskar und nachhaltige Auslandsprojekte"  Referent:
  Max von Technik ohne Grenzen  Wo? N103'
isCrawled: true
---
Vortrag zum Thema: "Madagaskar und nachhaltige Auslandsprojekte"

Referent: Max von Technik ohne Grenzen

Wo? N103