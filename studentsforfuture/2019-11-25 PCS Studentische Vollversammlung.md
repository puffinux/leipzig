---
id: "997349540600210"
title: "PCS: Studentische Vollversammlung"
start: 2019-11-25 15:00
end: 2019-11-25 17:00
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/997349540600210/
image: 78590867_441250843477065_2547496140077531136_n.jpg
teaser: "Titel: Studentische Vollversammlung Referent*in: Students for Future Ort:
  Audimax, Hauptcampus Zeit: 15.00-17.00 Uhr  Diese Veranstaltung ist teil der"
isCrawled: true
---
Titel: Studentische Vollversammlung
Referent*in: Students for Future
Ort: Audimax, Hauptcampus
Zeit: 15.00-17.00 Uhr

Diese Veranstaltung ist teil der Public Climate School. 
Das vollständige Programm findet ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 