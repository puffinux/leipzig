---
id: "500630170524939"
title: "PCS: War on Nature: Kolonialismus & Naturausbeutung im 19. Jhd."
start: 2019-11-26 11:15
end: 2019-11-26 12:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/500630170524939/
image: 75521960_439066340362182_1696570910087053312_n.jpg
teaser: "War on Nature - Kolonialismus & Naturausbeutung im 19. Jhd.   Referent*in:
  Prof. Dr Welz  Raum: SR 124 Uhrzeit: 11:15 - 12:45  Diese Veranstaltung ist"
isCrawled: true
---
War on Nature - Kolonialismus & Naturausbeutung im 19. Jhd. 

Referent*in: Prof. Dr Welz 
Raum: SR 124
Uhrzeit: 11:15 - 12:45

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findest du auf studentsforfuture.info/leipzig
 
Wir freuen uns auf euch!