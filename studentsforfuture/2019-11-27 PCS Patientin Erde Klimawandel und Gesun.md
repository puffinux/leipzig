---
id: "2506712609617979"
title: "PCS: Patientin Erde Klimawandel und Gesundheit"
start: 2019-11-27 17:00
end: 2019-11-27 17:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/2506712609617979/
image: 75462312_439516853650464_728647966339891200_n.jpg
teaser: "Titel: Patient Erde Klimawandel und Gesundheit  Referent*innen: Nathalie
  Nidens + Maria Heni (Health for Future)  Raum: Phyllis Omido (Kenia) / HS14"
isCrawled: true
---
Titel: Patient Erde Klimawandel und Gesundheit

Referent*innen: Nathalie Nidens + Maria Heni (Health for Future)

Raum: Phyllis Omido (Kenia) / HS14

Uhrzeit: 17:00 Uhr - 17:45 Uhr

Ein interaktiver Workshop der Initiative Health for Future Leipzig führt in die Zusammenhänge von Klimawandel und Gesundheit ein. Er liefert eine
Verständnisgrundlage und bereitet auf die anschließende Veranstaltung vor.

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findet Ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar

Wir freuen uns auf Euch !
