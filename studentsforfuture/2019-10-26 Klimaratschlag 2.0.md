---
id: "1575664025906653"
title: Klimaratschlag 2.0
start: 2019-10-26 12:00
end: 2019-10-27 17:00
locationName: GWZ- Das Geisteswissenschaftliche Zentrum
address: Beethovenstr. 15, 04107 Leipzig
link: https://www.facebook.com/events/1575664025906653/
image: 71730516_137219767618042_7444943220519206912_n.jpg
teaser: DIREKT ANMELDEN - macht die (Betten-)planung
  einfacher:-)  https://forms.gle/DBSwqR4jBWQ3xszh6   Im November werden wir
  eine Woche lang (25.-29.11.) d
isCrawled: true
---
DIREKT ANMELDEN - macht die (Betten-)planung einfacher:-)

https://forms.gle/DBSwqR4jBWQ3xszh6


Im November werden wir eine Woche lang (25.-29.11.) die Unis zu StudentsForFuture Public Climate Schools verwandeln! Eine Woche Streik, eine Woche Klimabildung und Strategiediskussion mit ALLEN!

Wir treffen uns in Leipzig um zu planen, wie wir dieses Großprojekt stemmen wollen.

mögliche Programmpunkte:

- wie liefen die ersten 2 Wochen, wo hakt´s?
- how to Streikaufbau - was sind die nächsten Schritte ?
- Public Climate School - mögliche Gestaltung der Woche
- Ziele der Streikwoche
- AG-Arbeitsphase
- Presseschulung mit Campact


Alle weiteren Infos folgen! 
Schnell Tickets buchen (für Freitag Abend oder Samstag früh). Um 12 Uhr soll´s am Samstag losgehen. Die LeipzigerInnen kümmern sich um Schlafplätze für alle.


Wir freuen uns riesig,
lasst uns zusammen in ein Protestsemester starten!
