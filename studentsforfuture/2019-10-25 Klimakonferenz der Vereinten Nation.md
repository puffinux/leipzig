---
id: "1123616498028417"
title: Klimakonferenz der Vereinten Nation
start: 2019-10-25 18:00
end: 2019-10-25 20:00
locationName: HTWK Leipzig
address: Karl-Liebknecht-Str. 132, 04277 Leipzig
link: https://www.facebook.com/events/1123616498028417/
image: 74666228_417941179141365_1926825446855409664_o.jpg
teaser: Welche Hegemonien bestehen in der Klimadebatte der Vereinten Nation? Wie wirkt
  sich negativ oder positiv der Klimawandel auf die unterschiedlichen Län
isCrawled: true
---
Welche Hegemonien bestehen in der Klimadebatte der Vereinten Nation? Wie wirkt sich negativ oder positiv der Klimawandel auf die unterschiedlichen Länder aus? Wie Reagieren und Argumentieren die führenden Köpfe der unterschiedlichen Länder zu der Klimakrise?

Raum: 016 Lipsiusbau

Referentin: Urte Grauwinkel                                                        
