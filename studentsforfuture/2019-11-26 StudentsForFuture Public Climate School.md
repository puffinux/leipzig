---
id: "551422362270993"
title: StudentsForFuture Public Climate School
start: 2019-11-26 08:00
end: 2019-11-26 20:00
address: Germany
link: https://www.facebook.com/events/551422362270993/
image: 75557548_142476873825108_2872539385297895424_n.jpg
teaser: EINE WOCHE PUBLIC CLIMATE SCHOOL Aufruf von Students for Future zum
  Hochschulstreik vom 25.-29. November 2019  Millionen Teilnehmer*innen des
  globalen
isCrawled: true
---
EINE WOCHE PUBLIC CLIMATE SCHOOL
Aufruf von Students for Future zum Hochschulstreik vom 25.-29. November 2019

Millionen Teilnehmer*innen des globalen Klimastreiks am 20. September machen deutlich, dass die Anliegen von Fridays for Future immer mehr Gehör in der Gesellschaft finden. Im starken Gegensatz zur in der Zivilgesellschaft verbreiteten Meinung, dass endlich grundlegende Maßnahmen gegen die Klimakrise getroffen werden müssen, steht das Handeln der Politik. Das in jeder Hinsicht völlig unzureichende „Klimapaket” der deutschen Bundesregierung verdeutlicht das erneut. 

Die Dramatik der Situation macht entschlossenes, außerplanmäßiges Handeln zur Pflicht. Wir, die Studierenden von Students for Future, rufen daher für die Woche vom 25. bis zum 29.  November 2019 zur  Bestreikung  des  regulären Lehrbetriebs der Hochschulen auf. Stattdessen sollen Diskussionen, Seminare, Vorlesungen, Aktionen usw. zur Klimafrage stattfinden. Unter dem Slogan „Public Climate School” wollen wir die Universitäten für alle Teile der Gesellschaft öffnen und Lösungsansätze diskutieren. In dieser Woche sollen auch Aktionen entwickelt und umgesetzt werden, die unser Anliegen einmal mehr verdeutlichen. Dabei ist es insbesondere unser Ziel, über Schüler*innen und Studierende hinaus in Austausch mit anderen Gesellschaftsgruppen zu kommen. Mit der „Public Climate School“ wollen wir einen Ort schaffen, an dem alle mitdiskutieren können, die von der Klimakrise betroffen sind: Also alle.

Der Hochschulstreik ist nicht nur ein wirksames Druckmittel, das die Politik  zum  Handeln  verpflichten  soll, sondern  auch  ein  Labor  für  nachhaltige  Zukunftsentwürfe. So können die Hochschulen dem ihnen von der Hochschulrektorenkonferenz zugeschriebenen Auftrag gerecht werden, „als Zentren demokratischer Kultur […] zur produktiven Diskussion um die Bewältigung der großen gesellschaftlichen Herausforderungen“ beizutragen. 

Alle Infos unter: http://sff-koeln.de/