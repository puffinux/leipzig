---
id: "2762232660488106"
title: "PCS: Krieg und Frieden (SDS Leipzig)"
start: 2019-11-26 15:15
end: 2019-11-26 16:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/2762232660488106/
image: 75325927_441313876804095_5152563374705344512_n.jpg
teaser: "Titel: Krieg und Frieden Referent*in: SDS Leipzig Ort: NSG S202, Hauptcampus
  Zeit: 15.15-16.45 Uhr  Die Bundeswehr stößt allein im Inland jährlich 1,7"
isCrawled: true
---
Titel: Krieg und Frieden
Referent*in: SDS Leipzig
Ort: NSG S202, Hauptcampus
Zeit: 15.15-16.45 Uhr

Die Bundeswehr stößt allein im Inland jährlich 1,7 Millionen Tonnen CO2 aus. Sie ist damit einer der Hauptverantwortlichen für die Zerstörung unseres Planeten. Wie können wir also den Kampf gegen die Klimakrise und für den Frieden zusammendenken?


 Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findest du auf studentsforfuture.info/leipzig
  
 Wir freuen uns auf euch!
 