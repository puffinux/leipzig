---
id: "1685197454944173"
title: HTWK - Wie sehen Wetter- und Klimamodelle aus?
start: 2019-11-25 15:30
end: 2019-11-25 17:00
locationName: HTWK Leipzig
address: Karl-Liebknecht-Str. 132, 04277 Leipzig
link: https://www.facebook.com/events/1685197454944173/
image: 75642400_438747833727366_468901332258914304_n.jpg
teaser: "Vorlesung zum Thema: Wie sehen Wetter- und Klimamodelle aus? Grundlegende
  Variablen und Gleichungen  Dozent*in: Prof. Merker  Wo? G119"
isCrawled: true
---
Vorlesung zum Thema: Wie sehen Wetter- und Klimamodelle aus? Grundlegende Variablen und Gleichungen

Dozent*in: Prof. Merker

Wo? G119