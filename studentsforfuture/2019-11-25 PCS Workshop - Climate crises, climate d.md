---
id: "479372496014015"
title: "PCS: Workshop - Climate crises, climate damage"
start: 2019-11-25 13:00
end: 2019-11-25 15:00
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/479372496014015/
image: 73279247_439004437035039_5585827950265106432_n.jpg
teaser: "Titel: Climate crises, climate damage – questions of global justice and
  responsibility Referent*in: Dr. Juliane Schumacher (S4F Berlin) Ort:
  Hörsaalge"
isCrawled: true
---
Titel: Climate crises, climate damage – questions of global justice and responsibility
Referent*in: Dr. Juliane Schumacher (S4F Berlin)
Ort: Hörsaalgebäude HS16, Universität Leipzig 
Zeit: 13:00-15:00

Climate Crises, Climate Damage - Questions of global justice and responsibility 

In the UN climate negotiations but also in international law people have been discussing how to deal with these climate-related losses: can affected people seek compensation? But can everything be compensated or replaced?
This workshop will give an overview of these questions of global responsibilities and justice. We will have a look on how the topic is debated in the UN and in international law. But, more importantly, we want to use the opportunity to think and discuss about the questions of justice and responsibility in and around global warming and how they can be addressed and dealt with. 


Workshop for about 20 participants
___________________________________________________________________
This event is part of the Public Climate School. 
The entire program is available shortly on www.studentsforfuture.info. 
We're looking forward to seeing to! 