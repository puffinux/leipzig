---
id: "2496328633782813"
title: Offenes Plenum St4F-Leipzig
start: 2019-10-14 17:00
end: 2019-10-14 19:00
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/2496328633782813/
image: 72486499_406045790330904_8447972638533353472_n.png
teaser: Liebe Klimaaktivist*innnen,  jeden Montag findet das wöchentliche Plenum
  statt. In dem Semester liegt der Fokus besonders auf der geplanten StudentsFo
isCrawled: true
---
Liebe Klimaaktivist*innnen, 
jeden Montag findet das wöchentliche Plenum statt. In dem Semester liegt der Fokus besonders auf der geplanten StudentsForFuture Public Climate School (25.11.-29.11.19).

Wo? Neues Seminargebäude 302
Wann? 17 Uhr jeden Montag

Kommt alle vorbei!
Ladet alle eure Freund*innen ein und teilt die Veranstaltung. 
Our house is on fire!!!