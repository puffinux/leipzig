---
id: "440095890256327"
title: "PCS: Jazzband saou tv feat. glitch.lpz"
start: 2019-11-26 13:45
end: 2019-11-26 15:15
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/440095890256327/
teaser: "Titel: Jazzband saou tv feat. glitch.lpz Ort: HS 9 / Berta Cáceres (Honduras)
  Zeit: 13:45 - 14:45  Audio-Visuelle Live Performance aus einer Colab zwi"
isCrawled: true
---
Titel: Jazzband saou tv feat. glitch.lpz
Ort: HS 9 / Berta Cáceres (Honduras)
Zeit: 13:45 - 14:45

Audio-Visuelle Live Performance aus einer Colab zwischen HGB und HMT. Elektronisch und Akustisch. Analog und Digital. Organisch und Synthetisch.

Diese Veranstaltung ist teil der Public Climate School. 
Das vollständige Programm findet ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 