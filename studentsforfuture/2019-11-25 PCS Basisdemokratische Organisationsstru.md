---
id: "2367087290208136"
title: "PCS: Basisdemokratische Organisationsstrukturen"
start: 2019-11-25 15:15
end: 2019-11-25 16:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/2367087290208136/
image: 70617116_439523530316463_163408425982623744_n.jpg
teaser: "Titel: Basisdemokratische Organisationsstrukturen Referent*in: Felix von Knoe
  Ort: HS 17 / Kavous Seyed-Emami Zeit: 15:15 - 16:45  Basisdemokratische"
isCrawled: true
---
Titel: Basisdemokratische Organisationsstrukturen
Referent*in: Felix von Knoe
Ort: HS 17 / Kavous Seyed-Emami
Zeit: 15:15 - 16:45

Basisdemokratische Organisationsstrukturen

Gutes Zusammenleben braucht gemeinsames Entscheiden. In diesem Workshop werden Strukturen der basisdemokratischen Organisation beleuchtet und diskutiert.

Diese Veranstaltung ist Teil der Public Climate School.
Das vollständige Programm gibt es auf studentsforfuture.info
Wir freuen uns auf euch!