---
id: '2541643085859555'
title: Klima & Unistreik
start: '2019-05-24 09:00'
end: '2019-05-24 12:30'
locationName: null
address: 'UNIVERSITÄT LEIPZIG, HTWK Leipzig uvm.'
link: 'https://www.facebook.com/events/2541643085859555/'
image: 59498876_299681897633961_6420389294728282112_n.jpg
teaser: '...an Leipziger Hochschulen! Seit Monaten bestreiken Leipziger Schüler*innen Freitags die Schule. Am Freitag, den24.05.19 kurz von den Europawahlen wo'
recurring: null
isCrawled: true
---
...an Leipziger Hochschulen! Seit Monaten bestreiken Leipziger Schüler*innen Freitags die Schule. Am Freitag, den24.05.19 kurz von den Europawahlen wollen die Schüler*innen nun ein europaweites Zeichen setzen - und die Europawahl zur einer Klimawahl machen. Auch wir als Student*innen sind betroffen - denn was bringt uns unser Abschluss, wenn es keinen lebenswerten Planeten mehr gibt? Deswegen wollen wir ein Zeichen setzen und am 24.05. die Schüler*innendemo unterstützen! 

Ab 09.00 Uhr: Versammlung auf dem Innenhof am Hauptcampus, Schilder malen, Absprachen treffen, Mobilisierung für den Streik
Ab 11.30 Uhr: Kundgebung im Innenhof, Redebeiträge von verschiedensten Leuten 
12.15 Gemeinsamer Spaziergang zum Simsonplatz, wo die #fridayforfuture Demonstration startet!

Warum wollen wir Friday for future unterstützen? Ihre Kernforderung ist klar: eine Klimapolitik, welche die auf uns zukommende und bald unumkehrbare Klimakatastrophe abwendet.
Die meisten Menschen sollten verstanden haben, dass ein Umdenken in der internationalen Klimapolitik notwendig ist. Laut Global Majors Report verursachen 100 Konzerne 70 Prozent aller Treibhausgasemissionen. Das ignorante Handeln einiger weniger Konzerne und Politiker*innen bringt also gravierende Schäden mit sich, die bald nicht mehr rückgängig gemacht werden können. Wir alle werden in den nächsten Jahrzehnten dadurch gefährdet.

Außerdem leiden arme Nationen, häufig aus dem globalen Süden, bereits heute am stärksten unter den Folgen des Klimawandels, der zu einem großen Teil von (nördlich gelegenen) Industriestaaten hauptverursacht wird. Schon jetzt leben diese Industrienationen auf Kosten eines großen Teils der übrigen Welt, werden aber in Zukunft ebenfalls verstärkt von den sich ändernden Verhältnissen betroffen sein. Der Klimawandel kann nur durch verstärkte und ernst gemeinte internationale Kooperation aller Beteiligten aufgehalten werden.
Der nachhaltige Konsum des Einzelnen wird die Klimakrise nicht aufhalten können – und erst recht keine Politik, die immer wieder Kompromisse auf Kosten der Umwelt eingeht. 

Das darf nicht sein. Wir müssen alle gemeinsam auf die Straße gehen und den Druck auf die derzeitigen Regierungen erhöhen. Schüler*innen, Studierende, Arbeitnehmer*innen und nicht zuletzt Unterstützende aus der Wissenschaft. Auch die "Scientists For Future" machen deutlich: Die Einhaltung des 1,5°C-Ziels sowie der dafür nötige frühere Kohleausstieg bis zum Jahr 2030 sind für die Zukunft unseres Planeten von großer Bedeutung. Nur ein klares Umdenken und eine eindeutige Umweltfokussierung bei allen politischen Entscheidungen können die drohende Klimakatastrophe noch abwenden - also lasst uns dafür sorgen, dass die wissenschaftlichen Fakten nicht länger ignoriert werden.
Die Schüler*innen, die seit Monaten für diese Forderungen auf die Straße gehen, werden teils von höchster Stelle aus Ministerien und Politik mit Bußgeldern und Schulversäumnisanzeigen bedroht – die Schulpflichtigen brauchen unsere Unterstützung! Nicht nur die Zukunft der protestierenden Schüler*innen ist gefährdet, sondern auch unsere. Gemeinsam treten wir für Zukunftsorientierung und Klimagerechtigkeit ein!

An Hochschulen wie auch Schulen steht noch immer die Ausbildung von Kräften für den Arbeitsmarkt an erster Stelle, der Teil eines Systems ist, das Klimaschutz nicht als Zielfunktion hat. Dabei müssen sich sowohl Schulen als auch Hochschulen - als Orte der Bildung und Forschung – direkt gegen die untragbaren Entscheidungen einer Politik stellen, die aktuell wichtige wissenschaftliche Erkenntnisse ignoriert und damit nicht nur die Zukunft ihrer Generation, sondern die Zukunft aller massiv gefährdet.
Für eine Klimapolitik, die uns allen eine Zukunft ermöglicht!

Deshalb: Kommt am 24.05. mit uns allen auf die Straße. Lassen wir die Uni an diesem Tag Uni sein und setzen wir ein riesiges Zeichen für Klimagerechtigkeit in Leipzig & Weltweit.
