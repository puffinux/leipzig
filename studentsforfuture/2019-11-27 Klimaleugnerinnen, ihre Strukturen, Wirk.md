---
id: "533378340828882"
title: Klimaleugner*innen, ihre Strukturen, Wirkungen und Gefahren
start: 2019-11-27 19:00
end: 2019-11-27 21:00
locationName: HTWK Leipzig
address: Karl-Liebknecht-Str. 132, 04277 Leipzig
link: https://www.facebook.com/events/533378340828882/
image: 75521621_417952289140254_2216032193627553792_o.jpg
teaser: Trotz des seit spätestens Anfang der 1990er Jahre herrschenden Konsens in der
  Wissenschaft, der unter anderem von allen wissenschaftlichen Akademien a
isCrawled: true
---
Trotz des seit spätestens Anfang der 1990er Jahre herrschenden Konsens in der Wissenschaft, der unter anderem von allen wissenschaftlichen Akademien aller großen Industriestaaten geteilt wird, lehnen Teile der Öffentlichkeit bis heute weiterhin die Existenz der menschengemachten globalen Erwärmung ab.
Wer zählt aber zu diesem Spektrum der Klimaleugner*innen und warum leugnen Sie den Klimawandel?
Was sind ihre Hauptargumente und nach welchem Schema gehen Sie vor?

Raum:016 Lipsiusbau

Referent: Jonas Lück                                                               