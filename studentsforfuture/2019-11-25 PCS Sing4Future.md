---
id: "2413488332238433"
title: "PCS: Sing4Future"
start: 2019-11-25 16:45
end: 2019-11-25 17:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/2413488332238433/
image: 75322574_443218399946976_5394758010750894080_n.jpg
teaser: "Titel: Sing4Future Referent*innen: Students for Future Leipzig Ort: ArtSpace
  Zeit: 16.45-17.45 Uhr  Lasst uns nicht nur debattieren, streiten und verz"
isCrawled: true
---
Titel: Sing4Future
Referent*innen: Students for Future Leipzig
Ort: ArtSpace
Zeit: 16.45-17.45 Uhr

Lasst uns nicht nur debattieren, streiten und verzweifeln. Wir wollen die Klimastreikwoche besingen! Am Montag um 15:00 Uhr treffen wir uns im Artspace und lernen, nach ein paar Singübungen, gemeinsam ein paar Klima-Kanones. Danach geht es über den Campus.

Diese Veranstaltung ist teil der Public Climate School. 
Das vollständige Programm findet ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 
