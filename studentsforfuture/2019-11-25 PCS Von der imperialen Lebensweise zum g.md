---
id: "2195566040549545"
title: "PCS: Von der imperialen Lebensweise zum guten Leben für Alle"
start: 2019-11-25 17:15
end: 2019-11-25 18:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/2195566040549545/
image: 76660670_439969080271908_8772125687357112320_n.jpg
teaser: "Titel: Von der imperialen Lebensweise zum guten Leben für Alle Referent:
  Maximilian Becker (u.A. Teil des ILA Kollektivs, das die Bücher „Auf Kosten a"
isCrawled: true
---
Titel: Von der imperialen Lebensweise zum guten Leben für Alle
Referent: Maximilian Becker (u.A. Teil des ILA Kollektivs, das die Bücher „Auf Kosten anderer“ und „Das gute Leben für Alle“ herausgebracht hat)
Ort: Pua Lay Peng (Malaysia)/HS 8 
Zeit 17:15-18:45


Alle reden von Missständen und Krisen: Die Probleme sind vielen bewusst - dennoch scheint sich wenig zu ändern. Warum? Das Konzept der »Imperialen Lebensweise« erklärt, warum sich angesichts zunehmender Ungerechtigkeiten keine zukunftsweisenden Alternativen durchsetzen und ein sozial-ökologischer Wandel daher weiter auf sich warten lässt. Im Workshop wird das Konzept vorgestellt und gemeinsam Strategien hin zu einem guten Leben für Alle besprochen. 

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findet Ihr in Kürze unter www.studentsforfuture.info. Wir freuen uns auf Euch! :) 
