---
id: "2440091162882334"
title: HTWK - Sozialwirtschaft und Ökologie
start: 2019-11-26 09:30
end: 2019-11-26 11:00
locationName: HTWK Leipzig
address: Karl-Liebknecht-Str. 132, 04277 Leipzig
link: https://www.facebook.com/events/2440091162882334/
image: 74511436_438768683725281_3457831397815746560_n.jpg
teaser: 'Vortrag und Diskussionsrunde zum Thema: "Sozialwirtschaft und
  Ökologie"  Dozent*in: Prof. Dr. Fehmel  Wo? Li112  !!!Die Veranstaltung findet
  im Anschl'
isCrawled: true
---
Vortrag und Diskussionsrunde zum Thema:
"Sozialwirtschaft und Ökologie"

Dozent*in: Prof. Dr. Fehmel

Wo? Li112

!!!Die Veranstaltung findet im Anschluss von 11:15 bis 12:45 nochmal statt!!!