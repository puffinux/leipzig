---
id: "530190647735060"
title: "Podium mit Klimaakteur*innen zum Thema: AfD und Klimawandel"
start: 2019-10-15 15:00
end: 2019-10-15 17:00
address: HS8 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/530190647735060/
image: 70857761_1027072124129653_7909339007530565632_o.jpg
teaser: "Di. 15.10.19 / 15.00  Podium mit Klimaakteur*innen zum Thema: AfD und
  Klimawandel - Was bedeutet das Ergebnis der Landtagswahl?  Students for Future
  /"
isCrawled: true
---
Di. 15.10.19 / 15.00

Podium mit Klimaakteur*innen zum Thema: AfD und Klimawandel - Was bedeutet das Ergebnis der Landtagswahl?

Students for Future / HS8

Die klimaleugnenden Parteien bekommen immer mehr Stimmen, gerade in Sachsen. Wir als eine der Klimagerechtigkeitsgruppen in Leipzig möchten uns vorstellen und zusammen mit weiteren Bewegungen wie Fridays For Future, Ende Gelände und Extinction Rebellion in Form eines Podiums darüber diskutieren, welche Auswirkungen das Ergebnis der Landtagswahl auf unsere Aktionen und den Kampf für Klimagerechtigkeit hat.