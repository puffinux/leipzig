---
id: "1340936262746591"
title: "PCS: calculating the economic costs of greenhouse gas emissions"
start: 2019-11-25 11:00
end: 2019-11-25 13:00
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/1340936262746591/
image: 75610887_442405696694913_6713891047451131904_n.jpg
teaser: "Titel: Lecture on calculating the economic costs of  greenhouse gas
  emissions, Part I Referent*in: Prof. Dr. Martin Quaas Ort: Seminarraum
  13  -  I274"
isCrawled: true
---
Titel: Lecture on calculating the economic costs of 
greenhouse gas emissions, Part I
Referent*in: Prof. Dr. Martin Quaas
Ort: Seminarraum 13  -  I274 I2.001 Wirtschaftsfakultät
Zeit: 11-13 Uhr

*open lecture
Every tonne of CO2 emitted causes economic damage due to the greenhouse effect. But how can these costs be calculated? Prof. Dr. Martin Quaas (Chair of Biodiversity Economics) will try to answer this question in the lectures of the 
module "Natural Resource Use and 
Conservation Economics". 
(Basic knowledges in economics needed)


Diese Veranstaltung ist teil der Public Climate School. 
Das vollständige Programm findet ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 
