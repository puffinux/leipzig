---
id: "440216616638058"
title: "PCS: Was treibt umweltrelevantes Handeln junger Menschen an?"
start: 2019-11-26 13:15
end: 2019-11-26 14:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/440216616638058/
image: 76710685_441308470137969_8542442286749843456_n.jpg
teaser: "Titel: Was treibt kollektives umweltrelevantes Handeln junger Menschen an?
  Referent*in: Hannah Wallis Ort: HS 6 / Marina Silva (Brasilien) Zeit: 13.15"
isCrawled: true
---
Titel: Was treibt kollektives umweltrelevantes Handeln junger Menschen an?
Referent*in: Hannah Wallis
Ort: HS 6 / Marina Silva (Brasilien)
Zeit: 13.15-14.45 Uhr

Was treibt kollektives umweltrelevantes Handeln junger Menschen an? Eine Feldstudie im Rahmen der Fridays for Future Bewegung.

Diese Veranstaltung ist teil der Public Climate School. 
Das vollständige Programm findet ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 