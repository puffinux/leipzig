---
id: "725805694605784"
title: Klima-Streikauftakt HTWK
start: 2019-11-25 11:00
end: 2019-11-25 14:00
locationName: HTWK Leipzig
address: Karl-Liebknecht-Str. 132, 04277 Leipzig
link: https://www.facebook.com/events/725805694605784/
image: 75474061_438740660394750_1521633121139687424_n.jpg
teaser: Kommt zum Streikauftakt zur HTWK in den Lipsiusbau. Wir haben Glühwein und
  vieles mehr am Start.
isCrawled: true
---
Kommt zum Streikauftakt zur HTWK in den Lipsiusbau. Wir haben Glühwein und vieles mehr am Start. 