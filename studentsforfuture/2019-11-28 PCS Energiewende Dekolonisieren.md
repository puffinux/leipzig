---
id: "991400727859988"
title: "PCS: Energiewende Dekolonisieren"
start: 2019-11-28 10:00
end: 2019-11-28 12:00
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/991400727859988/
image: 72869577_439626296972853_66237578344398848_n.jpg
teaser: "Energiewende Dekolonisieren!  Referent*in: Dr. Franziska Müller Ort: Chut
  Wutty (Kambodscha)/HS 5  Uhrzeit: 10.00 - 12.00 Uhr  Energiewende kommt an –"
isCrawled: true
---
Energiewende Dekolonisieren!

Referent*in: Dr. Franziska Müller
Ort: Chut Wutty (Kambodscha)/HS 5 
Uhrzeit: 10.00 - 12.00 Uhr

Energiewende kommt an – auch und gerade im globalen Süden. Viele afrikanische Staaten haben grüne Energiepolitiken realisiert, inszenieren sich als Standorte für Solar- und Windenergie und wenden sich von fossilen Wirtschaftsweisen ab. Doch wer gestaltet solche Transformationsprozesse? Inwieweit sind sie durch neokoloniale Muster geprägt und an den Interessen grüner Investoren ausgerichtet? Inwieweit ergeben sich Anknüpfungspunkte für soziale Bewegungen und für eine Aneignung von Transformationsprozessen? Um solche Prozesse kritisch zu betrachten, bietet das Konzept der „energy justice“ einen geeigneten Rahmen. Energy justice fragt, inwieweit Energiewende auch Kriterien sozialer und ökologischer Gerechtigkeit genügt. Gemeinsam können wir überlegen, worauf es ankommt, damit Energiewendeprozesse gerecht verlaufen, was eine dekoloniale Perspektive hier ans Licht bringt und welche Formen und Praxen transnationale Solidarität annehmen könnte.Der Workshop stellt Fallstudien aus Südafrika und Sambia vor und diskutiert diese aus machtkritischer und dekolonialer Perspektive. In beiden Ländern finden derzeit grüne Transformationsprozesse statt, die zwar – blickt man nur auf den Energiemix – recht erfolgreich erscheinen, aber auch mit sozial-ökologischen Problematiken einhergehen.

Diese Veranstatung ist Teil der Public Climate School.
Das vollstöndige Programm findest du auf studentsforfuture.info
Wir freuen uns auf euch!