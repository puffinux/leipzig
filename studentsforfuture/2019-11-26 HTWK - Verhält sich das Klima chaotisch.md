---
id: "2366905906894378"
title: HTWK - Verhält sich das Klima chaotisch?
start: 2019-11-26 13:30
end: 2019-11-26 16:00
locationName: HTWK Leipzig
address: Karl-Liebknecht-Str. 132, 04277 Leipzig
link: https://www.facebook.com/events/2366905906894378/
image: 76615056_438778697057613_1768396391207403520_n.jpg
teaser: 'Vorlesung zum Thema: "Verhält sich das Klima chaotisch? Von der
  Strömungsmechanik zum Lorenz-Attraktor"  Dozent*in: Prof. Merker  Wo? WE4'
isCrawled: true
---
Vorlesung zum Thema:
"Verhält sich das Klima chaotisch? Von der Strömungsmechanik zum Lorenz-Attraktor"

Dozent*in: Prof. Merker

Wo? WE4