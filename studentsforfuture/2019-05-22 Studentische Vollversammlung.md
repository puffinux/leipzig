---
id: '765156037214311'
title: Studentische Vollversammlung
start: '2019-05-22 15:00'
end: '2019-05-22 18:00'
locationName: UNIVERSITÄT LEIPZIG
address: 'Augustusplatz 10, 04109 Leipzig'
link: 'https://www.facebook.com/events/765156037214311/'
image: 60456748_306456176956533_4377042244125851648_n.jpg
teaser: 'Seit mindestens sieben Jahren gibt es sie das erste Mal wieder an der Uni Leipzig: die von der Studierendenschaft einberufene Vollversammlung. Das ist'
recurring: null
isCrawled: true
---
Seit mindestens sieben Jahren gibt es sie das erste Mal wieder an der Uni Leipzig: die von der Studierendenschaft einberufene Vollversammlung. Das ist eine Instrument der studentischen Selbstverwaltung, welches Entscheidungen mit empfehlenden Charakter für die Universität beschließen kann. Wir haben uns zu diesem Schritt entschieden,  um über ein Thema zu sprechen, das seit Monaten in aller Munde ist. Die Rede ist von den Schüler*innen von Fridays For Future und ihren Forderung nach einer Umweltpolitik, die die Klimakrise ernst nimmt und handelt.
Seit Jahren ignorieren Entscheidungsträger*innen, dass Wissenschaftler*innen aus der ganzen Welt vor einer eintretenden Klimakatastrophe warnen, die bald unumkehrbar ist. Doch noch ist dieser Tag nicht erreicht! Weil es genauso unsere Zukunft ist, lassen wir die Schüler*innen nicht alleine und kämpfen mit Ihnen gemeinsam für Klimagerechtigkeit. Folglich nehmen wir unser Recht auf eine studentische Vollversammlung wahr, um mit euch gemeinsam Forderungen an die Uni zu beschließen.
Deshalb seid mit dabei, um direkte Demokratie zu leben und zu zeigen, dass auch wir – wie die Schüler*innen - uns bewusst sind, was es heißt, Verantwortung zu übernehmen.