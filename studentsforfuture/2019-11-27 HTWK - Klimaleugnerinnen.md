---
id: "1011101615890244"
title: HTWK - Klimaleugner*innen
start: 2019-11-27 19:00
end: 2019-11-27 20:00
locationName: HTWK Leipzig
address: Karl-Liebknecht-Str. 132, 04277 Leipzig
link: https://www.facebook.com/events/1011101615890244/
image: 72463410_438903563711793_5146256941080641536_n.jpg
teaser: 'Vortrag zum Thema: "Klimaleugner*innen und ihre Strukturen, Wirkungen und
  Gefahren"  Referent: Jonas Lück  Wo? Li016'
isCrawled: true
---
Vortrag zum Thema:
"Klimaleugner*innen und ihre Strukturen, Wirkungen und Gefahren"

Referent: Jonas Lück

Wo? Li016