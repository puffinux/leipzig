---
id: "424302064882364"
title: "PCS: Klimagerechtigkeit und Rechtsphilosophie"
start: 2019-11-25 11:15
end: 2019-11-25 12:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/424302064882364/
image: 78067837_440645740204242_4982774302358110208_n.jpg
teaser: "Titel: Klimagerechtigkeit und Rechtsphilosophie Referent*in: Dr. Klesczewski
  Raum: HS 16 / Samir Flores Soberanes (Mexiko)  Zeit: 11.15-12.45 Uhr  Die"
isCrawled: true
---
Titel: Klimagerechtigkeit und Rechtsphilosophie
Referent*in: Dr. Klesczewski
Raum: HS 16 / Samir Flores Soberanes (Mexiko) 
Zeit: 11.15-12.45 Uhr

Diese Veranstaltung ist teil der Public Climate School. 
Das vollständige Programm findet ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 
