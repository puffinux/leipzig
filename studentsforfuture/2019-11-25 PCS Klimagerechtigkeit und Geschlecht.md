---
id: "1423001587860315"
title: "PCS: Klimagerechtigkeit und Geschlecht"
start: 2019-11-25 17:15
end: 2019-11-25 18:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/1423001587860315/
image: 77339129_441269256808557_3204724658620334080_n.jpg
teaser: "Titel: Klimagerechtigkeit und Geschlecht: Wieso sind Frauen* stärker von der
  Klimakrise betroffen? Referentin: Malu Tello  Raum: HS 6 / Marina Silva"
isCrawled: true
---
Titel: Klimagerechtigkeit und Geschlecht: Wieso sind Frauen* stärker von der Klimakrise betroffen?
Referentin: Malu Tello 
Raum: HS 6 / Marina Silva 
Zeit: 17:15-18:45 Uhr

Wieso muss Gendergerechtigkeit in Klimapolitiken mitgedacht werden? Wie wirkt sich der Klimawandel auf Frauen* und andere marginalisierte Gruppen aus? Welche Aspekte von Tender und welche gesellschaftlichen Strukturen führten zum Klimawandel?
Klimawandel und Umweltzerstörung verstärken bereits bestehende soziale Ungleichheiten. Von den Konsequenzen der Umweltkatastrophen sind in erster Linie Frauen*, Kinder und indigene Bevölkerungen in Ländern des Globalen Südens betroffen Diese wissenschaftlich belegten Tatsachen zeigen, dass die Klimakrise nicht geschlechtsneutral und eng mit patriarchalen Strukturen des Kapitalismus verwoben ist. In dem Vortrag sollen die unterschiedlichen gesellschaftlichen Strukturen ergründet und die spezifischen Auswirkungen des Klimawandels auf Gender erläutert werden. 


Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findet Ihr in Kürze unter www.studentsforfuture.info.
Wir freuen uns auf Euch! :) 
