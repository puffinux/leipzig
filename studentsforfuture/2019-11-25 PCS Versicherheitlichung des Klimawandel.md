---
id: "1717111595080489"
title: "PCS: Versicherheitlichung des Klimawandels"
start: 2019-11-25 11:15
end: 2019-11-25 12:45
locationName: GWZ- Das Geisteswissenschaftliche Zentrum
address: Beethovenstr. 15, 04107 Leipzig
link: https://www.facebook.com/events/1717111595080489/
image: 76702370_440690200199796_2391466936060870656_n.jpg
teaser: "Titel: Transformation der Sicherheitspolitik, Versicherheitlichung des
  Klimawandels Referent*in: Dr. Matthias Ecker-Ehrhardt Ort: GWZ Raum 5.0.15
  Zeit"
isCrawled: true
---
Titel: Transformation der Sicherheitspolitik,
Versicherheitlichung des Klimawandels
Referent*in: Dr. Matthias Ecker-Ehrhardt
Ort: GWZ Raum 5.0.15
Zeit: 11.15-12.45 Uhr

*Geöffnete Veranstaltung

Die kritischen Sicherheitsstudien verstehen unter "Versicherheitlichung" 
eine politische Strategie, die darauf zielt, durch die Konstruktion einer 
existentiellen Bedrohung demokratische Kontroll- und 
Beteiligungsmechanismen zu entwerten, um bestimmte 
Maßnahmen durchzusetzen. Lässt sich in diesem Sinne
 von einer "Versicherheitlichung" des Klimawandels sprechen?
Vorbereitende Textempfehlung (der Text kann unter
 matthias.ecker-ehrhardt@uni-leipzig.de angefordert werden):
 von Lucke, Franziskus, Thomas Diez, and Zehra Wellmann. 
2016. "Klimakämpfe: Eine komparative Studie der Versicherheitlichung von 
Klimawandel." ZIB Zeitschrift für Internationale Beziehungen 23(2): 112–43.

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findet Ihr in Kürze unter: https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 