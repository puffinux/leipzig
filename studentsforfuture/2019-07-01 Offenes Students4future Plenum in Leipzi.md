---
id: '342429709780101'
title: Offenes Students4future Plenum in Leipzig
start: '2019-07-01 17:00'
end: '2019-07-01 19:00'
locationName: UNIVERSITÄT LEIPZIG
address: 'Augustusplatz 10, 04109 Leipzig'
link: 'https://www.facebook.com/events/342429709780101/'
image: 64695536_327789708156513_6973228590192132096_n.png
teaser: 'Hallo!  Wie jeden Montag findet unser offenes Plenum um 17:00 im Raum S302 in der Universität Leipzig statt. Jede*r ist willkommen, wir freuen uns imm'
recurring: null
isCrawled: true
---
Hallo!

Wie jeden Montag findet unser offenes Plenum um 17:00 im Raum S302 in der Universität Leipzig statt. Jede*r ist willkommen, wir freuen uns immer über neue Gesichter! Auch Studis von anderen Hochschulen sind gerne gesehen.