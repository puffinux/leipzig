---
id: "2496328653782811"
title: Offenes Plenum Students for Future Leipzig
start: 2019-11-11 17:00
end: 2019-11-11 19:00
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/2496328653782811/
image: 73364213_428756894726460_8373787549625221120_o.png
teaser: Liebe Klimaaktivist*innnen,  jeden Montag findet das wöchentliche Plenum
  statt. In dem Semester liegt der Fokus besonders auf der geplanten StudentsFo
isCrawled: true
---
Liebe Klimaaktivist*innnen, 
jeden Montag findet das wöchentliche Plenum statt. In dem Semester liegt der Fokus besonders auf der geplanten StudentsForFuture Public Climate School (25.11. – 29.11.2019).

Wo? Neues Seminargebäude 302
Wann? 17 Uhr jeden Montag

Kommt alle vorbei!
Ladet alle eure Freund*innen ein und teilt die Veranstaltung. 
Our house is on fire!!!