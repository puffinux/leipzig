---
id: "545226169379548"
title: HTWK - Politische Ökonomie im Verhältnis zur Ökologie
start: 2019-11-26 15:00
end: 2019-11-26 17:00
locationName: HTWK Leipzig
address: Karl-Liebknecht-Str. 132, 04277 Leipzig
link: https://www.facebook.com/events/545226169379548/
image: 74530511_438781897057293_5539140337202626560_n.jpg
teaser: 'Seminar zum Thema: "Politische Ökonomie im Verhältnis zur Ökologie"  Leiter:
  Marx Kapital Lesekreis  Wo? G335'
isCrawled: true
---
Seminar zum Thema:
"Politische Ökonomie im Verhältnis zur Ökologie"

Leiter: Marx Kapital Lesekreis

Wo? G335