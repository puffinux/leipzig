---
id: "2428380644083268"
title: "PCS: Klimaschutz - eine persönliche Entscheidung?"
start: 2019-11-26 13:15
end: 2019-11-26 14:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/2428380644083268/
image: 76713983_445473593054790_1797163626889478144_n.jpg
teaser: "Titel: Klimaschutz - eine persönliche Entscheidung? Referent*in: PD Dr. Silke
  Gülker Ort: NSG S226, Hauptcampus Zeit: 13.15-14.45 Uhr  *Geöffnete Vera"
isCrawled: true
---
Titel: Klimaschutz - eine persönliche Entscheidung?
Referent*in: PD Dr. Silke Gülker
Ort: NSG S226, Hauptcampus
Zeit: 13.15-14.45 Uhr

*Geöffnete Veranstaltung
Ein kulturwissenschaftlicher Beitrag zur Climate Public School an der  Uni Leipzig zu Bewertungspraxen im Rahmen des Klimaschutzes

Diese Veranstaltung ist teil der Public Climate School. 
Das vollständige Programm findet ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 