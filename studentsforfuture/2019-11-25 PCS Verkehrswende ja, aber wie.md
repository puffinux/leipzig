---
id: "721567954994809"
title: "PCS: Verkehrswende ja, aber wie?"
start: 2019-11-25 13:15
end: 2019-11-25 14:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/721567954994809/
image: 78082454_441265010142315_6317044929818263552_n.jpg
teaser: "Titel: Verkehrswende ja, aber wie? Referent*in: Prof. Dr Paul Lehmann  Ort:
  Wirtschaftswissenschaftliche Fakultät, Raum 13 (I 274)  Zeit: 13.15-14.45"
isCrawled: true
---
Titel: Verkehrswende ja, aber wie?
Referent*in: Prof. Dr Paul Lehmann 
Ort: Wirtschaftswissenschaftliche Fakultät, Raum 13 (I 274) 
Zeit: 13.15-14.45 Uhr

*Geöffnete Veranstaltung

An der Verkehrswende hakt es in Deutschland. Der Absatz von Autos boomt.
Emissionsarme Antriebe und Kraftstoffe werden kaum genutzt. Welche politischen Weichenstellungen sind jetzt nötig, um den Klimaschutz auch im Verkehrssektor voranzubringen?

Diese Veranstaltung ist teil der Public Climate School. 
Das vollständige Programm findet ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 