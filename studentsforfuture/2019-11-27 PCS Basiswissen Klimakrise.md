---
id: "1480626532087440"
title: "PCS: Basiswissen Klimakrise"
start: 2019-11-27 11:15
end: 2019-11-27 12:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/1480626532087440/
image: 77347998_440513590217457_6152910753146863616_n.jpg
teaser: 'Titel: "Basiswissen Klimakrise" Referent*innen: Scientist 4 Future Leipzig
  Ort: HS2 / Gerald Bigurube (Tansania)  Zeit: 11:15 - 12:45  Das 1x1 der Kli'
isCrawled: true
---
Titel: "Basiswissen Klimakrise"
Referent*innen: Scientist 4 Future Leipzig
Ort: HS2 / Gerald Bigurube (Tansania) 
Zeit: 11:15 - 12:45

Das 1x1 der Klimakrise - erklärt von Scientist for Future

Diese Veranstaltung ist Teil der Public Climate School.
Das vollständige Programm gibt es auf studentsforfuture.info
Wir freuen uns auf euch!