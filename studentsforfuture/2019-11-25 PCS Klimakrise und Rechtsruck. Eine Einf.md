---
id: "534426213773533"
title: "PCS: Klimakrise und Rechtsruck. Eine Einführung."
start: 2019-11-25 13:15
end: 2019-11-25 14:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/534426213773533/
image: 75456902_440701520198664_7744735674401554432_n.jpg
teaser: "Titel: Workshop - Klimakrise und Rechtsruck. Eine Einführung. Veranstalter:
  PRISMA Leipzig  Raum: Seminarraum 203, NSG Universität Leipzig  Uhrzeit: 1"
isCrawled: true
---
Titel: Workshop - Klimakrise und Rechtsruck. Eine Einführung.
Veranstalter: PRISMA Leipzig 
Raum: Seminarraum 203, NSG Universität Leipzig 
Uhrzeit: 13:15-14:45 Uhr

(Teaser folgt)

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findet Ihr in Kürze unter: https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 


