---
id: "384368455774593"
title: System Change not Climate Change!
start: 2019-12-06 18:00
end: 2019-12-06 20:00
locationName: HTWK Leipzig
address: Karl-Liebknecht-Str. 132, 04277 Leipzig
link: https://www.facebook.com/events/384368455774593/
image: 73223182_417955685806581_7505622750405328896_o.jpg
teaser: Als Resümee der Vortragsreihe möchten wir mit euch darüber Diskutieren wie wir
  ein Konzept für ein Gesellschaftsmodell entwickeln können, welches zu e
isCrawled: true
---
Als Resümee der Vortragsreihe möchten wir mit euch darüber Diskutieren wie wir ein Konzept für ein Gesellschaftsmodell entwickeln können, welches zu einer ökologischen und sozial-gerechten Transformation explizit in Europa aber auch Weltweit führen kann.
Hierbei möchten wir schon geschaffene fortschrittliche Strukturen in Augenschein nehmen und wie man diese ausbauen kann aber auch Strukturen betrachten die so einer Transformation im Wege stehen. Hierbei geht es auch darum zu schauen wie parlamentarisch und außerparlamentarisch Möglichkeiten dazu geschaffen werden können.

Raum: 016 Lipsiusbau

Diskussionsleitung:Students for Future HTWK                                       