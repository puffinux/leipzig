---
id: "473520759952567"
title: Siebdruck, Upcycling und Bienenwachstuch Workshops beim Artspace
start: 2019-11-26 14:00
end: 2019-11-26 18:00
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/473520759952567/
image: 76963264_120347736079931_5279089383213891584_n.jpg
teaser: Artists for Future und Students for Future laden zu sechs kostenfreien DIY
  Workshops in den Artspace ein. Die Workshops finden im Rahmen der Public Cl
isCrawled: true
---
Artists for Future und Students for Future laden zu sechs kostenfreien DIY Workshops in den Artspace ein. Die Workshops finden im Rahmen der Public Climate School statt, welche vom 25.11. bis 29.11. bundesweit an Hochschulen stattfindet, um auf den Klimawandel aufmerksam zu machen und ein Zeichen gegen die Klimapolitik der Bundesregierung zu setzen. 
Das vollständige Programm mit zahlreichen theoretischen Inputs findet ihr hier: https://studentsforfuture.info/ortsgruppe/leipzig/

Für den künstlerisch kreativen Austausch kommt in unseren Artspace und macht bei ein paar Workshops mit:

Montag 25.11. bis Donnerstag 28.11 
Siebdruck Workshop mit Felix Wenzel 
14 bis 17 Uhr @ Art Space, Uni Leipzig 
In diesem Workshop lernt ihr die Technik des Siebdrucks kennen. Montag und Dienstag entwerft ihr Motive zum Thema Klimawandel und Protestbewegung. Mittwoch und Donnerstag besteht dann die Möglichkeit mit verschiedensten wasserbasierte Farben auf selbst mitgebrachte Beutel und T-Shirts zu drucken. 

Dienstag 26.11.2019
Upcycling Workshop mit Oskar Metzger
14 bis 18  Uhr @ Art Space, Uni Leipzig 
In diesem Workshop könnt ihr scheinbar kaputte Dingen eine neue Bestimmung geben. Die Caps von Spraydosen werden zu schmuckvollen Schlüsselanhängern, Tetra-Paks zu Geldbeuteln.

Mittwoch 27.11.2019
DIY - Bienenwachstuch Workshop mit Wiebke Pranz
15 bis 18 Uhr @ Art Space, Uni Leipzig
In diesem Praxis Workshop erfahrt ihr wie Bienenwachstücher als alternatives Verpackungsmaterial zu Alu- und Frischhaltefolie ganz einfach selbst hergestellt werden können. Ihr könnt eigene Stoffe mitbringen und upcyceln oder alte Stoffe vor Ort nutzen. 

PS: Der Artspace steht Euch von Montag bis Freitag von 12 bis 18 Uhr auch unabhängig von den Workshops als temporärer Ort für selbstständiges kreatives Schaffen zur Verfügung: https://www.facebook.com/events/569916233773116/