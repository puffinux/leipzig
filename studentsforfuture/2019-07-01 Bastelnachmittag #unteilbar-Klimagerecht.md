---
id: '318511899086059'
title: 'Bastelnachmittag #unteilbar-Klimagerechtigkeitsblock'
start: '2019-07-01 14:00'
end: '2019-07-01 18:00'
locationName: UNIVERSITÄT LEIPZIG
address: 'Augustusplatz 10, 04109 Leipzig'
link: 'https://www.facebook.com/events/318511899086059/'
image: 65549081_334063670862450_2185974973108060160_n.jpg
teaser: 'Am Montag, den 01.07.19, findet ein Bastelnachmittag auf dem Innenhof des Hauptcampusses statt! Dort könnt ihr eurer Kreativität freien Lauf lassen un'
recurring: null
isCrawled: true
---
Am Montag, den 01.07.19, findet ein Bastelnachmittag auf dem Innenhof des Hauptcampusses statt! Dort könnt ihr eurer Kreativität freien Lauf lassen und euch mit Plakaten/Schildern/Flaggen für die #unteilbar-Demo am 06.07.19 ausstatten.

Ab 14 Uhr findet ihr uns mit Farbe, Pappe und Stoffen auf dem Innenhof. Kommt vorbei und tobt euch aus, denn Antirassismus, die Soziale Frage und Klimagerechtigkeit sind #unteilbar! 

Nicht vergessen, am 06.07.19 ist die #unteilbar-Demo in Leipzig: 
https://www.facebook.com/events/353733895335129/

Wir freuen uns auf euch!

Die teilnehmenden Gruppierungen am Klimagerechtigkeitsblock auf der #unteilbar-Demonstration sind: Unteilbar Students for Future Leipzig Fridays For Future Leipzig Ende Gelände Leipzig BUND Leipzig Greenpeace Leipzig Klimacamp Leipziger Land 