---
id: "2511211715612063"
title: "PCS: Politik ohne Wissenschaft?!"
start: 2019-11-25 15:15
end: 2019-11-25 16:45
locationName: GWZ- Das Geisteswissenschaftliche Zentrum
address: Beethovenstr. 15, 04107 Leipzig
link: https://www.facebook.com/events/2511211715612063/
image: 74531740_443081449960671_7381644877274021888_n.jpg
teaser: "Titel: Politik ohne Wissenschaft?! - Welche Rolle spielen Expertennetzwerke
  in der Klimapolitik? Referent*in: Matthias Ecker-Ehrhardt Ort: GWZ 4116 Ze"
isCrawled: true
---
Titel: Politik ohne Wissenschaft?! - Welche Rolle spielen Expertennetzwerke in der Klimapolitik?
Referent*in: Matthias Ecker-Ehrhardt
Ort: GWZ 4116
Zeit: 15.15-16.45 Uhr

*Geöffnete Veranstaltung
Die vorbereitende Lektüre des folgenden Textes wird empfohlen (der Text kann bei matthias.ecker-ehrhardt@uni-leipzig.de angefordert werden): Grundmann, Reiner (2007): Climate change and knowledge politics, in: Environmental politics 16,  414-432

Diese Veranstaltung ist teil der Public Climate School. 
Das vollständige Programm findet ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 