---
id: '304177830519901'
title: Basteltag
start: '2019-05-23 11:00'
end: '2019-05-23 17:00'
locationName: UNIVERSITÄT LEIPZIG
address: 'Augustusplatz 10, 04109 Leipzig'
link: 'https://www.facebook.com/events/304177830519901/'
image: 60345079_308260820109402_2935364848348823552_n.jpg
teaser: 'Für eine bunte Demonstration braucht es bunte Transparente und Schilder mit guten Sprüchen.  Diese und weitere kreative Schönheiten wollen wir am: 23.'
recurring: null
isCrawled: true
---
Für eine bunte Demonstration braucht es bunte Transparente und Schilder mit guten Sprüchen.

Diese und weitere kreative Schönheiten wollen wir am:
23.05. ab 11 Uhr auf dem Hauptcampus der Universität Leipzig (vor den Büros des Studierenden Rats) mit euch basteln.

Kommt vorbei und bringt eure Freund*innen und gute Laune mit.
Wir freuen uns auf eure tatkräftige Unterstützung.