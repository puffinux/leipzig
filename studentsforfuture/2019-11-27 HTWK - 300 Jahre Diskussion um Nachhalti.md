---
id: "1445347768962793"
title: HTWK - 300 Jahre Diskussion um Nachhaltigkeit
start: 2019-11-27 12:45
end: 2019-11-27 14:45
locationName: HTWK Leipzig
address: Karl-Liebknecht-Str. 132, 04277 Leipzig
link: https://www.facebook.com/events/1445347768962793/
image: 76182564_438898320378984_453583872563609600_n.jpg
teaser: 'Vorlesung zum Thema: "300 Jahre Diskussion um Nachhaltigkeit: Viel Reden und
  wenig Tun - Klimawandel: Messen und Verstehen"  Dozent*in: Prof. Dr. Schu'
isCrawled: true
---
Vorlesung zum Thema:
"300 Jahre Diskussion um Nachhaltigkeit: Viel Reden und wenig Tun - Klimawandel: Messen und Verstehen"

Dozent*in: Prof. Dr. Schubert

Wo? Li110