---
id: "458997528067765"
title: "PCS: Ökologischer Kollaps, nicht nur Klimakrise!"
start: 2019-11-27 17:15
end: 2019-11-27 18:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/458997528067765/
image: 75279257_439519033650246_3483539641825492992_n.jpg
teaser: "Titel: Ökologischer Kollaps, nicht nur Klimakrise!  Referent*in: Luise, XR
  Leipzig  Raum: Sunita Narain (Indien) / HS4  Uhrzeit: 17.15 Uhr - 18.45 Uhr"
isCrawled: true
---
Titel: Ökologischer Kollaps, nicht nur Klimakrise!

Referent*in: Luise, XR Leipzig

Raum: Sunita Narain (Indien) / HS4

Uhrzeit: 17.15 Uhr - 18.45 Uhr 

Das Klima ist in aller Munde. Doch über Biodiversität und den drohenden ökologischen Kollaps spricht kaum eine*r. Brennende Regenwälder, ausgebleichte Korallenriffe, das drohende Aussterben von Million Arten… Insektenforscher*innen finden selbst in Schutzgebieten 75% weniger Fluginsekten! Viele verstehen nicht, was das bedeutet. Nicht das Klima, sondern die Ökosysteme der Erde befinden sich in einer globalen,
menschengemachten Krise. Der Vortrag beleuchtet, was wir über die ökologische Krise und die Bedeutung von Biodiversität wissen, was die Ursachen sind und was passieren muss, um die Krise zu stoppen.

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findet Ihr hier: https://studentsforfuture.info/ortsgruppe/leipzig/#calendar

Wir freuen uns auf Euch! 