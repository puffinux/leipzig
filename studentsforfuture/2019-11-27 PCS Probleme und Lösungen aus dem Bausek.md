---
id: "1006073566394521"
title: "PCS: Probleme und Lösungen aus dem Bausektor"
start: 2019-11-27 09:15
end: 2019-11-27 10:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/1006073566394521/
image: 75424712_439988983603251_6843157330993872896_n.jpg
teaser: "Titel:  Probleme und Lösungen aus dem Bausektor; Können wir Gebäuden mit
  einem positiven Fußabdruck errichten? Referent*innen: Tore Waldhausen (Archit"
isCrawled: true
---
Titel:  Probleme und Lösungen aus dem Bausektor; Können wir Gebäuden mit einem positiven Fußabdruck errichten?
Referent*innen: Tore Waldhausen (Architects for Future; Cradle to Cradle Baubündnis), Björn Heiden (Architects for Future, DGNB Auditor)
Ort: HS 11 /  Roberto Antonio Argueta (Honduras) 
Zeit: 9:15- 10:45 Uhr

Probleme und Lösungen aus dem Bausektor: 
Können wir Gebäuden mit einem positiven Fußabdruck errichten?

Diese Veranstaltung ist Teil der Public Climate School.
Das vollständige Programm gibt es auf studentsforfuture.info
Wir freuen uns auf euch!