---
id: "456227471693597"
title: "PCS: Gelbwesten - Verbindung von Klimafrage und sozialen Kämpfen"
start: 2019-11-27 15:15
end: 2019-11-27 16:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/456227471693597/
image: 75627586_439514583650691_7302058987250253824_n.jpg
teaser: "Titel: Die Bewegung der Gelbwesten – Verbindung von Klimafrage und sozialen
  Kämpfen  Raum: Ken Saro-Wiwa (Nigeria)/HS 20   Uhrzeit: 15.15 Uhr - 16.45"
isCrawled: true
---
Titel: Die Bewegung der Gelbwesten – Verbindung von Klimafrage und sozialen Kämpfen

Raum: Ken Saro-Wiwa (Nigeria)/HS 20 

Uhrzeit: 15.15 Uhr - 16.45 Uhr 

In Frankreich entfachte November 2018 die Bewegung der Gelbwesten, die sich gegen die Einführung einer CO2-Steuer werte und die Klimafrage grundlegend politisierte.

Die Frage der Verbindung zwischen Klimafrage und soziale Kämpfe diskutieren wir mit Hans Rackwitz, wissenschaftlicher Mitarbeiter an der Universität Jena, und Aimé Paris, ein Gelbwesten-Aktivist aus Paris und Philosophiestudent mit Schwerpunkt Marxismus und Ökologie.

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findet Ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar

Wir freuen uns auf Euch !

