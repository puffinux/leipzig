---
id: "480131422852741"
title: "PCS: Klimakrise vs. schulische Umweltbildung"
start: 2019-11-27 09:15
end: 2019-11-27 10:45
address: Universität Leipzig, Institut für Biologie, Großer Hörsaal
link: https://www.facebook.com/events/480131422852741/
image: 75543619_439486116986871_17562550770073600_n.jpg
teaser: "Titel: Klimakrise vs. Schulische Umweltbildung: Welchen Beitrag kann der
  Biologieunterricht leisten? Referent*in: Prof. Dr. Jörg Zabel Raum: Großer Hö"
isCrawled: true
---
Titel: Klimakrise vs. Schulische Umweltbildung: Welchen Beitrag kann der Biologieunterricht leisten?
Referent*in: Prof. Dr. Jörg Zabel
Raum: Großer Hörsaal, Institut für Biologie, Johannisallee 21
Uhrzeit: 09:15 - 10:45

Die Vorlesung beleuchtet die Geschichte der Umweltbildung in Deutschland sowie die Rolle der Schulen und speziell des Biologieunterrichts dabei. Wir werden die Grenzen diskutieren, die das System Schule setzt, aber auch an Beispielen guter Praxis die Chancen schulischer Umweltbildung aufzeigen, z.B. durch neue Formaten wie Citizen Science. 

Diese Veranstaltung ist Teil der Public Climate School
Das vollständige Programm findet ihr unter studentsforfuture.info
Wir freuen uns auf alle, die kommen!