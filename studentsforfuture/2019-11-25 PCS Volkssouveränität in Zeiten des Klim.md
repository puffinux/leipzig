---
id: "633778853693798"
title: "PCS: Volkssouveränität in Zeiten des Klimawandels"
start: 2019-11-25 11:15
end: 2019-11-25 12:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/633778853693798/
image: 75407744_439000570368759_1440321384407367680_n.jpg
teaser: "Titel: Volkssouveränität in Zeiten des Klimawandels - Wieviel Demokratie
  braucht, verträgt, gefährdet der Klimanotstand? Referent*in: Silke Beck Ort:"
isCrawled: true
---
Titel: Volkssouveränität in Zeiten des Klimawandels - Wieviel Demokratie braucht, verträgt, gefährdet der Klimanotstand?
Referent*in: Silke Beck
Ort: Universität Leipzig, HS10/Joël Imbangola 
Zeit: 11:15-12:45


Volkssouveränität in Zeiten des Klimawandels - wieviel Demokratie braucht, verträgt, gefährdet der Klimanotstand?
Diskussionsveranstaltung mit Inputs von Silke Beck (Helmholtz-Zentrum für Umweltforschung, UFZ) sowie Lehrenden des Instituts für Politikwissenschaft Rebecca Pates, Matthias Ecker-Ehrhardt, Pawel Karolewski. Nach einem kurzen Roundtable zu Beginn der Veranstaltung  planen wir eine offene und inklusive Diskussion in Form einer „Fishbowl“. 


Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findet Ihr in Kürze unter: https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 