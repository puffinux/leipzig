---
id: "1003261106676368"
title: "PCS: Educators For Future"
start: 2019-11-26 17:15
end: 2019-11-26 18:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/1003261106676368/
image: 76986935_439111260357690_5302018143432998912_n.jpg
teaser: "Titel: Vortrag über „Educators For Future“  Referent*innen:  Sanja Liebermann
  (Vortragende) Veit Polowy (Moderation)  Raum:  Uhrzeit: 17:15 Uhr -18:45"
isCrawled: true
---
Titel: Vortrag über „Educators For Future“

Referent*innen: 
Sanja Liebermann (Vortragende)
Veit Polowy (Moderation)

Raum:

Uhrzeit: 17:15 Uhr -18:45 Uhr

Educators sind gefragt, sich der umfassenden Verantwortung bewusst zu werden, die darin liegt, Kinder und Jugendliche von heute wirksam auf die Welt von morgen vorzubereiten. Dass sie dabei mitreden möchten, macht nicht zuletzt die Bewegung der Fridays for Future deutlich. Educators sind aufgerufen, in ihren Institutionen, Netzwerken und Einrichtungen, sowohl zeitlich als auch inhaltlich Raum für die Themen der Zukunft zu schaffen und sie so zu ‚Landebahnen der Zukunft‘ umzubauen.

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findet Ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar

Wir freuen uns auf Euch!