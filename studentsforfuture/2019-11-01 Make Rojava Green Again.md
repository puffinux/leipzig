---
id: "635244163671842"
title: Make Rojava Green Again
start: 2019-11-01 19:00
end: 2019-11-01 21:00
locationName: HTWK Leipzig
address: Karl-Liebknecht-Str. 132, 04277 Leipzig
link: https://www.facebook.com/events/635244163671842/
image: 71724732_417943072474509_1899024308231995392_o.jpg
teaser: Im März 2016 wurde die autonome Förderation Nordsyrien, auch bekannt unter dem
  Namen Rojava, ausgerufen. In diesem Zuge entstand die „Make Rojava Gree
isCrawled: true
---
Im März 2016 wurde die autonome Förderation Nordsyrien, auch bekannt unter dem Namen Rojava, ausgerufen.
In diesem Zuge entstand die „Make Rojava Green Again“- Bewegung, welche Spenden sammelt und Projekte organisiert, um das Ökosystem in Rojava wiederherzustellen. In dem Vortrag werden sowohl die Projekte vorgestellt als auch die ökologischen,feministischen und basisdemokratischen Grundlagen des Gesellschaftssystems.
Schwerpunkt wird aber ökologisches Bauen sein und der Vortrag soll im Sinne der internationalen Solidarität als Initialzündung für ein Austauschprojekt mit HTWK Student*innen und dem Projekt dienen.

Raum: 016 Lipsiusbau

Referent: Malte Buchholz                                                           
