---
id: "576626123164911"
title: HTWK - Treibhauseffekt
start: 2019-11-26 19:00
end: 2019-11-26 20:30
locationName: HTWK Leipzig
address: Karl-Liebknecht-Str. 132, 04277 Leipzig
link: https://www.facebook.com/events/576626123164911/
image: 75097186_438784533723696_6519236584066252800_n.jpg
teaser: 'Vorlesung zum Thema: "Treibhauseffekt"  Dozent*in: Prof. Schenk  Wo? G301'
isCrawled: true
---
Vorlesung zum Thema:
"Treibhauseffekt"

Dozent*in: Prof. Schenk

Wo? G301