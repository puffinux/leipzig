---
id: "1478659415625015"
title: "PCS: Siebdruck-Workshop"
start: 2019-11-26 14:00
end: 2019-11-26 16:00
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/1478659415625015/
image: 76720759_443221263280023_6908579681595817984_n.jpg
teaser: "Titel: Siebdruck-Workshop Referent*innen: Students for Future Leipzig Ort:
  ArtSpace Zeit: 14.00-17.00 Uhr  Artist for Future laden zum kostenfreien DI"
isCrawled: true
---
Titel: Siebdruck-Workshop
Referent*innen: Students for Future Leipzig
Ort: ArtSpace
Zeit: 14.00-17.00 Uhr

Artist for Future laden zum kostenfreien DIY Siebdruck Workshop mit Felix Wenzel im Artspace ein. In diesem Workshop lernt ihr die Technik des Siebdrucks kennen. Montag und Dienstag entwerft ihr Motive zum Thema Klimawandel und Protestbewegung. Mittwoch und Donnerstag besteht dann die Möglichkeit, mit verschiedensten wasserbasierten Farben auf selbst mitgebrachte Beutel und T-Shirts zu drucken. 

Diese Veranstaltung ist teil der Public Climate School. 
Das vollständige Programm findet ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 
