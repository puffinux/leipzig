---
id: "537664366794045"
title: "PCS: Aufruf zum Klima-Ungehorsam – Mensch und Natur zuliebe"
start: 2019-11-27 17:00
end: 2019-11-27 18:30
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/537664366794045/
image: 75392624_440337790235037_8781236704561332224_n.jpg
teaser: "Titel: Aufruf zum Klima-Ungehorsam – Mensch und Natur zuliebe Referent*in:
  Dipl.-Ing. A. Plehn (Architektin, Baubiologin, Mediatorin, Buchautorin) Ort"
isCrawled: true
---
Titel: Aufruf zum Klima-Ungehorsam – Mensch und Natur zuliebe
Referent*in: Dipl.-Ing. A. Plehn (Architektin, Baubiologin, Mediatorin, Buchautorin)
Ort: Ken Saro-Wiwa (Nigeria)/ HS20

1989 – 30 Jahre unwirksamer Klimaschutz Wie weiter in einer Zeit der Extreme? Wissen rund um CO2, Folgemaßnahmen, deren Konsequenzen und Wege aus dem Dilemma

- Computer wird benötigt

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findet ihr bei studentsforfuture.info
Wir freuen uns auf euch!