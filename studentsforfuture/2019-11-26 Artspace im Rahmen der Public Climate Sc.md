---
id: "569916247106448"
title: Artspace im Rahmen der Public Climate School
start: 2019-11-26 12:00
end: 2019-11-26 18:00
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/569916247106448/
image: 75279343_120087136105991_1787386709365751808_n.jpg
teaser: Der ARTSPACE ist ein temporärer Kunstraum an der Universität Leipzig und
  Kooperationsprojekt von Students for Future und Artists for Future im Rahmen
isCrawled: true
---
Der ARTSPACE ist ein temporärer Kunstraum an der Universität Leipzig und Kooperationsprojekt von Students for Future und Artists for Future im Rahmen der Public Climate School. Diese findet vom 25.11. bis 29.11. bundesweit an Hochschulen statt, um auf den Klimawandel aufmerksam zu machen und ein Zeichen gegen die wirkungslose Klimapolitik der Bundesregierung zu setzen. Das vollständige Programm mit zahlreichen theoretischen Inputs aber auch weiteren Kunst und Kultur Angeboten findet ihr hier: https://studentsforfuture.info/ortsgruppe/leipzig/

Unser ARTSPACE steht allen Interessierten (Schüler*innen, Student*innen, und erwachsene Leipziger*innen) in der Woche vom 25. bis 29. November von Mittags bis zum frühen Abend zur Verfügung und lädt dazu ein, sich mit kreativen, handwerklichen und künstlerischen Mitteln zu den Themen Klima, Nachhaltigkeit und Protest auseinander zu setzen und eigene kreative Ausdrucksformen zu finden.  

In dem ARTSPACE stellen wir Euch verschiedene Materialien zur Verfügung, mit denen selbstständig und kostenfrei gearbeitet werden kann. Es gibt die Möglichkeit, zu zeichnen, zu bauen und zu basteln oder Transparente, Plakate und Banner für den 29.11. zu gestalten. Zudem zeigen wir euch Euch eine Reihe von Techniken wie Siebdruck, Upcycling und Textildruck. An einigen Tagen haben wir dazu spezielle Workshop Angebote, die ihr ebenfalls kostenfrei nutzen könnt: https://www.facebook.com/events/473520749952568/

Kommt vorbei und bringt Eure Kommiliton*innen, Kinder, Eltern oder Freunde mit!

ACHTUNG: Am Freitag den 29.11. werden wir den ARTSPACE aufgrund des globalen Klimastreiks nur bis 13.00 Uhr geöffnet halten.

- - - - - 

The artspace is a temporary space for creativity and a cooperational project of Artists for Future and Students for Future Leipzig as a part of the Public Climate School (PCS) at Uni Leipzig.
 
From Monday to Friday it is open to everyone who wants to get active and creative. Its possible to paint, build and construct or to create your own strike banner. We have lots of material for you to use for free and will help you to try out different techniques, so come by and bring your friends, family, kids, parents or fellow students!

PS: On some days we have free workshops for upcycling, textile printing etc. Check them out: https://www.facebook.com/events/473520749952568/

https://studentsforfuture.info/ortsgruppe/leipzig/
https://artistsforfuture.org/de/
https://fridaysforfuture.de/allefuersklima/
https://artists4future-lei.wixsite.com/projekt