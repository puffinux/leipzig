---
id: "961177350907199"
title: "PCS: Klimafrühstück für eine gesunde Zukunft"
start: 2019-11-27 10:00
end: 2019-11-27 11:30
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/961177350907199/
image: 75587502_439488870319929_8252387086368243712_n.jpg
teaser: "Referent*innen: Health For Future Raum: NSG SR 201, Hauptcampus  Uhrzeit:
  10:00 Uhr bis 11:30 Uhr   Im Rahmen eines gemütlichen Klimafrühstücks möchte"
isCrawled: true
---
Referent*innen: Health For Future
Raum: NSG SR 201, Hauptcampus 
Uhrzeit: 10:00 Uhr bis 11:30 Uhr 

Im Rahmen eines gemütlichen Klimafrühstücks möchten wir mit Euch die Zusammenhänge von Klimawandel und Gesundheit ausarbeiten. Bringt gerne Eure Ideen und Eure Tassen mit!

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findet Ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar

Wir freuen uns auf Euch! 



