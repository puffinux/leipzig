---
id: "2456245127921804"
title: 'PCS: "Recycling Lab" (Initiative Latinxs Leipzig)'
start: 2019-11-26 13:00
end: 2019-11-28 16:00
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/2456245127921804/
image: 76730661_439990620269754_8816136178584518656_n.jpg
teaser: 'Titel: "Recycling Lab" (Initiative Latinxs Leipzig) Referent*innen:
  Gregorio&Yameli Ort: ArtSpace - 26.11.19 Zeit: 13:00 - 15:00     - 28.11.19
  Zeit:'
isCrawled: true
---
Titel: "Recycling Lab" (Initiative Latinxs Leipzig)
Referent*innen: Gregorio&Yameli
Ort: ArtSpace
- 26.11.19
Zeit: 13:00 - 15:00    
- 28.11.19
Zeit: 14:00 - 16:00    

"Recycling Lab" (Initiative Latinxs Leipzig)

Reduzieren, wiederverwerten recyclen. Hast du altes Zeug Zuhause und du weißt nicht, wie du es recyclen kannst? Bring es zum Recycling LAB und lass deiner Kreativität freien Lauf als Vorbereitung auf die Demo zum Klimastreik am 29.11.

Diese Veranstaltung ist Teil der Public Climate School.
Das vollständige Programm gibt es auf studentsforfuture.info
Wir freuen uns auf euch!