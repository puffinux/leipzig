---
id: "935271656857672"
title: "PCS: Klimawandel - Wichtige Folgen aus Sicht des Infektiologen"
start: 2019-11-28 11:00
end: 2019-11-28 11:45
address: Hörsaal Zentrales Forschungsgebäude Liebigstraße 19
link: https://www.facebook.com/events/935271656857672/
image: 72135105_439694516966031_5776849524152598528_n.jpg
teaser: "Klimawandel - Wichtige Folgen aus Sicht des Infektiologen  Referent*in: Prof.
  Dr. med. Christoph Lübbert Ort: HS Zentrales Forschungsgebäude, Liebigst"
isCrawled: true
---
Klimawandel - Wichtige Folgen aus Sicht des Infektiologen

Referent*in: Prof. Dr. med. Christoph Lübbert
Ort: HS Zentrales Forschungsgebäude, Liebigstraße 19 (Haus C)
Uhrzeit: 11.00 - 11.45 Uhr 

Prof. Dr. med. Christoph Lübbert, Leiter des Fachbereiches Infektions- und Tropenmedizin am Universitätsklinikum Leipzig wird seine fachliche Sicht als Infektiologe und Tropenmediziner auf die bereits aktuell vorliegenden und zukünftigen Auswirkungen des Klimawandels auf Deutschland und die Welt präsentieren.
Im Anschluss an die Vorlesung wird es eine Fragerunde geben. 

Diese Veranstaltung ist Teil der Public Climate School.
Das komplette Programm findet ihr auf studentsforfuture.info
Wir freuen uns auf euch!