---
id: "498214797439688"
title: "PCS: Klimakatastrophe, Öko-Kollaps und Ziviler Ungehorsam von XR"
start: 2019-11-26 13:15
end: 2019-11-26 14:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/498214797439688/
image: 78360100_443884876546995_2850482512029810688_n.jpg
teaser: "Titel: Über die Klimakatastrophe, ökologischen Kollaps und zivilen Ungehorsam
  von Extinction Rebellion Referent*innen: Lotte & Luke von XR Leipzig Rau"
isCrawled: true
---
Titel: Über die Klimakatastrophe, ökologischen Kollaps und zivilen Ungehorsam von Extinction Rebellion
Referent*innen: Lotte & Luke von XR Leipzig
Raum: Wirtschaftswissenschaftliche Fakultät SR 1, Hauptcampus
Uhrzeit: 13.15 Uhr - 14.45 Uhr 


Dies ist ein Notfall! Uns erwarten Dürren, Brände, Stürme, Überschwemmungen und mehr…
Was genau uns in den nächsten Jahren laut aktuellen wissenschaftlichen Erkenntnissen tatsächlich bevorsteht und wie wir von Extinction Rebellion die Regierungen endlich zum notwendigen Handeln bewegen wollen, erfahrt ihr in diesem Vortrag! Im Anschluss wollen wir gemeinsam mit euch über einen adäquaten Umgang mit der Situation, der Bedeutung für uns und strategische Überlegungen diskutieren.

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findet Ihr hier: https://studentsforfuture.info/ortsgruppe/leipzig/#calendar 

Wir freuen uns auf Euch! 