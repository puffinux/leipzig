---
id: "441295283197626"
title: "PCS: Climate Change, Development and Economic Growth Part 1"
start: 2019-11-26 11:15
end: 2019-11-26 12:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/441295283197626/
image: 76706845_441303443471805_629933206807773184_n.jpg
teaser: "Titel: Climate Change, Development and Economic Growth Part1 Referent*in:
  Gregor von Schweinitz Ort: WiFa I274 Zeit: 11.15-12.45 Uhr  *Geöffnete Veran"
isCrawled: true
---
Titel: Climate Change, Development and Economic Growth Part1
Referent*in: Gregor von Schweinitz
Ort: WiFa I274
Zeit: 11.15-12.45 Uhr

*Geöffnete Veranstaltung

Developing economies should get all opportunities to better living conditions and higher wealth. At the same time, avoiding climate change is a priority. Is the hope of economic development at odds with sustainability? How can we balance different interests?

Diese Veranstaltung ist teil der Public Climate School. 
Das vollständige Programm findet ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 