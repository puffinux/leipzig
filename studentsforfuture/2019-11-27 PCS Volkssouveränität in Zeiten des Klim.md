---
id: "2225111527788354"
title: "PCS: Volkssouveränität in Zeiten des Klimawandels"
start: 2019-11-27 11:00
end: 2019-11-27 13:00
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/2225111527788354/
image: 75513418_440345393567610_7816560863464128512_n.jpg
teaser: "Titel: Volkssouveränität in Zeiten des Klimawandels – wieviel Demokratie
  braucht, verträgt, gefährdet der Klimanotstand?  Referent*in: Dr. Silke Beck,"
isCrawled: true
---
Titel: Volkssouveränität in Zeiten des Klimawandels – wieviel Demokratie braucht, verträgt, gefährdet der Klimanotstand? 
Referent*in: Dr. Silke Beck, Dr. Matthias Ecker-Ehrhardt, Rebecca Pates, Pawel Karolewski 
Ort: HS10 / Joël Imbangola (D.R. Kongo)
Zeit: 11:00 - 13:00

Volkssouveränität in Zeiten des Klimawandels – wieviel Demokratie braucht, verträgt, gefährdet der Klimanotstand?

Diskussionsveranstaltung mit Inputs von Silke Beck (Helmholtz-Zentrum für Umweltforschung, UFZ) sowie Lehrenden des Instituts für Politikwissenschaft Rebecca Pates, Matthias Ecker-Ehrhardt, Pawel Karolewski. Nach einem kurzen Roundtable zu Beginn der Veranstaltung planen wir eine offene und inklusive Diskussion in Form einer "Fishbowl".

Diese Veranstaltung ist Teil der Public Climate School.
Das vollständige Programm gibt es auf studentsforfuture.info
Wir freuen uns auf euch!