---
id: "2331380987175035"
title: "PCS Podiumsdiskussion: Strategien und Ziele der Klimabewegung"
start: 2019-11-27 13:30
end: 2019-11-27 15:00
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/2331380987175035/
image: 76909073_439508096984673_1992729924716199936_n.jpg
teaser: "Titel: Podiumsdiskussion: „Alle gemeinsam fürs Klima! Strategien & Ziele der
  Klimabewegung“  Referent*innen: Vertreter*innen von Fridays for Future, S"
isCrawled: true
---
Titel: Podiumsdiskussion: „Alle gemeinsam fürs Klima! Strategien & Ziele der Klimabewegung“

Referent*innen: Vertreter*innen von Fridays for Future, Scientists for Future, Extinction Rebellion und Marco Böhme, (MdL DIE LINKE). Moderiert wird der Abend von Michael Neuhaus (Stadtrat Leipzig und Mitglied im Bundessprecher*innenrat der Linksjugend)

Raum: Sunita Narain (Indien)/HS 4

Uhrzeit: 13.30 Uhr - 15.00 Uhr 


Seit einiger Zeit gibt es wachsende Proteste für wirksame Maßnahmen gegen den Klimawandel. Insbesondere die Jugendbewegung Fridays for future hat die Politik unter Druck gesetzt.Das im September vorgelegte Klimapaket der Bundesregierung bleibt weiter hinten den Forderungen aus der Zivilgesellschaft zurück und wird nicht dazu beitragen die Klimaziele von Paris zu erfüllen. Was sind Ziele und Mittel des Protestes gegen den Klimawandel? Muss es nicht um einen grundlegenden Wandel der Produktionsverhältnisse anstelle von kosmetischen Maßnahmen gehen? Wie schaffen wir es Klimapolitik sozial gerecht zu gestalten und welche Mittel sind wirksam?

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findet Ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar

Wir freuen uns auf Euch !
