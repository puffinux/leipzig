---
id: "694673124377888"
title: Tierbefreiung - Antikapitalismus - Klimagerechtigkeit
start: 2019-11-12 20:00
end: 2019-11-12 22:00
locationName: HTWK Leipzig
address: Karl-Liebknecht-Str. 132, 04277 Leipzig
link: https://www.facebook.com/events/694673124377888/
image: 76686440_433822774219872_6976513290985799680_n.png
teaser: Wie hängen Tierrechte und Antikapitalismus zusammen? Und was hat die
  Klimakrise eigentlich damit zu tun?   Der eigentliche Vortrag zum Thema
  Ökologie
isCrawled: true
---
Wie hängen Tierrechte und Antikapitalismus zusammen? Und was hat die Klimakrise eigentlich damit zu tun? 

Der eigentliche Vortrag zum Thema Ökologie und Feminismus musste leider weichen, da die Referentin ausfällt. Stattdessen freuen wir uns über einen Vortrag zum Thema Tierbefreiung, Antikapitalismus und Klimagerechtigkeit. 

Raum: 016 Lipsiusbau

Referentin: Tina Krawczyk