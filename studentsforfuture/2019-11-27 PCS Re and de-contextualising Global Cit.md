---
id: "480234572589609"
title: "PCS: Re and de-contextualising Global Citizenship Education"
start: 2019-11-27 17:15
end: 2019-11-27 18:45
locationName: GWZ- Das Geisteswissenschaftliche Zentrum
address: Beethovenstr. 15, 04107 Leipzig
link: https://www.facebook.com/events/480234572589609/
image: 74377563_439525330316283_5404148252368961536_n.jpg
teaser: "Titel : Re and de-contextualising Global Citizenship Education – systematic
  analysis of the scholarship in the field and empirical exploration of teac"
isCrawled: true
---
Titel : Re and de-contextualising Global Citizenship Education – systematic analysis of the scholarship in the field and empirical exploration of teachers' perceptions

Offenes Kolloquium des Instituts für Politikwissenschaft 

Referent*in:  Dr. Miri Yemini (Tel Aviv University)

Ort: Geisteswissenschaftliches Zentrum, Raum 4.1.16
(Beethovenstraße 15, Leipzig) 

Uhrzeit: 17.15 Uhr - 18.45 Uhr 

Im Institutskolloquium des Instituts für Politikwissenschaft wird Dr. Miri Yemini zu Themen globaler Bildung sprechen. Dies betrifft auch Fragen, welchen Stellenwert der Klimawandel im Kontext globaler Bildung zukommt. Interessierte sind herzlich willkommen, eine vorbereitende Lektüre ist nicht erforderlich.

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findet Ihr hier: https://studentsforfuture.info/ortsgruppe/leipzig/#calendar

Wir freuen uns auf Euch! 
