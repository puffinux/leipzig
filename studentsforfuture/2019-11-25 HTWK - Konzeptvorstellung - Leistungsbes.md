---
id: "2457602151177133"
title: HTWK - Konzeptvorstellung - Leistungsbestimmung Solarzellen
start: 2019-11-25 09:30
end: 2019-11-25 11:30
address: HTWK
link: https://www.facebook.com/events/2457602151177133/
image: 77060466_438763103725839_7347396528882843648_n.jpg
teaser: 'Vorlesung zum Thema: "Konzeptvorstellung für eine optimierte
  Leistungsbestimmung von Solarzellen"  Dozent*innen: Prof. Rudolph, Julian
  Hoffmann, Eduar'
isCrawled: true
---
Vorlesung zum Thema:
"Konzeptvorstellung für eine optimierte Leistungsbestimmung von Solarzellen"

Dozent*innen: Prof. Rudolph, Julian Hoffmann, Eduard Betko

Wo? N001