---
id: "2698100366919069"
title: "PCS: Lebensmittel für die Tonne? Retten statt wegwerfen"
start: 2019-11-26 11:00
end: 2019-11-26 13:00
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/2698100366919069/
image: 76695134_444872519781564_6597178717091397632_n.jpg
teaser: "Titel: Lebensmittel für die Tonne? Retten statt wegwerfen Referent*innen:
  foodsharing Leipzig Ort: HS 11 / Roberto Antonio Argueta (Honduras)  Zeit: 1"
isCrawled: true
---
Titel: Lebensmittel für die Tonne? Retten statt wegwerfen
Referent*innen: foodsharing Leipzig
Ort: HS 11 / Roberto Antonio Argueta (Honduras) 
Zeit: 11-13 Uhr

Wie viele noch verwertbare Lebensmittel landen im Schnitt in der Tonne? Ist Lebensmittelverschwendung ein für uns relevantes Problem? Wenn ja, wie gehen wir damit um? Was können die Tafel, foodsharing und andere Initiativen tun und was sollte eigentlich die Regierung machen? Fragen über Fragen, die wir mit euch diskutieren wollen. Joan und Anastasia arbeiten seit mehreren Jahren aktiv bei foodsharing e.V. mit und Teilen gerne ihre Erfahrungen mit euch. Vorwissen zum Thema ist nicht notwendig.

Diese Veranstaltung ist teil der Public Climate School. 
Das vollständige Programm findet ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 