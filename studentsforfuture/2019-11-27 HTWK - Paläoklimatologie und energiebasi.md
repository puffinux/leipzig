---
id: "711640269244196"
title: HTWK - Paläoklimatologie und energiebasierte Modellierung
start: 2019-11-27 12:00
end: 2019-11-27 13:30
locationName: HTWK Leipzig
address: Karl-Liebknecht-Str. 132, 04277 Leipzig
link: https://www.facebook.com/events/711640269244196/
image: 75380185_438909633711186_2380197719265247232_n.jpg
teaser: 'Vorlesung zum Thema: "Paläoklimatologie und energiebasierte
  Modellierung"  Dozent*in: Prof. Merker  Wo? G119'
isCrawled: true
---
Vorlesung zum Thema:
"Paläoklimatologie und energiebasierte Modellierung"

Dozent*in: Prof. Merker

Wo? G119