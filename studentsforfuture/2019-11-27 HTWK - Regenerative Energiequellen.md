---
id: "2595076264061044"
title: HTWK - Regenerative Energiequellen
start: 2019-11-27 11:15
end: 2019-11-27 12:45
locationName: HTWK Leipzig
address: Karl-Liebknecht-Str. 132, 04277 Leipzig
link: https://www.facebook.com/events/2595076264061044/
image: 74611005_438906743711475_8132258925568327680_n.jpg
teaser: 'Vorlesung zu dem Thema: "Regenerative Energiequellen"  Dozent*in: Prof. Dr.
  Jung  Wo? Li110'
isCrawled: true
---
Vorlesung zu dem Thema:
"Regenerative Energiequellen"

Dozent*in: Prof. Dr. Jung

Wo? Li110