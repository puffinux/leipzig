---
id: "1483742958447083"
title: "PCS: Höher, schneller, heißer? - Workshop zur Klimakrise"
start: 2019-11-28 11:15
end: 2019-11-28 12:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/1483742958447083/
image: 76178145_439705413631608_4412015660306006016_n.jpg
teaser: "Höher, schneller, heißer? - Workshop zur Klimakrise und Wirtschaftswachstum
  Referent: Julian - Konzeptwerk- neue Ökonomie Raum: NSG 328 Uhrzeit: 11:15"
isCrawled: true
---
Höher, schneller, heißer? - Workshop zur Klimakrise und Wirtschaftswachstum
Referent: Julian - Konzeptwerk- neue Ökonomie
Raum: NSG 328
Uhrzeit: 11:15- 12:45 Uhr
Bei Fragen zur Klimakrise wird ein Aspekt kaum hinterfragt: Eine stetig wachsende Wirtschaft. Wir beschäftigen uns mit dem Zusammenhang von Wirtschaftswachstum und Klima und diskutieren Alternativen.

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findet ihr bei studentsforfuture.info
Wir freuen uns auf euch!