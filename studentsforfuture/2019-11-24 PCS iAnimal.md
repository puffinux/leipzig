---
id: "537133320475743"
title: "PCS: iAnimal"
start: 2019-11-24 18:00
end: 2019-11-24 21:00
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/537133320475743/
image: 76968592_444869259781890_8116000017631674368_n.jpg
teaser: "Titel: iAnimal (mit Virtual Reality in die industrielle Tierhaltung)
  Referent*innen: Aktionsgruppe Leipzig der Albert Schweitzer Stiftung für
  unsere M"
isCrawled: true
---
Titel: iAnimal (mit Virtual Reality in die industrielle Tierhaltung)
Referent*innen: Aktionsgruppe Leipzig der Albert Schweitzer Stiftung für unsere Mitwelt
Ort: NSG S303, Hauptcampus
Zeit: 17.15 -18.45 Uhr

Modernste Technologie vermittelt eindringlich, wie es sich anfühlt, ein Tier in der Fleischindustrie zu sein. Wir zeigen euch mit 360°-Videos einen authentischen Einblick in Mastbetriebe und Schlachthäuser.

Diese Veranstaltung ist teil der Public Climate School. 
Das vollständige Programm findet ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 