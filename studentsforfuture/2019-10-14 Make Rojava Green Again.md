---
id: "2513271022239251"
title: Make Rojava Green Again
start: 2019-10-14 13:00
end: 2019-10-14 15:00
address: S 420 / Campus Augustusplatz / Uni Leipzig
link: https://www.facebook.com/events/2513271022239251/
image: 70713299_1026269730876559_2614444080242884608_o.jpg
teaser: Mo. 14.10.19 / 13.00  Make Rojava Green Again  Students for Future Leipzig /
  S420  Im März 2016 wurde die autonome Förderation Nordsyrien, auch bekann
isCrawled: true
---
Mo. 14.10.19 / 13.00

Make Rojava Green Again

Students for Future Leipzig / S420

Im März 2016 wurde die autonome Förderation Nordsyrien, auch bekannt unter dem Namen Rojava, ausgerufen. In diesem Zuge entstand die „Make Rojava Green Again“-Bewegung, welche Spenden sammelt und Projekte organisiert, um das Ökosystem in Rojava wiederherzustellen. In dem Vortrag werden sowohl die Projekte vorgestellt als auch die ökologischen, feministischen und basisdemokratischen Grundlagen des Gesellschaftssystems.