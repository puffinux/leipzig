---
id: "523354628306705"
title: Umweltpolitische Praxis der Zapatisten
start: 2019-11-13 19:00
end: 2019-11-13 21:00
locationName: HTWK Leipzig
address: Karl-Liebknecht-Str. 132, 04277 Leipzig
link: https://www.facebook.com/events/523354628306705/
image: 75322614_417950059140477_7093374338883125248_o.jpg
teaser: "Politische Praxis der Zapatisten zum Thema ökologischer Landwirtschaft und
  Schutz des Lakandonischen Urwald:  Die Ejército Zapatista de Liberación Nac"
isCrawled: true
---
Politische Praxis der Zapatisten zum Thema ökologischer Landwirtschaft und Schutz des Lakandonischen Urwald:

Die Ejército Zapatista de Liberación Nacional (EZLN, Zapatistische Armee der Nationalen Befreiung) ist eine überwiegend aus Indigenas bestehende Organisation in Chiapas, einem der ärmsten Bundesstaaten Mexikos, die am 1. Januar 1994 erstmals öffentlich in Erscheinung trat und sich seitdem mit politischen Mitteln für die Rechte der indigenen Bevölkerung Mexikos, aber auch generell gegen neoliberale Wirtschaftspolitik und für autonome Selbstverwaltung einsetzt. Teil ihrer politischen Praxis ist der Aufbau von ökologischer Landwirtschaft und Schutz des Regenwaldes in den Autonomiegebieten.
Der Vortrag soll gleichfalls eine Basis im Sinne der internationalen Solidarität für ein  Austauschprojekt mit den Zapatisten dienen.

Raum: 016 Lipsiusbau

Referent: Simon Schuster                                                          
