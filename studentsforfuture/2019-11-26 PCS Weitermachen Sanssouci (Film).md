---
id: "528477424667181"
title: "PCS: Weitermachen Sanssouci (Film)"
start: 2019-11-26 17:00
end: 2019-11-26 18:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/528477424667181/
image: 78610330_441599400108876_2375490048561774592_n.jpg
teaser: "Titel: Weitermachen Sanssouci Ein Film von über Klimakrise,
  Wissenschaftsbetrieb, Wirtschaftsberater, Studi-Proteste und
  Ernährungskontrolle Ort: HSZ"
isCrawled: true
---
Titel: Weitermachen Sanssouci
Ein Film von über Klimakrise, Wissenschaftsbetrieb, Wirtschaftsberater, Studi-Proteste und Ernährungskontrolle
Ort: HSZ M201, Hauptcampus (Radio Mephisto)
Zeit: 17:00 - 18:45 

Die Erde hat nicht die ideale Gestalt einer Kugel. Sie sieht vielmehr aus wie eine Kartoffel. Klimaforscherin Phoebe Phaidon kommt mit einem Lehrauftrag an das Institut für Kybernetik der Berliner Universität, um das Seminar zur „Einführung in die Simulationsforschung“ von Institutsleiterin Brenda Berger zu übernehmen. Diese muss sich ihrem Drittmittel-Projekt zur virtuellen Simulation des Klimawandels widmen, um das Institut vor der drohenden Einsparung durch die Hochschulleitung zu bewahren. Alles hängt von einer erfolgreichen Evaluation am Ende des Wintersemesters ab. Phoebe wird verpflichtet, an der Simulation mitzuarbeiten und eine Unternehmensberaterin wird als Motivations-Coach ans Institut geholt. Währenddessen zieht der neuberufene Stiftungsprofessor Alfons Abstract-Wege mit einem Projekt zu Ernährungskontrolle die Aufmerksamkeit auf sich, „Nudging“ wird zum Zauberwort. Phoebes Studierende, die dahinter einen Business-Plan vermuten, unterbrechen den Betrieb und besetzen die Bibliothek, während Phoebe mit ihrem Kollegen Julius Kelp zu einer Konferenz nach Gdansk reist und versucht, hinter das Geheimnis der Apokalypse zu kommen. Die Zeit läuft ab, denn der jüngste Tag bricht an.

Diese Veranstaltung ist Teil der Public Climate School.
Das vollständige Programm gibt es auf studentsforfuture.info
Wir freuen uns auf euch!

Trailer: www.youtube.com/watch?v=rOn-Q09Dz4Q 
Filmaufführungsrechte, Foto und Pressetext - mit Dank an © Amerikafilm & Filmgalerie 451 - Produktion | Kinoverleih | DVD Label sowie GEW Sachsen