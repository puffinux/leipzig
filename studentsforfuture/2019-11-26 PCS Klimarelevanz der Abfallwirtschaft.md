---
id: "2390711361190293"
title: "PCS: Klimarelevanz der Abfallwirtschaft"
start: 2019-11-26 17:15
end: 2019-11-26 18:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/2390711361190293/
image: 76618273_439113967024086_7683062456586862592_n.jpg
teaser: "Klimarelevanz der Abfallwirtschaft  Referent*in: André Albrecht, Zweckverband
  Abfallwirtschaft  Westsachsen (ZAW) Raum: WiFa SR 1  Uhrzeit. 17:15 - 18"
isCrawled: true
---
Klimarelevanz der Abfallwirtschaft

Referent*in: André Albrecht, Zweckverband Abfallwirtschaft  Westsachsen (ZAW)
Raum: WiFa SR 1 
Uhrzeit. 17:15 - 18:45

Interaktives Seminar zum Thema Abfall und Rohstoffe.

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findest du auf studentsforfuture.info/leipzig
 
Wir freuen uns auf euch!