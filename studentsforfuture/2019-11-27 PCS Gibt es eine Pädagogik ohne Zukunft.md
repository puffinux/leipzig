---
id: "426919291314284"
title: 'PCS: Gibt es eine "Pädagogik ohne Zukunft"?'
start: 2019-11-27 13:15
end: 2019-11-27 14:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/426919291314284/
image: 74532307_439503266985156_5350743555268673536_n.jpg
teaser: "Veranstaltung: \"Gibt es eine 'Pädagogik ohne Zukunft'? Und wenn ja, was
  bedeutet das für uns?\" Referent*innen: Dr. Florian Heßdörfer  Raum: NSG
  S017,"
isCrawled: true
---
Veranstaltung: "Gibt es eine 'Pädagogik ohne Zukunft'? Und wenn ja, was bedeutet das für uns?"
Referent*innen: Dr. Florian Heßdörfer 
Raum: NSG S017, Hauptcampus

„Es gibt keinen globalisierungsgeeigneten Planeten“ – Latours „Terrestrisches Manifest“ und die Frage nach einer ‚Pädagogik ohne Zukunft‘

Was ‚Klimakrise‘ heißt, ist nicht nur eine Frage an unsere kollektive Zukunft, sondern stellt auch einige Grundlagen von Bildung und Erziehung auf den Kopf. Wenn es zur Aufgabe der Erziehung gehört, kulturelle Errungenschaften und Praxen an die kommende Generation weiterzugeben, so wird diese Aufgabe spätestens dann zum Problem, wenn uns diese Errungenschaften dorthin gebracht haben, wo wir heute stehen. Für ein besseres Verständnis dieser Situation leihen wir uns zunächst einige Begriffe und Thesen aus Bruno Latours „Terrestrischem Manifest“. Anschließend widmen wir uns vor diesem Hintergrund der Frage, was ‚Bildung‘ in einer Welt bedeuten kann, die ohne die Zukunftszuversicht auskommen muss, von der die Geschichte der Bildung lange Zeit ausgehen konnte. Gibt es eine ‚Pädagogik ohne Zukunft‘? Und wenn ja, was bedeutet das für uns?

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findet Ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar

Wir freuen uns auf Euch !