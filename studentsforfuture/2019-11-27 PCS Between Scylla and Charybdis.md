---
id: "2595013923939735"
title: "PCS: Between Scylla and Charybdis"
start: 2019-11-27 15:00
end: 2019-11-27 16:30
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/2595013923939735/
image: 70838977_439512106984272_7322897343936724992_n.jpg
teaser: "Titel: Between Scylla and Charybdis: is an effective climate protection only
  achievable in an undemocratic way?  Referent*in: Prof. Dr. Bernd Klauer"
isCrawled: true
---
Titel: Between Scylla and Charybdis: is an effective climate protection only achievable in an undemocratic way?

Referent*in: Prof. Dr. Bernd Klauer

Raum: Chico Mendes (Brasilien)/ HS7

Uhrzeit: 15:00 - 16:30 Uhr

Greta Thumberg urges us: “listen to the scientists”. James Lovelock, famous for his Gaia hypothesis, states: “Humans are too stupid to prevent climate change.” Is democracy to slow and sluggish to take the necessary action? Do we have to set aside democratic decision processes for the moment and give power to the experts? The German historian Andreas Rödder, however, warns: „Climate activists do not accept other opinions because they believe their own opinion is scientifically without alternatives. The climate movement is undemocratic.“
Public discussion (english and german) // Öffentliche Diskussion (bilingual auf englisch und deutsch)

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findet Ihr unter:  https://studentsforfuture.info/ortsgruppe/leipzig/#calendar

Wir freuen uns auf Euch!