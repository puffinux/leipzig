---
id: "2558294467596907"
title: 'PCS: "The Story of Tomorrow" ein Film von Emily Bankert'
start: 2019-11-25 17:15
end: 2019-11-25 18:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/2558294467596907/
image: 77398609_441275270141289_4693432676433002496_n.jpg
teaser: "Titel: THE STORY OF TOMORROW Referntin und Regisseurin: Emily Bankert Ort:
  HS16 / Samir Flores Soberanes Zeit: 17:15 Uhr - 18:45  THE STORY OF TOMORRO"
isCrawled: true
---
Titel: THE STORY OF TOMORROW
Referntin und Regisseurin: Emily Bankert
Ort: HS16 / Samir Flores Soberanes
Zeit: 17:15 Uhr - 18:45

THE STORY OF TOMORROW
Although the threats of the climate crisis become seemingly more visible, we are easily overwhelmed and paralysed by the complexity of the topic. It often seems easier to not get too deep into a spiral of negative news and apocalyptic future scenarios. But as we acknowledge the need for change, it becomes more and more important to start engaging with the issue and what it means for us and the world.

This workshop and film aim to break through the complex and confusing facts of climate change by looking at the root problem of the climate crisis and where change needs to start happening.


Film: 37 min., 2019 (Englisch mit englischen Untertiteln)
Diskussion auf Deutsch & English

Diese Veranstaltung ist Teil der Public Climate School.
Das vollständige Programm gibt es auf studentsforfuture.info
Wir freuen uns auf euch!