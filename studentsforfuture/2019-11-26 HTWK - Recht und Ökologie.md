---
id: "748020035703479"
title: HTWK - Recht und Ökologie
start: 2019-11-26 09:30
end: 2019-11-26 11:00
locationName: HTWK Leipzig
address: Karl-Liebknecht-Str. 132, 04277 Leipzig
link: https://www.facebook.com/events/748020035703479/
image: 74802731_438770657058417_2557068458244177920_n.jpg
teaser: 'Vorlesung zum Thema: "Recht und Ökologie"  Dozent*in: Prof. Vor  Wo?
  Li114  !!!Die Veranstaltung findet im Anschluss von 11:15 bis 12:45 nochmal
  statt'
isCrawled: true
---
Vorlesung zum Thema:
"Recht und Ökologie"

Dozent*in: Prof. Vor

Wo? Li114

!!!Die Veranstaltung findet im Anschluss von 11:15 bis 12:45 nochmal statt!!!