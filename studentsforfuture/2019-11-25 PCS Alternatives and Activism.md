---
id: "2558577707588920"
title: "PCS: Alternatives and Activism"
start: 2019-11-25 11:15
end: 2019-11-25 12:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/2558577707588920/
image: 77128577_441254373476712_3820378950299811840_n.jpg
teaser: "Titel: Alternatives and Activism Referent*in: Prof. Dr. Welz Ort: NSG S124,
  Hauptcampus Zeit: 11.15-12.45 Uhr  *Geöffnete Veranstaltung  Zusammenhang"
isCrawled: true
---
Titel: Alternatives and Activism
Referent*in: Prof. Dr. Welz
Ort: NSG S124, Hauptcampus
Zeit: 11.15-12.45 Uhr

*Geöffnete Veranstaltung

Zusammenhang von Kolonialismus und Naturausbeutung im 19. Jahrhundert sprechen. Dabei geht es insbesondere um das British Empire im 19. Jahrhundert mit seiner weltweiten Ausbeutung von Ressourcen und der Errichtung von kapitalistischen Marktstrategien, die bis in unsere  
Gegenwart hineinwirken und noch immer maßgeblich unser heutiges Verständnis von Natur und Ökonomie prägen. Der Kurs untersucht anhand literarischer Texte die zeitgenössischen Wertvorstellungen, was die Grundlage für eine Problemdiskussion bildet (Letzteres könnte bei einer  
starken Beteiligung von dazukommenden Studierenden auch auf Deutsch erfolgen.

Diese Veranstaltung ist Teil der Public Climate School. Das vollständige Programm findet Ihr in Kürze unter: https://studentsforfuture.info/ortsgruppe/leipzig/#calendar
Wir freuen uns auf Euch! 