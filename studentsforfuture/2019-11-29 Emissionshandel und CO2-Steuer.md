---
id: "583644742440011"
title: Emissionshandel und CO2-Steuer
start: 2019-11-29 17:00
end: 2019-11-29 19:00
locationName: HTWK Leipzig
address: Karl-Liebknecht-Str. 132, 04277 Leipzig
link: https://www.facebook.com/events/583644742440011/
image: 73404048_417954225806727_7260421289360228352_o.jpg
teaser: Die Klimaschutzdebatte konzentriert sich derzeit darauf, wie der Ausstoß von
  Treibhausgasen politisch und ökonomisch am effektivsten gesenkt werden ka
isCrawled: true
---
Die Klimaschutzdebatte konzentriert sich derzeit darauf, wie der Ausstoß von Treibhausgasen politisch und ökonomisch am effektivsten gesenkt werden kann. Immer mehr in den Fokus rückt dabei eine verstärkte Bepreisung von Kohlendioxid für die Sektoren Wärme und Verkehr, die inzwischen von vielen Seiten unterstützt wird. Eine weitere Möglichkeit besteht darin, den bestehenden EU-Emissionshandel auf Verkehr und Gebäude auszuweiten.
Aber wie funktioniert das mit dem Emissionshandel und einer CO2-Steuer genau? Gibt es hierzu unterschiedliche Konzepte?
Sind diese Maßnahmen sozialverträglich umsetzbar und führen sie zu dem gewünschten Ergebnis einer drastischen Reduzierung der Schadstoff-Emissionen?   

Raum: 001 Nieperbau

Referent: Prof. Dr. Jung                                                             