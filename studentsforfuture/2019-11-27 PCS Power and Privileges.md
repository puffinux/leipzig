---
id: "3245544532153917"
title: "PCS: Power and Privileges"
start: 2019-11-27 13:15
end: 2019-11-27 14:45
locationName: UNIVERSITÄT LEIPZIG
address: Augustusplatz 10, 04109 Leipzig
link: https://www.facebook.com/events/3245544532153917/
image: 75540271_439504900318326_7886572777463873536_n.jpg
teaser: "Titel/title: Power and Privileges: How to achieve the climate movement we
  want Referent*in: Tonny Nowshin Raum/room: NSG S211, Hauptcampus Uhrzeit/tim"
isCrawled: true
---
Titel/title: Power and Privileges: How to achieve the climate movement we want
Referent*in: Tonny Nowshin
Raum/room: NSG S211, Hauptcampus
Uhrzeit/time : 13.15 Uhr - 14.45 Uhr 

The Workshop will aim to collectively discuss how can we make our movement more inclusive and how to adress cross-sectional issues through the climate movement.

This event is part of the Public Climate School. You can find the program here: https://studentsforfuture.info/ortsgruppe/leipzig/#calendar

We're looking forward seeing you there! 

Language: English