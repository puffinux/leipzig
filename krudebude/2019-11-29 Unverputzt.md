---
id: "762063277555114"
title: Unverputzt
start: 2019-11-29 19:00
end: 2019-12-01 19:00
locationName: Projektwohnung "krudebude"
address: Stannebeinplatz 13, 04347 Leipzig
link: https://www.facebook.com/events/762063277555114/
image: 76693341_2684551738303825_1224659020502007808_n.jpg
teaser: freitag|29.11. |19 – 21 uhr |eröffnung samstag|30.11.|16 – 19
  uhr  sonntag|1.12. |16 – 19 uhr  krudebude|stannebeinplatz 13|leipzig
  eintritt|frei  ein
isCrawled: true
---
freitag|29.11. |19 – 21 uhr |eröffnung
samstag|30.11.|16 – 19 uhr 
sonntag|1.12. |16 – 19 uhr

krudebude|stannebeinplatz 13|leipzig
eintritt|frei

eine ausstellung der studierenden|fachbereich dramaturgie| medienpraxis|hmt leipzig

Eine Ausstellung von fünf Dramaturgiestudierenden, hhm? Bitte warten Sie hier! Was soll das? Ich will hier rein! Ich bin jetzt extra hier angerückt am Freitagabend! Worauf soll ich denn jetzt hier warten? Wer hat das Schild da überhaupt aufgestellt? – Okay, ich stell mich hinten an. Was ist das, ein Video? Gehen Sie mal bitte aus dem Bild! Unverschämt. War ihr Vater Glaser oder was? Sie kennen das Sprichwort nicht? Egal, jedenfalls macht man das so nicht, nehmen Sie mal Rücksicht auf die anderen, ja? Ah, die Bar in Halle kenne ich. – Und das da, eine Skulptur! Cool. Versteh ich aber nicht. Gibt’s da mal eine Erklärung zu? – Ja, ich komm schon. – Wow, endlich Platz. Die bewegen sich ja voll gleich, Kirche, Karneval, Fußballstadion. Hast du das schon mal so gesehen? – Uh, Datingportale. Hast du sowas schonmal genutzt? Sag ich nicht, ja schon... das ist Privatsache. Privatsachen gibt’s nicht? Wer sagt das, machst du hier die Regeln, oder was?

Stradtraum, Stadion, Kirche, Warteraum, Bar und Dating-Plattform – wir haben uns auf die Suche nach der Wechselwirkung zwischen Räumen und sozialem Verhalten begeben. Inwiefern regulieren diese Räume unser Handeln? Welche Strukturen bilden sich in den Räumen ab, welche sozialen Regeln stellen sie auf? Wer macht die Regeln? Und warum halten wir uns an sie? e . 
Als Installation, Film, Found-Footage-Montage, Interview und Figuration teilen wir mit euch den Jetzt-Stand unserer Arbeiten.

mit arbeiten von|
judith behrens|nicolaj gnirss|lea rießen|josefine sander|julia wassner

projektleitung|
christine lang


