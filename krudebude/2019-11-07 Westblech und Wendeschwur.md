---
id: "520488541856547"
title: Westblech und Wendeschwur
start: 2019-11-07 18:00
end: 2019-11-07 21:00
locationName: Projektwohnung "krudebude"
address: Stannebeinplatz 13, 04347 Leipzig
link: https://www.facebook.com/events/520488541856547/
image: 70794558_2571249692967364_5974586395371503616_n.jpg
teaser: "Westblech und Wendeschwur | Geschichten aus der (Nach-)Wendezeit  VERNISSAGE:
  10.10.19  | 19 Uhr   Anlässlich des 30. Wende-Jubiläums wollten wir Neu-"
isCrawled: true
---
Westblech und Wendeschwur | Geschichten aus der (Nach-)Wendezeit

VERNISSAGE: 10.10.19  | 19 Uhr 

Anlässlich des 30. Wende-Jubiläums wollten wir Neu-Leipziger*innen, die selbst diesen historischen Umbruch nicht miterlebt haben, es genauer wissen: Wie war das eigentlich mit der Wende? Wie erlebten unsere Nachbar*innen in Leipzig die Zeit um  '89 und danach? 
Dafür haben wir Zeitzeug*innen aus unserer Nachbarschaft interviewt und sie nach ihren ganz persönlichen Erfahrungen und Erinnerungen gefragt. Aus diesen Gesprächen ist eine Ausstellung entstanden, in der unterschiedliche und subjektive Perspektiven auf die (Nach-)Wendezeit zu Wort kommen. 

Öffnungszeiten
DO / FR : 18 – 21 UHR 
SA / SO : 15 –18 UHR

///Rahmenprogramm/// 

13/10 15:00 - „SPEEDDATING“ ZUM THEMA NACHWENDEZEIT

19/10 14:00 - FAHRRADTOUR DURCH SCHÖNEFELD

24/10 19:00 - INITIATIVEN STELLEN SICH VOR 
u.A „Aufbruch Ost“ & Workshopreihe zu „Friedl. Revolution & Migrant*innen“ 

25/10 19:30 - ALLES AUF ANFANG DAS WILDE JAHR 1990 
Filmvorstellung mit Regisseur Peter Wensierski

26/10 19:00 - VON VERLORENEN ILLUSIONEN
eine performative Lesung von Elisa Ueberschär

10/11 19:00 - FINISSAGE
mit „Speeddating“

Diese Veranstaltung ist gefördert durch die Leipzigstiftung, das Kulturamt der Stadt Leipzig und den Verfügungsfond Schönefeld. 

