---
id: "587788345364831"
title: Comic Workshop
start: 2019-11-21 17:00
end: 2019-11-21 20:00
locationName: Projektwohnung "krudebude"
address: Stannebeinplatz 13, 04347 Leipzig
link: https://www.facebook.com/events/587788345364831/
image: 74674491_2664199297005736_5646515099423211520_n.jpg
teaser: Comic ist ein riesen Themenfeld und von visuellen Kommunikationsregeln, über
  Charakterdesign bis zu klassischen Erzählstrukturen soll hier alles anged
isCrawled: true
---
Comic ist ein riesen Themenfeld und von visuellen Kommunikationsregeln, über Charakterdesign bis zu klassischen Erzählstrukturen soll hier alles angedacht werden, was das Medium zu bieten hat. Irgendwo zwischen handwerklicher Schulung und theoretischer Diskussion soll sich dieser Workshop verorten, Vorkenntnisse sind in beiden Fällen nicht nötig, Lust und Laune reichen aus.

21.11. 17-20 Uhr 

Bitte meldet euch vorher an: kontakt@krudebude.de