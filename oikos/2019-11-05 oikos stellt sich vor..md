---
id: "542502646319136"
title: oikos stellt sich vor.
start: 2019-11-05 19:00
end: 2019-11-05 20:30
address: Wirtschaftsfakultät der Uni Leipzig, SR 5
link: https://www.facebook.com/events/542502646319136/
image: 73258404_2520764308038056_7968616222004609024_n.png
teaser: Das neue Semester hat begonnen und wir freuen uns gemeinsam mit Euch wieder
  Nachhaltigkeit in all ihren Formen zu fördern und zu leben!  oikos ist ein
isCrawled: true
---
Das neue Semester hat begonnen und wir freuen uns gemeinsam mit Euch wieder Nachhaltigkeit in all ihren Formen zu fördern und zu leben!

oikos ist eine internationale Studierendenorganisation, die sich für die Förderung einer nachhaltigen Gesellschaft im Spannungsfeld zwischen Ökonomie, Ökologie und Sozialem einsetzt. Wir beschäftigen uns mit vielen verschiedenen Themen von Textilindustrie über Postwachstum bis hin zum essentiellen Thema Klimawandel. Wir sind immer offen für neue Ideen und Gesichter.

Wir möchten uns Euch daher näher vorstellen und dabei die Möglichkeit geben, in ganz entspannter Atmosphäre ein näheres Bild von uns als Verein zu bekommen. Dabei sind sowohl Studierende als auch Externe herzlich willkommen!



WANN?
Dienstag, 05.11.2019 um 19:00 Uhr

WO?
Institutsgebäude WiFa, Seminarraum 5

Wir freuen uns auf Euch. 

Falls ihr noch Fragen habt, schreibt uns gerne eine Nachricht :)
