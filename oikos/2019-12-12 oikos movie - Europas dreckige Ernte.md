---
id: "583817199112453"
title: oikos movie - Europas dreckige Ernte
start: 2019-12-12 19:30
end: 2019-12-12 21:30
locationName: oikos Leipzig
address: Grimmaische Str. 12, 04109 Leipzig
link: https://www.facebook.com/events/583817199112453/
image: 78199542_2605721589542327_9182350825685516288_n.jpg
teaser: Im Rahmen der Rezertifizierung der Uni Leipzig als FAIR TRADE Uni, zeigen wir
  die Dokumentation "Europas dreckige Ernte" von Vanessa Lünenschloß und J
isCrawled: true
---
Im Rahmen der Rezertifizierung der Uni Leipzig als FAIR TRADE Uni, zeigen wir die Dokumentation "Europas dreckige Ernte" von Vanessa Lünenschloß und Jan Zimmermann. 
- Mit anschließender Diskussion - 

Filmstart: 19:30 Uhr 
Wifa Institutsgebäude SR7 (1. Stock) 

KOSTENLOS 
GLÜHWEIN GEGEN SPENDE

Über den Film: 
"Europas dreckige Ernte" zeigt, wie unser Obst und Gemüse den Weg ins Supermarktregal schafft. Dabei beleuchtet der Film die gesamte Wertschöpfungskette mit all seinen Aktueren. Eine Doku über die europäische Agrarindustrie, ein subventioniertes Milliardengeschäft und (un)fairen Handel. 