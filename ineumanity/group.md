---
name: inEUmanity
website: https://ineumanity.noblogs.org/
scrape:
  source: facebook
  options:
    page_id: 697420763756249
---
By watching the militarization of the internal and external EU-borders and the planned tightening of the European asylum law through the new CEAS, including Dublin IV, we felt forced to get together to take action against the aggressive migration policy of the European Union. With our political work including information-events, workshops and an online-blog, we aim to raise attention on the current situation of refugees and migrants on their way to and stuck in Europe.

In our point of view it is not possible to discuss these issues without looking at the wider context of global inequality and exploitation as the root of refuge. As an important player of global exploitation the EU can be held responsible for the destabilization of the Global South causing war, famine and economical- and natural-catastrophes. It follows the interest of the EU to repel the consequences of their destruction by keeping refugees outside of “Fortress Europe”. In consequence, refugees have to face fences, restrictive laws, violence and cold ignorance.

Therefore we fight for a policy of true solidarity instead of the now practiced exclusion and isolation of “Fortress Europe”.
Our aim is to create a big publicity, to get connected with other individuals and groups and to fight together for the freedom of movement and the right to stay!

Join our fight!