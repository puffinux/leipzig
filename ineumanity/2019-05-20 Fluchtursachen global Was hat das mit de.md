---
id: '615654908908766'
title: Fluchtursachen global? Was hat das mit der EU zu tun?
start: '2019-05-20 17:00'
end: '2019-05-20 19:00'
locationName: null
address: 'Uni Leipzig Hauptcampus, Seminargebäude S 420'
link: 'https://www.facebook.com/events/615654908908766/'
image: 55674641_907068566130010_8851173259064901632_o.jpg
teaser: 'Achtung, neues Datum! verlegt auf: 20.05. um 17.00 - 19.00,   Fluchtursachen global? Was hat das mit der EU zu tun?  Weltweit sind etwa 65 Millionen M'
recurring: null
isCrawled: true
---
Achtung, neues Datum! verlegt auf: 20.05. um 17.00 - 19.00, 

Fluchtursachen global? Was hat das mit der EU zu tun?

Weltweit sind etwa 65 Millionen Menschen auf der Flucht. Die Ursachen dafür sind vielfältig und komplex. Wir wollen gemeinsam versuchen uns einem Verständnis von Fluchtursachen anzunähern, welches sowohl geschichtliche als auch politische Zusammenhänge mitdenkt. Vor allem aber soll die Frage beantwortet werden: Was hat das denn alles mit uns im globalen Norden zu tun?