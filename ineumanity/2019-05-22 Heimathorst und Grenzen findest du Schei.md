---
id: '2204895006215180'
title: Heimathorst und Grenzen findest du Scheiße? – Offenes Plenum
start: '2019-05-22 19:00'
end: '2019-05-22 21:00'
locationName: null
address: Campus Augustusplatz / S 015
link: 'https://www.facebook.com/events/2204895006215180/'
image: 55618320_907085952794938_2181944429768605696_o.jpg
teaser: 'Mi, 22.05., 19 – 21 Uhr:  Heimathorst und Grenzen findest du Scheiße? – Offenes Plenum InEUmanity  InEUmanity / Campus Augustusplatz / S 015  Wir woll'
recurring: null
isCrawled: true
---
Mi, 22.05., 19 – 21 Uhr:

Heimathorst und Grenzen findest du Scheiße? – Offenes Plenum InEUmanity

InEUmanity / Campus Augustusplatz / S 015

Wir wollen mit Euch gemeinsam über die Asylpolitik in der EU und in Deutschland diskutieren. Ob EU-Türkei-Deal, die Schließung der Balkanroute oder Seehofers Ankerzentren. Themen gibt es viele und so beschäftigen wir uns mit EU-Politik, deutscher Asylpolitik und den größeren Zusammenhängen von Flucht und Migration. Ob kreative Aktionen, Informations-Kampagnen oder Vortragsreihen. Bring gerne eigene Ideen mit und lass uns gemeinsam und solidarisch Grenzen überwinden!
