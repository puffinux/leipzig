---
id: '584449798631465'
title: Autoritäre Bearbeitung sozialer Probleme
start: '2019-05-23 19:00'
end: '2019-05-23 21:00'
locationName: null
address: Campus Augustusplatz / HS 11
link: 'https://www.facebook.com/events/584449798631465/'
image: 55594361_907087002794833_9045141631489015808_o.jpg
teaser: 'Do, 23.05., 19 – 22 Uhr:  Autoritäre Bearbeitung sozialer Probleme  CopWatch LE, SJL / Campus Augustusplatz / HS 11  Durch Verräumlichung sozialer Pro'
recurring: null
isCrawled: true
---
Do, 23.05., 19 – 22 Uhr:

Autoritäre Bearbeitung sozialer Probleme

CopWatch LE, SJL / Campus Augustusplatz / HS 11

Durch Verräumlichung sozialer Probleme und einem zunehmend repressiven Umgang mit Kriminalität wird eine autoritäre Entwicklung vorangetrieben, der wir die Waffenverbotszone und neue Polizeigesetze verdanken. Fehlende (soziale) Sicherheit wird mit Law-and-order beantwortet. Wie begegnen wir einem versicherheitlichtem Diskurs und was können solidarische Mechanismen zur Bekämpfung sozialer Probleme sein, die meist die Grundlage von Kriminalität bilden? 
