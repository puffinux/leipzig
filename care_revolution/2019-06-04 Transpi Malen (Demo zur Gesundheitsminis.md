---
id: '2026754997621388'
title: Transpi Malen (Demo zur GesundheitsministerInnenkonferenz)
start: '2019-06-04 17:00'
end: '2019-06-04 19:00'
locationName: null
address: vor dem CLI (Liebigstraße 27)
link: 'https://www.facebook.com/events/2026754997621388/'
image: 60591682_2238613319735902_193758778020069376_n.jpg
teaser: Am 05. Und 06. Juni treffen sich die Gesundheitsminister*innen der Länder im Leipziger Steigenberger Hotel um über die Zukunft der deutschen Gesundhei
recurring: null
isCrawled: true
---
Am 05. Und 06. Juni treffen sich die Gesundheitsminister*innen der Länder im Leipziger Steigenberger Hotel um über die Zukunft der deutschen Gesundheitsversorgung zu diskutieren. Bundesgesundheitsminister Jens Spahn wird ebenfalls anwesend sein. Diese Gelegenheit möchten wir nutzen, um unseren Protest und unsere Forderungen nach weitreichenden Änderungen im Finanzierungssystem sowie nach einer Entlastung aller Beschäftigten auf die Straße zu tragen. 
https://www.facebook.com/events/1288644451284807/

Am 05. beginnt um 11:00 Uhr die Demonstration, an deren Ende der nun seit Monaten durch Deutschland tourende „olympische Brief“ übergeben werden soll. Um unseren Protest möglichst bunt und kreativ zu gestalten, treffen wir uns ab 17:00 vor dem CLI zum Transparente malen und einander Kennenlernen. Also packt euch ne Limo ein, kommt rum und lasst uns gemeinsam in der Sonne kreativ werden. 

Im Anschluss findet im CLI noch eine Veranstaltung der AG krit. Med. zum Thema „Utopien im Gesundheitswesen“ mit dem Referent Peter Hoffmann vom vdää statt, zu der ihr alle herzlich eingeladen seid. 
https://www.facebook.com/events/2298604653696042/