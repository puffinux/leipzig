---
id: '2257382034530953'
title: 'VHS-Veranstaltungsreihe: Positive Alternativen – Beispiele zu St'
start: '2019-05-23 18:00'
end: '2019-05-23 20:00'
locationName: null
address: 'Löhrstraße 3, 04105 Leipzig'
link: 'https://www.facebook.com/events/2257382034530953/'
image: 51868986_2166836056965205_6432824994576728064_n.jpg
teaser: 'Die Zahnärztin in Großzschocher, der Psychotherapeut in Reudnitz und die Krankengymnastik in Gohlis? Wie schön wäre es, einfachen Zugang zur Gesundhei'
recurring: null
isCrawled: true
---
Die Zahnärztin in Großzschocher, der Psychotherapeut in Reudnitz und die Krankengymnastik in Gohlis? Wie schön wäre es, einfachen Zugang zur Gesundheitsversorgung im eigenen Viertel zu haben? Auch in diesem Bereich kann es hilfreich sein, den Blick von den Missständen hin zu positiven Beispielen und gelungenen Alternativen zu erweitern. In dem Zuge wollen wir über Wohnprojekte und solidarische Nachbarschaften genauso sprechen wie darüber, wie Gesundheit im Stadtteil gemeinsam organisiert werden kann.
