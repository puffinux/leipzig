---
id: '1288644451284807'
title: Demo zur Gesundheitsminister*innenkonferenz
start: '2019-06-05 11:00'
end: '2019-06-05 14:00'
locationName: null
address: gegenüber vom Hauptbahnhof Lpz.
link: 'https://www.facebook.com/events/1288644451284807/'
image: 60998893_2239403056323595_2848699190068903936_n.jpg
teaser: Am 05. und 06.06. treffen sich die Gesundheitsminsiter*innen der Länder und Bundesgesundheitsminister Jens Spahn um über die Zukunft unseres Versorgun
recurring: null
isCrawled: true
---
Am 05. und 06.06. treffen sich die Gesundheitsminsiter*innen der Länder und Bundesgesundheitsminister Jens Spahn um über die Zukunft unseres Versorgungssystems zu beraten. 

Die letzten Jahre zeigten, dass ein breiter gesellschaftlicher Druck viel zu erreichen vermag. Die geplanten Gesetzesänderungen sind nur durch den Druck vieler streikender Kolleg*innen und einer breiten öffentlichen Solidarität denkbar geworden. Jedoch sind diese Änderungen unzureichend und an dem Kern des Problems, der zunehmende Ökonomisierung und Ausrichtung des Gesundheitswesens an Profiten, hat sich nichts geändert. Nun heißt es weiter Druck zu machen und nicht locker zu lassen, bis tiefgreifende Reformen auch in den Parlamenten durchgebracht wurden. 

Wir, ein Zusammenschluss verschiedenster gesundheitspolitischer Gruppen in Leipzig zeigen uns solidarisch mit allen unter den Bedingungen leidenden Patient*innen und Beschäftigten in Pflege und anderen mit der Gesundheitsversorgung assoziierten Berufen. Wir unterstützen ihre Forderungen nach einer gesetzlich festgeschriebenen Personalbemessung und rufen auf zur gemeinsamen Ver.di Demo am 05.06. . (https://www.facebook.com/events/840835086251229/permalink/853923034942434/)

Los geht's um 11 mit einer Auftaktkundgebung gegenüber vom Hauptbahnhof. Nach der Demo über den Leipziger Ring, findet in Hörweite des Tagungsortes (Steigenberger Hotel) die Abschlusskundgebung inclusive Übergabe des seit Monaten durch Deutschland wandernden olympischen Briefs statt. 

Um den Protest möglichst bunt und kreativ zu gestalten, gibt es am 04.06. ab 17:00 eine gemeinschaftliche "Mal- und Bastelaktion" auf der Liebigstraße in Nähe des Uniklinikums. 
https://www.facebook.com/events/2026754997621388/
